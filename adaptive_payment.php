<?php 
ob_start();      
session_start();
header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
define( '_TEXEC', 1 );
define('TPATH_BASE', dirname(__FILE__) );
define( 'DS', DIRECTORY_SEPARATOR );
//header('Content-Type: text/html; charset=iso-8859-1');  

require_once ( TPATH_BASE .DS.'includes'.DS.'defines.php' ); 
require_once ( TPATH_BASE .DS.'includes'.DS.'configuration.php' ); 
ini_set("track_errors", true); 

//echo "<pre>";
//print_r($_SESSION); exit;


$returnURL = $tconfig['tsite_url']."adaptive_success.php?id=".$_SESSION['temp_booking_id'];
$cancleURL = $tconfig['tsite_url'];

// PLS REMOVE BELOW 2 LINES WHEN U  MAKE SITE LIVE
header("Location:".$returnURL);
exit;

$primary_receiver_amount = ($_SESSION['sess_bookingamount'] * $_SESSION['sess_commissionrate']) / 100;
$secondary_receiver_amount = $_SESSION['sess_bookingamount'] - $primary_receiver_amount; 

//set PayPal Endpoint to sandbox  
$url = trim("https://svcs.sandbox.paypal.com/AdaptivePayments/Pay");
//$url = trim("https://svcs.paypal.com/AdaptivePayments/Pay");


//PayPal API Credentials
//$API_UserName = 'paypal_api1.Blablacar.eu'; #'rahulb.esw_api1.gmail.com';   //TODO
$API_UserName = 'rahulb.esw_api1.gmail.com'; #'rahulb.esw_api1.gmail.com';   //TODO
$API_Password = '1383023224'; #'1383023224';   //TODO    
$API_Signature = 'An5ns1Kso7MWUdW4ErQKJJJ4qi4-AP0S4ZqnC6SxL3jsa28GEbG34NvR'; #'An5ns1Kso7MWUdW4ErQKJJJ4qi4-AP0S4ZqnC6SxL3jsa28GEbG34NvR';   //TODO  
	
//Default App ID for Sandbox	
$API_AppID = "APP-80W284485P519543T"; #"APP-80W284485P519543T"; 

$API_RequestFormat = "NV";
$API_ResponseFormat = "NV"; 

//Create request payload with minimum required parameters
$bodyparams = array (	"actionType"=> 'PAY_PRIMARY',     
											"clientDetails.applicationId"=> 'APP-80W284485P519543T', #'APP-80W284485P519543T', 
											"clientDetails.ipAddress"=> '192.168.1.44', #89.221.244.144
											"currencyCode"=> $_SESSION['sess_price_ratio'], 
											"feesPayer"=> 'PRIMARYRECEIVER',#'EACHRECEIVER',
											"memo"=> 'Ride of: '.$_SESSION['sess_mainaddress'],
											"receiverList.receiver(0).amount"=> $_SESSION['sess_bookingamount'],
											"receiverList.receiver(0).email"=> 'rahulb.esw@gmail.com', #'rahulb.esw@gmail.com',  
											"receiverList.receiver(0).primary"=> 'true',
											"receiverList.receiver(1).amount"=> $secondary_receiver_amount,
											"receiverList.receiver(1).email"=> 'test@test.com', #'harshilmehta1982@gmail.com', 
											"receiverList.receiver(1).primary"=> 'false',
											"requestEnvelope.errorLanguage"=> 'en_US',
											"returnUrl"=> $returnURL,
											"cancelUrl"=> $cancleURL
											);
											
// convert payload array into url encoded query string
$body_data = http_build_query($bodyparams, "", chr(38));  


try
{

    //create request and add headers
    $params = array("http" => array( 
    																 "method" => "POST",
                  									 "content" => $body_data,
                  									 "header" =>  "X-PAYPAL-SECURITY-USERID: " . $API_UserName . "\r\n" .
                               										"X-PAYPAL-SECURITY-SIGNATURE: " . $API_Signature . "\r\n" .
                 							 										"X-PAYPAL-SECURITY-PASSWORD: " . $API_Password . "\r\n" .
                   						 										"X-PAYPAL-APPLICATION-ID: " . $API_AppID . "\r\n" .
                   						 										"X-PAYPAL-REQUEST-DATA-FORMAT: " . $API_RequestFormat . "\r\n" .
                  						 										"X-PAYPAL-RESPONSE-DATA-FORMAT: " . $API_ResponseFormat . "\r\n" 
                  																));
    //echo "<pre>";print_r($params);exit;

    //create stream context
     $ctx = stream_context_create($params);
     

    //open the stream and send request
     $fp = @fopen($url, "r", false, $ctx);

    //get response
  	 $response = stream_get_contents($fp);
     
  	//check to see if stream is open
     if ($response === false) {
        throw new Exception("php error message = " . "$php_errormsg");
     }
           
    //close the stream
     fclose($fp);

    //parse the ap key from the response
    $keyArray = explode("&", $response);
    //echo "<pre>";print_r($keyArray);exit;    
    foreach ($keyArray as $rVal){
    	list($qKey, $qVal) = explode ("=", $rVal);
			$kArray[$qKey] = $qVal;
    }
      
    //set url to approve the transaction
    //$payPalURL = "https://www.paypal.com/webscr?cmd=_ap-payment&paykey=" . $kArray["payKey"];
    $payPalURL = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_ap-payment&paykey=".$kArray["payKey"];
    
    //print the url to screen for testing purposes
    if ( $kArray["responseEnvelope.ack"] == "Success") {
    	//echo '<p><a href="' . $payPalURL . '" target="_blank">' . $payPalURL . '</a></p>';
    	if($kArray["payKey"] != ''){
         $sql = "UPDATE temp_booking SET vAdaptivePaymentKey = '".$kArray["payKey"]."' WHERE iBookingId = '".$_SESSION['temp_booking_id']."'";
         $db_sql=$obj->sql_query($sql);
         
         if($db_sql){
          header("Location:".$payPalURL);
          exit;
         }
      }      	
     }
    else {
    	echo 'ERROR Code: ' .  $kArray["error(0).errorId"] . " <br/>";
      echo 'ERROR Message: ' .  urldecode($kArray["error(0).message"]) . " <br/>";
      
      exit;
    }
    
    exit;
            
   	//optional code to redirect to PP URL to approve payment  
    if($kArray["responseEnvelope.ack"] == "Success"){
      //echo "<pre>";
      //print_r($_SESSION); exit;
     
      $payment_details = $_SESSION['payment_details'];
      $confirm = $_SESSION['payment_details']['confirm'];
      for($i=0;$i<count($confirm);$i++){
        $Data_payment = array();
        
        $sql = "SELECT vBookingNo, iRideId, vFromPlace, vToPlace, dBookingDate, dBookingTime, iDriverId, eTripReturn, fAmount, vBookerCurrencyCode FROM booking_new WHERE iBookingId = '".$confirm[$i]."'";
        $db_booking = $obj->MySQLSelect($sql);
        
        if($db_booking[0]['eTripReturn'] == 'No'){            
          $where = "iRideId = '".$db_booking[0]['iRideId']."' AND dDateOut = '".$db_booking[0]['dBookingDate']."'";
        }else{            
          $where = "iRideId = '".$db_booking[0]['iRideId']."' AND dDateOut = '".$db_booking[0]['dBookingDate']."'";
        }
        
        $sql = "SELECT iRideDateId FROM ride_dates WHERE $where";
        $db_date = $obj->MySQLSelect($sql);
        
        $driver_amount_main = $payment_details['amount_'.$i.'_'.$confirm[$i]];
        $driver_amount = $driver_amount_main - (($driver_amount_main * $payment_details['fCommissionRate'])/100);
        
        $Data_payment['iBookingId'] = $confirm[$i];
        $Data_payment['iRideDateId'] = $db_date[0]['iRideDateId'];
        $Data_payment['iRideId'] = $db_booking[0]['iRideId'];
        $Data_payment['iMemberId'] = $db_booking[0]['iDriverId'];
        $Data_payment['fAmount'] = number_format($driver_amount,2,'.','');
        $Data_payment['dPaymentDate'] = date("Y-m-d H:i:s");       
        if($db_booking[0]['eTripReturn'] == 'No'){          
          $Data_payment['eType'] = 'Out';
        }else{         
          $Data_payment['eType'] = 'Ret';
        }
        
        $trns = $kArray["paymentInfoList.paymentInfo(0).senderTransactionId"];
        
        $id = $obj->MySQLQueryPerform("driver_payment",$Data_payment,'insert'); 
        if($id){
          $sql = "UPDATE booking_new SET eDriverPaymentPaid = 'Yes', vDriverPaymentTransactionId = '".$trns."' WHERE iBookingId = '".$confirm[$i]."'";
          $db_sql=$obj->sql_query($sql);
          
          if($db_booking[0]['eTripReturn'] == 'No'){
            $sql = "UPDATE ride_dates SET eDateOutPaid = 'Yes' WHERE iRideDateId = '".$db_date[0]['iRideDateId']."'";
            $db_sql=$obj->sql_query($sql);
          }else{
            $sql = "UPDATE ride_dates SET eDateRetPaid = 'Yes' WHERE iRideDateId = '".$db_date[0]['iRideDateId']."'";
            $db_sql=$obj->sql_query($sql);
          }
          
         $mail_booking_details .= '<tr>                          
                                    <td valign="top" style="border-right:1px solid #D6D6D6;border-bottom:1px solid #D6D6D6;padding:10px;text-align:left;font-size:13px;font-family:Arial, Helvetica, sans-serif;">
                                    <strong>Booking No:</strong> '.$db_booking[0]['vBookingNo'].'
                                    <br><strong>Trip of:</strong> '.$db_booking[0]['vFromPlace'].' - '.$db_booking[0]['vToPlace'].'
                                    <br><strong>Date & Time:</strong> '.$generalobj->DateTime($db_booking[0]['dBookingDate'],10).' - '.$generalobj->DateTime($db_booking[0]['dBookingTime'],12).'
                                    </td>
                                    <td valign="top" style="border-bottom:1px solid #D6D6D6;padding:10px;text-align:right;font-size:13px;font-family:Arial, Helvetica, sans-serif;">'.$_SESSION['payment_details']['drivercurrencycode'].' '.$driver_amount_main.'</td>
                                   </tr>'; 
        }                          
      }
      
      $sql = "SELECT vFirstName, vLastName, vEmail FROM member WHERE iMemberId = '".$_SESSION['payment_details']['iMemberId']."'";
      $db_driver = $obj->MySQLSelect($sql);
      
     $mailcont .= '<table width="590" style="border:1px solid #D6D6D6;border-collapse: collapse;background-color: #FFFFFF;">
                    <tr>
                      <td width="100%" colspan="2" style="border-bottom:1px solid #D6D6D6;background:#FAF9F3;color:#000000;padding:10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">Hello, '.$db_driver[0]['vFirstName'].' '.$db_driver[0]['vLastName'].'<br>Your payment has been successfully tranfsered in your paypal account for following bookings by projectName.</td>
                    </tr>
                    <tr>                          
                      <td width="75%" style="border-right:1px solid #D6D6D6;background:#FAF9F3;color:#000000;border-bottom:1px solid #D6D6D6;padding:10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;"><strong>Booking Details</strong></td>
                      <td width="25%" style="background:#FAF9F3;color:#000000;border-bottom:1px solid #D6D6D6;padding:10px;text-align:right;font-size:15px;font-family:Arial, Helvetica, sans-serif;"><strong>Amount</strong></td>
                    </tr>
                    '.$mail_booking_details.'
                    <tr>                          
                      <td style="border-right:1px solid #D6D6D6;background:#FAF9F3;color:#000000;padding:10px;text-align:right;font-size:13px;font-family:Arial, Helvetica, sans-serif;"><strong>Sub Total</strong></td>
                      <td style="border-bottom:1px solid #D6D6D6;background:#FAF9F3;color:#000000;padding:10px;text-align:right;font-size:13px;font-family:Arial, Helvetica, sans-serif;"><strong>'.$_SESSION['payment_details']['drivercurrencycode'].' '.$_SESSION['payment_details']['sub_total'].'</strong></td>
                    </tr>
                    <tr>                          
                      <td style="border-right:1px solid #D6D6D6;background:#FAF9F3;color:#000000;padding:10px;text-align:right;font-size:13px;font-family:Arial, Helvetica, sans-serif;"><strong>Site Commission ('.$_SESSION['payment_details']['fCommissionRate'].'%)</strong></td>
                      <td style="border-bottom:1px solid #D6D6D6;background:#FAF9F3;color:#000000;padding:10px;text-align:right;font-size:13px;font-family:Arial, Helvetica, sans-serif;"><strong>'.$_SESSION['payment_details']['drivercurrencycode'].' '.$_SESSION['payment_details']['commission'].'</strong></td>
                    </tr>
                    <tr>                          
                      <td style="border-right:1px solid #D6D6D6;background:#FAF9F3;color:#000000;padding:10px;text-align:right;font-size:13px;font-family:Arial, Helvetica, sans-serif;"><strong>Grand Total</strong></td>
                      <td style="background:#FAF9F3;color:#000000;padding:10px;text-align:right;font-size:13px;font-family:Arial, Helvetica, sans-serif;"><strong>'.$_SESSION['payment_details']['drivercurrencycode'].' '.$_SESSION['payment_details']['main_total'].'</strong></td>
                    </tr>';  
      $mailcont .= '</table>';
      
      $maildata = array();
      $maildata['Email'] = $db_driver[0]['vEmail'];
      $maildata['details'] = $mailcont;
      $generalobj->send_email_user("RIDE_PAYMENT_EMAIL_DRIVER",$maildata);
      $_SESSION['payment_details'] = '';       
      $var_msg = 'Payment to driver has been send successfully.'; 
      header("Location:".$tconfig['tpanel_url']."/index.php?file=ri-member_payment&mode=view&var_msg=".$var_msg);
      exit;
    }else{
   		$var_msg = 'Problem in Driver payment. Please try again.'; 
      header("Location:".$tconfig["tpanel_url"]."/index.php?file=ri-member_payment&mode=view&var_err_msg=".$var_msg);
      exit;
    }     
}   
catch(Exception $e) {
    $var_msg = 'Problem in Driver payment. Please try again.'; 
    header("Location:".$tconfig["tpanel_url"]."/index.php?file=ri-member_payment&mode=view&var_err_msg=".$var_msg);
    exit;
  } 
exit;        
?>