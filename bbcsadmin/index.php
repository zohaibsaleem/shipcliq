<?php
	ob_start();
	define( '_TEXEC', 1 );
	define('TPATH_BASE', dirname(dirname(__FILE__)) );
	define( 'DS', DIRECTORY_SEPARATOR );
	header("Content-type: text/html; charset=utf-8");
	
	require_once ( TPATH_BASE .DS.'includes'.DS.'defines.php' );
	require_once ( TPATH_BASE .DS.'includes'.DS.'configuration.php' );
	require_once ( TPATH_LIBRARIES .DS.'general'.DS.'license'.DS.'license_admin.php' );
	$generalobj->checkAuthntication();
	if($mode == 'view'){
		$include_script = TPATH_ADMIN_MODULES.DS.$module.DS.$mode."-".$script.".php";
		$include_template = TPATH_ADMIN_TEMPLATES.DS.$module.DS.$mode."-".$script.".tpl";    
		}else if($mode =='add' || $mode =='edit'){
		$include_script = TPATH_ADMIN_MODULES.DS.$module.DS.$script.".php";
		$include_template = TPATH_ADMIN_TEMPLATES.DS.$module.DS.$script.".tpl";
		}else if($mode =='au'){
		$include_script = TPATH_ADMIN_MODULES.DS.$module.DS.$mode."-".$script.".php";
		$include_template = TPATH_ADMIN_TEMPLATES.DS.$module.DS.$mode."-".$script.".tpl";
		}else{
		$include_script = TPATH_ADMIN_MODULES.DS.$module.DS.$script.".php";
	}
	$smarty->assign("modlist",$_SESSION["sess_module_list"]);
	
	if($_REQUEST['file'] == 'au-login'){
		$mode = 'au';
		if($var_msg !=''){
			$include_script = TPATH_ADMIN_MODULES.DS.$module.DS.$mode."-".$script.".php";    
		}    
		$include_template = TPATH_ADMIN_TEMPLATES.DS.$module.DS.$mode."-".$script.".tpl";
	}
	
	##################### Logo Display Start ########################
	$sql = "SELECT vValue FROM configurations WHERE vName = 'SITE_LOGO'";
	$db_logo = $obj->MySQLSelect($sql);
	
	$logo = $db_logo[0]['vValue'];
	#print_r($logo); exit;
	if($logo == "")
	{
		$logodis = $tconfig['tsite_images']."home/".$THEME."/logo.png";
	}
	else if($logo != "")
	{
		$logodis = $tconfig['tsite_upload_site_logo'].$logo;
		#print_r($logodis); exit;
	}
	##################### Logo Dispalay End #########################
	require_once($include_script);
	$smarty->assign("logodis",$logodis);
	$smarty->assign('include_template',$include_template);
	$smarty->assign('TPATH_ADMIN_TEMPLATES',TPATH_ADMIN_TEMPLATES);
	$smarty->assign('DS',DS);
	$smarty->assign('generalobj',$generalobj);
	$smarty->assign('admin_images_url',$admin_images_url);
	$smarty->assign('admin_url',$admin_url);
	$smarty->assign('plugins_url',$plugins_url);
	$smarty->assign('admin_title',$admin_title);
	$smarty->assign('cpanel_title',$cpanel_title);
	if($script != 'staticpages'){
		$var_msg = $_REQUEST['var_msg']; 
		}else{
		$var_msg = $_REQUEST['var_succ_msg']; 
	}
	
	$smarty->assign('sess_iAdminId',$_SESSION["sess_iAdminId"]);
	$smarty->assign('sess_vFirstName',$_SESSION["sess_vFirstName"]);
	$smarty->assign('sess_vLastName',$_SESSION["sess_vLastName"]);
	$smarty->assign('var_msg',$var_msg);
	$smarty->assign('script',$script);
	
	
	if($module == "authentication")
	$smarty->debug_smarty('layout-2.tpl');
	else
	$smarty->debug_smarty('layout.tpl');
	
?>

