<?php
    
    $mode = $_REQUEST['mode'];
    $iAdminId = $_REQUEST['iAdminId'];
	
    if($mode=="add"){
		$sql="select eCreateAdmin from administrators where iAdminId='".$_SESSION['sess_iAdminId']."'";
		$db_adminaccess = $obj->MySQLSelect($sql);
		if($db_adminaccess[0]['eCreateAdmin']=='No'){
			$var_msg="You are not authorized to add new administrator.";
        	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=ad-administrator&mode=view&var_msg=$var_msg");
	        exit;
		}
	}	
	
    $sql_groups = "SELECT * FROM admin_groups  WHERE eStatus='Active'";	
    $db_groups = $obj->MySQLSelect($sql_groups);
	//echo "<pre>";
	//print_r($_POST);exit;
    if(isset($_POST["Data"]))
    {
		$db_admin[0]['vUserName'] = $_POST["Data"]['vUserName'];
		$db_admin[0]['vEmail'] = $_POST["Data"]['vEmail'];
		$db_admin[0]['vFirstName'] = $_POST["Data"]['vFirstName'];
		$db_admin[0]['vLastName'] = $_POST["Data"]['vLastName'];
		
		$db_admin[0]['vContactNo'] = $_POST["Data"]['vContactNo'];
		$db_admin[0]['dAddedDate'] = $_POST["Data"]['dAddedDate'];
		
		$db_admin[0]['eStatus'] = $_POST["Data"]['eStatus'];
	}
    if($iAdminId !=''){
        $sql = "select * from administrators where iAdminId='".$iAdminId."' ";
        $db_admin = $obj->MySQLSelect($sql);		
	}
	
    $selctdmodule=array();
    if($iAdminId !=''){
        $sql = "select * from administrators where iAdminId='".$iAdminId."' ";
        $db_admin = $obj->MySQLSelect($sql);	
        
        $sql="select iModuleTransId,iModuleId from module_access_trans where iAdminId='".$iAdminId."'";
        $db_module_trans = $obj->MySQLSelect($sql);
        
        foreach($db_module_trans as $key => $val){
			$selctdmodule[]=$val[iModuleId];
		}
	}
	
	$sql = "select iModuleId,vModuleName from module_access_master";
	$db_module = $obj->MySQLSelect($sql);
 	$strmodule='<ul style="float: left;margin: 0;padding: 0;width: 100%;">';
	for($i=0;$i<count($db_module);$i++){
		if(in_array($db_module[$i]['iModuleId'],$selctdmodule)){
			$sel='checked';
		}
		else{
			$sel='';
		} 
		$strmodule.='<li style="float:left;margin: 0 15px 15px 0; padding: 0 0 5px;width: 230px; height:50px;">'.$db_module[$i]['vModuleName'].'<br><input type="checkbox" name="modules[]" value='.$db_module[$i]['iModuleId'].' '.$sel.'></li>';
	}
	$strmodule.='</ul>';
	$smarty->assign("strmodule",$strmodule);
	
	$sql = "select * from admin_modules";
	$db_admin_modules = $obj->MySQLSelect($sql);
	
	$sql = "select * from admin_assigned_modules where iAdminId='".$iAdminId."' ";
	$db_admin_assign_modules = $obj->MySQLSelect($sql);
	
	for($i=0;$i<count($db_admin_assign_modules);$i++)
	{
		$db_admin_assign_modules_arr[] = $db_admin_assign_modules[$i]['iModuleId'];
	}
	
	for($i=0;$i<count($db_admin_modules);$i++)
	{
		$selected = " ";
		if(in_array($db_admin_modules[$i]['iModuleId'],$db_admin_assign_modules_arr))
		{
			$selected = " selected ";
		}
		else
		{
			$selected = " ";
		}
		$option_txt.="<option value='".$db_admin_modules[$i]['iModuleId']."' ".$selected."  >".$db_admin_modules[$i]['vModule']."</option>";
	}
	
    $db_admin[0]['vPassword'] = $generalobj->decrypt($db_admin[0]['vPassword']);
	
    $smarty->assign("mode",$mode);
    $smarty->assign("db_groups",$db_groups);
    $smarty->assign("db_admin",$db_admin);
    $smarty->assign("option_txt",$option_txt);	
    $smarty->assign("iAdminId",$iAdminId);
	$smarty->assign("var_msg",$var_msg);
?>