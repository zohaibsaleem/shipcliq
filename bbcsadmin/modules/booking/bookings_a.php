<?php
$action = $_REQUEST['action'];
$iBookingId = $_POST['iBookingId'];
$Data = $_POST["Data"];
//echo "<pre>";print_r($_REQUEST);exit;
//include_once(TPATH_CLASS_APP."class.order.php");
//$OrderObj = new Order();

if($action == 'pay_ref_pass'){
  $iBookingId = $_REQUEST['iBookingId_pass'];
  $Data['ePassengerRefundBy'] = $_REQUEST['ePassengerRefundBy'];
  $Data['ePassengerRefundPaid'] = 'Yes';
  $Data['dPassengerRefundDate'] = date("Y-m-d H:i:s");

  $where = " iBookingId = '".$iBookingId."'";
	$res = $obj->MySQLQueryPerform("booking_new",$Data,'update',$where);

	if($res){
    $sql = "SELECT * FROM booking_new WHERE iBookingId = '".$iBookingId."'";
    $db_passenger_payment = $obj-> MySQLSelect($sql);

    if($db_passenger_payment[0]['ePassengerRefundPaid'] == 'Yes' && $_REQUEST['emailtopass'] == 'Yes'){
      $user_cont .= 'Dear '.$db_passenger_payment[0]['vBookerFirstName'].' '.$db_passenger_payment[0]['vBookerLastName'].',';
      $user_cont .= '<br>';
      $user_cont .= 'This email is just to inform you that your booking cancellation refund payment is being processed via '.$db_passenger_payment[0]['ePassengerRefundBy'].' for Booking No. #'.$db_passenger_payment[0]['vBookingNo'].' on Bravocar.';
      $user_cont .= '<br>';
      $user_cont .= 'Below is cancelled booking details.';
      $user_cont .= '<br>';
      $user_cont .= '<br>';
      $user_cont .= 'Booking No.: #'.$db_passenger_payment[0]['vBookingNo'];
      $user_cont .= '<br>';
      $user_cont .= 'Booking Date & Time: '.$generalobj->DateTime($db_passenger_payment[0]['dBookingDate'],14).' @ '.$generalobj->DateTime($db_passenger_payment[0]['dBookingTime'],18);
      $user_cont .= '<br>';
      $user_cont .= 'Booking From: '.$db_passenger_payment[0]['vFromPlace'];
      $user_cont .= '<br>';
      $user_cont .= 'Booking To: '.$db_passenger_payment[0]['vToPlace'];
      $user_cont .= '<br>';
      $user_cont .= 'Traveler Name: '.$db_passenger_payment[0]['vDriverFirstName'].' '.$db_passenger_payment[0]['vDriverLastName'];
      $user_cont .= '<br>';
      $user_cont .= 'Sender Name: '.$db_passenger_payment[0]['vBookerFirstName'].' '.$db_passenger_payment[0]['vBookerLastName'];
      $user_cont .= '<br>';
      $user_cont .= '<br>';
      $user_cont .= 'Please refer to our <a href="'.$tconfig['tsite_url'].'refund-cancellation-policy">Refund & Cancellation Policy</a> to get more details.';

      $EmailData['email'] = $db_passenger_payment[0]['vBookerEmail'];
      $EmailData['details'] = $user_cont;
      $generalobj->send_email_user("REFUND_USER",$EmailData);
    }

    $var_msg = 'Passenger refund upadated successfully';
    header("Location: ".$tconfig["tpanel_url"]."/index.php?file=bo-bookings&mode=edit&iBookingId=$iBookingId&var_msg=$var_msg");
    exit;
  }else{
    $var_msg = 'Problem in refund upadation. Please try again.';
    header("Location: ".$tconfig["tpanel_url"]."/index.php?file=o-orders&mode=edit&iBookingId=$iBookingId&var_msg_err=$var_msg");
    exit;
  }
}

if($action == 'pay_ref_driver'){
  $iBookingId = $_REQUEST['iBookingId_driver'];
  $Data['eDriverRefundBy'] = $_REQUEST['eDriverRefundBy'];
  $Data['eDriverRefundPaid'] = 'Yes';
  $Data['dDriverRefundDate'] = date("Y-m-d H:i:s");

  $where = " iBookingId = '".$iBookingId."'";
	$res = $obj->MySQLQueryPerform("booking_new",$Data,'update',$where);

	if($res){
    $sql = "SELECT * FROM booking_new WHERE iBookingId = '".$iBookingId."'";
    $db_passenger_payment = $obj-> MySQLSelect($sql);

    if($db_passenger_payment[0]['eDriverRefundPaid'] == 'Yes' && $_REQUEST['emailtodriver'] == 'Yes'){
      $user_cont .= 'Dear '.$db_passenger_payment[0]['vDriverFirstName'].' '.$db_passenger_payment[0]['vDriverLastName'].',';
      $user_cont .= '<br>';
      $user_cont .= 'This email is just to inform you that your booking cancellation refund payment is being processed via '.$db_passenger_payment[0]['eDriverRefundBy'].' for Booking No. #'.$db_passenger_payment[0]['vBookingNo'].' on Bravocar.';
      $user_cont .= '<br>';
      $user_cont .= 'Below is cancelled booking details.';
      $user_cont .= '<br>';
      $user_cont .= '<br>';
      $user_cont .= 'Booking No.: #'.$db_passenger_payment[0]['vBookingNo'];
      $user_cont .= '<br>';
      $user_cont .= 'Booking Date & Time: '.$generalobj->DateTime($db_passenger_payment[0]['dBookingDate'],14).' @ '.$generalobj->DateTime($db_passenger_payment[0]['dBookingTime'],18);
      $user_cont .= '<br>';
      $user_cont .= 'Booking From: '.$db_passenger_payment[0]['vFromPlace'];
      $user_cont .= '<br>';
      $user_cont .= 'Booking To: '.$db_passenger_payment[0]['vToPlace'];
      $user_cont .= '<br>';
      $user_cont .= 'Traveler Name: '.$db_passenger_payment[0]['vDriverFirstName'].' '.$db_passenger_payment[0]['vDriverLastName'];
      $user_cont .= '<br>';
      $user_cont .= 'Sender Name: '.$db_passenger_payment[0]['vBookerFirstName'].' '.$db_passenger_payment[0]['vBookerLastName'];
      $user_cont .= '<br>';
      $user_cont .= '<br>';
      $user_cont .= 'Please refer to our <a href="'.$tconfig['tsite_url'].'refund-cancellation-policy">Refund & Cancellation Policy</a> to get more details.';

      $EmailData['email'] = $db_passenger_payment[0]['vDriverEmail'];
      $EmailData['details'] = $user_cont;
      $generalobj->send_email_user("REFUND_USER",$EmailData);
    }

   $var_msg = 'Traveler refund upadated successfully';
   header("Location: ".$tconfig["tpanel_url"]."/index.php?file=bo-bookings&mode=edit&iBookingId=$iBookingId&var_msg=$var_msg");
	 exit;
  }else{
   $var_msg = 'Problem in refund upadation. Please try again.';
   header("Location: ".$tconfig["tpanel_url"]."/index.php?file=o-orders&mode=edit&iBookingId=$iBookingId&var_msg_err=$var_msg");
	 exit;
  }
}

if($action == "edit"){
  $ePaymentPaid=$_REQUEST['ePaymentPaid'];
   $eDriverPaymentPaid=$_REQUEST['eDriverPaymentPaid'];
  $sql="update booking set eBookerPaymentPaid='".$ePaymentPaid."', eDriverPaymentPaid='".$eDriverPaymentPaid."' where iBookingId='".$iBookingId."'";
  $id=$obj->sql_query($sql);

/*  if($id)
   $var_msg = "Payment Status Updated Successfully.";
  else
    $var_msg = "Error in Update."; */
  header("Location: ".$tconfig["tpanel_url"]."/index.php?file=bo-bookings&mode=edit&iBookingId=$iBookingId");
	exit;
}
/*
if($action == "edit")
{
    $Data['dDate'] = date("Y-m-d H:i:s");
    $Data['iBookingId'] = $iBookingId;

    $sql = "select vStatus from order_status where iOrderStatusId = '".$Data['vStatus']."'";
    $db_status = $obj->MySQLSelect($sql);

    $sql = "update orders set iOrderStatusId='".$Data['vStatus']."' where iBookingId='".$iBookingId."'";
    $obj->sql_query($sql);

    $Data['vStatus'] =  $db_status[0]['vStatus'];
    $id = $obj->MySQLQueryPerform("order_notification",$Data,'insert');



	if($_REQUEST['sendmail'] == 'yes'){
	$order_mail_content=$OrderObj->get_ordermail_content($iBookingId, $orderAction);

	$OrderObj->sendMailToAllCust($iBookingId, $orderAction);
  }

	if($id)
	{
		$var_msg = "Order Notification Updated Successfully.";
	}
	else
	{
		$var_msg="Eror-in Update.";
	}
	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=o-orders&mode=edit&iBookingId=$iBookingId&var_msg=$var_msg");
	exit;
} */

if($action == "Deletes"){
  for($i=0;$i<count($_REQUEST['iBookingId']);$i++){
    if($_REQUEST['iBookingId'][$i] != ''){
      $sql = "DELETE FROM booking WHERE iBookingId = '".$_REQUEST['iBookingId'][$i]."'";
      $db_sql=$obj->sql_query($sql);

  /*    $sql = "DELETE FROM order_details WHERE iBookingId = '".$_REQUEST['iBookingId'][$i]."'";
      $db_sql=$obj->sql_query($sql);

      $sql = "DELETE FROM order_notification WHERE iBookingId = '".$_REQUEST['iBookingId'][$i]."'";
      $db_sql=$obj->sql_query($sql);  */
    }
  }
  $var_msg = "Booking deleted Successfully.";
  header("Location: ".$tconfig["tpanel_url"]."/index.php?file=bo-bookings&mode=view&var_msg=$var_msg");
	exit;
}

if($action == "Delete"){

  if($_REQUEST['iBookingId'] != ''){
  //  echo "hi";exit;
   $sql = "DELETE FROM booking_new WHERE iBookingId = '".$_REQUEST['iBookingId']."'";
    $db_sql=$obj->sql_query($sql);
 /*
    $sql = "DELETE FROM order_details WHERE iBookingId = '".$_REQUEST['iBookingId']."'";
    $db_sql=$obj->sql_query($sql);

    $sql = "DELETE FROM order_notification WHERE iBookingId = '".$_REQUEST['iBookingId']."'";
    $db_sql=$obj->sql_query($sql); */
  }
  $var_msg = "Booking deleted Successfully.";
  header("Location: ".$tconfig["tpanel_url"]."/index.php?file=bo-bookings&mode=view&var_msg=$var_msg");
	exit;
}
?>
