<?php  
/*
*******************************************************************
THIS IS STRICTLY EXAMPLE SOURCE CODE. IT IS ONLY MEANT TO
QUICKLY DEMONSTRATE THE CONCEPT AND THE USAGE OF THE ADAPTIVE
PAYMENTS API. PLEASE NOTE THAT THIS IS *NOT* PRODUCTION-QUALITY
CODE AND SHOULD NOT BE USED AS SUCH.

THIS EXAMPLE CODE IS PROVIDED TO YOU ONLY ON AN "AS IS"
BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER
EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY WARRANTIES
OR CONDITIONS OF TITLE, NON-INFRINGEMENT, MERCHANTABILITY OR
FITNESS FOR A PARTICULAR PURPOSE. PAYPAL MAKES NO WARRANTY THAT
THE SOFTWARE OR DOCUMENTATION WILL BE ERROR-FREE. IN NO EVENT
SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
OF SUCH DAMAGE.


INSTRUCTIONS
1) Ensure that SSL and fopen() are enabled in the php.ini file
2) Written and Tested with PHP 5.3.0


IMPORTANT:
When you integrate this code look for TODO as an indication that 
you may need to provide a value or take action before executing this code.
*******************************************************************

*/ 
//turn php errors on
ini_set("track_errors", true);    
$sender = $PAYPAL_SENDER; 
$receiver = 'buyer_1314995019_per@gmail.com';

$sql = "SELECT vPaymentEmail FROM member WHERE iMemberId = '".$_REQUEST['iMemberId']."'";
$db_payment_mail = $obj->MySQLSelect($sql);
//$receiver = $db_payment_mail[0]['vPaymentEmail'];

$_SESSION['iRideId_payment'] = $_REQUEST['iRideId_payment'];
$_SESSION['date_payment'] = $_REQUEST['date_payment'];
$_SESSION['eTripReturn_payment'] = $_REQUEST['eTripReturn']; 
$_SESSION['fCommissionRate_payment'] = $_REQUEST['fCommissionRate'];
$_SESSION['iRideDateId_payment'] = $_REQUEST['iRideDateId'];
$_SESSION['iMemberId_payment'] = $_REQUEST['iMemberId'];
$_SESSION['amount_payment'] = $_REQUEST['main_total'];
$_SESSION['confirm_payment'] = $_REQUEST['confirm'];
$cnf = $_REQUEST['confirm'];


$stack = array();
for($i=0;$i<count($cnf);$i++){
  $newarr = array();
  $newarr['booking'] = $cnf[$i];
  $newarr['amt'] = $_REQUEST['amount_'.$i.'_'.$cnf[$i]];
  array_push($stack, $newarr);
}
$_SESSION['confirm_payment_main'] = $stack;  

$amount = $_REQUEST['main_total'];
$currency = $_REQUEST['drivercurrencycode'];
$returnURL = $tconfig['tpanel_url']."index.php?file=ri-ridepayment";
$cancleURL = $tconfig['tpanel_url']."index.php?file=ri-ridepayment".$_REQUEST['iMessageId'];

//set PayPal Endpoint to sandbox
$url = trim("https://svcs.paypal.com/AdaptivePayments/Pay");
//echo $url;exit;
/*
*******************************************************************
PayPal API Credentials
Replace <API_USERNAME> with your API Username
Replace <API_PASSWORD> with your API Password
Replace <API_SIGNATURE> with your Signature
*******************************************************************
*/

//PayPal API Credentials
$API_UserName = $API_USERNAME; //TODO
$API_Password = $API_PASSWORD; //TODO    
$API_Signature = $API_SIGNATURE; //TODO  
	
//Default App ID for Sandbox	
$API_AppID = "APP-80W284485P519543T";

$API_RequestFormat = "NV";
$API_ResponseFormat = "NV"; 

//Create request payload with minimum required parameters
$bodyparams = array (	"requestEnvelope.errorLanguage" => "en_US",
											"actionType" => "PAY",
											"currencyCode" => $currency,										
											"cancelUrl" => $cancleURL,
											"returnUrl" => $returnURL,
											"senderEmail" => $sender,
											"receiverList.receiver(0).email" => $receiver, //TODO
											"receiverList.receiver(0).amount" => $amount //TODO
											);
											
// convert payload array into url encoded query string
$body_data = http_build_query($bodyparams, "", chr(38));  


try
{

    //create request and add headers
    $params = array("http" => array( 
    																 "method" => "POST",
                  									 "content" => $body_data,
                  									 "header" =>  "X-PAYPAL-SECURITY-USERID: " . $API_UserName . "\r\n" .
                               										"X-PAYPAL-SECURITY-SIGNATURE: " . $API_Signature . "\r\n" .
                 							 										"X-PAYPAL-SECURITY-PASSWORD: " . $API_Password . "\r\n" .
                   						 										"X-PAYPAL-APPLICATION-ID: " . $API_AppID . "\r\n" .
                   						 										"X-PAYPAL-REQUEST-DATA-FORMAT: " . $API_RequestFormat . "\r\n" .
                  						 										"X-PAYPAL-RESPONSE-DATA-FORMAT: " . $API_ResponseFormat . "\r\n" 
                  																));
    //echo "<pre>";print_r($params);exit;

    //create stream context
     $ctx = stream_context_create($params);
     

    //open the stream and send request
     $fp = @fopen($url, "r", false, $ctx);

    //get response
  	 $response = stream_get_contents($fp);
     
  	//check to see if stream is open
     if ($response === false) {
        throw new Exception("php error message = " . "$php_errormsg");
     }
           
    //close the stream
     fclose($fp);

    //parse the ap key from the response
    $keyArray = explode("&", $response);
    //echo "<pre>";print_r($keyArray);exit;    
    foreach ($keyArray as $rVal){
    	list($qKey, $qVal) = explode ("=", $rVal);
			$kArray[$qKey] = $qVal;
    }
      
    //set url to approve the transaction
    $payPalURL = "https://www.paypal.com/webscr?cmd=_ap-payment&paykey=" . $kArray["payKey"];
    /*
    //print the url to screen for testing purposes
    If ( $kArray["responseEnvelope.ack"] == "Success") {
    	echo '<p><a href="' . $payPalURL . '" target="_blank">' . $payPalURL . '</a></p>';
     }
    else {
    	echo 'ERROR Code: ' .  $kArray["error(0).errorId"] . " <br/>";
      echo 'ERROR Message: ' .  urldecode($kArray["error(0).message"]) . " <br/>";
    }
    */           
   	//optional code to redirect to PP URL to approve payment  
    if($kArray["responseEnvelope.ack"] == "Success"){        
      //header("Location:".$payPalURL);
      //exit;
      
      $confirm_payment_main = $_SESSION['confirm_payment_main'];
      $comm = $_SESSION['fCommissionRate_payment']; 
      for($i=0;$i<count($confirm_payment_main);$i++){        
        $Data_payment = array();
        $amtpaid = $confirm_payment_main[$i]['amt'] - (($confirm_payment_main[$i]['amt'] * $comm)/100);
               
        $Data_payment['iBookingId'] = $confirm_payment_main[$i]['booking'];
        $Data_payment['iRideDateId'] = $_SESSION['iRideDateId_payment'];
        $Data_payment['iRideId'] = $_SESSION['iRideId_payment'];
        $Data_payment['iMemberId'] = $_SESSION['iMemberId_payment'];
        $Data_payment['fAmount'] = $amtpaid;
        $Data_payment['dPaymentDate'] = date("Y-m-d H:i:s");       
        if($_SESSION['eTripReturn_payment'] == 'No'){          
          $Data_payment['eType'] = 'Out';
        }else{         
          $Data_payment['eType'] = 'Ret';
        }
        $id = $obj->MySQLQueryPerform("driver_payment",$Data_payment,'insert'); 
        if($id){
          $sql = "UPDATE booking_new SET eDriverPaymentPaid = 'Yes' WHERE iBookingId = '".$confirm_payment_main[$i]['booking']."'";
          $db_sql=$obj->sql_query($sql);
        } 
      } 
      
      if($_SESSION['eTripReturn_payment'] == 'No'){
       $sql = "UPDATE ride_dates SET eDateOutPaid = 'Yes' WHERE iRideDateId = '".$_SESSION['iRideDateId_payment']."'";
       $db_sql=$obj->sql_query($sql);
      }else{
       $sql = "UPDATE ride_dates SET eDateRetPaid = 'Yes' WHERE iRideDateId = '".$_SESSION['iRideDateId_payment']."'";
       $db_sql=$obj->sql_query($sql);
      }    
      
      $iRideId = $_SESSION['iRideId_payment'];
      $date = $_SESSION['date_payment'];
      
      $_SESSION['iRideId_payment'] = '';
      $_SESSION['date_payment'] = '';
      $_SESSION['eTripReturn_payment'] = '';
      $_SESSION['fCommissionRate_payment'] = '';
      $_SESSION['iRideDateId_payment'] = '';
      $_SESSION['iMemberId_payment'] = '';
      $_SESSION['amount_payment'] = '';
      $_SESSION['confirm_payment'] = '';
      
      $var_msg = 'Traveler payment has been successful for Date '.$date;  
      header("Location:".$tconfig["tpanel_url"]."/index.php?file=ri-rideslist&mode=edit&var_msg=".$var_msg."&iRideId=".$iRideId);
      exit;   
    }else{
   		$var_msg = 'Problem in Traveler payment for Date '.$_SESSION['date_payment'].'. Please try again.'; 
      header("Location:".$tconfig["tpanel_url"]."/index.php?file=ri-rideslist&mode=edit&var_err_msg=".$var_msg."&iRideId=".$_SESSION['iRideId_payment']);
      exit;
    }     
}   
catch(Exception $e) {
    $var_msg = 'Problem in Traveler payment for Date '.$_SESSION['date_payment'].'. Please try again.'; 
    header("Location:".$tconfig["tpanel_url"]."/index.php?file=ri-rideslist&mode=edit&var_err_msg=".$var_msg."&iRideId=".$_SESSION['iRideId_payment']);
    exit;
  } 
exit;        
?>