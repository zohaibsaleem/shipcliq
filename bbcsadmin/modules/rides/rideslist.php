<?php 
  $mode = $_REQUEST['mode'];
  $iRideId = $_REQUEST['iRideId'];
  
  $sql = "SELECT rides_new.tDetails, rides_new.fDocumentPrice, rides_new.vDocumentWeight, rides_new.fBoxPrice, rides_new.vBoxWeight, rides_new.fLuggagePrice, rides_new.vLuggageWeight, rides_new.eLeaveTime, rides_new.eDocument, rides_new.eBox, rides_new.eLuggage, rides_new.iRideId, rides_new.iMemberId, rides_new.vMainDeparture, rides_new.vMainArrival, rides_new.eRideType, rides_new.dAddedDate, rides_new.iSeats, rides_new.eStatus, rides_new.vBookerCurrencyCode, member.vFirstName, member.vLastName  
          FROM rides_new 
          LEFT JOIN member ON member.iMemberId = rides_new.iMemberId   
          WHERE iRideId = '".$iRideId."'";
  $db_records_all = $obj->MySQLSelect($sql);
  
  ########### Document Box Luggage start ##########
  if($db_records_all[0]['eDocument'] == "Yes")
  {
		$doc = "Yes";
		$docprice = $db_records_all[0]['fDocumentPrice'];
		$docweight = $db_records_all[0]['vDocumentWeight'];
  }
  else if($db_records_all[0]['eDocument'] == "No")
  {
	   $doc = "No";
  }
  
  if($db_records_all[0]['eBox'] == "Yes")
  {
		$box = "Yes";
		$boxprice = $db_records_all[0]['fBoxPrice'] ;
		$boxweight = $db_records_all[0]['vBoxWeight'];
  }
  else if($db_records_all[0]['eBox'] == "No")
  {
	   $box = "No";
  }
  
  if($db_records_all[0]['eLuggage'] == "Yes")
  {
		$lug = "Yes";
		$lugprice = $db_records_all[0]['fLuggagePrice'];
		$lugweight = $db_records_all[0]['vLuggageWeight'];
  }
  else if($db_records_all[0]['eLuggage'] == "No")
  {
	   $lug = "No";
  }
  ########### Document Box Luggage end ##########
  
  $sql = "SELECT SUM(fPrice) as price FROM ride_points_new WHERE iRideId = '".$db_records_all[0]['iRideId']."' AND eReverse = 'No'";
  $db_points = $obj->MySQLSelect($sql);
  
  $db_records_all[0]['price'] = $db_points[0]['price']; 
  
  $sql = "SELECT iBookingId FROM booking_new WHERE iRideId = '".$db_records_all[0]['iRideId']."' AND dBookingDate = '".date("Y-m-d")."' AND eDriverPaymentPaid = 'No'";
  $db_todaybooking = $obj->MySQLSelect($sql);
  
  if(count($db_todaybooking) > 0){
    $db_records_all[0]['todaybooking'] = 'Today';      
  }else{
    $db_records_all[0]['paymentdue'] = 'No';
  }
  
  $sql = "SELECT iBookingId FROM booking_new WHERE iRideId = '".$db_records_all[0]['iRideId']."' AND dBookingDate < '".date("Y-m-d")."' AND eDriverPaymentPaid = 'No'";
  $db_pastbooking = $obj->MySQLSelect($sql);
  
  if(count($db_pastbooking) > 0){ 
    $db_records_all[0]['pastbooking'] = 'Past';        
  }else{
    $db_records_all[0]['paymentdue'] = 'No';
  }  
  
  if($db_records_all[0]['pastbooking'] == 'Past' && $db_records_all[0]['todaybooking'] == 'Today'){
    $db_records_all[0]['color'] = '#FFF380'; 
    $db_records_all[0]['paymentdue'] = 'Yes';
  }else if($db_records_all[0]['pastbooking'] == 'Past' && $db_records_all[0]['todaybooking'] != 'Today'){
    $db_records_all[0]['color'] = '#FFC9C9';
    $db_records_all[0]['paymentdue'] = 'Yes';
  }else if($db_records_all[0]['pastbooking'] != 'Past' && $db_records_all[0]['todaybooking'] == 'Today'){
    $db_records_all[0]['color'] = '#CAFFCA';
    $db_records_all[0]['paymentdue'] = 'Yes';
  }else{
    $db_records_all[0]['color'] = '#FFF';
    $db_records_all[0]['paymentdue'] = 'No';
  }
  
  $sql = "SELECT ride_dates.iRideDateId, ride_dates.iRideId, ride_dates.dDateOut, ride_dates.dDateRet, ride_dates.eDateOutPaid, ride_dates.eDateRetPaid      
          FROM ride_dates     
          WHERE ride_dates.iRideId = '".$db_records_all[0]['iRideId']."' ORDER BY ride_dates.iRideDateId ASC";
  $db_dates = $obj->MySQLSelect($sql);
  
  for($i=0;$i<count($db_dates);$i++){        
    $sql = "SELECT iBookingId, dBookingDate, eDriverPaymentPaid FROM booking_new WHERE iRideId = '".$iRideId."' AND dBookingDate = '".$db_dates[$i]['dDateOut']."' AND eTripReturn = 'No'";
    $db_bookings_out = $obj->MySQLSelect($sql);
    
    $sql = "SELECT iBookingId, dBookingDate, eDriverPaymentPaid FROM booking_new WHERE iRideId = '".$iRideId."' AND dBookingDate = '".$db_dates[$i]['dDateRet']."' AND eTripReturn = 'Yes'";
    $db_bookings_ret = $obj->MySQLSelect($sql);
    
    if(count($db_bookings_out)>0){
      $db_dates[$i]['totbookingsout'] = count($db_bookings_out); 
      if(strtotime($db_dates[$i]['dDateOut']) == strtotime(date("Y-m-d"))){
        $db_dates[$i]['colorout'] = '#CAFFCA'; 
      }
    }else{
      $db_dates[$i]['totbookingsout'] = 'No Bookings'; 
      $db_dates[$i]['colorout'] = '#FFF'; 
    }
    
    if(count($db_bookings_ret)>0){
      $db_dates[$i]['totbookingsret'] = count($db_bookings_ret);
      if(strtotime($db_dates[$i]['dDateRet']) == strtotime(date("Y-m-d"))){
        $db_dates[$i]['colorret'] = '#CAFFCA'; 
      }
    }else{
      $db_dates[$i]['totbookingsret'] = 'No Bookings'; 
      $db_dates[$i]['colorret'] = '#FFF'; 
    }
  }
  
  //echo "<pre>";
  //print_r($db_dates); exit;
  
  $smarty->assign("mode",$mode);
  $smarty->assign("db_records_all",$db_records_all);
  $smarty->assign("db_dates",$db_dates);
  $smarty->assign("doc",$doc);
  $smarty->assign("docprice",$docprice);
  $smarty->assign("docweight",$docweight);
  $smarty->assign("box",$box);
  $smarty->assign("boxprice",$boxprice);
  $smarty->assign("boxweight",$boxweight);
  $smarty->assign("lug",$lug);
  $smarty->assign("lugprice",$lugprice);
  $smarty->assign("lugweight",$lugweight);
  $smarty->assign("iRideId",$iRideId);
  $smarty->assign("var_err_msg",$_REQUEST['var_err_msg']);
?>