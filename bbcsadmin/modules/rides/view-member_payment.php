<?
$ssql = ""; 
$alp = $_REQUEST['alp'];
$option = $_REQUEST['option'];
$keyword = $_REQUEST['keyword'];

if($_REQUEST['sortby'] == ''){
  $sortby = 3;
}else{
  $sortby = $_REQUEST['sortby'];
}
if($_REQUEST['order'] == ''){
  $order = 0;
}else{
  $order = $_REQUEST['order'];
}

if($option != '' && $keyword != ''){
    $ssql.= " AND ".stripslashes($option)." LIKE '%".stripslashes($keyword)."%'";
}

if($alp != ''){
    $ssql.= " AND concat(vFirstName,' ',vLastName) LIKE '".stripslashes($alp)."%'";
}
if($sortby == 1){

  if($order == 0)
  $ord.= " order by concat(vFirstName,' ',vLastName) ASC";
  else
  $ord.= " order by concat(vFirstName,' ',vLastName) DESC";
}
// sortby 2 for vGroup Name

if($sortby == 2){ 
  if($order == 0)
  $ord.= " order by vEmail ASC";
  else
  $ord.= " order by vEmail DESC";
}
// sortby 3 for dDate On

if($sortby == 3){ 
  if($order == 1)
  $ord.= " order by dAddedDate ASC";
  else
  $ord.= " order by dAddedDate DESC";
}
if($sortby == 4){ 
  if($order == 0)
  $ord.= " order by eStatus ASC";
  else
  $ord.= " order by eStatus DESC";
}
$sql = "SELECT DISTINCT(booking_new.iDriverId), member.vFirstName, member.vLastName, member.vPhone, member.vEmail  
        FROM booking_new 
        JOIN member ON member.iMemberId = booking_new.iDriverId 
        WHERE booking_new.eDriverPaymentPaid = 'NO' 
        AND dBookingDate <= '".date("Y-m-d")."'";
$db_records = $obj->MySQLSelect($sql);  

$num_totrec = count($db_records);

include(TPATH_CLASS_GEN."admin.paging.inc.php");
 
$sql = "SELECT DISTINCT(booking_new.iDriverId), member.vFirstName, member.vLastName, member.vPhone, member.vEmail  
        FROM booking_new 
        JOIN member ON member.iMemberId = booking_new.iDriverId 
        WHERE booking_new.eDriverPaymentPaid = 'NO'"; 
        #AND dBookingDate <= '".date("Y-m-d")."'";
$db_booked_members = $obj->MySQLSelect($sql);

#echo "<pre>";
#print_r($db_booked_members); exit;

for($i=0;$i<count($db_booked_members);$i++){
  $sql = "SELECT iBookingId, vBookingNo, iRideId, iRidePointId, dBookingDate, dBookingTime, vBookerFirstName, vBookerLastName, iBookerId, fCommission, fVat, fRidePrice, fAmount,  eStatus, eCancelBy, vBookerCurrencyCode FROM booking_new WHERE iDriverId = '".$db_booked_members[$i]['iDriverId']."' AND eDriverPaymentPaid = 'No'";
  $db_booking_amount = $obj->MySQLSelect($sql);
  
  $tot_no_payment = 0;
  $cont = '';
  $cont .= '<table width="100%" style="border:1px solid #cccccc;">                  
        			<tr>
        				<td width="15%" style="border-bottom:1px solid #cccccc;background:#F2F2F2">Booking No.#</td>
        				<td width="15%" style="border-bottom:1px solid #cccccc;background:#F2F2F2">Booker</td>
        				<td width="10%" style="border-bottom:1px solid #cccccc;background:#F2F2F2">Date</td>
        				<td width="10%" style="text-align:center;border-bottom:1px solid #cccccc;background:#F2F2F2">Time</td>
        				<td width="10%" style="text-align:right;border-bottom:1px solid #cccccc;background:#F2F2F2">Driver Pay Amount</td>
        				<td width="50%" style="border-bottom:1px solid #cccccc;background:#F2F2F2">&nbsp;</td>
        			</tr>';
  for($j=0;$j<count($db_booking_amount);$j++){
    $sql = "SELECT fRatio_".$db_booking_amount[$j]['vBookerCurrencyCode']." as code, vBookerCurrencyCode as driver_curr FROM rides_new WHERE iRideId = '".$db_booking_amount[$j]['iRideId']."'";
    $db_booking_amount_ratio = $obj->MySQLSelect($sql);   
    
    /*$sql="select SUM(fPrice) as ride_price from ride_points_new where iRideId='".$db_booking_amount[$j]['iRideId']."' AND iRidePointId IN(".$db_booking_amount[$j]['iRidePointId'].")";
    $db_tot_price = $obj->MySQLSelect($sql);
    $driver_amount = number_format($db_tot_price[0]['ride_price'] / $db_booking_amount_ratio[0]['code'],2,'.','');*/
    $driver_amount = number_format($db_booking_amount[$j]['fRidePrice'] / $db_booking_amount_ratio[0]['code'],2,'.','');
    if($db_booking_amount[$j]['eStatus'] == "Cencelled"){
      if($db_booking_amount[$j]['eCancelBy'] == "Driver"){
        $db_booking_amount[$j]['driver_amount'] = "0.00";
      }else{
        $db_booking_amount[$j]['driver_amount'] = number_format($driver_amount/4,2,'.','');
      }
    }else{                               
        $db_booking_amount[$j]['driver_amount'] = $driver_amount;
    }
      
    
    $cont .= '<tr>
        				<td>'.$db_booking_amount[$j]['vBookingNo'].'</td>
        				<td>'.$db_booking_amount[$j]['vBookerFirstName'].' '.$db_booking_amount[$j]['vBookerLastName'].'</td>
        				<td>'.$generalobj->DateTime($db_booking_amount[$j]['dBookingDate'],10).'</td>
        				<td style="text-align:center;">'.$generalobj->DateTime($db_booking_amount[$j]['dBookingTime'],12).'</td>
        				<td style="text-align:right;">'.$db_booking_amount_ratio[0]['driver_curr'].' '.$db_booking_amount[$j]['driver_amount'].'</td>
        				<td>&nbsp;</td>
        			</tr>';    
    $tot_no_payment++; 
  }      
  $cont .= '</table>';
  $db_booked_members[$i]['cont'] = $cont; 
  $db_booked_members[$i]['tot_no_payment'] = $tot_no_payment;   
} 

if(!isset($start))
	$start = 1;
	$num_limit = ($start-1)*$rec_limit;
	$startrec = $num_limit;
	
	$lastrec = $startrec + $rec_limit;
	$startrec = $startrec + 1;
	if($lastrec > $num_totrec)
		$lastrec = $num_totrec;
		if($num_totrec > 0 )
		{
			$recmsg = "Showing ".$startrec." - ".$lastrec." Records Of ".$num_totrec;
		}
		else
		{
			$recmsg="No Records Found.";
		}

if(!count($db_booked_members)>0 && $keyword != ""){
	$var_msg_new = "Your search for <font color=#2e71b3>$keyword</font> has found <font color=#2e71b3>0</font> matches:";
}else if($keyword != ""){
	$var_msg_new = "Your search for <font color=#2e71b3>$keyword</font> has found <font color=#2e71b3>$num_totrec</font> matches:";
}else if($alp !=''){
    $var_msg_new = "Your search for <font color=#2e71b3>$alp</font> has found <font color=#2e71b3>$num_totrec</font> matches:";
}

$sql_alp = "SELECT member.vFirstName   
            FROM booking_new 
            LEFT JOIN member ON member.iMemberId = booking_new.iDriverId 
            WHERE booking_new.eDriverPaymentPaid = 'NO' 
            AND dBookingDate <= '".date("Y-m-d")."'";
$db_alp = $obj->MySQLSelect($sql_alp);

for($i=0;$i<count($db_alp);$i++){
    $db_alp[$i] = strtoupper(substr($db_alp[$i]['vFirstName'], 0,1));
}

$alpha_rs =implode(",",$db_alp);
$AlphaChar = @explode(',',$alpha_rs);
$AlphaBox.='<ul class="pagination">';
for($i=65;$i<=90;$i++){
	
	if(!@in_array(chr($i),$AlphaChar)){
		$AlphaBox.= '<li ><a href="#" onclick="return false;"  id="alch_'.$i.'">'.chr($i).'</a></li>';
	}else{
		$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
	}
}
$AlphaBox.='</ul>';


$smarty->assign("db_records_all",$db_records_all);
$smarty->assign("AlphaBox",$AlphaBox);
$smarty->assign("recmsg",$recmsg);
$smarty->assign("var_err_msg",$_REQUEST['var_err_msg']);  
$smarty->assign("var_msg",$_REQUEST['var_msg']);   
$smarty->assign("keyword",$keyword);
$smarty->assign("option",$option);
$smarty->assign("page_link",$page_link);
$smarty->assign("var_msg_new",$var_msg_new);
$smarty->assign("sortby",$sortby);
$smarty->assign("order",$order);
$smarty->assign("db_booked_members",$db_booked_members);    
?>