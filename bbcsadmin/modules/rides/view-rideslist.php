<?
  $ssql = ""; 
  $alp = $_REQUEST['alp'];
  $option = $_REQUEST['option'];
  $keyword = $_REQUEST['keyword'];
  $status = $_REQUEST['status'];
  $paymenttype = $_REQUEST['paymenttype'];
  
  if($paymenttype == ''){
    $paymenttype = 'today';    
  }
  
  if($option != '' && $keyword != ''){
    if(stripslashes($option) == 'Title'){       
      $ssql.= " AND concat(rides_new.vMainDeparture,' - ',rides_new.vMainArrival) LIKE '%".stripslashes($keyword)."%'";
    }else if(stripslashes($option) == 'Member'){
      $ssql.= " AND concat(member.vFirstName,' ',member.vLastName) LIKE '%".stripslashes($keyword)."%'";
    }else if(stripslashes($option) == 'Status'){
      $ssql.= " AND rides_new.eStatus = '".stripslashes($keyword)."'";
    }
  }
  
  if($_REQUEST['status'] != ''){
    $ssql .= " AND rides_new.eStatus = '".$_REQUEST['status']."'";
  }
  
  $sql = "SELECT iRideId, iMemberId, vMainDeparture, vMainArrival, eRideType, dAddedDate, iSeats, eStatus, vBookerCurrencyCode 
          FROM rides_new 
          WHERE 1=2";
  $db_records = $obj->MySQLSelect($sql);
  
  $num_totrec = count($db_records);
  
  include(TPATH_CLASS_GEN."admin.paging.inc.php");
  
  /*$sql = "SELECT rides_new.iRideId, rides_new.iMemberId, rides_new.vMainDeparture, rides_new.vMainArrival, rides_new.eRideType, rides_new.dAddedDate, rides_new.iSeats, rides_new.eStatus, rides_new.vBookerCurrencyCode, member.vFirstName, member.vLastName  
          FROM rides_new 
          LEFT JOIN member ON member.iMemberId = rides_new.iMemberId   
          WHERE 1=1 ORDER BY dAddedDate DESC";
  $db_records_all = $obj->MySQLSelect($sql);*/
  
  if($paymenttype == 'today'){
      $sql = "SELECT iBookingId, iRideId, eDriverPaymentPaid FROM booking_new WHERE dBookingDate = '".date("Y-m-d")."'";
      $db_todaybooking = $obj->MySQLSelect($sql); 
      
      if(count($db_todaybooking) > 0){
        $tArray = array();
        foreach ($db_todaybooking as $k => $v) {
          $tArray[$k] = $v['iRideId'];
        }         
       
        $rb = implode(',', $tArray);
        
        $sql = "SELECT rides_new.iRideId, rides_new.iMemberId, rides_new.vMainDeparture, rides_new.vMainArrival, rides_new.eRideType, rides_new.dAddedDate, rides_new.iSeats, rides_new.eStatus, rides_new.vBookerCurrencyCode, member.vFirstName, member.vLastName  
                FROM rides_new 
                LEFT JOIN member ON member.iMemberId = rides_new.iMemberId   
                WHERE rides_new.iRideId IN(".$rb.") 
                ".$ssql." 
                ORDER BY rides_new.iRideId DESC";
        $db_records_all = $obj->MySQLSelect($sql);
        
        for($i=0;$i<count($db_records_all);$i++){
          $sql = "SELECT SUM(fPrice) as price FROM ride_points_new WHERE iRideId = '".$db_records_all[$i]['iRideId']."' AND eReverse = 'No'";
          $db_points = $obj->MySQLSelect($sql);
          
          $db_records_all[$i]['price'] = $db_points[0]['price'];
          
          $sql = "SELECT iBookingId,vBookingNo, eDriverPaymentPaid FROM booking_new WHERE iRideId = '".$db_records_all[$i]['iRideId']."' AND dBookingDate = '".date("Y-m-d")."' AND eDriverPaymentPaid = 'No'";
          $db_todaybooking = $obj->MySQLSelect($sql);
          
          $db_records_all[$i]['todaybooking'] = 'Today';
          //$db_records_all[$i]['color'] = '#CAFFCA';   
          
          if(count($db_todaybooking) > 0){
            $db_records_all[$i]['paymentdue'] = 'Yes';  
          }else{
            $db_records_all[$i]['paymentdue'] = 'No'; 
          }        
        }     
      }else{
        $db_records_all = array();
      }
  }else if($paymenttype == 'past'){
      $sql = "SELECT iBookingId, iRideId,vBookingNo, eDriverPaymentPaid FROM booking_new WHERE dBookingDate < '".date("Y-m-d")."' AND eDriverPaymentPaid = 'No'";
      $db_todaybooking = $obj->MySQLSelect($sql);  
    
      if(count($db_todaybooking) > 0){
        $tArray = array();
        foreach ($db_todaybooking as $k => $v) {
          $tArray[$k] = $v['iRideId'];
        }         
       
        $rb = implode(',', $tArray);
        
        $sql = "SELECT rides_new.iRideId, rides_new.iMemberId, rides_new.vMainDeparture, rides_new.vMainArrival, rides_new.eRideType, rides_new.dAddedDate, rides_new.iSeats, rides_new.eStatus, rides_new.vBookerCurrencyCode, member.vFirstName, member.vLastName  
                FROM rides_new 
                LEFT JOIN member ON member.iMemberId = rides_new.iMemberId   
                WHERE rides_new.iRideId IN(".$rb.") 
                ORDER BY rides_new.iRideId DESC";
        $db_records_all = $obj->MySQLSelect($sql);
        
        for($i=0;$i<count($db_records_all);$i++){
          $sql = "SELECT SUM(fPrice) as price FROM ride_points_new WHERE iRideId = '".$db_records_all[$i]['iRideId']."' AND eReverse = 'No'";
          $db_points = $obj->MySQLSelect($sql);
          
          $db_records_all[$i]['price'] = $db_points[0]['price'];
          
          $sql = "SELECT iBookingId, vBookingNo, eDriverPaymentPaid FROM booking_new WHERE iRideId = '".$db_records_all[$i]['iRideId']."' AND dBookingDate < '".date("Y-m-d")."' AND eDriverPaymentPaid = 'No'";
          $db_todaybooking = $obj->MySQLSelect($sql);
          
          $db_records_all[$i]['todaybooking'] = 'Past';
          //$db_records_all[$i]['color'] = '#FFC9C9';   
          
          if(count($db_todaybooking) > 0){
            $db_records_all[$i]['paymentdue'] = 'Yes';  
          }else{
            $db_records_all[$i]['paymentdue'] = 'No'; 
          }        
        }
      }else{
        $db_records_all = array();
      }
  }else{
     $sql = "SELECT rides_new.iRideId, rides_new.iMemberId, rides_new.vMainDeparture, rides_new.vMainArrival, rides_new.eRideType, rides_new.dAddedDate, rides_new.iSeats, rides_new.eStatus, rides_new.vBookerCurrencyCode, member.vFirstName, member.vLastName  
              FROM rides_new 
              LEFT JOIN member ON member.iMemberId = rides_new.iMemberId   
              WHERE rides_new.eStatus != 'Inactive' AND rides_new.eStatus != 'Deleted' ".$ssql."  ORDER BY dAddedDate DESC";
     $db_records = $obj->MySQLSelect($sql);
     
     $num_totrec = count($db_records);
  
     include(TPATH_CLASS_GEN."admin.paging.inc.php");
  
     $sql = "SELECT rides_new.iRideId, rides_new.iMemberId, rides_new.vMainDeparture, rides_new.vMainArrival, rides_new.eRideType, rides_new.dAddedDate, rides_new.iSeats, rides_new.eStatus, rides_new.vBookerCurrencyCode, member.vFirstName, member.vLastName  
              FROM rides_new 
              LEFT JOIN member ON member.iMemberId = rides_new.iMemberId   
              WHERE rides_new.eStatus != 'Inactive' AND rides_new.eStatus != 'Deleted' ".$ssql." ORDER BY dAddedDate DESC". $var_limit;
     $db_records_all = $obj->MySQLSelect($sql);
     
     for($i=0;$i<count($db_records_all);$i++){
        $sql = "SELECT SUM(fPrice) as price FROM ride_points_new WHERE iRideId = '".$db_records_all[$i]['iRideId']."' AND eReverse = 'No'";
        $db_points = $obj->MySQLSelect($sql);
        
        $db_records_all[$i]['price'] = $db_points[0]['price']; 
        
        $sql = "SELECT iBookingId,vBookingNo FROM booking_new WHERE iRideId = '".$db_records_all[$i]['iRideId']."' AND dBookingDate = '".date("Y-m-d")."' AND eDriverPaymentPaid = 'No'";
        $db_todaybooking = $obj->MySQLSelect($sql);
        
        if(count($db_todaybooking) > 0){
          $db_records_all[$i]['todaybooking'] = 'Today';      
        }else{
          $db_records_all[$i]['paymentdue'] = 'No';
        }
        
        $sql = "SELECT iBookingId FROM booking_new WHERE iRideId = '".$db_records_all[$i]['iRideId']."' AND dBookingDate < '".date("Y-m-d")."' AND eDriverPaymentPaid = 'No'";
        $db_pastbooking = $obj->MySQLSelect($sql);
        
        if(count($db_pastbooking) > 0){ 
          $db_records_all[$i]['pastbooking'] = 'Past';        
        }else{
          $db_records_all[$i]['paymentdue'] = 'No';
        }  
        
        if($db_records_all[$i]['pastbooking'] == 'Past' && $db_records_all[$i]['todaybooking'] == 'Today'){
          $db_records_all[$i]['color'] = '#FFF380'; 
          $db_records_all[$i]['paymentdue'] = 'Yes';
        }else if($db_records_all[$i]['pastbooking'] == 'Past' && $db_records_all[$i]['todaybooking'] != 'Today'){
          $db_records_all[$i]['color'] = '#FFC9C9';
          $db_records_all[$i]['paymentdue'] = 'Yes';
        }else if($db_records_all[$i]['pastbooking'] != 'Past' && $db_records_all[$i]['todaybooking'] == 'Today'){
          $db_records_all[$i]['color'] = '#CAFFCA';
          $db_records_all[$i]['paymentdue'] = 'Yes';
        }else{
          $db_records_all[$i]['color'] = '#FFF';
          $db_records_all[$i]['paymentdue'] = 'No';
        }   
      }     
  }            
  
  //echo "<pre>";
  //print_r($db_records_all); exit;
  
   
  if(!isset($start))
  	$start = 1;
  	$num_limit = ($start-1)*$rec_limit;
  	$startrec = $num_limit;
  	
  	$lastrec = $startrec + $rec_limit;
  	$startrec = $startrec + 1;
  	if($lastrec > $num_totrec)
  		$lastrec = $num_totrec;
  		if($num_totrec > 0 )
  		{
  			$recmsg = "Showing ".$startrec." - ".$lastrec." Records Of ".$num_totrec;
  		}
  		else
  		{
  			$recmsg="No Records Found.";
  		}
  
  if($paymenttype == 'all'){
    if(!count($db_records_all)>0 && $keyword != ""){
    	$var_msg_new = "Your search for <font color=#2e71b3>$keyword</font> has found <font color=#2e71b3>0</font> matches:";
    }else if($keyword != ""){
    	$var_msg_new = "Your search for <font color=#2e71b3>$keyword</font> has found <font color=#2e71b3>$num_totrec</font> matches:";
    }else if($alp !=''){
        $var_msg_new = "Your search for <font color=#2e71b3>$alp</font> has found <font color=#2e71b3>$num_totrec</font> matches:";
    }
  }
 
  
  $sql_alp = "select vDeparturePoint from rides where 1=1";
  $db_alp = $obj->MySQLSelect($sql_alp);
  for($i=0;$i<count($db_alp);$i++){
      $db_alp[$i] = strtoupper(substr($db_alp[$i]['vDeparturePoint'], 0,1));
  }
  
  $alpha_rs =implode(",",$db_alp);
  $AlphaChar = @explode(',',$alpha_rs);
  $AlphaBox.='<ul class="pagination">';
  for($i=65;$i<=90;$i++){
  	
  	if(!@in_array(chr($i),$AlphaChar)){
  		$AlphaBox.= '<li ><a href="#" onclick="return false;"  id="alch_'.$i.'">'.chr($i).'</a></li>';
  	}else{
  		$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
  	}
  }
  $AlphaBox.='</ul>';
  
  $smarty->assign("status",$status);
  $smarty->assign("db_records_all",$db_records_all);
  $smarty->assign("AlphaBox",$AlphaBox);
  $smarty->assign("recmsg",$recmsg);
  $smarty->assign("var_msg",$var_msg);
  $smarty->assign("keyword",$keyword);
  $smarty->assign("option",$option);
  $smarty->assign("page_link",$page_link);
  $smarty->assign("var_msg_new",$var_msg_new);
  $smarty->assign("sortby",$sortby);
  $smarty->assign("order",$order);
  $smarty->assign("paymenttype",$paymenttype);    
?>