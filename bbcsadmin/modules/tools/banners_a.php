<?php  
include_once(TPATH_LIBRARIES."/general/Imagecrop.class.php");
$thumb = new thumbnail();
$action = $_REQUEST['action'];
$iBannerId = $_POST['iBannerId'];
$Data = $_POST["Data"];
$temp_gallery = $tconfig["tsite_temp_gallery"];

if($Data['eType']=='Image'){
   $Data['tCode']='';
}else{
   $_FILES['Data']['name']['vImage']='';
}
#echo "<pre>"; print_r($_REQUEST); 
#echo "<pre>"; print_r($_FILES); exit;
$mode = $_POST['imagemode'];

if($mode == "confirm_delete")
  {   
    $sql = "select vImage from banners where iBannerId='".$iBannerId."' ";
    $db_gallery = $obj->MySQLSelect($sql);
    
    $imgfolder = $tconfig["tsite_upload_images_banner_path"];
    #echo $imgfolder;exit;      
    unlink($imgfolder.$db_gallery[0]['vImage']);
    $sql="UPDATE banners SET vImage = '' where iBannerId = '$iBannerId'"; 
	  $db_delete=$obj->sql_query($sql);
  	
    if($db_delete)$var_msg = "Banner Image Deleted Successfully.";else $var_msg="Error-in Delete.";
	  header("Location: ".$tconfig["tpanel_url"]."/index.php?file=to-banners&mode=edit&iBannerId=$iBannerId&var_msg_new=$var_msg");
	  exit;
}

if($action == "add")
{
	$redirect_file = $tconfig["tpanel_url"].'/index.php?file=to-banners&mode=add';   
	 $generalobj->checkDuplicate('iBannerId', "banners" , Array('vTitle'), $redirect_file, " Banner you have entered is already exists", "");   
	
	$Data['eSection']=$_REQUEST['eSection'];
	$Data['dDate'] = $generalobj->getSystemDateTime();
	$video_file_object = $_FILES['Data']['tmp_name']['vImage'];
	$video_file_name = $_FILES['Data']['name']['vImage'];
	if($video_file_name != "")
	{
		if($_FILES['Data']['size']['vImage'] > 2000)
		{
			$msg = "You have selected Image of Size greater than 2 MB.";  
			$vRedirectFile = $tconfig["tpanel_url"].'/index.php?file=to-banners&mode=add';
			$generalobj->getPostForm($_POST, $msg, $vRedirectFile);
			exit;
		}
		else if($_FILES['Data']['size']['vImage'] <= 2000)
		{
			$vFile = $generalobj->fileupload($tconfig["tsite_upload_images_banner_path"],$video_file_object,$video_file_name,$prefix='', $vaildExt="jpg,jpeg,gif,png");
			if($vFile[2] == 1)
			{	
				$msg = "You have selected wrong file format for Image. Valid formats are jpg,jpeg,gif,png";  
				$vRedirectFile = $tconfig["tpanel_url"].'/index.php?file=to-banners&mode=add';
				$generalobj->getPostForm($_POST, $msg, $vRedirectFile);
				exit;
			}
			$Data['vImage'] = $vFile[0];
		}		
	} 

  $id = $obj->MySQLQueryPerform("banners",$Data,'insert');
  if($id)
	{
		$var_msg = "Banner Added Successfully.";
	}
	else
	{
		$var_msg="Eror-in Add.";
	}
	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=to-banners&mode=view&var_msg=$var_msg");
	exit;
}
if($action == "edit")
{
	$Data['eSection']=$_REQUEST['eSection'];
	$video_file_object = $_FILES['Data']['tmp_name']['vImage'];
	$video_file_name = $_FILES['Data']['name']['vImage'];
	
	if($_FILES['Data']['size']['vImage'] > 2000)
	{
		$msg = "You have selected Image of Size greater than 2 MB.";  
		header("Location: ".$tconfig['tpanel_url']."/index.php?file=to-banners&mode=edit&iBannerId=".$iBannerId."&var_msg=".$msg);
		exit;
	}
	else if($_FILES['Data']['size']['vImage'] <= 2000)
	{
		if($video_file_name != "")
		{
			$vFile = $generalobj->fileupload($tconfig["tsite_upload_images_banner_path"],$video_file_object,$video_file_name,$prefix='', $vaildExt="jpg,jpeg,gif,png");

			if($vFile[2] == 1)
			{	
				$msg = "You have selected wrong file format for Image. Valid formats are jpg,jpeg,gif,png";  
				header("Location: ".$tconfig['tpanel_url']."/index.php?file=to-banners&mode=edit&iBannerId=".$iBannerId."&var_msg=".$msg);
				exit;
			}  
			$Data['vImage'] = $vFile[0];
		}
	}

  $iBannerId = $_POST['iBannerId'];
  $redirect_file = $tconfig["tpanel_url"].'/index.php?file=to-banners&mode=edit&iBannerId='.$iBannerId;   
	 $generalobj->checkDuplicate('iBannerId', "banners" , Array('vTitle'), $redirect_file, "Banner you have entered is already exists", $iBannerId);   
	 
	$where = " iBannerId = '".$iBannerId."'";

	$res = $obj->MySQLQueryPerform("banners",$Data,'update',$where);
	if($res)
	{
		$var_msg = "Banner Updated Successfully.";
	}
	else
	{
		$var_msg="Eror-in Update.";
	}
	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=to-banners&mode=view&var_msg=$var_msg");
	exit;
}
if($action == "Delete")
{
	$iBannerId = $_POST['iBannerId'];
	$sql="Delete from banners where iBannerId='".$iBannerId."'"; 
	$db_sql=$obj->sql_query($sql);	
	if($db_sql)$var_msg = "Banner Deleted Successfully.";else $var_msg="Eror-in Delete.";
	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=to-banners&mode=view&var_msg=$var_msg");
	exit;
}
if($action=="Active")
{
    $iBannerId = $_REQUEST['iBannerId'];
    $totid = count($iBannerId);
       
    if(is_array($iBannerId)){
        $iBannerId  = @implode(",",$iBannerId);
    }
    $data = array('eStatus'=>'Active');
    $where = " iBannerId IN (".$iBannerId.")";
	$res = $obj->MySQLQueryPerform("banners",$data,'update',$where);
	if($res)$var_msg = $totid."  Record Activated Successfully.";else $var_msg="Eror-in Activation.";
	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=to-banners&mode=view&var_msg=$var_msg");
	exit;
}

if($action=="Inactive")
{
    $iBannerId = $_REQUEST['iBannerId'];
    $totid = count($iBannerId);
    if(is_array($iBannerId)){
        $iBannerId  = @implode(",",$iBannerId);
    }
    $data = array('eStatus'=>'Inactive');
    $where = " iBannerId IN (".$iBannerId.")";
	$res = $obj->MySQLQueryPerform("banners",$data,'update',$where);
	if($res)$var_msg = $totid." Record Inactivated Successfully.";else $var_msg="Eror-in Activation.";
	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=to-banners&mode=view&var_msg=$var_msg");
	exit;
}
if($action=="Deletes")
{
    $iBannerId = $_REQUEST['iBannerId'];
    
    $totid = count($iBannerId);
    
    if(is_array($iBannerId)){
        $iBannerId  = @implode(",",$iBannerId);
    }
    $where = " iBannerId IN (".$iBannerId.")";
    
    $sql = "select * from banners where ".$where; 
    $db_banner_image=$obj->sql_query($sql);
    for($i=0;$i<$totid;$i++)
    {    
    unlink($tconfig["tsite_upload_images_banner_path"].$db_banner_image[$i]['vImage']); 
    unlink($tconfig["tsite_upload_images_banner_path"].$db_banner_image[$i]['vImage_FR']); 
    unlink($tconfig["tsite_upload_images_banner_path"].$db_banner_image[$i]['vImage_DE']); 
    unlink($tconfig["tsite_upload_images_banner_path"].$db_banner_image[$i]['vImage_CH']); 
    unlink($tconfig["tsite_upload_images_banner_path"].$db_banner_image[$i]['vImage_JA']); 
    unlink($tconfig["tsite_upload_images_banner_path"].$db_banner_image[$i]['vImage_SP']);    
    }
    
	$sql="Delete from banners where ".$where; 
	$db_sql=$obj->sql_query($sql);	
	if($db_sql)$var_msg = $totid." Record Deleted Successfully.";else $var_msg="Eror-in Delete.";
	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=to-banners&mode=view&var_msg=$var_msg");
	exit;
}
?>