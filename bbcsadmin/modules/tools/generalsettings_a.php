<?php
	include_once(TPATH_LIBRARIES."/general/Imagecrop.class.php");
	$thumb = new thumbnail();
	$action = $_REQUEST['action'];
	$Data = $_POST["Data"];
	
	//echo "<pre>"; print_r($_POST); /*print_r($_FILES);*/	exit;
    $validext = array ('image/png',"image/jpeg","image/gif","image/jpg","image/bmp");
	
	if($action == "edit")
	{
		
		if($_POST) {
			$Data = $_POST['Data'];
			
			################# For Logo Start #########################
			$logo_file_object = $_FILES['Data']['tmp_name']['SITE_LOGO'];
			$logo_file_name = $_FILES['Data']['name']['SITE_LOGO'];
			$logo_file_type = $_FILES['Data']['type']['SITE_LOGO'];
			//echo "<pre>"; print_r($logo_file_type); exit;
			
			$PATH = $tconfig["tsite_upload_site_logo_path"];
			$size1 = $tconfig["tsite_upload_images_site_logo_size1"];
			//$validext = array ('image/png',"image/jpeg","image/gif","image/jpg","image/bmp");

			if($logo_file_name != "" && in_array($logo_file_type, $validext))
			{ 	
				$vFile = $generalobj->general_upload_image($logo_file_object,$logo_file_name,$PATH,$size1,"","","","","","y","");
				
				
				$logoupdatepara = array("vValue"=>"$vFile");
				$where2 = "vName = 'SITE_LOGO'";
				$result2  = $obj->MySQLQueryPerform('configurations',$logoupdatepara,'update',$where2);
			}
			################# For Logo End #########################
			
			################# For Footer Logo Start #########################
			
			$footer_logo_file_object = $_FILES['Data']['tmp_name']['FOOTER_LOGO'];
			$footer_logo_file_name = $_FILES['Data']['name']['FOOTER_LOGO'];
			$footer_logo_file_type = $_FILES['Data']['type']['FOOTER_LOGO'];
			
			$PATH = $tconfig["tsite_upload_footer_logo_path"];
			$size2 = $tconfig["tsite_upload_images_footer_logo_size2"];
			
			if($footer_logo_file_name != "" && in_array($footer_logo_file_type, $validext))
			{	 	
				$vFile2 = $generalobj->general_upload_image($footer_logo_file_object,$footer_logo_file_name,$PATH,$size2,"","","","","","","");
				
				$logoupdatepara = array("vValue"=>"$vFile2");
				$where2 = "vName = 'FOOTER_LOGO'";
				$result2  = $obj->MySQLQueryPerform('configurations',$logoupdatepara,'update',$where2);
			}
			################# For Logo End #########################
			
			################# For Ride Background Start #########################
			
			
			/* ------ code for unlink image Start -------*/
			$where2 = "vName = 'RIDE_BACKGROUND'";
		    $sql="SELECT * FROM configurations WHERE ".$where2;
			$db_res=$obj->MySQLSelect($sql);
			$PATH = $tconfig["tsite_upload_footer_logo_path"];
			$unlink_img=$PATH."1_".$db_res[0]['vValue'];
			if($background_logo_file_name !='')
			{
			unlink($unlink_img);
			}
			/* ------ code for unlink image END -------*/
			$background_logo_file_object = $_FILES['Data']['tmp_name']['RIDE_BACKGROUND'];
			$background_logo_file_name = $_FILES['Data']['name']['RIDE_BACKGROUND'];
			$background_logo_file_type = $_FILES['Data']['type']['RIDE_BACKGROUND'];
			
			
			$size2 = $tconfig["tsite_upload_images_site_logo_size_bg"];
			
			if($background_logo_file_name != "" && in_array($background_logo_file_type, $validext))
			{	 	
				$vFile2 = $generalobj->general_upload_image($background_logo_file_object,$background_logo_file_name,$PATH,$size2,"","","","","","","");
				//echo $vFile2; exit;
				$logoupdatepara = array("vValue"=>"$vFile2");
				
				$result2  = $obj->MySQLQueryPerform('configurations',$logoupdatepara,'update',$where2);
			}
			################# For Ride Background End #########################
			
			
			################# For Email Logo Start #########################
			
			$Email_logo_file_object = $_FILES['Data']['tmp_name']['EMAIL_LOGO'];
			$Email_logo_file_name = $_FILES['Data']['name']['EMAIL_LOGO'];
			$Email_logo_file_type = $_FILES['Data']['type']['EMAIL_LOGO'];
			
			$PATH = $tconfig["tsite_upload_email_logo_path"];
			$size3 = $tconfig["tsite_upload_images_email_logo_size1"];

			if($Email_logo_file_name != "" && in_array($Email_logo_file_type, $validext))
			{ 
				$vFile4 = $generalobj->general_upload_image($Email_logo_file_object,$Email_logo_file_name,$PATH,$size3,"","","","","","","");
				
				$logoupdatepara = array("vValue"=>"$vFile4");
				$where2 = "vName = 'EMAIL_LOGO'";
				$result2  = $obj->MySQLQueryPerform('configurations',$logoupdatepara,'update',$where2);
			}
			################# For Email Logo End #########################
			
			################# For Favicon Start #########################			
			$favicon_file_object = $_FILES['Data']['tmp_name']['FAVICON_ICON'];
			$favicon_file_name = $_FILES['Data']['name']['FAVICON_ICON'];
			$favicon_file_type = $_FILES['Data']['type']['FAVICON_ICON'];
			
			$PATH1 = $tconfig["tsite_upload_site_favicon_path"];
			$size4 = $tconfig["tsite_upload_images_site_favicon_size4"];
			
			if($favicon_file_name != "")
			{
				$vFile1 = $generalobj->imageupload($PATH1,$favicon_file_object,$favicon_file_name,$prefix='', $vaildExt="ico");
				
				if($vFile1[2] != "")
				{	
					$msg = "You have selected wrong file format for Image. Valid format is ico";  
					header("Location: ".$tconfig["tpanel_url"]."/index.php?file=to-generalsettings&mode=edit&var_msg=".$msg."&eType=".$_POST['eType']);
					exit;
				}
				$faviconupdatepara = array("vValue"=>"$vFile1[0]");
				$where1 = "vName = 'FAVICON_ICON'";
				$result1  = $obj->MySQLQueryPerform('configurations',$faviconupdatepara,'update',$where1);
			}
			################# For Favicon End #########################
			
			foreach($Data as $key=>$val) { 
				$Value = array(
				'vValue'=>$val
				);
				$where = ' vName  = "'.$key.'"';
				$result  = $obj->MySQLQueryPerform('configurations',$Value,'update',$where);                
			}
			if($result){
				$var_msg='Setting Updated Successfully.';
				}else{
				$var_msg='Error-in updating setting.Please try again.';
			}
			header("Location: ".$tconfig["tpanel_url"]."/index.php?file=to-generalsettings&mode=edit&var_msg=".$var_msg."&eType=".$_POST['eType']);
			exit;
		}
	}
	################## Delete Logo & Favicon Start ######################
	if($action == "confirm_delete")
	{  
		$imagemode = $_POST['imagemode'];
		
		$sql = "select vValue from configurations where vName = '$imagemode' ";
		$db_gallery = $obj->MySQLSelect($sql);
		
		if($imagemode == "FAVICON_ICON")
		{ 
			$imgfolder = $tconfig["tsite_upload_site_favicon_path"];
		}
		
		unlink($imgfolder.$db_gallery[0]['vValue']);
		$sql="UPDATE configurations SET vValue = '' where vName = '$imagemode'"; 
		$db_delete=$obj->sql_query($sql);
		
		if($db_delete)$var_msg = "Deleted Successfully.";else $var_msg="Error-in Delete.";
		header("Location: ".$tconfig["tpanel_url"]."/index.php?file=to-generalsettings&mode=edit&var_msg=".$var_msg."&eType=".$_POST['eType']);
		exit;
	}
	################## Delete Logo & Fvaicon End ########################
	
?>