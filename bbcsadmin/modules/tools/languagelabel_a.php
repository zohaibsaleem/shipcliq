<?php
$action = $_REQUEST['action'];
$LanguageLabelId = $_POST['LanguageLabelId'];
$Data = $_POST["Data"];
$redirect_file = $tconfig["tpanel_url"]."/index.php?file=to-languagelabel&mode=view"; 

if($action == "add")
{

  	$generalobj->checkDuplicate('LanguageLabelId', "language_label" , Array('vLabel'), $redirect_file, "Label name you have entered is already exists", "");

    $sql = "SELECT * FROM language_master WHERE eStatus='Active'";
    $db_lang = $obj->MySQLSelect($sql);
    
    for($i=0;$i<count($db_lang);$i++){
        $sql = "INSERT INTO language_label (vCode, vLabel, vValue) VALUES ('".$db_lang[$i]['vCode']."', '".$_POST['Data']['vLabel']."', '".$obj->cleanQuery($_POST['Data']['vValue_'.$db_lang[$i]['vCode']])."')";
        $obj->sql_query($sql);
    }

    $separator = "\n";
    for($i=0;$i<count($db_lang);$i++) {
      $get_label = "SELECT vLabel,vValue from language_label  where vCode='".$db_lang[$i]['vCode']."'";
      $res_label = $obj->MySQLSelect($get_label);

      $content = "";
      $content = "<?php ".$separator.$separator;
      for($j=0;$j<count($res_label);$j++){
      $content .= 'define("'.$res_label[$j][vLabel].'","'.trim(str_replace('"',"'",stripslashes($res_label[$j][vValue]))).'");';
      $content .= $separator;
      }
      $content .= $separator."?>";

      //$filename = $tconfig["tsite_label_path"].$db_lang[$i]['vCode']."/".$db_lang[$i]['vCode'].".php";
     // if (!$handle = fopen($filename, 'w+')) {
    //      echo "Cannot open file ($filename)";
    //      exit;
    //   }
       // Write $somecontent to our opened file.
      // if (fwrite($handle, $content) === FALSE) {
      //    echo "Cannot write to file ($filename)";
      //    exit;
      // }
    }


    header("Location: ".$tconfig["tpanel_url"]."/index.php?file=to-languagelabel&mode=view&var_msg=$var_msg");
    exit;
}

if($action == "edit")
{

    $sql = "SELECT * FROM language_master WHERE eStatus='Active'";
    $db_lang = $obj->MySQLSelect($sql);
    
    for($i=0;$i<count($db_lang);$i++){
        $sql = "UPDATE language_label set vValue='".$obj->cleanQuery($_POST['Data']['vValue_'.$db_lang[$i]['vCode']])."' WHERE vLabel='".$_POST['Data']['vLabel']."' AND vCode='".$db_lang[$i]['vCode']."'";
        $obj->sql_query($sql);
    }

    $separator = "\n";
    for($i=0;$i<count($db_lang);$i++) {
      $get_label = "SELECT vLabel,vValue from language_label  where vCode='".$db_lang[$i]['vCode']."'";
      $res_label = $obj->MySQLSelect($get_label);

      $content = "";
      $content = "<?php ".$separator.$separator;
      for($j=0;$j<count($res_label);$j++){
      $content .= 'define("'.$res_label[$j][vLabel].'","'.trim(str_replace('"',"'",stripslashes(utf8_encode($res_label[$j][vValue])))).'");';
      $content .= $separator;
      }
      $content .= $separator."?>";
      if($db_lang[$i]['vCode'] == "RS"){
      //echo $content;exit;
      //echo $enc = mb_detect_encoding($content);
      //exit;
      }
    //  $filename = $tconfig["tsite_label_path"].$db_lang[$i]['vCode']."/".$db_lang[$i]['vCode'].".php";
    //  if (!$handle = fopen($filename, 'w+')) {
   //       echo "Cannot open file ($filename)";
   //       exit;
   //    }
       // Write $somecontent to our opened file.
       //if (fwrite($handle, $content) === FALSE) {
       //   echo "Cannot write to file ($filename)";
       //   exit;
      // }
    }

    header("Location: ".$tconfig["tpanel_url"]."/index.php?file=to-languagelabel&mode=view&var_msg=$var_msg");
    exit;
}
?>