<?php
$action = $_REQUEST['action'];
$iMakeId = $_POST['iMakeId'];

$Data = $_POST["Data"];

if($action == "add")
{
  $redirect_file = $tconfig["tpanel_url"].'/index.php?file=u-make&mode=add';   
	$generalobj->checkDuplicate('iMakeId', "make" , Array('vMake'), $redirect_file, "Make you have entered is already exists", "");
  
  $id = $obj->MySQLQueryPerform("make",$Data,'insert');
	 
  if($id)
  {  
	   $var_msg = "Make is Added Successfully.";
  }
  else
	{
		$var_msg="Eror-in Add.";
	}
	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=u-make&mode=view&var_msg=$var_msg");
	exit;
}
if($action == "edit")
{
  $redirect_file = $tconfig["tpanel_url"].'/index.php?file=u-make&mode=edit&iMakeId='.$iMakeId;   
	$generalobj->checkDuplicate('iMakeId', "make" , Array('vMake'), $redirect_file, "Make you have entered is already exists", $iMakeId);
  
  $iMakeId = $_POST['iMakeId'];
	$where = " iMakeId = '".$iMakeId."'";

	$res = $obj->MySQLQueryPerform("make",$Data,'update',$where);
	if($res)
	{
		$var_msg = "Make is Updated Successfully.";
	}
	else
	{
		$var_msg="Eror-in Update.";
	}
	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=u-make&mode=view&var_msg=$var_msg");
	exit;
}
if($action == "Delete")
{
	$iMakeId = $_POST['iMakeId'];
	$sql="Delete from make where iMakeId='".$iMakeId."'"; 
	$db_sql=$obj->sql_query($sql);	
	if($db_sql)$var_msg = "Make is Deleted Successfully.";else $var_msg="Eror-in Delete.";
	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=u-make&mode=view&var_msg=$var_msg");
	exit;
}
if($action=="Active")
{
    $iMakeId = $_REQUEST['iMakeId'];
    $totid = count($iMakeId);
       
    if(is_array($iMakeId)){
        $iMakeId  = @implode(",",$iMakeId);
    }
    $data = array('eStatus'=>'Active');
    $where = " iMakeId IN (".$iMakeId.")";
	$res = $obj->MySQLQueryPerform("make",$data,'update',$where);
	if($res)$var_msg = $totid."  Record Activated Successfully.";else $var_msg="Eror-in Activation.";
	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=u-make&mode=view&var_msg=$var_msg");
	exit;
}

if($action=="Inactive")
{
    $iMakeId = $_REQUEST['iMakeId'];
    $totid = count($iMakeId);
    if(is_array($iMakeId)){
        $iMakeId  = @implode(",",$iMakeId);
    }
    $data = array('eStatus'=>'Inactive');
    $where = " iMakeId IN (".$iMakeId.")";
	$res = $obj->MySQLQueryPerform("make",$data,'update',$where);
	if($res)$var_msg = $totid." Record Inactivated Successfully.";else $var_msg="Eror-in Activation.";
	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=u-make&mode=view&var_msg=$var_msg");
	exit;
}                                                       
if($action=="Deletes")
{
    $iMakeId = $_REQUEST['iMakeId'];
    
    $totid = count($iMakeId);
    
    if(is_array($iMakeId)){
        $iMakeId  = @implode(",",$iMakeId);
    }
    $where = " iMakeId IN (".$iMakeId.")";
	$sql="Delete from make where ".$where; 
	$db_sql=$obj->sql_query($sql);	
	if($db_sql)$var_msg = $totid." Record Deleted Successfully.";else $var_msg="Eror-in Delete.";
	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=u-make&mode=view&var_msg=$var_msg");
	exit;
}

?>