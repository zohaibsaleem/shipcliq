<?php
include_once(TPATH_LIBRARIES."/general/Imagecrop.class.php");
$thumb = new thumbnail();
$action = $_REQUEST['action'];
$iMediaPartnerId = $_POST['iMediaPartnerId']; 
$Data = $_POST["Data"];    
$mode = $_POST['imagemode'];
if($mode == "delete_image")
  {
    $sql = "select vImage from media_partners where iMediaPartnerId='".$iMediaPartnerId."' ";
    $db_gallery = $obj->MySQLSelect($sql);    
    $imgfolder = $tconfig["tsite_upload_media_partners_path"];          
    @unlink($imgfolder.$db_gallery[0]['vImage']);
    @unlink($imgfolder."1_".$db_gallery[0]['vImage']);    
    @unlink($imgfolder."2_".$db_gallery[0]['vImage']);        	
	  @unlink($imgfolder."3_".$db_gallery[0]['vImage']);        	
	  $sql="UPDATE media_partners SET vImage = '' where iMediaPartnerId = '$iMediaPartnerId'"; 
	  $db_delete=$obj->sql_query($sql);
  	
    if($db_delete)$var_msg = "Media Partner Image Deleted Successfully.";else $var_msg="Error-in Delete.";
	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=u-media_partners&mode=edit&iMediaPartnerId=$iMediaPartnerId&var_msg=$var_msg");
	  exit;
}
/*------------------Code For Adding Data(Starts)-------------------------------------*/

if($action == "add")
{
  $redirect_file = $tconfig["tpanel_url"].'/index.php?file=u-media_partners&mode=add';   
	$generalobj->checkDuplicate('iMediaPartnerId', "media_partners" , Array('vTitle'), $redirect_file, "Media Partner you have entered is already exists", '');
  

	$image_object = $_FILES['Data']['tmp_name']['vImage'];
	$image_name = $_FILES['Data']['name']['vImage'];
	if($image_name != "")
	{  
		$imgfolder = $tconfig["tsite_upload_media_partners_path"];
		$Data['vImage'] = $generalobj->general_upload_image($image_object,$image_name,$imgfolder,$tconfig["tsite_upload_media_partners_size1"],$tconfig["tsite_upload_media_partners_size2"],$tconfig["tsite_upload_media_partners_size3"],"",'','','Y','');		
	}
	  $id = $obj->MySQLQueryPerform("media_partners",$Data,'insert');
	  
    if($id)
    {
        $var_msg = "Media Partner Details is Added Successfully.";
    }else{
        $var_msg="Error-in Add.";
    } 
	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=u-media_partners&mode=view&var_msg=$var_msg");
	exit;
}
/*------------------Code For Adding Data(Ends)-------------------------------------*/
/*------------------Code For Editing Data(Starts)-------------------------------------*/
if($action == "edit")
{
	$redirect_file = $tconfig["tpanel_url"].'/index.php?file=u-media_partners&mode=edit&iMediaPartnerId='.$iMediaPartnerId;   
	$generalobj->checkDuplicate('iMediaPartnerId', "media_partners" , Array('vTitle'), $redirect_file, "Media Partner you have entered is already exists", $iMediaPartnerId);
  
  $iMediaPartnerId = $_POST['iMediaPartnerId'];
	
	$image_object = $_FILES['Data']['tmp_name']['vImage'];
  $image_name = $_FILES['Data']['name']['vImage'];
  if($image_name != "")
  {        
        $sql = "select vImage from media_partners where iMediaPartnerId='".$iMediaPartnerId."' ";
        $db_gallery = $obj->MySQLSelect($sql);
        $imgfolder = $tconfig["tsite_upload_media_partners_path"];	
                 
        @unlink($imgfolder.$db_gallery[0]['vImage']);
        @unlink($imgfolder."1_".$db_gallery[0]['vImage']);    
        @unlink($imgfolder."2_".$db_gallery[0]['vImage']);        
		    @unlink($imgfolder."3_".$db_gallery[0]['vImage']);        
        $Data['vImage'] = $generalobj->general_upload_image($image_object,$image_name,$imgfolder,$tconfig["tsite_upload_media_partners_size1"],$tconfig["tsite_upload_media_partners_size2"],$tconfig["tsite_upload_media_partners_size3"],"",'','','Y','');
  }
  $where = " iMediaPartnerId = '".$iMediaPartnerId."'";
	$res = $obj->MySQLQueryPerform("media_partners",$Data,'update',$where);	
	if($res)
  {
      $var_msg = "Media Partner Details is Updated Successfully.";
  }
	else
	{
      $var_msg="Error-in Update.";
  }
	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=u-media_partners&mode=view&var_msg=$var_msg");
	exit;
}
/*------------------Code For Editing Data(Ends)-------------------------------------*/
/*------------------Code For General Action(Starts)-------------------------------------*/
if($action == "Delete")
{
	$iMediaPartnerId = $_POST['iMediaPartnerId'];
	$sql="Delete from media_partners where iMediaPartnerId='".$iMediaPartnerId."'"; 
	$db_sql=$obj->sql_query($sql);
    	
	if($db_sql)$var_msg = "Media Partner Details is Deleted Successfully.";else $var_msg="Error-in Delete.";
	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=u-media_partners&mode=view&var_msg=$var_msg");
	exit;
}
if($action=="Active")
{
    $iMediaPartnerId = $_REQUEST['iMediaPartnerId'];
    $totid = count($iMediaPartnerId);
    if(is_array($iMediaPartnerId)){
        $iMediaPartnerId = @implode(",",$iMediaPartnerId);
    }
    $data = array('eStatus'=>'Active');
    $where = " iMediaPartnerId IN (".$iMediaPartnerId.")";
	$res = $obj->MySQLQueryPerform("media_partners",$data,'update',$where);
	if($res)$var_msg = $totid."  Record Activated Successfully.";else $var_msg="Error-in Activation.";
	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=u-media_partners&mode=view&var_msg=$var_msg");
	exit;
}

if($action=="Inactive")
{
    $iMediaPartnerId = $_REQUEST['iMediaPartnerId'];
    $totid = count($iMediaPartnerId);
    if(is_array($iMediaPartnerId)){
        $iMediaPartnerId  = @implode(",",$iMediaPartnerId);
    }
    $data = array('eStatus'=>'Inactive');
    $where = " iMediaPartnerId IN (".$iMediaPartnerId.")";
	$res = $obj->MySQLQueryPerform("media_partners",$data,'update',$where);
	if($res)$var_msg = $totid." Record Inactivated Successfully.";else $var_msg="Error-in Activation.";
	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=u-media_partners&mode=view&var_msg=$var_msg");
	exit;
}
if($action=="Deletes")
{
    $iMediaPartnerId = $_REQUEST['iMediaPartnerId'];
    $totid = count($iMediaPartnerId);
    if(is_array($iMediaPartnerId)){
        $iMediaPartnerId  = @implode(",",$iMediaPartnerId);
    }
    $where = " iMediaPartnerId IN (".$iMediaPartnerId.")";
    
  $sql = "select iMediaPartnerId,vImage from media_partners where ".$where ;
  $db_gallery = $obj->MySQLSelect($sql);
  $imgfolder = $tconfig["tsite_upload_media_partners_path"];
  for($i=0;$i<count($db_gallery);$i++){
  @unlink($imgfolder.$db_gallery[$i]['vImage']);
  @unlink($imgfolder."1_".$db_gallery[$i]['vImage']);    
  @unlink($imgfolder."2_".$db_gallery[$i]['vImage']);        	
  @unlink($imgfolder."3_".$db_gallery[$i]['vImage']);        	
	}
    
    
	$sql="Delete from media_partners where ".$where; 
	$db_sql=$obj->sql_query($sql);	
	if($db_sql)$var_msg = $totid." Record Deleted Successfully.";else $var_msg="Error-in Delete.";
	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=u-media_partners&mode=view&var_msg=$var_msg");
	exit;
}
/*------------------Code For General Action(Ends)-------------------------------------*/
?>