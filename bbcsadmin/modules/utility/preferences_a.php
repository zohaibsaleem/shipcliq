<?php
include_once(TPATH_LIBRARIES."/general/Imagecrop.class.php");
$thumb = new thumbnail();
$action = $_REQUEST['action'];
$iPreferencesId = $_POST['iPreferencesId'];
$mode = explode("-",$_POST['imagemode']);
$Data = $_POST["Data"];     

if($mode[0] == "delete_image")
  {
    $sql = "select ".$mode[1]." as vImage from preferences where iPreferencesId='".$iPreferencesId."' ";
    $db_gallery = $obj->MySQLSelect($sql);    
    $imgfolder = $tconfig["tsite_upload_images_preferences_path"];          
    @unlink($imgfolder.$db_gallery[0]['vImage']);
         	
	  $sql="UPDATE preferences SET ".$mode[1]." = '' where iPreferencesId = '$iPreferencesId'"; 
	  $db_delete=$obj->sql_query($sql);
  	
    if($db_delete)$var_msg = "Image Deleted Successfully.";else $var_msg="Error-in Delete.";
	  header("Location: ".$tconfig["tpanel_url"]."/index.php?file=u-preferences&mode=edit&iPreferencesId=$iPreferencesId&var_msg_new=$var_msg");
	  exit;
}
  
if($action == "add")
{
  $redirect_file = $tconfig["tpanel_url"].'/index.php?file=u-preferences&mode=add';   
	$generalobj->checkDuplicate('iPreferencesId', "preferences" , Array('vPreferences'), $redirect_file, "Preferences you have entered is already exists", "");
  
  $id = $obj->MySQLQueryPerform("preferences",$Data,'insert');
	
   $image_Yes_object = $_FILES['Data']['tmp_name']['vYes'];
	$image_Yes = $_FILES['Data']['name']['vYes'];
	if($image_Yes != "")
	{  
		$imgfolder = $tconfig["tsite_upload_images_preferences_path"];		
		$vYesImage = $generalobj->general_upload_image($image_Yes_object,$image_Yes,$imgfolder,"","","","",'','','Y','');		
		$sql = "UPDATE preferences set vYes='".$vYesImage."' where iPreferencesId = '".$id."' ";
		$db_gallery = $obj->MySQLSelect($sql);
	} 
	$image_no_object = $_FILES['Data']['tmp_name']['vNO'];
	$image_no = $_FILES['Data']['name']['vNO'];
	if($image_no != "")
	{  
		$imgfolder = $tconfig["tsite_upload_images_preferences_path"];		
		$vNoImage = $generalobj->general_upload_image($image_no_object,$image_no,$imgfolder,"","","","",'','','Y','');		
		$sql = "UPDATE preferences set vNO='".$vNoImage."' where iPreferencesId = '".$id."' ";
		$db_gallery = $obj->MySQLSelect($sql);
	} 
	$image_MAYBE_object = $_FILES['Data']['tmp_name']['vMAYBE'];
	$image_MAYBE = $_FILES['Data']['name']['vMAYBE'];
	if($image_MAYBE != "")
	{  
		$imgfolder = $tconfig["tsite_upload_images_preferences_path"];		
		$vMAYBEImage = $generalobj->general_upload_image($image_MAYBE_object,$image_MAYBE,$imgfolder,"","","","",'','','Y','');		
		$sql = "UPDATE preferences set vMAYBE='".$vMAYBEImage."' where iPreferencesId = '".$id."' ";
		$db_gallery = $obj->MySQLSelect($sql);
	} 
	
  if($id)
  {  
	   $var_msg = "preferences is Added Successfully.";
  }
  else
	{
		$var_msg="Eror-in Add.";
	}
	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=u-preferences&mode=view&var_msg=$var_msg");
	exit;
}
if($action == "edit")
{
 

  $iPreferencesId = $_POST['iPreferencesId'];
	$where = " iPreferencesId = '".$iPreferencesId."'";

  $image_yes_object = $_FILES['Data']['tmp_name']['vYes'];
  $image_yes = $_FILES['Data']['name']['vYes'];
  if($image_yes != "")
  {        
        $sql = "select vYes from preferences where iPreferencesId='".$iPreferencesId."' ";
        $db_gallery = $obj->MySQLSelect($sql);
        $imgfolder = $tconfig["tsite_upload_images_preferences_path"];	                
        @unlink($imgfolder.$db_gallery[0]['vYes']);     
        $Data['vYes'] = $generalobj->general_upload_image($image_yes_object,$image_yes,$imgfolder,'','','',"",'','','Y','');
  }
   $image_MAYBE_object = $_FILES['Data']['tmp_name']['vMAYBE'];
  $image_MAYBE = $_FILES['Data']['name']['vMAYBE'];
  if($image_MAYBE != "")
  {        
        $sql = "select vMAYBE from preferences where iPreferencesId='".$iPreferencesId."' ";
        $db_gallery = $obj->MySQLSelect($sql);
        $imgfolder = $tconfig["tsite_upload_images_preferences_path"];	                
        @unlink($imgfolder.$db_gallery[0]['vMAYBE']);     
        $Data['vMAYBE'] = $generalobj->general_upload_image($image_MAYBE_object,$image_MAYBE,$imgfolder,'','','',"",'','','Y','');
  }

   $image_NO_object = $_FILES['Data']['tmp_name']['vNO'];
  $image_NO = $_FILES['Data']['name']['vNO'];
  if($image_NO != "")
  {        
        $sql = "select vNO from preferences where iPreferencesId='".$iPreferencesId."' ";
        $db_gallery = $obj->MySQLSelect($sql);
        $imgfolder = $tconfig["tsite_upload_images_preferences_path"];	                
        @unlink($imgfolder.$db_gallery[0]['vNO']);     
        $Data['vNO'] = $generalobj->general_upload_image($image_NO_object,$image_NO,$imgfolder,'','','',"",'','','Y','');
  }


	$res = $obj->MySQLQueryPerform("preferences",$Data,'update',$where);
	if($res)
	{
		$var_msg = "preferences is Updated Successfully.";
	}
	else
	{
		$var_msg="Eror-in Update.";
	}
	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=u-preferences&mode=view&var_msg=$var_msg");
	exit;
}
if($action == "Delete")
{
	$iPreferencesId = $_POST['iPreferencesId'];
	$sql="Delete from preferences where iPreferencesId='".$iPreferencesId."'"; 
	$db_sql=$obj->sql_query($sql);	
	if($db_sql)$var_msg = "preferences is Deleted Successfully.";else $var_msg="Eror-in Delete.";
	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=u-preferences&mode=view&var_msg=$var_msg");
	exit;
}
if($action=="Active")
{
    $iPreferencesId = $_REQUEST['iPreferencesId'];
    $totid = count($iPreferencesId);
       
    if(is_array($iPreferencesId)){
        $iPreferencesId  = @implode(",",$iPreferencesId);
    }
    $data = array('eStatus'=>'Active');
    $where = " iPreferencesId IN (".$iPreferencesId.")";
	$res = $obj->MySQLQueryPerform("preferences",$data,'update',$where);
	if($res)$var_msg = $totid."  Record Activated Successfully.";else $var_msg="Eror-in Activation.";
	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=u-preferences&mode=view&var_msg=$var_msg");
	exit;
}

if($action=="Inactive")
{
    $iPreferencesId = $_REQUEST['iPreferencesId'];
    $totid = count($iPreferencesId);
    if(is_array($iPreferencesId)){
        $iPreferencesId  = @implode(",",$iPreferencesId);
    }
    $data = array('eStatus'=>'Inactive');
    $where = " iPreferencesId IN (".$iPreferencesId.")";
	$res = $obj->MySQLQueryPerform("preferences",$data,'update',$where);
	if($res)$var_msg = $totid." Record Inactivated Successfully.";else $var_msg="Eror-in Activation.";
	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=u-preferences&mode=view&var_msg=$var_msg");
	exit;
}                                                       
if($action=="Deletes")
{
    $iPreferencesId = $_REQUEST['iPreferencesId'];
    
    $totid = count($iPreferencesId);
    
    if(is_array($iPreferencesId)){
        $iPreferencesId  = @implode(",",$iPreferencesId);
    }
    $where = " iPreferencesId IN (".$iPreferencesId.")";
    $sql = "select vYes,vNO,vMAYBE from preferences where ".$where ;
    $db_gallery = $obj->MySQLSelect($sql);
    $imgfolder = $tconfig["tsite_upload_images_preferences_path"];
      for($i=0;$i<count($db_gallery);$i++){
          @unlink($imgfolder.$db_gallery[$i]['vYes']);
          @unlink($imgfolder.$db_gallery[$i]['vNO']);
          @unlink($imgfolder.$db_gallery[$i]['vMAYBE']);      	
    	}
    
	$sql="Delete from preferences where ".$where; 
	$db_sql=$obj->sql_query($sql);	
	if($db_sql)$var_msg = $totid." Record Deleted Successfully.";else $var_msg="Eror-in Delete.";
	header("Location: ".$tconfig["tpanel_url"]."/index.php?file=u-preferences&mode=view&var_msg=$var_msg");
	exit;
}

?>