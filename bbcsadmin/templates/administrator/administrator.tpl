<script language="JavaScript" src="{$tconfig.tcp_javascript}validate.js"></script>
<div class="contentcontainer" id="tabs">
	<div class="headings">
		{if $mode eq 'edit'}
		<h2 class="left">Edit Administrator</h2>
    {else}
		<h2 class="left">Add Administrator</h2>
    {/if}
			{*if $mode eq 'edit'}
      <ul class="smltabs">
				<li><a href="#tabs-1">Edit Profile</a></li>
				<li><a href="#tabs-2">Change password</a></li>
			</ul>
            {else}
            <ul class="smltabs">
				<li><a href="#tabs-1">Edit Profile</a></li>
			</ul>
            {/if*}
	</div>
	<div class="contentbox" id="tabs-1">
            {if $var_msg neq ''}
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Success" />
					{$var_msg}</p> 
				</div>     
				<div></div>
			{/if}
			{if $var_msg_error neq ''}
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Success" />
					{$var_msg_error}</p> 
				</div>     
				<div></div>
			{/if}
			<form id="frmadd" name="frmadd" action="index.php?file=ad-administrator_a" method="post">
            <input type="hidden" name="iAdminId" id="iAdminId" value="{$iAdminId}" />
            <input type="hidden" name="action" id="action" value="{$mode}" />
       	<p>
					<label for="textfield"><strong>First Name :</strong></label>
					<input type="text" id="vLastName" name="Data[vFirstName]" class="inputbox" value="{$db_admin[0].vFirstName}" lang="*" title="First Name"/>
				</p>
				<p>
					<label for="textfield"><strong>Last Name :</strong></label>
					<input type="text" id="vLastName" name="Data[vLastName]" class="inputbox" value="{$db_admin[0].vLastName}" lang="*" title="Last Name"/>
				</p>
			  <p>
					<label for="textfield"><strong>Contact No :</strong></label>
					<input type="text" id="vContactNo" name="Data[vContactNo]" class="inputbox" value="{$db_admin[0].vContactNo}" lang="{literal}*{T}{/literal}" title="Contact No"/>
				</p>
                
				<p>
					<label for="textfield"><strong>E-mail :</strong></label>
					<input type="text" id="vEmail"  name="Data[vEmail]" class="inputbox" value="{$db_admin[0].vEmail}"  lang="{literal}*{E}{/literal}" title="Email" />
				</p>
				<p>
					<label for="textfield"><strong>User Name :</strong></label>
					<input type="text" id="vUserName" name="Data[vUserName]" class="inputbox" lang="{literal}*{P}6:0{/literal}" title="User Name" value="{$db_admin[0].vUserName}"/>
				</p>
				<p>
					<label for="textfield"><strong>Password :<em>*</em></strong></label>
					<input type="password" id="vPassword"  name="Data[vPassword]" class="inputbox" lang="{literal}*{P}6:0{/literal}" title="Password" value="{$db_admin[0].vPassword}"/>
				</p>
				{if $db_admin[0].vUserName NEQ "superadmin"}
				<p>
					<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $db_admin[0].eStatus eq Active}selected{/if}>Active</option>
						<option value="Inactive" {if $db_admin[0].eStatus eq Inactive}selected{/if}>Inactive</option>
					</select>
				</p>
				 <p>
					<label for="textfield"><strong>Grant Access to :</strong></label>
				  {$strmodule}
				</p>
				{/if}
				
				{if $mode eq 'edit'}
				<input type="submit" value="Edit Administrator" class="btn" onclick="return validate(document.frmadd);" title="Edit Administrator"/>
				      {else}
   				<input type="submit" value="Add Administrator" class="btn" onclick="return validate(document.frmadd);" title="Add Administrator"/>
  				
				      
				      {/if}
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
			</form>
	</div>
</div>
{literal}
<script>

function redirectcancel()
{
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=ad-administrator&mode=view";
    return false;
}

function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
{/literal}