<script language="JavaScript" src="{$tconfig.tcp_javascript}validate.js"></script>
<div class="contentcontainer" id="tabs">
	<div class="headings">
		{if $mode eq 'edit'}
		<h2 class="left">Edit Language Status</h2>
    
    {/if}
      	</div>
	<div class="contentbox" id="tabs-1">
            {if $var_msg neq ''}
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Success" />
					{$var_msg}</p> 
				</div>     
				<div></div>
			{/if}
			{if $var_msg_error neq ''}
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Success" />
					{$var_msg_error}</p> 
				</div>     
				<div></div>
			{/if}
			<form id="frmadd" name="frmadd" action="index.php?file=ad-languagemaster_a" method="post">
            <input type="hidden" name="iLanguageMasId" id="iLanguageMasId" value="{$iLanguageMasId}" />
            <input type="hidden" name="action" id="action" value="{$mode}" />
       	<p>
					<label for="textfield"><strong>Language :</strong></label>
					<input type="text" id="vTitle" name="Data[vTitle]" class="inputbox" value="{$db_admin[0].vTitle}" lang="*" title="Language"/>
				</p>
				<p>
					<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]" disabled>
						<option value="Active" {if $db_admin[0].eStatus eq Active}selected{/if}>Active</option>
						<option value="Inactive" {if $db_admin[0].eStatus eq Inactive}selected{/if}>Inactive</option>
					</select>
				</p>
					<p>
					<label for="textfield"><strong>Front Status :</strong></label>
					<select id="eStatus" name="Data[eFrontStatus]">
						<option value="Yes" {if $db_admin[0].eFrontStatus eq Yes}selected{/if}>Yes</option>
						<option value="No" {if $db_admin[0].eFrontStatus eq No}selected{/if}>No</option>
					</select>
				</p>
				
				
				{if $mode eq 'edit'}
				<input type="submit" value="Edit Language Status" class="btn" onclick="return validate(document.frmadd);" title="Edit Administrator"/>
				      {else}
   				<input type="submit" value="Add Language Status" class="btn" onclick="return validate(document.frmadd);" title="Add Administrator"/>
  				
				      
				      {/if}
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
			</form>
	</div>
</div>
{literal}
<script>

function redirectcancel()
{
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=ad-languagemaster&mode=view";
    return false;
}

function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
{/literal}