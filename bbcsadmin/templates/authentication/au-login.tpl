<script language="JavaScript" src="{$tconfig.tcp_javascript}jquery-latest.js"></script>
<script language="JavaScript" src="{$tconfig.tcp_javascript}validate.js"></script>    

<div id="logincontainer">
	<div id="loginbox">
		<div id="loginheader">	
			<img src="{$logodis}" alt="projectName" class="logo_login"/><br><br> 
      <!--<img src="{$tconfig.tpanel_theme_img}cp_logo_login.png" alt="Control Panel Login" />-->
	  <h2 class="heading_title">Control Panel</h2>
		</div>
        {if $var_msg neq ''}
        <div style="text-align:center;">
            {$var_msg}
        </div>
        {/if}
        <div id="showloginid">
		<div id="innerlogin">
			<form name="frmlogin" id="frmlogin" action="index.php?file=au-login" method="post"  >
				<input type="hidden" name="data[mode]" value="authenticate">
				<p>Enter your username:</p>
				<input type="text" class="logininput" name="vUserName" id="vUserName" title="User Name"/>
				
				<p>Enter your password:</p>
				<input type="password" class="logininput" id="vPassword" name="vPassword" title="Password"/>

				<input type="submit" class="loginbtn" value="Submit"  title="Submit" onclick="return CheckLogin();"/><br />
			</form>
		</div>
        </div>
        <div id="forgotpasswordid" style="display:none;">
		<div id="innerlogin">
			<form name="frmforgot" id="frmforgot" action="index.php?file=au-login" method="post"  >
				<input type="hidden" name="data[mode]" value="forgotpassword">
				<p>Enter your Email:</p>
				<input type="text" class="logininput" name="vEmail" id="vEmail" title="Email" lang="{literal}*{E}{/literal}"/>
				<input type="submit" class="loginbtn" value="Submit"  title="Submit" onclick="return validate(document.frmforgot);"/><br />
				<p><a href="javascript:void(0);" title="Back to Login" onclick="showlogin();">Back to Login</a></p>
			</form>
		</div>
        </div>
	</div>
	<img src="{$tconfig.tpanel_img}login_fade.png" alt="Fade" />
</div>
{literal}
<script type="text/javascript">
function CheckLogin()
{
	if (document.frmlogin.vUserName.value == "")
	{
		alert("Enter your Username");
		document.frmlogin.vUserName.focus();
		return false;
	}
	if (document.frmlogin.vPassword.value == "")
	{
		alert("Enter your password");
		document.frmlogin.vPassword.focus();
		return false;
	}
	document.frmlogin.submit()
}

</script>
<script>
function showforgot(){
    jQuery("#showloginid").hide("slow");
    jQuery("#forgotpasswordid").show("slow");
}
function showlogin(){
    jQuery("#forgotpasswordid").hide("slow");
    jQuery("#showloginid").show("slow");
}  
</script>
{/literal}