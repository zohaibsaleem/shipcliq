<div class="contentcontainer" id="tabs">
	<div class="headings">
    	{if $mode eq 'edit'}
			<h2 class="left">Booking Payment Details</h2>
        {else}
			<h2 class="left">Booking Payment Details</h2>
        {/if}
	</div>
	<div class="contentbox" id="tabs-1">           
			{if $var_msg neq ''}
				<div class="status success" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
					{$var_msg}</p> 
				</div> 
			{/if}
			{if $var_msg_err neq ''}
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Success" />
					{$var_msg_err}</p> 
				</div> 
			{/if}
                <table border="0" cellpadding="1" cellspacing="1" width="100%" align="center" class="oderpage">
									<tr>
										<td valign="top">
											<table class="headingbg" style="border: 1px solid #ccc;" border="0" cellpadding="1" cellspacing="1" width="100%" align="center">
										  <tr>
												<td {if $db_book[0].eStatus eq 'Cencelled'}colspan="3"{/if} width="100%"  class="headingbg wmatterbold" bgcolor="#309ec3" height="22">&nbsp;<b><span style="color:#000000">Booking #{$db_book[0].vBookingNo}</span></b></td>
											</tr>
											<tr bgcolor="#ffffff">
												<td valign="top" {if $db_book[0].eStatus eq 'Cencelled'} width="47%" {/if}>
													<table border="0" cellpadding="1" cellspacing="1" width="100%" style="border: 1px solid #ccc;">
													<tbody>
                          <tr>
    												<td colspan="3" width="100%"  class="headingbg wmatterbold" bgcolor="#E6E6E6" height="22">&nbsp;<b><span style="color:#000000">Booking Details</span></b></td>
    											</tr>
                          <tr>
														<td width="34%" valign="top">Booking No&nbsp;</td>
														<td width="1%" valign="top"> : </td>
														<td width="65%" valign="top">{$db_book[0].vBookingNo}</td>
													</tr>
													<tr>
														<td align="left" valign="top">Trips Details&nbsp;</td>
														<td valign="top"> : </td>
														<td align="left">
                              <table>
                                <tr>
                                  <td>From</td>
                                  <td>:</td>
                                  <td>
                                     {$db_book[0].vFromPlace}
                                  </td>
                                </tr>
                                <tr>
                                  <td>To</td>
                                  <td>:</td>
                                  <td>{$db_book[0].vToPlace}</td>
                                </tr>
                              </table>
                            </td>
													</tr>
													<tbody><tr>
														<td align="left">Main Trips Details</td>
														<td> : </td>
														<td align="left" style="padding-left:12px;"><a href="{$tconfig.tpanel_url}/index.php?file=ri-rideslist&mode=edit&iRideId={$db_book[0].iRideId}">{$db_book[0].vMainRidePlaceDetails}</a></td>
													</tr>
													
                          <tr>
														<td align="left" valign="top">Sender Details&nbsp;</td>
														<td valign="top"> : </td>
														<td align="left"><!--<a href="index.php?file=m-member&mode=edit&iMemberId={$db_book[0].iBookerId}">{$db_book[0].vBookerFirstName} {$db_book[0].vBookerLastName}</a>-->
                              <table>
                                <tr>
                                  <td>Name</td>
                                  <td>:</td>
                                  <td>
                                     <a href="index.php?file=m-member&mode=edit&iMemberId={$db_book[0].iBookerId}">{$db_book[0].vBookerFirstName} {$db_book[0].vBookerLastName}</a>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Email</td>
                                  <td>:</td>
                                  <td>{$db_book[0].vBookerEmail}</td>
                                </tr>
                                <tr>
                                  <td>Phone</td>
                                  <td>:</td>
                                  <td>{$db_book[0].vBookerPhone}</td>
                                </tr>
                                
                              </table>
                            </td>
													</tr>
													<tr>
														<td align="left" valign="top">Traveler Details&nbsp;</td>
														<td valign="top"> : </td>
														<td align="left">
                              <table>
                                <tr>
                                  <td>Name</td>
                                  <td>:</td>
                                  <td>
                                     <a href="index.php?file=m-member&mode=edit&iMemberId={$db_book[0].iDriverId}">{$db_book[0].vDriverFirstName} {$db_book[0].vDriverLastName}</a>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Email</td>
                                  <td>:</td>
                                  <td>{$db_book[0].vDriverEmail}</td>
                                </tr>
                                <tr>
                                  <td>Phone</td>
                                  <td>:</td>
                                  <td>{$db_book[0].vDriverPhone}</td>
                                </tr>
                                
                              </table>
                            </td>
													</tr>
													
													<!--<tr>
														<td align="left">No. Of Seats&nbsp;</td>
														<td> : </td>
														<td align="left">{$db_book[0].iNoOfSeats}</td>
													</tr>--->
													
													<tr>
														<td align="left">Document&nbsp;</td>
														<td> : </td>
														<td align="left">{$db_book[0].vBookerCurrencyCode} {$db_book[0].fDocumentPrice}</td>
													</tr>
													<tr>
														<td align="left">Box&nbsp;</td>
														<td> : </td>
														<td align="left">{$db_book[0].vBookerCurrencyCode} {$db_book[0].fBoxPrice}</td>
													</tr>
													<tr>
														<td align="left">Luggage&nbsp;</td>
														<td> : </td>
														<td align="left">{$db_book[0].vBookerCurrencyCode} {$db_book[0].fLuggagePrice}</td>
													</tr>
													<tr>
														<td align="left">Commission&nbsp;</td>
														<td> : </td>
														<td align="left">{$db_book[0].vBookerCurrencyCode} {$db_book[0].fCommission}</td>
													</tr>
													<tr>
														<td align="left">Vat&nbsp;</td>
														<td> : </td>
														<td align="left">{$db_book[0].vBookerCurrencyCode} {$db_book[0].fVat}</td>
													</tr>
													<tr>
														<td align="left">Amount&nbsp;</td>
														<td> : </td>
														<td align="left">{$db_book[0].vBookerCurrencyCode} {$db_book[0].fAmount}</td>
													</tr>
													<tr>
														<td align="left">Booking Date&nbsp;</td>
														<td> : </td>
														<td align="left">{$generalobj->DateTime($db_book[0].dBookingDate,9)} ({$generalobj->DateTime($db_book[0].dBookingTime,12)})</td>
													</tr>
													{if $smarty.const.PAYMENT_OPTION eq 'PayPal'}
                          <tr>
														<td align="left">Paypal Transaction Id of Sender&nbsp;</td>
														<td> : </td>
														<td align="left">{$db_book[0].vTransactionId}</td>
													</tr>
													<tr>
														<td align="left">Sender Payment Status&nbsp;</td>
														<td> : </td>
                            <td align="left">{if $db_book[0].eBookerPaymentPaid eq Yes} Paid {else} Unpaid {/if}</td>
													</tr>
                          <tr>
														<td align="left">Paypal Transaction Id of Traveler&nbsp;</td>
														<td> : </td>
														<td align="left">{$db_book[0].vDriverPaymentTransactionId}</td>
													</tr>
													<tr>
														<td align="left">Traveler Payment Status&nbsp;</td>
														<td> : </td>
														<td align="left">{if $db_book[0].eDriverPaymentPaid eq Yes} Paid {else} Unpaid {/if}</td>
													</tr>
													<tr>
														<td align="left">Traveler Payment Vie&nbsp;</td>
														<td> : </td>
														<td align="left">{$db_book[0].eDriverPaymentBy}</td>
													</tr>
													{/if}
													</tbody>
                          </table>
												</td>
												{if $db_book[0].eStatus eq 'Cencelled'}
                        <td width="1%"></td>
												<td valign="top" width="52%">
													<table border="0" style="border: 1px solid #ccc;" cellpadding="1" cellspacing="1" width="100%">
													<tbody>
                          <tr>
    												<td colspan="3" width="100%"  class="headingbg wmatterbold" bgcolor="#E6E6E6" height="22">&nbsp;<b><span style="color:#000000">Cancellation Details</span></b></td>
    											</tr>
                          <tr>
														<td width="24%" valign="top">Cancelled By</td>
														<td width="1%" valign="top"> : </td>
														{if $db_book[0].eCancelBy eq 'Passenger'}
                            <td width="75%" valign="top">{$db_book[0].vBookerFirstName} {$db_book[0].vBookerLastName} (Sender)</td>
                            {else}
                            <td width="75%" valign="top">{$db_book[0].vDriverFirstName} {$db_book[0].vDriverLastName} (Traveler)</td>
                            {/if}
													</tr>
													<tr>
														<td width="24%" valign="top">Cancellation On Date</td>
														<td width="1%" valign="top"> : </td>
														<td width="75%" valign="top">{$generalobj->DateTime($db_book[0].dCancelDate,7)}</td>
													</tr>
													<tr>
														<td width="24%" valign="top">Cancellation Reason</td>
														<td width="1%" valign="top"> : </td>
														<td width="75%" valign="top">{$db_book[0].tCancelReason}</td>
													</tr>
													{if $smarty.const.PAYMENT_OPTION eq 'PayPal'}
                          <tr>
    												<td colspan="3" width="100%"  class="headingbg wmatterbold" bgcolor="#E6E6E6" height="22">&nbsp;<b><span style="color:#000000">Sender Refund</span></b></td>
    											</tr>                           
												  <form name="frmpassengerpay" id="frmpassengerpay" method="post" action="{$tconfig.tpanel_url}/index.php?file=bo-bookings_a">
                          <input type="hidden" name="action" id="action" value="pay_ref_pass">
                          <input type="hidden" name="iBookingId_pass" id="iBookingId_pass" value="{$iBookingId}">
                          <tr>
														<td width="24%" valign="top">Refund Amount</td>
														<td width="1%" valign="top"> : </td>
														<td width="75%" valign="top"><b>{$db_book[0].vBookerCurrencyCode} {$db_book[0].fPassengerRefundAmount}</b></td>
													</tr> 													
													<tr>
														<td width="24%" valign="top">Refund Via</td>
														<td width="1%" valign="top"> : </td>
														<td width="75%" valign="top">
                            <span style="float: left;width: 144px;"><b>Payment Email (Paypal)  </b></span>: {$db_passenger_payment[0].vPaymentEmail}<br>
                            <b>Or</b><br>
                            <span style="float: left;width: 144px;"><b>Account Holder Name</b></span>: {$db_passenger_payment[0].vBankAccountHolderName}<br>
                            <span style="float: left;width: 144px;"><b>Account Number (IBAN) </b></span>: {$db_passenger_payment[0].vAccountNumber}<br>
                            <span style="float: left;width: 144px;"><b>Name of Bank  </b></span>: {$db_passenger_payment[0].vBankName}<br>
                            <span style="float: left;width: 144px;"><b>Bank Location  </b></span>: {$db_passenger_payment[0].vBankLocation}<br>
                            <span style="float: left;width: 144px;"><b>BIC/SWIFT Code  </b></span>: {$db_passenger_payment[0].vBIC_SWIFT_Code}<br>  
                            </td>
													</tr>
													<tr>
														<td width="24%" valign="top">Refund Paid</td>
														<td width="1%" valign="top"> : </td>
														<td width="75%" valign="top"><b>{$db_book[0].ePassengerRefundPaid}</b></td>
													</tr>
													<tr>
														<td width="24%" valign="top">Refund Date</td>
														<td width="1%" valign="top"> : </td>
														<td width="75%" valign="top"><b>{$generalobj->DateTime($db_book[0].dPassengerRefundDate,7)}</b></td>
													</tr>
													<tr>
														<td width="24%" valign="top">Refund Paid Via</td>
														<td width="1%" valign="top"> : </td>
														<td width="75%" valign="top">
                            <select name="ePassengerRefundBy" id="ePassengerRefundBy">
                              <option value="">Select</option>
                              <option value="Paypal" {if $db_book[0].ePassengerRefundBy eq 'Paypal'} selected {/if}>Paypal</option>
                              <option  value="Bank" {if $db_book[0].ePassengerRefundBy eq 'Bank'} selected {/if}>Bank</option>
                            </select>
                            </td>
													</tr>
                          <tr>
														<td width="24%" valign="top">&nbsp;</td>
														<td width="1%" valign="top"> &nbsp; </td>
														<td width="75%" valign="top">
                            <input type="checkbox" name="emailtopass" id="emailtopass" value="Yes">Send refund mail to Sender
                            </td>
													</tr>													
													<tr>
														<td width="24%" valign="top">&nbsp;</td>
														<td width="1%" valign="top">&nbsp;</td>
                            <td width="75%" valign="top"><input type="button" value="Refund To Sender" class="btn" onClick="chk_pass_ref();" title="Refund To Sender"/></td>														
													</tr>
													</form>
													{if $db_book[0].eCancelBy eq 'Passenger'}
													<form name="frmdriverpay" id="frmdriverpay" method="post" action="{$tconfig.tpanel_url}/index.php?file=bo-bookings_a">
                          <input type="hidden" name="action" id="action" value="pay_ref_driver">
                          <input type="hidden" name="iBookingId_driver" id="iBookingId_driver" value="{$iBookingId}">
                          <tr>
    												<td colspan="3" width="100%"  class="headingbg wmatterbold" bgcolor="#E6E6E6" height="22">&nbsp;<b><span style="color:#000000">Traveler Refund</span></b></td>
    											</tr>
													<tr>
														<td width="24%" valign="top">Refund Amount</td>
														<td width="1%" valign="top"> : </td>
														<td width="75%" valign="top"><b>{$db_book[0].vBookerCurrencyCode} {$db_book[0].fDriverRefundAmount}</b></td>
													</tr>   												
													<tr>
														<td width="24%" valign="top">Refund Via</td>
														<td width="1%" valign="top"> : </td>
														<td width="75%" valign="top">
                            <span style="float: left;width: 144px;"><b>Payment Email (Paypal)  </b></span>: {$db_driver_payment[0].vPaymentEmail}<br>
                            <b>Or</b><br>
                            <span style="float: left;width: 144px;"><b>Account Holder Name</b></span>: {$db_driver_payment[0].vBankAccountHolderName}<br>
                            <span style="float: left;width: 144px;"><b>Account Number (IBAN) </b></span>: {$db_driver_payment[0].vAccountNumber}<br>
                            <span style="float: left;width: 144px;"><b>Name of Bank  </b></span>: {$db_driver_payment[0].vBankName}<br>
                            <span style="float: left;width: 144px;"><b>Bank Location  </b></span>: {$db_driver_payment[0].vBankLocation}<br>
                            <span style="float: left;width: 144px;"><b>BIC/SWIFT Code  </b></span>: {$db_driver_payment[0].vBIC_SWIFT_Code}<br>  
                            </td>
													</tr>
													<tr>
														<td width="24%" valign="top">Refund Paid</td>
														<td width="1%" valign="top"> : </td>
														<td width="75%" valign="top"><b>{$db_book[0].eDriverRefundPaid}</b></td>
													</tr>
													<tr>
														<td width="24%" valign="top">Refund Date</td>
														<td width="1%" valign="top"> : </td>
														<td width="75%" valign="top"><b>{$generalobj->DateTime($db_book[0].dDriverRefundDate,7)}</b></td>
													</tr>
													<tr>
														<td width="24%" valign="top">Refund Paid Via</td>
														<td width="1%" valign="top"> : </td>
														<td width="75%" valign="top">
                            <select name="eDriverRefundBy" id="eDriverRefundBy">
                              <option value="">Select</option>
                              <option value="Paypal" {if $db_book[0].eDriverRefundBy eq 'Paypal'} selected {/if}>Paypal</option>
                              <option value="Bank" {if $db_book[0].eDriverRefundBy eq 'Bank'} selected {/if}>Bank</option>
                            </select>
                            </td>
													</tr>
													<tr>
														<td width="24%" valign="top">&nbsp;</td>
														<td width="1%" valign="top"> &nbsp; </td>
														<td width="75%" valign="top">
                            <input type="checkbox" name="emailtodriver" id="emailtodriver" value="Yes">Send refund mail to Traveler
                            </td>
													</tr>	
                          <tr>
														<td width="24%" valign="top">&nbsp;</td>
														<td width="1%" valign="top">&nbsp;</td>
                            <td width="75%"><input type="button" value="Refund To Traveler" class="btn" onClick="chk_driver_ref();" title="Refund To Traveler"/></td>														
													</tr> 
                          </form>
                          {/if}
                          {/if}												
													</tbody>
                          </table>
												</td>
												{/if}
											</tr>
										</table>
										</td>
										</tr>
										
		            </table>
    <!--<form id="frmadd" name="frmadd" action="index.php?file=bo-bookings_a" method="post">
			<input type="hidden" name="iBookingId" id="iBookingId" value="{$iBookingId}" />
            <input type="hidden" name="action" id="action" value="{$mode}" />
            <input type="hidden" name="sendmail" id="sendmail" value="" />
            
									<table border="0" cellpadding="1" cellspacing="1" width="100%" align="center" class="oderpage">
									<tr>
										<td valign="top">
											<table class="headingbg" style="border: 1px solid #ccc;" border="0" cellpadding="1" cellspacing="1" width="100%" align="center">
										  <tr>
												<td class="headingbg wmatterbold" bgcolor="#309ec3" height="22">&nbsp;<b><span style="color:#000000">Booking Information</span></b></td>
											</tr>
											<tr bgcolor="#ffffff">
												<td valign="top" width="100%" align="center">
													<table border="0" cellpadding="1" cellspacing="1" width="50%">
													<tbody>
                          <tr>
														<td align="left">Booking No&nbsp;</td>
														<td> : </td>
														<td align="left" style="padding-left:12px;">{$db_book[0].vBookingNo}</td>
													</tr>
													<tr>
														<td align="left" valign="top">Trips Details&nbsp;</td>
														<td valign="top"> : </td>
														<td align="left">
                              <table>
                                <tr>
                                  <td>From</td>
                                  <td>:</td>
                                  <td>
                                     {$db_book[0].vFromPlace}
                                  </td>
                                </tr>
                                <tr>
                                  <td>To</td>
                                  <td>:</td>
                                  <td>{$db_book[0].vToPlace}</td>
                                </tr>
                              </table>
                            </td>
													</tr>
													<tbody><tr>
														<td align="left">Main Trip Details</td>
														<td> : </td>
														<td align="left" style="padding-left:12px;"><a href="{$tconfig.tpanel_url}/index.php?file=ri-rideslist&mode=edit&iRideId={$db_book[0].iRideId}">{$db_book[0].vMainRidePlaceDetails}</a></td>
													</tr>
													
                          <tr>
														<td align="left" valign="top">Sender Details&nbsp;</td>
														<td valign="top"> : </td>
														<td align="left">
                              <table>
                                <tr>
                                  <td>Name</td>
                                  <td>:</td>
                                  <td>
                                     <a href="index.php?file=m-member&mode=edit&iMemberId={$db_book[0].iBookerId}">{$db_book[0].vBookerFirstName} {$db_book[0].vBookerLastName}</a>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Email</td>
                                  <td>:</td>
                                  <td>{$db_book[0].vBookerEmail}</td>
                                </tr>
                                <tr>
                                  <td>Phone</td>
                                  <td>:</td>
                                  <td>{$db_book[0].vBookerPhone}</td>
                                </tr>
                                
                              </table>
                            </td>
													</tr>
													<tr>
														<td align="left" valign="top">Traveler Details&nbsp;</td>
														<td valign="top"> : </td>
														<td align="left">
                              <table>
                                <tr>
                                  <td>Name</td>
                                  <td>:</td>
                                  <td>
                                     <a href="index.php?file=m-member&mode=edit&iMemberId={$db_book[0].iDriverId}">{$db_book[0].vDriverFirstName} {$db_book[0].vDriverLastName}</a>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Email</td>
                                  <td>:</td>
                                  <td>{$db_book[0].vDriverEmail}</td>
                                </tr>
                                <tr>
                                  <td>Phone</td>
                                  <td>:</td>
                                  <td>{$db_book[0].vDriverPhone}</td>
                                </tr>
                                
                              </table>
                            </td>
													</tr>
													
													<tr>
														<td align="left">No. Of Seats&nbsp;</td>
														<td> : </td>
														<td align="left">{$db_book[0].iNoOfSeats}</td>
													</tr>
													<tr>
														<td align="left">Amount&nbsp;</td>
														<td> : </td>
														<td align="left">{$db_book[0].vBookerCurrencyCode} {$db_book[0].fAmount}</td>
													</tr>
													<tr>
														<td align="left">Booking Date&nbsp;</td>
														<td> : </td>
														<td align="left">{$generalobj->DateTime($db_book[0].dBookingDate,9)} ({$generalobj->DateTime($db_book[0].dBookingTime,12)})</td>
													</tr>
													<tr>
														<td align="left">Paypal Transaction Id of Sender&nbsp;</td>
														<td> : </td>
														<td align="left">{$db_book[0].vTransactionId}</td>
													</tr>
													<tr>
														<td align="left">Sender Payment Status&nbsp;</td>
														<td> : </td>
                            <td align="left">{if $db_book[0].eBookerPaymentPaid eq Yes} Paid {else} Unpaid {/if}</td>
													</tr>
                          <tr>
														<td align="left">Paypal Transaction Id of Traveler&nbsp;</td>
														<td> : </td>
														<td align="left">{$db_book[0].vDriverPaymentTransactionId}</td>
													</tr>
													<tr>
														<td align="left">Traveler Payment Status&nbsp;</td>
														<td> : </td>
														<td align="left">{if $db_book[0].eDriverPaymentPaid eq Yes} Paid {else} Unpaid {/if}</td>
													</tr>
													<tr>
														<td align="left">Traveler Payment Vie&nbsp;</td>
														<td> : </td>
														<td align="left">{$db_book[0].eDriverPaymentBy}</td>
													</tr>
													</tbody></table>
												</td>
											</tr>
												</table>
										</td>
										</tr>
										
		            </table>
                	</form>
                -->
								    
	                    <table width="100%">
                       <tr>                                 
                        <td style="float:right;" align="right">
                        <input type="button" value=" OK " class="btn" onClick="backtolist();" title="List"/>
                        <input type="button" value="Show Invoice" class="btn" onClick="for_print('{$tconfig.tsite_url}invoice_print.php?id={$iBookingId}&print=yes');" title="Print Booking Invoice"/>
    <!--                     &nbsp;<input type="button" value="Print Commercial Invoice" class="btn" onclick="redirectprint('Commercial');" title="Print Commercial Invoice"/>
                        &nbsp;<input type="button" value="Print Booking List" class="btn" onclick="redirectprint('Packing');" title="Print Packing List"/>
                        &nbsp;<input type="button" value="Delete Booking" class="btnalt" onclick="del_ord();" title="Delete Order"/> 
   -->                     </td>
                        </tr>
                      </table>
							        
	
	</div>
</div>
<form name="frmlist" id="frmlist"  action="index.php?file=bo-bookings_a" method="post">
<input type="hidden" name="action" id="action" value="" />
<input  type="hidden" name="iBookingId" value=""/>
</form>
{literal}
<script>
function for_print(url)
{
    window.open(url,'','width=700,height=900,resizable=yes,scrollbars=yes');
}
function backtolist(){
   window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=bo-bookings&mode=view"
}
/*function redirectprint(type)
{
    var id = document.frmadd.iBookingId.value;
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=bo-concertbook_print_admin&iBookingId="+id+"&print_page=Yes&type="+type;
    return false;
} 
function sendemailto()
{   
    document.getElementById("sendmail").value = 'yes';
    document.frmadd.submit();
   // window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=bo-concertbook_print_admin&iBookingId="+id+"&print_page=Yes";
   // return false;
} */
function edit_paymentstatus(){
   document.frmadd.submit();
}
function del_ord(){
  var r=confirm("Are you sure to want delete this Booking?");
  if (r==true){
    document.frmlist.iBookingId.value = '{/literal}{$iBookingId}{literal}';
    document.frmlist.action.value = 'Delete';
    document.frmlist.submit();	
  }else{
    return false;
  }
}

function chk_pass_ref(){
  if($('#ePassengerRefundBy').val() == ''){
    alert('Please select Refund Paid Via option first'); 
    return false;
  }
  
  document.frmpassengerpay.submit();
}

function chk_driver_ref(){
  if($('#eDriverRefundBy').val() == ''){
    alert('Please select Refund Paid Via option first'); 
    return false;
  }
  
  document.frmdriverpay.submit();
}

function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
{/literal}