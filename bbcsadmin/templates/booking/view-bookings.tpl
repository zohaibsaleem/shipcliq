<link type="text/css" rel="stylesheet" href="{$tconfig.tsite_javascript}dhtmlgoodies_calendar.css" media="screen">
<script type="text/javascript" src="{$tconfig.tsite_javascript}dhtmlgoodies_calendar.js"></script>
<script language="JavaScript" src="{$tconfig.tcp_javascript}jlist.js"></script>
<div class="contentcontainer">
	<div class="headings altheading">
		<h2>Trips Booking</h2>
	</div>
	<div class="contentbox">
    {if $var_msg_new neq ''}
     <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg_new}</p> 
     </div>     
    <div></div>
    {elseif $var_msg neq ''}
    <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg}</p> 
     </div>     
    <div></div>
    {/if}
		<form name="frmsearch" id="frmsearch" action="" method="post">
    <input type="hidden" name="paymenttype" id="paymenttype" value="{$paymenttype}">
    <table width="100%" border="0">
		<tbody>
			<tr>
				<td width="10%"><label for="textfield"><strong>Search:</strong></label></td>
				<td width="10%"><input type="Text" id="keyword" name="keyword" value="{$keyword}"  class="inputbox" /></td>
				<td width="10%" >
					<select name="option" id="option">
						<option  value="vBookingNo" {if $option eq 'vBookingNo'} selected {/if}>Booking No</option>
						<option  value="concat(vBookerFirstName,' ',vBookerLastName)" {if $option eq "concat(vBookerFirstName,\' \',vBookerLastName)"} selected {/if}>Sender Name</option>
						<option  value="concat(vDriverFirstName,' ',vDriverLastName)" {if $option eq "concat(vDriverFirstName,\' \',vDriverLastName)"} selected {/if}>Traveler Name</option>
			<!--			<option value="b.ePaymentPaid">Payment Status</option> -->
					</select>
				</td>
				<td width="60%">
      <!--    <input type="button" value="Search" class="btnalt" id="Search" name="Search" title="Search" onclick="Searchoption();"/>
          &nbsp;
          <input type="button" value="Export Trips Booking" onclick="return export_ride();" class="btnalt" /> -->
          </td>
			</tr>	
      <tr>
        <td width="10%"><label for="textfield"><strong>Date :</strong></label></td>
        <td width="10%"> 
          <input type="text" Readonly id="dFDate" name="dFDate" style="width:100px;" class="inputbox" value="{$startdate}" lang="" title="Start Date"/>&nbsp;<img src="{$tconfig.tsite_images}cal-icon.gif" style="cursor:pointer" onclick="displayCalendar(document.getElementById('dFDate'),'yyyy-mm-dd',this)" /> &nbsp;
          To &nbsp; <input type="text" Readonly id="dTDate" name="dTDate" style="width:100px;" class="inputbox" value="{$enddate}" lang="" title="End Date"/>&nbsp;<img src="{$tconfig.tsite_images}cal-icon.gif" style="cursor:pointer" onclick="displayCalendar(document.getElementById('dTDate'),'yyyy-mm-dd',this)" />
        </td>
        <td width="10%"></td>
        <td width="60%"></td>
      </tr>
      <tr>
        <td></td>
        <td>
         <input type="button" value="Search" class="btnalt" id="Search" name="Search" title="Search" onclick="Searchoption();"/>
          &nbsp;
          <input type="button" value="Export Trips Booking" onclick="return export_ride();" class="btnalt" />
        </td>
      </tr>
      {if $smarty.const.PAYMENT_OPTION eq 'PayPal'}
			<tr>
				<td colspan="7" align="center">
					{*$AlphaBox*}
          <input type="button" value="Unpaid" onclick="checkvalid(1);" class="{if $paymenttype eq 'No'}btn{else}btnalt{/if}" />
          <input type="button" value="Paid" onclick="checkvalid(2);" class="{if $paymenttype eq 'Yes'}btn{else}btnalt{/if}" />
          <input type="button" value="Both" onclick="checkvalid(3);" class="{if $paymenttype eq 'both'}btn{else}btnalt{/if}" />
				</td>
			</tr>
			{/if}
		</tbody>			
		</table> 
    </form>
   
    <form name="frmlist" id="frmlist"  action="index.php?file=bo-bookings_a" method="post">
   	<table width="100%" border="0">
		<input type="hidden" name="action" id="action" value="" />
    <input type="hidden" name="iBookingId" value=""/>
    <thead>
			<tr>
				<th width="11%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=bo-bookings&mode=view&sortby=1&order={if $order eq 0}1{else}0{/if}');">Booking No</a>{if $sortby eq 1}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>
				<th width="12%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=bo-bookings&mode=view&sortby=2&order={if $order eq 0}1{else}0{/if}');">Sender Name</a>{if $sortby eq 2}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>
				<th width="15%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=bo-bookings&mode=view&sortby=5&order={if $order eq 0}1{else}0{/if}');">Traveler Name</a>{if $sortby eq 5}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>
				<!--<th width="10%">No of Seats</th>
				<th width="10%">Trips</th> 
				<th width="10%">Amount</th> -->
				<!--<th width="10%">Categories</th>-->
				<th width="10%">Document</th>
				<th width="10%">Box</th>
				<th width="10%">Luggage</th>
				<th width="10%">Platform fee</th>
				<th width="10%">Vat</th>
				<th width="10%">Amount</th>
				<th width="17%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=bo-bookings&mode=view&sortby=4&order={if $order eq 0}1{else}0{/if}');">Booking Date</a>{if $sortby eq 4}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/down_active.png{else}icons/up_active.png{/if}">{/if}</th>
		<!--		<th width="15%">SenderPaymentStatus</th> -->
				<th width="15%">PaymentStatus</th>
				<th>Invoice</td>
				<th width="5%">Action</th>
				<th width="5%"><input type="checkbox" id="check_all" name="check_all" onclick="checkAll(document.frmlist);"/></th>
			</tr>
		</thead>
		<tbody>
    {if $db_book_all|@count gt 0}            
		{section name=i loop=$db_book_all}
     <tr>
			<td {if $db_book_all[i].eStatus eq 'Cencelled'} style="background:none repeat scroll 0 0 #F6CECE;"{/if}><a href="{$tconfig.tpanel_url}/index.php?file=bo-bookings&mode=edit&iBookingId={$db_book_all[i].iBookingId}" title="">{$db_book_all[i].vBookingNo}</a></td>
			<td {if $db_book_all[i].eStatus eq 'Cencelled'} style="background:none repeat scroll 0 0 #F6CECE;"{/if}><a href="{$tconfig.tpanel_url}/index.php?file=m-member&mode=edit&iMemberId={$db_book_all[i].iBookerId}">{$db_book_all[i].vBookerFirstName} {$db_book_all[i].vBookerLastName}</a></td>
			<td {if $db_book_all[i].eStatus eq 'Cencelled'} style="background:none repeat scroll 0 0 #F6CECE;"{/if}><a href="{$tconfig.tpanel_url}/index.php?file=m-member&mode=edit&iMemberId={$db_book_all[i].iDriverId}">{$db_book_all[i].vDriverFirstName} {$db_book_all[i].vDriverLastName}</a></td>
			<!-- <td {if $db_book_all[i].eStatus eq 'Cencelled'} style="background:none repeat scroll 0 0 #F6CECE;"{/if}><div style="text-align:center;">{$db_book_all[i].iNoOfSeats}</div></td>
			<td><a href="{$tconfig.tpanel_url}/index.php?file=ri-rideslist&mode=edit&iRideId={$db_book_all[i].iRideId}">view</a></td> 
			<td {if $db_book_all[i].eStatus eq 'Cencelled'} style="background:none repeat scroll 0 0 #F6CECE;"{/if}>{$db_book_all[i].vBookerCurrencyCode} {$db_book_all[i].fAmount}</td> -->
			<td {if $db_book_all[i].eStatus eq 'Cencelled'} style="background:none repeat scroll 0 0 #F6CECE;"{/if}>
			   {$db_book_all[i].vBookerCurrencyCode} {$db_book_all[i].fDocumentPrice}
          <!--{if $db_book_all[i].fDocumentPrice gt '0.00'}
					   <!-- <img src="{$tconfig.tsite_images}document-icon.png">--**
					   {$db_book_all[i].vBookerCurrencyCode} {$db_book_all[i].fDocPrice} 
					{else if $db_book_all[i].fDocumentPrice lt '0.00' or $db_book_all[i].fDocumentPrice eq '0.00'}
					    --
				  {/if}-->  
			</td>
			<td {if $db_book_all[i].eStatus eq 'Cencelled'} style="background:none repeat scroll 0 0 #F6CECE;"{/if}>
			   {$db_book_all[i].vBookerCurrencyCode} {$db_book_all[i].fBoxPrice}			   
			   <!--{if $db_book_all[i].fBoxPrice gt '0.00'}
					<!-- <img src="{$tconfig.tsite_images}box-icon.png">--**
					Box ({$db_book_all[i].vBookerCurrencyCode} {$db_book_all[i].fBoxPrice}) 
				  {/if}--->
			</td>
			<td {if $db_book_all[i].eStatus eq 'Cencelled'} style="background:none repeat scroll 0 0 #F6CECE;"{/if}>
			     {$db_book_all[i].vBookerCurrencyCode} {$db_book_all[i].fLuggagePrice}
			   <!--{if $db_book_all[i].fLuggagePrice gt '0.00'}
					<!-- <img src="{$tconfig.tsite_images}luggage-icon.png"> --**
					Luggage ({$db_book_all[i].vBookerCurrencyCode} {$db_book_all[i].fLuggagePrice}) 
				{/if} -->
			</td>
			<td {if $db_book_all[i].eStatus eq 'Cencelled'} style="background:none repeat scroll 0 0 #F6CECE;"{/if}>
			     {$db_book_all[i].vBookerCurrencyCode} {$db_book_all[i].fCommission}
			</td>
			<td {if $db_book_all[i].eStatus eq 'Cencelled'} style="background:none repeat scroll 0 0 #F6CECE;"{/if}>
			     {$db_book_all[i].vBookerCurrencyCode} {$db_book_all[i].fVat}
			</td>
			<td {if $db_book_all[i].eStatus eq 'Cencelled'} style="background:none repeat scroll 0 0 #F6CECE;"{/if}>{$db_book_all[i].vBookerCurrencyCode} {$db_book_all[i].fAmount}</td>			
			<td {if $db_book_all[i].eStatus eq 'Cencelled'} style="background:none repeat scroll 0 0 #F6CECE;"{/if}>{$generalobj->DateTime($db_book_all[i].dBookingDate,9)}  &nbsp;({$generalobj->DateTime($db_book_all[i].dBookingTime,12)})</td>
			<td {if $db_book_all[i].eStatus eq 'Cencelled'} style="background:none repeat scroll 0 0 #F6CECE;"{/if}><div style="text-align:center;">{if $db_book_all[i].eDriverPaymentPaid eq Yes} Paid {else} Unpaid {/if}</div></td>
		 <!-- <td><a href="javascript:void(0);" onClick="for_print('{$tconfig.tsite_url}invoice_print.php?id={$db_book_all[i].iBookingId}&print=yes');">Invoice</a></td> -->
		  <td {if $db_book_all[i].eStatus eq 'Cencelled'} style="background:none repeat scroll 0 0 #F6CECE;"{/if}><a href="javascript:void(0);" onClick="for_print('{$tconfig.tsite_url}invoice_print.php?id={$db_book_all[i].iBookingId}&print=yes');">view</a></td>
      <td {if $db_book_all[i].eStatus eq 'Cencelled'} style="background:none repeat scroll 0 0 #F6CECE;"{/if}><div style="text-align:center;"><a href="javascript:void(0);" title="Delete" onclick="MakeAction('{$db_book_all[i].iBookingId}','Delete');"><img src="{$tconfig.tpanel_img}icons/icon_delete.png" title="Delete" /></a></div></td>			
			<td {if $db_book_all[i].eStatus eq 'Cencelled'} style="background:none repeat scroll 0 0 #F6CECE;"{/if}><input name="iBookingId[]" type="checkbox" id="iId" value="{$db_book_all[i].iBookingId}"/></td>
	   </tr>
    {/section}
    {else}
     <tr>
			<td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No Record Found.</td>
	   </tr>
    {/if}
		</tbody>
		</table>
    </form>
	
  	<div class="extrabottom">
			<div class="bulkactions">
				<select name="newaction" id="newaction">
					<option value="">Select Action</option>
					<option value="Deletes">Make Delete</option>
					<option value="Show All">Show All</option>
				</select>
				<input type="submit" value="Apply" class="btn" onclick="return Doaction(document.getElementById('newaction').value,'bo-bookings',document.frmlist);"/>
			</div>
		</div>
        <div>
          <div class="pagination">
          {if $db_book_all|@count gt 0}
	          <span class="switch" style="float: left;">{$recmsg}</span>
	        {/if}
           </div>
          {$page_link}
        </div>     	
		<div style="clear: both;"></div></div>
</div>
{literal}
<script>
function for_print(url)
{
    window.open(url,'','width=700,height=900,resizable=yes,scrollbars=yes');
}
function Searchoption(){
    document.frmsearch.action="";
    if(document.frmsearch.dTDate.value!='' && document.frmsearch.dFDate.value!='')
    if(document.frmsearch.dTDate.value < document.frmsearch.dFDate.value){
  		alert("From date should be lesser than To date.")
  		document.frmsearch.dFDate.select();
		return false;
  	}
    
    document.getElementById('frmsearch').submit();
}
function checkvalid(val){
  document.frmsearch.action="";
  if(val==1){
     document.frmsearch.paymenttype.value='No';
     document.getElementById('frmsearch').submit();
  }else if(val==2){
     document.frmsearch.paymenttype.value='Yes';
     document.getElementById('frmsearch').submit();
  }else{
     document.frmsearch.paymenttype.value='both';
     document.getElementById('frmsearch').submit();
  }
}
function AlphaSearch(val){
    var alphavalue = val;
    var file = 'bo-bookings';
    window.location="index.php?file="+file+"&alp="+alphavalue+"&mode=view";
    return false;
}
function MakeAction(loopid,type){
  var r=confirm("Are you sure to want delete this Booking?");
  if (r==true){
    document.frmlist.iBookingId.value = loopid;
    document.frmlist.action.value = type;
    document.frmlist.submit();	
  }else{
    return false;
  }
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
function for_print(url)
{
    window.open(url,'','width=700,height=900,resizable=yes,scrollbars=yes');
}
function export_ride(){
    document.frmsearch.action="index.php?file=bo-export_rides_booking&mode=edit";
    document.getElementById('frmsearch').submit();
//    window.location="index.php?file=bo-export_rides_booking&mode=edit";
    return false;
}
</script>
{/literal}