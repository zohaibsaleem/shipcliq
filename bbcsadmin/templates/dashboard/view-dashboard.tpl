<div class="contentcontainer">
	<div class="headings altheading">
		<h2>Site Map</h2>
	</div>
	<div class="contentbox" style="border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
    margin-bottom: 15px;">
		<div>
			{if in_array('ad-administrator', $modlist) or in_array('ad-languagemaster', $modlist)}
			<div class="ControlPanelLeft">
				<h1>Administrator</h1>
				<ul>
					{if in_array('ad-administrator', $modlist)}
					<li><a href="{$tconfig.tpanel_url}/index.php?file=ad-administrator&mode=view">Administrator</a></li>
					{/if}
					{if in_array('ad-languagemaster', $modlist)}
					<li><a class="last" ref="{$tconfig.tpanel_url}/index.php?file=ad-languagemaster&mode=view">Language</a></li>
					{/if}
				</ul>
			</div> 
			{/if}
			{if in_array('m-member', $modlist) or in_array('m-member_messages', $modlist)}
			<div class="ControlPanelLeft">
				<h1>Members Management</h1>
				<ul>
					{if in_array('m-member', $modlist)}
					<li><a href="{$tconfig.tpanel_url}/index.php?file=m-member&mode=view">Members List</a></li>
					{/if}
					{if in_array('m-member_messages', $modlist)}
					<li><a class="last" href="{$tconfig.tpanel_url}/index.php?file=m-member_messages&mode=view">Message Records</a></li>
					{/if}
				</ul>
			</div> 
			{/if}
			{if in_array('ri-rideslist', $modlist) or in_array('bo-bookings', $modlist) or in_array('ri-member_payment', $modlist)}
			<div class="ControlPanelLeft">
				<h1>Trip Management</h1>
				<ul>
					<li><a class="last" href="{$tconfig.tpanel_url}/index.php?file=ri-rideslist&mode=view&paymenttype=all">Trip List</a></li>
					{if $smarty.const.PAYMENT_OPTION neq 'Contact'}
						{if in_array('bo-bookings', $modlist)}
					<li><a href="{$tconfig.tpanel_url}/index.php?file=bo-bookings&mode=view">Trip Booking</a></li>
					{/if}
					{if in_array('ri-member_payment', $modlist)}
					{if $smarty.const.PAYMENT_OPTION eq 'PayPal' and in_array('ri-member_payment', $modlist)}
					<li><a class="last" href="{$tconfig.tpanel_url}/index.php?file=ri-member_payment&mode=view">Pay To Traveler</a></li>
					{/if}{/if}
					{/if}
				</ul>
			</div>
			{/if}
			
			
			
			{if in_array('r-member_reports', $modlist) or in_array('r-rides_reports', $modlist) or in_array('r-payment_reports', $modlist) or in_array('r-money_reports', $modlist)}
			<div class="ControlPanelLeft">
				<h1>Report</h1>
				<ul>
					{if in_array('r-member_reports', $modlist)}
					<li><a href="{$tconfig.tpanel_url}/index.php?file=r-member_reports&mode=view">Members Report</a></li>
					{/if}
					{if in_array('r-rides_reports', $modlist)}
					<li><a href="{$tconfig.tpanel_url}/index.php?file=r-rides_reports&mode=view">Trip Posted Report</a></li>
					{/if}
					{if in_array('r-money_reports', $modlist)}
					<li><a class="last" href="{$tconfig.tpanel_url}/index.php?file=r-money_reports&mode=view">Money Report</a></li>
					{/if}
					{if in_array('r-payment_reports', $modlist)}
					<li><a class="last" href="{$tconfig.tpanel_url}/index.php?file=r-payment_reports&mode=view">Payment Report</a></li>
					{/if}
				</ul>
			</div>
			{/if}
			{if in_array('m-member_stories', $modlist) or in_array('u-media_partners', $modlist) or in_array('u-email_templates', $modlist) or in_array('u-faqcategory', $modlist)}
			<div class="ControlPanelLeft">
				<h1>Utility</h1>
				<ul>
					{if in_array('m-member_stories', $modlist)}	
					<li><a href="{$tconfig.tpanel_url}/index.php?file=m-member_stories&mode=view">Members Stories</a></li>
					{/if}
					{if in_array('u-media_partners', $modlist)}
					<li><a href="{$tconfig.tpanel_url}/index.php?file=u-media_partners&mode=view"  title="">Media Partners</a></li>
					{/if}
					{if in_array('u-email_templates', $modlist)}
					<li><a href="{$tconfig.tpanel_url}/index.php?file=u-email_templates&mode=view"  title="">Email templates</a></li>
					{/if}
					{if in_array('u-faqcategory', $modlist)}
					<li><a href="{$tconfig.tpanel_url}/index.php?file=u-faqcategory&mode=view" title="">FAQ Category</a></li>
					{/if}					
				</ul>
			</div>
			{/if}
			{if in_array('to-homebanners', $modlist) or in_array('to-banners', $modlist) or in_array('u-currency', $modlist) or in_array('u-newsletter_subscriber', $modlist) or in_array('u-faq', $modlist)}
			<div class="ControlPanelLeft">
				<h1>Utility</h1>
				<ul>
					{if in_array('u-faq', $modlist)}
					<li><a class="last" href="{$tconfig.tpanel_url}/index.php?file=u-faq&mode=view" title="">FAQ</a></li>
					{/if}
					{if in_array('to-homebanners', $modlist)}
					<li><a href="{$tconfig.tpanel_url}/index.php?file=to-homebanners&mode=view">Home Banners</a></li> 		
					{/if}
					{if in_array('to-banners', $modlist)}
					<li><a href="{$tconfig.tpanel_url}/index.php?file=to-banners&mode=view">Adv. Banners</a></li> 		
					{/if}
					{if in_array('u-currency', $modlist)}
					<li><a href="{$tconfig.tpanel_url}/index.php?file=u-currency&mode=view">Currency Rates</a></li>
					{/if}
					{if in_array('u-newsletter_subscriber', $modlist)}
					<li><a href="{$tconfig.tpanel_url}/index.php?file=u-newsletter_subscriber&mode=view">Newsletter Subscriber</a></li> 				 							
					{/if}
					<!---{if in_array('u-preferences', $modlist)}
						<li><a class="last" href="{$tconfig.tpanel_url}/index.php?file=u-preferences&mode=view">Manage Preferences</a></li>
					{/if}--->
				</ul>
			</div>
			{/if}
			{if in_array('l-country', $modlist) or in_array('l-state', $modlist) or in_array('l-city', $modlist)}
			<div class="ControlPanelLeft">
				<h1>Localization</h1>
				<ul>
					{if in_array('l-country', $modlist)}
					<li><a href="{$tconfig.tpanel_url}/index.php?file=l-country&mode=view">Country</a></li>
					{/if}
					{if in_array('l-state', $modlist)}
					<li><a href="{$tconfig.tpanel_url}/index.php?file=l-state&mode=view">Province</a></li>
					{/if}
					{if in_array('l-city', $modlist)} 
					<li><a class="last" href="{$tconfig.tpanel_url}/index.php?file=l-city&mode=view">City</a></li>
					{/if}
				</ul>
			</div>
			{/if}
			{if in_array('to-generalsettings', $modlist) or in_array('to-languagelabel', $modlist) or in_array('pg-staticpages', $modlist)}
			<div class="ControlPanelLeft">
				<h1>Settings</h1>
				<ul>
					{if in_array('to-generalsettings', $modlist)}
					<li><a href="{$tconfig.tpanel_url}/index.php?file=to-generalsettings&mode=edit">General Configuration</a></li>
					{/if}
					{if in_array('to-languagelabel', $modlist)}
					<li><a href="{$tconfig.tpanel_url}/index.php?file=to-languagelabel&mode=view">Language Label</a></li>
					{/if}
					{if in_array('pg-staticpages', $modlist)}
					<li><a class="last" href="{$tconfig.tpanel_url}/index.php?file=pg-staticpages&mode=view">Static Pages</a></li>
					{/if}
				</ul>
			</div>
			{/if}
			<div style=" clear:both"></div>
		</div>
		<br><br><br>
		<div style="clear:both"></div>
	</div>  
</div>
<div class="contentcontainer">
	<div class="headings altheading">
		<h2>Update statistics</h2>
	</div>
	<div class="contentbox">
		<table width="100%">
			<thead>
				<!--
					<tr>
					<th width="33%">Name</th>
					<th width="33%">Email</th>
					<th width="33%">Registration Date</th>
					</tr>
				-->
			</thead>
			<tbody>
				<tr class="alt">
					<td width="20%">Total Admin</td>
					<td width="1%">:</td>
					<td width="79%">{$totadmin}</td>
				</tr>			
				<tr class="alt">
					<td>Total Active Members </td>
					<td>:</td>
					<td>{$totstudent}</td>
				</tr>
				<tr class="alt">
					<td>Total Rides Offered </td>
					<td>:</td>
					<td>{$totevent}</td>
				</tr>
				
				
			</tbody>
		</table>
		<div style="clear: both;"></div>
	</div>
</div>




