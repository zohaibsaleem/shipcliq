<?php
error_reporting(1);
session_start();
defined( '_TEXEC' ) or die( 'Restricted access' );
$parts = explode( DS, TPATH_BASE );
define( 'TPATH_ROOT',			TPATH_BASE );
define( 'TPATH_ADMINISTRATOR', 	TPATH_ROOT.DS.'shropadmin' );
define( 'TPATH_LIBRARIES', 		TPATH_ROOT.DS.'libraries' );
define( 'TPATH_CLASS_APP', 		TPATH_ROOT.DS.'libraries'.DS.'application'.DS);
define( 'TPATH_TEMPLATES', 		TPATH_ROOT.DS.'templates' );
define( 'TPATH_TEMPLATES_C', 		TPATH_ROOT.  DS.'templates_c' );
define( 'TPATH_MODULES', 		TPATH_ROOT.DS.'modules' );
define( 'TPATH_ADMIN_TEMPLATES', 		TPATH_ADMINISTRATOR.DS.'templates' );
define( 'TPATH_ADMIN_TEMPLATES_C', 		TPATH_ADMINISTRATOR.DS.'templates_c' );
define( 'TPATH_ADMIN_MODULES', 		TPATH_ADMINISTRATOR.DS.'modules' );
define( 'TPATH_CLASS_DATABASE', 		TPATH_ROOT.DS.'libraries'.DS.'database/' );
define( 'TPATH_CLASS_GEN', 		TPATH_ROOT.DS.'libraries'.DS.'general/' );
define( 'TPATH_PUBLIC_HTML', 	TPATH_ROOT.DS.'public_html' );
define( 'TPATH_CACHE', 		TPATH_PUBLIC_HTML.DS.'cache' );

define( 'TPATH_UPLOADS', 	TPATH_ROOT.DS.'public_html/uploads' );
define( 'TPATH_UPLOADS_SERIES', 	TPATH_UPLOADS.DS.'series' );
define( 'TPATH_UPLOADS_EPISODES', 	TPATH_UPLOADS.DS.'episodes' );


if($_SERVER["HTTP_HOST"] == "localhost")
{
	define( 'TSITE_SERVER','localhost');
	define( 'TSITE_DB','mywebdes_shropshire');
	define( 'TSITE_USERNAME','mywebdes_project');
	define( 'TSITE_PASS','bH}t}GU329P7');
}
else
{
	define( 'TSITE_SERVER','localhost');
	define( 'TSITE_DB','mywebdes_shropshire');
	define( 'TSITE_USERNAME','mywebdes_project');
	define( 'TSITE_PASS','bH}t}GU329P7');
}

if(!isset($obj))
{
	require_once(TPATH_CLASS_DATABASE."class.dbquery.php");
	$obj=	new DBConnection(TSITE_SERVER, TSITE_DB, TSITE_USERNAME,TSITE_PASS);
}
if(!isset($generalobj)){
	require_once(TPATH_CLASS_GEN."class.general.php");
	$generalobj=new General();
}

$generalobj->getGeneralVar();
?>