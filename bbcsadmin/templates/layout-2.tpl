<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: {$admin_title} ::</title>
<link rel="icon" href="{$tconfig.tsite_url}favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="{$tconfig.tsite_url}favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" href="{$tconfig.tsite_url}favicon.ico">
<link href="{$tconfig.tsite_stylesheets}cp/layout.css" rel="stylesheet" type="text/css" />
<link href="{$tconfig.tsite_stylesheets}cp/wysiwyg.css" rel="stylesheet" type="text/css" />
<link href="{$tconfig.tsite_stylesheets}cp/login.css" rel="stylesheet" type="text/css" />
 <!-- Theme Start -->
<link href="{$tconfig.tpanel_theme}styles.css" rel="stylesheet" type="text/css" />
<!-- Theme End -->
</head>
<body id="homepage">
		{include file="$include_template"}
</body>
</html>