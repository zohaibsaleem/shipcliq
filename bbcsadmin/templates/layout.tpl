<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<!--meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /--->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>:: {$admin_title}  ::</title>
		<link rel="icon" href="{$tconfig.tsite_url}favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="{$tconfig.tsite_url}favicon.ico" type="image/x-icon">
		<link rel="apple-touch-icon" href="{$tconfig.tsite_url}favicon.ico">
		<link href="{$tconfig.tsite_stylesheets}cp/layout.css" rel="stylesheet" type="text/css" />
		<link href="{$tconfig.tsite_stylesheets}cp/wysiwyg.css" rel="stylesheet" type="text/css" />
		<link href="{$tconfig.tsite_stylesheets}cp/login.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="{$tconfig.tsite_stylesheets}cp/superfish.css" media="screen">  
		<!-- Theme Start -->
		<link href="{$tconfig.tpanel_theme}styles.css" rel="stylesheet" type="text/css" /> 
		<script type="text/javascript" src="{$tconfig.tsite_javascript}jquery-1.6.2.js"></script>
		<script type="text/javascript" src="{$tconfig.tsite_javascript}jquery.min.js"></script>
		<script type="text/javascript" src="{$tconfig.tcp_javascript}validate.js"></script>
		
		<script src="{$tconfig.tsite_javascript}hoverIntent.js"></script>
		<!--<script src="{$tconfig.tsite_javascript}superfish.js"></script>
		Theme End -->
		<!--	<script language="JavaScript" src="{$tconfig.tcp_javascript}jquery-latest.js"></script> -->
		<!--		<script type='text/javascript' src='{$tconfig.tsite_javascript}jquery.min.js'></script> --> 
		{literal}
		<script>
			var	site_url = '{/literal}{$tconfig.tsite_url}{literal}';
			//jQuery(function(){
			//	jQuery('#example').superfish();
			//});
		</script>
		{/literal}
	</head>
	<body id="homepage">
		
		<!-- Header Start -->
		<div id="header">
			{include file="header.tpl"}
		</div>
		<!-- Header Ends -->
		<div id="top-menu">
			{include file="menu.tpl"}
		</div>
		<!-- Top Breadcrumb Start -->
		<!--<div id="breadcrumb">
			{include file="breadcrumbs.tpl"}
		</div> -->
		<!-- Top Breadcrumb End -->
		
		
		<!-- Right Side/Main Content Start -->
		<div id="rightside">
			{include file="$include_template"}
			<div id="footer">
				&copy;  projectName 2015
			</div> 
		</div>
		<!-- Right Side/Main Content End -->
		
		<!-- Left Dark Bar Start -->
		<!--	<div id="leftside">
			{include file="left.tpl"}
			</div>
		-->
		<!-- Left Dark Bar End -->
		
		<script type="text/javascript" src="{$tconfig.tsite_javascript}enhance.js"></script>	
		<script type='text/javascript' src='{$tconfig.tsite_javascript}excanvas.js'></script>
		{if $script neq 'scholarlist' AND  $script neq 'scholar_donation' AND  $script neq 'member_registered' AND  $script neq 'scholar_donation_report' AND  $script neq 'scholarlist_report'}
		<script type='text/javascript' src='{$tconfig.tsite_javascript}jquery-ui.min.js'></script>
		{/if}
		<script type='text/javascript' src='{$tconfig.tsite_javascript}jquery.wysiwyg.js'></script>
		<script type='text/javascript' src='{$tconfig.tsite_javascript}visualize.jQuery.js'></script>
		<script type="text/javascript" src='{$tconfig.tsite_javascript}functions.js'></script>
		<script type="text/javascript" src='{$tconfig.tsite_javascript}general.js'></script>
		<!--	<script language="JavaScript" src="{$tconfig.tcp_javascript}validate.js"></script> -->    
		{literal}
		<script>
			var	tsite_url = '{/literal}{$tconfig.tsite_url}{literal}';
			var	site_url = '{/literal}{$tconfig.tsite_url}{literal}';
			var	tpanel_url = '{/literal}{$tconfig.tpanel_url}{literal}';
			var	tcp_javascript = '{/literal}{$tconfig.tcp_javascript}{literal}';        
		</script>
		{/literal}
		{$GOOGLE_ANALYTICS_CODE}
	</body>
</html>
