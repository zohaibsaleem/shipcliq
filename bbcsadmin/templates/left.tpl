<div class="user">
	<img src="{$tconfig.tpanel_img}avatar.png" width="44" height="44" class="hoverimg" alt="Avatar" />
	<p>Logged in as:</p>
	<p class="username">{$sess_vFirstName} {$sess_vLastName}</p>
	<p class="userbtn"><a href="{$tconfig.tpanel_url}/index.php?file=ad-administrator&mode=edit&iAdminId={$sess_iAdminId}" title="{$sess_vFirstName} {$sess_vLastName}">Profile</a></p>
	<p class="userbtn"><a href="{$tconfig.tpanel_url}/index.php?file=au-logout" title="Log out">Log out</a></p>
</div>
<ul id="nav">
	<li>
		<a class="collapsed heading">Dashboard</a>
		<ul class="navigation">
		{if $smarty.get.file eq ''}
			<li class="heading selected">Dashboard</li>
		{else}
			<li class="heading"><a href="{$tconfig.tpanel_url}/index.php?file=home-dashboard"  title="">Dashboard</a></li>
		{/if}
		</ul>
	</li>
	<li>
		<a class="collapsed heading">Administrators</a>
		<ul class="navigation">
			{if $smarty.get.file eq 'ad-administrator'}
			<li class="selected">Administrator</li>
			{else}
			<li><a href="{$tconfig.tpanel_url}/index.php?file=ad-administrator&mode=view"  title="">Administrator</a></li>
			{/if}
		</ul>
	</li>
	<li>
		<a class="collapsed heading">Product Management</a>
		<ul class="navigation">
			{if $smarty.get.file eq 'pr-category'}
			<li class="selected">Product Category</li>
			{else}
			<li><a href="{$tconfig.tpanel_url}/index.php?file=pr-category&mode=view"  title="">Product Category</a></li>
			{/if} 
      {if $smarty.get.file eq 'pr-attributes'}
			<li class="selected">Attributes</li>
			{else}
			<li><a href="{$tconfig.tpanel_url}/index.php?file=pr-attributes&mode=view"  title="">Attributes</a></li>
			{/if}			       
			{if $smarty.get.file eq 'pr-product'}
			<li class="selected">Products</li>
			{else}
			<li><a href="{$tconfig.tpanel_url}/index.php?file=pr-product&mode=view"  title="">Products</a></li>
			{/if}
      {if $smarty.get.file eq 'pr-currency'}
			<li class="selected">Currency Rates Management</li>
			{else}
			<li><a href="{$tconfig.tpanel_url}/index.php?file=pr-currency&mode=view"  title="">Currency Rates Management</a></li>
			{/if}    	
		</ul>
	</li>	
	<li>
		<a class="collapsed heading">Member Management</a>
		<ul class="navigation">
			{if $smarty.get.file eq 'm-member_group'}
			<li class="selected">Members Groups</li>
			{else}
			<li><a href="{$tconfig.tpanel_url}/index.php?file=m-member_group&mode=view"  title="">Members Groups</a></li>
			{/if}
      {if $smarty.get.file eq 'm-member'}
			<li class="selected">Members</li>
			{else}
			<li><a href="{$tconfig.tpanel_url}/index.php?file=m-member&mode=view"  title="">Members</a></li>
			{/if}
		</ul>
	</li>
    <li>
		<a class="collapsed heading">Order Management</a>
		<ul class="navigation">
			{if $smarty.get.file eq 'o-orders'}
			<li class="selected">Orders</li>
			{else}
			<li><a href="{$tconfig.tpanel_url}/index.php?file=o-orders&mode=view"  title="">Orders</a></li>
			{/if}
		</ul>
	</li>
  <li>
		<a class="collapsed heading">Reports</a>
		<ul class="navigation">
			{if $smarty.get.file eq 'r-report'}
			<li class="selected">Order Report</li>
			{else}
			<li><a href="{$tconfig.tpanel_url}/index.php?file=r-order_report&mode=view"  title="">Order Report</a></li>
			{/if}
		</ul>
	</li>	
  <li>
		<a class="collapsed heading">Shipping Charges Mgmt.</a>
		<ul class="navigation">
			{if $smarty.get.file eq 'u-shipping'}
			<li class="selected">Shipping Charges Management</li>
			{else}
			<li><a href="{$tconfig.tpanel_url}/index.php?file=u-shipping&mode=view"  title="">Shipping Charges Management</a></li>
			{/if}
		</ul>
	</li>			
	<li>
		<a class="collapsed heading">Utility</a>
		<ul class="navigation">
     {if $smarty.get.file eq 'u-email_templates'}
			<li class="selected">Email templates</li>
			{else}
			<li><a href="{$tconfig.tpanel_url}/index.php?file=u-email_templates&mode=view"  title="">Email templates</a></li>
			{/if}			
		  {if $smarty.get.file eq 'u-coupon'}
			<li class="selected">Coupon Management</li>
			{else}
			<li><a href="{$tconfig.tpanel_url}/index.php?file=u-coupon&mode=view"  title="">Coupon Management</a></li>
			{/if}  		
			{if $smarty.get.file eq 'u-faq'}
				<li class="selected">FAQ</li>
			{else}
				<li><a href="{$tconfig.tpanel_url}/index.php?file=u-faq&mode=view" title="">FAQ</a></li>
			{/if}
      {if $smarty.get.file eq 'u-newsletter'}
			<li class="selected">Newsletters</li>
			{else}
			<li><a href="{$tconfig.tpanel_url}/index.php?file=u-newsletter&mode=view"  title="">Newsletters</a></li>
			{/if}
      {if $smarty.get.file eq 'u-newsletter_subscriber'}
			<li class="selected">Newsletter Subscriber</li>
			{else}
			<li><a href="{$tconfig.tpanel_url}/index.php?file=u-newsletter_subscriber&mode=view"  title="">Newsletter Subscriber</a></li>
			{/if}                 		
		</ul>
	</li>
     <li>
		<a class="collapsed heading">Localization</a>
		<ul class="navigation">
			{if $smarty.get.file eq 'l-country'}
			<li class="selected">Country</li>
			{else}
			<li><a href="{$tconfig.tpanel_url}/index.php?file=l-country&mode=view"  title="">Country</a></li>
			{/if}
			{if $smarty.get.file eq 'l-state'}
			<li class="selected">State</li>
			{else}
			<li><a href="{$tconfig.tpanel_url}/index.php?file=l-state&mode=view"  title="">State</a></li>
			{/if}
		</ul>
	</li>
	<li>
		<a class="collapsed heading">Settings</a>
		<ul class="navigation">
			{if $smarty.get.file eq 'to-generalsettings'}
				<li class="selected">General Configration</li>
			{else}
				<li><a href="{$tconfig.tpanel_url}/index.php?file=to-generalsettings&mode=edit" title="">General Configration</a></li>
			{/if}			
			{if $smarty.get.file eq 'to-banners'}
				<li class="selected">Banners</li>
			{else}
				<li><a href="{$tconfig.tpanel_url}/index.php?file=to-banners&mode=view" title="">Banners</a></li>
			{/if}
			{if $smarty.get.file eq 'pg-staticpages'}
				<li class="selected">Static Pages</li>
			{else}
				<li><a href="{$tconfig.tpanel_url}/index.php?file=pg-staticpages&mode=view" title="">Static Pages</a></li>
			{/if}
		  {if $smarty.get.file eq 'to-languagelabel'}
				<li class="selected">Language Label</li>
			{else}
				<li><a href="{$tconfig.tpanel_url}/index.php?file=to-languagelabel&mode=view" title="">Language Label</a></li>
			{/if} 
		</ul>
	</li>            	     	    
</ul>	
