<div class="contentcontainer" id="tabs">
	<div class="headings">
  	{if $mode eq 'add'}
		<h2 class="left">Add City</h2>
    {else}
	<h2 class="left">Edit City</h2>
    {/if}

  </div>
	<div class="contentbox" id="tabs-1">
         {if $var_msg neq ''}
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Success" />
					{$var_msg}</p> 
				</div>     
				<div></div>
			{/if}
			{if $var_msg_error neq ''}
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Success" />
					{$var_msg_error}</p> 
				</div>     
				<div></div>
			{/if}
			<form id="frmadd" name="frmadd" action="index.php?file=l-city_a" method="post">
            <input type="hidden" name="iCityId" id="iCityId" value="{$iCityId}" />
            <input type="hidden" name="action" id="action" value="{$mode}" />
				
        <p>
					<label for="textfield"><strong>Country : *</strong></label>
					<select id="vCountryCode" name="Data[vCountryCode]" lang="*" onchange="get_county_list(this.value);" title="Country">
						<option value=""> -------- Select Country --------- </option>
            {section name=rec loop=$db_country}
					   	<option value="{$db_country[rec].vCountryCode}" {if $db_country[rec].vCountryCode eq $db_city[0].vCountryCode}selected {/if}>{$db_country[rec].vCountry}</option>
            {/section}
					</select>
				</p>
				<p>
            <label for="textfield"><strong>State : <em>*</em></strong></label>
    <!--        <span id="county_list">    --> 
           <select id="vStateCode" name="Data[vStateCode]" lang="*" title="State" style="width:25%;">
              <option value="">--------Select State---------</option>
            </select>
      <!--        </span>   -->
        </p>
				
        <p>
					<label for="textfield"><strong>City :<em>*</em></strong></label>
					<input type="text" id="vCity" name="Data[vCity]" class="inputbox" value="{$db_city[0].vCity}" lang="*" title="City"/>
				</p>
			
				<p>
					<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $db_city[0].eStatus eq Active}selected{/if}>Active</option>
						<option value="Inactive" {if $db_city[0].eStatus eq Inactive}selected{/if}>Inactive</option>
					</select>
				</p>
				{if $mode eq 'edit'}
				<input type="submit" value="Edit City" class="btn" onclick="return validate(document.frmadd);" title="Edit City"/>
				      {else}
   				<input type="submit" value="Add City" class="btn" onclick="return validate(document.frmadd);" title="Add City"/>
  				
				      
				      {/if}
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
			</form>
	</div>
</div>
{literal}
<script>
 
function redirectcancel()
{
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=l-city&mode=view";
    return false;
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}

function get_county_list(code,selected)
{   
$("#vStateCode").html("Please wait...");
var request = $.ajax({
type: "POST",
url: '{/literal}{$tconfig.tpanel_url}{literal}'+'/index.php?file=l-getState',
data: "code="+code+"&stcode="+selected,

success: function(data) { 
$("#vStateCode").append(data);
}
});

request.fail(function(jqXHR, textStatus) {
alert( "Request failed: " + textStatus );
});
}

if('{/literal}{$db_city[0].vStateCode}{literal}' != '' || '{/literal}{$db_city[0].vCountryCode}{literal}' != ''){
get_county_list('{/literal}{$db_city[0].vCountryCode}{literal}', '{/literal}{$db_city[0].vStateCode}{literal}');
}
</script>
{/literal}