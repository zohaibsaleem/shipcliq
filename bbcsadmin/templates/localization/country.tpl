<div class="contentcontainer" id="tabs">
	<div class="headings">
		{if $mode eq "edit"}
			<h2 class="left">Edit Country</h2>
		{else}
			<h2 class="left">Add Country</h2>
		{/if}
	</div>
	<div class="contentbox" id="tabs-1">
             {if $var_msg neq ''}
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Success" />
					{$var_msg}</p> 
				</div>     
				<div></div>
			{/if}
			{if $var_msg_error neq ''}
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Success" />
					{$var_msg_error}</p> 
				</div>     
				<div></div>
			{/if}
			<form id="frmadd" name="frmadd" action="index.php?file=l-country_a" method="post">
            <input type="hidden" name="iCountryId" id="iCountryId" value="{$iCountryId}" />
            <input type="hidden" name="action" id="action" value="{$mode}" />
				<p>
					<label for="textfield"><strong>Country :<em>*</em></strong></label>
					<input type="text" id="vCountry" name="Data[vCountry]" class="inputbox" value="{$db_city[0].vCountry}" lang="*" title="Country"/>
				</p>
				<p>
					<label for="textfield"><strong>Country Code:<em>*</em></strong></label>
					<input type="text" id="vCountryCode" name="Data[vCountryCode]" class="inputbox" value="{$db_city[0].vCountryCode}" lang="*" title="Country Code"/>
				</p>
				<p>
					<label for="textfield"><strong>Country Code ISO :<em>*</em></strong></label>
					<input type="text" id="vCountryCodeISO_3" name="Data[vCountryCodeISO_3]" class="inputbox" value="{$db_city[0].vCountryCodeISO_3}" lang="*" title="Country Code ISO"/>
				</p>
				<p>
					<label for="textfield"><strong>Phone Code :<em>*</em></strong></label>
					<input type="text" id="vPhoneCode" name="Data[vPhoneCode]" class="inputbox" value="{$db_city[0].vPhoneCode}" lang="*" title="Phone Code"/>
				</p>
				<p>
					<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $db_city[0].eStatus eq Active}selected{/if}>Active</option>
						<option value="Inactive" {if $db_city[0].eStatus eq Inactive}selected{/if}>Inactive</option>
					</select>
				</p>
				{if $mode eq 'edit'}
				<input type="submit" value="Edit County" class="btn" onclick="return validate(document.frmadd);" title="Edit County"/>
				      {else}
   				<input type="submit" value="Add County" class="btn" onclick="return validate(document.frmadd);" title="Add County"/>
  				
				      
				      {/if}
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
			</form>
	</div>
</div>
{literal}
<script>
function redirectcancel()
{
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=l-country&mode=view";
    return false;
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
{/literal}