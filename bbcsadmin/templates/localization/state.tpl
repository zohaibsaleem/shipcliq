<div class="contentcontainer" id="tabs">
	<div class="headings">
  	{if $mode eq 'add'}
		<h2 class="left">Add Province</h2>
    {else}
	<h2 class="left">Edit Province</h2>
    {/if}

  </div>
	<div class="contentbox" id="tabs-1">
             {if $var_msg neq ''}
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Success" />
					{$var_msg}</p> 
				</div>     
				<div></div>
			{/if}
			{if $var_msg_error neq ''}
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Success" />
					{$var_msg_error}</p> 
				</div>     
				<div></div>
			{/if}
			<form id="frmadd" name="frmadd" action="index.php?file=l-state_a" method="post">
            <input type="hidden" name="iStateId" id="iStateId" value="{$iStateId}" />
            <input type="hidden" name="action" id="action" value="{$mode}" />
				<p>
					<label for="textfield"><strong>Province :<em>*</em></strong></label>
					<input type="text" id="vState" name="Data[vState]" class="inputbox" value="{$db_state[0].vState}" lang="*" title="Province"/>
				</p>
				<p>
					<label for="textfield"><strong>Province Code :<em>*</em></strong></label>
					<input type="text" id="vStateCode" name="Data[vStateCode]" class="inputbox" value="{$db_state[0].vStateCode}" lang="*" title="Province Code"/>
				</p>
				<p>
					<label for="textfield"><strong>Country : *</strong></label>
					<select id="iCountryId" name="Data[iCountryId]" lang="*" title="Country">
						<option value=""> --- Select Country --- </option>
                        {section name=rec loop=$db_country}
						<option  {if $db_country[rec].iCountryId eq $db_state[0].iCountryId} selected {/if} value="{$db_country[rec].iCountryId}">{$db_country[rec].vCountry}</option>
                        {/section}
					</select>
				</p>
        
			<!--
    <p>
					<label for="textfield"><strong>Country Code :</strong></label>
					<input type="text" id="vCountryCode"  name="Data[vCountryCode]" class="inputbox" lang="*" title="Country Code" value="{$db_state[0].vCountryCode}"/>
				</p>
   -->	
				<p>
					<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $db_state[0].eStatus eq Active}selected{/if}>Active</option>
						<option value="Inactive" {if $db_state[0].eStatus eq Inactive}selected{/if}>Inactive</option>
					</select>
				</p>
				{if $mode eq 'edit'}
				<input type="submit" value="Edit Province" class="btn" onclick="return validate(document.frmadd);" title="Edit Province"/>
				      {else}
   				<input type="submit" value="Add Province" class="btn" onclick="return validate(document.frmadd);" title="Add Province"/>
  				
				      
				      {/if}
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
			</form>
	</div>
</div>
{literal}
<script>
 
function redirectcancel()
{
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=l-state&mode=view";
    return false;
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
{/literal}