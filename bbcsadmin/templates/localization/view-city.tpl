<script language="JavaScript" src="{$tconfig.tcp_javascript}jlist.js"></script>
<div class="contentcontainer">
	<div class="headings altheading">
		<h2>City</h2>
	</div>     
	<div class="contentbox">
    {if $var_msg_new neq ''}
     <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg_new}</p> 
     </div>     
    <div></div>
    {elseif $var_msg neq ''}
    <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg}</p> 
     </div>     
    <div></div>
    {/if}
		<form name="frmsearch" id="frmsearch" action="" method="post">
        
        <table width="100%" border="0">
		<tbody>
			<tr>
				<td width="10%"><label for="textfield"><strong>Search:</strong></label><td>
				<td width="10%"><input type="Text" id="keyword" name="keyword" value="{$keyword}"  class="inputbox" /><td>
				<td width="10%" >
					<select name="option" id="option">
						<option  value="vCity">City</option>
						<option value="eStatus"{if $option eq 'eStatus'}selected{/if}>Status</option>
					</select>
				</td>
					<td width="60%">&nbsp;</td>
	<!--			<td width="60%"><input type="button" value="Search" class="btnalt" id="Search" name="Search" title="Search" onclick="Searchoption();"/></td> -->
				<td width="10%"><input type="button" value="Add New" onclick="Redirect('index.php?file=l-city&mode=add');" class="btnalt" />&nbsp;</td>
				<td width="10%"></td>
			</tr>
      <tr>
        <td>
          <label for="textfield"><strong>Country: </strong></label>
        </td>
        <td colspan="6">
          &nbsp; &nbsp;
          <select name="country" id="country" onchange="get_county_list(this.value);" title="Country">
            <option value="">--------Select Country--------</option>
           	{section name=i loop=$db_country1}
              <option value="{$db_country1[i].vCountryCode}" {if $country eq $db_country1[i].vCountryCode}selected {/if}>{$db_country1[i].vCountry}</option>
            {/section}
          </select>
        </td>
      </tr>
     <tr>
        <td>
          <label for="textfield"><strong>State: </strong></label>
        </td>
        <td colspan="6">
          &nbsp; &nbsp;
           <select id="vStateCode" name="Data[vStateCode]" lang="*" title="State" style="width:28%;">
              <option value="">--------Select State---------</option>
            </select>
        </td>
      </tr>	
      <tr>
        <td>&nbsp;</td>
        <td colspan="6">
           &nbsp; &nbsp;
          <input type="button" value="Search" class="btnalt" id="Search" name="Search" title="Search" onclick="Searchoption();"/>
        </td>
      </tr>
			<tr>
				<td colspan="7" align="center">
					{$AlphaBox}
				</td>
			</tr>
		</tbody>			
		</table> 
   </form>
  
   <form name="frmlist" id="frmlist"  action="index.php?file=l-city_a" method="post">
  	<table width="100%" border="0">
		<input type="hidden" name="action" id="action" value="" />
    <input  type="hidden" name="iCityId" value=""/>
     <thead>
			<tr>
				<th>City</th>
				<th>State</th>
				<th>Country</th>
				<th>Status</th>
				<th>Action</th>
				<th><input type="checkbox" id="check_all" name="check_all" onclick="checkAll(document.frmlist);"/></th>
			</tr>
		</thead>
		<tbody>
    {if $db_state_all|@count gt 0}
		{section name=i loop=$db_state_all}
     <tr>
      <td><a href="{$tconfig.tpanel_url}/index.php?file=l-city&mode=edit&iCityId={$db_state_all[i].iCityId}" title="">{$db_state_all[i].vCity}</a></td>
		 	<td>{$db_state_all[i].vState}</td>
			<td>{$db_state_all[i].vCountry}</td>
			<td>{$db_state_all[i].eStatus}</td>
			<td>
				<a href="{$tconfig.tpanel_url}/index.php?file=l-city&mode=edit&iCityId={$db_state_all[i].iCityId}" title=""><img src="{$tconfig.tpanel_img}icons/icon_edit.png" title="Edit" /></a>
				<a href="javascript:void(0);" title="Active" onclick="MakeAction('{$db_state_all[i].iCityId}','Active');"><img src="{$tconfig.tpanel_img}icons/icon_approve.png" title="Active" /></a>
				<a href="javascript:void(0);" title="Inactive" onclick="MakeAction('{$db_state_all[i].iCityId}','Inactive');"><img src="{$tconfig.tpanel_img}icons/icon_unapprove.png" title="Inactive" /></a>
				<a href="javascript:void(0);" title="Delete" onclick="MakeAction('{$db_state_all[i].iCityId}','Deletes');"><img src="{$tconfig.tpanel_img}icons/icon_delete.png" title="Delete" /></a>
			</td>
			<td><input name="iCityId[]" type="checkbox" id="iId" value="{$db_state_all[i].iCityId}"/></td>
	  	</tr>
     {/section}
     {else}
     <tr>
			<td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No Record Found.</td>
		 </tr>
     {/if}
		</tbody>
		</table>
   </form>
		<div class="extrabottom">
			<ul>
				<li><img src="{$tconfig.tpanel_img}icons/icon_edit.png" alt="Edit" /> Edit</li>
				<li><img src="{$tconfig.tpanel_img}icons/icon_approve.png" alt="Approve" /> Active</li>
				<li><img src="{$tconfig.tpanel_img}icons/icon_unapprove.png" alt="Unapprove" /> Inactive</li>
				<li><img src="{$tconfig.tpanel_img}icons/icon_delete.png" alt="Delete" /> Remove</li>
			</ul>
			<div class="bulkactions">
				<select name="newaction" id="newaction">
					<option value="">Select Action</option>
					<option value="Active">Make Active</option>
					<option value="Inactive">Make Inactive</option>
					<option value="Deletes">Make Delete</option>
					<option value="Show All">Show All</option>
				</select>
				<input type="submit" value="Apply" class="btn" onclick="return Doaction(document.getElementById('newaction').value,'l-city',document.frmlist);"/>
			</div>
		</div>
        <div>
         <div class="pagination">
            {if $db_state_all|@count gt 0}
	        <span class="switch" style="float: left;">{$recmsg}</span>
	        {/if}
         </div>
         {$page_link}
        </div>
	
		<div style="clear: both;"></div></div>
</div>
{literal}
<script>
function Searchoption(){
    document.getElementById('frmsearch').submit();
}
function AlphaSearch(val){
    var alphavalue = val;
    var file = 'l-city';
    window.location="index.php?file="+file+"&alp="+alphavalue+"&mode=view";
    return false;
}
function MakeAction(loopid,type){
if(type == 'Deletes')
    {
	   ans = confirm("Are you sure you want to delete?");
     if (ans == false)
      {
        return false;
      }
    }
    document.frmlist.iCityId.value = loopid;
    document.frmlist.action.value = type;
	document.frmlist.submit();	
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
function get_county_list(code,selected)
{   
$("#vStateCode").html("Please wait...");
var request = $.ajax({
type: "POST",
url: '{/literal}{$tconfig.tpanel_url}{literal}'+'/index.php?file=l-getState',
data: "code="+code+"&stcode="+selected,

success: function(data) { 
$("#vStateCode").append(data);
}
});

request.fail(function(jqXHR, textStatus) {
alert( "Request failed: " + textStatus );
});
} /*
if('{/literal}{$db_state_all[0].vStateCode}{literal}' != '' || '{/literal}{$db_state_all[0].vCountryCode}{literal}' != ''){
get_county_list('{/literal}{$db_state_all[0].vCountryCode}{literal}', '{/literal}{$db_state_all[0].vStateCode}{literal}');
}   */
if('{/literal}{$state2}{literal}' != '' || '{/literal}{$country}{literal}' != ''){
get_county_list('{/literal}{$country}{literal}', '{/literal}{$state2}{literal}');
}
</script>
{/literal}