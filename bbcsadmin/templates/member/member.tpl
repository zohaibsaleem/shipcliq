<div class="contentcontainer" id="tabs">
	<div class="headings">
		{if $mode eq 'edit'}
			<h2 class="left">Edit Member</h2>
		{else}
			<h2 class="left">Add Member</h2>
		{/if}
	</div>
	<div id="tabs-1" class="contentbox">  
  {if $mode eq 'edit'}
    <div id="tabs2" class="VideoTabs">
	  <ul>
    	  <li><a href="javascript:void(0);" id="tab1" class="current"><em>Member Details</em></a></li>
    	  <li><a href="javascript:void(0);" onclick="javascript:window.location='index.php?file=m-member_alert&mode=view&iMemberId={$iMemberId}';" id="tab2"><em>Member Alert</em></a></li>
        <li><a href="javascript:void(0);" onclick="javascript:window.location='index.php?file=m-member_rate&mode=view&iMemberId={$iMemberId}';" id="tab3"><em>Member Ratings & Reviews</em></a></li>			  
    </ul>
		  <div style="clear:both"></div>
		</div>
		
	{/if}
	      {if $var_msg neq ''}
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Success" />
					{$var_msg}</p> 
				</div>     
				<div></div>
			{/if}
			{if $smarty.get.var_msg_error neq ''}
				<div class="status success" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
					{$var_msg_error}</p> 
				</div>     
				 {/if}
				 {if $var_msg_mow neq ''}
				<div class="status success" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
					{$var_msg_mow}</p> 
				</div>     
				<div></div>
			{/if}
		<div id="keyfeatures" class="VideoText"> 			
		  <form id="frmadd" name="frmadd" action="index.php?file=m-member_a" method="post" enctype="multipart/form-data">
            <input type="hidden" name="iMemberId" id="iMemberId" value="{$iMemberId}" />
            <input type="hidden" name="action" id="action" value="{$mode}" />
		      	<input type="hidden" name="imagemode" id="imagemode" />	
			<p>
					<label for="textfield"><strong>First Name :<em>*</em></strong></label>
					<input type="text" id="vFirstName" name="Data[vFirstName]" class="inputbox" value="{$db_records[0].vFirstName}" lang="*" title="First Name"/>
			</p>
			<p>
					<label for="textfield"><strong>Last Name :<em>*</em></strong></label>
					<input type="text" id="vLastName" name="Data[vLastName]" class="inputbox" value="{$db_records[0].vLastName}" lang="*" title="Last Name"/>
			</p>
			<p>
					<label for="textfield"><strong>Gender :</strong></label>
					<select id="eGender" name="Data[eGender]">
						<option value="Male" {if $db_records[0].eGender eq Male}selected {/if}>Male</option>
						<option value="Female" {if $db_records[0].eGender eq Female}selected {/if}>Female</option>
					</select>
			</p>
			<p>
			   	<label for="textfield"><strong>BirthDate :</strong></label>
			   <table>
					<tr>
					<!--
          <td style="float:left;">
                      {$generalobj->gend_DisplayDay($uid,'startDay',$datearr[2],'')}                         
                      </td>
                      <td style="float:left;">
                      {$generalobj->gend_DisplayMonth($pass,'startMonth',$datearr[1],'')}                         
                      </td>
                      -->
                      <td style="float:left;">
                      {$generalobj->DisplayYear($user,'startYear','1920',date('Y'))}                         
                      </td>
					</tr></table>
			</p>
			<p>
					<label for="textfield"><strong>E-mail :<em>*</em></strong></label>
					<input type="text" id="vEmail"  name="Data[vEmail]" class="inputbox"  lang="{literal}*{E}{/literal}" title="E-mail" value="{$db_records[0].vEmail}"/>
			</p>
			<p>
					<label for="textfield"><strong>Password :<em>*</em></strong></label>
					<input type="password" id="vPassword"  name="Data[vPassword]" class="inputbox" lang="{literal}*{P}6:0{/literal}" title="Password" value="{$db_records[0].vPassword}"/>
			</p>

			<p>
					<label for="textfield"><strong>Address :</strong></label>
					<textarea name="Data[vAddress]" id="vAddress" class="inputbox"  title="Address">{$db_records[0].vAddress}</textarea>
			</p>
			
		  <p>
        <label for="textfield"><strong>Country : <em>*</em></strong></label> 
            <select id="vCountryCode" name="Data[vCountry]" lang="*" onchange="get_county_list(this.value);" style="" title="Country">
              <option value="">--------Select Country--------</option>           
    		      {section name=i loop=$db_country}   		        
    		            <option value="{$db_country[i].vCountryCode}" {if $db_country[i].vCountryCode eq $db_records[0].vCountry}selected {/if}>{$db_country[i].vCountry}</option>		                                                                                     		                     
              {/section}           
           </select>
        </p>
		  	<p>
            <label for="textfield"><strong>State : <em>*</em></strong></label>
            <span id="county_list"> 
            <select id="vStateCode" name="Data[vState]" style="width:25%;">
              <option value="">--------Select State---------</option>
            </select>
            </span>
        </p>
				<p>
					<label for="textfield"><strong>City :</strong></label>
					<input type="text" id="vCity"  name="Data[vCity]" class="inputbox" title="City" value="{$db_records[0].vCity}"/>
                </p>
				<p>
					<label for="textfield"><strong>Zipcode :</strong></label>
					<input type="text" id="vZip"  name="Data[vZip]" class="inputbox" title="Zip" value="{$db_records[0].vZip}" />
				</p>
				<p>
					<label for="textfield"><strong>Phone :</strong></label>
					<input type="text" id="vPhone"  name="Data[vPhone]" class="inputbox" title="Phone" lang="{literal}*{T}{/literal}" value="{$db_records[0].vPhone}"/>
				</p>
					<p>
					<label for="textfield"><strong>Upload Image :</strong></label>
					<input type="file" id="vImage" name="Data[vImage]" class="inputbox" title="Image" title="Upload Image" />
				</p>
			  {if $mode eq 'edit'}					
				{if $img eq 'yes'}	
				<p>                                
				  <table>
				    <tr>
				        <td width="25%"><img src= "{$tconfig.tsite_upload_images_member}/{$db_records[0].iMemberId}/1_{$db_records[0].vImage}" /></td>
            </tr>
             <tr><td><input type="button" value="Delete Image" class="btn" onclick="return confirm_delete();" title="Delete Image"/>         
        </td></tr>
          </table>
				</p>
				{/if}
        {else}
        {/if} 
				
				<p>
					<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $db_records[0].eStatus eq Active}selected {/if}>Active</option>
						<option value="Inactive" {if $db_records[0].eStatus eq Inactive}selected {/if}>Inactive</option>
						<option value="Pending" {if $db_records[0].eStatus eq Pending}selected {/if}>Pending</option>
            <option value="Deleted" {if $db_records[0].eStatus eq Deleted}selected {/if}>Deleted</option>						
					</select>
				</p>
			{if $mode eq 'edit'}
				<input type="submit" value="Edit Member" class="btn" onclick="return validate(document.frmadd);" title="Edit"/>
			{else}
   	    <input type="submit" value="Add Member" class="btn" onclick="return validate(document.frmadd);" title="Add"/>
  		{/if}
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
			
			</form>
		
 		 </div>	 
	</div>
</div>
{literal}
<script>
function redirectcancel()
{
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=m-member&mode=view";
    return false;
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}

$(document).ready(function(){

  $(function() {
    $( "#tabs2" ).tabs({ selected: 0 });
	
  });
});
function confirm_delete()
{
   ans = confirm('Are you sure you want to delete Image?');
   if(ans == true){
    document.frmadd.imagemode.value = 'delete_image';
    document.frmadd.submit();
   }else{
    return false;
   }
} 

function get_county_list(code,selected)
{   
$("#county_list").html("Please wait...");
var request = $.ajax({
type: "POST",
url: '{/literal}{$tconfig.tpanel_url}{literal}'+'/index.php?file=m-getState',
data: "code="+code+"&stcode="+selected,

success: function(data) { 
$("#county_list").html(data);
}
});

request.fail(function(jqXHR, textStatus) {
//alert( "Request failed: " + textStatus );
});
}

if('{/literal}{$db_records[0].vState}{literal}' != '' || '{/literal}{$db_records[0].vCountry}{literal}' != ''){
get_county_list('{/literal}{$db_records[0].vCountry}{literal}', '{/literal}{$db_records[0].vState}{literal}');
}

function show_addnew_tr(){
  document.getElementById("addnewtr").style.display="";
}
function hide_addnew_tr(){
  document.getElementById("addnewtr").style.display="none";
}
function memberOfWeek(val){
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=m-member_a&action=memberofweek&status={/literal}{$db_records[0].eStatus}{literal}&iMemberId={/literal}{$db_records[0].iMemberId}{literal}&val="+val;
    return false;
}
</script>
{/literal}

