<div class="contentcontainer" id="tabs">
	<div class="headings">
			<h2 class="left">Edit Member Message</h2>
	</div>
	<div id="tabs-1" class="contentbox">  
        {if $var_msg neq ''}
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Success" />
					{$var_msg}</p> 
				</div>     
				<div></div>
			{/if}
			{if $smarty.get.var_msg_error neq ''}
				<div class="status success" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
					{$var_msg_error}</p> 
				</div>     
				 {/if}
				 {if $var_msg_mow neq ''}
				<div class="status success" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
					{$var_msg_mow}</p> 
				</div>     
				<div></div>
			{/if}
		 			
		  <form id="frmadd" name="frmadd" action="index.php?file=m-member_messages_a" method="post" enctype="multipart/form-data">
      <input type="hidden" name="iMessageId" id="iMessageId" value="{$iMessageId}" />
      <input type="hidden" name="action" id="action" value="{$mode}" />
		
			<p>
					<label for="textfield"><strong>Message :</strong></label>
					<textarea name="Data[tMessage]" id="tMessage" class="inputbox"  title="Message">{$db_records[0].tMessage}</textarea>
			</p>
			
				<input type="submit" value="Edit Message" class="btn" onclick="return validate(document.frmadd);" title="Edit"/>
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
			
			</form>
		 
	</div>
</div>
{literal}
<script>
function redirectcancel()
{
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=m-member_messages&mode=view";
    return false;
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}


</script>
{/literal}

