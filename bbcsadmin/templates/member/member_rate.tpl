<link type="text/css" rel="stylesheet" href="{$tconfig.tsite_javascript}dhtmlgoodies_calendar.css" media="screen">
<script type="text/javascript" src="{$tconfig.tsite_javascript}dhtmlgoodies_calendar.js"></script>

<div class="contentcontainer" id="tabs">
	<div class="headings">
		<h2>Edit Member Rate</h2>
	</div>
	<div class="contentbox" id="tabs-1">
	<div id="tabs2" class="VideoTabs">
	  <ul>
    	  <li><a href="javascript:void(0);" onclick="javascript:window.location='index.php?file=m-member&mode=edit&iMemberId={$iMemberId}';" id="tab1"><em>Member Details</em></a></li>
    	  <li><a href="javascript:void(0);" onclick="javascript:window.location='index.php?file=m-member_alert&mode=view&iMemberId={$iMemberId}';" id="tab2"><em>Member Alert</em></a></li>
    	  <li><a href="javascript:void(0);" onclick="javascript:window.location='index.php?file=m-member_rate&mode=view&iMemberId={$iMemberId}';" id="tab3" class="current"><em>Member Ratings & Reviews</em></a></li>			  
    </ul>
		<div style="clear:both"></div>
	</div>
	<div id="keyfeatures" class="VideoText">
   <form id="frmadd" name="frmadd" action="index.php?file=m-member_rate_a" method="post" enctype="multipart/form-data">
   <input type="hidden" name="iRateId" id="iRateId" value="{$iRateId}" />
   <input type="hidden" name="iMemberId" id="iMemberId" value="{$iMemberId}" />
   <input type="hidden" name="action" id="action" value="{$mode}" />
   <p>
	   <label for="textfield"><strong>Rate : <em>*</em></strong></label>
     <select id="iRate" name="Data[iRate]" lang="*" title="Rate">
      <option value="">Select Rate</option>
      <option value="1" {if $db_rate[0].iRate eq 1} selected {/if}>1</option>
      <option value="2" {if $db_rate[0].iRate eq 2} selected {/if}>2</option>
      <option value="3" {if $db_rate[0].iRate eq 3} selected {/if}>3</option>
      <option value="4" {if $db_rate[0].iRate eq 4} selected {/if}>4</option>
      <option value="5" {if $db_rate[0].iRate eq 5} selected {/if}>5</option>
     </select>
	 </p>
   <p>
     <label for="textfield"><strong>Skill : <em>*</em></strong></label> 
     <select id="eSkill" name="Data[eSkill]" lang="*" title="Skill">
      <option value="Pleasant" {if $db_rate[0].eSkill eq 'Pleasant'} selected {/if}>Pleasant</option>
      <option value="ToImprove" {if $db_rate[0].eSkill eq 'ToImprove'} selected {/if}>ToImprove</option>
      <option value="Avoid" {if $db_rate[0].eSkill eq 'Avoid'} selected {/if}>Avoid</option>
     </select>
   </p>
   <p>
    <label for="textfield"><strong>Added Date : <em>*</em></strong></label> 
    <input type="text" Readonly id="dAddedDate" name="Data[dAddedDate]" style="width:100px;" class="inputbox" value="{$generalobj->DateTime($db_rate[0].dAddedDate,14)}" lang="*" title="Added Date"/>&nbsp;<img src="{$tconfig.tsite_images}cal-icon.gif" style="cursor:pointer" onclick="displayCalendar(document.getElementById('dAddedDate'),'yyyy-mm-dd',this)" />
   </p>
   <p>
    <label for="textfield"><strong>Review : <em>*</em></strong></label> 
    <textarea id="tFeedback" name="Data[tFeedback]" class="inputbox" lang="*" title="Review">{$db_rate[0].tFeedback}</textarea>
   </p>
   <p>
    <input type="submit" value="Edit Rate" class="btn" onclick="return validate(document.frmadd);" title="Edit"/>
    <input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
   </p>
   </form>
  </div>
</div>
</div>
{literal}
<script>
function redirectcancel()
{
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=m-member_rate&mode=view&iMemberId={/literal}{$iMemberId}{literal}";
    return false;
}

function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
$(document).ready(function(){
$(function() {
    $( "#tabs2" ).tabs({ selected: 2 });
  });
});
</script>
{/literal}