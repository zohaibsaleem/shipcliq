<script type="text/javascript" src="../plugins/ckeditor/ckeditor.js"></script>
<script language="JavaScript" src="{$tconfig.tcp_javascript}validate.js"></script>
<link type="text/css" rel="stylesheet" href="{$tconfig.tsite_javascript}dhtmlgoodies_calendar.css" media="screen">
<script type="text/javascript" src="{$tconfig.tsite_javascript}dhtmlgoodies_calendar.js"></script>
<div class="contentcontainer" id="tabs">
	<div class="headings">
		{if $mode eq 'edit'}
		<h2 class="left">Edit Member Story</h2>
    {else}
		<h2 class="left">Add Member Story</h2>
    {/if}
	</div>
	<div class="contentbox" id="tabs-1">
            {if $var_msg neq ''}
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Success" />
					{$var_msg}</p> 
				</div>     
				<div></div>
			{/if}
			{if $var_msg_error neq ''}
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Success" />
					{$var_msg_error}</p> 
				</div>     
				<div></div>
			{/if}
			<form id="frmadd" name="frmadd" action="index.php?file=m-member_stories_a" method="post" enctype="multipart/form-data">
      <input type="hidden" name="iMemberStoriesId" id="iMemberStoriesId" value="{$iMemberStoriesId}" />
      <input type="hidden" name="action" id="action" value="{$mode}" />
      <input type="hidden" name="imagemode" id="imagemode" />
      
       <p>
					<label for="textfield"><strong>Member :<em>*</em></strong></label>
				  <select name="Data[iMemberId]" id="iMemberId" style="width:200px;" lang="*" title="Member">
					 	           <option value="">---Select Member---</option>
                        {section name=i loop=$db_member}
                            {if $db_admin[0].iMemberId eq $db_member[i].iMemberId}
                               {assign var="selected" value="selected"}
                            {else}
                            {assign var="selected" value=""}
                            {/if}
                            <option value="{$db_member[i].iMemberId}" {$selected}>{$db_member[i].vFirstName} {$db_member[i].vLastName}</option>
                        {/section}
					</select>
				</p>
       	<p>
					<label for="textfield"><strong>Title  :<em>*</em></strong></label>
					<input type="text" id="vTitle" name="Data[vTitle]" class="inputbox" value="{$db_admin[0].vTitle}" lang="*" title="Title English"/>
				</p>
				<p>
					<label for="textfield"><strong>Description  :</strong></label>
				  {fckeditor BasePath="../plugins/FCKeditor/" InstanceName="Data[tDescription]" Width="500px" Height="450px" Value="$tDescription"}
				</p>
			<!--	<p>
					<label for="textfield"><strong>Title [Norwegian] :</strong></label>
					<input type="text" id="vTitle_NO" name="Data[vTitle_NO]" class="inputbox" value="{$db_admin[0].vTitle_NO}" title="Title Norwegian"/>
				</p>
				<p>
					<label for="textfield"><strong>Description [Norwegian] :</strong></label>
				  {fckeditor BasePath="../plugins/FCKeditor/" InstanceName="Data[tDescription_NO]" Width="500px" Height="450px" Value="$tDescription_NO"}
				</p>
				<p>
					<label for="textfield"><strong>Title [Polish] :</strong></label>
					<input type="text" id="vTitle_PO" name="Data[vTitle_PO]" class="inputbox" value="{$db_admin[0].vTitle_PO}" title="Title Polish"/>
				</p>
				<p>
					<label for="textfield"><strong>Description [Polish] :</strong></label>
				  {fckeditor BasePath="../plugins/FCKeditor/" InstanceName="Data[tDescription_PO]" Width="500px" Height="450px" Value="$tDescription_PO"}
				</p>
				<p>
					<label for="textfield"><strong>Title [Finnish] :</strong></label>
					<input type="text" id="vTitle_FN" name="Data[vTitle_FN]" class="inputbox" value="{$db_admin[0].vTitle_FN}" title="Title Finnish"/>
				</p>
				<p>
					<label for="textfield"><strong>Description [Finnish] :</strong></label>
				  {fckeditor BasePath="../plugins/FCKeditor/" InstanceName="Data[tDescription_FN]" Width="500px" Height="450px" Value="$tDescription_FN"}
				</p>
				<p>
					<label for="textfield"><strong>Title [Russian] :</strong></label>
					<input type="text" id="vTitle_RS" name="Data[vTitle_RS]" class="inputbox" value="{$db_admin[0].vTitle_RS}" title="Title Russian"/>
				</p>
				<p>
					<label for="textfield"><strong>Description [Russian] :</strong></label>
				  {fckeditor BasePath="../plugins/FCKeditor/" InstanceName="Data[tDescription_RS]" Width="500px" Height="450px" Value="$tDescription_RS"}
				</p>
				<p>
					<label for="textfield"><strong>Title [Swedish] :</strong></label>
					<input type="text" id="vTitle_SW" name="Data[vTitle_SW]" class="inputbox" value="{$db_admin[0].vTitle_SW}" title="Title Swedish"/>
				</p>
				<p>
					<label for="textfield"><strong>Description [Swedish] :</strong></label>
				  {fckeditor BasePath="../plugins/FCKeditor/" InstanceName="Data[tDescription_SW]" Width="500px" Height="450px" Value="$tDescription_SW"}
				</p>
				<p>
					<label for="textfield"><strong>Title [Danish] :</strong></label>
					<input type="text" id="vTitle_DN" name="Data[vTitle_DN]" class="inputbox" value="{$db_admin[0].vTitle_DN}" title="Title Danish"/>
				</p>
				<p>
					<label for="textfield"><strong>Description [Danish] :</strong></label>
				  {fckeditor BasePath="../plugins/FCKeditor/" InstanceName="Data[tDescription_DN]" Width="500px" Height="450px" Value="$tDescription_DN"}
				</p>-->
		    <p>
					<label for="textfield"><strong>Upload Photo :</strong></label>
			    <input type="file" id="vImage" name="Data[vImage]" class="inputbox" title="Images" />
				<br />[Note: Supported File Types are *.jpg,*.jpeg,*.png,*.gif]
			</p>
			{if $mode eq 'edit'}					
			{if $img eq 'yes'}	
			<p>
				<table>
					<tr>
						<td width="25%"><img src= "{$tconfig.tsite_upload_images_memberstory}/1_{$db_admin[0].vImage}" /></td>
					</tr>
					<tr>
						<td>
							<input type="button" value="Delete Image" class="btn" onclick="return confirm_delete();" title="Delete Image"/>        
						</td>
					</tr>
				</table>
			</p>
			{/if}
			{/if}
			
		  	<p>
					<label for="textfield"><strong>Added Date :</strong></label>
				  <input type="text" Readonly id="dFDate" name="dFDate" style="width:100px;" class="inputbox" value="{$db_admin[0].dAddedDate}" lang="" title="Added Date"/>&nbsp;<img src="{$tconfig.tsite_images}cal-icon.gif" style="cursor:pointer" onclick="displayCalendar(document.getElementById('dFDate'),'yyyy-mm-dd',this)" />
				</p>
        
				<p>
					<label for="textfield"><strong>Type :</strong></label>
					<select id="eType" name="Data[eType]">
						<option value="Passengers" {if $db_admin[0].eType eq Passengers}selected{/if}>Sender</option>
						<option value="Drivers" {if $db_admin[0].eType eq Drivers}selected{/if}>Traveler</option>
					</select>
				</p>
				<p>
					<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $db_admin[0].eStatus eq Active}selected{/if}>Active</option>
						<option value="Inactive" {if $db_admin[0].eStatus eq Inactive}selected{/if}>Inactive</option>
					</select>
				</p>
				
				{if $mode eq 'edit'}
			   	<input type="submit" value="Edit Story" class="btn" onclick="return validate(document.frmadd);" title="Edit Story"/>
			  {else}
   				<input type="submit" value="Add Story" class="btn" onclick="return validate(document.frmadd);" title="Add Story"/>
  		  {/if}
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
			</form>
	</div>
</div>
{literal}
<script>

function redirectcancel()
{
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=m-member_stories&mode=view";
    return false;
}

function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
function confirm_delete()
{
   ans = confirm('Are you sure Delete Image?');
   if(ans == true){
    document.frmadd.imagemode.value = 'delete_image';
    document.frmadd.submit();
   }else{
    return false;
   }
}
</script>
{/literal}