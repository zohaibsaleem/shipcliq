<script language="JavaScript" src="{$tconfig.tcp_javascript}jlist.js"></script>
<div class="contentcontainer">
	<div class="headings altheading">
		<h2>Members</h2>
	</div>
	<div class="contentbox">
    {if $var_msg_new neq ''}
     <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg_new}</p> 
     </div>     
    <div></div>
    {elseif $var_msg neq ''}
    <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg}</p> 
     </div>     
    <div></div>
    {/if}
    {if $smarty.get.var_msg_err neq ''}
     <div class="status error" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Error" />
              {$smarty.get.var_msg_err}</p> 
     </div>     
    <div></div>
    {/if}
		<form name="frmsearch" id="frmsearch" action="" method="post">
        
        <table width="100%" border="0">
		<tbody>
			<tr>
				<td width="10%"><label for="textfield"><strong>Search:</strong></label><td>
				<td width="10%"><input type="Text" id="keyword" name="keyword" value="{$keyword}"  class="inputbox" /><td>
				<td width="10%" >
					<select name="option" id="option">
						<option  value='concat(vFirstName," ",vLastName)'>Name</option> 					
						<option value="vEmail"{if $option eq 'vEmail'}selected{/if}>E-mail</option>
						<option value="eStatus"{if $option eq 'eStatus'}selected{/if}>Status</option>
					</select>
				</td>
				<td width="60%"><input type="button" value="Search" class="btnalt" id="Search" name="Search" title="Search" onclick="Searchoption();"/></td>
	<!--			<td width="10%"><input type="button" value="Add New" onclick="Redirect('index.php?file=m-member&mode=add');" class="btnalt" />&nbsp;</td> -->
			</tr>	
			<tr>
				<td colspan="7" align="center">
					{$AlphaBox}
				</td>
			</tr>
		</tbody>			
		</table> 
    </form>
 
    <form name="frmlist" id="frmlist"  action="index.php?file=m-member_a" method="post">
 		<table width="100%" border="0">
		<input type="hidden" name="action" id="action" value="" />
        <input  type="hidden" name="iMemberId" value=""/>
		<input type="hidden" name="doc" id="doc" value=""/>
        <thead>
			<tr>
				 <th width="13%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=m-member&mode=view&sortby=1&order={if $order eq 0}1{else}0{/if}');">Name</a>{if $sortby eq 1}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>               
        <th width="10%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=m-member&mode=view&sortby=2&order={if $order eq 0}1{else}0{/if}');">E-mail</a>{if $sortby eq 2}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>               
        <th width="9%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=m-member&mode=view&sortby=3&order={if $order eq 0}1{else}0{/if}');">Added Date</a>{if $sortby eq 3}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/down_active.png{else}icons/up_active.png{/if}">{/if}</th>
		<th width="10%">Documents</th>
				
    	  <th width="8%"><div align="center">Access Account</div></th>
        <th width="8%"><div align="center">Delete Account</div></th>
         <th width="9%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=m-member&mode=view&sortby=4&order={if $order eq 0}1{else}0{/if}');">Status</a>{if $sortby eq 4}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>               
       	<th width="10%">Action</th>
				<th width="5%"><input type="checkbox" id="check_all" name="check_all" onclick="checkAll(document.frmlist);"/></th>
			</tr>
		</thead>
		<tbody>
    {if $db_records_all|@count gt 0}
	  {section name=i loop=$db_records_all}
     <tr>
			<td><a href="{$tconfig.tpanel_url}/index.php?file=m-member&mode=edit&iMemberId={$db_records_all[i].iMemberId}" title="">{$db_records_all[i].vFirstName} {$db_records_all[i].vLastName}</a></td>
      <td>{$db_records_all[i].vEmail}</td>
			<td>{$generalobj->DateTime($db_records_all[i].dAddedDate,9)}</td>
		<td nowrap="">
							{if $db_records_all[i].vCarPaper eq ''}	
									
							{else}
								<a href="{$tconfig['tsite_upload_images_member']}{$db_records_all[i].iMemberId}/{$db_records_all[i].vCarPaper}" target="_blank">Car Papers</a>>>{if $db_records_all[i].eCarPaperStatus eq 'Pending' && $db_records_all[i].vCarPaper neq ''}
									<a href="javascript:void(0);" title="Approve" onclick="MakeAction('{$db_records_all[i].iMemberId}','Approved','CarPaper');">Approve</a> |
									<a href="javascript:void(0);" title="Decline" onclick="MakeAction('{$db_records_all[i].iMemberId}','Unapproved','CarPaper');">Decline</a>
								{else if $db_records_all[i].eCarPaperStatus eq 'Approved' && $db_records_all[i].vCarPaper neq ''}
									
									<a href="javascript:void(0);" title="click to Decline" onclick="MakeAction('{$db_records_all[i].iMemberId}','Unapproved','CarPaper');">{$db_records_all[i].eCarPaperStatus}</a>
								{else if $db_records_all[i].eCarPaperStatus eq 'Unapproved' && $db_records_all[i].vCarPaper neq ''}								
									<a href="javascript:void(0);" title="click to Approve" onclick="MakeAction('{$db_records_all[i].iMemberId}','Approved','CarPaper');">{$db_records_all[i].eCarPaperStatus}</a> 
								{else}
									- 
								{/if}
								<br />
							{/if}
							
							
							{if $db_records_all[i].vLicense eq ''}	
									
							{else}
								<a href="{$tconfig['tsite_upload_images_member']}{$db_records_all[i].iMemberId}/{$db_records_all[i].vLicense}" target="_blank">License</a>>>{if $db_records_all[i].eLicenseStatus eq 'Pending' && $db_records_all[i].vLicense neq ''}
									<a href="javascript:void(0);" title="Approve" onclick="MakeAction('{$db_records_all[i].iMemberId}','Approved');">Approve</a> |
									<a href="javascript:void(0);" title="Decline" onclick="MakeAction('{$db_records_all[i].iMemberId}','Unapproved');">Decline</a>
								{else if $db_records_all[i].eLicenseStatus eq 'Approved' && $db_records_all[i].vLicense neq ''}
									
									<a href="javascript:void(0);" title="click to Decline" onclick="MakeAction('{$db_records_all[i].iMemberId}','Unapproved');">{$db_records_all[i].eLicenseStatus}</a>
								{else if $db_records_all[i].eLicenseStatus eq 'Unapproved' && $db_records_all[i].vLicense neq ''}								
									<a href="javascript:void(0);" title="click to Approve" onclick="MakeAction('{$db_records_all[i].iMemberId}','Approved');">{$db_records_all[i].eLicenseStatus}</a> 
								{else}
									- 
								{/if}
							{/if}
							
		</td>
		
		
							
      <td><a href="{$tconfig.tsite_url}/login_admin.php?iMemberId={$db_records_all[i].iMemberId|@md5}" target="_blank"><div align="center"><img src="{$tconfig.tpanel_img}login-icon.png"/></div></a></td>  
			<td style="text-align:center;"><span id="delacc{$db_records_all[i].iMemberId}"></span><span id="delac{$db_records_all[i].iMemberId}"><a href="javascript:void(0);" onclick="deleteAccount({$db_records_all[i].iMemberId},'');">Delete</a></span></td>
      <td>{$db_records_all[i].eStatus}</td>
			<td>
				<a href="{$tconfig.tpanel_url}/index.php?file=m-member&mode=edit&iMemberId={$db_records_all[i].iMemberId}" title=""><img src="{$tconfig.tpanel_img}icons/icon_edit.png" title="Edit" /></a>
				<a href="javascript:void(0);" title="Active" onclick="MakeAction('{$db_records_all[i].iMemberId}','Active');"><img src="{$tconfig.tpanel_img}icons/icon_approve.png" title="Active" /></a>
				<a href="javascript:void(0);" title="Inactive" onclick="MakeAction('{$db_records_all[i].iMemberId}','Inactive');"><img src="{$tconfig.tpanel_img}icons/icon_unapprove.png" title="Inactive" /></a>
				<a href="javascript:void(0);" title="Pending" onclick="MakeAction('{$db_records_all[i].iMemberId}','Pending');"><img src="{$tconfig.tpanel_img}icons/icon-pending.gif" title="Pending" /></a>
        <a href="javascript:void(0);" title="Delete" onclick="MakeAction('{$db_records_all[i].iMemberId}','Deleted');"><img src="{$tconfig.tpanel_img}icons/icon_delete.png" title="Delete" /></a>
			</td>
			<td><input name="iMemberId[]" type="checkbox" id="iId" value="{$db_records_all[i].iMemberId}"/></td>
		</tr>
        {/section}
        {else}
        <tr>
			<td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No Record Found.</td>
		</tr>
        {/if}
		</tbody>
		</table>
        </form>
		<div class="extrabottom">
			<ul>
				<li><img src="{$tconfig.tpanel_img}icons/icon_edit.png" alt="Edit" /> Edit</li>
				<li><img src="{$tconfig.tpanel_img}icons/icon_approve.png" alt="Approve" /> Active</li>
				<li><img src="{$tconfig.tpanel_img}icons/icon_unapprove.png" alt="Unapprove" /> Inactive</li>
				<li><img src="{$tconfig.tpanel_img}icons/icon-pending.gif" alt="Pending" /> Pending</li>
				<li><img src="{$tconfig.tpanel_img}icons/icon_delete.png" alt="Deleted" /> Deleted</li>
			</ul>
			<div class="bulkactions">
				<select name="newaction" id="newaction">
					<option value="">Select Action</option>
					<option value="Active">Make Active</option>
					<option value="Inactive">Make Inactive</option>
					<option value="Pending">Make Pending</option>
					<option value="Deleted">Make Deleted</option>
					<option value="Show All">Show All</option>
				</select>
				<input type="submit" value="Apply" class="btn" onclick="return Doaction(document.getElementById('newaction').value,'m-member',document.frmlist);"/>
			</div>
		</div>
        <div>
            <div class="pagination">
            {if $db_records_all|@count gt 0}
	        <span class="switch" style="float: left;">{$recmsg}</span>
	        {/if}
            </div>
            {$page_link}
        </div>
		
        <!--<ul class="pagination">
			<li class="text">Previous</li>
			<li class="page"><a href="#" title="">1</a></li>
			<li><a href="#" title="">2</a></li>
			<li><a href="#" title="">3</a></li>
			<li><a href="#" title="">4</a></li>
			<li class="text"><a href="#" title="">Next</a></li>
		</ul>-->
		<div style="clear: both;"></div></div>
</div>
{literal}
<script>
function Searchoption(){
    document.getElementById('frmsearch').submit();
}
function AlphaSearch(val){
    var alphavalue = val;
    var file = 'm-member';
	// changes by hemali to show all values
	if(alphavalue != '') // need to search on any alpha value
		window.location="index.php?file="+file+"&alp="+alphavalue+"&mode=view";
	else  // search all result
		window.location="index.php?file="+file+"&mode=view";
	
    return false;
}
function MakeAction(loopid,type,doc){
if(type == 'Deleted')
    {
	   ans = confirm("Are you sure you want to delete?");
     if (ans == false)
      {
        return false;
      }
    }
    document.frmlist.iMemberId.value = loopid;
    document.frmlist.action.value = type;
	document.frmlist.doc.value=doc;
	document.frmlist.submit();	
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}

function deleteAccount(id,conf){
  if(conf!='yes'){
    var del=confirm("Are you sure delete member account ?");
    if(del==false)
      return false;
  }
  
  $("#delacc"+id).html('Please Wait...');
  $("#delac"+id).hide();
  
  var request = $.ajax({
  type: "POST",
  url: '{/literal}{$tconfig.tpanel_url}{literal}'+'/index.php?file=m-deleteAccount',
  data: "id="+id+"&delete="+conf,
  
  success: function(data) {
  
   if(data==9999){
      window.location="index.php?file=m-member&mode=view&var_msg=Member account deleted successfully";
      return false;
   }
   if(data==9998){
      window.location="index.php?file=m-member&mode=view&var_msg_err=Erron in delete";
      return false;
   }
   
   var res = data.split("|");
   id=res[1];
   tot=res[0];
   
   if(tot>0){
      var res1=confirm("This member has posted rides or booking rides, sure for delete ?");
      if(res1==false){
        $("#delac"+id).show();
        $("#delacc"+id).html('');
        return false;
      }else{
        deleteAccount(id,"yes");
      }
   }else{
      window.location="index.php?file=m-member&mode=view";
   }
  }
  });
  
  request.fail(function(jqXHR, textStatus) {
    alert( "Request failed: " + textStatus );
  });
} 
</script>
{/literal}