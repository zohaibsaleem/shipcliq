<div class="contentcontainer" id="tabs">
	<div class="headings">
		<h2>Member Alert</h2>
	</div>
	<div class="contentbox" id="tabs-1">
	<div id="tabs2" class="VideoTabs">
	  <ul>
    	  <li><a href="javascript:void(0);" onclick="javascript:window.location='index.php?file=m-member&mode=edit&iMemberId={$iMemberId}';" id="tab1"><em>Member Details</em></a></li>
    	  <li><a href="javascript:void(0);" id="tab2" class="current"><em>Member Alert</em></a></li>			  
    	  <li><a href="javascript:void(0);" onclick="javascript:window.location='index.php?file=m-member_rate&mode=view&iMemberId={$iMemberId}';" id="tab3"><em>Member Ratings & Reviews</em></a></li>
    </ul>
		<div style="clear:both"></div>
	</div>
	<div id="keyfeatures" class="VideoText">
   	<table width="100%" style="width:100%;border:1px solid #CCCCCC;border-collapse: collapse;">
	  <thead>
			<tr>
				<th width="20%">#Sr. No.</th>               
        <th width="30%">From Location</th>               
        <th width="30%">To Location</th>               
    	  <th width="20%">Travel Date</th>
    	</tr>
		</thead>
		<tbody>
    {if $db_alert|@count gt 0}
		{section name=i loop=$db_alert}
		  <tr>
		    <td style="padding-left:20px;">{$smarty.section.i.index+1}</td>
		    <td style="padding-left:10px;">{$db_alert[i].vStartPoint}</td>
		    <td style="padding-left:10px;">{$db_alert[i].vEndPoint}</td>
		    <td style="padding-left:10px;">{$generalobj->DateTime($db_alert[i].dDate,14)}</td>
		  </tr>
    {/section}
    {else}
    <tr>
			<td height="70px;" colspan="4" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No Alert Created.</td>
		</tr>
    {/if}
		</tbody>
		</table>
   </div>
</div>
</div>
{literal}
<script>

function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
$(document).ready(function(){
$(function() {
    $( "#tabs2" ).tabs({ selected: 1 });
  });
});
</script>
{/literal}