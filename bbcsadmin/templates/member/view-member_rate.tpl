<div class="contentcontainer" id="tabs">
	<div class="headings">
		<h2>Member Rate</h2>
	</div>
	<div class="contentbox" id="tabs-1">
	<div id="tabs2" class="VideoTabs">
	  <ul>
    	  <li><a href="javascript:void(0);" onclick="javascript:window.location='index.php?file=m-member&mode=edit&iMemberId={$iMemberId}';" id="tab1"><em>Member Details</em></a></li>
    	  <li><a href="javascript:void(0);" onclick="javascript:window.location='index.php?file=m-member_alert&mode=view&iMemberId={$iMemberId}';" id="tab2"><em>Member Alert</em></a></li>
    	  <li><a href="javascript:void(0);" id="tab3" class="current"><em>Member Ratings & Reviews</em></a></li>			  
    </ul>
		<div style="clear:both"></div>
	</div>
	<div id="keyfeatures" class="VideoText">
    {if $var_msg neq ''}
    <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg}</p> 
     </div>     
    <div></div>
    {/if}
    <form name="frm" id="frm" method="post" action="index.php?file=m-member_rate_a">
      <input type="hidden" id="iMemberId" name="iMemberId" value="{$iMemberId}">
      <input type="hidden" id="iRateId" name="iRateId" value="">
      <input type="hidden" id="action" name="action" value="Deletes">
    </form>
   	<table width="100%" style="width:100%;border:1px solid #CCCCCC;border-collapse: collapse;">
	  <thead>
			<tr>
				<th width="30%">From</th>               
        <th width="20%">Rate</th>               
        <th width="25%">Skill</th>               
    	  <th width="15%">Added Date</th>
        <th width="10%">Action</th>
    	</tr>
		</thead>
		<tbody>
    {if $db_alert|@count gt 0}
		{section name=i loop=$db_alert}
		  <tr>
		    <td style="padding-left:10px;"><a href="index.php?file=m-member&mode=edit&iMemberId={$db_alert[i].iMemberFromId}">{$db_alert[i].vFirstName} {$db_alert[i].vLastName}</a> &nbsp;( {$db_alert[i].eWas} )</td>
		    <td style="padding-left:10px;">{$db_alert[i].iRate} / 5</td>
		    <td style="padding-left:10px;">{if $db_alert[i].eSkill neq ''}{$db_alert[i].eSkill}{else}--{/if}</td>
		    <td style="padding-left:10px;">{$generalobj->DateTime($db_alert[i].dAddedDate,14)}</td>
        <td style="padding-left:10px;">
           <input type="button" value="Edit" class="btn" onclick="return edit_rate({$db_alert[i].iRateId});" title="Edit"/>
           <input type="button" value="Delete" class="btn" onclick="return delete_rate({$db_alert[i].iRateId});" title="Delete"/>
        </td>
		  </tr>
		  <tr>
		    <td style="padding-left:10px; border-bottom:1px solid #000000;" colspan="5">
		      <span><b>Review</b> :</span> &nbsp;{if $db_alert[i].tFeedback neq ''}{$db_alert[i].tFeedback}{else}--{/if}
		    </td>
		  </tr>  
    {/section}
    {else}
    <tr>
			<td height="70px;" colspan="5" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No Ratings & Reviews Available.</td>
		</tr>
    {/if}
		</tbody>
		</table>
   </div>
</div>
</div>
{literal}
<script>

function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
$(document).ready(function(){
$(function() {
    $( "#tabs2" ).tabs({ selected: 2 });
  });
});
function edit_rate(val){
  window.location="index.php?file=m-member_rate&mode=edit&iRateId="+val+"&iMemberId={/literal}{$iMemberId}{literal}";
  return false;
}
function delete_rate(val){
  var res=confirm("Are you sure for delete ?");
  if(res==false) return false;
  document.frm.iRateId.value=val;
  document.frm.submit();
}
</script>
{/literal}