<script language="JavaScript" src="{$tconfig.tcp_javascript}jlist.js"></script>
<div class="contentcontainer">
	<div class="headings altheading">
		<h2>Member Stories</h2>
	</div>
	<div class="contentbox">
    {if $var_msg_new neq ''}
     <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg_new}</p> 
     </div>     
    <div></div>
    {elseif $var_msg neq ''}
    <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg}</p> 
     </div>     
    <div></div>
    {/if}
		<form name="frmsearch" id="frmsearch" action="" method="post">
        
   <table width="100%" border="0">
		<tbody>
			<tr>
				<td width="10%"><label for="textfield"><strong>Search:</strong></label><td>
				<td width="10%"><input type="Text" id="keyword" name="keyword" value="{$keyword}"  class="inputbox" /><td>
				<td width="10%" >
					<select name="option" id="option">
						<option  value='ms.vTitle' {if $option eq 'vTitle'}selected {/if}>Title</option>
						<option  value='concat(m.vFirstName,m.vLastName)' {if $option eq 'concat(m.vFirstName,m.vLastName)'}selected {/if}>Member</option>
						<option  value="ms.eType"{if $option eq 'eType'}selected{/if}>Type</option>
						<option value="ms.eStatus"{if $option eq 'eStatus'}selected{/if}>Status</option>
					</select>
				</td>
				<td width="60%"><input type="button" value="Search" class="btnalt" id="Search" name="Search" title="Search" onclick="Searchoption();"/></td>
				<td width="10%"><input type="button" value="Add New" onclick="Redirect('index.php?file=m-member_stories&mode=add');" class="btnalt" /></td>
			</tr>	
			<tr>
				<td colspan="7" align="center">
					{$AlphaBox}
				</td>
			</tr>
		</tbody>			
		</table> 
        </form>
        <form name="frmlist" id="frmlist"  action="index.php?file=m-member_stories_a" method="post">
        
		<table width="100%" border="0">
		<input type="hidden" name="action" id="action" value="" />
        <input  type="hidden" name="iMemberStoriesId" value=""/>
        <thead>
			<tr>
				<th width="15%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=m-member_stories&mode=view&sortby=1&order={if $order eq 0}1{else}0{/if}');">Title</a>{if $sortby eq 1}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>               
        <th width="15%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=m-member_stories&mode=view&sortby=3&order={if $order eq 0}1{else}0{/if}');">Member</a>{if $sortby eq 3}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>
        <th width="15%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=m-member_stories&mode=view&sortby=2&order={if $order eq 0}1{else}0{/if}');">Type</a>{if $sortby eq 2}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>               
        <th width="15%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=m-member_stories&mode=view&sortby=6&order={if $order eq 0}1{else}0{/if}');">Added Date</a>{if $sortby eq 6}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/down_active.png{else}icons/up_active.png{/if}">{/if}</th>
        <th width="20%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=m-member_stories&mode=view&sortby=4&order={if $order eq 0}1{else}0{/if}');">Status</a>{if $sortby eq 4}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>
       	<th width="15%">Action</th>
				<th width="5%"><input type="checkbox" id="check_all" name="check_all" onclick="checkAll(document.frmlist);"/></th>
			</tr>
		</thead>
		<tbody>
    {if $db_admin_all|@count gt 0}
		{section name=i loop=$db_admin_all}
      <tr>
			<td><a href="{$tconfig.tpanel_url}/index.php?file=m-member_stories&mode=edit&iMemberStoriesId={$db_admin_all[i].iMemberStoriesId}" title="">{$db_admin_all[i].vTitle}</a></td>
			<td>{$db_admin_all[i].vFirstName} {$db_admin_all[i].vLastName}</td>
      <td>{$db_admin_all[i].eType}</td>		
      <td>{$generalobj->DateTime($db_admin_all[i].dAddedDate,9)}</td>		
			<td>{$db_admin_all[i].eStatus}</td>
			<td>
				<a href="{$tconfig.tpanel_url}/index.php?file=m-member_stories&mode=edit&iMemberStoriesId={$db_admin_all[i].iMemberStoriesId}" title=""><img src="{$tconfig.tpanel_img}icons/icon_edit.png" title="Edit" /></a>
				<a href="javascript:void(0);" title="Active" onclick="MakeAction('{$db_admin_all[i].iMemberStoriesId}','Active');"><img src="{$tconfig.tpanel_img}icons/icon_approve.png" title="Active" /></a>
				<a href="javascript:void(0);" title="Inactive" onclick="MakeAction('{$db_admin_all[i].iMemberStoriesId}','Inactive');"><img src="{$tconfig.tpanel_img}icons/icon_unapprove.png" title="Inactive" /></a>
				<a href="javascript:void(0);" title="Delete" onclick="MakeAction('{$db_admin_all[i].iMemberStoriesId}','Deletes');"><img src="{$tconfig.tpanel_img}icons/icon_delete.png" title="Delete" /></a>
			</td>
			<td><input name="iMemberStoriesId[]" type="checkbox" id="iId" value="{$db_admin_all[i].iMemberStoriesId}"/></td>
		</tr>
    {/section}
    {else}
    <tr>
			<td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No Record Found.</td>
		</tr>
    {/if}
		</tbody>
		</table>
    </form>
		<div class="extrabottom">
			<ul>
				<li><img src="{$tconfig.tpanel_img}icons/icon_edit.png" alt="Edit" /> Edit</li>
				<li><img src="{$tconfig.tpanel_img}icons/icon_approve.png" alt="Approve" /> Active</li>
				<li><img src="{$tconfig.tpanel_img}icons/icon_unapprove.png" alt="Unapprove" /> Inactive</li>
				<li><img src="{$tconfig.tpanel_img}icons/icon_delete.png" alt="Delete" /> Remove</li>
			</ul>
			<div class="bulkactions">
				<select name="newaction" id="newaction">
					<option value="">Select Action</option>
					<option value="Active">Make Active</option>
					<option value="Inactive">Make Inactive</option>
					<option value="Deletes">Make Delete</option>
					<option value="Show All">Show All</option>
				</select>
				<input type="submit" value="Apply" class="btn" onclick="return Doaction(document.getElementById('newaction').value,'m-member_stories',document.frmlist);"/>
			</div>
		</div>
        <div>
          <div class="pagination">
          {if $db_admin_all|@count gt 0}
	          <span class="switch" style="float: left;">{$recmsg}</span>
	        {/if}
          </div>
          {$page_link}
        </div>
		<div style="clear: both;"></div></div>
</div>
{literal}
<script>
function Searchoption(){
    document.getElementById('frmsearch').submit();
}
function AlphaSearch(val){
    var alphavalue = val;
    var file = 'm-member_stories';
    window.location="index.php?file="+file+"&alp="+alphavalue+"&mode=view";
    return false;
}
function MakeAction(loopid,type){
if(type == 'Deletes')
    {
	   ans = confirm("Are you sure you want to delete?");
     if (ans == false)
      {
        return false;
      }
    }
    document.frmlist.iMemberStoriesId.value = loopid;
    document.frmlist.action.value = type;
	document.frmlist.submit();	
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
{/literal}