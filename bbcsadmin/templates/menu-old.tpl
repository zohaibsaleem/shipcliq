<ul class="sf-menu" id="example">
      <li>
         <a href="{$tconfig.tpanel_url}" style="border-right: 1px solid #525252;">Home</a>
      </li>
      <li>
				<a href="javascript:void(0);" style="border-right: 1px solid #525252;" class="sf-with-ul">Administrators <span class="sf-sub-indicator"> </span></a>
				<ul style="width:128px;">
					<li>
						<a href="{$tconfig.tpanel_url}/index.php?file=ad-administrator&mode=view">Administrator</a>
					</li>
						</ul>
			</li>
      <li>
				<a href="javascript:void(0);" style="border-right: 1px solid #525252;" class="sf-with-ul">Members Management<span class="sf-sub-indicator">  </span></a>
				<ul> 			 
					<li style="width:170px;"><a href="{$tconfig.tpanel_url}/index.php?file=m-member&mode=view">Members List</a></li>
          <li style="width:170px;"><a href="{$tconfig.tpanel_url}/index.php?file=m-member_messages&mode=view">Message Records</a></li>
				</ul>
			</li>	
     			<li>
				<a href="javascript:void(0);" style="border-right: 1px solid #525252;" class="sf-with-ul">Rides Management<span class="sf-sub-indicator">  </span></a>
				<ul> 			 
					<li style="width:170px;"><a href="{$tconfig.tpanel_url}/index.php?file=ri-rideslist&mode=view&paymenttype=all">Rides List</a></li>
				</ul>
			</li>	
      {if $smarty.const.PAYMENT_OPTION neq 'Contact'}
      <li>
				<a href="javascript:void(0);" style="border-right: 1px solid #525252;" class="sf-with-ul">Booking Payment<span class="sf-sub-indicator">  </span></a>
				<ul>					 
					<li style="width:170px;"><a href="{$tconfig.tpanel_url}/index.php?file=bo-bookings&mode=view">Rides Booking</a></li>
					{if $smarty.const.PAYMENT_OPTION eq 'PayPal'}
          <li style="width:170px;"><a href="{$tconfig.tpanel_url}/index.php?file=ri-member_payment&mode=view">Pay To Driver</a></li>
          {/if}										
				</ul>
			</li>
      {/if}	
      <li>
				<a href="javascript:void(0);" style="border-right: 1px solid #525252;" class="sf-with-ul">Report<span class="sf-sub-indicator">  </span></a>
				<ul>					 
					<li style="width:170px;"><a href="{$tconfig.tpanel_url}/index.php?file=r-member_reports&mode=view">Members Report</a></li>
          <li style="width:170px;"><a href="{$tconfig.tpanel_url}/index.php?file=r-rides_reports&mode=view">Rides Posted Report</a></li>										
				</ul>
			</li>	
					 	
      <li style="width:120px;">
				<a href="javascript:void(0);" style="border-right: 1px solid #525252;" class="sf-with-ul"><center>Utility</center ><span class="sf-sub-indicator"> </span></a>
				<ul style="width:160px;">
				  <li><a href="{$tconfig.tpanel_url}/index.php?file=u-make&mode=view"  title="">Car Make</a></li>
          <li><a href="{$tconfig.tpanel_url}/index.php?file=u-model&mode=view"  title="">Car Model</a></li>
          <li><a href="{$tconfig.tpanel_url}/index.php?file=u-color&mode=view"  title="">Car Colour</a></li>
          <li><a href="{$tconfig.tpanel_url}/index.php?file=u-car_type&mode=view"  title="">Car Type</a></li>
          <li><a href="{$tconfig.tpanel_url}/index.php?file=m-member_stories&mode=view">Members Stories</a></li>
          <li><a href="{$tconfig.tpanel_url}/index.php?file=u-media_partners&mode=view"  title="">Media Partners</a></li>
          <li><a href="{$tconfig.tpanel_url}/index.php?file=u-email_templates&mode=view"  title="">Email templates</a></li>
					<li><a href="{$tconfig.tpanel_url}/index.php?file=u-faqcategory&mode=view" title="">FAQ Category</a></li>
          <li><a href="{$tconfig.tpanel_url}/index.php?file=u-faq&mode=view" title="">FAQ</a></li> 
		  <li><a href="{$tconfig.tpanel_url}/index.php?file=to-homebanners&mode=view">Home Banners</a></li> 		
          <li><a href="{$tconfig.tpanel_url}/index.php?file=to-banners&mode=view">Banners</a></li> 		
          <li><a href="{$tconfig.tpanel_url}/index.php?file=u-currency&mode=view">Currency Rates</a></li>
          <li><a href="{$tconfig.tpanel_url}/index.php?file=u-newsletter_subscriber&mode=view">Newsletter Subscriber</a></li> 				 							
			 <li><a class="last" href="{$tconfig.tpanel_url}/index.php?file=u-preferences&mode=view">Manage Preferences</a></li>
         	</ul>
			</li>					
		  <li>
				<a href="javascript:void(0);" style="border-right: 1px solid #525252;" class="sf-with-ul">Localization<span class="sf-sub-indicator"> </span></a>
				<ul>
					<li>
						<a href="{$tconfig.tpanel_url}/index.php?file=l-country&mode=view">Country</a>
					</li>
					<li>
						<a href="{$tconfig.tpanel_url}/index.php?file=l-state&mode=view">Province</a>
					</li> 
          <li>
						<a href="{$tconfig.tpanel_url}/index.php?file=l-city&mode=view">City</a>
					</li>										
				</ul>
			</li>
      <li>
				<a href="javascript:void(0);" style="border-right: 1px solid #525252;" class="sf-with-ul">Settings<span class="sf-sub-indicator">  </span></a>
				<ul style="width:150px;">
					<li>
						<a href="{$tconfig.tpanel_url}/index.php?file=to-generalsettings&mode=edit">General Configuration</a>
					</li> 					
          <li><a href="{$tconfig.tpanel_url}/index.php?file=to-languagelabel&mode=view" title="">Language Label</a></li>
          <li>
						<a href="{$tconfig.tpanel_url}/index.php?file=pg-staticpages&mode=view">Static Pages</a>
					</li>
             							
				</ul>
			</li>	
    <!--  <li>
				<a href="{$tconfig.tpanel_url}/index.php?file=au-logout" style="border-right: 1px solid #525252;" class="sf-with-ul">Logout<span class="sf-sub-indicator"> �</span></a>
			</li>-->	
     <div style="clear:both;"></div>	       
</ul>
