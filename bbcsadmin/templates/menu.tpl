<ul class="sf-menu" id="example">
	<li>
		<a href="{$tconfig.tpanel_url}" style="border-right: 1px solid #525252;">Home</a>
	</li>
	{if in_array('ad-administrator', $modlist) or in_array('ad-languagemaster', $modlist)}
	<li>
		<a href="javascript:void(0);" style="border-right: 1px solid #525252;" class="sf-with-ul">Administrators <span class="sf-sub-indicator"> </span></a>
		<ul style="width:128px;">
			{if in_array('ad-administrator', $modlist)}
			<li>
				<a href="{$tconfig.tpanel_url}/index.php?file=ad-administrator&mode=view">Administrator</a>
			</li>
			{/if}
			{if in_array('ad-languagemaster', $modlist)}
			<li>
				<a href="{$tconfig.tpanel_url}/index.php?file=ad-languagemaster&mode=view">Language</a>
			</li>
			{/if}
		</ul>
	</li>
	{/if}
	{if in_array('m-member', $modlist) or in_array('m-member_messages', $modlist)}
	<li>
		<a href="javascript:void(0);" style="border-right: 1px solid #525252;" class="sf-with-ul">Members Management<span class="sf-sub-indicator">  </span></a>
		<ul>
			{if in_array('m-member', $modlist)}
			<li style="width:170px;"><a href="{$tconfig.tpanel_url}/index.php?file=m-member&mode=view">Members List</a></li>
			{/if}
			{if in_array('m-member_messages', $modlist)}
			<li style="width:170px;"><a href="{$tconfig.tpanel_url}/index.php?file=m-member_messages&mode=view">Message Records</a></li>
			{/if}
		</ul>
	</li>
	{/if}
	{if in_array('ri-rideslist', $modlist) or in_array('bo-bookings', $modlist) or in_array('ri-member_payment', $modlist)}
	<li>
		<a href="javascript:void(0);" style="border-right: 1px solid #525252;" class="sf-with-ul">Trip Management<span class="sf-sub-indicator">  </span></a>
		<ul>
			{if in_array('ri-rideslist', $modlist)}
			<li style="width:170px;"><a href="{$tconfig.tpanel_url}/index.php?file=ri-rideslist&mode=view&paymenttype=all">Trip List</a></li>
			{/if}
			{if in_array('bo-bookings', $modlist)}
			<li style="width:170px;"><a href="{$tconfig.tpanel_url}/index.php?file=bo-bookings&mode=view">Trip Booking</a></li>
			{/if}
			{if $smarty.const.PAYMENT_OPTION neq 'Contact'}
				{if $smarty.const.PAYMENT_OPTION eq 'PayPal' and in_array('ri-member_payment', $modlist)}
				<li style="width:170px;"><a href="{$tconfig.tpanel_url}/index.php?file=ri-member_payment&mode=view">Pay To Traveller</a></li>
				{/if}
			{/if}

		</ul>
	</li>
	{/if}


	{if in_array('r-member_reports', $modlist) or in_array('r-rides_reports', $modlist) or in_array('r-payment_reports', $modlist) or in_array('r-money_reports', $modlist)}
	<li>
		<a href="javascript:void(0);" style="border-right: 1px solid #525252;" class="sf-with-ul">Report<span class="sf-sub-indicator">  </span></a>
		<ul>
			{if in_array('r-member_reports', $modlist)}
			<li style="width:170px;"><a href="{$tconfig.tpanel_url}/index.php?file=r-member_reports&mode=view">Members Report</a></li>
			{/if}
			{if in_array('r-rides_reports', $modlist)}
			<li style="width:170px;"><a href="{$tconfig.tpanel_url}/index.php?file=r-rides_reports&mode=view">Trip Posted Report</a></li>
			{/if}
			{if in_array('r-money_reports', $modlist)}
			<li style="width:170px;"><a href="{$tconfig.tpanel_url}/index.php?file=r-money_reports&mode=view">Money Report</a></li>
			{/if}
			{if in_array('r-payment_reports', $modlist)}
			<li style="width:170px;"><a href="{$tconfig.tpanel_url}/index.php?file=r-payment_reports&mode=view">Payment Report</a></li>
			{/if}
		</ul>
	</li>
	{/if}
	{if in_array('m-member_stories', $modlist) or in_array('u-media_partners', $modlist) or in_array('u-email_templates', $modlist) or in_array('u-faqcategory', $modlist) or in_array('u-faq', $modlist) or in_array('to-homebanners', $modlist) or in_array('to-banners', $modlist) or in_array('u-currency', $modlist) or in_array('u-newsletter_subscriber', $modlist)}
	<li style="width:120px;">
		<a href="javascript:void(0);" style="border-right: 1px solid #525252;" class="sf-with-ul"><center>Utility</center ><span class="sf-sub-indicator"> </span></a>
		<ul style="width:160px;">
			{if in_array('m-member_stories', $modlist)}	
			<li><a href="{$tconfig.tpanel_url}/index.php?file=m-member_stories&mode=view">Members Stories</a></li>
			{/if}
			{if in_array('u-media_partners', $modlist)}
			<li><a href="{$tconfig.tpanel_url}/index.php?file=u-media_partners&mode=view"  title="">Media Partners</a></li>
			{/if}
			{if in_array('u-email_templates', $modlist)}
			<li><a href="{$tconfig.tpanel_url}/index.php?file=u-email_templates&mode=view"  title="">Email templates</a></li>
			{/if}
			{if in_array('u-faqcategory', $modlist)}
			<li><a href="{$tconfig.tpanel_url}/index.php?file=u-faqcategory&mode=view" title="">FAQ Category</a></li>
			{/if}
			{if in_array('u-faq', $modlist)}
			<li><a href="{$tconfig.tpanel_url}/index.php?file=u-faq&mode=view" title="">FAQ</a></li>
			{/if}
			{if in_array('to-homebanners', $modlist)}
			<li><a href="{$tconfig.tpanel_url}/index.php?file=to-homebanners&mode=view">Home Banners</a></li>
			{/if}
			{if in_array('to-banners', $modlist)}
			<li><a href="{$tconfig.tpanel_url}/index.php?file=to-banners&mode=view">Adv. Banners</a></li>
			{/if}
			{if in_array('u-currency', $modlist)}
			<li><a href="{$tconfig.tpanel_url}/index.php?file=u-currency&mode=view">Currency Rates</a></li>
			{/if}
			{if in_array('u-newsletter_subscriber', $modlist)}
			<li><a href="{$tconfig.tpanel_url}/index.php?file=u-newsletter_subscriber&mode=view">Newsletter Subscriber</a></li>
			{/if}

			<!---{if in_array('u-preferences', $modlist)}
				<li><a class="last" href="{$tconfig.tpanel_url}/index.php?file=u-preferences&mode=view">Manage Preferences</a></li>
			{/if}--->
		</ul>
	</li>
	{/if}
	{if in_array('l-country', $modlist) or in_array('l-state', $modlist) or in_array('l-city', $modlist)}
	<li>
		<a href="javascript:void(0);" style="border-right: 1px solid #525252;" class="sf-with-ul">Localization<span class="sf-sub-indicator"> </span></a>
		<ul>
			{if in_array('l-country', $modlist)}
			<li>
				<a href="{$tconfig.tpanel_url}/index.php?file=l-country&mode=view">Country</a>
			</li>
			{/if}
			{if in_array('l-state', $modlist)}
			<li>
				<a href="{$tconfig.tpanel_url}/index.php?file=l-state&mode=view">Province</a>
			</li>
			{/if}
			{if in_array('l-city', $modlist)}
			<li>
				<a href="{$tconfig.tpanel_url}/index.php?file=l-city&mode=view">City</a>
			</li>
			{/if}
		</ul>
	</li>
	{/if}
	{if in_array('to-generalsettings', $modlist) or in_array('to-languagelabel', $modlist) or in_array('pg-staticpages', $modlist)}
	<li>
		<a href="javascript:void(0);" style="border-right: 1px solid #525252;" class="sf-with-ul">Settings<span class="sf-sub-indicator"></span></a>
		<ul style="width:150px;">
			{if in_array('to-generalsettings', $modlist)}
			<li>
				<a href="{$tconfig.tpanel_url}/index.php?file=to-generalsettings&mode=edit">General Configuration</a>
			</li>
			{/if}
			{if in_array('to-languagelabel', $modlist)}
			<li>
				<a href="{$tconfig.tpanel_url}/index.php?file=to-languagelabel&mode=view" title="">Language Label</a>
			</li>
			{/if}
			{if in_array('pg-staticpages', $modlist)}
			<li>
				<a href="{$tconfig.tpanel_url}/index.php?file=pg-staticpages&mode=view">Static Pages</a>
			</li>
			{/if}
		</ul>
	</li>
	{/if}
    <li>
		<a href="{$tconfig.tpanel_url}/index.php?file=au-logout" style="border-right: 1px solid #525252;" class="sf-with-ul">Logout</a>
	</li>
    <div style="clear:both;"></div>
</ul>
