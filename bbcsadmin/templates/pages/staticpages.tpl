<script language="JavaScript" src="{$tconfig.tcp_javascript}validate.js"></script>  
<div class="contentcontainer" id="tabs">
	<div class="headings">
		{if $mode eq 'edit'}
			<h2 class="left">Edit Page ({$db_pages[0].vPageName})</h2>
        {else}
			<h2 class="left">Add Page</h2>
        {/if}
	</div>
	<div class="contentbox" id="tabs-1">
		{if $smarty.get.var_msg neq ""}
		<div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$smarty.get.var_msg}</p> 
     </div>
     {/if}
     {if $smarty.get.var_succ_msg neq ""}
		<div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$smarty.get.var_succ_msg}</p> 
     </div>
     {/if}
     	{if $var_err_msg neq ""}
		<div class="status error" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Error" />
              {$var_err_msg}</p> 
     </div>
     {/if}  
    <form id="frmadd" name="frmadd" action="index.php?file=pg-staticpages_a" method="post" enctype="multipart/form-data">
            <input type="hidden" name="iPageId" id="iPageId" value="{$iPageId}" />
            <input type="hidden" name="action" id="action" value="{$mode}" />
            <input type="hidden" name="vOldImage" id="vOldImage" value="{$db_pages[0].vImage}" />
           <input type="hidden" name="imagemode" id="imagemode" /> 
        <p>
					<label for="textfield"><strong>Page/Section :</strong></label>
					<input type="text" id="vPageName" name="Data[vPageName]" class="inputbox" value="{$db_pages[0].vPageName}" lang="*" title="Page/Section Name"/>
				</p>
				<p>
				
				{section name='rrb' loop=$db_languagemas}              
  			<p>
  				<label for="textfield"><strong>Car Type [{$db_languagemas[rrb].vTitle}]:</strong></label>
  				<input type="text" id="vPageTitle_{$db_languagemas[rrb].vCode}" name="vPageTitle_{$db_languagemas[rrb].vCode}" class="inputbox" value="{$db_languagemas[rrb].ttl}"/>
        </p>
  			
  			<p>
  				<label for="textfield"><strong>Page Description [{$db_languagemas[rrb].vTitle}] :</strong></label>
  				{$db_languagemas[rrb].editor}
  			</p>    			
        {/section}   
			
			{if $iPageId eq 7}
				<p>
				  <!--<label for="textfield"><strong>Vimeo Video ID :</strong></label>
          <input type="text" id="iFrameCode" name="Data[iFrameCode]" class="inputbox" value="{$db_pages[0].iFrameCode}" lang="*" title="Vimeo Video ID"/>-->
          <label for="textfield"><strong>Video IFrame :</strong></label>
					<textarea name='Data[iFrameCode]' id="iFrameCode"  style="width: 320px; height: 51px;" class="inputbox"  title="Video IFrame">{$db_pages[0].iFrameCode}</textarea>
			 </p>	
			{/if}	
      {if $iPageId eq 8 or $iPageId eq 13 or $iPageId eq 14 or $iPageId eq 15}
        <p>
				  <label for="textfield"><strong>Upload Image :</strong></label>
					<input type="file" name=Data[vImage] id=vImage class="inputbox" titile="Image">
					{if $iPageId eq 8}
          <br>[ Note : For good result use image of width : 289px , Height : 111px ]
          {else}
          <br>[ Note : For good result use image of width : 250px , Height : 276px ]
          {/if}
			    <br><br>
			    {if $img eq yes}
			     <img src= "{$tconfig.tsite_upload_images_media_partners}{$db_pages[0].vImage}" /> <br><br>
			     <input type="button" value="Delete Image" class="btn" onclick="return confirm_delete();" title="Delete Image"/>
          {/if}
         </p>	
			   
		  {/if}			
			  
        <p>
				<label for="textfield"><strong>Meta Title :</strong></label>
				<input type="text" name="Data[vTitle]" id="tMetaTitle" value="{$db_pages[0].vTitle}" class="inputbox"  title="Meta Title" />
			</p>	
      <p>
				<label for="textfield"><strong>Meta Keywords :</strong></label>
				<input type="text" name="Data[tMetaKeyword]" id="tMetaKeyWords" value="{$db_pages[0].tMetaKeyword}" class="inputbox"  title="Meta Keywords" />
			</p>							
			 <p>
				  <label for="textfield"><strong>Meta Description :</strong></label>
					<textarea name="Data[tMetaDescription]" id="tMetaDescription"  style="width: 320px; height: 51px;"   value="{$db_pages[0].tMetaDescription}"  title="Meta Description">{$db_pages[0].tMetaDescription}</textarea>
			 </p>	
			 
			  <p>
				  <label for="textfield"><strong>Status :</strong></label>
				  <select name="Data[eStatus]" id="eStatus"   title="Status">{$db_pages[0].eStatus}
					 <option value="Active" {if $db_pages[0].eStatus eq 'Active'} selected {/if}>Active</option>
					 <option value="Inactive" {if $db_pages[0].eStatus eq 'Inactive'} selected {/if}>Inactive</option>
				  </select>
			 </p>	
			 
				<input type="submit" value="Save Static Page" class="btn" onclick="return validate(document.frmadd);" title="Save Static Page"/>
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
			</form>
	</div>
</div>
{literal}
<script>
function redirectcancel()
{
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=pg-staticpages&mode=view";
    return false;      
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
function confirm_delete()
{
   ans = confirm('Are you sure Delete Image?');
   if(ans == true){
    document.frmadd.imagemode.value = 'delete_image';
    document.frmadd.submit();
   }else{
    return false;
   }
}


</script>
{/literal}