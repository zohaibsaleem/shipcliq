<script language="JavaScript" src="{$tconfig.tcp_javascript}jlist.js"></script>
<div class="contentcontainer">
	<div class="headings altheading">
		<h2>Static Pages</h2>
	</div>
	<div class="contentbox">
    {if $smarty.get.var_msg_new neq ''}
     <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$smarty.get.var_msg_new}</p> 
     </div>     
    <div></div>
    {elseif $smarty.get.var_msg neq ''}
    <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$smarty.get.var_msg}</p> 
     </div>     
    <div></div>
    {/if}
	<br /><br />
		<form name="frmlist" id="frmlist"  action="index.php?file=pg-staticpages_a" method="post">
        
		<table width="100%" border="0">
		<input type="hidden" name="action" id="action" value="" />
        <input  type="hidden" name="iPageId" value=""/>
        <thead>
			<tr>
			 <th width="30%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=pg-staticpages&mode=view&sortby=1&order={if $order eq 0}1{else}0{/if}');">Name</a>{if $sortby eq 1}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th> 
			 <th>Page Title</th>
              <th width="20%">Status</th>			 
             <th>Action</th>
			
			</tr>
		</thead>
		<tbody>
        {if $db_pages_all|@count gt 0}
		{section name=i loop=$db_pages_all}
        <tr>
			<td><a href="{$tconfig.tpanel_url}/index.php?file=pg-staticpages&mode=edit&iPageId={$db_pages_all[i].iPageId}" title="">{$db_pages_all[i].vPageName}</a></td>
			<td>{$db_pages_all[i].vPageTitle_EN}</td>
			<td>{$db_pages_all[i].eStatus}</td>
			<td>
			{if $db_pages_all[i].eStatus eq 'Inactive'}
				<a href="javascript:void(0);" title="Active" onclick="MakeAction('{$db_pages_all[i].iPageId}','Active');"><img src="{$tconfig.tpanel_img}icons/icon_approve.png" title="Active" /></a>
		    {/if}	
			{if $db_pages_all[i].eStatus eq 'Active'}
				<a href="javascript:void(0);" title="Inactive" onclick="MakeAction('{$db_pages_all[i].iPageId}','Inactive');"><img src="{$tconfig.tpanel_img}icons/icon_unapprove.png" title="Inactive" /></a>
			{/if}
			</td>
			
		</tr>
        {/section}
        {else}
        <tr>
			<td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No Record Found.</td>
		</tr>
        {/if}
		</tbody>
		</table>
        </form>
		<div style="clear: both;"></div></div>
</div>
{literal}
<script>
function Searchoption(){
    document.getElementById('frmsearch').submit();
}
function AlphaSearch(val){
    var alphavalue = val;
    var file = 'pg-staticpages';
    window.location="index.php?file="+file+"&alp="+alphavalue+"&mode=view";
    return false;
}
function MakeAction(loopid,type){
    document.frmlist.iPageId.value = loopid;
    document.frmlist.action.value = type;
	document.frmlist.submit();	
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
{/literal}