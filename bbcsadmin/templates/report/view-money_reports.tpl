<link type="text/css" rel="stylesheet" href="{$tconfig.tsite_javascript}dhtmlgoodies_calendar.css" media="screen">
<script type="text/javascript" src="{$tconfig.tsite_javascript}dhtmlgoodies_calendar.js"></script>
<script language="JavaScript" src="{$tconfig.tcp_javascript}jlist.js"></script>
<div class="contentcontainer">
	<div class="headings altheading">
		<h2>Payment Report</h2>
	</div>
	<div class="contentbox">
		{if $var_msg_new neq ''}
		<div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
			{$var_msg_new}</p> 
		</div>     
		<div></div>
		{elseif $var_msg neq ''}
		<div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
			{$var_msg}</p> 
		</div>     
		<div></div>
		{/if}
		<form name="frmlist" id="frmlist" method="post" action="">
			<input type="hidden" id="action1" name="action1" value="datesearch">  
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tbody>
					<tr>
						<td width="50%" class="td-listing" colspan="3">&nbsp;&nbsp;&nbsp;<font class="errormsg">&nbsp;</font></td>
					</tr>
					<tr>	
						<td width="1%">&nbsp;</td>
						<td width="59%" valign="top">
							<b>Search Bookings by date</b>
							<table width="100%" cellspacing="1" cellpadding="1" border="0">
								<tbody>
									<tr>
										<td align="right" width="12%" valign="middle" height="35">Date From </td>
										<td width="1%" valign="middle"> :</td>
										<td align="left" width="26%" valign="middle">
											<input type="text" Readonly id="dFDate" name="dFDate" style="width:100px;" class="inputbox" value="{$startdate}" lang="" title="Start Date"/>&nbsp;<img src="{$tconfig.tsite_images}cal-icon.gif" style="cursor:pointer" onclick="displayCalendar(document.getElementById('dFDate'),'yyyy-mm-dd',this)" />
										</td>
										<td align="right" width="9%" valign="middle">Date To</td>
										<td width="1%" valign="middle"> :</td>
										<td align="left" width="35%" valign="middle">
											<input type="text" Readonly id="dTDate" name="dTDate" style="width:100px;" class="inputbox" value="{$enddate}" lang="" title="End Date"/>&nbsp;<img src="{$tconfig.tsite_images}cal-icon.gif" style="cursor:pointer" onclick="displayCalendar(document.getElementById('dTDate'),'yyyy-mm-dd',this)" />
										</td>
									</tr>
									<tr>
										<td align="left" height="25" colspan="6">
											<b>Search Members by time period</b>
										</td>
									</tr>
									<tr>
										<td align="left" colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<img width="6" height="8" src="images/arrow.gif">&nbsp;&nbsp;[<a onclick="return todayDate('dFDate','dTDate');" href="javascript:void(0);">Today</a>]
											[<a onclick="return yesterdayDate('dFDate','dTDate');" href="javascript:void(0);">Yesterday</a>]
											<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img width="6" height="8" src="images/arrow.gif">&nbsp;&nbsp;[<a onclick="return currentweekDate('dFDate','dTDate');" href="javascript:void(0);">Current Week</a>]
											[<a onclick="return previousweekDate('dFDate','dTDate');" href="javascript:void(0);">Previous Week</a>]
											<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img width="6" height="8" src="images/arrow.gif">&nbsp;
											[<a onclick="return currentmonthDate('dFDate','dTDate');" href="javascript:void(0);">Current Month</a>]
											[<a onclick="return previousmonthDate('dFDate','dTDate');" href="javascript:void(0);">Previous Month</a>]
											<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	<img width="6" height="8" src="images/arrow.gif">&nbsp;
											[<a onclick="return currentyearDate('dFDate','dTDate');" href="javascript:void(0);">Current Year</a>] 
											[<a onclick="return previousyearDate('dFDate','dTDate');" href="javascript:void(0);">Previous Year</a>]
										</td>
									</tr>
								</tbody>
							</table>
						</td>
						<td width="40%">
							<b>Show only Refund</b><br><br> 										 
							<!--<input type="Checkbox" value="Active" name="sta[]" {if in_array('Active', $sta)}checked="checked"{/if}> Active   <br>
								<input type="Checkbox" value="Inactive" name="sta[]" {if in_array('Inactive', $sta)}checked="checked"{/if} > Inactive   <br>
							<input type="Checkbox" value="Pending" name="sta[]" {if in_array('Pending', $sta)}checked="checked"{/if}> Pending   <br> -->
							<input type="Checkbox" value="PRefund" name="psta[]" {if in_array('PRefund', $psta)}checked="checked"{/if}> Passenger Refund <br>
							<input type="Checkbox" value="DRefund" name="psta[]" {if in_array('DRefund', $psta)}checked="checked"{/if}> Driver Refund <br>
							<input type="Checkbox" value="Paid" name="psta[]" {if in_array('Paid', $psta)}checked="checked"{/if}> Paid <br>
							<input type="Checkbox" value="Unpaid" name="psta[]" {if in_array('Unpaid', $psta)}checked="checked"{/if}> Unpaid <br>
						</td>
					</tr>
					<tr>
						<td width="5%">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="3">
							<table align="center" width="100%" border="0">
								<tbody>
									<tr>
										<td width="45%">&nbsp;</td>	
										<td width="55%">
											<!--<input type="submit" value="Search" class="btn" onclick="return checkvalid(document.frmadd);" title="Search"/> -->	
											<input type="button" value="Search" class="btn" onclick="return checkvalid(1);" title="Search"/>
											<input type="button" value="Export Member List" onclick="return checkvalid(2);" class="btnalt" />
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table> 
		</form>
		<form name="frmlistmain" id="frmlistmain"  action="index.php?file=m-member_a" method="post">        
			<table width="100%" border="0">
				<input type="hidden" name="action" id="action" value="" />
				<input  type="hidden" name="iBookingId" value=""/>
				<thead>
					<tr>
						<th nowrap><a href="javascript:void(0);" onClick="Redirect('index.php?file=r-money_reports&mode=view&sortby=1&order={if $order eq 0}1{else}0{/if}');">Booking No. #</a>{if $sortby eq 1}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>   
						<th>Member</th>   					
						<th nowrap><a href="javascript:void(0);" onClick="Redirect('index.php?file=r-money_reports&mode=view&sortby=3&order={if $order eq 0}1{else}0{/if}');">Booking Date</a>{if $sortby eq 3}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>
						<th><a href="javascript:void(0);" onClick="Redirect('index.php?file=r-money_reports&mode=view&sortby=4&order={if $order eq 0}1{else}0{/if}');">Trip Details</a>{if $sortby eq 4}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>
						<th><a href="javascript:void(0);" onClick="Redirect('index.php?file=r-money_reports&mode=view&sortby=6&order={if $order eq 0}1{else}0{/if}');">Amount</a>{if $sortby eq 6}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>
						<th><a href="javascript:void(0);" onClick="Redirect('index.php?file=r-money_reports&mode=view&sortby=8&order={if $order eq 0}1{else}0{/if}');">Commision</a>{if $sortby eq 8}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>
						<th><a href="javascript:void(0);" onClick="Redirect('index.php?file=r-money_reports&mode=view&sortby=9&order={if $order eq 0}1{else}0{/if}');">Refund</a>{if $sortby eq 9}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>
						<th>Cancellled By</th>
						<th>Payment Status</th>
					</tr>
				</thead>
				<tbody>
					{if $db_order_all|@count gt 0}            
					{section name=i loop=$db_order_all}
					<tr>
						<td style="height:50px;"><a target="_BLANK" href="{$tconfig.tpanel_url}/index.php?file=bo-bookings&mode=edit&iBookingId={$db_order_all[i].iBookingId}" title="">{$db_order_all[i].vBookingNo}</a></td>
						<td><a href="{$tconfig.tpanel_url}/index.php?file=m-member&mode=edit&iMemberId={$db_order_all[i].iDriverId}" target="_blank">{$db_order_all[i].vMemberName}</a></td>
						<td>{$generalobj->DateTime($db_order_all[i].dBookingDate,9)}</td>
						<td><a href="{$tconfig.tpanel_url}/index.php?file=ri-rideslist&mode=edit&iRideId={$db_order_all[i].iRideId}" target="_blank">{$db_order_all[i].vMainRidePlaceDetails}</a></td>
						<td>{$db_order_all[i].fAmount}</td>
						<td>{$db_order_all[i].fCommission}</td>
						<td>{$db_order_all[i].fSiteRefundAmount}</td>
						<td>{if $db_order_all[i].eCancelBy eq ''} Not Cancelled 
							{else}
							{$db_order_all[i].eCancelBy}
						{/if}</td>
						<td>
							{$db_order_all[i].status}
						</td>
					</tr>
					{/section}
					{else}
					<tr>
						<td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No Record Found.</td>
					</tr>
					{/if}
					<tr>
						<td colspan="7" width="80%" height="20" style="border-bottom:0px solid #cccccc;font-size:13px;text-align:right;"></td>
						<td colspan="2" width="20%" height="20" style="border-bottom:1px solid #cccccc;border-left:1px solid #cccccc;font-size:13px;text-align:right;">
							<strong>Total Amount: <span id="totpaymentspan">{$driver_currency} {$tot_driver_refund}</span></strong>
						</td>
					</tr>
					<tr>
						<td colspan="7" width="80%" height="20" style="border-bottom:0px solid #cccccc;font-size:13px;text-align:right;"></td>
						<td colspan="2" width="20%" height="20" style="border-bottom:1px solid #cccccc;border-left:1px solid #cccccc;font-size:13px;text-align:right;">
							<strong>Total Commision: <span id="totpaymentspan">{$driver_currency} {$tot_site_commission}</span></strong>
						</td>
					</tr>
					<tr>
						<td colspan="7" width="80%" height="20" style="border-bottom:0px solid #cccccc;font-size:13px;text-align:right;"></td>
						<td colspan="2" width="20%" height="20" style="border-bottom:1px solid #cccccc;border-left:1px solid #cccccc;font-size:13px;text-align:right;">
							<strong>Total Refund: <span id="totpaymentspan">{$driver_currency} {$tot_site_refund}</span></strong>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
		<div>
			<div class="pagination">
				{if $db_order_all|@count gt 0}
				<span class="switch" style="float: left;">{$recmsg}</span>
				{/if}
			</div>
			{$page_link}
		</div>
	<div style="clear: both;"></div></div>
</div>
{literal}
<script>
	function Searchoption(){
		document.getElementById('frmsearch').submit();
	}
	function AlphaSearch(val){
		var alphavalue = val;
		var file = 'r-member_reports';
		window.location="index.php?file="+file+"&alp="+alphavalue+"&mode=view";
		return false;
	}
	function MakeAction(loopid,type){
		document.frmlist.iMemberId.value = loopid;
		document.frmlist.action.value = type;
		document.frmlist.submit();	
	}
	function hidemessage(){
		jQuery("#errormsgdiv").slideUp();
	}
	function checkvalid(val){    
		if(document.frmlist.dTDate.value < document.frmlist.dFDate.value){
			alert("From date should be lesser than To date.")
			document.frmlist.dFDate.select();
			return false;
		}
		if(val==1){
			document.frmlist.action="index.php?file=r-money_reports&mode=view";
			document.frmlist.submit();	
			}else{
			document.frmlist.action="index.php?file=r-export_money_details&mode=edit";
			document.frmlist.submit();	
		}
	}
	
	function todayDate(dt,df)
	{
		document.frmlist.elements[dt].value='{/literal}{'Y-m-d'|date}{literal}';
		document.frmlist.elements[df].value='{/literal}{'Y-m-d'|date}{literal}'; 
	}
	function yesterdayDate(dt,df)
	{
		document.frmlist.elements[dt].value='{/literal}{$yesterday}{literal}';
		document.frmlist.elements[df].value='{/literal}{$yesterday}{literal}';	
	}
	function currentweekDate(dt,df)
	{ 
		document.frmlist.elements[dt].value='{/literal}{$currweekFDate}{literal}';
		document.frmlist.elements[df].value='{/literal}{$currweekTDate}{literal}';	
	}
	function previousweekDate(dt,df)
	{
		document.frmlist.elements[dt].value='{/literal}{$prevweekFDate}{literal}';
		document.frmlist.elements[df].value='{/literal}{$prevweekTDate}{literal}';	
	}
	function currentmonthDate(dt,df)
	{
		document.frmlist.elements[dt].value='{/literal}{$currmonthFDate}{literal}';
		document.frmlist.elements[df].value='{/literal}{$currmonthTDate}{literal}';
	}
	function previousmonthDate(dt,df)
	{
		document.frmlist.elements[dt].value='{/literal}{$prevmonthFDate}{literal}';
		document.frmlist.elements[df].value='{/literal}{$prevmonthTDate}{literal}';	
	}
	function currentyearDate(dt,df)
	{
		document.frmlist.elements[dt].value='{/literal}{$curryearFDate}{literal}';
		document.frmlist.elements[df].value='{/literal}{$curryearTDate}{literal}';	
	}
	function previousyearDate(dt,df)
	{
		document.frmlist.elements[dt].value='{/literal}{$prevyearFDate}{literal}';
		document.frmlist.elements[df].value='{/literal}{$prevyearTDate}{literal}';	
	}
	function checkAll()
	{ 
		var rs = (document.frmlist.abc.checked)?true:false;
		
		for(i=0;i<document.frmlist.elements.length;i++)
		{
			if(document.frmlist.elements[i].id == 'iId')
			{
				document.frmlist.elements[i].checked = rs;
			}
			
		}  
	}
</script>
{/literal}