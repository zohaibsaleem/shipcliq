<link type="text/css" rel="stylesheet" href="{$tconfig.tsite_javascript}dhtmlgoodies_calendar.css" media="screen">
<script type="text/javascript" src="{$tconfig.tsite_javascript}dhtmlgoodies_calendar.js"></script>
<script language="JavaScript" src="{$tconfig.tcp_javascript}jlist.js"></script>
<div class="contentcontainer">
	<div class="headings altheading">
		<h2>Organizations Report</h2>
	</div>
	<div class="contentbox">
    {if $var_msg_new neq ''}
     <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg_new}</p> 
     </div>     
    <div></div>
    {elseif $var_msg neq ''}
    <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg}</p> 
     </div>     
    <div></div>
    {/if}
		<form name="frmlist" method="post" action="index.php?file=r-organization_reports&mode=view" onsubmit="return checkvalid();">
      <input type="hidden" id="action" name="action" value="datesearch">  
       <table width="100%" cellspacing="0" cellpadding="0" border="0">
										<tbody><tr>
											<td width="50%" class="td-listing" colspan="3">&nbsp;&nbsp;&nbsp;<font class="errormsg">&nbsp;</font>
											</td>
										</tr>
										<tr>	
											<td width="1%">&nbsp;</td>
											<td width="59%" valign="top">
												<b>Search Organization by date</b>
												<table width="100%" cellspacing="1" cellpadding="1" border="0">
																										<tbody><tr>
														<td align="right" width="12%" valign="middle" height="35">Date From </td>
														<td width="1%" valign="middle"> :</td>
														<td align="left" width="26%" valign="middle">
															<input type="text" Readonly id="dFDate" name="dFDate" style="width:100px;" class="inputbox" value="{$startdate}" lang="" title="Start Date"/>&nbsp;<img src="{$tconfig.tsite_images}cal-icon.gif" style="cursor:pointer" onclick="displayCalendar(document.getElementById('dFDate'),'yyyy-mm-dd',this)" />
														</td>
														<td align="right" width="9%" valign="middle">Date To</td>
														<td width="1%" valign="middle"> :</td>
														<td align="left" width="35%" valign="middle">
															<input type="text" Readonly id="dTDate" name="dTDate" style="width:100px;" class="inputbox" value="{$enddate}" lang="" title="End Date"/>&nbsp;<img src="{$tconfig.tsite_images}cal-icon.gif" style="cursor:pointer" onclick="displayCalendar(document.getElementById('dTDate'),'yyyy-mm-dd',this)" />
														</td>
													</tr>
													<tr>
														<td align="left" height="25" colspan="6">
															<b>Search Organization by time period</b>
														</td>
													</tr>
													<tr>
														<td align="left" colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<img width="6" height="8" src="images/arrow.gif">&nbsp;&nbsp;[<a onclick="return todayDate('dFDate','dTDate');" href="javascript:void(0);">Today</a>]
															[<a onclick="return yesterdayDate('dFDate','dTDate');" href="javascript:void(0);">Yesterday</a>]
															<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img width="6" height="8" src="images/arrow.gif">&nbsp;&nbsp;[<a onclick="return currentweekDate('dFDate','dTDate');" href="javascript:void(0);">Current Week</a>]
															[<a onclick="return previousweekDate('dFDate','dTDate');" href="javascript:void(0);">Previous Week</a>]
															<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img width="6" height="8" src="images/arrow.gif">&nbsp;
															[<a onclick="return currentmonthDate('dFDate','dTDate');" href="javascript:void(0);">Current Month</a>]
															[<a onclick="return previousmonthDate('dFDate','dTDate');" href="javascript:void(0);">Previous Month</a>]
															<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	<img width="6" height="8" src="images/arrow.gif">&nbsp;
															[<a onclick="return currentyearDate('dFDate','dTDate');" href="javascript:void(0);">Current Year</a>] 
															[<a onclick="return previousyearDate('dFDate','dTDate');" href="javascript:void(0);">Previous Year</a>]
														</td>
													</tr>
												</tbody></table>
											</td>
											<td width="40%">
												<b>Show Organization in the following Status</b><br><br> 
											 
											 <!--<input type="Checkbox" value="Active" name="sta[]" {if in_array('Active', $sta)}checked="checked"{/if}> Active   <br>
                        <input type="Checkbox" value="Inactive" name="sta[]" {if in_array('Inactive', $sta)}checked="checked"{/if} > Inactive   <br>
                        <input type="Checkbox" value="Pending" name="sta[]" {if in_array('Pending', $sta)}checked="checked"{/if}> Pending   <br> -->
                        {$status_chk}
                        <br>
                        <b>Show Organization in the following Type</b><br><br>
                         {$status_chktype}
                      </td>
										</tr>
										<tr>
											<td width="5%">&nbsp;</td>
										</tr>
										<tr>
											<td colspan="3">
												<table align="center" width="100%" border="0">
													<tbody><tr>
														<td width="45%">&nbsp;</td>	
														<td width="55%">
														<input type="submit" value="Search" class="btn" onclick="return checkvalid(document.frmadd);" title="Search"/>	
														</td>
													</tr>
												</tbody></table>
											</td>
										</tr>
									
									</tbody></table> 
        </form>
    <form name="frmlistmain" id="frmlistmain"  action="index.php?file=m-organization_a" method="post">        
		<table width="100%" border="0">
		<input type="hidden" name="action" id="action" value="" />
        <input  type="hidden" name="iOrganizationId" value=""/>
        <thead>
			<tr>
				<th width="10%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=r-organization_reports&mode=view&sortby=1&order={if $order eq 0}1{else}0{/if}');">Name</a>{if $sortby eq 1}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>        
        	<th width="15%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=r-organization_reports&mode=view&sortby=3&order={if $order eq 0}1{else}0{/if}');">Email</a>{if $sortby eq 3}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>
        <th width="10%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=r-organization_reports&mode=view&sortby=4&order={if $order eq 0}1{else}0{/if}');">Added Date</a>{if $sortby eq 4}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>
         <th width="10%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=r-organization_reports&mode=view&sortby=5&order={if $order eq 0}1{else}0{/if}');">Type</a>{if $sortby eq 5}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>
	      <th width="10%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=r-organization_reports&mode=view&sortby=6&order={if $order eq 0}1{else}0{/if}');">Status</a>{if $sortby eq 6}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>
	</tr>
		</thead>
		<tbody>
        {if $db_order_all|@count gt 0}            
		{section name=i loop=$db_order_all}
        <tr>
			<td width="10%"><a href="{$tconfig.tpanel_url}/index.php?file=m-organization&mode=edit&iOrganizationId={$db_order_all[i].iOrganizationId}" title="">{$db_order_all[i].vOrganizationName}</a></td>		
      <td width="10%">{$db_order_all[i].vEmail}</td>         
			<td width="20%">{$generalobj->DateTime($db_order_all[i].dAddedDate,13)}</td>
			<td width="10%">{$db_order_all[i].eType}</td>
			<td width="10%">{$db_order_all[i].eStatus}</td>
			<!--<td width="5%"><input name="iMemberId[]" type="checkbox" id="iId" value="{$db_order_all[i].iMemberId}"/></td> -->
		</tr>
        {/section}
        {else}
        <tr>
			<td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No Record Found.</td>
		</tr>
        {/if}
		</tbody>
		</table>
        </form>
		<div class="extrabottom">  			
			<div class="bulkactions">
				<select name="newaction" id="newaction">
					<option value="">Select Action</option>
					<option value="Show All">Show All</option>
				</select>
				<input type="submit" value="Apply" class="btn" onclick="return Doaction(document.getElementById('newaction').value,'r-organization_reports',document.frmlist);"/>
			</div>
		</div>
        <div>
            <div class="pagination">
            {if $db_order_all|@count gt 0}
	        <span class="switch" style="float: left;">{$recmsg}</span>
	        {/if}
            </div>
            {$page_link}
        </div>
		
        <!--<ul class="pagination">
			<li class="text">Previous</li>
			<li class="page"><a href="#" title="">1</a></li>
			<li><a href="#" title="">2</a></li>
			<li><a href="#" title="">3</a></li>
			<li><a href="#" title="">4</a></li>
			<li class="text"><a href="#" title="">Next</a></li>
		</ul>-->
		<div style="clear: both;"></div></div>
</div>
{literal}
<script>
function Searchoption(){
    document.getElementById('frmsearch').submit();
}
function AlphaSearch(val){
    var alphavalue = val;
    var file = 'r-organization_reports';
    window.location="index.php?file="+file+"&alp="+alphavalue+"&mode=view";
    return false;
}
function MakeAction(loopid,type){
    document.frmlist.iOrganizationId.value = loopid;
    document.frmlist.action.value = type;
	document.frmlist.submit();	
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
function checkvalid(){

	if(document.frmlist.dTDate.value < document.frmlist.dFDate.value){
  		alert("From date should be lesser than To date.")
  		document.frmlist.dFDate.select();
		return false;
  	}
}

function todayDate(dt,df)
{
	document.frmlist.elements[dt].value='{/literal}{'Y-m-d'|date}{literal}';
	document.frmlist.elements[df].value='{/literal}{'Y-m-d'|date}{literal}'; 
}
function yesterdayDate(dt,df)
{
	document.frmlist.elements[dt].value='{/literal}{$yesterday}{literal}';
	document.frmlist.elements[df].value='{/literal}{$yesterday}{literal}';	
}
function currentweekDate(dt,df)
{ 
	document.frmlist.elements[dt].value='{/literal}{$currweekFDate}{literal}';
	document.frmlist.elements[df].value='{/literal}{$currweekTDate}{literal}';	
}
function previousweekDate(dt,df)
{
	document.frmlist.elements[dt].value='{/literal}{$prevweekFDate}{literal}';
	document.frmlist.elements[df].value='{/literal}{$prevweekTDate}{literal}';	
}
function currentmonthDate(dt,df)
{
	document.frmlist.elements[dt].value='{/literal}{$currmonthFDate}{literal}';
	document.frmlist.elements[df].value='{/literal}{$currmonthTDate}{literal}';
}
function previousmonthDate(dt,df)
{
	document.frmlist.elements[dt].value='{/literal}{$prevmonthFDate}{literal}';
	document.frmlist.elements[df].value='{/literal}{$prevmonthTDate}{literal}';	
}
function currentyearDate(dt,df)
{
	document.frmlist.elements[dt].value='{/literal}{$curryearFDate}{literal}';
	document.frmlist.elements[df].value='{/literal}{$curryearTDate}{literal}';	
}
function previousyearDate(dt,df)
{
	document.frmlist.elements[dt].value='{/literal}{$prevyearFDate}{literal}';
	document.frmlist.elements[df].value='{/literal}{$prevyearTDate}{literal}';	
}
function checkAll()
{ 
	var rs = (document.frmlist.abc.checked)?true:false;
	
	for(i=0;i<document.frmlist.elements.length;i++)
	{
	  	if(document.frmlist.elements[i].id == 'iId')
  		{
			document.frmlist.elements[i].checked = rs;
		}

	}  
}
</script>
{/literal}