<div class="contentcontainer" id="tabs">
	<div class="headings">         	
		<h2 class="left">Rides booking / Payment information of {$generalobj->DateTime($date,5)}</h2>          
	</div>
	<div class="contentbox" id="tabs-1">  
		{if $var_msg neq ''}
		
		<div class="status success" id="errormsgdiv"> 
			<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
			<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
			{$var_msg}</p> 
		</div> 
		{/if} 
		<form name="frmpaymenr" id="frmpaymenr" method="post" action="index.php?file=ri-ridepayment_a">
			<input type="hidden" name="mode" value="edit">
			<input type="hidden" name="iRideId_payment" value="{$iRideId}">
			<input type="hidden" name="date_payment" value="{$date}">
			<input type="hidden" name="action" value="payment">
			<input type="hidden" name="main_total" id="main_total" value="{$grandtotal}">
			<input type="hidden" name="commission" id="commission" value="{$itecommission}">
			<input type="hidden" name="sub_total" id="sub_total" value="{$totpayment}">  	
			<input type="hidden" name="drivercurrencycode" id="drivercurrencycode" value="{$db_rides[0].vBookerCurrencyCode}">
			<input type="hidden" name="eTripReturn" id="eTripReturn" value="{$eTripReturn}">
			<input type="hidden" name="fCommissionRate" id="fCommissionRate" value="{$db_rides[0].fCommissionRate}">
			<input type="hidden" name="iRideDateId" id="iRideDateId" value="{$iRideDateId}">
			<input type="hidden" name="iMemberId" id="iMemberId" value="{$db_rides[0].iMemberId}">
			
			<table class="headingbg" style="border: 1px solid #ccc;" border="0" cellpadding="1" cellspacing="1" width="100%"> 
				<thead>
					<tr>
						<th style="font-size:12px;text-align:left;" width="30%">Ride Details</th> 
						<th style="font-size:12px;text-align:left;" width="15%">Booking Amount</th> 
						<th style="font-size:12px;text-align:center;" width="15%">Paid To Traveler</th> 
						<th style="font-size:12px;text-align:center;" width="15%">Confirm By Booker</th> 
						<th style="font-size:12px;text-align:center;" width="5%">Pay</th>            
						<th style="font-size:12px;text-align:right;" width="20%">Traveler Payment</th>
					</tr> 
				</thead>   
				<tbody>   
					{section name="booking" loop=$db_bookings}      
					<tr>
						<td valign="top" style="border-bottom:1px solid #cccccc;font-size:13px;">
							<strong>Booking No.</strong> #{$db_bookings[booking].vBookingNo}
							<br><strong>Booker Name:</strong> {$db_bookings[booking].vBookerFirstName} {$db_bookings[booking].vBookerLastName}
							<br><strong>Trip of:</strong> {$db_bookings[booking].vFromPlace} &rarr; {$db_bookings[booking].vToPlace}
							<br><strong>Time:</strong> {$generalobj->DateTime($db_bookings[booking].dBookingTime,12)}
						</td> 
						<td valign="top" style="border-bottom:1px solid #cccccc;font-size:13px;">
							<strong>Booker Pay:</strong> {$db_bookings[booking].vBookerCurrencyCode} {$db_bookings[booking].fAmount}
						</td> 
						<td valign="top" style="border-bottom:1px solid #cccccc;font-size:13px;text-align:center;">
							{$db_bookings[booking].eDriverPaymentPaid}
						</td>     
						
						<td valign="top" style="border-bottom:1px solid #cccccc;font-size:13px;text-align:center;">
							{$db_bookings[booking].eBookerConfirmation}
						</td>
						<td valign="top" style="border-bottom:1px solid #cccccc;font-size:13px;text-align:center;">
							<input {if $db_bookings[booking].eDriverPaymentPaid eq 'Yes'} disabled {/if} type="checkbox" onClick="calculate_again({$smarty.section.booking.index}, {$db_bookings[booking].iBookingId});" name="confirm[]" id="confirm_{$db_bookings[booking].iBookingId}" {if $db_bookings[booking].eDriverPaymentPaid eq 'No'}{if $db_bookings[booking].eBookerConfirmation eq 'Yes' and $db_bookings[booking].eBookerPaymentPaid eq 'Yes'} checked {/if}{/if} value="{$db_bookings[booking].iBookingId}">
							<input type="hidden" name="amount_{$smarty.section.booking.index}_{$db_bookings[booking].iBookingId}" id="amount_{$smarty.section.booking.index}_{$db_bookings[booking].iBookingId}" value="{$db_bookings[booking].driveramount} ">
						</td>
						<td valign="top" style="border-bottom:1px solid #cccccc;font-size:13px;text-align:right;">        
							{$db_rides[0].vBookerCurrencyCode} {$db_bookings[booking].driveramount} 
							<input type="hidden" class="inputbox" name="" id="" value="{$db_bookings[booking].driveramount}" style="width:100px"> 
						</td>
					</tr>  
					{/section} 
					<tr>
						<td colspan="5" width="80%" height="20" style="border-bottom:0px solid #cccccc;font-size:13px;text-align:right;">
						</td>
						<td colspan="1" width="20%" height="20" style="border-bottom:1px solid #cccccc;border-left:1px solid #cccccc;font-size:13px;text-align:right;">
							<strong>Total Payment: {$db_rides[0].vBookerCurrencyCode} <span id="totpaymentspan">{$totpayment}</span></strong>
						</td>
					</tr>
					<!--<tr>
						<td colspan="5" width="80%" height="20" style="border-bottom:0px solid #cccccc;font-size:13px;text-align:right;"></td>
						<td colspan="1" width="20%" height="20" style="border-bottom:1px solid #cccccc;border-left:1px solid #cccccc;font-size:13px;text-align:right;">
						<strong>Site Commission ({$db_rides[0].fCommissionRate} %): {$db_rides[0].vBookerCurrencyCode} <span id="itecommissionspan">{$itecommission}</span></strong>
						</td>
						</tr>
						<tr>
						<td colspan="5" width="80%" height="20" style="border-bottom:0px solid #cccccc;font-size:13px;text-align:right;"></td>
						<td colspan="1" width="20%" height="20" style="border-bottom:1px solid #cccccc;border-left:1px solid #cccccc;font-size:13px;text-align:right;">
						<strong>Grand Total: {$db_rides[0].vBookerCurrencyCode} <span id="grandtotalspan">{$grandtotal}</span></strong>
						</td>
					</tr> -->
					<!--<tr>
						<td colspan="5" width="80%" height="20" style="border-bottom:0px solid #cccccc;font-size:13px;text-align:right;"></td>
						<td colspan="1" width="20%" height="20" style="border-bottom:0px solid #cccccc;border-left:1px solid #cccccc;font-size:13px;text-align:right;">
						{if $receveremail eq 'Yes'}
						<input type="button" value="Pay To Driver" class="btn" onClick="check_payment();" title="Pay To Driver"/>
						{else}
						Receiver payment email not found
						{/if}
						</td>
					</tr>-->
				</tbody>
			</table>
		</form>
	</div>
</div>  
{literal}
<script>
	function backtolist(){
		window.location="index.php?file=ri-rideslist&mode=view";
	}
	function edit_dealstatus(){
		document.frmadd.submit();
	} 
	
	var fCommissionRate = {/literal}{$fCommissionRate}{literal}
	
	function calculate_again(ind,id){
		if($("#confirm_"+id).is(':checked')){
			var r=confirm("Are you sure want to add this booking in traveler payment?");
			if (r==true)
			{
				var sub_total = $("#sub_total").val();
				var add_amt = $("#amount_"+ind+"_"+id).val();
				var nwsubtotal = Number(sub_total) + Number(add_amt); 
				$("#sub_total").val(nwsubtotal.toFixed(2));
				$("#totpaymentspan").html(nwsubtotal.toFixed(2));
				
				var commission = (nwsubtotal.toFixed(2) * Number(fCommissionRate)) / 100;
				$("#commission").val(commission.toFixed(2));
				$("#itecommissionspan").html(commission.toFixed(2));
				
				var main_total = Number(nwsubtotal) - Number(commission);
				$("#main_total").val(main_total.toFixed(2));
				$("#grandtotalspan").html(main_total.toFixed(2));
				
				
				}else{
				return false;
			} 
			}else{
			var r=confirm("Are you sure want to remove this booking in traveler payment?");
			if (r==true)
			{
				var sub_total = $("#sub_total").val();
				var add_amt = $("#amount_"+ind+"_"+id).val();
				var nwsubtotal = Number(sub_total) - Number(add_amt); 
				$("#sub_total").val(nwsubtotal.toFixed(2));
				$("#totpaymentspan").html(nwsubtotal.toFixed(2));
				
				var commission = (nwsubtotal.toFixed(2) * Number(fCommissionRate)) / 100;
				$("#commission").val(commission.toFixed(2));
				$("#itecommissionspan").html(commission.toFixed(2));
				
				var main_total = Number(nwsubtotal) - Number(commission);
				$("#main_total").val(main_total.toFixed(2));
				$("#grandtotalspan").html(main_total.toFixed(2));
				}else{
				return false;
			} 
		}
		
	}
	
	function check_payment(){
		if($("#sub_total").val() > 0){
			var r=confirm("Are you sure want to payment to traveler?");
			if (r==true)
			{
				document.frmpaymenr.submit();
				}else{
				return false;
			} 
			}else{
			alert('Please select atleast one pay checkbox for payment to traveler');
			return false;    
		}    
	} 
	function hidemessage(){
		jQuery("#errormsgdiv").slideUp();
	}
</script>
{/literal}