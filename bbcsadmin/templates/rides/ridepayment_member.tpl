<div class="contentcontainer" id="tabs">
	<div class="headings">         	
		<h2 class="left">Rides booking / Payment information of {$db_member[0].vFirstName} {$db_member[0].vLastName}</h2>          
	</div>
	<div class="contentbox" id="tabs-1">  
		{if $var_msg neq ''}
		
		<div class="status success" id="errormsgdiv"> 
			<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
			<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
			{$var_msg}</p> 
		</div> 
		{/if} 
		<form name="frmpaymenr" id="frmpaymenr" method="post" action="index.php?file=ri-ridepaymentmember_a">
			<input type="hidden" name="mode" value="edit">
			<input type="hidden" name="action" value="payment">
			<input type="hidden" name="main_total" id="main_total" value="0.00">
			<input type="hidden" name="commission" id="commission" value="0.00">
			<input type="hidden" name="sub_total" id="sub_total" value="0.00">  	
			<input type="hidden" name="drivercurrencycode" id="drivercurrencycode" value="{$driver_currency}">
			<input type="hidden" name="eTripReturn" id="eTripReturn" value="{$eTripReturn}">
			<input type="hidden" name="fCommissionRate" id="fCommissionRate" value="{$fCommissionRate}">
			<input type="hidden" name="iMemberId" id="iMemberId" value="{$iMemberId}">
			<input type="hidden" name="vPaymentEmail" id="vPaymentEmail" value="{$vPaymentEmail}"> 
			
			<table class="headingbg" style="border: 1px solid #ccc;" border="0" cellpadding="1" cellspacing="1" width="100%"> 
				<thead>
					<tr>
						<th style="font-size:12px;text-align:left;" width="35%">Ride Details</th> 
						<th style="font-size:12px;text-align:left;" width="15%">Booking Amount</th>
						<th style="font-size:12px;text-align:center;" width="10%">Confirm By Booker</th> 
						<th style="font-size:12px;text-align:center;" width="10%">Pay</th>            
						<th style="font-size:12px;text-align:right;" width="10%">Site Commission</th>
						<th style="font-size:12px;text-align:right;" width="10%">Traveler Payment</th>
						<th style="font-size:12px;text-align:right;" width="10%">Ride Cancel By</th>
					</tr> 
				</thead>   
				<tbody>   
					{section name="booking" loop=$db_bookings}      
					<tr>
						<td valign="top" style="border-bottom:1px solid #cccccc;font-size:13px;">
							<strong>Booking No.</strong> #{$db_bookings[booking].vBookingNo}
							<br><strong>Booker Name:</strong> {$db_bookings[booking].vBookerFirstName} {$db_bookings[booking].vBookerLastName}
							<br><strong>Trip of:</strong> {$db_bookings[booking].vFromPlace} &rarr; {$db_bookings[booking].vToPlace}
							<br><strong>Date & Time:</strong>{$generalobj->DateTime($db_bookings[booking].dBookingDate,10)} - {$generalobj->DateTime($db_bookings[booking].dBookingTime,12)}
						</td> 
						<td valign="top" style="border-bottom:1px solid #cccccc;font-size:13px;">
							<strong>Booker Pay:</strong> {$db_bookings[booking].vBookerCurrencyCode} {$db_bookings[booking].fAmount}
						</td>            
						<td valign="top" style="border-bottom:1px solid #cccccc;font-size:13px;text-align:center;">
							{$db_bookings[booking].eBookerConfirmation}
						</td>
						{if $db_bookings[booking].eStatus eq 'Cencelled'}
						<td valign="top" style="border-bottom:1px solid #cccccc;font-size:13px;text-align:center;">     
							<input type="button" class="btn" onclick="go_for_refund({$db_bookings[booking].iBookingId});" value="Cancellation Refund">
						</td>
						{else}
						<td valign="top" style="border-bottom:1px solid #cccccc;font-size:13px;text-align:center;">
							<!--<input type="checkbox" onClick="calculate_again({$smarty.section.booking.index}, {$db_bookings[booking].iBookingId});" name="confirm[]" id="confirm_{$db_bookings[booking].iBookingId}" {if $db_bookings[booking].eBookerConfirmation eq 'Yes'} checked {/if} value="{$db_bookings[booking].iBookingId}">-->
							<input type="hidden" name="amount_{$smarty.section.booking.index}_{$db_bookings[booking].iBookingId}" id="amount_{$smarty.section.booking.index}_{$db_bookings[booking].iBookingId}" value="{$db_bookings[booking].driver_amount} ">
							
							{if $db_bookings[booking].driver_amount neq '0.00' && $db_bookings[booking].dBookingDate lte $currdate}
								{if $db_bookings[booking].dBookingTime lte $currtime}
									<select name="eDriverPaymentType" id="eDriverPaymentType_{$db_bookings[booking].iBookingId}">
										<option value="Paypal">Paypal</option>
										<option value="BankTransfer">Bank Transfer</option>
									</select>
									<br />
									<input type="button" class="btn" onclick="go_for_payment({$db_bookings[booking].iBookingId},{$db_bookings[booking].driver_amount});" value="Pay" style="margin-top:10px;">
								{/if} 
							{/if} 
						</td>
						{/if}
						<td valign="top" style="border-bottom:1px solid #cccccc;font-size:13px;text-align:right;"> 
							{$driver_currency} {$db_bookings[booking].commission_amount}        
						</td>
						<td valign="top" style="border-bottom:1px solid #cccccc;font-size:13px;text-align:right;">
							{$driver_currency} {$db_bookings[booking].driver_amount}        
						</td>
						<td valign="top" style="border-bottom:1px solid #cccccc;font-size:13px;text-align:right;">
							{if $db_bookings[booking].eStatus eq Cencelled} 
								{$db_bookings[booking].eCancelBy}
							{else}
								--
							{/if}
						</td>
					</tr>  
					{/section} 
					<tr>
						<td colspan="5" width="80%" height="20" style="border-bottom:0px solid #cccccc;font-size:13px;text-align:right;">
						</td>
						<td colspan="2" width="20%" height="20" style="border-bottom:1px solid #cccccc;border-left:1px solid #cccccc;font-size:13px;text-align:right;">
							<strong>Total payment: <span id="totpaymentspan">{$driver_currency} {$tot_driver_refund}</span></strong>
						</td>
					</tr>
					<tr>
						<td colspan="5" width="80%" height="20" style="border-bottom:0px solid #cccccc;font-size:13px;text-align:right;">
						</td>
						<td colspan="2" width="20%" height="20" style="border-bottom:1px solid #cccccc;border-left:1px solid #cccccc;font-size:13px;text-align:right;">
							<strong>Payout limit: <span id="totpaymentspan">{$driver_currency} {$MINIMUM_PAYOUT_LIMIT}</span></strong>
						</td>
					</tr>
					<!--<tr>
						<td colspan="4" width="80%" height="20" style="border-bottom:0px solid #cccccc;font-size:13px;text-align:right;"></td>
						<td colspan="1" width="20%" height="20" style="border-bottom:1px solid #cccccc;border-left:1px solid #cccccc;font-size:13px;text-align:right;">
						<strong>Sub Total: {$driver_currency} <span id="totpaymentspan">0.00</span></strong>
						</td>
						</tr>
						<tr>
						<td colspan="4" width="80%" height="20" style="border-bottom:0px solid #cccccc;font-size:13px;text-align:right;"></td>
						<td colspan="1" width="20%" height="20" style="border-bottom:1px solid #cccccc;border-left:1px solid #cccccc;font-size:13px;text-align:right;">
						<strong>Site Commission ({$fCommissionRate} %): {$driver_currency} <span id="itecommissionspan">0.00</span></strong>
						</td>
						</tr>
						<tr>
						<td colspan="4" width="80%" height="20" style="border-bottom:0px solid #cccccc;font-size:13px;text-align:right;"></td>
						<td colspan="1" width="20%" height="20" style="border-bottom:1px solid #cccccc;border-left:1px solid #cccccc;font-size:13px;text-align:right;">
						<strong>Grand Total: {$driver_currency} <span id="grandtotalspan">0.00</span></strong>
						</td>
						</tr>
						<tr>
						<td colspan="4" width="80%" height="20" style="border-bottom:0px solid #cccccc;font-size:13px;text-align:right;"></td>
						<td colspan="1" width="20%" height="20" style="border-bottom:0px solid #cccccc;border-left:1px solid #cccccc;font-size:13px;text-align:right;">
						{if $vPaymentEmail neq ''}
						<input type="button" value="Pay To Driver" class="btn" onClick="check_payment();" title="Pay To Driver"/>
						{else}
						Receiver payment email not found
						{/if}
						</td>
					</tr>-->
				</tbody>
			</table>
		</form>
		<table class="headingbg" style="border: 1px solid #ccc;margin-top:20px;" border="0" cellpadding="1" cellspacing="1" width="100%">
			<tr>
				<th><h2>Payment Details Of {$db_member[0].vFirstName} {$db_member[0].vLastName}</h2></th>
			</tr>
			<tr>
				<td>
					<fieldset style="width:400px;">
						<legend>Paypal Details</legend>
						<strong>Paypal E-mail Id : </strong>  {if $db_member[0].vPaymentEmail neq ''}{$db_member[0].vPaymentEmail}{else}---{/if}
					</fieldset>  
				</td>
			</tr>
			<tr>
				<td>
					<fieldset style="width:400px;">
						<legend>Bank Details</legend>
						<strong>Account Holder Name  : </strong> {if $db_member[0].vBankAccountHolderName neq ''}{$db_member[0].vBankAccountHolderName}{else}---{/if}<br />
						<strong>Account Number (IBAN) : </strong> {if $db_member[0].vAccountNumber neq ''}{$db_member[0].vAccountNumber}{else}---{/if}<br />
						<strong>Name of Bank : </strong>  {if $db_member[0].vBankName neq ''}{$db_member[0].vBankName}{else}---{/if}<br />
						<strong>Bank Location  : </strong>  {if $db_member[0].vBankLocation neq ''}{$db_member[0].vBankLocation}{else}---{/if}<br />
						<strong>BIC/SWIFT Code  : </strong>  {if $db_member[0].vBIC_SWIFT_Code neq ''}{$db_member[0].vBIC_SWIFT_Code}{else}---{/if}<br />
					</fieldset>  
				</td>
			</tr>
		</table>
	</div>
</div> 
<form name="frmdriverpayment" id="frmdriverpayment" method="post" action="{$tconfig.tpanel_url}/index.php?file=ri-ridepaymentmember_a">
	<input type="hidden" name="payment_action" id="payment_action" value="paynow">
	<input type="hidden" name="payment_booking" id="payment_booking" value="">
	<input type="hidden" name="payment_amount" id="payment_amount" value="">
	<input type="hidden" name="payment_type" id="payment_type" value="">
</form>
{literal}
<script>
	function backtolist(){
		window.location="index.php?file=ri-rideslist&mode=view";
	}
	function edit_dealstatus(){
		document.frmadd.submit();
	} 
	
	var fCommissionRate = {/literal}{$fCommissionRate}{literal}
	
	function calculate_again(ind,id){     
		if($("#confirm_"+id).is(':checked')){
			var r=confirm("Are you sure want to add this booking in driver payment?");
			if (r==true)
			{
				var sub_total = $("#sub_total").val();
				var add_amt = $("#amount_"+ind+"_"+id).val();
				var nwsubtotal = Number(sub_total) + Number(add_amt); 
				$("#sub_total").val(nwsubtotal.toFixed(2));
				$("#totpaymentspan").html(nwsubtotal.toFixed(2));
				
				var commission = (nwsubtotal.toFixed(2) * Number(fCommissionRate)) / 100;
				$("#commission").val(commission.toFixed(2));
				$("#itecommissionspan").html(commission.toFixed(2));
				
				var main_total = Number(nwsubtotal) - Number(commission);
				$("#main_total").val(main_total.toFixed(2));
				$("#grandtotalspan").html(main_total.toFixed(2));
				
				
				}else{
				//$("#confirm_"+id).prop('checked', false);
				$("#confirm_"+id).removeAttr('checked');
				return false;
			} 
			}else{
			var r=confirm("Are you sure want to remove this booking in driver payment?");
			if (r==true)
			{
				var sub_total = $("#sub_total").val();
				var add_amt = $("#amount_"+ind+"_"+id).val();
				var nwsubtotal = Number(sub_total) - Number(add_amt); 
				$("#sub_total").val(nwsubtotal.toFixed(2));
				$("#totpaymentspan").html(nwsubtotal.toFixed(2));
				
				var commission = (nwsubtotal.toFixed(2) * Number(fCommissionRate)) / 100;
				$("#commission").val(commission.toFixed(2));
				$("#itecommissionspan").html(commission.toFixed(2));
				
				var main_total = Number(nwsubtotal) - Number(commission);
				$("#main_total").val(main_total.toFixed(2));
				$("#grandtotalspan").html(main_total.toFixed(2));
				}else{
				return false;
			} 
		}
		
	}
	
	function check_payment(){
		if($("#sub_total").val() > 0){
			var r=confirm("Are you sure want to payment to driver?");
			if (r==true)
			{
				document.frmpaymenr.submit();
				}else{
				return false;
			} 
			}else{
			alert('Please select atleast one pay checkbox for payment to driver');
			return false;    
		}    
	} 
	function hidemessage(){
		jQuery("#errormsgdiv").slideUp();
	}
	
	function go_for_payment(id,driveramount){ //alert(id);return false;
		if(id != ''){
			var r=confirm("Are you sure want to payment to driver?");
			if (r==true)
			{
				$("#payment_booking").val(id);
				$("#payment_amount").val(driveramount);
				var paymenttype = document.getElementById("eDriverPaymentType_"+id).value;
				$("#payment_type").val(paymenttype);
				if($("#payment_booking").val() != ''){
					document.frmdriverpayment.submit();
					return false;
				}
				}else{
				return false;
			} 
		}
	}
	
	function go_for_refund(id){
		window.location="index.php?file=bo-bookings&mode=edit&iBookingId="+id;
	}
</script>
{/literal}