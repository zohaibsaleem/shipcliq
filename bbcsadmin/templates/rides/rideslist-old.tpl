<div class="contentcontainer" id="tabs">
	<div class="headings">
    	{if $mode eq 'edit'}
			<h2 class="left">Rides Information</h2>
        {else}
			<h2 class="left">Rides Information</h2>
        {/if}
	</div>
	<div class="contentbox" id="tabs-1">  
   {if $var_msg neq ''}
                    			
                          	<div class="status success" id="errormsgdiv"> 
                    					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
                    					<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
                    					{$var_msg}</p> 
                    				</div>
                    			
	                          {/if}  
									<table border="0" cellpadding="1" cellspacing="1" width="100%" align="center" class="">
									<tr>
										<td valign="top" width="35%">
											<form id="frmadd" name="frmadd" action="index.php?file=ri-rideslist_a" method="post">
                      <input type="hidden" name="iRideId" id="iRideId" value="{$iRideId}" />
                      <input type="hidden" name="action" id="action" value="{$mode}" /> 
                      <table class="headingbg" style="border: 1px solid #ccc;" border="0" cellpadding="1" cellspacing="1" width="100%" align="center">
										  <tr>
											<td class="headingbg wmatterbold" bgcolor="#309ec3" height="22">&nbsp;<b><span style="color:#000000;">Ride Details</span></b></td>
											</tr>
											<tr bgcolor="#ffffff">
												<td valign="top" width="100%" align="center">
													<table border="0" cellpadding="1" cellspacing="1" width="100%">
													<tbody><tr>
														<td align="left">Ride &nbsp;</td>
														<td> : </td>
														<td align="left">{$db_records[0].vDeparturePoint} - {$db_records[0].vArrivalPoint}</b><!--<a target="_BLANK" href="index.php?file=m-member&amp;iCustomerId={$db_records[0].iCustomerId}&amp;mode=edit&amp;order=true">{$db_records[0].vFirstName} {$db_records[0].vLastName}</a>--></td>
													</tr>
													<tr>
														<td align="left">Member Name&nbsp;</td>
														<td> : </td>
														<td align="left"><a target="_BLANK" href="{$tconfig.tpanel_url}/index.php?file=m-member&mode=edit&iMemberId={$db_records[0].iMemberId}">{$db_records[0].vFirstName} {$db_records[0].vLastName}</a></td>
													</tr>
													<tr>
														<td align="left">Departure Point&nbsp;</td>
														<td> : </td>
														<td align="left">{$db_records[0].vDeparturePoint}</td>
													</tr>
													<tr>
														<td align="left">Arrival Point&nbsp;</td>
														<td> : </td>
														<td align="left">{$db_records[0].vArrivalPoint}</td>
													</tr>
													<tr>
														<td align="left">Departure Date&nbsp;</td>
														<td> : </td>
														<td align="left">{$db_records[0].dDepartureDate} {$db_records[0].vDepartureTime}</td>
													</tr>
                          <tr>
														<td align="left">Arrival Date&nbsp;</td>
														<td> : </td>
														<td align="left">{$db_records[0].dReturnDate} {$db_records[0].vReturnTime}</td>
													</tr>
													<tr>
														<td align="left">Price&nbsp;</td>
														<td> : </td>
														<td align="left">{$db_records[0].fPrice}</td>
													</tr>
													<tr>
														<td align="left">Seat Left&nbsp;</td>
														<td> : </td>
														<td align="left">{$db_records[0].iNumberOfSeats}</td>
													</tr>
													<tr>
														<td align="left">Total Booking&nbsp;</td>
														<td> : </td>
														<td align="left">{$db_records[0].totbooking} &nbsp;<a target="_BLANK" href="{$tconfig.tpanel_url}/index.php?file=bo-bookings&mode=view&iRideId={$db_records[0].iRideId}">view</a></td>
													</tr>
													<tr>
														<td align="left" valign="top">Departure Details&nbsp;</td>
														<td valign="top"> : </td>
														<td align="left">{$db_records[0].tDepartureDetails}</td>
													</tr>
													<tr>
														<td align="left" valign="top">Arrival Details&nbsp;</td>
														<td valign="top"> : </td>
														<td align="left">{$db_records[0].tArrivalDetails}</td>
													</tr>
													<tr>
														<td align="left">Type&nbsp;</td>
														<td> : </td>
														<td align="left">{$db_records[0].eType}</td>
													</tr>
													<tr>
														<td align="left">Round Trip&nbsp;</td>
														<td> : </td>
														<td align="left">{$db_records[0].eRoundTrip}</td>
													</tr>
													<tr>
														<td align="left">Detour&nbsp;</td>
														<td> : </td>
														<td align="left">{$db_records[0].eDetour}</td>
													</tr>
													<tr>
														<td align="left">Schedule Flexibility&nbsp;</td>
														<td> : </td>
														<td align="left">{$db_records[0].eScheduleFlexibility}</td>
													</tr>
												  <tr>
														<td align="left">Luggage Size&nbsp;</td>
														<td> : </td>
												    <td align="left">{$db_records[0].eLuggageSize}</td>
													</tr>
													<tr>
														<td align="left">Car&nbsp;</td>
														<td> : </td>
												    <td align="left">{$db_records[0].vMake} {$db_records[0].model}</td>
													</tr>
													<tr>
														<td align="left">Mark as Good Deal&nbsp;</td>
														<td> : </td>
												    <td align="left">
                              <select id="eGoodDeal" name="eGoodDeal">
                    				    <option value="No" {if $db_records[0].eGoodDeal eq No}selected {/if}>No</option>
                            		<option value="Yes" {if $db_records[0].eGoodDeal eq Yes}selected {/if}>Yes</option>
                    					</select>
                    					<input type="submit" class="btnalt" name="editDeal" value="Submit" onClick="edit_dealstatus();">
                            </td>
													</tr>
													</tbody></table>
												</td>
											</tr>
											</table>
											</form>
										</td>
										<td valign="top" width="65%">
										  <form name="frmpaymenr" id="frmpaymenr" method="post" action="index.php?file=ri-rideslist">
										  <input type="hidden" name="mode" value="edit">
										  <input type="hidden" name="iRideId_payment" value="{$iRideId}">
										  <input type="hidden" name="action" value="payment">
										  <input type="hidden" name="main_total" value="{$main_total}">
                      <input type="hidden" name="commission" value="{$commission}">
                      <input type="hidden" name="sub_total" value="{$sub_total}">  										  
                      <table class="headingbg" border="0" cellpadding="1" cellspacing="1" width="100%" align="center">
										    <tr>
										      <td class="headingbg wmatterbold" bgcolor="#309ec3" height="22">&nbsp;<b><span style="color:#000000;">Ride Booking Details / Driver Payment Details</span></b></td>
										      <table border="0" style="border: 1px solid #ccc;" cellpadding="1" cellspacing="1" width="100%">
                            <tr>
										          <th width="5%" style="text-align:left;font-size: 12px;">&nbsp;</th>
                              <th width="10%" style="text-align:left;font-size: 12px;">Booking No.#</th>
										          <th width="20%" style="text-align:left;font-size: 12px;">Booker Name</th>
										          <th width="20%" style="text-align:left;font-size: 12px;">Payment Date</th>
										          <th width="20%" style="text-align:center;font-size: 12px;">Confirmed By Booker</th>  										          
										          <th width="10%" style="text-align:right;font-size: 12px;">Amount</th>
										        </tr>
										        {section name="booking" loop=$db_bookings}
                            <tr>
										          <td style="border-bottom:1px solid #c3c3c3;border-right:1px solid #c3c3c3;"><input type="checkbox" onClick="calculate_again({$db_bookings[booking].iBookingId});" name="confirm[]" id="confirm_{$db_bookings[booking].iBookingId}" {if $db_bookings[booking].eBookerConfirmation eq 'Yes' and $db_bookings[booking].eBookerPaymentPaid eq 'Yes'} checked {/if} value="{$db_bookings[booking].iBookingId}"></td>
                              <td style="border-bottom:1px solid #c3c3c3;border-right:1px solid #c3c3c3;">{$db_bookings[booking].vBookingNo}</td>
										          <td style="border-bottom:1px solid #c3c3c3;border-right:1px solid #c3c3c3;">{$db_bookings[booking].vBookerName}</td>
										          <td style="border-bottom:1px solid #c3c3c3;border-right:1px solid #c3c3c3;">{$generalobj->DateTime($db_bookings[booking].ePaymentDate,9)}</td>
										          <td style="border-bottom:1px solid #c3c3c3;border-right:1px solid #c3c3c3;text-align:center;">{$db_bookings[booking].eBookerConfirmation}</td> 										          
										          <td style="border-bottom:1px solid #c3c3c3;text-align:right;{if $db_bookings[booking].eBookerConfirmation eq 'No'} color:#c3c3c3; {/if}">{$generalobj->booking_currency($db_bookings[booking].fAmount, $currency_code)}</td>
										        </tr>  
										        {/section}
										        <tr>  										         
										          <td colspan="5" style="border-bottom:1px solid #c3c3c3;text-align:right;border-right:1px solid #c3c3c3;"><strong>Sub Total</strong></td>
										          <td style="border-bottom:1px solid #c3c3c3;text-align:right;"><strong>{$generalobj->booking_currency($sub_total, $currency_code)}</strong></td>
										        </tr>   
                             <tr>  										         
										          <td colspan="5" style="border-bottom:1px solid #c3c3c3;text-align:right;border-right:1px solid #c3c3c3;"><strong>Site Commission ({$SITE_COMMISSION}%)</strong></td>
										          <td style="border-bottom:1px solid #c3c3c3;text-align:right;"><strong>{$generalobj->booking_currency($commission, $currency_code)}</strong></td>
										        </tr>  
                            <tr>  										         
										          <td colspan="5" style="border-bottom:1px solid #c3c3c3;text-align:right;border-right:1px solid #c3c3c3;"><strong>Total Amount</strong></td>
										          <td style="border-bottom:1px solid #c3c3c3;text-align:right;"><strong>{$generalobj->booking_currency($main_total, $currency_code)}</strong></td>
										        </tr> 
                            <tr>  
										          <td colspan="6" style="text-align:right;">{if $db_records[0].eStatus neq 'Archived'}<input type="button" value="Pay To Driver" class="btn" onClick="check_payment();" title="Pay To Driver"/>{else}<span style="color:#348017;font-size:15px;">Driver paid</span>{/if}</td>
										        </tr> 							       
										      </table>
										      </td>
										    </tr>
										  </table>
										  </form>
										</td>
										</tr>     										
		            </table>
								    
	                    <table width="100%">
                       <tr>                                 
                        <td style="float:right;" align="center">
                          <input type="button" value="Ride List" class="btn" onClick="backtolist();" title="Ride List"/>
                         </td>
                        </tr>
                      </table>
	</div>
</div>  
{literal}
<script>
function backtolist(){
  window.location="index.php?file=ri-rideslist&mode=view";
}
function edit_dealstatus(){
   document.frmadd.submit();
} 

function calculate_again(id){
  if($("#confirm_"+id).is(':checked')){
    var r=confirm("Are you sure want to add this booking from driver payment?");
    if (r==true)
    {
       window.location="index.php?file=ri-rideslist&mode=edit&iRideId={/literal}{$iRideId}{literal}&action=add&iBookingId="+id;
    }else{
       return false;
    } 
  }else{
    var r=confirm("Are you sure want to remove this booking from driver payment?");
    if (r==true)
    {
       window.location="index.php?file=ri-rideslist&mode=edit&iRideId={/literal}{$iRideId}{literal}&action=remove&iBookingId="+id;
    }else{
       return false;
    } 
  }

}

function check_payment(){
  var r=confirm("Are you sure want to payment to driver?");
  if (r==true)
  {
    document.frmpaymenr.submit();
  }else{
    return false;
  } 
} 
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
{/literal}