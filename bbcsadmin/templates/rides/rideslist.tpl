<div class="contentcontainer" id="tabs">
	<div class="headings">
    	{if $mode eq 'edit'}
			<h2 class="left">Rides Information</h2>
        {else}
			<h2 class="left">Rides Information</h2>
        {/if}
	</div>
	<div class="contentbox" id="tabs-1">  
{if $var_msg neq ''}
		
<div class="status success" id="errormsgdiv"> 
<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
{$var_msg}</p> 
</div> 
{/if} 

{if $var_err_msg neq ''}
<div class="status error" id="errormsgdiv"> 
<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Error" />
{$var_err_msg}</p> 
</div> 
{/if}
 
<table class="headingbg" style="border: 1px solid #ccc;" border="0" cellpadding="1" cellspacing="1" width="100%">
<input type="hidden" name="action" id="action" value="" />
<input  type="hidden" name="iRideId" value=""/>
<thead>
<tr>
		<th style="font-size:12px;text-align:left;" width="80%">Ride Details</th> 
    {if $smarty.const.PAYMENT_OPTION != 'Contact'}
    <th style="font-size:12px;text-align:center;" width="10%">Payment Due</th> 
    {/if}             
    <th style="font-size:12px;text-align:center;" width="10%">Status</th>
</tr> 
</thead>   
<tbody>
{if $db_records_all|@count gt 0}  
<tr style="background:{$db_records_all[0].color}">
	<td width="80%" >
    <table width="100%" style="border:1px solid #cccccc;">
    <tr>
      <td style="color: #05A2DB;font-size:15px;">
        <a href="{$tconfig.tpanel_url}/index.php?file=ri-rideslist&mode=edit&iRideId={$db_records_all[0].iRideId}" style="color: #05A2DB;font-size:15px;"><strong>Ride of:</strong> {$db_records_all[0].vMainDeparture} - {$db_records_all[0].vMainArrival}</a>
      </td>         
    </tr>
    <tr>
      <td style="color:#7C7C7C;font-size:14px;">
        <strong>Offered By:</strong> {$db_records_all[0].vFirstName}&nbsp;{$db_records_all[0].vLastName}
        &nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp;
        <!--<strong>Price:</strong> {$db_records_all[0].vBookerCurrencyCode}&nbsp;{$db_records_all[0].price}
        &nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp;   -->
        <strong>Type:</strong> {$db_records_all[0].eRideType}
        &nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp;
        <!-- <strong>Seats:</strong> {$db_records_all[0].iSeats}
        &nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp; -->
        <strong>Add Date:</strong> {$generalobj->DateTime($db_records_all[0].dAddedDate,9)}
      </td>
    </tr>
    </table>
  </td>
  {if $smarty.const.PAYMENT_OPTION != 'Contact'}
  <td width="10%">
    <table width="100%" style="border:1px solid #cccccc;">
    <tr>
      <td style="text-align:center;">
        <strong>{$db_records_all[0].paymentdue}</strong>
      </td>         
    </tr>
    <tr><td style="text-align:center;">{$db_records_all[0].todaybooking}&nbsp;{$db_records_all[0].pastbooking}</td></tr>
    </table>
  </td>
  {/if}
  <td width="10%">
    <table width="100%" style="border:1px solid #cccccc;">
    <tr>
      <td style="text-align:center;">
        <strong>{$db_records_all[0].eStatus}</strong>
      </td>         
    </tr>
    <tr><td>&nbsp;</td></tr>
    </table>
  </td>   
</tr>    
{else}
<tr>
	<td height="70px;" colspan="12" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No Record Found.</td>
</tr>
{/if}
</tbody>
</table>
<table style="border: 1px solid #ccc;" border="0" cellpadding="1" cellspacing="1" width="100%">
  <tr>
    <th>
      Further Details
    </th>
  </tr>
  <tr>
    <td>
      <table width="100%" style="border:1px solid #cccccc;">
        <tr>
          <td>
            <strong>Description :</strong> {$db_records_all[0].tDetails}
            <br><br>
            <strong>Leave on :</strong> {$db_records_all[0].eLeaveTime}
            {if $doc eq "Yes"}
              &nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp;
              <strong>Document:</strong> {$docprice} ({$docweight})
            {/if}
            {if $box eq "Yes"}
              &nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp;
              <strong>Box:</strong> {$boxprice} ({$boxweight})
            {/if}
            {if $lug eq "Yes"}
              &nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp;
              <strong>Luggage:</strong> {$lugprice} ({$lugweight})
            {/if}
          </td>
        </tr>       
    </td>
  </tr>
</table>
<br>
<table class="headingbg" style="border: 1px solid #ccc;" border="0" cellpadding="1" cellspacing="1" width="100%">
<tr>
  <th colspan="6">
    Ride Dates and Booking Details
  </th> 
</tr>
<tr>
  <th>
    Outbound Dates
  </th>
  {if $smarty.const.PAYMENT_OPTION != 'Contact'}
  <th>
    View Bookings
  </th>   
  <th>
    Traveler Payment
  </th>
  {/if} 
  <th>
    Return Dates
  </th>
  {if $smarty.const.PAYMENT_OPTION != 'Contact'}
  <th>
    View Bookings
  </th> 
   <th>
    Traveler Payment
  </th>
  {/if}
</tr> 
{section name="dates" loop=$db_dates} 
<tr>
  <td style="background:{$db_dates[dates].colorout}">
    {$db_dates[dates].dDateOut}
  </td>  
  {if $smarty.const.PAYMENT_OPTION != 'Contact'}
  <td style="background:{$db_dates[dates].colorout}">
    {if $db_dates[dates].totbookingsout neq 'No Bookings'}
    <a href="{$tconfig.tsite_panel}index.php?file=ri-ridepayment&mode=edit&iRideId={$iRideId}&dateout={$db_dates[dates].dDateOut}">{$db_dates[dates].totbookingsout} Booking(s) | View / Driver Payment</a>
    {else}
    {$db_dates[dates].totbookingsout}
    {/if}
  </td> 
  <td style="background:{$db_dates[dates].colorout}">
    {if $db_dates[dates].eDateOutPaid eq 'Yes'}
    Paid
    {else}
    Unpaid
    {/if}
  </td> 
  {/if}
  <td style="background:{$db_dates[dates].colorret}">
    {$db_dates[dates].dDateRet}
  </td>
  {if $smarty.const.PAYMENT_OPTION != 'Contact'}
  <td style="background:{$db_dates[dates].colorret}">
    {if $db_dates[dates].totbookingsret neq 'No Bookings'}
    <a href="{$tconfig.tsite_panel}index.php?file=ri-ridepayment&mode=edit&iRideId={$iRideId}&dDateRet={$db_dates[dates].dDateRet}">{$db_dates[dates].totbookingsret} Booking(s) | View / Driver Payment</a>
    {else}
    {$db_dates[dates].totbookingsret}
    {/if}
  </td>
  <td style="background:{$db_dates[dates].colorret}">
   {if $db_dates[dates].eDateRetPaid eq 'Yes'}
    Paid
    {else}
    Unpaid
    {/if}
  </td> 
  {/if} 
</tr>  
{/section}
</table>
</div>
</div>  
{literal}
<script>
function backtolist(){
  window.location="index.php?file=ri-rideslist&mode=view";
}
function edit_dealstatus(){
   document.frmadd.submit();
} 

function calculate_again(id){
  if($("#confirm_"+id).is(':checked')){
    var r=confirm("Are you sure want to add this booking from traveler payment?");
    if (r==true)
    {
       window.location="index.php?file=ri-rideslist&mode=edit&iRideId={/literal}{$iRideId}{literal}&action=add&iBookingId="+id;
    }else{
       return false;
    } 
  }else{
    var r=confirm("Are you sure want to remove this booking from traveler payment?");
    if (r==true)
    {
       window.location="index.php?file=ri-rideslist&mode=edit&iRideId={/literal}{$iRideId}{literal}&action=remove&iBookingId="+id;
    }else{
       return false;
    } 
  }

}

function check_payment(){
  var r=confirm("Are you sure want to payment to traveler?");
  if (r==true)
  {
    document.frmpaymenr.submit();
  }else{
    return false;
  } 
} 
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
{/literal}