<script language="JavaScript" src="{$tconfig.tcp_javascript}jlist.js"></script>
<div class="contentcontainer">
	<div class="headings altheading">
		<h2>Pay To Traveler</h2>
	</div>
	<div class="contentbox">
    {if $var_msg_new neq ''}
     <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg_new}</p> 
     </div>     
    <div></div>
    {elseif $var_msg neq ''}
    <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg}</p> 
     </div>     
    <div></div>
    {/if}
    {if $smarty.get.var_err_msg neq ''}
     <div class="status error" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Error" />
              {$smarty.get.var_err_msg}</p> 
     </div>     
    <div></div>
    {/if}
		<form name="frmsearch" id="frmsearch" action="" method="post">
        
        <table width="100%" border="0">
		<tbody>
			<tr>
				<td width="10%"><label for="textfield"><strong>Search:</strong></label><td>
				<td width="10%"><input type="Text" id="keyword" name="keyword" value="{$keyword}"  class="inputbox" /><td>
				<td width="10%" >
					<select name="option" id="option">
						<option  value='concat(vFirstName," ",vLastName)'>Name</option> 					
						<option value="vEmail"{if $option eq 'vEmail'}selected{/if}>E-mail</option>
						<option value="eStatus"{if $option eq 'eStatus'}selected{/if}>Status</option>
					</select>
				</td>
				<td width="60%"><input type="button" value="Search" class="btnalt" id="Search" name="Search" title="Search" onclick="Searchoption();"/></td>
	<!--			<td width="10%"><input type="button" value="Add New" onclick="Redirect('index.php?file=m-member&mode=add');" class="btnalt" />&nbsp;</td> -->
			</tr>	
			<tr>
				<td colspan="7" align="center">
					{$AlphaBox}
				</td>
			</tr>
		</tbody>			
		</table> 
    </form>
 
    <form name="frmlist" id="frmlist"  action="index.php?file=m-member_a" method="post">
 		<table width="100%" border="0">
		<input type="hidden" name="action" id="action" value="" />
        <input  type="hidden" name="iMemberId" value=""/>
        <thead>
			<tr>
				<th width="15%">Traveler</th>
				<th width="20%">Email</th>
				<th width="10%">Phone</th>
				<th style="text-align:center;" width="15%">Due Payments</th>
				<th style="text-align:center;" width="10%">Pay Now</th>
				<th style="text-align:center;" width="30%">View Details</th>
			</tr>
		</thead>
		<tbody>
    {if $db_booked_members|@count gt 0}
	  {section name=i loop=$db_booked_members}
      <tr>
  			 <td>{$db_booked_members[i].vFirstName} {$db_booked_members[i].vLastName}</td>
  			 <td>{$db_booked_members[i].vEmail}</td>
  			 <td>{$db_booked_members[i].vPhone}</td>
  			 <td style="text-align:center;">{$db_booked_members[i].tot_no_payment}</td>
  			 <td style="text-align:center;"><a href="{$tconfig.tpanel_url}/index.php?file=ri-ridepayment_member&mode=edit&iMemberId={$db_booked_members[i].iDriverId}">Pay Now</a></td>
         <td style="text-align:center;"><a href="javascript:void(0);" onClick="show_hide_details({$smarty.section.i.index});">View</a></td>			 
  		</tr>
  		<tr id="details_{$smarty.section.i.index}" style="display:none;">
  		  <td colspan="6">{$db_booked_members[i].cont}</td>
  		</tr>
        {/section}
        {else}
        <tr>
			<td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No Record Found.</td>
		</tr>
        {/if}
		</tbody>
		</table>
        </form>
		<div class="extrabottom">  		
			<div class="bulkactions">
				<select name="newaction" id="newaction">

					<option value="Show All">Show All</option>
				</select>
				<input type="submit" value="Apply" class="btn" onclick="return Doaction(document.getElementById('newaction').value,'m-member',document.frmlist);"/>
			</div>
		</div>
        <div>
            <div class="pagination">
            {if $db_booked_members|@count gt 0}
	        <span class="switch" style="float: left;">{$recmsg}</span>
	        {/if}
            </div>
            {$page_link}
        </div>
		
        <!--<ul class="pagination">
			<li class="text">Previous</li>
			<li class="page"><a href="#" title="">1</a></li>
			<li><a href="#" title="">2</a></li>
			<li><a href="#" title="">3</a></li>
			<li><a href="#" title="">4</a></li>
			<li class="text"><a href="#" title="">Next</a></li>
		</ul>-->
		<div style="clear: both;"></div></div>
</div>
{literal}
<script>
function Searchoption(){
    document.getElementById('frmsearch').submit();
}
function AlphaSearch(val){
    var alphavalue = val;
    var file = 'm-member';
    window.location="index.php?file="+file+"&alp="+alphavalue+"&mode=view";
    return false;
}
function MakeAction(loopid,type){
if(type == 'Deleted')
    {
	   ans = confirm("Are you sure you want to delete?");
     if (ans == false)
      {
        return false;
      }
    }
    document.frmlist.iMemberId.value = loopid;
    document.frmlist.action.value = type;
	document.frmlist.submit();	
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}

function show_hide_details(id){
    $("#details_"+id).toggle();
}
</script>
{/literal}