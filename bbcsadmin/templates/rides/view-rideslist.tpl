<script language="JavaScript" src="{$tconfig.tcp_javascript}jlist.js"></script>
<div class="contentcontainer">
	<div class="headings altheading">
		<h2>Trips</h2>
	</div>
	<div class="contentbox">
    {if $var_msg_new neq ''}
     <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg_new}</p> 
     </div>     
    <div></div>
    {elseif $var_msg neq ''}
    <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg}</p> 
     </div>     
    <div></div>
    {/if}
		<form name="frmsearch" id="frmsearch" action="" method="post">
    <input  type="hidden" name="paymenttype" value="{$paymenttype}"/>   
        <table width="100%" border="0">
		<tbody>
			<tr>
				<td width="10%"><label for="textfield"><strong>Search:</strong></label><td>
				<td width="10%"><input type="Text" id="keyword" name="keyword" value="{$keyword}"  class="inputbox" /><td>
				<td width="10%" >
					<select name="option" id="option">
						<option  value="Title" {if $option eq 'Title'}selected{/if}>Title</option> 					
					  <option  value="Member" {if $option eq 'Member'}selected{/if}>Member</option>
						<option value="Status"{if $option eq 'Status'}selected{/if}>Status</option>
					</select>
				</td>
				  <td width="60%">&nbsp;</td>
	<!--			<td width="60%"><input type="button" value="Search" class="btnalt" id="Search" name="Search" title="Search" onclick="Searchoption();"/></td> -->
	<!--			<td width="10%"><input type="button" value="Add New" onclick="Redirect('index.php?file=m-member&mode=add');" class="btnalt" />&nbsp;</td> -->
			</tr>
      <tr>
        <td><label for="textfield"><strong>Filter By Status:</strong></label></td>
        <td colspan="6">
          &nbsp; &nbsp;
          <select name="status" id="status">
            <option value="">All</option>
            <option value="Active" {if $status eq 'Active'}selected {/if}>Trip is Ongoing (Active)</option>
            <option value="Inactive" {if $status eq 'Inactive'}selected {/if}>Inactive (Inactive)</option>
            <option value="Pending" {if $status eq 'Pending'}selected {/if}>Trip is under Under Registration (Pending)</option>
            <option value="Completed" {if $status eq 'Completed'}selected {/if}>The trip is Over (Completed)</option>
            <option value="Deleted" {if $status eq 'Deleted'}selected {/if}>The trip has been Deleted (Deleted)</option>
            <option value="Archived" {if $status eq 'Archived'}selected {/if}>Archived (Archived)</option>
          </select>
        </td>
      </tr>	
      <tr>
        <td>
          &nbsp;
        </td>
        <td colspan="6">
           &nbsp; &nbsp;
          <input type="button" value="Search" class="btnalt" id="Search" name="Search" title="Search" onclick="Searchoption();"/>
        </td>
      </tr>
			<tr>
				<td colspan="7" align="center">
					{*$AlphaBox*}
          {if $smarty.const.PAYMENT_OPTION != 'Contact'}
          <input type="button" value="Past Bookings/ Payment" onclick="Redirect('index.php?file=ri-rideslist&mode=view&paymenttype=past');" class="{if $paymenttype eq 'past'}btn{else}btnalt{/if}" />
          <input type="button" value="Today's Bookings/ Payment" onclick="Redirect('index.php?file=ri-rideslist&mode=view&paymenttype=today');" class="{if $paymenttype eq 'today'}btn{else}btnalt{/if}" />
          <input type="button" value="All Bookings/ Payment" onclick="Redirect('index.php?file=ri-rideslist&mode=view&paymenttype=all');" class="{if $paymenttype eq 'all'}btn{else}btnalt{/if}" />
				  {else}
				  <input type="button" value="All Bookings" onclick="Redirect('index.php?file=ri-rideslist&mode=view&paymenttype=all');" class="{if $paymenttype eq 'all'}btn{else}btnalt{/if}" />
				  {/if}
        </td>
			</tr>
		</tbody>			
		</table> 
    </form>          
   <form name="frmlist" id="frmlist" action="index.php?file=ri-rideslist_a" method="post">
   	<table width="100%" border="0">
		<input type="hidden" name="action" id="action" value="" />
    <input  type="hidden" name="iRideId" value=""/>
    <input  type="hidden" name="paymenttype" value="{$paymenttype}"/>
    <thead>
    <tr>
				<th style="font-size:12px;text-align:left;" width="65%">Trips Details</th> 
        {if $smarty.const.PAYMENT_OPTION != 'Contact'}
        <th style="font-size:12px;text-align:center;" width="10%">Payment Due</th> 
        {/if}             
        <th style="font-size:12px;text-align:center;" width="10%">Status</th>               
        <th style="font-size:12px;text-align:center;" width="15%">Action&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" onclick="checkAll(document.frmlist);" name="check_all" id="check_all"></th>
    </tr> 
    </thead>
     <!--<thead>
			<tr>
				<th style="font-size:12px;" width=""><a href="javascript:void(0);" onClick="Redirect('index.php?file=ri-rideslist&mode=view&sortby=1&order={if $order eq 0}1{else}0{/if}');">Title</a>{if $sortby eq 1}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>               
        <th style="font-size:12px;" width="8%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=ri-rideslist&mode=view&sortby=2&order={if $order eq 0}1{else}0{/if}');">Member</a>{if $sortby eq 2}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>               
 
        <th style="font-size:12px;" width="8%">Price</th>               
    	  <th style="font-size:12px;" width="8%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=ri-rideslist&mode=view&sortby=7&order={if $order eq 0}1{else}0{/if}');">Total Seats{if $sortby eq 7}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>
    	  <th style="font-size:12px;" width="7%">Seats Left</th>
    	  <th style="font-size:12px;" width="11%">Departure Date/Time</th>
    	  <th style="font-size:12px;" width="11%">Arrival Date/Time</th>
    	  <th style="font-size:12px;" width="10%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=ri-rideslist&mode=view&sortby=3&order={if $order eq 0}1{else}0{/if}');">Added Date</a>{if $sortby eq 3}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/down_active.png{else}icons/up_active.png{/if}">{/if}</th>
        <th style="font-size:12px;" width="6%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=ri-rideslist&mode=view&sortby=4&order={if $order eq 0}1{else}0{/if}');">Status</a>{if $sortby eq 4}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>               
       	<th style="font-size:12px;" width="12%" style="text-align:center;">Action</th>
				<th style="font-size:12px;" width="5%"><input type="checkbox" id="check_all" name="check_all" onclick="checkAll(document.frmlist);"/></th>
			</tr>
		</thead>-->
		<tbody>
    {if $db_records_all|@count gt 0}
		{section name=i loop=$db_records_all}
    <tr style="background:{$db_records_all[i].color}">
			<td width="65%" >
        <table width="100%" style="border:1px solid #cccccc;">
        <tr>
          <td style="color: #05A2DB;font-size:15px;">
            <a href="{$tconfig.tpanel_url}/index.php?file=ri-rideslist&mode=edit&iRideId={$db_records_all[i].iRideId}" style="color: #05A2DB;font-size:15px;"><strong>Trip of:</strong> {$db_records_all[i].vMainDeparture} - {$db_records_all[i].vMainArrival}</a>
          </td>         
        </tr>
        <tr>
          <td style="color:#7C7C7C;font-size:14px;">
            <strong>Offered By:</strong> {$db_records_all[i].vFirstName}&nbsp;{$db_records_all[i].vLastName}
            <!--&nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp;
            <strong>Price:</strong> {$db_records_all[i].vBookerCurrencyCode}&nbsp;{$db_records_all[i].price}  -->
            &nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp;
            <strong>Type:</strong> {$db_records_all[i].eRideType}
            &nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp;
            <!-- <strong>Seats:</strong> {$db_records_all[i].iSeats}
            &nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp; -->
            <strong>Add Date:</strong> {$generalobj->DateTime($db_records_all[i].dAddedDate,9)}
          </td>
        </tr>
        </table>
      </td>
      {if $smarty.const.PAYMENT_OPTION != 'Contact'}
      <td width="10%">
        <table width="100%" style="border:1px solid #cccccc;">
        <tr>
          <td style="text-align:center;">
            <strong>{$db_records_all[i].paymentdue}</strong>
          </td>         
        </tr>
        <tr><td style="text-align:center;">{$db_records_all[i].todaybooking}&nbsp;{$db_records_all[i].pastbooking}</td></tr>
        </table>
      </td>
      {/if}
      <td width="10%">
        <table width="100%" style="border:1px solid #cccccc;">
        <tr>
          <td style="text-align:center;">
            <strong>{$db_records_all[i].eStatus}</strong>
          </td>         
        </tr>
        <tr><td>&nbsp;</td></tr>
        </table>
      </td>
      <td width="15%">
        <table width="100%" style="border:1px solid #cccccc;">
        <tr>
          <td>
              <a title="" href="{$tconfig.tpanel_url}/index.php?file=ri-rideslist&mode=edit&iRideId={$db_records_all[i].iRideId}"><img title="Edit" src="{$tconfig.tpanel_img}icons/icon_edit.png"></a>
              <a onclick="MakeAction('{$db_records_all[i].iRideId}','Active');" title="Active" href="javascript:void(0);"><img title="Active" src="{$tconfig.tpanel_img}icons/icon_approve.png"></a>
              <a onclick="MakeAction('{$db_records_all[i].iRideId}','Inactive');" title="Inactive" href="javascript:void(0);"><img title="Inactive" src="{$tconfig.tpanel_img}icons/icon_unapprove.png"></a>
              <a onclick="MakeAction('{$db_records_all[i].iRideId}','Pending');" title="Pending" href="javascript:void(0);"><img title="Pending" src="{$tconfig.tpanel_img}icons/icon-pending.gif"></a>
              <a onclick="MakeAction('{$db_records_all[i].iRideId}','Deleted');" title="Delete" href="javascript:void(0);"><img title="Delete" src="{$tconfig.tpanel_img}icons/icon_delete.png"></a>
              <a onclick="MakeAction('{$db_records_all[i].iRideId}','Completed');" title="Delete" href="javascript:void(0);"><img title="Completed" src="{$tconfig.tpanel_img}icons/comp.png"></a>
              <a onclick="MakeAction('{$db_records_all[i].iRideId}','Archived');" title="Delete" href="javascript:void(0);"><img title="Archived" src="{$tconfig.tpanel_img}icons/arch.png"></a>
              &nbsp;&nbsp;&nbsp;<input name="iRideId[]" type="checkbox" id="iId" value="{$db_records_all[i].iRideId}"/>
          </td>         
        </tr>
        <tr><td>&nbsp;</td></tr>
        </table>
      </td>
	  </tr>
    {/section}
    {else}
    <tr>
			<td height="70px;" colspan="12" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No Record Found.</td>
		</tr>
    {/if}
		</tbody>
		</table>
   </form>
		<div class="extrabottom">
			<ul>
				<li><img src="{$tconfig.tpanel_img}icons/icon_edit.png" alt="Edit" /> Edit</li>
				<li><img src="{$tconfig.tpanel_img}icons/icon_approve.png" alt="Approve" /> Active</li>
				<li><img src="{$tconfig.tpanel_img}icons/icon_unapprove.png" alt="Unapprove" /> Inactive</li>
				<li><img src="{$tconfig.tpanel_img}icons/icon-pending.gif" alt="Pending" /> Pending</li>
				<li><img src="{$tconfig.tpanel_img}icons/icon_delete.png" alt="Deleted" /> Deleted</li>
				<li><img src="{$tconfig.tpanel_img}icons/comp.png" alt="Deleted" /> Completed</li>
				<li><img src="{$tconfig.tpanel_img}icons/arch.png" alt="Archived" /> Archived</li>
			</ul>
			<div class="bulkactions">
				<select name="newaction" id="newaction">
					<option value="">Select Action</option>
					<option value="Active">Make Active</option>
					<option value="Inactive">Make Inactive</option>
					<option value="Deleted">Make Deleted</option>
					<option value="Pending">Make Pending</option>
					<option value="Completed">Make Completed</option>
					<option value="Archived">Make Archived</option>
					<option value="Show All">Show All</option>
				</select>
				<input type="submit" value="Apply" class="btn" onclick="return Doaction(document.getElementById('newaction').value,'ri-rideslist',document.frmlist);"/>
			</div>
		</div>
        <div>
          <div class="pagination">
          {if $db_records_all|@count gt 0}
	          <span class="switch" style="float: left;">{$recmsg}</span>
	        {/if}
          </div>
          {$page_link}
        </div>
		
		<div style="clear: both;"></div></div>
</div>
{literal}
<script>
function Searchoption(){
    document.getElementById('frmsearch').submit();
}
function AlphaSearch(val){
    var alphavalue = val;
    var file = 'ri-rideslist';
    window.location="index.php?file="+file+"&alp="+alphavalue+"&mode=view";
    return false;
}
function MakeAction(loopid,type){
if(type == 'Deletes')
    {
	   ans = confirm("Are you sure you want to delete?");
     if (ans == false)
      {
        return false;
      }
    }
    document.frmlist.iRideId.value = loopid;
    document.frmlist.action.value = type;
	document.frmlist.submit();	
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
{/literal}