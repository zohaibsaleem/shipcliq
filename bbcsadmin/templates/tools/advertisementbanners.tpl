<script language="JavaScript" src="{$tconfig.tcp_javascript}validate.js"></script>
<div class="contentcontainer" id="tabs">
	<div class="headings">
		{if $mode eq 'edit'}
			<h2 class="left">Edit Advertisement Banners</h2>
        {else}
			<h2 class="left">Add Advertisement Banners</h2>
        {/if}
	</div>
	<div class="contentbox" id="tabs-1">
        {if $var_msg neq ''}
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Success" />
					{$var_msg}</p> 
				</div>     
				<div></div>
			{/if}
			{if $var_msg_error neq ''}
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Success" />
					{$var_msg_error}</p> 
				</div>     
				<div></div>
			{/if}
			<form id="frmadd" name="frmadd" action="index.php?file=to-advertisementbanners_a" method="post" enctype="multipart/form-data">
            <input type="hidden" name="iBannerId" id="iBannerId" value="{$iBannerId}" />
            <input type="hidden" name="action" id="action" value="{$mode}" />
            <input type="hidden" name="eSection" id="eSection" value="Inner" />
				 
				<p>
					<label for="textfield"><strong>Title :<em>*</em></strong></label>
					<input type="text" id="vTitle" name="Data[vTitle]" class="inputbox" value="{$db_design[0].vTitle}" lang="*" title="Title"/>
				</p>
				<p>
					<label for="textfield"><strong>URL :</strong></label>
					<textarea id="tURL" name="Data[tURL]" class="inputbox" title="URL">{$db_design[0].tURL}</textarea>
					<br />
					[Note: Please include <b>http://</b> in URL. Else it will not work properly on site.]
				</p>
				<p>
					<label for="textfield"><strong>Upload Banner :</strong></label>
					<input type="file" id="vImage" name="Data[vImage]" class="inputbox" title="Design" title="Upload Design" />{if $db_design[0].thumb_url neq ""}&nbsp;&nbsp;[<a href="{$db_design[0].thumb_url}" target="_blank"  title="{$db_design[0].vTitle}" rel="[images]">View</a>]{/if}
				  <br>[Note : Please upload Home  Page Top banner of width : 1021 and height : 483]
				  <br>[Note : Please upload Inner Page banner of width : 1021 and height : 324]
				  <br>[Note : Please upload Home  Page Middle banner of width : 477 and height : 141]
				  <br>[Note : Please upload Home  Page Bottom banner of width : 321 and height : 171]
				  <br>[Note: Supported File Types are *.jpg,*.jpeg,*.png,*.gif]  
		</p>
			<p>
					<label for="textfield"><strong>Position :<em>*</em></strong></label>					
						<select id="ePortion" name="Data[ePortion]" lang="*" title="Position">
						<option value="">--Select Position--</option>
						<option value="Top" {if $db_design[0].ePortion eq 'Top'} selected {/if}>Top</option>
						<option value="Bottom" {if $db_design[0].ePortion eq 'Bottom'} selected {/if}>Bottom</option>
						<option value="Middle" {if $db_design[0].ePortion eq 'Middle'} selected {/if}>Middle</option>
            </select>
				</p>  				
				<p>        			
					<label for="textfield"><strong>Display Order :</strong></label>
					<select id="iDisplayOrder" name="Data[iDisplayOrder]" lang="" title="Category Display Order" />
					<option value="">Please Select Display Order</option>
					{section name=i loop=$num_rows}
					 <option {if $db_design[0].iDisplayOrder eq $smarty.section.i.index_next}selected{/if} value="{$smarty.section.i.index_next}">{$smarty.section.i.index_next}</option>
					{/section}
					</select>
				</p>
				<p>
					<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $db_design[0].eStatus eq Active}selected{/if}>Active</option>
						<option value="Inactive" {if $db_design[0].eStatus eq Inactive}selected{/if}>Inactive</option>
					</select>
				</p>

				{if $mode eq 'edit'}
				<input type="submit" value="Save Banner" class="btn" onclick="return validate(document.frmadd);" title="Save Banner"/>
				{else}
   			<input type="submit" value="Add Banner" class="btn" onclick="return validate(document.frmadd);" title="Add Banner"/>
  			{/if}
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
			</form>
	</div>
</div>
{literal}
<script>
function redirectcancel()
{
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=to-advertisementbanners&mode=view";
    return false;
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
{/literal}
 