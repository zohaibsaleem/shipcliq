<div class="contentcontainer">
<div class="headings">
   {if $mode eq 'edit'}
     <h2 class="left">Edit System E-mail</h2>
   {else}
	   <h2 class="left">Add System E-mail</h2>
	 {/if}  
</div>
<div class="contentbox" id="tabs-1">
		{if $var_msg neq ""}
		<div class="status error" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Success" />
              {$var_msg}</p> 
     </div>
     {/if}
<div class="contentbox" id="tabs-1">
      <div id="keyfeatures" class="VideoText">  
		<form id="frmadd" name="frmadd" action="index.php?file=to-email_templates_a" method="post">
        <input type="hidden" name="iEmailId" id="iEmailId" value="{$iEmailId}" />
        <input type="hidden" name="action" id="action" value="{$mode}" />
		 
    	<p>
				<label for="textfield"><strong>Title :</strong></label>
				<input type="text" id="vEmail_Code" name="Data[vEmail_Code]" class="inputbox" value="{$db_faq_cat[0].vEmail_Code}" lang="*" title="Title"/>
			</p>
      
      <p>
				<label for="textfield"><strong>Subject :</strong></label>
				<input type="text" id="vSubject_EN" name="Data[vSubject_EN]" class="inputbox" value="{$db_faq_cat[0].vSubject_EN}" lang="*" title="Subject"/>
			</p>
      			
		  <p>
				<label for="textfield"><strong>Body :</strong></label>
				{fckeditor BasePath="../plugins/FCKeditor/" InstanceName="Data[vBody_EN]" Width="500px" Height="400px" Value="{$db_faq_cat[0].vBody_EN}"}
			</p>
			
			
			{if $mode eq 'edit'}
		  <input type="submit" value="Edit System E-mail" class="btn" onclick="return validate(document.frmadd);" title="Edit Area"/>
		  {else}
			<input type="submit" value="Add System E-mail" class="btn" title="Add System E-mail" onclick="return validate(document.frmadd);"/>
			{/if}
			<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
		</form>
		</div>
</div>
</div>
{literal}
<script>
function redirectcancel(){
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=to-email_templates&mode=view";
    return false;
} 
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
{/literal}