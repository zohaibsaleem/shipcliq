<div class="contentcontainer">
    <div class="headings alt">
        <h2>Edit General Settings</h2>
    </div>
    <div class="contentbox">
    {if $var_msg neq ''}
     <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg}</p> 
     </div>     
    <div></div>
    {/if}
        <form  name="frmadd" id="frmadd"  method="post" action="index.php?file=to-generalsettings_a" enctype="multipart/form-data">
    		<p style="float:right;">
    		  <select name="eType" id="eType" class="inputbox" style="width:200px;" onChange="go_for_change(this.value);">
    		    <option value=""> -- Select -- </option>
    		    <option value="General" {if $eType eq 'General'} selected {/if}>General</option>
    		    <option value="Email" {if $eType eq 'Email'} selected {/if}>Email</option>
    		    <!--<option value="Apperance" {if $eType eq 'Apperance'} selected {/if}>Apperance</option>-->
    		    {if $smarty.const.PAYMENT_OPTION neq 'Contact'}
            <option value="Paypal" {if $eType eq 'Paypal'} selected {/if}>Paypal</option>
            {/if}
    		    <!--<option value="Prices" {if $eType eq 'Prices'} selected {/if}>Prices</option>-->
    		  </select>
    		</p>
        <input type="hidden" name="action" id="action" value="{$mode}" />
		<input type="hidden" name="imagemode" id="imagemode" value="">
            {section name=i loop=$db_res}
				{if $db_res[i].eType neq $currType}{/if}
				<p>
					<label for="textfield"><strong>{$db_res[i].tDescription}<em>*</em></strong></label>
					{if $db_res[i].tDescription eq "Site Logo" or $db_res[i].tDescription eq "Site Favicon"}
						<input type="file" id="{$db_res[i].vName}" name="Data[{$db_res[i].vName}]" value="{$db_res[i].vValue}" title="{$db_res[i].tDescription}"/> 
						{if $db_res[i].vValue neq ""}
							&nbsp;&nbsp;[<a href="{$tconfig.tsite_upload_site_favicon}/{$db_res[i].vValue}" target="_blank" rel="[images]">View</a>]
							&nbsp;&nbsp;[<a href="javascript:void(0);" onclick="confirm_delete('{$db_res[i].vName}');" title="Delete Image">Delete</a>]
						{/if}
						{if $db_res[i].tDescription eq "Site Logo"}
							<br><br>[Note : Preferable size would be (width:215px; height:55px;)]
							<br>[Note: Supported File Type is *.png]
						{/if}
						{if $db_res[i].tDescription eq "Site Favicon"}
							<br><br>[Note : Preferable size would be (width:16px; height:16px;)]
							<br>[Note: Supported File Type is *.ico]
						{/if}
						
					{elseif $db_res[i].vName EQ "COMPANY_ADDRESS" OR $db_res[i].vName EQ "COMPANY_CONTACTUS_ADDRESS" OR $db_res[i].vName EQ "GOOGLE_MAP"}
					<textarea id="{$db_res[i].vName}" name="Data[{$db_res[i].vName}]" class="inputbox" lang="*">{$db_res[i].vValue}</textarea>
					{if $db_res[i].vName EQ "GOOGLE_MAP"}<br />[Note: Please upload google iframe code of width 284 an height 211 for display map.]{/if}
					{elseif $db_res[i].vName EQ "LANG_BOX"}
						<Select name="Data[{$db_res[i].vName}]" class="inputbox">
							<option value="Yes" {if $db_res[i].vValue eq 'Yes'}selected{/if}>Yes</option>
							<option value="No" {if $db_res[i].vValue eq 'No'}selected{/if}>No</option>
						</select>	
					{elseif $db_res[i].vName EQ "SELECT_THEME"}
						<Select name="Data[{$db_res[i].vName}]" class="inputbox">
							<!--------------------BLABLACAR STYLE TEMPLATE Default ----------------------------------------------->
							<option value="Default-green-home" {if $db_res[i].vValue eq 'Default-green-home'}selected{/if}>Default</option>
							<option value="Theme1-blue-home" {if $db_res[i].vValue eq 'Theme1-blue-home'}selected{/if}>Theme 1</option>
							<option value="Theme2-orange-home" {if $db_res[i].vValue eq 'Theme2-orange-home'}selected{/if}>Theme 2</option>
							<option value="Theme3-skyblue-home" {if $db_res[i].vValue eq 'Theme3-skyblue-home'}selected{/if}>Theme 3</option>
							<option value="Theme4-red-home" {if $db_res[i].vValue eq 'Theme4-red-home'}selected{/if}>Theme 4</option>
							<option value="Theme5-brown-home" {if $db_res[i].vValue eq 'Theme5-brown-home'}selected{/if}>Theme 5</option>
							<option value="Theme6-purple-home" {if $db_res[i].vValue eq 'Theme6-purple-home'}selected{/if}>Theme 6</option>
							<!--------------------BLABLACAR STYLE TEMPLATE Default  Ends----------------------------------------------------->
							<!--------------------FULL SCREEN DESIGN TEMPLATES � STYLE 1 ----------------------------------------------------->
							<option value="Theme7-orange-home1" {if $db_res[i].vValue eq 'Theme7-orange-home1'}selected{/if}>Theme 7</option>
							<option value="Theme8-red-home1" {if $db_res[i].vValue eq 'Theme8-red-home1'}selected{/if}>Theme 8</option>
							<option value="Theme9-green-home1" {if $db_res[i].vValue eq 'Theme9-green-home1'}selected{/if}>Theme 9</option>
							<option value="Theme10-lightgreen-home1" {if $db_res[i].vValue eq 'Theme10-lightgreen-home1'}selected{/if}>Theme 10</option>
							<!----------FULL SCREEN DESIGN TEMPLATES � STYLE 1 Ends ----------------------------------------------------->
							<!--------------FULL SCREEN DESIGN TEMPLATES � STYLE 4--------------------------------------------------------->
							<option value="Theme17-skyblue-home2" {if $db_res[i].vValue eq 'Theme17-skyblue-home2'}selected{/if}>Theme 17</option>
							<option value="Theme18-green-home2" {if $db_res[i].vValue eq 'Theme18-green-home2'}selected{/if}>Theme 18</option>
							<option value="Theme19-brown-home2" {if $db_res[i].vValue eq 'Theme19-brown-home2'}selected{/if}>Theme 19</option>
							<option value="Theme20-red-home2" {if $db_res[i].vValue eq 'Theme20-red-home2'}selected{/if}>Theme 20</option>
							<option value="Theme21-orange-home2" {if $db_res[i].vValue eq 'Theme21-orange-home2'}selected{/if}>Theme 21</option>
							<option value="Theme22-blue-home2" {if $db_res[i].vValue eq 'Theme22-blue-home2'}selected{/if}>Theme 22</option>
							<!-------------FULL SCREEN DESIGN TEMPLATES � STYLE 4 Ends--------------------------------------------------------->
							<!------------FULL SCREEN DESIGN TEMPLATES � STYLE 3--------------------------------------------------------->
							<option value="Theme23-green-home3" {if $db_res[i].vValue eq 'Theme23-green-home3'}selected{/if}>Theme 23</option>
							<option value="Theme26-skyblue-home3" {if $db_res[i].vValue eq 'Theme26-skyblue-home3'}selected{/if}>Theme 26</option>
							<!-------------FULL SCREEN DESIGN TEMPLATES � STYLE 3 Ends--------------------------------------------------------->
							<!-------FULL SCREEN DESIGN TEMPLATES � STYLE 2 ----------------------------------------------------->
							<option value="Theme29-orange-home4" {if $db_res[i].vValue eq 'Theme29-orange-home4'}selected{/if}>Theme 29</option>
							<option value="Theme31-green-home4"  {if $db_res[i].vValue eq 'Theme31-green-home4'}selected{/if}>Theme 31</option>
							<!----------FULL SCREEN DESIGN TEMPLATES � STYLE 2 ----------------------------------------------------->
							<!--
							<option value="Theme9-green-home1" {if $db_res[i].vValue eq 'Theme9-green-home1'}selected{/if}>Theme 9</option>
							<option value="Theme10-orangel-home1" {if $db_res[i].vValue eq 'Theme10-orangel-home1'}selected{/if}>Theme 10</option>
							<option value="Theme11-seagreen-home1" {if $db_res[i].vValue eq 'Theme11-seagreen-home1'}selected{/if}>Theme 11</option>
							<option value="Theme12-orchid-home1" {if $db_res[i].vValue eq 'Theme12-orchid-home1'}selected{/if}>Theme 12</option>-->
						</select>				
					{else}
						<input type="text" id="{$db_res[i].vName}" name="Data[{$db_res[i].vName}]" class="inputbox" value="{$db_res[i].vValue}" lang="*" title="{$db_res[i].tDescription}"/> <br />
					{/if}
				</p>
				{assign var="currType" value="$db_res[i].eType"}
            {/section}
      	<input type="submit" value="Save Settings" class="btn" title="Save Settings" onclick="return validate(document.frmadd);"/>
        </form>
    </div>
</div>
{literal}
<script>
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}

function go_for_change(val){
  window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=to-generalsettings&mode=edit&eType="+val;
  return false;
}
function confirm_delete(val)
{
	//alert(val);
   ans = confirm('Are you sure for Delete Image?');
   if(ans == true)
   {
    document.frmadd.action.value = 'confirm_delete';
	document.frmadd.imagemode.value = val;
    document.frmadd.submit();
   }
   else
   {
    return false;
   }
}
</script>
{/literal}        