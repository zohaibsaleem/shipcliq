<div class="contentcontainer">
    <div class="headings alt">
        <h2>Edit General Settings</h2>
	</div>
    <div class="contentbox">
		{if $var_msg neq ''}
		<div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
			{$var_msg}</p> 
		</div>     
		<div></div>
		{/if}
        <form  name="frmadd" id="frmadd"  method="post" action="index.php?file=to-generalsettings_a" enctype="multipart/form-data">
    		<p style="float:right;">
				<select name="eType" id="eType" class="inputbox" style="width:200px;" onChange="go_for_change(this.value);">
					<option value=""> -- Select -- </option>
					<option value="General" {if $eType eq 'General'} selected {/if}>General</option>
					<option value="Email" {if $eType eq 'Email'} selected {/if}>Email</option>
					<!--<option value="Apperance" {if $eType eq 'Apperance'} selected {/if}>Apperance</option>-->
					{if $smarty.const.PAYMENT_OPTION neq 'Contact'}
					<option value="Paypal" {if $eType eq 'Paypal'} selected {/if}>Paypal</option>
					<option value="SEO" {if $eType eq 'SEO'} selected {/if}>SEO</option>
					{/if}
					<!--<option value="Prices" {if $eType eq 'Prices'} selected {/if}>Prices</option>-->
				</select>
			</p>
			<input type="hidden" name="action" id="action" value="{$mode}" />
			<input type="hidden" name="imagemode" id="imagemode" value="">
            {section name=i loop=$db_res}
			{if $db_res[i].eType neq $currType}{/if}
			<p>
				<label for="textfield"><strong>{$db_res[i].tDescription}<em>*</em></strong></label>
				
				{if $db_res[i].tDescription eq "Site Logo" or $db_res[i].tDescription eq "Site Favicon" or $db_res[i].tDescription eq "Footer Logo" or $db_res[i].tDescription eq "Email Logo" or $db_res[i].tDescription eq "Ride Background"}
				<input type="file" id="{$db_res[i].vName}" name="Data[{$db_res[i].vName}]" value="{$db_res[i].vValue}" title="{$db_res[i].tDescription}"/> 
				{if $db_res[i].vValue neq "" && $db_res[i].tDescription eq "Site Favicon"}
				&nbsp;&nbsp;[<a href="{$tconfig.tsite_upload_site_favicon}/{$db_res[i].vValue}" target="_blank" rel="[images]">View</a>]
				&nbsp;&nbsp;[<a href="javascript:void(0);" onclick="confirm_delete('{$db_res[i].vName}');" title="Delete Image">Delete</a>]
				{/if}
				
				{if $db_res[i].vValue neq "" && $db_res[i].tDescription eq "Site Logo"}
				&nbsp;&nbsp;[<a href="{$tconfig.tsite_upload_site_logo}/1_{$db_res[i].vValue}" target="_blank" rel="[images]">View</a>]
				&nbsp;&nbsp;[<a href="javascript:void(0);" onclick="confirm_delete('{$db_res[i].vName}');" title="Delete Image">Delete</a>]
				{/if}
				
				{if $db_res[i].vValue neq "" && $db_res[i].tDescription eq "Email Logo"}
				&nbsp;&nbsp;[<a href="{$tconfig.tsite_upload_email_logo}/1_{$db_res[i].vValue}" target="_blank" rel="[images]">View</a>]
				&nbsp;&nbsp;[<a href="javascript:void(0);" onclick="confirm_delete('{$db_res[i].vName}');" title="Delete Image">Delete</a>]
				{/if}
				
				{if $db_res[i].vValue neq "" && $db_res[i].tDescription eq "Footer Logo"}
				&nbsp;&nbsp;[<a href="{$tconfig.tsite_upload_footer_logo}/1_{$db_res[i].vValue}" target="_blank" rel="[images]">View</a>]
				&nbsp;&nbsp;[<a href="javascript:void(0);" onclick="confirm_delete('{$db_res[i].vName}');" title="Delete Image">Delete</a>]
				{/if}
				
				{if $db_res[i].vValue neq "" && $db_res[i].tDescription eq "Ride Background"}
				&nbsp;&nbsp;[<a href="{$tconfig.tsite_upload_footer_logo}/1_{$db_res[i].vValue}" target="_blank" rel="[images]">View</a>]
				&nbsp;&nbsp;[<a href="javascript:void(0);" onclick="confirm_delete('{$db_res[i].vName}');" title="Delete Image">Delete</a>]
				{/if}
				
				{if $db_res[i].tDescription eq "Site Logo"}
				<br><br>[Note : Preferable size would be (width:215px; height:55px;)]
				<br>[Note: Supported File Type is *.png,jpg,jpeg,bmp,gif]
				{/if}
				{if $db_res[i].tDescription eq "Email Logo"}
				<br><br>[Note : Preferable size would be (width:215px; height:55px;)]
				<br>[Note: Supported File Type is *.png,jpg,jpeg,bmp,gif]
				{/if}
				{if $db_res[i].tDescription eq "Site Favicon"}
				<br><br>[Note : Preferable size would be (width:16px; height:16px;)]
				<br>[Note: Supported File Type is *.ico]
				{/if}
				{if $db_res[i].tDescription eq "Footer Logo"}
				<br><br>[Note : Preferable size would be (width:16px; height:16px;)]
				<br>[Note: Supported File Type is *.png,jpg,jpeg,bmp,gif]
				{/if}
				
				{if $db_res[i].tDescription eq "Ride Background"}
				<br><br>[Note : Preferable size would be (width:1150px; height:480px;)]
				<br>[Note: Supported File Type is *.png,jpg,jpeg,bmp,gif]
				{/if}
				{elseif $db_res[i].vName EQ "COMPANY_ADDRESS" OR $db_res[i].vName EQ "COMPANY_CONTACTUS_ADDRESS" OR $db_res[i].vName EQ "GOOGLE_MAP" OR $db_res[i].vName EQ "GOOGLE_ANALYTIC_CODE" OR $db_res[i].eFieldType EQ "Textarea"}
				
				<textarea id="{$db_res[i].vName}" name="Data[{$db_res[i].vName}]" class="inputbox" lang="*">{$db_res[i].vValue}</textarea>
				{if $db_res[i].vName EQ "GOOGLE_MAP"}<br />[Note: Please upload google iframe code of width 284 an height 211 for display map.]{/if}
				{elseif $db_res[i].vName EQ "LANG_BOX" OR $db_res[i].vName EQ "GEO_LOCATION" or $db_res[i].vName EQ "SHOW_COOKIE_POLICY" or $db_res[i].vName EQ "AIRPORTS_RIDES_SHOW" or $db_res[i].vName EQ "CRON_MEMBERSHIP_EMAIL" or $db_res[i].vName EQ "CRON_SATISFACTION_EMAIL" or $db_res[i].vName EQ "PHONE_VERIFICATION_REQUIRED" or $db_res[i].vName EQ "LICENSE_VERIFICATION_REQUIRED" or $db_res[i].vName EQ "CARPAPER_VERIFICATION_REQUIRED"}
				<select name="Data[{$db_res[i].vName}]" class="inputbox">
					<option value="Yes" {if $db_res[i].vValue eq 'Yes'}selected{/if}>Yes</option>
					<option value="No" {if $db_res[i].vValue eq 'No'}selected{/if}>No</option>
				</select>
				{elseif $db_res[i].vName EQ "PAYMENT_OPTION"}
				<select name="Data[{$db_res[i].vName}]" class="inputbox">
					<option value="PayPal" {if $db_res[i].vValue eq 'PayPal'}selected{/if}>Online Payment (Payment gateway required)</option>
					<option value="Manual" {if $db_res[i].vValue eq 'Manual'}selected{/if}>Offline Payment (Payment gateway not required)</option>
					<!-- <option value="Contact" {if $db_res[i].vValue eq 'Contact'}selected{/if}>No Payment</option>  -->
				</select>	
				{elseif $db_res[i].vName EQ "SELECT_THEME"}
				<Select name="Data[{$db_res[i].vName}]" class="inputbox">
					<option value="Theme29-orange-home4" {if $db_res[i].vValue eq 'Theme29-orange-home4'}selected{/if}>Theme 29</option>
				</select>				
				{else}
				<input type="text" id="{$db_res[i].vName}" name="Data[{$db_res[i].vName}]" class="inputbox" value="{$db_res[i].vValue}" lang="*" title="{$db_res[i].tDescription}"/> <br />
				{/if}
			</p>
			{assign var="currType" value="$db_res[i].eType"}
            {/section}
			<input type="submit" value="Save Settings" class="btn" title="Save Settings" onclick="return validate(document.frmadd);"/>
		</form>
	</div>
</div>
{literal}
<script>
	function hidemessage(){
		jQuery("#errormsgdiv").slideUp();
	}
	
	function go_for_change(val){
		window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=to-generalsettings&mode=edit&eType="+val;
		return false;
	}
	function confirm_delete(val)
	{
		//alert(val);
		ans = confirm('Are you sure for Delete Image?');
		if(ans == true)
		{
			document.frmadd.action.value = 'confirm_delete';
			document.frmadd.imagemode.value = val;
			document.frmadd.submit();
		}
		else
		{
			return false;
		}
	}
</script>
{/literal}        