<script type="text/javascript" src="../plugins/ckeditor/ckeditor.js"></script>
<div class="contentcontainer" id="tabs">
	<div class="headings">
		{if $mode eq 'edit'}
    <h2 class="left">Edit Label</h2>
    {else}
    <h2 class="left">Add Label</h2>
    {/if}
	</div>
	<div class="contentbox" id="tabs-1">
      {if $mode eq 'edit'}
         <form id="frmadd" name="frmadd" action="index.php?file=to-languagelabel_a" method="post">
            <input type="hidden" name="LanguageLabelId" id="LanguageLabelId" value="{$LanguageLabelId}" />
            <input type="hidden" name="action" id="action" value="{$mode}" />
				<p>
					<label for="textfield"><strong>Label :</strong></label>
					<input type="text" id="vLabel" name="Data[vLabel]" class="inputbox" value="{$db_labels[0].vLabel}" lang="*" title="Label" />
				</p>
        {section name=i loop=$db_lang}
				{section name=lab loop=$db_labels}
				{if $db_lang[i].vCode eq $db_labels[lab].vCode}
        <p>
					<label for="textfield"><strong>Value [{$db_lang[i].vTitle}] :</strong></label>
					<textarea id="vValue" name="Data[vValue_{$db_lang[i].vCode}]" class="inputbox">{$db_labels[lab].vValue}</textarea>
				</p>
				{/if}
				{/section}
        {/section}
				{if $mode eq 'edit'}
				<input type="submit" value="Edit Label" class="btn" onclick="return validate(document.frmadd);" title="Edit Label"/>
				      {else}
   				<input type="submit" value="Add Label" class="btn" onclick="return validate(document.frmadd);" title="Add Label"/>
  				
				      
				      {/if}
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
			</form>
      {else}
      	<form id="frmadd" name="frmadd" action="index.php?file=to-languagelabel_a" method="post">
            <input type="hidden" name="LanguageLabelId" id="LanguageLabelId" value="{$LanguageLabelId}" />
            <input type="hidden" name="action" id="action" value="{$mode}" />
				<p>
					<label for="textfield"><strong>Label :</strong></label>
					<input type="text" id="vLabel" name="Data[vLabel]" class="inputbox" value="{$db_labels[0].vLabel}" lang="*" title="Label" />
				</p>
        {section name=i loop=$db_lang}
				 <p>
					<label for="textfield"><strong>Value [{$db_lang[i].vTitle}] :</strong></label>
					<textarea id="vValue" name="Data[vValue_{$db_lang[i].vCode}]" class="inputbox">{$db_labels[i].vValue}</textarea>
				</p>
        {/section}
				{if $mode eq 'edit'}
				<input type="submit" value="Edit Label" class="btn" onclick="return validate(document.frmadd);" title="Edit Label"/>
				      {else}
   				<input type="submit" value="Add Label" class="btn" onclick="return validate(document.frmadd);" title="Add Label"/>
  				
				      
				      {/if}
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
			</form>
      {/if}      
			
     
	</div>

</div>
{literal}
<script>
function redirectcancel()
{
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=to-languagelabel&mode=view";
    return false;
}

</script>
{/literal}