<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>{$DEFAULT_META_TITLE}</title>
<link rel="icon" href="{$tconfig.tsite_url}favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="{$tconfig.tsite_url}favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" href="{$tconfig.tsite_url}favicon.ico">
<!--<link rel="icon" type="image/vnd.microsoft.icon" href="{$tconfig.tsite_url}favicon.ico" />-->
<meta name="keywords" content="{$DEFAULT_META_KEYWORD}" />
<meta name="description" content="{$DEFAULT_META_DESCRIPTION}" />
<base href="{$tconfig.tsite_url}"  />
{literal}<script>var site_url = '{/literal}{$tconfig.tsite_url}{literal}';</script>{/literal}
{if $homepage eq 'Yes'}
<link href="{$tconfig.tsite_stylesheets}{$THEME}/home/home.css" rel="stylesheet" type="text/css" />
<link href="{$tconfig.tsite_stylesheets}{$THEME}/home/{$THEMECSS}.css" rel="stylesheet" type="text/css" />
<link href="{$tconfig.tsite_stylesheets}{$THEME}/home/homemedia.css" rel="stylesheet" type="text/css" />
<link href="{$tconfig.tsite_stylesheets}{$THEME}/home/homemenu.css" rel="stylesheet" type="text/css" />

<!----------------- HOME LANGUAGE CSS ------------------------>
<link href="{$tconfig.tsite_stylesheets}{$THEME}/{$sess_lang}/home.css" rel="stylesheet" type="text/css" />
<link href="{$tconfig.tsite_stylesheets}{$THEME}/{$sess_lang}/homemedia.css" rel="stylesheet" type="text/css" />
<link href="{$tconfig.tsite_stylesheets}{$THEME}/{$sess_lang}/homemenu.css" rel="stylesheet" type="text/css" />
<!----------------- /HOME LANGUAGE CSS ------------------------>

{else}
<link href="{$tconfig.tsite_stylesheets}front/style.css" rel="stylesheet" type="text/css" />
<link href="{$tconfig.tsite_stylesheets}{$THEME}/home/{$THEMECSS}.css" rel="stylesheet" type="text/css" />
<link href="{$tconfig.tsite_stylesheets}front/media.css" rel="stylesheet" type="text/css" />
<link href="{$tconfig.tsite_stylesheets}front/menu.css" rel="stylesheet" type="text/css" />

<!----------------- INNER LANGUAGE CSS ------------------------>
<link href="{$tconfig.tsite_stylesheets}{$THEME}/{$sess_lang}/style.css" rel="stylesheet" type="text/css" />
<link href="{$tconfig.tsite_stylesheets}{$THEME}/{$sess_lang}/media.css" rel="stylesheet" type="text/css" />
<link href="{$tconfig.tsite_stylesheets}{$THEME}/{$sess_lang}/menu.css" rel="stylesheet" type="text/css" />
<!----------------- /INNER LANGUAGE CSS ------------------------>

{/if}

<script type="text/javascript" src="{$tconfig.tsite_javascript}jquery-1.7.1.min.js"></script>
<link rel="stylesheet" href="{$tconfig.tsite_stylesheets}front/validationEngine.jquery.css" type="text/css"/> 
<script src="{$tconfig.tsite_javascript}jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script> 
<script src="{$tconfig.tsite_javascript}jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>   
<link href="{$tconfig.tsite_url}notifications/css/jquery_notification.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="{$tconfig.tsite_url}notifications/js/jquery_notification_v.1.js"></script>
<script src="{$tconfig.tsite_javascript}ajax_newsletter.js"></script>
<script src="{$tconfig.tsite_javascript}script.js"></script>  
{if $SHOW_COOKIE_POLICY eq 'Yes'}
{literal}
<script>
var cookietext = '{/literal}{$smarty.const.LBL_COOKIE_TEXT}{literal}';
var viewcookie = '{/literal}{$smarty.const.LBL_VIEW_COOKIE}{literal}';
var cookieaccept = '{/literal}{$smarty.const.LBL_ACCEPT_COOKIE}{literal}';
</script>
{/literal}
<link href="{$tconfig.tsite_stylesheets}front/twc.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{$tconfig.tsite_javascript}cookies.js"></script>
<!--<script type="text/javascript" src="{$tconfig.tsite_javascript}TWCcookies.js"></script>-->
{/if}
{literal} 
<script>
	  /*(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-49621023-1', 'blablacarscript.com');
	  ga('send', 'pageview');*/
	</script>
{/literal}
</head>
<body>
{if $homepage eq 'Yes'}
{include file="$include_template"}
{else}
<div id="main">
{include file="innertop.tpl"} 
<div id="main-div-top-bg">
<div id="main-div-top-second-bg">
<div id="main-div-top-second-bg-new-in">
  {include file="header.tpl"}   
  <div id="body-content">
  {*if $ismobile eq 'No'*}
  {*if $script neq 'home'*}
  <!--<div class="site-banner-main2">
  <div class="sitebanner-left-img2"><img src="{$tconfig.tsite_images}banner-left1.jpg" /></div>-->
  {*/if*}
  {*/if*}
  {include file="$include_template"}
  {*if $ismobile eq 'No'*}
  {*if $script neq 'home'*}
  <!--<div class="sitebanner-right-img2"><img src="{$tconfig.tsite_images}banner-left1.jpg" /></div>
  </div>-->
  {*/if*}
  {*/if*}
  {include file="footer.tpl"} 
  </div>
  <div style="clear:both;"></div>
</div>
  <div style="clear:both;"></div>
</div>

  <div style="clear:both;"></div>
</div>

 <div style="clear:both;"></div>
</div>
{/if}
{$GOOGLE_ANALYTICS_CODE}
</body>
</html>
