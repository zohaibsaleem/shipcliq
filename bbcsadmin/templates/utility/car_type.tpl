<div class="contentcontainer">
<div class="headings"> 
  	{if $mode eq 'edit'}
		<h2 class="left">Edit Car Type</h2>
    {else}
	<h2 class="left">Add Car Type</h2>
    {/if}

</div>
<div class="contentbox" id="tabs-1">
{if $var_msg neq ''}
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Success" />
					{$var_msg}</p> 
				</div>     
				<div></div>
			{/if}
			{if $var_msg_error neq ''}
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Success" />
					{$var_msg_error}</p> 
				</div>     
				<div></div>
			{/if}
		<form id="frmadd" name="frmadd" action="index.php?file=u-car_type_a" method="post">
        <input type="hidden" name="iCarTypeId" id="iCarTypeId" value="{$iCarTypeId}" />
        <input type="hidden" name="action" id="action" value="{$mode}" />
    {section name=i loop=$db_languagemas}              
			<p>
				<label for="textfield"><strong>Car Type [ {$vLanguage[i]} ]:<em>*</em></strong></label>
				<input type="text" id="{$vLanCode[i]}" name="Data[{$vLanCode[i]}]" class="inputbox" value="{$db_faq[0].{$vLanCode[i]}}" lang="*" title="Car Type {$vLanguage[i]}"/>
        
			</p>
    {/section}        
		<!--	<p>
				<label for="textfield"><strong>Car Type [English]:<em>*</em></strong></label>
				<input type="text" id="vTitle_EN" name="Data[vTitle_EN]" class="inputbox" value="{$db_faq[0].vTitle_EN}" lang="*" title="Car Type English"/>
			</p>
      
      <p>
				<label for="textfield"><strong>Car Type [French]:<em>*</em></strong></label>
				<input type="text" id="vTitle_FN" name="Data[vTitle_FN]" class="inputbox" value="{$db_faq[0].vTitle_FN}" lang="*" title="Car Type French"/>
			</p>
      
      <p>
				<label for="textfield"><strong>Car Type [Latvian]:<em>*</em></strong></label>
				<input type="text" id="vTitle_LV" name="Data[vTitle_LV]" class="inputbox" value="{$db_faq[0].vTitle_LV}" lang="*" title="Car Type Latvian"/>
			</p>
      
      <p>
				<label for="textfield"><strong>Car Type [Estonian]:<em>*</em></strong></label>
				<input type="text" id="vTitle_EE" name="Data[vTitle_EE]" class="inputbox" value="{$db_faq[0].vTitle_EE}" lang="*" title="Car Type Estonian"/>
			</p>
      
      <p>
				<label for="textfield"><strong>Car Type [Lithuanian]:<em>*</em></strong></label>
				<input type="text" id="vTitle_LT" name="Data[vTitle_LT]" class="inputbox" value="{$db_faq[0].vTitle_LT}" lang="*" title="Car Type Lithuanian"/>
			</p>
      
      <p>
				<label for="textfield"><strong>Car Type [German]:<em>*</em></strong></label>
				<input type="text" id="vTitle_DE" name="Data[vTitle_DE]" class="inputbox" value="{$db_faq[0].vTitle_DE}" lang="*" title="Car Type German"/>
			</p>
      
      <p>
				<label for="textfield"><strong>Car Type [Russian]:<em>*</em></strong></label>
				<input type="text" id="vTitle_RS" name="Data[vTitle_RS]" class="inputbox" value="{$db_faq[0].vTitle_RS}" lang="*" title="Car Type Russian"/>
			</p> -->
          
			<p>
				<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]">
					<option value="Active" {if $db_faq[0].eStatus eq Active}selected{/if} >Active</option>
					<option value="Inactive" {if $db_faq[0].eStatus eq Inactive}selected{/if} >Inactive</option>
				</select>
			</p>
			{if $mode eq 'edit'}
			<input type="submit" value="Save Car Type" class="btn" title="Save Car Type" onclick="return validate(document.frmadd);"/>
			{else}
			<input type="submit" value="Add  Car Type" class="btn" title="Add Car Type" onclick="return validate(document.frmadd);"/>
			{/if}
			<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
		</form>
</div>
</div>
{literal}
<script>
function redirectcancel(){
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=u-car_type&mode=view";
    return false;
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
{/literal}
