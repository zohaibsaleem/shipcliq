<div class="contentcontainer">
<div class="headings"> 
  	{if $mode eq 'edit'}
		<h2 class="left">Edit Car Colour</h2>
    {else}
	<h2 class="left">Add Car Colour</h2>
    {/if}

</div>
<div class="contentbox" id="tabs-1">
{if $var_msg neq ''}
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Success" />
					{$var_msg}</p> 
				</div>     
				<div></div>
			{/if}
			{if $var_msg_error neq ''}
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Success" />
					{$var_msg_error}</p> 
				</div>     
				<div></div>
			{/if}
		<form id="frmadd" name="frmadd" action="index.php?file=u-color_a" method="post">
        <input type="hidden" name="iColourId" id="iColourId" value="{$iColourId}" />
        <input type="hidden" name="action" id="action" value="{$mode}" />
    {section name=i loop=$db_languagemas}              
			<p>
				<label for="textfield"><strong>Colour [ {$vColour[i]} ]:<em>*</em></strong></label>
				<input type="text" id="{$vCode[i]}" name="Data[{$vCode[i]}]" class="inputbox" value="{$db_faq[0].{$vCode[i]}}" lang="*" title="Colour {$vColour[i]}"/>
        
			</p>
    {/section}
      <!--<p>
				<label for="textfield"><strong>Colour [French]:<em>*</em></strong></label>
				<input type="text" id="vColour_FN" name="Data[vColour_FN]" class="inputbox" value="{$db_faq[0].vColour_FN}" lang="*" title="Colour French"/>
			</p>
      
      <p>
				<label for="textfield"><strong>Colour [Latvian]:<em>*</em></strong></label>
				<input type="text" id="vColour_LV" name="Data[vColour_LV]" class="inputbox" value="{$db_faq[0].vColour_LV}" lang="*" title="Colour Latvian"/>
			</p>
      
      <p>
				<label for="textfield"><strong>Colour [Estonian]:<em>*</em></strong></label>
				<input type="text" id="vColour_EE" name="Data[vColour_EE]" class="inputbox" value="{$db_faq[0].vColour_EE}" lang="*" title="Colour Estonian"/>
			</p>
      
      <p>
				<label for="textfield"><strong>Colour [Lithuanian]:<em>*</em></strong></label>
				<input type="text" id="vColour_LT" name="Data[vColour_LT]" class="inputbox" value="{$db_faq[0].vColour_LT}" lang="*" title="Colour Lithuanian"/>
			</p>
      
      <p>
				<label for="textfield"><strong>Colour [German]:<em>*</em></strong></label>
				<input type="text" id="vColour_DE" name="Data[vColour_DE]" class="inputbox" value="{$db_faq[0].vColour_DE}" lang="*" title="Colour German"/>
			</p>
      
      <p>
				<label for="textfield"><strong>Colour [Russian]:<em>*</em></strong></label>
				<input type="text" id="vColour_RS" name="Data[vColour_RS]" class="inputbox" value="{$db_faq[0].vColour_RS}" lang="*" title="Colour Russian"/>
			</p> -->
          
			<p>
				<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]">
					<option value="Active" {if $db_faq[0].eStatus eq Active}selected{/if} >Active</option>
					<option value="Inactive" {if $db_faq[0].eStatus eq Inactive}selected{/if} >Inactive</option>
				</select>
			</p>
			{if $mode eq 'edit'}
      
			<input type="submit" value="Save Car Colour" class="btn" title="Save Car Colour" onclick="return validate(document.frmadd);"/>
			{else}
			<input type="submit" value="Add Car Colour" class="btn" title="Add Car Colour" onclick="return validate(document.frmadd);"/>
			{/if}
			<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
		</form>
</div>
</div>
{literal}
<script>
function redirectcancel(){
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=u-color&mode=view";
    return false;
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
{/literal}
