<script type="text/javascript" src="../plugins/ckeditor/ckeditor.js"></script>
<div class="contentcontainer" id="tabs">
	<div class="headings">
		{if $mode eq 'edit'}
			<h2 class="left">Edit Email Template ( {$db_email_templates[0].vEmail_Code} )
                    
            </h2>
        {else}
			<h2 class="left">Add Email Template</h2>
        {/if}
	</div>
	<div class="contentbox" id="tabs-1">
		{if $var_msg neq ""}
		<div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg}</p> 
     </div>
     {/if}
    <form id="frmadd" name="frmadd" action="index.php?file=u-email_templates_a" method="post">
    <input type="hidden" name="iEmailId" id="iEmailId" value="{$iEmailId}" />
    <input type="hidden" name="action" id="action" value="{$mode}" />
    <p>
  		<label for="textfield"><strong>Email Code :</strong></label>
  		<input type="text" id="vEmail_Code" name="Data[vEmail_Code]" class="inputbox" value="{$db_email_templates[0].vEmail_Code}" readonly title="Email Code"/> 
	  </p>

	  {section name='rrb' loop=$db_languagemas}              
  		<p>
  			<label for="textfield"><strong>Subject [{$db_languagemas[rrb].vTitle}]:</strong></label>
  			<input type="text" id="vSubject_{$db_languagemas[rrb].vCode}" name="vSubject_{$db_languagemas[rrb].vCode}" class="inputbox" value="{$db_languagemas[rrb].ttl}" title="Email Subject [{$db_languagemas[rrb].vTitle}]"/>
        </p>
  			
  		<p>
  			<label for="textfield"><strong>Email Template Body [{$db_languagemas[rrb].vTitle}] :</strong></label>
  			{$db_languagemas[rrb].editor}
  		</p>    			
        {/section} 
	<!--
    <p>
			<label for="textfield"><strong>Subject [English]:</strong></label>
			<input type="text" id="vSubject_EN" name="Data[vSubject_EN]" class="inputbox" value="{$db_email_templates[0].vSubject_EN}" lang="*" title="Email Subject English"/>
	</p>
	
    <p>        
			<label for="textfield"><strong>Email Template Body [English]:</strong></label>
      {fckeditor BasePath="../plugins/FCKeditor/" InstanceName="Data[vBody_EN]" Width="850px" Height="550px" Value="$vBody_EN"}
		</p>
    <p>
			<label for="textfield"><strong>Subject [Norwegian]:</strong></label>
			<input type="text" id="vSubject_NO" name="Data[vSubject_NO]" class="inputbox" value="{$db_email_templates[0].vSubject_NO}" lang="*" title="Email Subject Norwegian"/>
		</p>
    <p>        
			<label for="textfield"><strong>Email Template Body [Norwegian]:</strong></label>
      {fckeditor BasePath="../plugins/FCKeditor/" InstanceName="Data[vBody_NO]" Width="850px" Height="550px" Value="$vBody_NO"}
		</p>
     <p>
			<label for="textfield"><strong>Subject [Polish]:</strong></label>
			<input type="text" id="vSubject_PO" name="Data[vSubject_PO]" class="inputbox" value="{$db_email_templates[0].vSubject_PO}" lang="*" title="Email Subject Polish"/>
		</p>
    <p>        
			<label for="textfield"><strong>Email Template Body [Polish]:</strong></label>
      {fckeditor BasePath="../plugins/FCKeditor/" InstanceName="Data[vBody_PO]" Width="850px" Height="550px" Value="$vBody_PO"}
		</p>
    <p>
			<label for="textfield"><strong>Subject [French]:</strong></label>
			<input type="text" id="vSubject_FN" name="Data[vSubject_FN]" class="inputbox" value="{$db_email_templates[0].vSubject_FN}" lang="*" title="Email Subject French"/>
		</p>
    <p>        
  		<label for="textfield"><strong>Email Template Body [French]:</strong></label>
      {fckeditor BasePath="../plugins/FCKeditor/" InstanceName="Data[vBody_FN]" Width="850px" Height="550px" Value="$vBody_FN"}
		</p>
    <p>
			<label for="textfield"><strong>Subject [Russian]:</strong></label>
			<input type="text" id="vSubject_RS" name="Data[vSubject_RS]" class="inputbox" value="{$db_email_templates[0].vSubject_RS}" lang="*" title="Email Subject Russian"/>
		</p>
    <p>        
			<label for="textfield"><strong>Email Template Body [Russian]:</strong></label>
      {fckeditor BasePath="../plugins/FCKeditor/" InstanceName="Data[vBody_RS]" Width="850px" Height="550px" Value="$vBody_RS"}
		</p>		
    <p>
			<label for="textfield"><strong>Subject [Swedish]:</strong></label>
			<input type="text" id="vSubject_SW" name="Data[vSubject_SW]" class="inputbox" value="{$db_email_templates[0].vSubject_SW}" lang="*" title="Email Subject Swedish"/>
		</p>
    <p>        
			<label for="textfield"><strong>Email Template Body [Swedish]:</strong></label>
      {fckeditor BasePath="../plugins/FCKeditor/" InstanceName="Data[vBody_SW]" Width="850px" Height="550px" Value="$vBody_SW"}
		</p>
    <p>
			<label for="textfield"><strong>Subject [Danish]:</strong></label>
			<input type="text" id="vSubject_DN" name="Data[vSubject_DN]" class="inputbox" value="{$db_email_templates[0].vSubject_DN}" lang="*" title="Email Subject Danish"/>
		</p>
		<p>        
		 	<label for="textfield"><strong>Email Template Body [Danish]:</strong></label>
      {fckeditor BasePath="../plugins/FCKeditor/" InstanceName="Data[vBody_DN]" Width="850px" Height="550px" Value="$vBody_DN"}
		</p>
    <p>
			<label for="textfield"><strong>Subject [Finnish]:</strong></label>
			<input type="text" id="vSubject_FI" name="Data[vSubject_FI]" class="inputbox" value="{$db_email_templates[0].vSubject_FI}" lang="*" title="Email Subject Finnish"/>
		</p>
		<p>        
		 	<label for="textfield"><strong>Email Template Body [Finnish]:</strong></label>
      {fckeditor BasePath="../plugins/FCKeditor/" InstanceName="Data[vBody_FI]" Width="850px" Height="550px" Value="$vBody_FI"}
		</p>			
    <p>
			<label for="textfield"><strong>Subject [Latvian]:</strong></label>
			<input type="text" id="vSubject_LV" name="Data[vSubject_LV]" class="inputbox" value="{$db_email_templates[0].vSubject_LV}" lang="*" title="Email Subject Latvian"/>
		</p>
		<p>        
		 	<label for="textfield"><strong>Email Template Body [Latvian]:</strong></label>
      {fckeditor BasePath="../plugins/FCKeditor/" InstanceName="Data[vBody_LV]" Width="850px" Height="550px" Value="$vBody_LV"}
		</p>	
    <p>
			<label for="textfield"><strong>Subject [Estonian]:</strong></label>
			<input type="text" id="vSubject_EE" name="Data[vSubject_EE]" class="inputbox" value="{$db_email_templates[0].vSubject_EE}" lang="*" title="Email Subject Estonian"/>
		</p>
    <p>        
			<label for="textfield"><strong>Email Template Body [Estonian]:</strong></label>
      {fckeditor BasePath="../plugins/FCKeditor/" InstanceName="Data[vBody_EE]" Width="850px" Height="550px" Value="$vBody_EE"}
		</p>	
    <p>
  		<label for="textfield"><strong>Subject [Lithuanian]:</strong></label>
			<input type="text" id="vSubject_LT" name="Data[vSubject_LT]" class="inputbox" value="{$db_email_templates[0].vSubject_LT}" lang="*" title="Email Subject Lithuanian"/>
		</p>
    <p>        
			<label for="textfield"><strong>Email Template Body [Lithuanian]:</strong></label>
      {fckeditor BasePath="../plugins/FCKeditor/" InstanceName="Data[vBody_LT]" Width="850px" Height="550px" Value="$vBody_LT"}
		</p>	
    <p>
  		<label for="textfield"><strong>Subject [German]:</strong></label>
			<input type="text" id="vSubject_DE" name="Data[vSubject_DE]" class="inputbox" value="{$db_email_templates[0].vSubject_DE}" lang="*" title="Email Subject German"/>
	  </p>
    <p>        
			<label for="textfield"><strong>Email Template Body [German]:</strong></label>
      {fckeditor BasePath="../plugins/FCKeditor/" InstanceName="Data[vBody_DE]" Width="850px" Height="550px" Value="$vBody_DE"}
		</p>	
    <p>
  		<label for="textfield"><strong>Subject [Spanish]:</strong></label>
			<input type="text" id="vSubject_ES" name="Data[vSubject_ES]" class="inputbox" value="{$db_email_templates[0].vSubject_ES}" lang="*" title="Email Subject Spanish"/>
	  </p>
    <p>        
			<label for="textfield"><strong>Email Template Body [Spanish]:</strong></label>
      {fckeditor BasePath="../plugins/FCKeditor/" InstanceName="Data[vBody_ES]" Width="850px" Height="550px" Value="$vBody_ES"}
		</p> 
	-->
				<input type="submit" value="Save" class="btn" onclick="return validate(document.frmadd);" title="Save"/>
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
			</form>
	</div>
</div>
{literal}
<script>
function redirectcancel()
{
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=u-email_templates&mode=view";
    return false;
    
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
{/literal}