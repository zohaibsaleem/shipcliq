<div class="contentcontainer">
<div class="headings"> 
  	{if $mode eq 'edit'}
		<h2 class="left">Edit Faq</h2>
    {else}
	<h2 class="left">Add Faq</h2>
    {/if}

</div>
<div class="contentbox" id="tabs-1">
{if $var_msg neq ''}
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Success" />
					{$var_msg}</p> 
				</div>     
				<div></div>
			{/if}
			{if $var_msg_error neq ''}
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Success" />
					{$var_msg_error}</p> 
				</div>     
				<div></div>
			{/if}
		<form id="frmadd" name="frmadd" action="index.php?file=u-faq_a" method="post">
        <input type="hidden" name="iFaqId" id="iFaqId" value="{$iFaqId}" />
        <input type="hidden" name="action" id="action" value="{$mode}" />
        
        <p>
					<label for="textfield"><strong>Category :<em>*</em></strong></label>
				  <select name="Data[iFaqcategoryId]" id="iFaqcategoryId" style="width:200px;" lang="*" title="Category">
						<option value="">---Select Category---</option>
              {section name=i loop=$db_faq_cat}
              {if $db_faq[0].iFaqcategoryId eq $db_faq_cat[i].iFaqcategoryId}
                 {assign var="selected" value="selected"}
              {else}
              {assign var="selected" value=""}
              {/if}
              <option value="{$db_faq_cat[i].iFaqcategoryId}" {$selected}>{$db_faq_cat[i].vTitle_EN}</option>
              {/section}
					</select>
				</p> 
        {section name=i loop=$db_languagemas}       
			{assign var="title" value="vTitle_{$db_languagemas[i].vCode}"}  
			{assign var="description" value="tAnswer_{$db_languagemas[i].vCode}"} 
			 <p>
				<label for="textfield"><strong>Question [ {$db_languagemas[i].vTitle} ]:<em>*</em></strong></label>
				<input type="text" id="{$title}" name="Data[{$title}]" class="inputbox" value="{$db_faq[0].$title}" lang="*" title="Question  {$db_languagemas[i].vTitle}"/>
        
			</p>   			
      <p>
					<label for="textfield"><strong>Answer [ {$db_languagemas[i].vTitle} ]:<em>*</em></strong></label>
					<textarea class="text-input textarea" id="{$description}" name="Data[{$description}]" rows="7" cols="75" lang="*" title="Answer  {$db_languagemas[i].vTitle}">{$db_faq[0].$description}</textarea>
			</p>
    {/section}  
     <!-- <p>
				<label for="textfield"><strong>Question [ {$vLanguage[i]} ]:<em>*</em></strong></label>
				<input type="text" id="{$vLanCode[i]}" name="Data[{$vLanCode[i]}]" class="inputbox" value="{$db_faq[0].{$vLanCode[i]}}" lang="*" title="Question {$vLanguage[i]}"/>
        
			</p> 
     <p>
				<label for="textfield"><strong>Question [Russian] :</strong></label>
				<input type="text" id="vTitle_RS" name="Data[vTitle_RS]" class="inputbox" value="{$db_faq[0].vTitle_RS}" title="Question {$vLanguage[i]}"/>
			</p>
        
			<p>
				<label for="textfield"><strong>Question [English] :<em>*</em></strong></label>
				<input type="text" id="vTitle_EN" name="Data[vTitle_EN]" class="inputbox" value="{$db_faq[0].vTitle_EN}" lang="*" title="Question English"/>
			</p>
        			
      <p>
					<label for="textfield"><strong>Answer [English] :<em>*</em></strong></label>
					<textarea class="text-input textarea" id="tAnswer_EN" name="Data[tAnswer_EN]" rows="7" cols="75" lang="*" title="Answer English">{$db_faq[0].tAnswer_EN}</textarea>
			</p>
			<p>
				<label for="textfield"><strong>Question [Russian] :</strong></label>
				<input type="text" id="vTitle_RS" name="Data[vTitle_RS]" class="inputbox" value="{$db_faq[0].vTitle_RS}" title="Question Russian"/>
			</p>
      <p>
					<label for="textfield"><strong>Answer [Russian] :</strong></label>
					<textarea class="text-input textarea" id="tAnswer_RS" name="Data[tAnswer_RS]" rows="7" cols="75" title="Answer Russian">{$db_faq[0].tAnswer_RS}</textarea>
			</p>
      <p>
				<label for="textfield"><strong>Question [Latvian] :</strong></label>
				<input type="text" id="vTitle_LV" name="Data[vTitle_LV]" class="inputbox" value="{$db_faq[0].vTitle_LV}" title="Question Latvian"/>
			</p>
      <p>
					<label for="textfield"><strong>Answer [Latvian] :</strong></label>
					<textarea class="text-input textarea" id="tAnswer_LV" name="Data[tAnswer_LV]" rows="7" cols="75" title="Answer Latvian">{$db_faq[0].tAnswer_LV}</textarea>
			</p>
      <p>
				<label for="textfield"><strong>Question [Lithuanian] :</strong></label>
				<input type="text" id="vTitle_LT" name="Data[vTitle_LT]" class="inputbox" value="{$db_faq[0].vTitle_LT}" title="Question Lithuanian"/>
			</p>
      <p>
					<label for="textfield"><strong>Answer [Lithuanian] :</strong></label>
					<textarea class="text-input textarea" id="tAnswer_LT" name="Data[tAnswer_LT]" rows="7" cols="75" title="Answer Lithuanian">{$db_faq[0].tAnswer_LT}</textarea>
			</p>
      
      <p>
				<label for="textfield"><strong>Question [Estonian] :</strong></label>
				<input type="text" id="vTitle_EE" name="Data[vTitle_EE]" class="inputbox" value="{$db_faq[0].vTitle_EE}" title="Question Estonian"/>
			</p>
      <p>
					<label for="textfield"><strong>Answer [Estonian] :</strong></label>
					<textarea class="text-input textarea" id="tAnswer_EE" name="Data[tAnswer_EE]" rows="7" cols="75" title="Answer Estonian">{$db_faq[0].tAnswer_EE}</textarea>
			</p>
      <p>
				<label for="textfield"><strong>Question [Norwegian] :</strong></label>
				<input type="text" id="vTitle_NO" name="Data[vTitle_NO]" class="inputbox" value="{$db_faq[0].vTitle_NO}" title="Question Norwegian"/>
			</p>
        			
      <p>
					<label for="textfield"><strong>Answer [Norwegian] :</strong></label>
					<textarea class="text-input textarea" id="tAnswer_NO" name="Data[tAnswer_NO]" rows="7" cols="75" title="Answer Norwegian">{$db_faq[0].tAnswer_NO}</textarea>
			</p>
			<p>
				<label for="textfield"><strong>Question [Polish] :</strong></label>
				<input type="text" id="vTitle_PO" name="Data[vTitle_PO]" class="inputbox" value="{$db_faq[0].vTitle_PO}" title="Question Polish"/>
			</p>
        			
      <p>
					<label for="textfield"><strong>Answer [Polish] :</strong></label>
					<textarea class="text-input textarea" id="tAnswer_PO" name="Data[tAnswer_PO]" rows="7" cols="75" title="Answer Polish">{$db_faq[0].tAnswer_PO}</textarea>
			</p>
      <p>
				<label for="textfield"><strong>Question [Finnish] :</strong></label>
				<input type="text" id="vTitle_FI" name="Data[vTitle_FI]" class="inputbox" value="{$db_faq[0].vTitle_FI}" title="Question Finnish"/>
			</p>
        			
      <p>
					<label for="textfield"><strong>Answer [Finnish] :</strong></label>
					<textarea class="text-input textarea" id="tAnswer_FI" name="Data[tAnswer_FI]" rows="7" cols="75" title="Answer Finnish">{$db_faq[0].tAnswer_FI}</textarea>
			</p>
			<p>
				<label for="textfield"><strong>Question [Swedish] :</strong></label>
				<input type="text" id="vTitle_SW" name="Data[vTitle_SW]" class="inputbox" value="{$db_faq[0].vTitle_SW}" title="Question Swedish"/>
			</p>
        			
      <p>
					<label for="textfield"><strong>Answer [Swedish] :</strong></label>
					<textarea class="text-input textarea" id="tAnswer_SW" name="Data[tAnswer_SW]" rows="7" cols="75" title="Answer Swedish">{$db_faq[0].tAnswer_SW}</textarea>
			</p>
			<p>
				<label for="textfield"><strong>Question [Danish] :</strong></label>
				<input type="text" id="vTitle_DN" name="Data[vTitle_DN]" class="inputbox" value="{$db_faq[0].vTitle_DN}" title="Question Danish"/>
			</p>
        			
      <p>
					<label for="textfield"><strong>Answer [Danish] :</strong></label>
					<textarea class="text-input textarea" id="tAnswer_DN" name="Data[tAnswer_DN]" rows="7" cols="75" title="Answer Danish">{$db_faq[0].tAnswer_DN}</textarea>
			</p>
      
      <p>
				<label for="textfield"><strong>Question [German] :</strong></label>
				<input type="text" id="vTitle_DE" name="Data[vTitle_DE]" class="inputbox" value="{$db_faq[0].vTitle_DE}" title="Question German"/>
			</p>
        			
      <p>
					<label for="textfield"><strong>Answer [German] :</strong></label>
					<textarea class="text-input textarea" id="tAnswer_DE" name="Data[tAnswer_DE]" rows="7" cols="75" title="Answer German">{$db_faq[0].tAnswer_DE}</textarea>
			</p>
      
      <p>
				<label for="textfield"><strong>Question [French] :</strong></label>
				<input type="text" id="vTitle_FN" name="Data[vTitle_FN]" class="inputbox" value="{$db_faq[0].vTitle_FN}" title="Question French"/>
			</p>
        			
      <p>
					<label for="textfield"><strong>Answer [French] :</strong></label>
					<textarea class="text-input textarea" id="tAnswer_FN" name="Data[tAnswer_FN]" rows="7" cols="75" title="Answer Finnish">{$db_faq[0].tAnswer_FN}</textarea>
			</p>
      <p>
				<label for="textfield"><strong>Question [Spanish] :</strong></label>
				<input type="text" id="vTitle_ES" name="Data[vTitle_ES]" class="inputbox" value="{$db_faq[0].vTitle_ES}" title="Question Spanish"/>
			</p>
        			
      <p>
					<label for="textfield"><strong>Answer [Spanish] :</strong></label>
					<textarea class="text-input textarea" id="tAnswer_ES" name="Data[tAnswer_ES]" rows="7" cols="75" title="Answer Spanish">{$db_faq[0].tAnswer_ES}</textarea>
			</p> -->
      
      <p>        			
					<label for="textfield"><strong>Display Order :</strong></label>
					<select id="iDisplayOrder" name="Data[iDisplayOrder]" lang="" title="Category Display Order" />
					{section name=i loop=$num_rows}
					 <option {if $db_faq[0].iDisplayOrder eq $smarty.section.i.index_next} selected {/if} value="{$smarty.section.i.index_next}">{$smarty.section.i.index_next}</option>
					{/section}
					</select>
				</p>
          
			<p>
				<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]">
					<option value="Active" {if $db_faq[0].eStatus eq Active}selected{/if} >Active</option>
					<option value="Inactive" {if $db_faq[0].eStatus eq Inactive}selected{/if} >Inactive</option>
				</select>
			</p>
			{if $mode eq 'edit'}
			<input type="submit" value="Save Faq" class="btn" title="Save Faq" onclick="return validate(document.frmadd);"/>
			{else}
			<input type="submit" value="Add Faq" class="btn" title="Add Faq" onclick="return validate(document.frmadd);"/>
			{/if}
			<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
		</form>
</div>
</div>
{literal}
<script>
function redirectcancel(){
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=u-faq&mode=view";
    return false;
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
{/literal}
