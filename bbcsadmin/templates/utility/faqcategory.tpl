<div class="contentcontainer">
<div class="headings">
	<h2 class="left">Add Faq Category
    </h2>
</div>
<div class="contentbox" id="tabs-1">
    {if $var_msg neq ''}
     <div class="status error" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Error" />
              {$var_msg}</p> 
     </div>     
    <div></div>
    {/if} 
		<form id="frmadd" name="frmadd" action="index.php?file=u-faqcategory_a" method="post">
        <input type="hidden" name="iFaqcategoryId" id="iFaqcategoryId" value="{$iFaqcategoryId}" />
        <input type="hidden" name="action" id="action" value="{$mode}" />
       {section name=i loop=$db_languagemas}       
			{assign var="title" value="vTitle_{$db_languagemas[i].vCode}"}             
			<p>
				<label for="textfield"><strong>Title [ {$db_languagemas[i].vTitle} ]:<em>*</em></strong></label>
				<input type="text" id="{$title}" name="Data[{$title}]" class="inputbox" value="{$db_faq_cat[0].$title}" lang="*" title="Category {$db_languagemas[i].vTitle}"/>
        
			</p>
    {/section}  
		<!--	<p>
				<label for="textfield"><strong>Title [English] :</strong></label>
				<input type="text" id="vTitle_EN" name="Data[vTitle_EN]" class="inputbox" value="{$db_faq_cat[0].vTitle_EN}" lang="*" title="Title English"/>
			</p>
      <p>
				<label for="textfield"><strong>Title [Russian] :</strong></label>
				<input type="text" id="vTitle_RS" name="Data[vTitle_RS]" class="inputbox" value="{$db_faq_cat[0].vTitle_RS}" title="Title Russian"/>
			</p>
			<p>
				<label for="textfield"><strong>Title [Latvian] :</strong></label>
				<input type="text" id="vTitle_LV" name="Data[vTitle_LV]" class="inputbox" value="{$db_faq_cat[0].vTitle_LV}" title="Title Latvian"/>
			</p>
			<p>
				<label for="textfield"><strong>Title [Lithuanian] :</strong></label>
				<input type="text" id="vTitle_LT" name="Data[vTitle_LT]" class="inputbox" value="{$db_faq_cat[0].vTitle_LT}" title="Title Lithuanian"/>
			</p>
			<p>
				<label for="textfield"><strong>Title [Estonian] :</strong></label>
				<input type="text" id="vTitle_EE" name="Data[vTitle_EE]" class="inputbox" value="{$db_faq_cat[0].vTitle_EE}" title="Title Estonian"/>
			</p>
      <p>
				<label for="textfield"><strong>Title [German] :</strong></label>
				<input type="text" id="vTitle_DE" name="Data[vTitle_DE]" class="inputbox" value="{$db_faq_cat[0].vTitle_DE}" title="Title German"/>
			</p>
      <p>
				<label for="textfield"><strong>Title [French] :</strong></label>
				<input type="text" id="vTitle_FN" name="Data[vTitle_FN]" class="inputbox" value="{$db_faq_cat[0].vTitle_FN}" title="Title French"/>
			</p>
      
      <p>
				<label for="textfield"><strong>Title [Spanish] :</strong></label>
				<input type="text" id="vTitle_ES" name="Data[vTitle_ES]" class="inputbox" value="{$db_faq_cat[0].vTitle_ES}" title="Title Spanish"/>
			</p>
      <p>
				<label for="textfield"><strong>Title [Norwegian] :</strong></label>
				<input type="text" id="vTitle_NO" name="Data[vTitle_NO]" class="inputbox" value="{$db_faq_cat[0].vTitle_NO}" title="Title Norwagina"/>
			</p>
      <p>
				<label for="textfield"><strong>Title [Polish] :</strong></label>
				<input type="text" id="vTitle_PO" name="Data[vTitle_PO]" class="inputbox" value="{$db_faq_cat[0].vTitle_PO}" title="Title Polish"/>
			</p>
      
      <p>
				<label for="textfield"><strong>Title [Finnish] :</strong></label>
				<input type="text" id="vTitle_FI" name="Data[vTitle_FI]" class="inputbox" value="{$db_faq_cat[0].vTitle_FI}" title="Title Polish"/>
			</p>
      <p>
				<label for="textfield"><strong>Title [Swedish] :</strong></label>
				<input type="text" id="vTitle_SW" name="Data[vTitle_SW]" class="inputbox" value="{$db_faq_cat[0].vTitle_SW}" title="Title Swedish"/>
			</p>
      <p>
				<label for="textfield"><strong>Title [Danish] :</strong></label>
				<input type="text" id="vTitle_DN" name="Data[vTitle_DN]" class="inputbox" value="{$db_faq_cat[0].vTitle_DN}"  title="Title Danish"/>
			</p>-->
      
      <p>        			
					<label for="textfield"><strong>Display Order :</strong></label>
					<select id="iDisplayOrder" name="Data[iDisplayOrder]" lang="" title="Category Display Order" />
					{section name=i loop=$num_rows}
					 <option {if $db_faq_cat[0].iDisplayOrder eq $smarty.section.i.index_next} selected {/if} value="{$smarty.section.i.index_next}">{$smarty.section.i.index_next}</option>
					{/section}
					</select>
				</p>			
			<p>
				<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]">
					<option value="Active" {if $db_faq_cat[0].eStatus eq Active}selected{/if} >Active</option>
					<option value="Inactive" {if $db_faq_cat[0].eStatus eq Inactive}selected{/if} >Inactive</option>
				</select>
			</p>
		{if $mode eq 'edit'}
      <input type="submit" value="Edit Faq Category" class="btn" title="Edit Faq Category" onclick="return validate(document.frmadd);"/>
    {else}	
      <input type="submit" value="Add Faq Category" class="btn" title="Add Faq Category" onclick="return validate(document.frmadd);"/>
		{/if}	
      <input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
		</form>
</div>
</div>
{literal}
<script>
function redirectcancel(){
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=u-faqcategory&mode=view";
    return false;
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
{/literal}
