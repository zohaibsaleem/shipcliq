<div class="contentcontainer" id="tabs">
	<div class="headings">
		{if $mode eq "edit"}
			<h2 class="left">Edit Make</h2>
		{else}
			<h2 class="left">Add Make</h2>
		{/if}
	</div>
	<div class="contentbox" id="tabs-1">
     {if $var_msg neq ''}
     <div class="status error" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Error" />
              {$var_msg}</p> 
     </div>     
    <div></div>
    {/if}       
			<form id="frmadd" name="frmadd" action="index.php?file=u-make_a" method="post">
            <input type="hidden" name="iMakeId" id="iMakeId" value="{$iMakeId}" />
            <input type="hidden" name="action" id="action" value="{$mode}" />
		
					<label for="textfield"><strong>Make :</strong></label>
					<input type="text" id="vMake" name="Data[vMake]" class="inputbox" value="{$db_make[0].vMake}" lang="*" title="Make"/>
				</p>
			
				<p>
					<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $db_make[0].eStatus eq Active}selected{/if}>Active</option>
						<option value="Inactive" {if $db_make[0].eStatus eq Inactive}selected{/if}>Inactive</option>
					</select>
				</p>
				{if $mode eq 'edit'}
				<input type="submit" value="Edit Make" class="btn" onclick="return validate(document.frmadd);" title="Edit Make"/>
				      {else}
   				<input type="submit" value="Add Make" class="btn" onclick="return validate(document.frmadd);" title="Add Make"/>
  				
				      
				      {/if}
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
			</form>
	</div>
</div>
{literal}
<script>
function redirectcancel()
{
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=u-make&mode=view";
    return false;
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
{/literal}