<div class="contentcontainer">
<div class="headings">
		{if $mode eq 'edit'}
	<h2>Edit Global Partners</h2>
    {else}
     <h2>Add Global Partners</h2>
    {/if}
</div>
<div class="contentbox" id="tabs-1">
		<form id="frmadd" name="frmadd" action="index.php?file=u-global_partners_a" method="post" enctype="multipart/form-data">
        <input type="hidden" name="iGlobalPartnerId" id="iGlobalPartnerId" value="{$iGlobalPartnerId}" />
        <input type="hidden" name="action" id="action" value="{$mode}" />
		<input type="hidden" name="imagemode" id="imagemode" />
			<p>
				<label for="textfield"><strong>Title :</strong></label>
				<input type="text" id="vTitle" name="Data[vTitle]" class="inputbox" value="{$db_commision[0].vTitle}" lang="*" title="Logo Title"/>
			</p>
			<p>
				<label for="textfield"><strong>Upload Image :</strong></label>
				<input type="file" id="vImage" name="Data[vImage]" class="inputbox" title="Images" />
			<br />[Note: Supported File Types are *.jpg,*.jpeg,*.png,*.gif]				
			</p>
			{if $mode eq 'edit'}					
			{if $img eq 'yes'}	
			<p>
				<table>
					<tr>
						<td width="25%"><img src= "{$tconfig.tsite_upload_global_partners}/2_{$db_commision[0].vImage}" /></td>
					</tr>
					<tr>
						<td>
							<input type="button" value="Delete Image" class="btn" onclick="return confirm_delete();" title="Delete Image"/>        
						</td>
					</tr>
				</table>
			</p>
			{/if}
			{else}
			{/if}			
			<p>
				<label for="textfield"><strong>URL :</strong></label>
				<input type="text" id="vURL" name="Data[vURL]" class="inputbox" value="{$db_commision[0].vURL}" lang="*" title="URL"/>
			</p>
			<p>
				<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]">
					<option value="Active" {if $db_commision[0].eStatus eq Active}selected{/if} >Active</option>
					<option value="Inactive" {if $db_commision[0].eStatus eq Inactive}selected{/if} >Inactive</option>
				</select>
			</p>
			<input type="submit" value="Save" class="btn" title="Save" onclick="return validate(document.frmadd);"/>
			<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
		</form>
</div>
</div>
{literal}
<script>
function confirm_delete()
{
   ans = confirm('Are you sure Delete Image?');
   if(ans == true){
    document.frmadd.imagemode.value = 'delete_image';
    document.frmadd.submit();
   }else{
    return false;
   }
}
function redirectcancel(){
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=u-global_partners&mode=view";
    return false;
}
</script>
{/literal}