<div class="contentcontainer">
<div class="headings">
		{if $mode eq 'edit'}
	<h2>Edit Media Partners</h2>
    {else}
     <h2>Add Media Partners</h2>
    {/if}
</div>
<div class="contentbox" id="tabs-1">
    {if $var_msg neq ''}
     <div class="status error" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Error" />
              {$var_msg}</p> 
     </div>     
    <div></div>
    {/if}
		<form id="frmadd" name="frmadd" action="index.php?file=u-media_partners_a" method="post" enctype="multipart/form-data">
        <input type="hidden" name="iMediaPartnerId" id="iMediaPartnerId" value="{$iMediaPartnerId}" />
        <input type="hidden" name="action" id="action" value="{$mode}" />
		<input type="hidden" name="imagemode" id="imagemode" />
			<p>
				<label for="textfield"><strong>Title :</strong></label>
				<input type="text" id="vTitle" name="Data[vTitle]" class="inputbox" value="{$db_commision[0].vTitle}" lang="*" title="Logo Title"/>
			</p>
			<p>
				<label for="textfield"><strong>Upload Image :</strong></label>
				<input type="file" id="vImage" name="Data[vImage]" class="inputbox" title="Images" />
			<br />[Note: Supported File Types are *.jpg,*.jpeg,*.png,*.gif]				
			</p>
			{if $mode eq 'edit'}					
			{if $img eq 'yes'}	
			<p>
				<table>
					<tr>
						<td width="25%"><img src= "{$tconfig.tsite_upload_images_media_partners}/2_{$db_commision[0].vImage}" /></td>
					</tr>
					<tr>
						<td>
							<input type="button" value="Delete Image" class="btn" onclick="return confirm_delete();" title="Delete Image"/>        
						</td>
					</tr>
				</table>
			</p>
			{/if}
			{else}
			{/if}	
     <p>
				<label for="textfield"><strong>URL :</strong></label>
				<input type="text" id="vURL" name="Data[vURL]" class="inputbox" value="{$db_commision[0].vURL}"/>
				<br />[Example: http://www.sitename.com]
			</p>		
			<p>
				<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]">
					<option value="Active" {if $db_commision[0].eStatus eq Active}selected{/if} >Active</option>
					<option value="Inactive" {if $db_commision[0].eStatus eq Inactive}selected{/if} >Inactive</option>
				</select>
			</p>
			<input type="submit" value="Save" class="btn" title="Save" onclick="return validate(document.frmadd);"/>
			<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
		</form>
</div>
</div>
{literal}
<script>
function confirm_delete()
{
   ans = confirm('Are you sure Delete Image?');
   if(ans == true){
    document.frmadd.imagemode.value = 'delete_image';
    document.frmadd.submit();
   }else{
    return false;
   }
}
function redirectcancel(){
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=u-media_partners&mode=view";
    return false;
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
{/literal}