<div class="contentcontainer" id="tabs">
	<div class="headings">
		{if $mode eq "edit"}
			<h2 class="left">Edit Model</h2>
		{else}
			<h2 class="left">Add Model</h2>
		{/if}
	</div>
	<div class="contentbox" id="tabs-1">
     {if $var_msg neq ''}
     <div class="status error" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Error" />
              {$var_msg}</p> 
     </div>     
    <div></div>
    {/if}      
            
			<form id="frmadd" name="frmadd" action="index.php?file=u-model_a" method="post">
            <input type="hidden" name="iModelId" id="iModelId" value="{$iModelId}" />
            <input type="hidden" name="action" id="action" value="{$mode}" />
        {*	{$smarty.server.SERVER_NAME} *}     
         <p>                            
            <label for="textfield"><strong>Make:</strong></label>
            <select name="Data[iMakeId]" id="iMakeId" lang="*" Title="Make">
		           <option value=""> --- Select Make --- </option>
               {section name=i loop=$db_make}
				{if $db_make[i].iMakeId eq $db_model[0].iMakeId}
               
               <option value = "{$db_make[i].iMakeId}" SELECTED >{$db_make[i].vMake}</option>
               {else}
               <option value = "{$db_make[i].iMakeId}">{$db_make[i].vMake}</option>
               {/if}
               {/section} 
             </select>    
         </p>
        <p>
					<label for="textfield"><strong>Model :</strong></label>
					<input type="text" id="vTitle" name="Data[vTitle]" class="inputbox" value="{$db_model[0].vTitle}" lang="*" title="Model"/>
				</p>
			
				<p>
					<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $db_model[0].eStatus eq Active}selected{/if}>Active</option>
						<option value="Inactive" {if $db_model[0].eStatus eq Inactive}selected{/if}>Inactive</option>
					</select>
				</p>
				{if $mode eq 'edit'}
				<input type="submit" value="Edit Model" class="btn" onclick="return validate(document.frmadd);" title="Edit Model"/>
				      {else}
   				<input type="submit" value="Add Model" class="btn" onclick="return validate(document.frmadd);" title="Add Model"/>
  				
				      
				      {/if}
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
			</form>
	</div>
</div>
{literal}
<script>
function redirectcancel()
{
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=u-model&mode=view";
    return false;
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
{/literal}