<div class="contentcontainer" id="tabs">
	<div class="headings">
		{if $mode eq 'edit'}
			<h2 class="left">Edit Events</h2>
        {else}
			<h2 class="left">Add Events</h2>
        {/if}
	</div>
	<div class="contentbox" id="tabs-1">
            
			<form id="frmadd" name="frmadd" action="index.php?file=u-news_a" method="post" enctype="multipart/form-data">
            <input type="hidden" name="iNewsId" id="iNewsId" value="{$iNewsId}" />
            <input type="hidden" name="action" id="action" value="{$mode}" />
            <input type="hidden" name="imagemode" id="imagemode" />
				<p>
					<label for="textfield"><strong>Title :</strong></label>
					<input type="text" id="vTitle" name="Data[vTitle]" class="inputbox" value="{$db_news[0].vTitle}" lang="*" title="Title" />
				</p>				 				
				<p>
					<label for="textfield"><strong>Description :</strong></label>
					<!--textarea id="vTitle_FR" name="Data[tDescription]" class="inputbox" lang="*" title="Title"/>{$tDescription_EN}</textarea-->
          {fckeditor BasePath="../plugins/FCKeditor/" InstanceName="Data[tDescription]" Width="850px" Height="550px" Value="$tDescription_EN"}         
				</p>							
				<p>        			
					<label for="textfield"><strong>News Display Order :</strong></label>
					<select id="iDisplayOrder" name="Data[iDisplayOrder]" lang="" title="News Display Order" />
					<option value="">Please Select News Display Order</option>
					{section name=i loop=$num_rows}
					 <option {if $db_news[0].iDisplayOrder eq $smarty.section.i.index_next}selected {/if} value="{$smarty.section.i.index_next}">{$smarty.section.i.index_next}</option>
					{/section}
					</select>
				</p>
			<!--	<p>
					<label for="textfield"><strong>Upload Image :</strong></label>
					<input type="file" id="vImage" name="Data[vImage]" class="inputbox" title="News" />					
				</p>
				 {if $mode eq 'edit'}					
			     {if $checkimg eq 'yes'}	
				<p>
			<table>
				  <tr>
				  <td width="25%"><img src= "{$tconfig.tsite_upload_images_promotion}2_{$db_news[0].vImage}" /></a></td>
                  </tr>
                  <tr>
                    <td>
                    <input type="button" value="Delete Image" class="btn" onclick="return confirm_delete();" title="Delete Image"/>         
         
                    </td>
                  </tr>
          </table>
				</p>
				{/if}
                {else}
                {/if}
				<p>
					<label for="textfield"><strong>Date :</strong></label>
					<input type="text" id="dDate" name="Data[dDate]" style="width:105px;" class="inputbox" value="{$db_news[0].dDate}" lang="*" title="Date"/>
				</p>-->
        <p>        
					<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $db_news[0].eStatus eq Active}selected {/if}>Active</option>
						<option value="Inactive" {if $db_news[0].eStatus eq Inactive}selected {/if}>Inactive</option>
					</select>
				</p>				
				{if $mode eq 'edit'}
				<input type="submit" value="Save Events" class="btn" onclick="return validate(document.frmadd);" title="Save Events"/>
				      {else}
   				<input type="submit" value="Add Events" class="btn" onclick="return validate(document.frmadd);" title="Add Events"/>
  				
				      
				      {/if}
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
			</form>
	</div>
</div>
{literal}
<script>
function redirectcancel()
{
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=u-news&mode=view";
    return false;
}
function confirm_delete()
{
   ans = confirm('Are you sure Delete Image?');
   if(ans == true){
    document.frmadd.imagemode.value = 'delete_image';
    document.frmadd.submit();
   }else{
    return false;
   }
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
{/literal}