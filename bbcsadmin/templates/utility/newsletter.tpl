<div class="contentcontainer" id="tabs">
	<div class="headings">
		{if $mode eq 'edit'}
			<h2 class="left">Edit Newsletter</h2>
		{else}
			<h2 class="left">Add Newsletter</h2>
		{/if}
	</div>
	<div class="contentbox" id="tabs-1">
            
			<form id="frmadd" name="frmadd" action="index.php?file=u-newsletter_a" method="post" enctype="multipart/form-data">
            <input type="hidden" name="iNewsTId" id="iNewsTId" value="{$iNewsTId}" />
            <input type="hidden" name="action" id="action" value="{$mode}" />

				<p>
					<label for="textfield"><strong>Title:</strong></label>
					<input type="text" id="vTitle" name="Data[vTitle]" class="inputbox" value="{$db_newsletter[0].vTitle}" lang="*" title="Title"/>
				</p>		
				<p>
					<label for="textfield"><strong>From Name:</strong></label>
					<input type="text" id="vFromName" name="Data[vFromName]" class="inputbox" {if $db_newsletter[0].vFromName EQ ""} value="{$EMAIL_FROM_NAME}"{else}value="{$db_newsletter[0].vFromName}"{/if} lang="*" title="From Name"/>
				</p>
				<p>
					<label for="textfield"><strong>From Email:</strong></label>
					<input type="text" id="vFromEmail" name="Data[vFromEmail]" class="inputbox" {if $db_newsletter[0].vFromEmail EQ ""} value="{$ADMIN_EMAIL}"{else}value="{$db_newsletter[0].vFromEmail}"{/if} lang="*" title="From Email"/>
				</p>
				<p>
					<label for="textfield"><strong>Content:</strong></label>
					{fckeditor BasePath="../plugins/FCKeditor/" InstanceName="Data[tContent]" Width="850px" Height="550px" Value="$tContent"}
				</p>
				<p>
					<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $db_newsletter[0].eStatus eq Active}selected{/if}>Active</option>
						<option value="Inactive" {if $db_newsletter[0].eStatus eq Inactive}selected{/if}>Inactive</option>
					</select>
				</p>
				
				{if $mode eq 'edit'}
				<input type="submit" value="Save Newsletter" class="btn" onclick="return validate(document.frmadd);" title="Save Newsletter"/>
				      {else}
   				<input type="submit" value="Add Newsletter" class="btn" onclick="return validate(document.frmadd);" title="Add Newsletter"/>
  				
				      
				      {/if}
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
			</form>
	</div>
</div>
{literal}
<script>
function redirectcancel()
{
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=u-newsletter&mode=view";
    return false;
}
</script>
{/literal}