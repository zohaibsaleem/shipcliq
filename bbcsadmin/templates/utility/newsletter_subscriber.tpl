<div class="contentcontainer" id="tabs">
	<div class="headings">
    	{if $mode eq 'edit'}
			<h2 class="left">Edit Degree</h2>
        {else}
			<h2 class="left">Add Degree</h2>
        {/if}
	</div>
	<div class="contentbox" id="tabs-1">           
		<form id="frmadd" name="frmadd" action="index.php?file=u-degree_a" method="post">
			<input type="hidden" name="iDegreeId" id="iDegreeId" value="{$iDegreeId}" />
            <input type="hidden" name="action" id="action" value="{$mode}" />
			<p>
				<label for="textfield"><strong>Degree:</strong></label>
				<input type="text" name="Data[vDegree]" id="vDegree" value="{$db_degree[0].vDegree}" class="inputbox" />
			</p>
			<p>
				<label for="textfield"><strong>Status :</strong></label>
				<select id="eStatus" name="Data[eStatus]">
					<option value="Active" {if $db_degree[0].eStatus eq Active}selected{/if}>Active</option>
					<option value="Inactive" {if $db_degree[0].eStatus eq Inactive}selected{/if}>Inactive</option>
				</select>
			</p>
			{if $mode eq 'edit'}
			<input type="submit" value="Edit Degree" class="btn" onclick="return validate(document.frmadd);" title="Edit Degree"/>
			      {else}
   			<input type="submit" value="Add Degree" class="btn" onclick="return validate(document.frmadd);" title="Add Degree"/>
			{/if}
			<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
		</form>
	</div>
</div>
{literal}
<script>
function redirectcancel()
{
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=u-testimonial&mode=view";
    return false;
}
</script>
{/literal}