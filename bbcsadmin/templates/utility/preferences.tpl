<div class="contentcontainer" id="tabs">
	<div class="headings">
		{if $mode eq "edit"}
			<h2 class="left">Edit Share Preferences</h2>
		{else}
			<h2 class="left">Add Share Preferences</h2>
		{/if}
	</div>
	<div class="contentbox" id="tabs-1">
     {if $var_msg neq ''}
     <div class="status error" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_error.png" title="Error" />
              {$var_msg}</p> 
     </div>     
    <div></div>
    {/if} 
    {if $var_msg_new neq ''}
     <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg_new}</p> 
     </div>       
    <div></div>
    {/if}       
			<form id="frmadd" name="frmadd" action="index.php?file=u-preferences_a" method="post" enctype="multipart/form-data">
            <input type="hidden" name="iPreferencesId" id="iPreferencesId" value="{$iPreferencesId}" />
            <input type="hidden" name="action" id="action" value="{$mode}" />
		       <input type="hidden" name="imagemode" id="imagemode" value=""/>
             <p>
					<label for="textfield"><strong>Preference :</strong></label>
					<input type="text" id="vPreferences" name="Data[vPreferences]" class="inputbox" value="{$db_preferences[0].vPreferences}" lang="*" title="preferences"/>
				</p>
				  <p>
					<label for="textfield"><strong>Title:</strong></label>
					<input type="text" id="vTitle" name="Data[vTitle]" class="inputbox" value="{$db_preferences[0].vTitle}" lang="*" title="Title"/>
				</p>
				
				<p>
					<label for="textfield"><strong>Load Icon 1 :</strong></label>
					<input type="file" id="vYes" name="Data[vYes]" class="inputbox" title="Yes" title="Upload Image Yes" onchange="filename();"/>
				   	<br />[Note: Supported File Types are *.jpg,*.png,*.gif]
            </p>
			{if $mode eq 'edit'}					
				{if $img_YES eq 'yes'}	
				<p>                                
				  <table>
				    <tr>
				        <td width="25%"><img src= "{$tconfig.tsite_upload_images_preferences}/{$db_preferences[0].vYes}"  height="42" width="42"/></td>
            </tr>
             <tr><td><input type="button" value="Delete Image" class="btn" onclick="return confirm_delete('vYES');" title="Delete Image"/>         
                   
        </td></tr>
          </table>
				</p>
				{/if}
           {else}
            {/if}  
            	
            
				<p>	
               <label for="textfield"><strong>Load Icon 2 :</strong></label>
					<input type="file" id="vMAYBE" name="Data[vMAYBE]" class="inputbox" title="MAYBE" title="Upload Image MAYBE" onchange="filename();"/>
				          	<br />[Note: Supported File Types are *.jpg,*.png,*.gif]
            </p>
				 {if $mode eq 'edit'}					
				{if $img_MAYBE eq 'yes'}	
				<p>                                
				  <table>
				    <tr>
				        <td width="25%"><img src= "{$tconfig.tsite_upload_images_preferences}/{$db_preferences[0].vMAYBE}"  height="42" width="42"/></td>
            </tr>
             <tr><td><input type="button" value="Delete Image" class="btn" onclick="return confirm_delete('vMAYBE');" title="Delete Image MayBe"/>         
           </td></tr>
             </table>
				</p>
				{/if}
        {else}
        {/if}
      
        	<p>
					<label for="textfield"><strong>Load Icon 3 :</strong></label>
					<input type="file" id="vNO" name="Data[vNO]" class="inputbox" title="vNO" title="Upload Image NO" onchange="filename();"/>
				         	<br />[Note: Supported File Types are *.jpg,*.png,*.gif]
            </p>
				 {if $mode eq 'edit'}					
				{if $img_NO eq 'yes'}	
				<p>                                
				  <table>
				    <tr>
				        <td width="25%"><img src= "{$tconfig.tsite_upload_images_preferences}/{$db_preferences[0].vNO}"  height="42" width="42"/></td>
            </tr>
             <tr><td><input type="button" value="Delete Image" class="btn" onclick="return confirm_delete('vNO');" title="Delete Image"/>         
           </td></tr>
             </table>
				</p>
				{/if}
        {else}
        {/if}  
			   
				<p>
					<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $db_preferences[0].eStatus eq Active}selected{/if}>Active</option>
						<option value="Inactive" {if $db_preferences[0].eStatus eq Inactive}selected{/if}>Inactive</option>
					</select>
				</p>
				{if $mode eq 'edit'}
				<input type="submit" value="Edit Preferences" class="btn" onclick="return validate(document.frmadd);" title="Edit Preferences"/>
				      {else}
   				<input type="submit" value="Add Preferences" class="btn" onclick="return validate(document.frmadd);" title="Add preferences"/>
  				
				      
				      {/if}
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
			</form>
	</div>
</div>
{literal}
<script>
function redirectcancel()
{
    window.location="{/literal}{$tconfig.tpanel_url}{literal}/index.php?file=u-preferences&mode=view";
    return false;
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}

function filename(){

   var vYes = document.getElementById("vYes").value;
   var vMAYBE = document.getElementById("vMAYBE").value;
   var vNO = document.getElementById("vNO").value;
    if(vYes != '')
    {
        var vYes1=vYes.split(".");
        if(vYes1[1] != 'jpg' && vYes1[1] != 'gif' && vYes1[1] != 'png' )
        {
          alert("111Only .jpg,.png,.gif Files Are Supported..");
          document.getElementById("vYes").value = "";
          return false;
        } 
    }
    if(vMAYBE != '')
    {
        var vMAYBE1=vMAYBE.split(".");
        if(vMAYBE1[1] != 'jpg' && vMAYBE1[1] != 'gif' && vMAYBE1[1] != 'png' )
        {
          alert("Only .jpg,.png,.gif Files Are Supported..");
           document.getElementById("vMAYBE").value = "";
          return false;
        } 
    }
    if(vNO != '')
    {
        var vNO1=vNO.split(".");
        if(vNO1[1] != 'jpg' && vNO1[1] != 'gif' && vNO1[1] != 'png' )
        {
          alert("Only .jpg,.png,.gif Files Are Supported..");
          document.getElementById("vNO").value = "";
          return false;
        } 
    }
}
function confirm_delete(val)
{
   ans = confirm('Are you sure Delete Image?');
   if(ans == true){
    document.frmadd.imagemode.value = 'delete_image-'+val;
    document.frmadd.submit();
   }else{
    return false;
   }
}
</script>
{/literal}