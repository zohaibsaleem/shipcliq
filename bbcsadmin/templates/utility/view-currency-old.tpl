<script language="JavaScript" src="{$tconfig.tcp_javascript}jlist.js"></script>
<div class="contentcontainer">
	<div class="headings altheading">
		<h2>Currency Rates</h2>
	</div>
	<div class="contentbox">
    {if $var_msg_new neq ''}
     <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg_new}</p> 
     </div>     
    <div></div>
    {elseif $var_msg neq ''}
    <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg}</p> 
     </div>     
    <div></div>
    {/if}
		
  <form name="frmlist" id="frmlist"  action="index.php?file=u-currency_a" method="post">           
		<table width="100%" border="0">
	
    <thead>
			<tr>
				<th width="10%">Currncy</th>
				<th width="90%">Ratio for EUR</th>
			</tr>
		</thead>
		<tbody>
      {if $db_curr|@count gt 0}
		  {section name=i loop=$db_curr}
      <tr>
        <td width="10%">{$db_curr[i].vName}</td>
        <td width="90%"><input type="text" name="Ratio[]" id="Ratio{$smarty.section.i.index}" class="inputbox" style="width:100px;height:12px;" value="{$db_curr[i].Ratio}" lang="{literal}*{N}{/literal}" title="{$db_curr[i].vName}"></td>
	    </tr>  	    
      {/section}
      <tr><td width="10%"></td><td width="90%"><input class="btn" type="submit" title="Edit Currency" onclick="return validate(document.frmlist);" value="Edit Currency"></td><tr>
      {else}
      <tr>
			<td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No Record Found.</td>
		  </tr>
        {/if}
		</tbody>
		</table>
    </form>         
		<div style="clear: both;"></div></div>
</div>
{literal}
<script>
function Searchoption(){
    document.getElementById('frmsearch').submit();
}
function AlphaSearch(val){
    var alphavalue = val;
    var file = 'pr-currency';
    window.location="index.php?file="+file+"&alp="+alphavalue+"&mode=view";
    return false;
}
function MakeAction(loopid,type){
    document.frmlist.iLanguageMasId.value = loopid;
    document.frmlist.action.value = type;
	document.frmlist.submit();	
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
{/literal}