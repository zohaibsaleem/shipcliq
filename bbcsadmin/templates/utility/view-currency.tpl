<script language="JavaScript" src="{$tconfig.tcp_javascript}jlist.js"></script>



<div class="contentcontainer">
	<div class="headings altheading">
		<h2>Currency Rates</h2>
	</div>
	<div class="contentbox">
    {if $var_msg_new neq ''}
     <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg_new}</p> 
     </div>     
    <div></div>
    {elseif $var_msg neq ''}
    <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg}</p> 
     </div>     
    <div></div>
    {/if}
		
  <form name="frmlist" id="frmlist"  action="index.php?file=u-currency_a" method="post">           
		<table width="100%" border="0">
	
    <thead>
			<tr>
				<th>Currency</th>
        <th>Front Status</th>
        <th>USD Ratio</th>
        <th>Symbol</th>
		<th>Display Order</th>
        {section name=i loop=$db_curr}
        <th>{$db_curr[i].vName}</th>
        {/section}
			</tr>
		</thead>
		<tbody>

      {if $db_curr|@count gt 0}
		 {foreach from=$db_curr key=k1 item=v1}
      <tr>
        <td >{$db_curr[$k1].vName}</td>
      <td >
	  {$db_curr[i].eFrontStatus}
	  <select name="eFrontStatus[]" id="eFrontStatus{$smarty.section.i.index}" class="inputbox" lang="*"   style="width:100px;"> 
	  <option value='Yes' {if $db_curr[$k1].eFrontStatus eq 'Yes'} selected{/if} >Yes</option>
	  <option value='No' {if $db_curr[$k1].eFrontStatus eq 'No'} selected{/if}>No</option>
	  </select>
	  </td>
		<td ><input type="text" name="Ratio[]" id="Ratio{$smarty.section.i.index}" class="inputbox" style="width:100px;"  value="{$db_curr[$k1].Ratio}" lang="{literal}*{N}{/literal}" title="Ratio"> </td>
        <td><input type="text" name="vSymbole[]"  class="inputbox"  value="{$db_curr[$k1].vSymbole}" lang="*" title="{$db_curr[$k1].vSymbole}" style="width:100px;"></td>
        <td><input type="text" name="iDispOrder[]" class="inputbox"  value="{$db_curr[$k1].iDispOrder}" lang="{literal}*{N}-{0}{/literal}" title="Dispaly Order" style="width:100px;" onChange="disporder(this,{$db_count},{$k1});" id="iDispOrder[{$k1}]"></td>
        {foreach from=$db_name key=k2 item=v2}
           <td>
      <!-- <input type="text" name="{$v2}[]" class="inputbox" value="{$db_curr[$k1].$v2}" lang="{literal}*{N}{/literal}" style="width:100px;" onkeyup="numericFilter(this)" {if $db_curr[$k2].vName eq $db_curr[$k1].vName} readonly {/if} onChange="validzero(this);" />       -->
	  {if $db_curr[$k2].vName eq $db_curr[$k1].vName} 
          <input type="hidden" name="{$v2}[]" id="{$k1}{$k2}" value="{$db_curr[$k1].$v2}" />
          {$db_curr[$k1].$v2}
         {/if}   
        {if $db_curr[$k2].vName neq $db_curr[$k1].vName}   
       <input type="text" name="{$v2}[]" id="{$k1}{$k2}" class="inputbox" value="{$db_curr[$k1].$v2}" lang="{literal}*{N}{/literal}" style="width:100px;" onkeyup="numericFilter(this)" onChange="validzero(this,{$k1}{$k2});" />
         {/if}
      </td>
         {/foreach}
      {/foreach}
	    </tr>  	    
   
      <tr><td></td><td ><input class="btn" type="submit" title="Edit Currency" onclick="return validate(document.frmlist);" value="Edit Currency"></td><tr>
      {else}
      <tr>
			<td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No Record Found.</td>
		  </tr>
      
        {/if}
      
      
		</tbody>
		</table>
    </form>         
		<div style="clear: both;"></div></div>
</div>
{literal}
<script>
function Searchoption(){
    document.getElementById('frmsearch').submit();
}
function numericFilter(txb) {
//alert(txb.value);
txb.value = txb.value.replace(/[^0-9.]/g,"");
// txb = parseFloat((txb + "").replace(/^(.*\.\d\d)\d*$/, '$1'));

   return txb > 0 ;
}
function disporder(num,len,r)
{
    var n1=num.value;
    var count=0;
   
    var arr=[];
     for(k=0;k<len;k++)
    {
      arr[k]=document.getElementById('iDispOrder[' + k + ']').value;
       //alert(arr[k]);
       if(arr[k]==n1)
      {
         count++;
          if(count>1)
         {
            document.getElementById('iDispOrder[' + r + ']').value='';
         }
      }            
   }
  
 
    
}
function validzero(num)
{
    var n1=num.value;
    if(num<=0)
    {
    alert("Number should be greater than 0");

    }
}

function AlphaSearch(val){
    var alphavalue = val;
    var file = 'pr-currency';
    window.location="index.php?file="+file+"&alp="+alphavalue+"&mode=view";
    return false;
}
function MakeAction(loopid,type){
    document.frmlist.iLanguageMasId.value = loopid;
    document.frmlist.action.value = type;
	document.frmlist.submit();	
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
{/literal}