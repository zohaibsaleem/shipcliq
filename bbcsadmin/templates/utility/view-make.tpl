<script language="JavaScript" src="{$tconfig.tcp_javascript}jlist.js"></script>
<div class="contentcontainer">
	<div class="headings altheading">
		<h2>Car Make</h2>
	</div>
	<div class="contentbox">
    {if $var_msg_new neq ''}
     <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg_new}</p> 
     </div>     
    <div></div>
    {elseif $var_msg neq ''}
    <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg}</p> 
     </div>     
    <div></div>
    {/if}
		<form name="frmsearch" id="frmsearch" action="" method="post" enctype="multipart/form-data"> 
		<input type="hidden" name="action" id="action" value=""/>
        <table width="100%" border="0">
		<tbody>
			<tr>
				<td width="10%"><label for="textfield"><strong>Search:</strong></label><td>
				<td width="10%"><input type="Text" id="keyword" name="keyword" value="{$keyword}"  class="inputbox" /><td>
				<td width="10%" >
					<select name="option" id="option">
						<option  value="vMake">Make</option>
						<option value="eStatus">Status</option>
					</select>
				</td>
				<td width="60%"><input type="button" value="Search" class="btnalt" id="Search" name="Search" title="Search" onclick="Searchoption();"/></td>
        <td width="10%"><input type="button" value="Add New" onclick="Redirect('index.php?file=u-make&mode=add');" class="btnalt" /></td>
				
			</tr>	
			
<!--			 <tr>
            <td></td><td></td>
            <td width="10%"><label for="textfield" style="padding-right:0px;"><strong>Import Make:</strong></label></td>
            <td width="30%"><input type="file" id="csv" name="csv"/>&nbsp;&nbsp;
                <input type="button" name="Import" value="Import" onclick="return import_data();" class="btnalt"/></td>
      </tr>-->
			
			
			
			<tr>
				<td colspan="7" align="center">
					{$AlphaBox}
				</td>
			</tr>
		</tbody>			
		</table> 
        </form>
        <form name="frmlist" id="frmlist"  action="index.php?file=u-make_a" method="post">
        
		<table width="100%" border="0">
		<input type="hidden" name="action" id="action" value="" />
        <input  type="hidden" name="iMakeId" value=""/>
        <thead>
			<tr>
					<th width="30%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=u-make&mode=view&sortby=1&order={if $order eq 0}1{else}0{/if}');">Make</a>{if $sortby eq 1}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>   
				<th width="30%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=u-make&mode=view&sortby=2&order={if $order eq 0}1{else}0{/if}');">Status</a>{if $sortby eq 2}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>
				<th>Action</th>
				<th><input type="checkbox" id="check_all" name="check_all" onclick="checkAll(document.frmlist);"/></th>
			</tr>
		</thead>
		<tbody>
        {if $db_make_all|@count gt 0}
		{section name=i loop=$db_make_all}
        <tr>
			<td><a href="{$tconfig.tpanel_url}/index.php?file=u-make&mode=edit&iMakeId={$db_make_all[i].iMakeId}" title="">{$db_make_all[i].vMake}</a></td>
			<td>{$db_make_all[i].eStatus}</td>
			<td>
				<a href="javascript:void(0);" title="Active" onclick="MakeAction('{$db_make_all[i].iMakeId}','Active');"><img src="{$tconfig.tpanel_img}icons/icon_approve.png" title="Active" /></a>
				<a href="javascript:void(0);" title="Inactive" onclick="MakeAction('{$db_make_all[i].iMakeId}','Inactive');"><img src="{$tconfig.tpanel_img}icons/icon_unapprove.png" title="Inactive" /></a>
				<a href="javascript:void(0);" title="Delete" onclick="MakeAction('{$db_make_all[i].iMakeId}','Deletes');"><img src="{$tconfig.tpanel_img}icons/icon_delete.png" title="Delete" /></a>
			</td>
			<td><input name="iMakeId[]" type="checkbox" id="iId" value="{$db_make_all[i].iMakeId}"/></td>
		</tr>
        {/section}
        {else}
        <tr>
			<td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No Record Found.</td>
		</tr>
        {/if}
		</tbody>
		</table>
        </form>
		<div class="extrabottom">
			<ul>
				<li><img src="{$tconfig.tpanel_img}icons/icon_edit.png" alt="Edit" /> Edit</li>
				<li><img src="{$tconfig.tpanel_img}icons/icon_approve.png" alt="Approve" /> Active</li>
				<li><img src="{$tconfig.tpanel_img}icons/icon_unapprove.png" alt="Unapprove" /> Inactive</li>
				<li><img src="{$tconfig.tpanel_img}icons/icon_delete.png" alt="Delete" /> Remove</li>
			</ul>
			<div class="bulkactions">
				<select name="newaction" id="newaction">
					<option value="">Select Action</option>
					<option value="Active">Make Active</option>
					<option value="Inactive">Make Inactive</option>
					<option value="Deletes">Make Delete</option>
					<option value="Show All">Show All</option>
				</select>
				<input type="submit" value="Apply" class="btn" onclick="return Doaction(document.getElementById('newaction').value,'u-make',document.frmlist);"/>
			</div>
		</div>
        <div>
            <div class="pagination">
            {if $db_make_all|@count gt 0}
	        <span class="switch" style="float: left;">{$recmsg}</span>
	        {/if}
            </div>
            {$page_link}
        </div>
		
        <!--<ul class="pagination">
			<li class="text">Previous</li>
			<li class="page"><a href="#" title="">1</a></li>
			<li><a href="#" title="">2</a></li>
			<li><a href="#" title="">3</a></li>
			<li><a href="#" title="">4</a></li>
			<li class="text"><a href="#" title="">Next</a></li>
		</ul>-->
		<div style="clear: both;"></div></div>
</div>
{literal}
<script>
function Searchoption(){
    document.getElementById('frmsearch').submit();
}
function AlphaSearch(val){
    var alphavalue = val;
    var file = 'u-make';
    window.location="index.php?file="+file+"&alp="+alphavalue+"&mode=view";
    return false;
}
function MakeAction(loopid,type){
    document.frmlist.iMakeId.value = loopid;
    document.frmlist.action.value = type;
	document.frmlist.submit();	
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
function import_data()
{   
   if(document.getElementById("csv").value == "")
   {
      alert("Please Select CSV File For import data");
      return false;
   }
   var ans = confirm('Are you sure import data?');                                                                                                                                                                                                                                                          
   if(ans == true)
   {    
    document.frmsearch.action.value = "Upload";
    document.frmsearch.submit();                                                                                                  
   }else{                                                                                                                                                                                                                                                                          
    return false;
   }
}
</script>
{/literal}