<script language="JavaScript" src="{$tconfig.tcp_javascript}jlist.js"></script>
<div class="contentcontainer">
	<div class="headings altheading">
		<h2>Events</h2>
	</div>
	<div class="contentbox">
    {if $var_msg_new neq ''}
     <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg_new}</p> 
     </div>     
    <div></div>
    {elseif $var_msg neq ''}
    <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg}</p> 
     </div>     
    <div></div>
    {/if}
		<form name="frmsearch" id="frmsearch" action="" method="post">
        
        <table width="100%" border="0">
		<tbody>
			<tr>
				<td width="10%"><label for="textfield"><strong>Search:</strong></label><td>
				<td width="10%"><input type="Text" id="keyword" name="keyword" value="{$keyword}"  class="inputbox" /><td>
				<td width="10%" >
					<select name="option" id="option">
						<option  value='vTitle'>Title</option>
            <option  value='iDisplayOrder'{if $option eq 'iDisplayOrder'}selected{/if}>Display Order</option>					
						<option value='eStatus'{if $option eq 'eStatus'}selected{/if}>Status</option>
					</select>
				</td>
				<td width="60%"><input type="button" value="Search" class="btnalt" id="Search" name="Search" title="Search" onclick="Searchoption();"/></td>
				<td width="10%"><input type="button" value="Add New" onclick="Redirect('index.php?file=u-news&mode=add');" class="btnalt" /></td>
			</tr>	
			<tr>
				<td colspan="7" align="center">
					{$AlphaBox}
				</td>
			</tr>
		</tbody>			
		</table> 
        </form>
        <form name="frmlist" id="frmlist"  action="index.php?file=u-news_a" method="post">
        
		<table width="100%" border="0">
		<input type="hidden" name="action" id="action" value="" />
        <input  type="hidden" name="iNewsId" value=""/>
        <thead>
			<tr>
				<th width="25%">Title</th>
				<th width="15%">Display Order</th>
				<th width="20%">Added Date</th>
                <th width="10%">Image</th>			
				<th width="10%">Status</th>
				<th width="10%">Action</th>
				<th width="10%"><input type="checkbox" id="check_all" name="check_all" onclick="checkAll(document.frmlist);"/></th>
			</tr>
		</thead>
		<tbody>
        {if $db_news_all|@count gt 0}
		{section name=i loop=$db_news_all}
        <tr>
			<td width="25%"><a href="{$tconfig.tpanel_url}/index.php?file=u-news&mode=edit&iNewsId={$db_news_all[i].iNewsId}" title="">{$db_news_all[i].vTitle}</a></td>
			<td width="10%">{$db_news_all[i].iDisplayOrder}</td>
			<td width="25%">{$generalobj->DateTime($db_news_all[i].dDate,7)}</td>
      {if $db_news_all[i].checkimg eq 'yes'}
      
      <td width="10%"><img src= "{$tconfig.tsite_upload_images_promotion}1_{$db_news_all[i].vImage}" Width="50px" Height="50px" /></td>
      {else}
      <td></td>
      {/if}		
			<td width="10%">{$db_news_all[i].eStatus}</td>
			<td width="10%">
				<a href="{$tconfig.tpanel_url}/index.php?file=u-news&mode=edit&iNewsId={$db_news_all[i].iNewsId}" title=""><img src="{$tconfig.tpanel_img}icons/icon_edit.png" title="Edit" /></a>
				<a href="javascript:void(0);" title="Active" onclick="MakeAction('{$db_news_all[i].iNewsId}','Active');"><img src="{$tconfig.tpanel_img}icons/icon_approve.png" title="Active" /></a>
				{if $db_admin_all[i].vUserName NEQ "superadmin"}
				<a href="javascript:void(0);" title="Inactive" onclick="MakeAction('{$db_news_all[i].iNewsId}','Inactive');"><img src="{$tconfig.tpanel_img}icons/icon_unapprove.png" title="Inactive" /></a>
				<a href="javascript:void(0);" title="Delete" onclick="MakeAction('{$db_news_all[i].iNewsId}','Deletes');"><img src="{$tconfig.tpanel_img}icons/icon_delete.png" title="Delete" /></a>
				{/if}
			</td>
			<td width="10%">{if $db_admin_all[i].vUserName NEQ "superadmin"}<input name="iNewsId[]" type="checkbox" id="iId" value="{$db_news_all[i].iNewsId}"/>{/if}</td>
		</tr>
        {/section}
        {else}
        <tr>
			<td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No Record Found.</td>
		</tr>
        {/if}
		</tbody>
		</table>
        </form>
		<div class="extrabottom">
			<ul>
				<li><img src="{$tconfig.tpanel_img}icons/icon_edit.png" alt="Edit" /> Edit</li>
				<li><img src="{$tconfig.tpanel_img}icons/icon_approve.png" alt="Approve" /> Active</li>
				<li><img src="{$tconfig.tpanel_img}icons/icon_unapprove.png" alt="Unapprove" /> Inactive</li>
				<li><img src="{$tconfig.tpanel_img}icons/icon_delete.png" alt="Delete" /> Remove</li>
			</ul>
			<div class="bulkactions">
				<select name="newaction" id="newaction">
					<option value="">Select Action</option>
					<option value="Active">Make Active</option>
					<option value="Inactive">Make Inactive</option>
					<option value="Deletes">Make Delete</option>
					<option value="Show All">Show All</option>
				</select>
				<input type="submit" value="Apply" class="btn" onclick="return Doaction(document.getElementById('newaction').value,'u-news',document.frmlist);"/>
			</div>
		</div>
        <div>
            <div class="pagination">
            {if $db_news_all|@count gt 0}
	        <span class="switch" style="float: left;">{$recmsg}</span>
	        {/if}
            </div>
            {$page_link}
        </div>
		
        <!--<ul class="pagination">
			<li class="text">Previous</li>
			<li class="page"><a href="#" title="">1</a></li>
			<li><a href="#" title="">2</a></li>
			<li><a href="#" title="">3</a></li>
			<li><a href="#" title="">4</a></li>
			<li class="text"><a href="#" title="">Next</a></li>
		</ul>-->
		<div style="clear: both;"></div></div>
</div>
{literal}
<script>
function Searchoption(){
    document.getElementById('frmsearch').submit();
}
function AlphaSearch(val){
    var alphavalue = val;
    var file = 'u-news';
    window.location="index.php?file="+file+"&alp="+alphavalue+"&mode=view";
    return false;
}
function MakeAction(loopid,type){
if(type == 'Deletes')
    {
	   ans = confirm("Are you sure you want to delete?");
     if (ans == false)
      {
        return false;
      }
    }
    document.frmlist.iNewsId.value = loopid;
    document.frmlist.action.value = type;
	document.frmlist.submit();	
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
{/literal}