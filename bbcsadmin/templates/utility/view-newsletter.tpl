<script language="JavaScript" src="{$tconfig.tcp_javascript}jlist.js"></script>
<div class="contentcontainer">
	<div class="headings altheading">
		<h2>Newsletter</h2>
	</div>
	<div class="contentbox">
    {if $var_msg_new neq ''}
     <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg_new}</p> 
     </div>     
    <div></div>
    {elseif $var_msg neq ''}
    <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg}</p> 
     </div>     
    <div></div>
    {/if}
		<form name="frmsearch" id="frmsearch" action="" method="post">
        
        <table width="100%" border="0">
		<tbody>
			<tr>
				<td width="10%"><label for="textfield"><strong>Search:</strong></label><td>
				<td width="10%"><input type="Text" id="keyword" name="keyword" value="{$keyword}"  class="inputbox" /><td>
				<td width="10%" >
					<select name="option" id="option">
						<option  value="vEmail">Newsletter</option>
						<option value="eType"{if $option eq 'eType'}selected{/if}>Type</option>
					</select>
				</td>
				<td width="60%"><input type="button" value="Search" class="btnalt" id="Search" name="Search" title="Search" onclick="Searchoption();"/></td>
				<td width="10%"><input type="button" value="Add New" onclick="Redirect('index.php?file=u-newsletter&mode=add');" class="btnalt" /></td>
			</tr>	
			<tr>
				<td colspan="7" align="center">
					{$AlphaBox}
				</td>
			</tr>
		</tbody>			
		</table> 
        </form>
        <form name="frmlist" id="frmlist"  action="index.php?file=u-newsletter_a" method="post">
        
		<table width="100%" border="0">
		<input type="hidden" name="action" id="action" value="" />
        <input  type="hidden" name="iNewsTId" value=""/>
        <thead>
			<tr>
			
			 <th width="20%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=u-newsletter&mode=view&sortby=1&order={if $order eq 0}1{else}0{/if}');">Newsletter</a>{if $sortby eq 1}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>               
        <th width="20%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=u-newsletter&mode=view&sortby=2&order={if $order eq 0}1{else}0{/if}');">Added Date</a>{if $sortby eq 2}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>               
        <th width="20%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=u-newsletter&mode=view&sortby=3&order={if $order eq 0}1{else}0{/if}');">Last Sent Date</a>{if $sortby eq 3}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>               
        <th width="20%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=u-newsletter&mode=view&sortby=4&order={if $order eq 0}1{else}0{/if}');">Send Status</a>{if $sortby eq 4}&nbsp;<img src="{$tconfig.tpanel_img}{if $order eq 0}icons/up_active.png{else}icons/down_active.png{/if}">{/if}</th>               
      		
				<th>Send</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
        {if $db_newsletter_article_all|@count gt 0}
		{section name=i loop=$db_newsletter_article_all}
        <tr>
			<td><a href="{$tconfig.tpanel_url}/index.php?file=u-newsletter&mode=edit&iNewsTId={$db_newsletter_article_all[i].iNewsTId}" title="">{$db_newsletter_article_all[i].vTitle}</a></td>
			<td>{$db_newsletter_article_all[i].dAddedDate}</td>
			<td>{$db_newsletter_article_all[i].dLastSentDate}</td>
			<td>{$db_newsletter_article_all[i].eSendStatus}</td>
			<td><a href="{$tconfig.tpanel_url}/index.php?file=u-newsletter_send&mode=edit&iNewsTId={$db_newsletter_article_all[i].iNewsTId}">Send</a></td>
			
			<td>
				<a href="javascript:void(0);" title="Delete" onclick="MakeAction('{$db_newsletter_article_all[i].iNewsTId}','Deletes');"><img src="{$tconfig.tpanel_img}icons/icon_delete.png" title="Delete" /></a>
			</td>
		</tr>
        {/section}
        {else}
        <tr>
			<td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No Record Found.</td>
		</tr>
        {/if}
		</tbody>
		</table>
        </form>
        <div>
            <div class="pagination">
            {if $db_newsletter_article_all|@count gt 0}
	        <span class="switch" style="float: left;">{$recmsg}</span>
	        {/if}
            </div>
            {$page_link}
        </div>
		<div style="clear: both;"></div></div>
</div>
{literal}
<script>
function Searchoption(){
    document.getElementById('frmsearch').submit();
}
function AlphaSearch(val){
    var alphavalue = val;
    var file = 'u-newsletter';
    window.location="index.php?file="+file+"&alp="+alphavalue+"&mode=view";
    return false;
}
function MakeAction(loopid,type){
if(type == 'Deletes')
    {
	   ans = confirm("Are you sure you want to delete?");
     if (ans == false)
      {
        return false;
      }
    }
    document.frmlist.iNewsTId.value = loopid;
    document.frmlist.action.value = type;
	document.frmlist.submit();	
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
{/literal}