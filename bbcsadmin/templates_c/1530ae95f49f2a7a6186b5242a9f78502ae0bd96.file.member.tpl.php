<?php /* Smarty version Smarty-3.0.7, created on 2016-02-01 21:51:23
         compiled from "/home4/shipcliq/public_html/bbcsadmin/templates/member/member.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11153193556b027bbe343e8-26886799%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1530ae95f49f2a7a6186b5242a9f78502ae0bd96' => 
    array (
      0 => '/home4/shipcliq/public_html/bbcsadmin/templates/member/member.tpl',
      1 => 1453900196,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11153193556b027bbe343e8-26886799',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="contentcontainer" id="tabs">
	<div class="headings">
		<?php if ($_smarty_tpl->getVariable('mode')->value=='edit'){?>
			<h2 class="left">Edit Member</h2>
		<?php }else{ ?>
			<h2 class="left">Add Member</h2>
		<?php }?>
	</div>
	<div id="tabs-1" class="contentbox">  
  <?php if ($_smarty_tpl->getVariable('mode')->value=='edit'){?>
    <div id="tabs2" class="VideoTabs">
	  <ul>
    	  <li><a href="javascript:void(0);" id="tab1" class="current"><em>Member Details</em></a></li>
    	  <li><a href="javascript:void(0);" onclick="javascript:window.location='index.php?file=m-member_alert&mode=view&iMemberId=<?php echo $_smarty_tpl->getVariable('iMemberId')->value;?>
';" id="tab2"><em>Member Alert</em></a></li>
        <li><a href="javascript:void(0);" onclick="javascript:window.location='index.php?file=m-member_rate&mode=view&iMemberId=<?php echo $_smarty_tpl->getVariable('iMemberId')->value;?>
';" id="tab3"><em>Member Ratings & Reviews</em></a></li>			  
    </ul>
		  <div style="clear:both"></div>
		</div>
		
	<?php }?>
	      <?php if ($_smarty_tpl->getVariable('var_msg')->value!=''){?>
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_error.png" title="Success" />
					<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
</p> 
				</div>     
				<div></div>
			<?php }?>
			<?php if ($_GET['var_msg_error']!=''){?>
				<div class="status success" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_success.png" title="Success" />
					<?php echo $_smarty_tpl->getVariable('var_msg_error')->value;?>
</p> 
				</div>     
				 <?php }?>
				 <?php if ($_smarty_tpl->getVariable('var_msg_mow')->value!=''){?>
				<div class="status success" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_success.png" title="Success" />
					<?php echo $_smarty_tpl->getVariable('var_msg_mow')->value;?>
</p> 
				</div>     
				<div></div>
			<?php }?>
		<div id="keyfeatures" class="VideoText"> 			
		  <form id="frmadd" name="frmadd" action="index.php?file=m-member_a" method="post" enctype="multipart/form-data">
            <input type="hidden" name="iMemberId" id="iMemberId" value="<?php echo $_smarty_tpl->getVariable('iMemberId')->value;?>
" />
            <input type="hidden" name="action" id="action" value="<?php echo $_smarty_tpl->getVariable('mode')->value;?>
" />
		      	<input type="hidden" name="imagemode" id="imagemode" />	
			<p>
					<label for="textfield"><strong>First Name :<em>*</em></strong></label>
					<input type="text" id="vFirstName" name="Data[vFirstName]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_records')->value[0]['vFirstName'];?>
" lang="*" title="First Name"/>
			</p>
			<p>
					<label for="textfield"><strong>Last Name :<em>*</em></strong></label>
					<input type="text" id="vLastName" name="Data[vLastName]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_records')->value[0]['vLastName'];?>
" lang="*" title="Last Name"/>
			</p>
			<p>
					<label for="textfield"><strong>Gender :</strong></label>
					<select id="eGender" name="Data[eGender]">
						<option value="Male" <?php if ($_smarty_tpl->getVariable('db_records')->value[0]['eGender']=='Male'){?>selected <?php }?>>Male</option>
						<option value="Female" <?php if ($_smarty_tpl->getVariable('db_records')->value[0]['eGender']=='Female'){?>selected <?php }?>>Female</option>
					</select>
			</p>
			<p>
			   	<label for="textfield"><strong>BirthDate :</strong></label>
			   <table>
					<tr>
					<!--
          <td style="float:left;">
                      <?php echo $_smarty_tpl->getVariable('generalobj')->value->gend_DisplayDay($_smarty_tpl->getVariable('uid')->value,'startDay',$_smarty_tpl->getVariable('datearr')->value[2],'');?>
                         
                      </td>
                      <td style="float:left;">
                      <?php echo $_smarty_tpl->getVariable('generalobj')->value->gend_DisplayMonth($_smarty_tpl->getVariable('pass')->value,'startMonth',$_smarty_tpl->getVariable('datearr')->value[1],'');?>
                         
                      </td>
                      -->
                      <td style="float:left;">
                      <?php echo $_smarty_tpl->getVariable('generalobj')->value->DisplayYear($_smarty_tpl->getVariable('user')->value,'startYear','1920',date('Y'));?>
                         
                      </td>
					</tr></table>
			</p>
			<p>
					<label for="textfield"><strong>E-mail :<em>*</em></strong></label>
					<input type="text" id="vEmail"  name="Data[vEmail]" class="inputbox"  lang="*{E}" title="E-mail" value="<?php echo $_smarty_tpl->getVariable('db_records')->value[0]['vEmail'];?>
"/>
			</p>
			<p>
					<label for="textfield"><strong>Password :<em>*</em></strong></label>
					<input type="password" id="vPassword"  name="Data[vPassword]" class="inputbox" lang="*{P}6:0" title="Password" value="<?php echo $_smarty_tpl->getVariable('db_records')->value[0]['vPassword'];?>
"/>
			</p>

			<p>
					<label for="textfield"><strong>Address :</strong></label>
					<textarea name="Data[vAddress]" id="vAddress" class="inputbox"  title="Address"><?php echo $_smarty_tpl->getVariable('db_records')->value[0]['vAddress'];?>
</textarea>
			</p>
			
		  <p>
        <label for="textfield"><strong>Country : <em>*</em></strong></label> 
            <select id="vCountryCode" name="Data[vCountry]" lang="*" onchange="get_county_list(this.value);" style="" title="Country">
              <option value="">--------Select Country--------</option>           
    		      <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_country')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>   		        
    		            <option value="<?php echo $_smarty_tpl->getVariable('db_country')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vCountryCode'];?>
" <?php if ($_smarty_tpl->getVariable('db_country')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vCountryCode']==$_smarty_tpl->getVariable('db_records')->value[0]['vCountry']){?>selected <?php }?>><?php echo $_smarty_tpl->getVariable('db_country')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vCountry'];?>
</option>		                                                                                     		                     
              <?php endfor; endif; ?>           
           </select>
        </p>
		  	<p>
            <label for="textfield"><strong>State : <em>*</em></strong></label>
            <span id="county_list"> 
            <select id="vStateCode" name="Data[vState]" style="width:25%;">
              <option value="">--------Select State---------</option>
            </select>
            </span>
        </p>
				<p>
					<label for="textfield"><strong>City :</strong></label>
					<input type="text" id="vCity"  name="Data[vCity]" class="inputbox" title="City" value="<?php echo $_smarty_tpl->getVariable('db_records')->value[0]['vCity'];?>
"/>
                </p>
				<p>
					<label for="textfield"><strong>Zipcode :</strong></label>
					<input type="text" id="vZip"  name="Data[vZip]" class="inputbox" title="Zip" value="<?php echo $_smarty_tpl->getVariable('db_records')->value[0]['vZip'];?>
" />
				</p>
				<p>
					<label for="textfield"><strong>Phone :</strong></label>
					<input type="text" id="vPhone"  name="Data[vPhone]" class="inputbox" title="Phone" lang="*{T}" value="<?php echo $_smarty_tpl->getVariable('db_records')->value[0]['vPhone'];?>
"/>
				</p>
					<p>
					<label for="textfield"><strong>Upload Image :</strong></label>
					<input type="file" id="vImage" name="Data[vImage]" class="inputbox" title="Image" title="Upload Image" />
				</p>
			  <?php if ($_smarty_tpl->getVariable('mode')->value=='edit'){?>					
				<?php if ($_smarty_tpl->getVariable('img')->value=='yes'){?>	
				<p>                                
				  <table>
				    <tr>
				        <td width="25%"><img src= "<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_upload_images_member'];?>
/<?php echo $_smarty_tpl->getVariable('db_records')->value[0]['iMemberId'];?>
/1_<?php echo $_smarty_tpl->getVariable('db_records')->value[0]['vImage'];?>
" /></td>
            </tr>
             <tr><td><input type="button" value="Delete Image" class="btn" onclick="return confirm_delete();" title="Delete Image"/>         
        </td></tr>
          </table>
				</p>
				<?php }?>
        <?php }else{ ?>
        <?php }?> 
				
				<p>
					<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" <?php if ($_smarty_tpl->getVariable('db_records')->value[0]['eStatus']=='Active'){?>selected <?php }?>>Active</option>
						<option value="Inactive" <?php if ($_smarty_tpl->getVariable('db_records')->value[0]['eStatus']=='Inactive'){?>selected <?php }?>>Inactive</option>
						<option value="Pending" <?php if ($_smarty_tpl->getVariable('db_records')->value[0]['eStatus']=='Pending'){?>selected <?php }?>>Pending</option>
            <option value="Deleted" <?php if ($_smarty_tpl->getVariable('db_records')->value[0]['eStatus']=='Deleted'){?>selected <?php }?>>Deleted</option>						
					</select>
				</p>
			<?php if ($_smarty_tpl->getVariable('mode')->value=='edit'){?>
				<input type="submit" value="Edit Member" class="btn" onclick="return validate(document.frmadd);" title="Edit"/>
			<?php }else{ ?>
   	    <input type="submit" value="Add Member" class="btn" onclick="return validate(document.frmadd);" title="Add"/>
  		<?php }?>
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
			
			</form>
		
 		 </div>	 
	</div>
</div>

<script>
function redirectcancel()
{
    window.location="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=m-member&mode=view";
    return false;
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}

$(document).ready(function(){

  $(function() {
    $( "#tabs2" ).tabs({ selected: 0 });
	
  });
});
function confirm_delete()
{
   ans = confirm('Are you sure you want to delete Image?');
   if(ans == true){
    document.frmadd.imagemode.value = 'delete_image';
    document.frmadd.submit();
   }else{
    return false;
   }
} 

function get_county_list(code,selected)
{   
$("#county_list").html("Please wait...");
var request = $.ajax({
type: "POST",
url: '<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
'+'/index.php?file=m-getState',
data: "code="+code+"&stcode="+selected,

success: function(data) { 
$("#county_list").html(data);
}
});

request.fail(function(jqXHR, textStatus) {
//alert( "Request failed: " + textStatus );
});
}

if('<?php echo $_smarty_tpl->getVariable('db_records')->value[0]['vState'];?>
' != '' || '<?php echo $_smarty_tpl->getVariable('db_records')->value[0]['vCountry'];?>
' != ''){
get_county_list('<?php echo $_smarty_tpl->getVariable('db_records')->value[0]['vCountry'];?>
', '<?php echo $_smarty_tpl->getVariable('db_records')->value[0]['vState'];?>
');
}

function show_addnew_tr(){
  document.getElementById("addnewtr").style.display="";
}
function hide_addnew_tr(){
  document.getElementById("addnewtr").style.display="none";
}
function memberOfWeek(val){
    window.location="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=m-member_a&action=memberofweek&status=<?php echo $_smarty_tpl->getVariable('db_records')->value[0]['eStatus'];?>
&iMemberId=<?php echo $_smarty_tpl->getVariable('db_records')->value[0]['iMemberId'];?>
&val="+val;
    return false;
}
</script>


