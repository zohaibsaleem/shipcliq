<?php /* Smarty version Smarty-3.0.7, created on 2015-05-21 13:04:15
         compiled from "/home/www/xfetch/xfadmin/templates/member/view-member.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1644935193555d8a772242a2-82744676%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1b7bc48f173afebff52280f0c10b5e91e1ced67e' => 
    array (
      0 => '/home/www/xfetch/xfadmin/templates/member/view-member.tpl',
      1 => 1432032005,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1644935193555d8a772242a2-82744676',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<script language="JavaScript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tcp_javascript'];?>
jlist.js"></script>
<div class="contentcontainer">
	<div class="headings altheading">
		<h2>Members</h2>
	</div>
	<div class="contentbox">
    <?php if ($_smarty_tpl->getVariable('var_msg_new')->value!=''){?>
     <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_success.png" title="Success" />
              <?php echo $_smarty_tpl->getVariable('var_msg_new')->value;?>
</p> 
     </div>     
    <div></div>
    <?php }elseif($_smarty_tpl->getVariable('var_msg')->value!=''){?>
    <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_success.png" title="Success" />
              <?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
</p> 
     </div>     
    <div></div>
    <?php }?>
    <?php if ($_GET['var_msg_err']!=''){?>
     <div class="status error" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_error.png" title="Error" />
              <?php echo $_GET['var_msg_err'];?>
</p> 
     </div>     
    <div></div>
    <?php }?>
		<form name="frmsearch" id="frmsearch" action="" method="post">
        
        <table width="100%" border="0">
		<tbody>
			<tr>
				<td width="10%"><label for="textfield"><strong>Search:</strong></label><td>
				<td width="10%"><input type="Text" id="keyword" name="keyword" value="<?php echo $_smarty_tpl->getVariable('keyword')->value;?>
"  class="inputbox" /><td>
				<td width="10%" >
					<select name="option" id="option">
						<option  value='concat(vFirstName," ",vLastName)'>Name</option> 					
						<option value="vEmail"<?php if ($_smarty_tpl->getVariable('option')->value=='vEmail'){?>selected<?php }?>>E-mail</option>
						<option value="eStatus"<?php if ($_smarty_tpl->getVariable('option')->value=='eStatus'){?>selected<?php }?>>Status</option>
					</select>
				</td>
				<td width="60%"><input type="button" value="Search" class="btnalt" id="Search" name="Search" title="Search" onclick="Searchoption();"/></td>
	<!--			<td width="10%"><input type="button" value="Add New" onclick="Redirect('index.php?file=m-member&mode=add');" class="btnalt" />&nbsp;</td> -->
			</tr>	
			<tr>
				<td colspan="7" align="center">
					<?php echo $_smarty_tpl->getVariable('AlphaBox')->value;?>

				</td>
			</tr>
		</tbody>			
		</table> 
    </form>
 
    <form name="frmlist" id="frmlist"  action="index.php?file=m-member_a" method="post">
 		<table width="100%" border="0">
		<input type="hidden" name="action" id="action" value="" />
        <input  type="hidden" name="iMemberId" value=""/>
        <thead>
			<tr>
				 <th width="15%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=m-member&mode=view&sortby=1&order=<?php if ($_smarty_tpl->getVariable('order')->value==0){?>1<?php }else{ ?>0<?php }?>');">Name</a><?php if ($_smarty_tpl->getVariable('sortby')->value==1){?>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
<?php if ($_smarty_tpl->getVariable('order')->value==0){?>icons/up_active.png<?php }else{ ?>icons/down_active.png<?php }?>"><?php }?></th>               
        <th width="10%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=m-member&mode=view&sortby=2&order=<?php if ($_smarty_tpl->getVariable('order')->value==0){?>1<?php }else{ ?>0<?php }?>');">E-mail</a><?php if ($_smarty_tpl->getVariable('sortby')->value==2){?>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
<?php if ($_smarty_tpl->getVariable('order')->value==0){?>icons/up_active.png<?php }else{ ?>icons/down_active.png<?php }?>"><?php }?></th>               
        <th width="10%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=m-member&mode=view&sortby=3&order=<?php if ($_smarty_tpl->getVariable('order')->value==0){?>1<?php }else{ ?>0<?php }?>');">Added Date</a><?php if ($_smarty_tpl->getVariable('sortby')->value==3){?>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
<?php if ($_smarty_tpl->getVariable('order')->value==0){?>icons/down_active.png<?php }else{ ?>icons/up_active.png<?php }?>"><?php }?></th>               
    	  <th width="10%"><div align="center">Access Account</div></th>
        <th width="10%"><div align="center">Delete Account</div></th>
         <th width="10%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=m-member&mode=view&sortby=4&order=<?php if ($_smarty_tpl->getVariable('order')->value==0){?>1<?php }else{ ?>0<?php }?>');">Status</a><?php if ($_smarty_tpl->getVariable('sortby')->value==4){?>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
<?php if ($_smarty_tpl->getVariable('order')->value==0){?>icons/up_active.png<?php }else{ ?>icons/down_active.png<?php }?>"><?php }?></th>               
       	<th width="10%">Action</th>
				<th width="5%"><input type="checkbox" id="check_all" name="check_all" onclick="checkAll(document.frmlist);"/></th>
			</tr>
		</thead>
		<tbody>
    <?php if (count($_smarty_tpl->getVariable('db_records_all')->value)>0){?>
	  <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_records_all')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
     <tr>
			<td><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=m-member&mode=edit&iMemberId=<?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iMemberId'];?>
" title=""><?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vLastName'];?>
</a></td>
      <td><?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vEmail'];?>
</td>
			<td><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dAddedDate'],9);?>
</td>
      <td><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
/login_admin.php?iMemberId=<?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iMemberId'];?>
" target="_blank"><div align="center"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
login-icon.png"/></div></a></td>  
			<td style="text-align:center;"><span id="delacc<?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iMemberId'];?>
"></span><span id="delac<?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iMemberId'];?>
"><a href="javascript:void(0);" onclick="deleteAccount(<?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iMemberId'];?>
,'');">Delete</a></span></td>
      <td><?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eStatus'];?>
</td>
			<td>
				<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=m-member&mode=edit&iMemberId=<?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iMemberId'];?>
" title=""><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_edit.png" title="Edit" /></a>
				<a href="javascript:void(0);" title="Active" onclick="MakeAction('<?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iMemberId'];?>
','Active');"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_approve.png" title="Active" /></a>
				<a href="javascript:void(0);" title="Inactive" onclick="MakeAction('<?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iMemberId'];?>
','Inactive');"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_unapprove.png" title="Inactive" /></a>
				<a href="javascript:void(0);" title="Pending" onclick="MakeAction('<?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iMemberId'];?>
','Pending');"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon-pending.gif" title="Pending" /></a>
        <a href="javascript:void(0);" title="Delete" onclick="MakeAction('<?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iMemberId'];?>
','Deleted');"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_delete.png" title="Delete" /></a>
			</td>
			<td><input name="iMemberId[]" type="checkbox" id="iId" value="<?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iMemberId'];?>
"/></td>
		</tr>
        <?php endfor; endif; ?>
        <?php }else{ ?>
        <tr>
			<td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No Record Found.</td>
		</tr>
        <?php }?>
		</tbody>
		</table>
        </form>
		<div class="extrabottom">
			<ul>
				<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_edit.png" alt="Edit" /> Edit</li>
				<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_approve.png" alt="Approve" /> Active</li>
				<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_unapprove.png" alt="Unapprove" /> Inactive</li>
				<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon-pending.gif" alt="Pending" /> Pending</li>
				<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_delete.png" alt="Deleted" /> Deleted</li>
			</ul>
			<div class="bulkactions">
				<select name="newaction" id="newaction">
					<option value="">Select Action</option>
					<option value="Active">Make Active</option>
					<option value="Inactive">Make Inactive</option>
					<option value="Pending">Make Pending</option>
					<option value="Deleted">Make Deleted</option>
					<option value="Show All">Show All</option>
				</select>
				<input type="submit" value="Apply" class="btn" onclick="return Doaction(document.getElementById('newaction').value,'m-member',document.frmlist);"/>
			</div>
		</div>
        <div>
            <div class="pagination">
            <?php if (count($_smarty_tpl->getVariable('db_records_all')->value)>0){?>
	        <span class="switch" style="float: left;"><?php echo $_smarty_tpl->getVariable('recmsg')->value;?>
</span>
	        <?php }?>
            </div>
            <?php echo $_smarty_tpl->getVariable('page_link')->value;?>

        </div>
		
        <!--<ul class="pagination">
			<li class="text">Previous</li>
			<li class="page"><a href="#" title="">1</a></li>
			<li><a href="#" title="">2</a></li>
			<li><a href="#" title="">3</a></li>
			<li><a href="#" title="">4</a></li>
			<li class="text"><a href="#" title="">Next</a></li>
		</ul>-->
		<div style="clear: both;"></div></div>
</div>

<script>
function Searchoption(){
    document.getElementById('frmsearch').submit();
}
function AlphaSearch(val){
    var alphavalue = val;
    var file = 'm-member';
	// changes by hemali to show all values
	if(alphavalue != '') // need to search on any alpha value
		window.location="index.php?file="+file+"&alp="+alphavalue+"&mode=view";
	else  // search all result
		window.location="index.php?file="+file+"&mode=view";
	
    return false;
}
function MakeAction(loopid,type){
if(type == 'Deleted')
    {
	   ans = confirm("Are you sure you want to delete?");
     if (ans == false)
      {
        return false;
      }
    }
    document.frmlist.iMemberId.value = loopid;
    document.frmlist.action.value = type;
	document.frmlist.submit();	
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}

function deleteAccount(id,conf){
  if(conf!='yes'){
    var del=confirm("Are you sure delete member account ?");
    if(del==false)
      return false;
  }
  
  $("#delacc"+id).html('Please Wait...');
  $("#delac"+id).hide();
  
  var request = $.ajax({
  type: "POST",
  url: '<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
'+'/index.php?file=m-deleteAccount',
  data: "id="+id+"&delete="+conf,
  
  success: function(data) {
  
   if(data==9999){
      window.location="index.php?file=m-member&mode=view&var_msg=Member account deleted successfully";
      return false;
   }
   if(data==9998){
      window.location="index.php?file=m-member&mode=view&var_msg_err=Erron in delete";
      return false;
   }
   
   var res = data.split("|");
   id=res[1];
   tot=res[0];
   
   if(tot>0){
      var res1=confirm("This member has posted rides or booking rides, sure for delete ?");
      if(res1==false){
        $("#delac"+id).show();
        $("#delacc"+id).html('');
        return false;
      }else{
        deleteAccount(id,"yes");
      }
   }else{
      window.location="index.php?file=m-member&mode=view";
   }
  }
  });
  
  request.fail(function(jqXHR, textStatus) {
    alert( "Request failed: " + textStatus );
  });
} 
</script>
