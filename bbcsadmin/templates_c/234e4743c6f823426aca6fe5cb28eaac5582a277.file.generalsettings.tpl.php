<?php /* Smarty version Smarty-3.0.7, created on 2016-01-21 16:36:08
         compiled from "/home/www/shipcliq/bbcsadmin/templates/tools/generalsettings.tpl" */ ?>
<?php /*%%SmartyHeaderCode:133334695756a108f8b2d5e3-59181351%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '234e4743c6f823426aca6fe5cb28eaac5582a277' => 
    array (
      0 => '/home/www/shipcliq/bbcsadmin/templates/tools/generalsettings.tpl',
      1 => 1452693096,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '133334695756a108f8b2d5e3-59181351',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="contentcontainer">
    <div class="headings alt">
        <h2>Edit General Settings</h2>
	</div>
    <div class="contentbox">
		<?php if ($_smarty_tpl->getVariable('var_msg')->value!=''){?>
		<div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_success.png" title="Success" />
			<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
</p> 
		</div>     
		<div></div>
		<?php }?>
        <form  name="frmadd" id="frmadd"  method="post" action="index.php?file=to-generalsettings_a" enctype="multipart/form-data">
    		<p style="float:right;">
				<select name="eType" id="eType" class="inputbox" style="width:200px;" onChange="go_for_change(this.value);">
					<option value=""> -- Select -- </option>
					<option value="General" <?php if ($_smarty_tpl->getVariable('eType')->value=='General'){?> selected <?php }?>>General</option>
					<option value="Email" <?php if ($_smarty_tpl->getVariable('eType')->value=='Email'){?> selected <?php }?>>Email</option>
					<!--<option value="Apperance" <?php if ($_smarty_tpl->getVariable('eType')->value=='Apperance'){?> selected <?php }?>>Apperance</option>-->
					<?php if (@PAYMENT_OPTION!='Contact'){?>
					<option value="Paypal" <?php if ($_smarty_tpl->getVariable('eType')->value=='Paypal'){?> selected <?php }?>>Paypal</option>
					<option value="SEO" <?php if ($_smarty_tpl->getVariable('eType')->value=='SEO'){?> selected <?php }?>>SEO</option>
					<?php }?>
					<!--<option value="Prices" <?php if ($_smarty_tpl->getVariable('eType')->value=='Prices'){?> selected <?php }?>>Prices</option>-->
				</select>
			</p>
			<input type="hidden" name="action" id="action" value="<?php echo $_smarty_tpl->getVariable('mode')->value;?>
" />
			<input type="hidden" name="imagemode" id="imagemode" value="">
            <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_res')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
			<?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eType']!=$_smarty_tpl->getVariable('currType')->value){?><?php }?>
			<p>
				<label for="textfield"><strong><?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription'];?>
<em>*</em></strong></label>
				
				<?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Site Logo"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Site Favicon"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Footer Logo"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Email Logo"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Ride Background"){?>
				<input type="file" id="<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
" name="Data[<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
]" value="<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue'];?>
" title="<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription'];?>
"/> 
				<?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']!=''&&$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Site Favicon"){?>
				&nbsp;&nbsp;[<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_upload_site_favicon'];?>
/<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue'];?>
" target="_blank" rel="[images]">View</a>]
				&nbsp;&nbsp;[<a href="javascript:void(0);" onclick="confirm_delete('<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
');" title="Delete Image">Delete</a>]
				<?php }?>
				
				<?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']!=''&&$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Site Logo"){?>
				&nbsp;&nbsp;[<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_upload_site_logo'];?>
/1_<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue'];?>
" target="_blank" rel="[images]">View</a>]
				&nbsp;&nbsp;[<a href="javascript:void(0);" onclick="confirm_delete('<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
');" title="Delete Image">Delete</a>]
				<?php }?>
				
				<?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']!=''&&$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Email Logo"){?>
				&nbsp;&nbsp;[<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_upload_email_logo'];?>
/1_<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue'];?>
" target="_blank" rel="[images]">View</a>]
				&nbsp;&nbsp;[<a href="javascript:void(0);" onclick="confirm_delete('<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
');" title="Delete Image">Delete</a>]
				<?php }?>
				
				<?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']!=''&&$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Footer Logo"){?>
				&nbsp;&nbsp;[<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_upload_footer_logo'];?>
/1_<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue'];?>
" target="_blank" rel="[images]">View</a>]
				&nbsp;&nbsp;[<a href="javascript:void(0);" onclick="confirm_delete('<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
');" title="Delete Image">Delete</a>]
				<?php }?>
				
				<?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']!=''&&$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Ride Background"){?>
				&nbsp;&nbsp;[<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_upload_footer_logo'];?>
/1_<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue'];?>
" target="_blank" rel="[images]">View</a>]
				&nbsp;&nbsp;[<a href="javascript:void(0);" onclick="confirm_delete('<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
');" title="Delete Image">Delete</a>]
				<?php }?>
				
				<?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Site Logo"){?>
				<br><br>[Note : Preferable size would be (width:215px; height:55px;)]
				<br>[Note: Supported File Type is *.png,jpg,jpeg,bmp,gif]
				<?php }?>
				<?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Email Logo"){?>
				<br><br>[Note : Preferable size would be (width:215px; height:55px;)]
				<br>[Note: Supported File Type is *.png,jpg,jpeg,bmp,gif]
				<?php }?>
				<?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Site Favicon"){?>
				<br><br>[Note : Preferable size would be (width:16px; height:16px;)]
				<br>[Note: Supported File Type is *.ico]
				<?php }?>
				<?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Footer Logo"){?>
				<br><br>[Note : Preferable size would be (width:16px; height:16px;)]
				<br>[Note: Supported File Type is *.png,jpg,jpeg,bmp,gif]
				<?php }?>
				
				<?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Ride Background"){?>
				<br><br>[Note : Preferable size would be (width:1150px; height:480px;)]
				<br>[Note: Supported File Type is *.png,jpg,jpeg,bmp,gif]
				<?php }?>
				<?php }elseif($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="COMPANY_ADDRESS"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="COMPANY_CONTACTUS_ADDRESS"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="GOOGLE_MAP"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="GOOGLE_ANALYTIC_CODE"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eFieldType']=="Textarea"){?>
				
				<textarea id="<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
" name="Data[<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
]" class="inputbox" lang="*"><?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue'];?>
</textarea>
				<?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="GOOGLE_MAP"){?><br />[Note: Please upload google iframe code of width 284 an height 211 for display map.]<?php }?>
				<?php }elseif($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="LANG_BOX"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="GEO_LOCATION"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="SHOW_COOKIE_POLICY"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="AIRPORTS_RIDES_SHOW"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="CRON_MEMBERSHIP_EMAIL"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="CRON_SATISFACTION_EMAIL"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="PHONE_VERIFICATION_REQUIRED"){?>
				<select name="Data[<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
]" class="inputbox">
					<option value="Yes" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Yes'){?>selected<?php }?>>Yes</option>
					<option value="No" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='No'){?>selected<?php }?>>No</option>
				</select>
				<?php }elseif($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="PAYMENT_OPTION"){?>
				<select name="Data[<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
]" class="inputbox">
					<option value="PayPal" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='PayPal'){?>selected<?php }?>>Online Payment (Payment gateway required)</option>
					<option value="Manual" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Manual'){?>selected<?php }?>>Offline Payment (Payment gateway not required)</option>
					<option value="Contact" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Contact'){?>selected<?php }?>>No Payment</option> 
				</select>	
				<?php }elseif($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="SELECT_THEME"){?>
				<Select name="Data[<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
]" class="inputbox">
					<!--------------------BLABLACAR STYLE TEMPLATE Default ----------------------------------------------->
					<option value="Default-green-home" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Default-green-home'){?>selected<?php }?>>Default</option>
					<option value="Theme1-blue-home" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Theme1-blue-home'){?>selected<?php }?>>Theme 1</option>
					<option value="Theme2-orange-home" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Theme2-orange-home'){?>selected<?php }?>>Theme 2</option>
					<option value="Theme3-skyblue-home" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Theme3-skyblue-home'){?>selected<?php }?>>Theme 3</option>
					<option value="Theme4-red-home" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Theme4-red-home'){?>selected<?php }?>>Theme 4</option>
					<option value="Theme5-brown-home" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Theme5-brown-home'){?>selected<?php }?>>Theme 5</option>
					<option value="Theme6-purple-home" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Theme6-purple-home'){?>selected<?php }?>>Theme 6</option>
					<!--------------------BLABLACAR STYLE TEMPLATE Default  Ends----------------------------------------------------->
					<!--------------------FULL SCREEN DESIGN TEMPLATES � STYLE 1 ----------------------------------------------------->
					<option value="Theme7-orange-home1" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Theme7-orange-home1'){?>selected<?php }?>>Theme 7</option>
					<option value="Theme8-red-home1" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Theme8-red-home1'){?>selected<?php }?>>Theme 8</option>
					<option value="Theme9-green-home1" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Theme9-green-home1'){?>selected<?php }?>>Theme 9</option>
					<option value="Theme10-lightgreen-home1" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Theme10-lightgreen-home1'){?>selected<?php }?>>Theme 10</option>
					<!----------FULL SCREEN DESIGN TEMPLATES � STYLE 1 Ends ----------------------------------------------------->
					<!--------------FULL SCREEN DESIGN TEMPLATES � STYLE 4--------------------------------------------------------->
					<option value="Theme17-skyblue-home2" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Theme17-skyblue-home2'){?>selected<?php }?>>Theme 17</option>
					<option value="Theme18-green-home2" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Theme18-green-home2'){?>selected<?php }?>>Theme 18</option>
					<option value="Theme19-brown-home2" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Theme19-brown-home2'){?>selected<?php }?>>Theme 19</option>
					<option value="Theme20-red-home2" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Theme20-red-home2'){?>selected<?php }?>>Theme 20</option>
					<option value="Theme21-orange-home2" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Theme21-orange-home2'){?>selected<?php }?>>Theme 21</option>
					<option value="Theme22-blue-home2" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Theme22-blue-home2'){?>selected<?php }?>>Theme 22</option>
					<!-------------FULL SCREEN DESIGN TEMPLATES � STYLE 4 Ends--------------------------------------------------------->
					<!------------FULL SCREEN DESIGN TEMPLATES � STYLE 3--------------------------------------------------------->
					<option value="Theme23-green-home3" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Theme23-green-home3'){?>selected<?php }?>>Theme 23</option>
					<option value="Theme26-skyblue-home3" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Theme26-skyblue-home3'){?>selected<?php }?>>Theme 26</option>
					<!-------------FULL SCREEN DESIGN TEMPLATES � STYLE 3 Ends--------------------------------------------------------->
					<!-------FULL SCREEN DESIGN TEMPLATES � STYLE 2 ----------------------------------------------------->
					<option value="Theme29-orange-home4" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Theme29-orange-home4'){?>selected<?php }?>>Theme 29</option>
					<option value="Theme31-green-home4"  <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Theme31-green-home4'){?>selected<?php }?>>Theme 31</option>
					<!----------FULL SCREEN DESIGN TEMPLATES � STYLE 2 ----------------------------------------------------->
					<!--
						<option value="Theme9-green-home1" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Theme9-green-home1'){?>selected<?php }?>>Theme 9</option>
						<option value="Theme10-orangel-home1" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Theme10-orangel-home1'){?>selected<?php }?>>Theme 10</option>
						<option value="Theme11-seagreen-home1" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Theme11-seagreen-home1'){?>selected<?php }?>>Theme 11</option>
					<option value="Theme12-orchid-home1" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Theme12-orchid-home1'){?>selected<?php }?>>Theme 12</option>-->
				</select>				
				<?php }else{ ?>
				<input type="text" id="<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
" name="Data[<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue'];?>
" lang="*" title="<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription'];?>
"/> <br />
				<?php }?>
			</p>
			<?php $_smarty_tpl->tpl_vars["currType"] = new Smarty_variable(($_smarty_tpl->getVariable('db_res')->value)."[i].eType", null, null);?>
            <?php endfor; endif; ?>
			<input type="submit" value="Save Settings" class="btn" title="Save Settings" onclick="return validate(document.frmadd);"/>
		</form>
	</div>
</div>

<script>
	function hidemessage(){
		jQuery("#errormsgdiv").slideUp();
	}
	
	function go_for_change(val){
		window.location="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=to-generalsettings&mode=edit&eType="+val;
		return false;
	}
	function confirm_delete(val)
	{
		//alert(val);
		ans = confirm('Are you sure for Delete Image?');
		if(ans == true)
		{
			document.frmadd.action.value = 'confirm_delete';
			document.frmadd.imagemode.value = val;
			document.frmadd.submit();
		}
		else
		{
			return false;
		}
	}
</script>
        