<?php /* Smarty version Smarty-3.0.7, created on 2016-01-07 19:37:57
         compiled from "/home/www/cargosharing1/bbcsadmin/templates/dashboard/view-dashboard.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1575807634568ebe95d3f9f6-18637835%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2515ddd381d39426e84b3e763bcc6d7d7344bd4c' => 
    array (
      0 => '/home/www/cargosharing1/bbcsadmin/templates/dashboard/view-dashboard.tpl',
      1 => 1452195473,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1575807634568ebe95d3f9f6-18637835',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="contentcontainer">
	<div class="headings altheading">
		<h2>Site Map</h2>
	</div>
	<div class="contentbox" style="border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
    margin-bottom: 15px;">
		<div>
			<?php if (in_array('ad-administrator',$_smarty_tpl->getVariable('modlist')->value)||in_array('ad-languagemaster',$_smarty_tpl->getVariable('modlist')->value)){?>
			<div class="ControlPanelLeft">
				<h1>Administrator</h1>
				<ul>
					<?php if (in_array('ad-administrator',$_smarty_tpl->getVariable('modlist')->value)){?>
					<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=ad-administrator&mode=view">Administrator</a></li>
					<?php }?>
					<?php if (in_array('ad-languagemaster',$_smarty_tpl->getVariable('modlist')->value)){?>
					<li><a class="last" ref="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=ad-languagemaster&mode=view">Language</a></li>
					<?php }?>
				</ul>
			</div> 
			<?php }?>
			<?php if (in_array('m-member',$_smarty_tpl->getVariable('modlist')->value)||in_array('m-member_messages',$_smarty_tpl->getVariable('modlist')->value)){?>
			<div class="ControlPanelLeft">
				<h1>Members Management</h1>
				<ul>
					<?php if (in_array('m-member',$_smarty_tpl->getVariable('modlist')->value)){?>
					<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=m-member&mode=view">Members List</a></li>
					<?php }?>
					<?php if (in_array('m-member_messages',$_smarty_tpl->getVariable('modlist')->value)){?>
					<li><a class="last" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=m-member_messages&mode=view">Message Records</a></li>
					<?php }?>
				</ul>
			</div> 
			<?php }?>
			<?php if (in_array('ri-rideslist',$_smarty_tpl->getVariable('modlist')->value)||in_array('bo-bookings',$_smarty_tpl->getVariable('modlist')->value)||in_array('ri-member_payment',$_smarty_tpl->getVariable('modlist')->value)){?>
			<div class="ControlPanelLeft">
				<h1>Trip Management</h1>
				<ul>
					<li><a class="last" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=ri-rideslist&mode=view&paymenttype=all">Trip List</a></li>
					<?php if (@PAYMENT_OPTION!='Contact'){?>
						<?php if (in_array('bo-bookings',$_smarty_tpl->getVariable('modlist')->value)){?>
					<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=bo-bookings&mode=view">Trip Booking</a></li>
					<?php }?>
					<?php if (in_array('ri-member_payment',$_smarty_tpl->getVariable('modlist')->value)){?>
					<?php if (@PAYMENT_OPTION=='PayPal'&&in_array('ri-member_payment',$_smarty_tpl->getVariable('modlist')->value)){?>
					<li><a class="last" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=ri-member_payment&mode=view">Pay To Traveler</a></li>
					<?php }?><?php }?>
					<?php }?>
				</ul>
			</div>
			<?php }?>
			
			
			
			<?php if (in_array('r-member_reports',$_smarty_tpl->getVariable('modlist')->value)||in_array('r-rides_reports',$_smarty_tpl->getVariable('modlist')->value)||in_array('r-payment_reports',$_smarty_tpl->getVariable('modlist')->value)||in_array('r-money_reports',$_smarty_tpl->getVariable('modlist')->value)){?>
			<div class="ControlPanelLeft">
				<h1>Report</h1>
				<ul>
					<?php if (in_array('r-member_reports',$_smarty_tpl->getVariable('modlist')->value)){?>
					<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=r-member_reports&mode=view">Members Report</a></li>
					<?php }?>
					<?php if (in_array('r-rides_reports',$_smarty_tpl->getVariable('modlist')->value)){?>
					<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=r-rides_reports&mode=view">Trip Posted Report</a></li>
					<?php }?>
					<?php if (in_array('r-money_reports',$_smarty_tpl->getVariable('modlist')->value)){?>
					<li><a class="last" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=r-money_reports&mode=view">Money Report</a></li>
					<?php }?>
					<?php if (in_array('r-payment_reports',$_smarty_tpl->getVariable('modlist')->value)){?>
					<li><a class="last" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=r-payment_reports&mode=view">Payment Report</a></li>
					<?php }?>
				</ul>
			</div>
			<?php }?>
			<?php if (in_array('m-member_stories',$_smarty_tpl->getVariable('modlist')->value)||in_array('u-media_partners',$_smarty_tpl->getVariable('modlist')->value)||in_array('u-email_templates',$_smarty_tpl->getVariable('modlist')->value)||in_array('u-faqcategory',$_smarty_tpl->getVariable('modlist')->value)||in_array('u-faq',$_smarty_tpl->getVariable('modlist')->value)){?>
			<div class="ControlPanelLeft">
				<h1>Utility</h1>
				<ul>
					<?php if (in_array('r-member_stories',$_smarty_tpl->getVariable('modlist')->value)){?>	
					<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=m-member_stories&mode=view">Members Stories</a></li>
					<?php }?>
					<?php if (in_array('u-media_partners',$_smarty_tpl->getVariable('modlist')->value)){?>
					<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-media_partners&mode=view"  title="">Media Partners</a></li>
					<?php }?>
					<?php if (in_array('u-email_templates',$_smarty_tpl->getVariable('modlist')->value)){?>
					<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-email_templates&mode=view"  title="">Email templates</a></li>
					<?php }?>
					<?php if (in_array('u-faqcategory',$_smarty_tpl->getVariable('modlist')->value)){?>
					<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-faqcategory&mode=view" title="">FAQ Category</a></li>
					<?php }?>
					<?php if (in_array('u-faq',$_smarty_tpl->getVariable('modlist')->value)){?>
					<li><a class="last" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-faq&mode=view" title="">FAQ</a></li>
					<?php }?>
					
				</ul>
			</div>
			<?php }?>
			<?php if (in_array('to-homebanners',$_smarty_tpl->getVariable('modlist')->value)||in_array('to-banners',$_smarty_tpl->getVariable('modlist')->value)||in_array('u-currency',$_smarty_tpl->getVariable('modlist')->value)||in_array('u-newsletter_subscriber',$_smarty_tpl->getVariable('modlist')->value)){?>
			<div class="ControlPanelLeft">
				<h1>Utility</h1>
				<ul>
					<?php if (in_array('to-homebanners',$_smarty_tpl->getVariable('modlist')->value)){?>
					<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=to-homebanners&mode=view">Home Banners</a></li> 		
					<?php }?>
					<?php if (in_array('to-banners',$_smarty_tpl->getVariable('modlist')->value)){?>
					<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=to-banners&mode=view">Adv. Banners</a></li> 		
					<?php }?>
					<?php if (in_array('u-currency',$_smarty_tpl->getVariable('modlist')->value)){?>
					<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-currency&mode=view">Currency Rates</a></li>
					<?php }?>
					<?php if (in_array('u-newsletter_subscriber',$_smarty_tpl->getVariable('modlist')->value)){?>
					<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-newsletter_subscriber&mode=view">Newsletter Subscriber</a></li> 				 							
					<?php }?>
					<!---<?php if (in_array('u-preferences',$_smarty_tpl->getVariable('modlist')->value)){?>
						<li><a class="last" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-preferences&mode=view">Manage Preferences</a></li>
					<?php }?>--->
				</ul>
			</div>
			<?php }?>
			<?php if (in_array('l-country',$_smarty_tpl->getVariable('modlist')->value)||in_array('l-state',$_smarty_tpl->getVariable('modlist')->value)||in_array('l-city',$_smarty_tpl->getVariable('modlist')->value)){?>
			<div class="ControlPanelLeft">
				<h1>Localization</h1>
				<ul>
					<?php if (in_array('l-country',$_smarty_tpl->getVariable('modlist')->value)){?>
					<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=l-country&mode=view">Country</a></li>
					<?php }?>
					<?php if (in_array('l-state',$_smarty_tpl->getVariable('modlist')->value)){?>
					<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=l-state&mode=view">Province</a></li>
					<?php }?>
					<?php if (in_array('l-city',$_smarty_tpl->getVariable('modlist')->value)){?> 
					<li><a class="last" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=l-city&mode=view">City</a></li>
					<?php }?>
				</ul>
			</div>
			<?php }?>
			<?php if (in_array('to-generalsettings',$_smarty_tpl->getVariable('modlist')->value)||in_array('to-languagelabel',$_smarty_tpl->getVariable('modlist')->value)||in_array('pg-staticpages',$_smarty_tpl->getVariable('modlist')->value)){?>
			<div class="ControlPanelLeft">
				<h1>Settings</h1>
				<ul>
					<?php if (in_array('to-generalsettings',$_smarty_tpl->getVariable('modlist')->value)){?>
					<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=to-generalsettings&mode=edit">General Configuration</a></li>
					<?php }?>
					<?php if (in_array('to-languagelabel',$_smarty_tpl->getVariable('modlist')->value)){?>
					<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=to-languagelabel&mode=view">Language Label</a></li>
					<?php }?>
					<?php if (in_array('pg-staticpages',$_smarty_tpl->getVariable('modlist')->value)){?>
					<li><a class="last" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=pg-staticpages&mode=view">Static Pages</a></li>
					<?php }?>
				</ul>
			</div>
			<?php }?>
			<div style=" clear:both"></div>
		</div>
		<br><br><br>
		<div style="clear:both"></div>
	</div>  
</div>
<div class="contentcontainer">
	<div class="headings altheading">
		<h2>Update statistics</h2>
	</div>
	<div class="contentbox">
		<table width="100%">
			<thead>
				<!--
					<tr>
					<th width="33%">Name</th>
					<th width="33%">Email</th>
					<th width="33%">Registration Date</th>
					</tr>
				-->
			</thead>
			<tbody>
				<tr class="alt">
					<td width="20%">Total Admin</td>
					<td width="1%">:</td>
					<td width="79%"><?php echo $_smarty_tpl->getVariable('totadmin')->value;?>
</td>
				</tr>			
				<tr class="alt">
					<td>Total Active Members </td>
					<td>:</td>
					<td><?php echo $_smarty_tpl->getVariable('totstudent')->value;?>
</td>
				</tr>
				<tr class="alt">
					<td>Total Rides Offered </td>
					<td>:</td>
					<td><?php echo $_smarty_tpl->getVariable('totevent')->value;?>
</td>
				</tr>
				
				
			</tbody>
		</table>
		<div style="clear: both;"></div>
	</div>
</div>




