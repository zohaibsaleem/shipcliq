<?php /* Smarty version Smarty-3.0.7, created on 2016-01-09 14:08:33
         compiled from "/home/www/cargosharing1/bbcsadmin/templates/rides/rideslist.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1880736741569114618aa889-37714176%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2760027e8180b969aa39f4d549cdda7c7bc4fdfe' => 
    array (
      0 => '/home/www/cargosharing1/bbcsadmin/templates/rides/rideslist.tpl',
      1 => 1432640425,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1880736741569114618aa889-37714176',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="contentcontainer" id="tabs">
	<div class="headings">
    	<?php if ($_smarty_tpl->getVariable('mode')->value=='edit'){?>
			<h2 class="left">Rides Information</h2>
        <?php }else{ ?>
			<h2 class="left">Rides Information</h2>
        <?php }?>
	</div>
	<div class="contentbox" id="tabs-1">  
<?php if ($_smarty_tpl->getVariable('var_msg')->value!=''){?>
		
<div class="status success" id="errormsgdiv"> 
<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_success.png" title="Success" />
<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
</p> 
</div> 
<?php }?> 

<?php if ($_smarty_tpl->getVariable('var_err_msg')->value!=''){?>
<div class="status error" id="errormsgdiv"> 
<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_error.png" title="Error" />
<?php echo $_smarty_tpl->getVariable('var_err_msg')->value;?>
</p> 
</div> 
<?php }?>
 
<table class="headingbg" style="border: 1px solid #ccc;" border="0" cellpadding="1" cellspacing="1" width="100%">
<input type="hidden" name="action" id="action" value="" />
<input  type="hidden" name="iRideId" value=""/>
<thead>
<tr>
		<th style="font-size:12px;text-align:left;" width="80%">Ride Details</th> 
    <?php if (@PAYMENT_OPTION!='Contact'){?>
    <th style="font-size:12px;text-align:center;" width="10%">Payment Due</th> 
    <?php }?>             
    <th style="font-size:12px;text-align:center;" width="10%">Status</th>
</tr> 
</thead>   
<tbody>
<?php if (count($_smarty_tpl->getVariable('db_records_all')->value)>0){?>  
<tr style="background:<?php echo $_smarty_tpl->getVariable('db_records_all')->value[0]['color'];?>
">
	<td width="80%" >
    <table width="100%" style="border:1px solid #cccccc;">
    <tr>
      <td style="color: #05A2DB;font-size:15px;">
        <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=ri-rideslist&mode=edit&iRideId=<?php echo $_smarty_tpl->getVariable('db_records_all')->value[0]['iRideId'];?>
" style="color: #05A2DB;font-size:15px;"><strong>Ride of:</strong> <?php echo $_smarty_tpl->getVariable('db_records_all')->value[0]['vMainDeparture'];?>
 - <?php echo $_smarty_tpl->getVariable('db_records_all')->value[0]['vMainArrival'];?>
</a>
      </td>         
    </tr>
    <tr>
      <td style="color:#7C7C7C;font-size:14px;">
        <strong>Offered By:</strong> <?php echo $_smarty_tpl->getVariable('db_records_all')->value[0]['vFirstName'];?>
&nbsp;<?php echo $_smarty_tpl->getVariable('db_records_all')->value[0]['vLastName'];?>

        &nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp;
        <!--<strong>Price:</strong> <?php echo $_smarty_tpl->getVariable('db_records_all')->value[0]['vBookerCurrencyCode'];?>
&nbsp;<?php echo $_smarty_tpl->getVariable('db_records_all')->value[0]['price'];?>

        &nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp;   -->
        <strong>Type:</strong> <?php echo $_smarty_tpl->getVariable('db_records_all')->value[0]['eRideType'];?>

        &nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp;
        <!-- <strong>Seats:</strong> <?php echo $_smarty_tpl->getVariable('db_records_all')->value[0]['iSeats'];?>

        &nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp; -->
        <strong>Add Date:</strong> <?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_records_all')->value[0]['dAddedDate'],9);?>

      </td>
    </tr>
    </table>
  </td>
  <?php if (@PAYMENT_OPTION!='Contact'){?>
  <td width="10%">
    <table width="100%" style="border:1px solid #cccccc;">
    <tr>
      <td style="text-align:center;">
        <strong><?php echo $_smarty_tpl->getVariable('db_records_all')->value[0]['paymentdue'];?>
</strong>
      </td>         
    </tr>
    <tr><td style="text-align:center;"><?php echo $_smarty_tpl->getVariable('db_records_all')->value[0]['todaybooking'];?>
&nbsp;<?php echo $_smarty_tpl->getVariable('db_records_all')->value[0]['pastbooking'];?>
</td></tr>
    </table>
  </td>
  <?php }?>
  <td width="10%">
    <table width="100%" style="border:1px solid #cccccc;">
    <tr>
      <td style="text-align:center;">
        <strong><?php echo $_smarty_tpl->getVariable('db_records_all')->value[0]['eStatus'];?>
</strong>
      </td>         
    </tr>
    <tr><td>&nbsp;</td></tr>
    </table>
  </td>   
</tr>    
<?php }else{ ?>
<tr>
	<td height="70px;" colspan="12" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No Record Found.</td>
</tr>
<?php }?>
</tbody>
</table>
<table style="border: 1px solid #ccc;" border="0" cellpadding="1" cellspacing="1" width="100%">
  <tr>
    <th>
      Further Details
    </th>
  </tr>
  <tr>
    <td>
      <table width="100%" style="border:1px solid #cccccc;">
        <tr>
          <td>
            <strong>Description :</strong> <?php echo $_smarty_tpl->getVariable('db_records_all')->value[0]['tDetails'];?>

            <br><br>
            <strong>Leave on :</strong> <?php echo $_smarty_tpl->getVariable('db_records_all')->value[0]['eLeaveTime'];?>

            <?php if ($_smarty_tpl->getVariable('doc')->value=="Yes"){?>
              &nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp;
              <strong>Document:</strong> <?php echo $_smarty_tpl->getVariable('docprice')->value;?>
 (<?php echo $_smarty_tpl->getVariable('docweight')->value;?>
)
            <?php }?>
            <?php if ($_smarty_tpl->getVariable('box')->value=="Yes"){?>
              &nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp;
              <strong>Box:</strong> <?php echo $_smarty_tpl->getVariable('boxprice')->value;?>
 (<?php echo $_smarty_tpl->getVariable('boxweight')->value;?>
)
            <?php }?>
            <?php if ($_smarty_tpl->getVariable('lug')->value=="Yes"){?>
              &nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp;
              <strong>Luggage:</strong> <?php echo $_smarty_tpl->getVariable('lugprice')->value;?>
 (<?php echo $_smarty_tpl->getVariable('lugweight')->value;?>
)
            <?php }?>
          </td>
        </tr>       
    </td>
  </tr>
</table>
<br>
<table class="headingbg" style="border: 1px solid #ccc;" border="0" cellpadding="1" cellspacing="1" width="100%">
<tr>
  <th colspan="6">
    Ride Dates and Booking Details
  </th> 
</tr>
<tr>
  <th>
    Outbound Dates
  </th>
  <?php if (@PAYMENT_OPTION!='Contact'){?>
  <th>
    View Bookings
  </th>   
  <th>
    Traveler Payment
  </th>
  <?php }?> 
  <th>
    Return Dates
  </th>
  <?php if (@PAYMENT_OPTION!='Contact'){?>
  <th>
    View Bookings
  </th> 
   <th>
    Traveler Payment
  </th>
  <?php }?>
</tr> 
<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['name'] = "dates";
$_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_dates')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["dates"]['total']);
?> 
<tr>
  <td style="background:<?php echo $_smarty_tpl->getVariable('db_dates')->value[$_smarty_tpl->getVariable('smarty')->value['section']['dates']['index']]['colorout'];?>
">
    <?php echo $_smarty_tpl->getVariable('db_dates')->value[$_smarty_tpl->getVariable('smarty')->value['section']['dates']['index']]['dDateOut'];?>

  </td>  
  <?php if (@PAYMENT_OPTION!='Contact'){?>
  <td style="background:<?php echo $_smarty_tpl->getVariable('db_dates')->value[$_smarty_tpl->getVariable('smarty')->value['section']['dates']['index']]['colorout'];?>
">
    <?php if ($_smarty_tpl->getVariable('db_dates')->value[$_smarty_tpl->getVariable('smarty')->value['section']['dates']['index']]['totbookingsout']!='No Bookings'){?>
    <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_panel'];?>
index.php?file=ri-ridepayment&mode=edit&iRideId=<?php echo $_smarty_tpl->getVariable('iRideId')->value;?>
&dateout=<?php echo $_smarty_tpl->getVariable('db_dates')->value[$_smarty_tpl->getVariable('smarty')->value['section']['dates']['index']]['dDateOut'];?>
"><?php echo $_smarty_tpl->getVariable('db_dates')->value[$_smarty_tpl->getVariable('smarty')->value['section']['dates']['index']]['totbookingsout'];?>
 Booking(s) | View / Driver Payment</a>
    <?php }else{ ?>
    <?php echo $_smarty_tpl->getVariable('db_dates')->value[$_smarty_tpl->getVariable('smarty')->value['section']['dates']['index']]['totbookingsout'];?>

    <?php }?>
  </td> 
  <td style="background:<?php echo $_smarty_tpl->getVariable('db_dates')->value[$_smarty_tpl->getVariable('smarty')->value['section']['dates']['index']]['colorout'];?>
">
    <?php if ($_smarty_tpl->getVariable('db_dates')->value[$_smarty_tpl->getVariable('smarty')->value['section']['dates']['index']]['eDateOutPaid']=='Yes'){?>
    Paid
    <?php }else{ ?>
    Unpaid
    <?php }?>
  </td> 
  <?php }?>
  <td style="background:<?php echo $_smarty_tpl->getVariable('db_dates')->value[$_smarty_tpl->getVariable('smarty')->value['section']['dates']['index']]['colorret'];?>
">
    <?php echo $_smarty_tpl->getVariable('db_dates')->value[$_smarty_tpl->getVariable('smarty')->value['section']['dates']['index']]['dDateRet'];?>

  </td>
  <?php if (@PAYMENT_OPTION!='Contact'){?>
  <td style="background:<?php echo $_smarty_tpl->getVariable('db_dates')->value[$_smarty_tpl->getVariable('smarty')->value['section']['dates']['index']]['colorret'];?>
">
    <?php if ($_smarty_tpl->getVariable('db_dates')->value[$_smarty_tpl->getVariable('smarty')->value['section']['dates']['index']]['totbookingsret']!='No Bookings'){?>
    <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_panel'];?>
index.php?file=ri-ridepayment&mode=edit&iRideId=<?php echo $_smarty_tpl->getVariable('iRideId')->value;?>
&dDateRet=<?php echo $_smarty_tpl->getVariable('db_dates')->value[$_smarty_tpl->getVariable('smarty')->value['section']['dates']['index']]['dDateRet'];?>
"><?php echo $_smarty_tpl->getVariable('db_dates')->value[$_smarty_tpl->getVariable('smarty')->value['section']['dates']['index']]['totbookingsret'];?>
 Booking(s) | View / Driver Payment</a>
    <?php }else{ ?>
    <?php echo $_smarty_tpl->getVariable('db_dates')->value[$_smarty_tpl->getVariable('smarty')->value['section']['dates']['index']]['totbookingsret'];?>

    <?php }?>
  </td>
  <td style="background:<?php echo $_smarty_tpl->getVariable('db_dates')->value[$_smarty_tpl->getVariable('smarty')->value['section']['dates']['index']]['colorret'];?>
">
   <?php if ($_smarty_tpl->getVariable('db_dates')->value[$_smarty_tpl->getVariable('smarty')->value['section']['dates']['index']]['eDateRetPaid']=='Yes'){?>
    Paid
    <?php }else{ ?>
    Unpaid
    <?php }?>
  </td> 
  <?php }?> 
</tr>  
<?php endfor; endif; ?>
</table>
</div>
</div>  

<script>
function backtolist(){
  window.location="index.php?file=ri-rideslist&mode=view";
}
function edit_dealstatus(){
   document.frmadd.submit();
} 

function calculate_again(id){
  if($("#confirm_"+id).is(':checked')){
    var r=confirm("Are you sure want to add this booking from traveler payment?");
    if (r==true)
    {
       window.location="index.php?file=ri-rideslist&mode=edit&iRideId=<?php echo $_smarty_tpl->getVariable('iRideId')->value;?>
&action=add&iBookingId="+id;
    }else{
       return false;
    } 
  }else{
    var r=confirm("Are you sure want to remove this booking from traveler payment?");
    if (r==true)
    {
       window.location="index.php?file=ri-rideslist&mode=edit&iRideId=<?php echo $_smarty_tpl->getVariable('iRideId')->value;?>
&action=remove&iBookingId="+id;
    }else{
       return false;
    } 
  }

}

function check_payment(){
  var r=confirm("Are you sure want to payment to traveler?");
  if (r==true)
  {
    document.frmpaymenr.submit();
  }else{
    return false;
  } 
} 
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
