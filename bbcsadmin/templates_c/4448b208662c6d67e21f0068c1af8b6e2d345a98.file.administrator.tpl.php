<?php /* Smarty version Smarty-3.0.7, created on 2015-05-25 16:15:54
         compiled from "/home/www/xfetch/xfadmin/templates/administrator/administrator.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16229164425562fd6253e754-35267481%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4448b208662c6d67e21f0068c1af8b6e2d345a98' => 
    array (
      0 => '/home/www/xfetch/xfadmin/templates/administrator/administrator.tpl',
      1 => 1432032808,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16229164425562fd6253e754-35267481',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<script language="JavaScript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tcp_javascript'];?>
validate.js"></script>
<div class="contentcontainer" id="tabs">
	<div class="headings">
		<?php if ($_smarty_tpl->getVariable('mode')->value=='edit'){?>
		<h2 class="left">Edit Administrator</h2>
    <?php }else{ ?>
		<h2 class="left">Add Administrator</h2>
    <?php }?>
	</div>
	<div class="contentbox" id="tabs-1">
            <?php if ($_smarty_tpl->getVariable('var_msg')->value!=''){?>
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_error.png" title="Success" />
					<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
</p> 
				</div>     
				<div></div>
			<?php }?>
			<?php if ($_smarty_tpl->getVariable('var_msg_error')->value!=''){?>
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_error.png" title="Success" />
					<?php echo $_smarty_tpl->getVariable('var_msg_error')->value;?>
</p> 
				</div>     
				<div></div>
			<?php }?>
			<form id="frmadd" name="frmadd" action="index.php?file=ad-administrator_a" method="post">
            <input type="hidden" name="iAdminId" id="iAdminId" value="<?php echo $_smarty_tpl->getVariable('iAdminId')->value;?>
" />
            <input type="hidden" name="action" id="action" value="<?php echo $_smarty_tpl->getVariable('mode')->value;?>
" />
       	<p>
					<label for="textfield"><strong>First Name :</strong></label>
					<input type="text" id="vLastName" name="Data[vFirstName]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_admin')->value[0]['vFirstName'];?>
" lang="*" title="First Name"/>
				</p>
				<p>
					<label for="textfield"><strong>Last Name :</strong></label>
					<input type="text" id="vLastName" name="Data[vLastName]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_admin')->value[0]['vLastName'];?>
" lang="*" title="Last Name"/>
				</p>
			  <p>
					<label for="textfield"><strong>Contact No :</strong></label>
					<input type="text" id="vContactNo" name="Data[vContactNo]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_admin')->value[0]['vContactNo'];?>
" lang="*{T}" title="Contact No"/>
				</p>
                
				<p>
					<label for="textfield"><strong>E-mail :</strong></label>
					<input type="text" id="vEmail"  name="Data[vEmail]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_admin')->value[0]['vEmail'];?>
"  lang="*{E}" title="Email" />
				</p>
				<p>
					<label for="textfield"><strong>User Name :</strong></label>
					<input type="text" id="vUserName" name="Data[vUserName]" class="inputbox" lang="*{P}6:0" title="User Name" value="<?php echo $_smarty_tpl->getVariable('db_admin')->value[0]['vUserName'];?>
"/>
				</p>
				<p>
					<label for="textfield"><strong>Password :<em>*</em></strong></label>
					<input type="password" id="vPassword"  name="Data[vPassword]" class="inputbox" lang="*{P}6:0" title="Password" value="<?php echo $_smarty_tpl->getVariable('db_admin')->value[0]['vPassword'];?>
"/>
				</p>
				<?php if ($_smarty_tpl->getVariable('db_admin')->value[0]['vUserName']!="superadmin"){?>
				<p>
					<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" <?php if ($_smarty_tpl->getVariable('db_admin')->value[0]['eStatus']=='Active'){?>selected<?php }?>>Active</option>
						<option value="Inactive" <?php if ($_smarty_tpl->getVariable('db_admin')->value[0]['eStatus']=='Inactive'){?>selected<?php }?>>Inactive</option>
					</select>
				</p>
				 <p>
					<label for="textfield"><strong>Grant Access to :</strong></label>
				  <?php echo $_smarty_tpl->getVariable('strmodule')->value;?>

				</p>
				<?php }?>
				
				<?php if ($_smarty_tpl->getVariable('mode')->value=='edit'){?>
				<input type="submit" value="Edit Administrator" class="btn" onclick="return validate(document.frmadd);" title="Edit Administrator"/>
				      <?php }else{ ?>
   				<input type="submit" value="Add Administrator" class="btn" onclick="return validate(document.frmadd);" title="Add Administrator"/>
  				
				      
				      <?php }?>
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
			</form>
	</div>
</div>

<script>

function redirectcancel()
{
    window.location="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=ad-administrator&mode=view";
    return false;
}

function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
