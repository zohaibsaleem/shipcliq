<?php /* Smarty version Smarty-3.0.7, created on 2015-04-30 16:04:36
         compiled from "/home/www/blablaclone/bcadmin/templates/utility/faqcategory.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12033539015542053c36f422-28438111%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '572af5110f03c24bc053b27b4ac9f13254955c57' => 
    array (
      0 => '/home/www/blablaclone/bcadmin/templates/utility/faqcategory.tpl',
      1 => 1401263603,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12033539015542053c36f422-28438111',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="contentcontainer">
<div class="headings">
	<h2 class="left">Add Faq Category
    </h2>
</div>
<div class="contentbox" id="tabs-1">
    <?php if ($_smarty_tpl->getVariable('var_msg')->value!=''){?>
     <div class="status error" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_error.png" title="Error" />
              <?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
</p> 
     </div>     
    <div></div>
    <?php }?> 
		<form id="frmadd" name="frmadd" action="index.php?file=u-faqcategory_a" method="post">
        <input type="hidden" name="iFaqcategoryId" id="iFaqcategoryId" value="<?php echo $_smarty_tpl->getVariable('iFaqcategoryId')->value;?>
" />
        <input type="hidden" name="action" id="action" value="<?php echo $_smarty_tpl->getVariable('mode')->value;?>
" />
      <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_languagemas')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>              
			<p>
				<label for="textfield"><strong>Title [ <?php echo $_smarty_tpl->getVariable('vLanguage')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
 ]:<em>*</em></strong></label>
				<input type="text" id="<?php echo $_smarty_tpl->getVariable('vLanCode')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" name="Data[<?php echo $_smarty_tpl->getVariable('vLanCode')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_faq_cat')->value[0][$_smarty_tpl->getVariable('vLanCode')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]];?>
" lang="*" title="Title <?php echo $_smarty_tpl->getVariable('vLanguage')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"/>
        
			</p>
    <?php endfor; endif; ?>  
		<!--	<p>
				<label for="textfield"><strong>Title [English] :</strong></label>
				<input type="text" id="vTitle_EN" name="Data[vTitle_EN]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_faq_cat')->value[0]['vTitle_EN'];?>
" lang="*" title="Title English"/>
			</p>
      <p>
				<label for="textfield"><strong>Title [Russian] :</strong></label>
				<input type="text" id="vTitle_RS" name="Data[vTitle_RS]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_faq_cat')->value[0]['vTitle_RS'];?>
" title="Title Russian"/>
			</p>
			<p>
				<label for="textfield"><strong>Title [Latvian] :</strong></label>
				<input type="text" id="vTitle_LV" name="Data[vTitle_LV]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_faq_cat')->value[0]['vTitle_LV'];?>
" title="Title Latvian"/>
			</p>
			<p>
				<label for="textfield"><strong>Title [Lithuanian] :</strong></label>
				<input type="text" id="vTitle_LT" name="Data[vTitle_LT]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_faq_cat')->value[0]['vTitle_LT'];?>
" title="Title Lithuanian"/>
			</p>
			<p>
				<label for="textfield"><strong>Title [Estonian] :</strong></label>
				<input type="text" id="vTitle_EE" name="Data[vTitle_EE]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_faq_cat')->value[0]['vTitle_EE'];?>
" title="Title Estonian"/>
			</p>
      <p>
				<label for="textfield"><strong>Title [German] :</strong></label>
				<input type="text" id="vTitle_DE" name="Data[vTitle_DE]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_faq_cat')->value[0]['vTitle_DE'];?>
" title="Title German"/>
			</p>
      <p>
				<label for="textfield"><strong>Title [French] :</strong></label>
				<input type="text" id="vTitle_FN" name="Data[vTitle_FN]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_faq_cat')->value[0]['vTitle_FN'];?>
" title="Title French"/>
			</p>
      
      <p>
				<label for="textfield"><strong>Title [Spanish] :</strong></label>
				<input type="text" id="vTitle_ES" name="Data[vTitle_ES]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_faq_cat')->value[0]['vTitle_ES'];?>
" title="Title Spanish"/>
			</p>
      <p>
				<label for="textfield"><strong>Title [Norwegian] :</strong></label>
				<input type="text" id="vTitle_NO" name="Data[vTitle_NO]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_faq_cat')->value[0]['vTitle_NO'];?>
" title="Title Norwagina"/>
			</p>
      <p>
				<label for="textfield"><strong>Title [Polish] :</strong></label>
				<input type="text" id="vTitle_PO" name="Data[vTitle_PO]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_faq_cat')->value[0]['vTitle_PO'];?>
" title="Title Polish"/>
			</p>
      
      <p>
				<label for="textfield"><strong>Title [Finnish] :</strong></label>
				<input type="text" id="vTitle_FI" name="Data[vTitle_FI]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_faq_cat')->value[0]['vTitle_FI'];?>
" title="Title Polish"/>
			</p>
      <p>
				<label for="textfield"><strong>Title [Swedish] :</strong></label>
				<input type="text" id="vTitle_SW" name="Data[vTitle_SW]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_faq_cat')->value[0]['vTitle_SW'];?>
" title="Title Swedish"/>
			</p>
      <p>
				<label for="textfield"><strong>Title [Danish] :</strong></label>
				<input type="text" id="vTitle_DN" name="Data[vTitle_DN]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_faq_cat')->value[0]['vTitle_DN'];?>
"  title="Title Danish"/>
			</p>-->
      
      <p>        			
					<label for="textfield"><strong>Display Order :</strong></label>
					<select id="iDisplayOrder" name="Data[iDisplayOrder]" lang="" title="Category Display Order" />
					<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('num_rows')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
					 <option <?php if ($_smarty_tpl->getVariable('db_faq_cat')->value[0]['iDisplayOrder']==$_smarty_tpl->getVariable('smarty')->value['section']['i']['index_next']){?> selected <?php }?> value="<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index_next'];?>
"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index_next'];?>
</option>
					<?php endfor; endif; ?>
					</select>
				</p>			
			<p>
				<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]">
					<option value="Active" <?php if ($_smarty_tpl->getVariable('db_faq_cat')->value[0]['eStatus']=='Active'){?>selected<?php }?> >Active</option>
					<option value="Inactive" <?php if ($_smarty_tpl->getVariable('db_faq_cat')->value[0]['eStatus']=='Inactive'){?>selected<?php }?> >Inactive</option>
				</select>
			</p>
		<?php if ($_smarty_tpl->getVariable('mode')->value=='edit'){?>
      <input type="submit" value="Edit Faq Category" class="btn" title="Edit Faq Category" onclick="return validate(document.frmadd);"/>
    <?php }else{ ?>	
      <input type="submit" value="Add Faq Category" class="btn" title="Add Faq Category" onclick="return validate(document.frmadd);"/>
		<?php }?>	
      <input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
		</form>
</div>
</div>

<script>
function redirectcancel(){
    window.location="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-faqcategory&mode=view";
    return false;
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>

