<?php /* Smarty version Smarty-3.0.7, created on 2016-02-01 23:15:03
         compiled from "/home4/shipcliq/public_html/bbcsadmin/templates/utility/view-currency.tpl" */ ?>
<?php /*%%SmartyHeaderCode:33341055656b03b5781f260-94707825%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5b6edb62306696c552b378d3676b761109f89d20' => 
    array (
      0 => '/home4/shipcliq/public_html/bbcsadmin/templates/utility/view-currency.tpl',
      1 => 1453900253,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '33341055656b03b5781f260-94707825',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<script language="JavaScript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tcp_javascript'];?>
jlist.js"></script>



<div class="contentcontainer">
	<div class="headings altheading">
		<h2>Currency Rates</h2>
	</div>
	<div class="contentbox">
    <?php if ($_smarty_tpl->getVariable('var_msg_new')->value!=''){?>
     <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_success.png" title="Success" />
              <?php echo $_smarty_tpl->getVariable('var_msg_new')->value;?>
</p> 
     </div>     
    <div></div>
    <?php }elseif($_smarty_tpl->getVariable('var_msg')->value!=''){?>
    <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_success.png" title="Success" />
              <?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
</p> 
     </div>     
    <div></div>
    <?php }?>
		
  <form name="frmlist" id="frmlist"  action="index.php?file=u-currency_a" method="post">           
		<table width="100%" border="0">
	
    <thead>
			<tr>
				<th>Currency</th>
        <th>Front Status</th>
        <th>USD Ratio</th>
        <th>Symbol</th>
		<th>Display Order</th>
        <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_curr')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
        <th><?php echo $_smarty_tpl->getVariable('db_curr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
</th>
        <?php endfor; endif; ?>
			</tr>
		</thead>
		<tbody>

      <?php if (count($_smarty_tpl->getVariable('db_curr')->value)>0){?>
		 <?php  $_smarty_tpl->tpl_vars['v1'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k1'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('db_curr')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['v1']->key => $_smarty_tpl->tpl_vars['v1']->value){
 $_smarty_tpl->tpl_vars['k1']->value = $_smarty_tpl->tpl_vars['v1']->key;
?>
      <tr>
        <td ><?php echo $_smarty_tpl->getVariable('db_curr')->value[$_smarty_tpl->tpl_vars['k1']->value]['vName'];?>
</td>
      <td >
	  <?php echo $_smarty_tpl->getVariable('db_curr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eFrontStatus'];?>

	  <select name="eFrontStatus[]" id="eFrontStatus<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index'];?>
" class="inputbox" lang="*"   style="width:100px;"> 
	  <option value='Yes' <?php if ($_smarty_tpl->getVariable('db_curr')->value[$_smarty_tpl->tpl_vars['k1']->value]['eFrontStatus']=='Yes'){?> selected<?php }?> >Yes</option>
	  <option value='No' <?php if ($_smarty_tpl->getVariable('db_curr')->value[$_smarty_tpl->tpl_vars['k1']->value]['eFrontStatus']=='No'){?> selected<?php }?>>No</option>
	  </select>
	  </td>
		<td ><input type="text" name="Ratio[]" id="Ratio<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index'];?>
" class="inputbox" style="width:100px;"  value="<?php echo $_smarty_tpl->getVariable('db_curr')->value[$_smarty_tpl->tpl_vars['k1']->value]['Ratio'];?>
" lang="*{N}" title="Ratio"> </td>
        <td><input type="text" name="vSymbole[]"  class="inputbox"  value="<?php echo $_smarty_tpl->getVariable('db_curr')->value[$_smarty_tpl->tpl_vars['k1']->value]['vSymbole'];?>
" lang="*" title="<?php echo $_smarty_tpl->getVariable('db_curr')->value[$_smarty_tpl->tpl_vars['k1']->value]['vSymbole'];?>
" style="width:100px;"></td>
        <td><input type="text" name="iDispOrder[]" class="inputbox"  value="<?php echo $_smarty_tpl->getVariable('db_curr')->value[$_smarty_tpl->tpl_vars['k1']->value]['iDispOrder'];?>
" lang="*{N}-{0}" title="Dispaly Order" style="width:100px;" onChange="disporder(this,<?php echo $_smarty_tpl->getVariable('db_count')->value;?>
,<?php echo $_smarty_tpl->tpl_vars['k1']->value;?>
);" id="iDispOrder[<?php echo $_smarty_tpl->tpl_vars['k1']->value;?>
]"></td>
        <?php  $_smarty_tpl->tpl_vars['v2'] = new Smarty_Variable;
 $_smarty_tpl->tpl_vars['k2'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('db_name')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['v2']->key => $_smarty_tpl->tpl_vars['v2']->value){
 $_smarty_tpl->tpl_vars['k2']->value = $_smarty_tpl->tpl_vars['v2']->key;
?>
           <td>
      <!-- <input type="text" name="<?php echo $_smarty_tpl->tpl_vars['v2']->value;?>
[]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_curr')->value[$_smarty_tpl->tpl_vars['k1']->value][$_smarty_tpl->getVariable('v2')->value];?>
" lang="*{N}" style="width:100px;" onkeyup="numericFilter(this)" <?php if ($_smarty_tpl->getVariable('db_curr')->value[$_smarty_tpl->tpl_vars['k2']->value]['vName']==$_smarty_tpl->getVariable('db_curr')->value[$_smarty_tpl->tpl_vars['k1']->value]['vName']){?> readonly <?php }?> onChange="validzero(this);" />       -->
	  <?php if ($_smarty_tpl->getVariable('db_curr')->value[$_smarty_tpl->tpl_vars['k2']->value]['vName']==$_smarty_tpl->getVariable('db_curr')->value[$_smarty_tpl->tpl_vars['k1']->value]['vName']){?> 
          <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['v2']->value;?>
[]" id="<?php echo $_smarty_tpl->tpl_vars['k1']->value;?>
<?php echo $_smarty_tpl->tpl_vars['k2']->value;?>
" value="<?php echo $_smarty_tpl->getVariable('db_curr')->value[$_smarty_tpl->tpl_vars['k1']->value][$_smarty_tpl->getVariable('v2')->value];?>
" />
          <?php echo $_smarty_tpl->getVariable('db_curr')->value[$_smarty_tpl->tpl_vars['k1']->value][$_smarty_tpl->getVariable('v2')->value];?>

         <?php }?>   
        <?php if ($_smarty_tpl->getVariable('db_curr')->value[$_smarty_tpl->tpl_vars['k2']->value]['vName']!=$_smarty_tpl->getVariable('db_curr')->value[$_smarty_tpl->tpl_vars['k1']->value]['vName']){?>   
       <input type="text" name="<?php echo $_smarty_tpl->tpl_vars['v2']->value;?>
[]" id="<?php echo $_smarty_tpl->tpl_vars['k1']->value;?>
<?php echo $_smarty_tpl->tpl_vars['k2']->value;?>
" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_curr')->value[$_smarty_tpl->tpl_vars['k1']->value][$_smarty_tpl->getVariable('v2')->value];?>
" lang="*{N}" style="width:100px;" onkeyup="numericFilter(this)" onChange="validzero(this,<?php echo $_smarty_tpl->tpl_vars['k1']->value;?>
<?php echo $_smarty_tpl->tpl_vars['k2']->value;?>
);" />
         <?php }?>
      </td>
         <?php }} ?>
      <?php }} ?>
	    </tr>  	    
   
      <tr><td></td><td ><input class="btn" type="submit" title="Edit Currency" onclick="return validate(document.frmlist);" value="Edit Currency"></td><tr>
      <?php }else{ ?>
      <tr>
			<td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No Record Found.</td>
		  </tr>
      
        <?php }?>
      
      
		</tbody>
		</table>
    </form>         
		<div style="clear: both;"></div></div>
</div>

<script>
function Searchoption(){
    document.getElementById('frmsearch').submit();
}
function numericFilter(txb) {
//alert(txb.value);
txb.value = txb.value.replace(/[^0-9.]/g,"");
// txb = parseFloat((txb + "").replace(/^(.*\.\d\d)\d*$/, '$1'));

   return txb > 0 ;
}
function disporder(num,len,r)
{
    var n1=num.value;
    var count=0;
   
    var arr=[];
     for(k=0;k<len;k++)
    {
      arr[k]=document.getElementById('iDispOrder[' + k + ']').value;
       //alert(arr[k]);
       if(arr[k]==n1)
      {
         count++;
          if(count>1)
         {
            document.getElementById('iDispOrder[' + r + ']').value='';
         }
      }            
   }
  
 
    
}
function validzero(num)
{
    var n1=num.value;
    if(num<=0)
    {
    alert("Number should be greater than 0");

    }
}

function AlphaSearch(val){
    var alphavalue = val;
    var file = 'pr-currency';
    window.location="index.php?file="+file+"&alp="+alphavalue+"&mode=view";
    return false;
}
function MakeAction(loopid,type){
    document.frmlist.iLanguageMasId.value = loopid;
    document.frmlist.action.value = type;
	document.frmlist.submit();	
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
