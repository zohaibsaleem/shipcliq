<?php /* Smarty version Smarty-3.0.7, created on 2015-12-22 19:27:53
         compiled from "/home/www/cargosharing1/bbcsadmin/templates/utility/media_partners.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5560354005679a439808fd2-73072487%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6f752d5490a8e5a1ccb502cb08039b88cc7360a5' => 
    array (
      0 => '/home/www/cargosharing1/bbcsadmin/templates/utility/media_partners.tpl',
      1 => 1432032823,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5560354005679a439808fd2-73072487',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="contentcontainer">
<div class="headings">
		<?php if ($_smarty_tpl->getVariable('mode')->value=='edit'){?>
	<h2>Edit Media Partners</h2>
    <?php }else{ ?>
     <h2>Add Media Partners</h2>
    <?php }?>
</div>
<div class="contentbox" id="tabs-1">
    <?php if ($_smarty_tpl->getVariable('var_msg')->value!=''){?>
     <div class="status error" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_error.png" title="Error" />
              <?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
</p> 
     </div>     
    <div></div>
    <?php }?>
		<form id="frmadd" name="frmadd" action="index.php?file=u-media_partners_a" method="post" enctype="multipart/form-data">
        <input type="hidden" name="iMediaPartnerId" id="iMediaPartnerId" value="<?php echo $_smarty_tpl->getVariable('iMediaPartnerId')->value;?>
" />
        <input type="hidden" name="action" id="action" value="<?php echo $_smarty_tpl->getVariable('mode')->value;?>
" />
		<input type="hidden" name="imagemode" id="imagemode" />
			<p>
				<label for="textfield"><strong>Title :</strong></label>
				<input type="text" id="vTitle" name="Data[vTitle]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_commision')->value[0]['vTitle'];?>
" lang="*" title="Logo Title"/>
			</p>
			<p>
				<label for="textfield"><strong>Upload Image :</strong></label>
				<input type="file" id="vImage" name="Data[vImage]" class="inputbox" title="Images" />
			<br />[Note: Supported File Types are *.jpg,*.jpeg,*.png,*.gif]				
			</p>
			<?php if ($_smarty_tpl->getVariable('mode')->value=='edit'){?>					
			<?php if ($_smarty_tpl->getVariable('img')->value=='yes'){?>	
			<p>
				<table>
					<tr>
						<td width="25%"><img src= "<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_upload_images_media_partners'];?>
/2_<?php echo $_smarty_tpl->getVariable('db_commision')->value[0]['vImage'];?>
" /></td>
					</tr>
					<tr>
						<td>
							<input type="button" value="Delete Image" class="btn" onclick="return confirm_delete();" title="Delete Image"/>        
						</td>
					</tr>
				</table>
			</p>
			<?php }?>
			<?php }else{ ?>
			<?php }?>	
     <p>
				<label for="textfield"><strong>URL :</strong></label>
				<input type="text" id="vURL" name="Data[vURL]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_commision')->value[0]['vURL'];?>
"/>
				<br />[Example: http://www.sitename.com]
			</p>		
			<p>
				<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]">
					<option value="Active" <?php if ($_smarty_tpl->getVariable('db_commision')->value[0]['eStatus']=='Active'){?>selected<?php }?> >Active</option>
					<option value="Inactive" <?php if ($_smarty_tpl->getVariable('db_commision')->value[0]['eStatus']=='Inactive'){?>selected<?php }?> >Inactive</option>
				</select>
			</p>
			<input type="submit" value="Save" class="btn" title="Save" onclick="return validate(document.frmadd);"/>
			<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
		</form>
</div>
</div>

<script>
function confirm_delete()
{
   ans = confirm('Are you sure Delete Image?');
   if(ans == true){
    document.frmadd.imagemode.value = 'delete_image';
    document.frmadd.submit();
   }else{
    return false;
   }
}
function redirectcancel(){
    window.location="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-media_partners&mode=view";
    return false;
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
