<?php /* Smarty version Smarty-3.0.7, created on 2016-01-01 18:27:31
         compiled from "/home/www/cargosharing1/bbcsadmin/templates/rides/view-rideslist.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5949035845686c513f085e5-97918919%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '83510e691f372a07a6f563ff021e73bcd7bcba35' => 
    array (
      0 => '/home/www/cargosharing1/bbcsadmin/templates/rides/view-rideslist.tpl',
      1 => 1432638008,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5949035845686c513f085e5-97918919',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<script language="JavaScript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tcp_javascript'];?>
jlist.js"></script>
<div class="contentcontainer">
	<div class="headings altheading">
		<h2>Trips</h2>
	</div>
	<div class="contentbox">
    <?php if ($_smarty_tpl->getVariable('var_msg_new')->value!=''){?>
     <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_success.png" title="Success" />
              <?php echo $_smarty_tpl->getVariable('var_msg_new')->value;?>
</p> 
     </div>     
    <div></div>
    <?php }elseif($_smarty_tpl->getVariable('var_msg')->value!=''){?>
    <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_success.png" title="Success" />
              <?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
</p> 
     </div>     
    <div></div>
    <?php }?>
		<form name="frmsearch" id="frmsearch" action="" method="post">
    <input  type="hidden" name="paymenttype" value="<?php echo $_smarty_tpl->getVariable('paymenttype')->value;?>
"/>   
        <table width="100%" border="0">
		<tbody>
			<tr>
				<td width="10%"><label for="textfield"><strong>Search:</strong></label><td>
				<td width="10%"><input type="Text" id="keyword" name="keyword" value="<?php echo $_smarty_tpl->getVariable('keyword')->value;?>
"  class="inputbox" /><td>
				<td width="10%" >
					<select name="option" id="option">
						<option  value="Title" <?php if ($_smarty_tpl->getVariable('option')->value=='Title'){?>selected<?php }?>>Title</option> 					
					  <option  value="Member" <?php if ($_smarty_tpl->getVariable('option')->value=='Member'){?>selected<?php }?>>Member</option>
						<option value="Status"<?php if ($_smarty_tpl->getVariable('option')->value=='Status'){?>selected<?php }?>>Status</option>
					</select>
				</td>
				  <td width="60%">&nbsp;</td>
	<!--			<td width="60%"><input type="button" value="Search" class="btnalt" id="Search" name="Search" title="Search" onclick="Searchoption();"/></td> -->
	<!--			<td width="10%"><input type="button" value="Add New" onclick="Redirect('index.php?file=m-member&mode=add');" class="btnalt" />&nbsp;</td> -->
			</tr>
      <tr>
        <td><label for="textfield"><strong>Filter By Status:</strong></label></td>
        <td colspan="6">
          &nbsp; &nbsp;
          <select name="status" id="status">
            <option value="">All</option>
            <option value="Active" <?php if ($_smarty_tpl->getVariable('status')->value=='Active'){?>selected <?php }?>>Trip is Ongoing (Active)</option>
            <option value="Inactive" <?php if ($_smarty_tpl->getVariable('status')->value=='Inactive'){?>selected <?php }?>>Inactive (Inactive)</option>
            <option value="Pending" <?php if ($_smarty_tpl->getVariable('status')->value=='Pending'){?>selected <?php }?>>Trip is under Under Registration (Pending)</option>
            <option value="Completed" <?php if ($_smarty_tpl->getVariable('status')->value=='Completed'){?>selected <?php }?>>The trip is Over (Completed)</option>
            <option value="Deleted" <?php if ($_smarty_tpl->getVariable('status')->value=='Deleted'){?>selected <?php }?>>The trip has been Deleted (Deleted)</option>
            <option value="Archived" <?php if ($_smarty_tpl->getVariable('status')->value=='Archived'){?>selected <?php }?>>Archived (Archived)</option>
          </select>
        </td>
      </tr>	
      <tr>
        <td>
          &nbsp;
        </td>
        <td colspan="6">
           &nbsp; &nbsp;
          <input type="button" value="Search" class="btnalt" id="Search" name="Search" title="Search" onclick="Searchoption();"/>
        </td>
      </tr>
			<tr>
				<td colspan="7" align="center">
          <?php if (@PAYMENT_OPTION!='Contact'){?>
          <input type="button" value="Past Bookings/ Payment" onclick="Redirect('index.php?file=ri-rideslist&mode=view&paymenttype=past');" class="<?php if ($_smarty_tpl->getVariable('paymenttype')->value=='past'){?>btn<?php }else{ ?>btnalt<?php }?>" />
          <input type="button" value="Today's Bookings/ Payment" onclick="Redirect('index.php?file=ri-rideslist&mode=view&paymenttype=today');" class="<?php if ($_smarty_tpl->getVariable('paymenttype')->value=='today'){?>btn<?php }else{ ?>btnalt<?php }?>" />
          <input type="button" value="All Bookings/ Payment" onclick="Redirect('index.php?file=ri-rideslist&mode=view&paymenttype=all');" class="<?php if ($_smarty_tpl->getVariable('paymenttype')->value=='all'){?>btn<?php }else{ ?>btnalt<?php }?>" />
				  <?php }else{ ?>
				  <input type="button" value="All Bookings" onclick="Redirect('index.php?file=ri-rideslist&mode=view&paymenttype=all');" class="<?php if ($_smarty_tpl->getVariable('paymenttype')->value=='all'){?>btn<?php }else{ ?>btnalt<?php }?>" />
				  <?php }?>
        </td>
			</tr>
		</tbody>			
		</table> 
    </form>          
   <form name="frmlist" id="frmlist" action="index.php?file=ri-rideslist_a" method="post">
   	<table width="100%" border="0">
		<input type="hidden" name="action" id="action" value="" />
    <input  type="hidden" name="iRideId" value=""/>
    <input  type="hidden" name="paymenttype" value="<?php echo $_smarty_tpl->getVariable('paymenttype')->value;?>
"/>
    <thead>
    <tr>
				<th style="font-size:12px;text-align:left;" width="65%">Trips Details</th> 
        <?php if (@PAYMENT_OPTION!='Contact'){?>
        <th style="font-size:12px;text-align:center;" width="10%">Payment Due</th> 
        <?php }?>             
        <th style="font-size:12px;text-align:center;" width="10%">Status</th>               
        <th style="font-size:12px;text-align:center;" width="15%">Action&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" onclick="checkAll(document.frmlist);" name="check_all" id="check_all"></th>
    </tr> 
    </thead>
     <!--<thead>
			<tr>
				<th style="font-size:12px;" width=""><a href="javascript:void(0);" onClick="Redirect('index.php?file=ri-rideslist&mode=view&sortby=1&order=<?php if ($_smarty_tpl->getVariable('order')->value==0){?>1<?php }else{ ?>0<?php }?>');">Title</a><?php if ($_smarty_tpl->getVariable('sortby')->value==1){?>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
<?php if ($_smarty_tpl->getVariable('order')->value==0){?>icons/up_active.png<?php }else{ ?>icons/down_active.png<?php }?>"><?php }?></th>               
        <th style="font-size:12px;" width="8%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=ri-rideslist&mode=view&sortby=2&order=<?php if ($_smarty_tpl->getVariable('order')->value==0){?>1<?php }else{ ?>0<?php }?>');">Member</a><?php if ($_smarty_tpl->getVariable('sortby')->value==2){?>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
<?php if ($_smarty_tpl->getVariable('order')->value==0){?>icons/up_active.png<?php }else{ ?>icons/down_active.png<?php }?>"><?php }?></th>               
 
        <th style="font-size:12px;" width="8%">Price</th>               
    	  <th style="font-size:12px;" width="8%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=ri-rideslist&mode=view&sortby=7&order=<?php if ($_smarty_tpl->getVariable('order')->value==0){?>1<?php }else{ ?>0<?php }?>');">Total Seats<?php if ($_smarty_tpl->getVariable('sortby')->value==7){?>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
<?php if ($_smarty_tpl->getVariable('order')->value==0){?>icons/up_active.png<?php }else{ ?>icons/down_active.png<?php }?>"><?php }?></th>
    	  <th style="font-size:12px;" width="7%">Seats Left</th>
    	  <th style="font-size:12px;" width="11%">Departure Date/Time</th>
    	  <th style="font-size:12px;" width="11%">Arrival Date/Time</th>
    	  <th style="font-size:12px;" width="10%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=ri-rideslist&mode=view&sortby=3&order=<?php if ($_smarty_tpl->getVariable('order')->value==0){?>1<?php }else{ ?>0<?php }?>');">Added Date</a><?php if ($_smarty_tpl->getVariable('sortby')->value==3){?>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
<?php if ($_smarty_tpl->getVariable('order')->value==0){?>icons/down_active.png<?php }else{ ?>icons/up_active.png<?php }?>"><?php }?></th>
        <th style="font-size:12px;" width="6%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=ri-rideslist&mode=view&sortby=4&order=<?php if ($_smarty_tpl->getVariable('order')->value==0){?>1<?php }else{ ?>0<?php }?>');">Status</a><?php if ($_smarty_tpl->getVariable('sortby')->value==4){?>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
<?php if ($_smarty_tpl->getVariable('order')->value==0){?>icons/up_active.png<?php }else{ ?>icons/down_active.png<?php }?>"><?php }?></th>               
       	<th style="font-size:12px;" width="12%" style="text-align:center;">Action</th>
				<th style="font-size:12px;" width="5%"><input type="checkbox" id="check_all" name="check_all" onclick="checkAll(document.frmlist);"/></th>
			</tr>
		</thead>-->
		<tbody>
    <?php if (count($_smarty_tpl->getVariable('db_records_all')->value)>0){?>
		<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_records_all')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
    <tr style="background:<?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['color'];?>
">
			<td width="65%" >
        <table width="100%" style="border:1px solid #cccccc;">
        <tr>
          <td style="color: #05A2DB;font-size:15px;">
            <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=ri-rideslist&mode=edit&iRideId=<?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iRideId'];?>
" style="color: #05A2DB;font-size:15px;"><strong>Trip of:</strong> <?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainDeparture'];?>
 - <?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainArrival'];?>
</a>
          </td>         
        </tr>
        <tr>
          <td style="color:#7C7C7C;font-size:14px;">
            <strong>Offered By:</strong> <?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vFirstName'];?>
&nbsp;<?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vLastName'];?>

            <!--&nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp;
            <strong>Price:</strong> <?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBookerCurrencyCode'];?>
&nbsp;<?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['price'];?>
  -->
            &nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp;
            <strong>Type:</strong> <?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eRideType'];?>

            &nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp;
            <!-- <strong>Seats:</strong> <?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iSeats'];?>

            &nbsp;&nbsp;<strong>|</strong>&nbsp;&nbsp; -->
            <strong>Add Date:</strong> <?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dAddedDate'],9);?>

          </td>
        </tr>
        </table>
      </td>
      <?php if (@PAYMENT_OPTION!='Contact'){?>
      <td width="10%">
        <table width="100%" style="border:1px solid #cccccc;">
        <tr>
          <td style="text-align:center;">
            <strong><?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['paymentdue'];?>
</strong>
          </td>         
        </tr>
        <tr><td style="text-align:center;"><?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['todaybooking'];?>
&nbsp;<?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['pastbooking'];?>
</td></tr>
        </table>
      </td>
      <?php }?>
      <td width="10%">
        <table width="100%" style="border:1px solid #cccccc;">
        <tr>
          <td style="text-align:center;">
            <strong><?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eStatus'];?>
</strong>
          </td>         
        </tr>
        <tr><td>&nbsp;</td></tr>
        </table>
      </td>
      <td width="15%">
        <table width="100%" style="border:1px solid #cccccc;">
        <tr>
          <td>
              <a title="" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=ri-rideslist&mode=edit&iRideId=<?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iRideId'];?>
"><img title="Edit" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_edit.png"></a>
              <a onclick="MakeAction('<?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iRideId'];?>
','Active');" title="Active" href="javascript:void(0);"><img title="Active" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_approve.png"></a>
              <a onclick="MakeAction('<?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iRideId'];?>
','Inactive');" title="Inactive" href="javascript:void(0);"><img title="Inactive" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_unapprove.png"></a>
              <a onclick="MakeAction('<?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iRideId'];?>
','Pending');" title="Pending" href="javascript:void(0);"><img title="Pending" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon-pending.gif"></a>
              <a onclick="MakeAction('<?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iRideId'];?>
','Deleted');" title="Delete" href="javascript:void(0);"><img title="Delete" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_delete.png"></a>
              <a onclick="MakeAction('<?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iRideId'];?>
','Completed');" title="Delete" href="javascript:void(0);"><img title="Completed" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/comp.png"></a>
              <a onclick="MakeAction('<?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iRideId'];?>
','Archived');" title="Delete" href="javascript:void(0);"><img title="Archived" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/arch.png"></a>
              &nbsp;&nbsp;&nbsp;<input name="iRideId[]" type="checkbox" id="iId" value="<?php echo $_smarty_tpl->getVariable('db_records_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iRideId'];?>
"/>
          </td>         
        </tr>
        <tr><td>&nbsp;</td></tr>
        </table>
      </td>
	  </tr>
    <?php endfor; endif; ?>
    <?php }else{ ?>
    <tr>
			<td height="70px;" colspan="12" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No Record Found.</td>
		</tr>
    <?php }?>
		</tbody>
		</table>
   </form>
		<div class="extrabottom">
			<ul>
				<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_edit.png" alt="Edit" /> Edit</li>
				<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_approve.png" alt="Approve" /> Active</li>
				<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_unapprove.png" alt="Unapprove" /> Inactive</li>
				<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon-pending.gif" alt="Pending" /> Pending</li>
				<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_delete.png" alt="Deleted" /> Deleted</li>
				<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/comp.png" alt="Deleted" /> Completed</li>
				<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/arch.png" alt="Archived" /> Archived</li>
			</ul>
			<div class="bulkactions">
				<select name="newaction" id="newaction">
					<option value="">Select Action</option>
					<option value="Active">Make Active</option>
					<option value="Inactive">Make Inactive</option>
					<option value="Deleted">Make Deleted</option>
					<option value="Pending">Make Pending</option>
					<option value="Completed">Make Completed</option>
					<option value="Archived">Make Archived</option>
					<option value="Show All">Show All</option>
				</select>
				<input type="submit" value="Apply" class="btn" onclick="return Doaction(document.getElementById('newaction').value,'ri-rideslist',document.frmlist);"/>
			</div>
		</div>
        <div>
          <div class="pagination">
          <?php if (count($_smarty_tpl->getVariable('db_records_all')->value)>0){?>
	          <span class="switch" style="float: left;"><?php echo $_smarty_tpl->getVariable('recmsg')->value;?>
</span>
	        <?php }?>
          </div>
          <?php echo $_smarty_tpl->getVariable('page_link')->value;?>

        </div>
		
		<div style="clear: both;"></div></div>
</div>

<script>
function Searchoption(){
    document.getElementById('frmsearch').submit();
}
function AlphaSearch(val){
    var alphavalue = val;
    var file = 'ri-rideslist';
    window.location="index.php?file="+file+"&alp="+alphavalue+"&mode=view";
    return false;
}
function MakeAction(loopid,type){
if(type == 'Deletes')
    {
	   ans = confirm("Are you sure you want to delete?");
     if (ans == false)
      {
        return false;
      }
    }
    document.frmlist.iRideId.value = loopid;
    document.frmlist.action.value = type;
	document.frmlist.submit();	
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
