<?php /* Smarty version Smarty-3.0.7, created on 2016-02-01 23:22:44
         compiled from "/home4/shipcliq/public_html/bbcsadmin/templates/localization/state.tpl" */ ?>
<?php /*%%SmartyHeaderCode:26117267656b03d24d175c2-16614405%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8351593051c7288c0ba6a26a93ef032b37c80f74' => 
    array (
      0 => '/home4/shipcliq/public_html/bbcsadmin/templates/localization/state.tpl',
      1 => 1453900240,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '26117267656b03d24d175c2-16614405',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="contentcontainer" id="tabs">
	<div class="headings">
  	<?php if ($_smarty_tpl->getVariable('mode')->value=='add'){?>
		<h2 class="left">Add Province</h2>
    <?php }else{ ?>
	<h2 class="left">Edit Province</h2>
    <?php }?>

  </div>
	<div class="contentbox" id="tabs-1">
             <?php if ($_smarty_tpl->getVariable('var_msg')->value!=''){?>
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_error.png" title="Success" />
					<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
</p> 
				</div>     
				<div></div>
			<?php }?>
			<?php if ($_smarty_tpl->getVariable('var_msg_error')->value!=''){?>
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_error.png" title="Success" />
					<?php echo $_smarty_tpl->getVariable('var_msg_error')->value;?>
</p> 
				</div>     
				<div></div>
			<?php }?>
			<form id="frmadd" name="frmadd" action="index.php?file=l-state_a" method="post">
            <input type="hidden" name="iStateId" id="iStateId" value="<?php echo $_smarty_tpl->getVariable('iStateId')->value;?>
" />
            <input type="hidden" name="action" id="action" value="<?php echo $_smarty_tpl->getVariable('mode')->value;?>
" />
				<p>
					<label for="textfield"><strong>Province :<em>*</em></strong></label>
					<input type="text" id="vState" name="Data[vState]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_state')->value[0]['vState'];?>
" lang="*" title="Province"/>
				</p>
				<p>
					<label for="textfield"><strong>Province Code :<em>*</em></strong></label>
					<input type="text" id="vStateCode" name="Data[vStateCode]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_state')->value[0]['vStateCode'];?>
" lang="*" title="Province Code"/>
				</p>
				<p>
					<label for="textfield"><strong>Country : *</strong></label>
					<select id="iCountryId" name="Data[iCountryId]" lang="*" title="Country">
						<option value=""> --- Select Country --- </option>
                        <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['name'] = 'rec';
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_country')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['total']);
?>
						<option  <?php if ($_smarty_tpl->getVariable('db_country')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rec']['index']]['iCountryId']==$_smarty_tpl->getVariable('db_state')->value[0]['iCountryId']){?> selected <?php }?> value="<?php echo $_smarty_tpl->getVariable('db_country')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rec']['index']]['iCountryId'];?>
"><?php echo $_smarty_tpl->getVariable('db_country')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rec']['index']]['vCountry'];?>
</option>
                        <?php endfor; endif; ?>
					</select>
				</p>
        
			<!--
    <p>
					<label for="textfield"><strong>Country Code :</strong></label>
					<input type="text" id="vCountryCode"  name="Data[vCountryCode]" class="inputbox" lang="*" title="Country Code" value="<?php echo $_smarty_tpl->getVariable('db_state')->value[0]['vCountryCode'];?>
"/>
				</p>
   -->	
				<p>
					<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" <?php if ($_smarty_tpl->getVariable('db_state')->value[0]['eStatus']=='Active'){?>selected<?php }?>>Active</option>
						<option value="Inactive" <?php if ($_smarty_tpl->getVariable('db_state')->value[0]['eStatus']=='Inactive'){?>selected<?php }?>>Inactive</option>
					</select>
				</p>
				<?php if ($_smarty_tpl->getVariable('mode')->value=='edit'){?>
				<input type="submit" value="Edit Province" class="btn" onclick="return validate(document.frmadd);" title="Edit Province"/>
				      <?php }else{ ?>
   				<input type="submit" value="Add Province" class="btn" onclick="return validate(document.frmadd);" title="Add Province"/>
  				
				      
				      <?php }?>
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
			</form>
	</div>
</div>

<script>
 
function redirectcancel()
{
    window.location="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=l-state&mode=view";
    return false;
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
