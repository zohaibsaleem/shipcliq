<?php /* Smarty version Smarty-3.0.7, created on 2015-11-25 19:15:58
         compiled from "/home/www/cargosharing1/cargoadmin/templates/rides/ridepayment_member.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1281213847565608ee16f3f5-15903386%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8a300f7567dcc76f3d89e1199633bb9a5f91a620' => 
    array (
      0 => '/home/www/cargosharing1/cargoadmin/templates/rides/ridepayment_member.tpl',
      1 => 1448478954,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1281213847565608ee16f3f5-15903386',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="contentcontainer" id="tabs">
	<div class="headings">         	
		<h2 class="left">Rides booking / Payment information of <?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vLastName'];?>
</h2>          
	</div>
	<div class="contentbox" id="tabs-1">  
		<?php if ($_smarty_tpl->getVariable('var_msg')->value!=''){?>
		
		<div class="status success" id="errormsgdiv"> 
			<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
			<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_success.png" title="Success" />
			<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
</p> 
		</div> 
		<?php }?> 
		<form name="frmpaymenr" id="frmpaymenr" method="post" action="index.php?file=ri-ridepaymentmember_a">
			<input type="hidden" name="mode" value="edit">
			<input type="hidden" name="action" value="payment">
			<input type="hidden" name="main_total" id="main_total" value="0.00">
			<input type="hidden" name="commission" id="commission" value="0.00">
			<input type="hidden" name="sub_total" id="sub_total" value="0.00">  	
			<input type="hidden" name="drivercurrencycode" id="drivercurrencycode" value="<?php echo $_smarty_tpl->getVariable('driver_currency')->value;?>
">
			<input type="hidden" name="eTripReturn" id="eTripReturn" value="<?php echo $_smarty_tpl->getVariable('eTripReturn')->value;?>
">
			<input type="hidden" name="fCommissionRate" id="fCommissionRate" value="<?php echo $_smarty_tpl->getVariable('fCommissionRate')->value;?>
">
			<input type="hidden" name="iMemberId" id="iMemberId" value="<?php echo $_smarty_tpl->getVariable('iMemberId')->value;?>
">
			<input type="hidden" name="vPaymentEmail" id="vPaymentEmail" value="<?php echo $_smarty_tpl->getVariable('vPaymentEmail')->value;?>
"> 
			
			<table class="headingbg" style="border: 1px solid #ccc;" border="0" cellpadding="1" cellspacing="1" width="100%"> 
				<thead>
					<tr>
						<th style="font-size:12px;text-align:left;" width="35%">Ride Details</th> 
						<th style="font-size:12px;text-align:left;" width="15%">Booking Amount</th>
						<th style="font-size:12px;text-align:center;" width="10%">Confirm By Booker</th> 
						<th style="font-size:12px;text-align:center;" width="10%">Pay</th>            
						<th style="font-size:12px;text-align:right;" width="10%">Site Commission</th>
						<th style="font-size:12px;text-align:right;" width="10%">Traveler Payment</th>
						<th style="font-size:12px;text-align:right;" width="10%">Ride Cancel By</th>
					</tr> 
				</thead>   
				<tbody>   
					<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['name'] = "booking";
$_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_bookings')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["booking"]['total']);
?>      
					<tr>
						<td valign="top" style="border-bottom:1px solid #cccccc;font-size:13px;">
							<strong>Booking No.</strong> #<?php echo $_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['vBookingNo'];?>

							<br><strong>Booker Name:</strong> <?php echo $_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['vBookerFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['vBookerLastName'];?>

							<br><strong>Trip of:</strong> <?php echo $_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['vFromPlace'];?>
 &rarr; <?php echo $_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['vToPlace'];?>

							<br><strong>Date & Time:</strong><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['dBookingDate'],10);?>
 - <?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['dBookingTime'],12);?>

						</td> 
						<td valign="top" style="border-bottom:1px solid #cccccc;font-size:13px;">
							<strong>Booker Pay:</strong> <?php echo $_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['vBookerCurrencyCode'];?>
 <?php echo $_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['fAmount'];?>

						</td>            
						<td valign="top" style="border-bottom:1px solid #cccccc;font-size:13px;text-align:center;">
							<?php echo $_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['eBookerConfirmation'];?>

						</td>
						<?php if ($_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['eStatus']=='Cencelled'){?>
						<td valign="top" style="border-bottom:1px solid #cccccc;font-size:13px;text-align:center;">     
							<input type="button" class="btn" onclick="go_for_refund(<?php echo $_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['iBookingId'];?>
);" value="Cancellation Refund">
						</td>
						<?php }else{ ?>
						<td valign="top" style="border-bottom:1px solid #cccccc;font-size:13px;text-align:center;">
							<!--<input type="checkbox" onClick="calculate_again(<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['booking']['index'];?>
, <?php echo $_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['iBookingId'];?>
);" name="confirm[]" id="confirm_<?php echo $_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['iBookingId'];?>
" <?php if ($_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['eBookerConfirmation']=='Yes'){?> checked <?php }?> value="<?php echo $_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['iBookingId'];?>
">-->
							<input type="hidden" name="amount_<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['booking']['index'];?>
_<?php echo $_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['iBookingId'];?>
" id="amount_<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['booking']['index'];?>
_<?php echo $_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['iBookingId'];?>
" value="<?php echo $_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['driver_amount'];?>
 ">
							
							<?php if ($_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['driver_amount']!='0.00'&&$_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['dBookingDate']<=$_smarty_tpl->getVariable('currdate')->value){?>
								<?php if ($_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['dBookingTime']<=$_smarty_tpl->getVariable('currtime')->value){?>
									<select name="eDriverPaymentType" id="eDriverPaymentType_<?php echo $_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['iBookingId'];?>
">
										<option value="Paypal">Paypal</option>
										<option value="BankTransfer">Bank Transfer</option>
									</select>
									<br />
									<input type="button" class="btn" onclick="go_for_payment(<?php echo $_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['iBookingId'];?>
,<?php echo $_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['driver_amount'];?>
);" value="Pay" style="margin-top:10px;">
								<?php }?> 
							<?php }?> 
						</td>
						<?php }?>
						<td valign="top" style="border-bottom:1px solid #cccccc;font-size:13px;text-align:right;"> 
							<?php echo $_smarty_tpl->getVariable('driver_currency')->value;?>
 <?php echo $_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['commission_amount'];?>
        
						</td>
						<td valign="top" style="border-bottom:1px solid #cccccc;font-size:13px;text-align:right;">
							<?php echo $_smarty_tpl->getVariable('driver_currency')->value;?>
 <?php echo $_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['driver_amount'];?>
        
						</td>
						<td valign="top" style="border-bottom:1px solid #cccccc;font-size:13px;text-align:right;">
							<?php if ($_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['eStatus']=='Cencelled'){?> 
								<?php echo $_smarty_tpl->getVariable('db_bookings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['booking']['index']]['eCancelBy'];?>

							<?php }else{ ?>
								--
							<?php }?>
						</td>
					</tr>  
					<?php endfor; endif; ?> 
					<tr>
						<td colspan="5" width="80%" height="20" style="border-bottom:0px solid #cccccc;font-size:13px;text-align:right;">
						</td>
						<td colspan="2" width="20%" height="20" style="border-bottom:1px solid #cccccc;border-left:1px solid #cccccc;font-size:13px;text-align:right;">
							<strong>Total payment: <span id="totpaymentspan"><?php echo $_smarty_tpl->getVariable('driver_currency')->value;?>
 <?php echo $_smarty_tpl->getVariable('tot_driver_refund')->value;?>
</span></strong>
						</td>
					</tr>
					<tr>
						<td colspan="5" width="80%" height="20" style="border-bottom:0px solid #cccccc;font-size:13px;text-align:right;">
						</td>
						<td colspan="2" width="20%" height="20" style="border-bottom:1px solid #cccccc;border-left:1px solid #cccccc;font-size:13px;text-align:right;">
							<strong>Payout limit: <span id="totpaymentspan"><?php echo $_smarty_tpl->getVariable('driver_currency')->value;?>
 <?php echo $_smarty_tpl->getVariable('MINIMUM_PAYOUT_LIMIT')->value;?>
</span></strong>
						</td>
					</tr>
					<!--<tr>
						<td colspan="4" width="80%" height="20" style="border-bottom:0px solid #cccccc;font-size:13px;text-align:right;"></td>
						<td colspan="1" width="20%" height="20" style="border-bottom:1px solid #cccccc;border-left:1px solid #cccccc;font-size:13px;text-align:right;">
						<strong>Sub Total: <?php echo $_smarty_tpl->getVariable('driver_currency')->value;?>
 <span id="totpaymentspan">0.00</span></strong>
						</td>
						</tr>
						<tr>
						<td colspan="4" width="80%" height="20" style="border-bottom:0px solid #cccccc;font-size:13px;text-align:right;"></td>
						<td colspan="1" width="20%" height="20" style="border-bottom:1px solid #cccccc;border-left:1px solid #cccccc;font-size:13px;text-align:right;">
						<strong>Site Commission (<?php echo $_smarty_tpl->getVariable('fCommissionRate')->value;?>
 %): <?php echo $_smarty_tpl->getVariable('driver_currency')->value;?>
 <span id="itecommissionspan">0.00</span></strong>
						</td>
						</tr>
						<tr>
						<td colspan="4" width="80%" height="20" style="border-bottom:0px solid #cccccc;font-size:13px;text-align:right;"></td>
						<td colspan="1" width="20%" height="20" style="border-bottom:1px solid #cccccc;border-left:1px solid #cccccc;font-size:13px;text-align:right;">
						<strong>Grand Total: <?php echo $_smarty_tpl->getVariable('driver_currency')->value;?>
 <span id="grandtotalspan">0.00</span></strong>
						</td>
						</tr>
						<tr>
						<td colspan="4" width="80%" height="20" style="border-bottom:0px solid #cccccc;font-size:13px;text-align:right;"></td>
						<td colspan="1" width="20%" height="20" style="border-bottom:0px solid #cccccc;border-left:1px solid #cccccc;font-size:13px;text-align:right;">
						<?php if ($_smarty_tpl->getVariable('vPaymentEmail')->value!=''){?>
						<input type="button" value="Pay To Driver" class="btn" onClick="check_payment();" title="Pay To Driver"/>
						<?php }else{ ?>
						Receiver payment email not found
						<?php }?>
						</td>
					</tr>-->
				</tbody>
			</table>
		</form>
		<table class="headingbg" style="border: 1px solid #ccc;margin-top:20px;" border="0" cellpadding="1" cellspacing="1" width="100%">
			<tr>
				<th><h2>Payment Details Of <?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vLastName'];?>
</h2></th>
			</tr>
			<tr>
				<td>
					<fieldset style="width:400px;">
						<legend>Paypal Details</legend>
						<strong>Paypal E-mail Id : </strong>  <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['vPaymentEmail']!=''){?><?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vPaymentEmail'];?>
<?php }else{ ?>---<?php }?>
					</fieldset>  
				</td>
			</tr>
			<tr>
				<td>
					<fieldset style="width:400px;">
						<legend>Bank Details</legend>
						<strong>Account Holder Name  : </strong> <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['vBankAccountHolderName']!=''){?><?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vBankAccountHolderName'];?>
<?php }else{ ?>---<?php }?><br />
						<strong>Account Number (IBAN) : </strong> <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['vAccountNumber']!=''){?><?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vAccountNumber'];?>
<?php }else{ ?>---<?php }?><br />
						<strong>Name of Bank : </strong>  <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['vBankName']!=''){?><?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vBankName'];?>
<?php }else{ ?>---<?php }?><br />
						<strong>Bank Location  : </strong>  <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['vBankLocation']!=''){?><?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vBankLocation'];?>
<?php }else{ ?>---<?php }?><br />
						<strong>BIC/SWIFT Code  : </strong>  <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['vBIC_SWIFT_Code']!=''){?><?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vBIC_SWIFT_Code'];?>
<?php }else{ ?>---<?php }?><br />
					</fieldset>  
				</td>
			</tr>
		</table>
	</div>
</div> 
<form name="frmdriverpayment" id="frmdriverpayment" method="post" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=ri-ridepaymentmember_a">
	<input type="hidden" name="payment_action" id="payment_action" value="paynow">
	<input type="hidden" name="payment_booking" id="payment_booking" value="">
	<input type="hidden" name="payment_amount" id="payment_amount" value="">
	<input type="hidden" name="payment_type" id="payment_type" value="">
</form>

<script>
	function backtolist(){
		window.location="index.php?file=ri-rideslist&mode=view";
	}
	function edit_dealstatus(){
		document.frmadd.submit();
	} 
	
	var fCommissionRate = <?php echo $_smarty_tpl->getVariable('fCommissionRate')->value;?>

	
	function calculate_again(ind,id){     
		if($("#confirm_"+id).is(':checked')){
			var r=confirm("Are you sure want to add this booking in driver payment?");
			if (r==true)
			{
				var sub_total = $("#sub_total").val();
				var add_amt = $("#amount_"+ind+"_"+id).val();
				var nwsubtotal = Number(sub_total) + Number(add_amt); 
				$("#sub_total").val(nwsubtotal.toFixed(2));
				$("#totpaymentspan").html(nwsubtotal.toFixed(2));
				
				var commission = (nwsubtotal.toFixed(2) * Number(fCommissionRate)) / 100;
				$("#commission").val(commission.toFixed(2));
				$("#itecommissionspan").html(commission.toFixed(2));
				
				var main_total = Number(nwsubtotal) - Number(commission);
				$("#main_total").val(main_total.toFixed(2));
				$("#grandtotalspan").html(main_total.toFixed(2));
				
				
				}else{
				//$("#confirm_"+id).prop('checked', false);
				$("#confirm_"+id).removeAttr('checked');
				return false;
			} 
			}else{
			var r=confirm("Are you sure want to remove this booking in driver payment?");
			if (r==true)
			{
				var sub_total = $("#sub_total").val();
				var add_amt = $("#amount_"+ind+"_"+id).val();
				var nwsubtotal = Number(sub_total) - Number(add_amt); 
				$("#sub_total").val(nwsubtotal.toFixed(2));
				$("#totpaymentspan").html(nwsubtotal.toFixed(2));
				
				var commission = (nwsubtotal.toFixed(2) * Number(fCommissionRate)) / 100;
				$("#commission").val(commission.toFixed(2));
				$("#itecommissionspan").html(commission.toFixed(2));
				
				var main_total = Number(nwsubtotal) - Number(commission);
				$("#main_total").val(main_total.toFixed(2));
				$("#grandtotalspan").html(main_total.toFixed(2));
				}else{
				return false;
			} 
		}
		
	}
	
	function check_payment(){
		if($("#sub_total").val() > 0){
			var r=confirm("Are you sure want to payment to driver?");
			if (r==true)
			{
				document.frmpaymenr.submit();
				}else{
				return false;
			} 
			}else{
			alert('Please select atleast one pay checkbox for payment to driver');
			return false;    
		}    
	} 
	function hidemessage(){
		jQuery("#errormsgdiv").slideUp();
	}
	
	function go_for_payment(id,driveramount){ //alert(id);return false;
		if(id != ''){
			var r=confirm("Are you sure want to payment to driver?");
			if (r==true)
			{
				$("#payment_booking").val(id);
				$("#payment_amount").val(driveramount);
				var paymenttype = document.getElementById("eDriverPaymentType_"+id).value;
				$("#payment_type").val(paymenttype);
				if($("#payment_booking").val() != ''){
					document.frmdriverpayment.submit();
					return false;
				}
				}else{
				return false;
			} 
		}
	}
	
	function go_for_refund(id){
		window.location="index.php?file=bo-bookings&mode=edit&iBookingId="+id;
	}
</script>
