<?php /* Smarty version Smarty-3.0.7, created on 2016-01-01 18:50:59
         compiled from "/home/www/cargosharing1/bbcsadmin/templates/report/view-money_reports.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12540399325686ca9309d8f3-95084365%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8d3f5b719ccc5b98a6d5bc22ae345747c4dfbe1b' => 
    array (
      0 => '/home/www/cargosharing1/bbcsadmin/templates/report/view-money_reports.tpl',
      1 => 1451674067,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12540399325686ca9309d8f3-95084365',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<link type="text/css" rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
dhtmlgoodies_calendar.css" media="screen">
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
dhtmlgoodies_calendar.js"></script>
<script language="JavaScript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tcp_javascript'];?>
jlist.js"></script>
<div class="contentcontainer">
	<div class="headings altheading">
		<h2>Payment Report</h2>
	</div>
	<div class="contentbox">
		<?php if ($_smarty_tpl->getVariable('var_msg_new')->value!=''){?>
		<div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_success.png" title="Success" />
			<?php echo $_smarty_tpl->getVariable('var_msg_new')->value;?>
</p> 
		</div>     
		<div></div>
		<?php }elseif($_smarty_tpl->getVariable('var_msg')->value!=''){?>
		<div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_success.png" title="Success" />
			<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
</p> 
		</div>     
		<div></div>
		<?php }?>
		<form name="frmlist" id="frmlist" method="post" action="">
			<input type="hidden" id="action1" name="action1" value="datesearch">  
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tbody>
					<tr>
						<td width="50%" class="td-listing" colspan="3">&nbsp;&nbsp;&nbsp;<font class="errormsg">&nbsp;</font></td>
					</tr>
					<tr>	
						<td width="1%">&nbsp;</td>
						<td width="59%" valign="top">
							<b>Search Bookings by date</b>
							<table width="100%" cellspacing="1" cellpadding="1" border="0">
								<tbody>
									<tr>
										<td align="right" width="12%" valign="middle" height="35">Date From </td>
										<td width="1%" valign="middle"> :</td>
										<td align="left" width="26%" valign="middle">
											<input type="text" Readonly id="dFDate" name="dFDate" style="width:100px;" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('startdate')->value;?>
" lang="" title="Start Date"/>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
cal-icon.gif" style="cursor:pointer" onclick="displayCalendar(document.getElementById('dFDate'),'yyyy-mm-dd',this)" />
										</td>
										<td align="right" width="9%" valign="middle">Date To</td>
										<td width="1%" valign="middle"> :</td>
										<td align="left" width="35%" valign="middle">
											<input type="text" Readonly id="dTDate" name="dTDate" style="width:100px;" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('enddate')->value;?>
" lang="" title="End Date"/>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
cal-icon.gif" style="cursor:pointer" onclick="displayCalendar(document.getElementById('dTDate'),'yyyy-mm-dd',this)" />
										</td>
									</tr>
									<tr>
										<td align="left" height="25" colspan="6">
											<b>Search Members by time period</b>
										</td>
									</tr>
									<tr>
										<td align="left" colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<img width="6" height="8" src="images/arrow.gif">&nbsp;&nbsp;[<a onclick="return todayDate('dFDate','dTDate');" href="javascript:void(0);">Today</a>]
											[<a onclick="return yesterdayDate('dFDate','dTDate');" href="javascript:void(0);">Yesterday</a>]
											<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img width="6" height="8" src="images/arrow.gif">&nbsp;&nbsp;[<a onclick="return currentweekDate('dFDate','dTDate');" href="javascript:void(0);">Current Week</a>]
											[<a onclick="return previousweekDate('dFDate','dTDate');" href="javascript:void(0);">Previous Week</a>]
											<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img width="6" height="8" src="images/arrow.gif">&nbsp;
											[<a onclick="return currentmonthDate('dFDate','dTDate');" href="javascript:void(0);">Current Month</a>]
											[<a onclick="return previousmonthDate('dFDate','dTDate');" href="javascript:void(0);">Previous Month</a>]
											<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	<img width="6" height="8" src="images/arrow.gif">&nbsp;
											[<a onclick="return currentyearDate('dFDate','dTDate');" href="javascript:void(0);">Current Year</a>] 
											[<a onclick="return previousyearDate('dFDate','dTDate');" href="javascript:void(0);">Previous Year</a>]
										</td>
									</tr>
								</tbody>
							</table>
						</td>
						<td width="40%">
							<b>Show only Refund</b><br><br> 										 
							<!--<input type="Checkbox" value="Active" name="sta[]" <?php if (in_array('Active',$_smarty_tpl->getVariable('sta')->value)){?>checked="checked"<?php }?>> Active   <br>
								<input type="Checkbox" value="Inactive" name="sta[]" <?php if (in_array('Inactive',$_smarty_tpl->getVariable('sta')->value)){?>checked="checked"<?php }?> > Inactive   <br>
							<input type="Checkbox" value="Pending" name="sta[]" <?php if (in_array('Pending',$_smarty_tpl->getVariable('sta')->value)){?>checked="checked"<?php }?>> Pending   <br> -->
							<input type="Checkbox" value="PRefund" name="psta[]" <?php if (in_array('PRefund',$_smarty_tpl->getVariable('psta')->value)){?>checked="checked"<?php }?>> Passenger Refund <br>
							<input type="Checkbox" value="DRefund" name="psta[]" <?php if (in_array('DRefund',$_smarty_tpl->getVariable('psta')->value)){?>checked="checked"<?php }?>> Driver Refund <br>
							<input type="Checkbox" value="Paid" name="psta[]" <?php if (in_array('Paid',$_smarty_tpl->getVariable('psta')->value)){?>checked="checked"<?php }?>> Paid <br>
							<input type="Checkbox" value="Unpaid" name="psta[]" <?php if (in_array('Unpaid',$_smarty_tpl->getVariable('psta')->value)){?>checked="checked"<?php }?>> Unpaid <br>
						</td>
					</tr>
					<tr>
						<td width="5%">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="3">
							<table align="center" width="100%" border="0">
								<tbody>
									<tr>
										<td width="45%">&nbsp;</td>	
										<td width="55%">
											<!--<input type="submit" value="Search" class="btn" onclick="return checkvalid(document.frmadd);" title="Search"/> -->	
											<input type="button" value="Search" class="btn" onclick="return checkvalid(1);" title="Search"/>
											<input type="button" value="Export Member List" onclick="return checkvalid(2);" class="btnalt" />
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table> 
		</form>
		<form name="frmlistmain" id="frmlistmain"  action="index.php?file=m-member_a" method="post">        
			<table width="100%" border="0">
				<input type="hidden" name="action" id="action" value="" />
				<input  type="hidden" name="iBookingId" value=""/>
				<thead>
					<tr>
						<th nowrap><a href="javascript:void(0);" onClick="Redirect('index.php?file=r-money_reports&mode=view&sortby=1&order=<?php if ($_smarty_tpl->getVariable('order')->value==0){?>1<?php }else{ ?>0<?php }?>');">Booking No. #</a><?php if ($_smarty_tpl->getVariable('sortby')->value==1){?>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
<?php if ($_smarty_tpl->getVariable('order')->value==0){?>icons/up_active.png<?php }else{ ?>icons/down_active.png<?php }?>"><?php }?></th>   
						<th>Member</th>   					
						<th nowrap><a href="javascript:void(0);" onClick="Redirect('index.php?file=r-money_reports&mode=view&sortby=3&order=<?php if ($_smarty_tpl->getVariable('order')->value==0){?>1<?php }else{ ?>0<?php }?>');">Booking Date</a><?php if ($_smarty_tpl->getVariable('sortby')->value==3){?>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
<?php if ($_smarty_tpl->getVariable('order')->value==0){?>icons/up_active.png<?php }else{ ?>icons/down_active.png<?php }?>"><?php }?></th>
						<th><a href="javascript:void(0);" onClick="Redirect('index.php?file=r-money_reports&mode=view&sortby=4&order=<?php if ($_smarty_tpl->getVariable('order')->value==0){?>1<?php }else{ ?>0<?php }?>');">Trip Details</a><?php if ($_smarty_tpl->getVariable('sortby')->value==4){?>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
<?php if ($_smarty_tpl->getVariable('order')->value==0){?>icons/up_active.png<?php }else{ ?>icons/down_active.png<?php }?>"><?php }?></th>
						<th><a href="javascript:void(0);" onClick="Redirect('index.php?file=r-money_reports&mode=view&sortby=6&order=<?php if ($_smarty_tpl->getVariable('order')->value==0){?>1<?php }else{ ?>0<?php }?>');">Amount</a><?php if ($_smarty_tpl->getVariable('sortby')->value==6){?>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
<?php if ($_smarty_tpl->getVariable('order')->value==0){?>icons/up_active.png<?php }else{ ?>icons/down_active.png<?php }?>"><?php }?></th>
						<th><a href="javascript:void(0);" onClick="Redirect('index.php?file=r-money_reports&mode=view&sortby=8&order=<?php if ($_smarty_tpl->getVariable('order')->value==0){?>1<?php }else{ ?>0<?php }?>');">Commision</a><?php if ($_smarty_tpl->getVariable('sortby')->value==8){?>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
<?php if ($_smarty_tpl->getVariable('order')->value==0){?>icons/up_active.png<?php }else{ ?>icons/down_active.png<?php }?>"><?php }?></th>
						<th><a href="javascript:void(0);" onClick="Redirect('index.php?file=r-money_reports&mode=view&sortby=9&order=<?php if ($_smarty_tpl->getVariable('order')->value==0){?>1<?php }else{ ?>0<?php }?>');">Refund</a><?php if ($_smarty_tpl->getVariable('sortby')->value==9){?>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
<?php if ($_smarty_tpl->getVariable('order')->value==0){?>icons/up_active.png<?php }else{ ?>icons/down_active.png<?php }?>"><?php }?></th>
						<th>Cancellled By</th>
						<th>Payment Status</th>
					</tr>
				</thead>
				<tbody>
					<?php if (count($_smarty_tpl->getVariable('db_order_all')->value)>0){?>            
					<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_order_all')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
					<tr>
						<td style="height:50px;"><a target="_BLANK" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=bo-bookings&mode=edit&iBookingId=<?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iBookingId'];?>
" title=""><?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBookingNo'];?>
</a></td>
						<td><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=m-member&mode=edit&iMemberId=<?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iDriverId'];?>
" target="_blank"><?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMemberName'];?>
</a></td>
						<td><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dBookingDate'],9);?>
</td>
						<td><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=ri-rideslist&mode=edit&iRideId=<?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iRideId'];?>
" target="_blank"><?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainRidePlaceDetails'];?>
</a></td>
						<td><?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fAmount'];?>
</td>
						<td><?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fCommission'];?>
</td>
						<td><?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fSiteRefundAmount'];?>
</td>
						<td><?php if ($_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eCancelBy']==''){?> Not Cancelled 
							<?php }else{ ?>
							<?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eCancelBy'];?>

						<?php }?></td>
						<td>
							<?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['status'];?>

						</td>
					</tr>
					<?php endfor; endif; ?>
					<?php }else{ ?>
					<tr>
						<td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No Record Found.</td>
					</tr>
					<?php }?>
					<tr>
						<td colspan="7" width="80%" height="20" style="border-bottom:0px solid #cccccc;font-size:13px;text-align:right;"></td>
						<td colspan="2" width="20%" height="20" style="border-bottom:1px solid #cccccc;border-left:1px solid #cccccc;font-size:13px;text-align:right;">
							<strong>Total Amount: <span id="totpaymentspan"><?php echo $_smarty_tpl->getVariable('driver_currency')->value;?>
 <?php echo $_smarty_tpl->getVariable('tot_driver_refund')->value;?>
</span></strong>
						</td>
					</tr>
					<tr>
						<td colspan="7" width="80%" height="20" style="border-bottom:0px solid #cccccc;font-size:13px;text-align:right;"></td>
						<td colspan="2" width="20%" height="20" style="border-bottom:1px solid #cccccc;border-left:1px solid #cccccc;font-size:13px;text-align:right;">
							<strong>Total Commision: <span id="totpaymentspan"><?php echo $_smarty_tpl->getVariable('driver_currency')->value;?>
 <?php echo $_smarty_tpl->getVariable('tot_site_commission')->value;?>
</span></strong>
						</td>
					</tr>
					<tr>
						<td colspan="7" width="80%" height="20" style="border-bottom:0px solid #cccccc;font-size:13px;text-align:right;"></td>
						<td colspan="2" width="20%" height="20" style="border-bottom:1px solid #cccccc;border-left:1px solid #cccccc;font-size:13px;text-align:right;">
							<strong>Total Refund: <span id="totpaymentspan"><?php echo $_smarty_tpl->getVariable('driver_currency')->value;?>
 <?php echo $_smarty_tpl->getVariable('tot_site_refund')->value;?>
</span></strong>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
		<div>
			<div class="pagination">
				<?php if (count($_smarty_tpl->getVariable('db_order_all')->value)>0){?>
				<span class="switch" style="float: left;"><?php echo $_smarty_tpl->getVariable('recmsg')->value;?>
</span>
				<?php }?>
			</div>
			<?php echo $_smarty_tpl->getVariable('page_link')->value;?>

		</div>
	<div style="clear: both;"></div></div>
</div>

<script>
	function Searchoption(){
		document.getElementById('frmsearch').submit();
	}
	function AlphaSearch(val){
		var alphavalue = val;
		var file = 'r-member_reports';
		window.location="index.php?file="+file+"&alp="+alphavalue+"&mode=view";
		return false;
	}
	function MakeAction(loopid,type){
		document.frmlist.iMemberId.value = loopid;
		document.frmlist.action.value = type;
		document.frmlist.submit();	
	}
	function hidemessage(){
		jQuery("#errormsgdiv").slideUp();
	}
	function checkvalid(val){    
		if(document.frmlist.dTDate.value < document.frmlist.dFDate.value){
			alert("From date should be lesser than To date.")
			document.frmlist.dFDate.select();
			return false;
		}
		if(val==1){
			document.frmlist.action="index.php?file=r-money_reports&mode=view";
			document.frmlist.submit();	
			}else{
			document.frmlist.action="index.php?file=r-export_money_details&mode=edit";
			document.frmlist.submit();	
		}
	}
	
	function todayDate(dt,df)
	{
		document.frmlist.elements[dt].value='<?php echo date('Y-m-d');?>
';
		document.frmlist.elements[df].value='<?php echo date('Y-m-d');?>
'; 
	}
	function yesterdayDate(dt,df)
	{
		document.frmlist.elements[dt].value='<?php echo $_smarty_tpl->getVariable('yesterday')->value;?>
';
		document.frmlist.elements[df].value='<?php echo $_smarty_tpl->getVariable('yesterday')->value;?>
';	
	}
	function currentweekDate(dt,df)
	{ 
		document.frmlist.elements[dt].value='<?php echo $_smarty_tpl->getVariable('currweekFDate')->value;?>
';
		document.frmlist.elements[df].value='<?php echo $_smarty_tpl->getVariable('currweekTDate')->value;?>
';	
	}
	function previousweekDate(dt,df)
	{
		document.frmlist.elements[dt].value='<?php echo $_smarty_tpl->getVariable('prevweekFDate')->value;?>
';
		document.frmlist.elements[df].value='<?php echo $_smarty_tpl->getVariable('prevweekTDate')->value;?>
';	
	}
	function currentmonthDate(dt,df)
	{
		document.frmlist.elements[dt].value='<?php echo $_smarty_tpl->getVariable('currmonthFDate')->value;?>
';
		document.frmlist.elements[df].value='<?php echo $_smarty_tpl->getVariable('currmonthTDate')->value;?>
';
	}
	function previousmonthDate(dt,df)
	{
		document.frmlist.elements[dt].value='<?php echo $_smarty_tpl->getVariable('prevmonthFDate')->value;?>
';
		document.frmlist.elements[df].value='<?php echo $_smarty_tpl->getVariable('prevmonthTDate')->value;?>
';	
	}
	function currentyearDate(dt,df)
	{
		document.frmlist.elements[dt].value='<?php echo $_smarty_tpl->getVariable('curryearFDate')->value;?>
';
		document.frmlist.elements[df].value='<?php echo $_smarty_tpl->getVariable('curryearTDate')->value;?>
';	
	}
	function previousyearDate(dt,df)
	{
		document.frmlist.elements[dt].value='<?php echo $_smarty_tpl->getVariable('prevyearFDate')->value;?>
';
		document.frmlist.elements[df].value='<?php echo $_smarty_tpl->getVariable('prevyearTDate')->value;?>
';	
	}
	function checkAll()
	{ 
		var rs = (document.frmlist.abc.checked)?true:false;
		
		for(i=0;i<document.frmlist.elements.length;i++)
		{
			if(document.frmlist.elements[i].id == 'iId')
			{
				document.frmlist.elements[i].checked = rs;
			}
			
		}  
	}
</script>
