<?php /* Smarty version Smarty-3.0.7, created on 2015-11-02 15:45:34
         compiled from "/home/www/cargosharing1/cargoadmin/templates/utility/email_templates.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1272977801563737c6e150a1-23521466%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ab63898d1d4d2d49ede9d6904872cc8cc563e2e1' => 
    array (
      0 => '/home/www/cargosharing1/cargoadmin/templates/utility/email_templates.tpl',
      1 => 1432032826,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1272977801563737c6e150a1-23521466',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_fckeditor')) include '/home/www/cargosharing1/libraries/general/smarty/plugins/function.fckeditor.php';
?><script type="text/javascript" src="../plugins/ckeditor/ckeditor.js"></script>
<div class="contentcontainer" id="tabs">
	<div class="headings">
		<?php if ($_smarty_tpl->getVariable('mode')->value=='edit'){?>
			<h2 class="left">Edit Email Template ( <?php echo $_smarty_tpl->getVariable('db_email_templates')->value[0]['vEmail_Code'];?>
 )
                    
            </h2>
        <?php }else{ ?>
			<h2 class="left">Add Email Template</h2>
        <?php }?>
	</div>
	<div class="contentbox" id="tabs-1">
		<?php if ($_smarty_tpl->getVariable('var_msg')->value!=''){?>
		<div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_success.png" title="Success" />
              <?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
</p> 
     </div>
     <?php }?>
    <form id="frmadd" name="frmadd" action="index.php?file=u-email_templates_a" method="post">
    <input type="hidden" name="iEmailId" id="iEmailId" value="<?php echo $_smarty_tpl->getVariable('iEmailId')->value;?>
" />
    <input type="hidden" name="action" id="action" value="<?php echo $_smarty_tpl->getVariable('mode')->value;?>
" />
    <p>
  		<label for="textfield"><strong>Email Code :</strong></label>
  		<input type="text" id="vEmail_Code" name="Data[vEmail_Code]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_email_templates')->value[0]['vEmail_Code'];?>
" readonly title="Email Code"/> 
	  </p>

	  <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['name'] = 'rrb';
$_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_languagemas')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['rrb']['total']);
?>              
  		<p>
  			<label for="textfield"><strong>Subject [<?php echo $_smarty_tpl->getVariable('db_languagemas')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rrb']['index']]['vTitle'];?>
]:</strong></label>
  			<input type="text" id="vSubject_<?php echo $_smarty_tpl->getVariable('db_languagemas')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rrb']['index']]['vCode'];?>
" name="vSubject_<?php echo $_smarty_tpl->getVariable('db_languagemas')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rrb']['index']]['vCode'];?>
" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_languagemas')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rrb']['index']]['ttl'];?>
" title="Email Subject [<?php echo $_smarty_tpl->getVariable('db_languagemas')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rrb']['index']]['vTitle'];?>
]"/>
        </p>
  			
  		<p>
  			<label for="textfield"><strong>Email Template Body [<?php echo $_smarty_tpl->getVariable('db_languagemas')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rrb']['index']]['vTitle'];?>
] :</strong></label>
  			<?php echo $_smarty_tpl->getVariable('db_languagemas')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rrb']['index']]['editor'];?>

  		</p>    			
        <?php endfor; endif; ?> 
	<!--
    <p>
			<label for="textfield"><strong>Subject [English]:</strong></label>
			<input type="text" id="vSubject_EN" name="Data[vSubject_EN]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_email_templates')->value[0]['vSubject_EN'];?>
" lang="*" title="Email Subject English"/>
	</p>
	
    <p>        
			<label for="textfield"><strong>Email Template Body [English]:</strong></label>
      <?php echo smarty_function_fckeditor(array('BasePath'=>"../plugins/FCKeditor/",'InstanceName'=>"Data[vBody_EN]",'Width'=>"850px",'Height'=>"550px",'Value'=>($_smarty_tpl->getVariable('vBody_EN')->value)),$_smarty_tpl);?>

		</p>
    <p>
			<label for="textfield"><strong>Subject [Norwegian]:</strong></label>
			<input type="text" id="vSubject_NO" name="Data[vSubject_NO]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_email_templates')->value[0]['vSubject_NO'];?>
" lang="*" title="Email Subject Norwegian"/>
		</p>
    <p>        
			<label for="textfield"><strong>Email Template Body [Norwegian]:</strong></label>
      <?php echo smarty_function_fckeditor(array('BasePath'=>"../plugins/FCKeditor/",'InstanceName'=>"Data[vBody_NO]",'Width'=>"850px",'Height'=>"550px",'Value'=>($_smarty_tpl->getVariable('vBody_NO')->value)),$_smarty_tpl);?>

		</p>
     <p>
			<label for="textfield"><strong>Subject [Polish]:</strong></label>
			<input type="text" id="vSubject_PO" name="Data[vSubject_PO]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_email_templates')->value[0]['vSubject_PO'];?>
" lang="*" title="Email Subject Polish"/>
		</p>
    <p>        
			<label for="textfield"><strong>Email Template Body [Polish]:</strong></label>
      <?php echo smarty_function_fckeditor(array('BasePath'=>"../plugins/FCKeditor/",'InstanceName'=>"Data[vBody_PO]",'Width'=>"850px",'Height'=>"550px",'Value'=>($_smarty_tpl->getVariable('vBody_PO')->value)),$_smarty_tpl);?>

		</p>
    <p>
			<label for="textfield"><strong>Subject [French]:</strong></label>
			<input type="text" id="vSubject_FN" name="Data[vSubject_FN]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_email_templates')->value[0]['vSubject_FN'];?>
" lang="*" title="Email Subject French"/>
		</p>
    <p>        
  		<label for="textfield"><strong>Email Template Body [French]:</strong></label>
      <?php echo smarty_function_fckeditor(array('BasePath'=>"../plugins/FCKeditor/",'InstanceName'=>"Data[vBody_FN]",'Width'=>"850px",'Height'=>"550px",'Value'=>($_smarty_tpl->getVariable('vBody_FN')->value)),$_smarty_tpl);?>

		</p>
    <p>
			<label for="textfield"><strong>Subject [Russian]:</strong></label>
			<input type="text" id="vSubject_RS" name="Data[vSubject_RS]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_email_templates')->value[0]['vSubject_RS'];?>
" lang="*" title="Email Subject Russian"/>
		</p>
    <p>        
			<label for="textfield"><strong>Email Template Body [Russian]:</strong></label>
      <?php echo smarty_function_fckeditor(array('BasePath'=>"../plugins/FCKeditor/",'InstanceName'=>"Data[vBody_RS]",'Width'=>"850px",'Height'=>"550px",'Value'=>($_smarty_tpl->getVariable('vBody_RS')->value)),$_smarty_tpl);?>

		</p>		
    <p>
			<label for="textfield"><strong>Subject [Swedish]:</strong></label>
			<input type="text" id="vSubject_SW" name="Data[vSubject_SW]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_email_templates')->value[0]['vSubject_SW'];?>
" lang="*" title="Email Subject Swedish"/>
		</p>
    <p>        
			<label for="textfield"><strong>Email Template Body [Swedish]:</strong></label>
      <?php echo smarty_function_fckeditor(array('BasePath'=>"../plugins/FCKeditor/",'InstanceName'=>"Data[vBody_SW]",'Width'=>"850px",'Height'=>"550px",'Value'=>($_smarty_tpl->getVariable('vBody_SW')->value)),$_smarty_tpl);?>

		</p>
    <p>
			<label for="textfield"><strong>Subject [Danish]:</strong></label>
			<input type="text" id="vSubject_DN" name="Data[vSubject_DN]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_email_templates')->value[0]['vSubject_DN'];?>
" lang="*" title="Email Subject Danish"/>
		</p>
		<p>        
		 	<label for="textfield"><strong>Email Template Body [Danish]:</strong></label>
      <?php echo smarty_function_fckeditor(array('BasePath'=>"../plugins/FCKeditor/",'InstanceName'=>"Data[vBody_DN]",'Width'=>"850px",'Height'=>"550px",'Value'=>($_smarty_tpl->getVariable('vBody_DN')->value)),$_smarty_tpl);?>

		</p>
    <p>
			<label for="textfield"><strong>Subject [Finnish]:</strong></label>
			<input type="text" id="vSubject_FI" name="Data[vSubject_FI]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_email_templates')->value[0]['vSubject_FI'];?>
" lang="*" title="Email Subject Finnish"/>
		</p>
		<p>        
		 	<label for="textfield"><strong>Email Template Body [Finnish]:</strong></label>
      <?php echo smarty_function_fckeditor(array('BasePath'=>"../plugins/FCKeditor/",'InstanceName'=>"Data[vBody_FI]",'Width'=>"850px",'Height'=>"550px",'Value'=>($_smarty_tpl->getVariable('vBody_FI')->value)),$_smarty_tpl);?>

		</p>			
    <p>
			<label for="textfield"><strong>Subject [Latvian]:</strong></label>
			<input type="text" id="vSubject_LV" name="Data[vSubject_LV]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_email_templates')->value[0]['vSubject_LV'];?>
" lang="*" title="Email Subject Latvian"/>
		</p>
		<p>        
		 	<label for="textfield"><strong>Email Template Body [Latvian]:</strong></label>
      <?php echo smarty_function_fckeditor(array('BasePath'=>"../plugins/FCKeditor/",'InstanceName'=>"Data[vBody_LV]",'Width'=>"850px",'Height'=>"550px",'Value'=>($_smarty_tpl->getVariable('vBody_LV')->value)),$_smarty_tpl);?>

		</p>	
    <p>
			<label for="textfield"><strong>Subject [Estonian]:</strong></label>
			<input type="text" id="vSubject_EE" name="Data[vSubject_EE]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_email_templates')->value[0]['vSubject_EE'];?>
" lang="*" title="Email Subject Estonian"/>
		</p>
    <p>        
			<label for="textfield"><strong>Email Template Body [Estonian]:</strong></label>
      <?php echo smarty_function_fckeditor(array('BasePath'=>"../plugins/FCKeditor/",'InstanceName'=>"Data[vBody_EE]",'Width'=>"850px",'Height'=>"550px",'Value'=>($_smarty_tpl->getVariable('vBody_EE')->value)),$_smarty_tpl);?>

		</p>	
    <p>
  		<label for="textfield"><strong>Subject [Lithuanian]:</strong></label>
			<input type="text" id="vSubject_LT" name="Data[vSubject_LT]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_email_templates')->value[0]['vSubject_LT'];?>
" lang="*" title="Email Subject Lithuanian"/>
		</p>
    <p>        
			<label for="textfield"><strong>Email Template Body [Lithuanian]:</strong></label>
      <?php echo smarty_function_fckeditor(array('BasePath'=>"../plugins/FCKeditor/",'InstanceName'=>"Data[vBody_LT]",'Width'=>"850px",'Height'=>"550px",'Value'=>($_smarty_tpl->getVariable('vBody_LT')->value)),$_smarty_tpl);?>

		</p>	
    <p>
  		<label for="textfield"><strong>Subject [German]:</strong></label>
			<input type="text" id="vSubject_DE" name="Data[vSubject_DE]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_email_templates')->value[0]['vSubject_DE'];?>
" lang="*" title="Email Subject German"/>
	  </p>
    <p>        
			<label for="textfield"><strong>Email Template Body [German]:</strong></label>
      <?php echo smarty_function_fckeditor(array('BasePath'=>"../plugins/FCKeditor/",'InstanceName'=>"Data[vBody_DE]",'Width'=>"850px",'Height'=>"550px",'Value'=>($_smarty_tpl->getVariable('vBody_DE')->value)),$_smarty_tpl);?>

		</p>	
    <p>
  		<label for="textfield"><strong>Subject [Spanish]:</strong></label>
			<input type="text" id="vSubject_ES" name="Data[vSubject_ES]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_email_templates')->value[0]['vSubject_ES'];?>
" lang="*" title="Email Subject Spanish"/>
	  </p>
    <p>        
			<label for="textfield"><strong>Email Template Body [Spanish]:</strong></label>
      <?php echo smarty_function_fckeditor(array('BasePath'=>"../plugins/FCKeditor/",'InstanceName'=>"Data[vBody_ES]",'Width'=>"850px",'Height'=>"550px",'Value'=>($_smarty_tpl->getVariable('vBody_ES')->value)),$_smarty_tpl);?>

		</p> 
	-->
				<input type="submit" value="Save" class="btn" onclick="return validate(document.frmadd);" title="Save"/>
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
			</form>
	</div>
</div>

<script>
function redirectcancel()
{
    window.location="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-email_templates&mode=view";
    return false;
    
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
