<?php /* Smarty version Smarty-3.0.7, created on 2016-01-21 14:44:19
         compiled from "/home/www/shipcliq/bbcsadmin/templates/left.tpl" */ ?>
<?php /*%%SmartyHeaderCode:124453151256a0eec3f26ce7-67013407%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b6253c55c8424af7012e988fd345ada460457d9d' => 
    array (
      0 => '/home/www/shipcliq/bbcsadmin/templates/left.tpl',
      1 => 1432031998,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '124453151256a0eec3f26ce7-67013407',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="user">
	<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
avatar.png" width="44" height="44" class="hoverimg" alt="Avatar" />
	<p>Logged in as:</p>
	<p class="username"><?php echo $_smarty_tpl->getVariable('sess_vFirstName')->value;?>
 <?php echo $_smarty_tpl->getVariable('sess_vLastName')->value;?>
</p>
	<p class="userbtn"><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=ad-administrator&mode=edit&iAdminId=<?php echo $_smarty_tpl->getVariable('sess_iAdminId')->value;?>
" title="<?php echo $_smarty_tpl->getVariable('sess_vFirstName')->value;?>
 <?php echo $_smarty_tpl->getVariable('sess_vLastName')->value;?>
">Profile</a></p>
	<p class="userbtn"><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=au-logout" title="Log out">Log out</a></p>
</div>
<ul id="nav">
	<li>
		<a class="collapsed heading">Dashboard</a>
		<ul class="navigation">
		<?php if ($_GET['file']==''){?>
			<li class="heading selected">Dashboard</li>
		<?php }else{ ?>
			<li class="heading"><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=home-dashboard"  title="">Dashboard</a></li>
		<?php }?>
		</ul>
	</li>
	<li>
		<a class="collapsed heading">Administrators</a>
		<ul class="navigation">
			<?php if ($_GET['file']=='ad-administrator'){?>
			<li class="selected">Administrator</li>
			<?php }else{ ?>
			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=ad-administrator&mode=view"  title="">Administrator</a></li>
			<?php }?>
		</ul>
	</li>
	<li>
		<a class="collapsed heading">Product Management</a>
		<ul class="navigation">
			<?php if ($_GET['file']=='pr-category'){?>
			<li class="selected">Product Category</li>
			<?php }else{ ?>
			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=pr-category&mode=view"  title="">Product Category</a></li>
			<?php }?> 
      <?php if ($_GET['file']=='pr-attributes'){?>
			<li class="selected">Attributes</li>
			<?php }else{ ?>
			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=pr-attributes&mode=view"  title="">Attributes</a></li>
			<?php }?>			       
			<?php if ($_GET['file']=='pr-product'){?>
			<li class="selected">Products</li>
			<?php }else{ ?>
			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=pr-product&mode=view"  title="">Products</a></li>
			<?php }?>
      <?php if ($_GET['file']=='pr-currency'){?>
			<li class="selected">Currency Rates Management</li>
			<?php }else{ ?>
			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=pr-currency&mode=view"  title="">Currency Rates Management</a></li>
			<?php }?>    	
		</ul>
	</li>	
	<li>
		<a class="collapsed heading">Member Management</a>
		<ul class="navigation">
			<?php if ($_GET['file']=='m-member_group'){?>
			<li class="selected">Members Groups</li>
			<?php }else{ ?>
			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=m-member_group&mode=view"  title="">Members Groups</a></li>
			<?php }?>
      <?php if ($_GET['file']=='m-member'){?>
			<li class="selected">Members</li>
			<?php }else{ ?>
			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=m-member&mode=view"  title="">Members</a></li>
			<?php }?>
		</ul>
	</li>
    <li>
		<a class="collapsed heading">Order Management</a>
		<ul class="navigation">
			<?php if ($_GET['file']=='o-orders'){?>
			<li class="selected">Orders</li>
			<?php }else{ ?>
			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=o-orders&mode=view"  title="">Orders</a></li>
			<?php }?>
		</ul>
	</li>
  <li>
		<a class="collapsed heading">Reports</a>
		<ul class="navigation">
			<?php if ($_GET['file']=='r-report'){?>
			<li class="selected">Order Report</li>
			<?php }else{ ?>
			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=r-order_report&mode=view"  title="">Order Report</a></li>
			<?php }?>
		</ul>
	</li>	
  <li>
		<a class="collapsed heading">Shipping Charges Mgmt.</a>
		<ul class="navigation">
			<?php if ($_GET['file']=='u-shipping'){?>
			<li class="selected">Shipping Charges Management</li>
			<?php }else{ ?>
			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-shipping&mode=view"  title="">Shipping Charges Management</a></li>
			<?php }?>
		</ul>
	</li>			
	<li>
		<a class="collapsed heading">Utility</a>
		<ul class="navigation">
     <?php if ($_GET['file']=='u-email_templates'){?>
			<li class="selected">Email templates</li>
			<?php }else{ ?>
			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-email_templates&mode=view"  title="">Email templates</a></li>
			<?php }?>			
		  <?php if ($_GET['file']=='u-coupon'){?>
			<li class="selected">Coupon Management</li>
			<?php }else{ ?>
			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-coupon&mode=view"  title="">Coupon Management</a></li>
			<?php }?>  		
			<?php if ($_GET['file']=='u-faq'){?>
				<li class="selected">FAQ</li>
			<?php }else{ ?>
				<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-faq&mode=view" title="">FAQ</a></li>
			<?php }?>
      <?php if ($_GET['file']=='u-newsletter'){?>
			<li class="selected">Newsletters</li>
			<?php }else{ ?>
			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-newsletter&mode=view"  title="">Newsletters</a></li>
			<?php }?>
      <?php if ($_GET['file']=='u-newsletter_subscriber'){?>
			<li class="selected">Newsletter Subscriber</li>
			<?php }else{ ?>
			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-newsletter_subscriber&mode=view"  title="">Newsletter Subscriber</a></li>
			<?php }?>                 		
		</ul>
	</li>
     <li>
		<a class="collapsed heading">Localization</a>
		<ul class="navigation">
			<?php if ($_GET['file']=='l-country'){?>
			<li class="selected">Country</li>
			<?php }else{ ?>
			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=l-country&mode=view"  title="">Country</a></li>
			<?php }?>
			<?php if ($_GET['file']=='l-state'){?>
			<li class="selected">State</li>
			<?php }else{ ?>
			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=l-state&mode=view"  title="">State</a></li>
			<?php }?>
		</ul>
	</li>
	<li>
		<a class="collapsed heading">Settings</a>
		<ul class="navigation">
			<?php if ($_GET['file']=='to-generalsettings'){?>
				<li class="selected">General Configration</li>
			<?php }else{ ?>
				<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=to-generalsettings&mode=edit" title="">General Configration</a></li>
			<?php }?>			
			<?php if ($_GET['file']=='to-banners'){?>
				<li class="selected">Banners</li>
			<?php }else{ ?>
				<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=to-banners&mode=view" title="">Banners</a></li>
			<?php }?>
			<?php if ($_GET['file']=='pg-staticpages'){?>
				<li class="selected">Static Pages</li>
			<?php }else{ ?>
				<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=pg-staticpages&mode=view" title="">Static Pages</a></li>
			<?php }?>
		  <?php if ($_GET['file']=='to-languagelabel'){?>
				<li class="selected">Language Label</li>
			<?php }else{ ?>
				<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=to-languagelabel&mode=view" title="">Language Label</a></li>
			<?php }?> 
		</ul>
	</li>            	     	    
</ul>	
