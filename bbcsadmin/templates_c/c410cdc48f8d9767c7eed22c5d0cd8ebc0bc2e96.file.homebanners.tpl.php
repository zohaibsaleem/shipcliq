<?php /* Smarty version Smarty-3.0.7, created on 2015-06-06 11:17:09
         compiled from "/home/www/xfetch/xfadmin/templates/tools/homebanners.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6444294255572895d7cf378-42785856%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c410cdc48f8d9767c7eed22c5d0cd8ebc0bc2e96' => 
    array (
      0 => '/home/www/xfetch/xfadmin/templates/tools/homebanners.tpl',
      1 => 1432032830,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6444294255572895d7cf378-42785856',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<script language="JavaScript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tcp_javascript'];?>
validate.js"></script>
<div class="contentcontainer" id="tabs">
	<div class="headings">
		<?php if ($_smarty_tpl->getVariable('mode')->value=='edit'){?>
			<h2 class="left">Edit Home Page Banners</h2>
        <?php }else{ ?>
			<h2 class="left">Add Home Page Banners</h2>
        <?php }?>
	</div>
	<div class="contentbox" id="tabs-1">
        <?php if ($_smarty_tpl->getVariable('var_msg')->value!=''){?>
			<div class="status error" id="errormsgdiv"> 
				<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
				<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_error.png" title="Success" />
				<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
</p> 
			</div>     
			<div></div>
		<?php }?>
		<?php if ($_GET['var_msg_new']!=''){?>
			<div class="status success" id="errormsgdiv"> 
				<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
				<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_success.png" title="Success" />
				<?php echo $_GET['var_msg_new'];?>
</p> 
			</div>     
			<div></div>
		<?php }?>
		<?php if ($_smarty_tpl->getVariable('var_msg_error')->value!=''){?>
			<div class="status error" id="errormsgdiv"> 
				<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
				<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_error.png" title="Success" />
				<?php echo $_smarty_tpl->getVariable('var_msg_error')->value;?>
</p> 
			</div>     
			<div></div>
		<?php }?>
		<form id="frmadd" name="frmadd" action="index.php?file=to-homebanners_a" method="post" enctype="multipart/form-data">
            <input type="hidden" name="iBannerId" id="iBannerId" value="<?php echo $_smarty_tpl->getVariable('iBannerId')->value;?>
" />
            <input type="hidden" name="action" id="action" value="<?php echo $_smarty_tpl->getVariable('mode')->value;?>
" />
            <input type="hidden" name="eSection" id="eSection" value="Home" />
            <input type="hidden" name="imagemode" id="imagemode"/>
			<p>
				<label for="textfield"><strong>Select Type :<em>*</em></strong></label>
				<select id="eType" name="Data[eType]" onchange="getType(this.value);">
					<option value="Image" <?php if ($_smarty_tpl->getVariable('db_design')->value[0]['eType']=='Image'){?> selected <?php }?>>Image</option>
					<option value="Video" <?php if ($_smarty_tpl->getVariable('db_design')->value[0]['eType']=='Video'){?> selected <?php }?>>Video</option>
				</select>
			</p>
			<p>
				<label for="textfield"><strong>Title :<em>*</em></strong></label>
				<input type="text" id="vTitle" name="Data[vTitle]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_design')->value[0]['vTitle'];?>
" lang="*" title="Title"/>
			</p>
			<p id="imgurl">
				<label for="textfield"><strong>URL :</strong></label>
				<textarea id="tURL" name="Data[tURL]" class="inputbox" title="URL"><?php echo $_smarty_tpl->getVariable('db_design')->value[0]['tURL'];?>
</textarea>
				<br />
				[Note: Please include <b>http://</b> in URL. Else it will not work properly on site.]
			</p>
			<p id="img">
				<label for="textfield"><strong>Upload Banner :</strong></label>
				<input type="file" id="vImage" name="Data[vImage]" class="inputbox" title="Design" title="Upload Banner" />
				<?php if ($_smarty_tpl->getVariable('db_design')->value[0]['thumb_url']!=''){?>
					&nbsp;&nbsp;[<a href="<?php echo $_smarty_tpl->getVariable('db_design')->value[0]['thumb_url'];?>
" target="_blank"  title="<?php echo $_smarty_tpl->getVariable('db_design')->value[0]['vTitle'];?>
" rel="[images]">View</a>]
					&nbsp;&nbsp;[<a href="javascript:void(0);" onclick="confirm_delete();" title="Delete Image">Delete</a>]
				<?php }?>
				<br>[Note : Please upload Home  Page Top banner of width : 1320 and height : 666]
				<!--	
				<br>[Note : Please upload Inner Page banner of width : 1021 and height : 324]
				<br>[Note : Please upload Home  Page Middle banner of width : 477 and height : 141]
				<br>[Note : Please upload Home  Page Bottom banner of width : 321 and height : 171]
				-->	
				<!--<br>[Note: Please upload Left - Right banner of width : 112px and height : 600px]-->  
				<br>[Note: Supported File Types are *.jpg,*.jpeg,*.png,*.gif]  
			</p>
			<p id="cd" style="display:none;">
				<label for="textfield"><strong>Code :</strong></label>
				<textarea id="tCode" name="Data[tCode]" class="inputbox" title="Code"><?php echo $_smarty_tpl->getVariable('db_design')->value[0]['tCode'];?>
</textarea>
			</p> 				
			<p id="imgord">        			
				<label for="textfield"><strong>Display Order :</strong></label>
				<select id="iDisplayOrder" name="Data[iDisplayOrder]" lang="" title="Category Display Order" />
					<option value="">Please Select Display Order</option>
					<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('num_rows')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
					 <option <?php if ($_smarty_tpl->getVariable('db_design')->value[0]['iDisplayOrder']==$_smarty_tpl->getVariable('smarty')->value['section']['i']['index_next']){?>selected<?php }?> value="<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index_next'];?>
"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index_next'];?>
</option>
					<?php endfor; endif; ?>
				</select>
			</p>
			<p>
				<label for="textfield"><strong>Status :</strong></label>
				<select id="eStatus" name="Data[eStatus]">
					<option value="Active" <?php if ($_smarty_tpl->getVariable('db_design')->value[0]['eStatus']=='Active'){?>selected<?php }?>>Active</option>
					<option value="Inactive" <?php if ($_smarty_tpl->getVariable('db_design')->value[0]['eStatus']=='Inactive'){?>selected<?php }?>>Inactive</option>
				</select>
			</p>
			<?php if ($_smarty_tpl->getVariable('mode')->value=='edit'){?>
				<input type="submit" value="Save Banner" class="btn" onclick="return validate(document.frmadd);" title="Save Banner"/>
			<?php }else{ ?>
				<input type="submit" value="Add Banner" class="btn" onclick="return validate(document.frmadd);" title="Add Banner"/>
  			<?php }?>
			<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
		</form>
	</div>
</div>

<script>
function redirectcancel()
{
    window.location="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=to-homebanners&mode=view";
    return false;
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
function getType(val)
{
	//alert(val);
  if(val=="Image")
  {
    document.getElementById('tCode').value='';
    document.getElementById('cd').style.display='none';
    document.getElementById('img').style.display='';
	document.getElementById('imgurl').style.display='block';
	document.getElementById('imgord').style.display='block';
  }
  else if(val=="Video")
  {
    document.getElementById('vImage').value='';
    document.getElementById('cd').style.display='';
    document.getElementById('img').style.display='none';
	document.getElementById('imgurl').style.display='none';
	document.getElementById('imgord').style.display='none';
 }
}
function confirm_delete()
{
   ans = confirm('Are you sure for Delete Image?');
   if(ans == true)
   {
    document.frmadd.imagemode.value = 'confirm_delete';
    document.frmadd.submit();
   }
   else
   {
    return false;
   }
}
if('<?php echo $_smarty_tpl->getVariable('mode')->value;?>
' == 'edit'){
  getType('<?php echo $_smarty_tpl->getVariable('db_design')->value[0]['eType'];?>
');
}
</script>

