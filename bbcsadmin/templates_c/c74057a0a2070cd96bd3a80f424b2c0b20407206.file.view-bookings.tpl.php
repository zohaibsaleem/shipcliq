<?php /* Smarty version Smarty-3.0.7, created on 2015-05-28 12:38:40
         compiled from "/home/www/xfetch/xfadmin/templates/booking/view-bookings.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2913680505566bef830fa13-95605469%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c74057a0a2070cd96bd3a80f424b2c0b20407206' => 
    array (
      0 => '/home/www/xfetch/xfadmin/templates/booking/view-bookings.tpl',
      1 => 1432796796,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2913680505566bef830fa13-95605469',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<link type="text/css" rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
dhtmlgoodies_calendar.css" media="screen">
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
dhtmlgoodies_calendar.js"></script>
<script language="JavaScript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tcp_javascript'];?>
jlist.js"></script>
<div class="contentcontainer">
	<div class="headings altheading">
		<h2>Trips Booking</h2>
	</div>
	<div class="contentbox">
    <?php if ($_smarty_tpl->getVariable('var_msg_new')->value!=''){?>
     <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_success.png" title="Success" />
              <?php echo $_smarty_tpl->getVariable('var_msg_new')->value;?>
</p> 
     </div>     
    <div></div>
    <?php }elseif($_smarty_tpl->getVariable('var_msg')->value!=''){?>
    <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_success.png" title="Success" />
              <?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
</p> 
     </div>     
    <div></div>
    <?php }?>
		<form name="frmsearch" id="frmsearch" action="" method="post">
    <input type="hidden" name="paymenttype" id="paymenttype" value="<?php echo $_smarty_tpl->getVariable('paymenttype')->value;?>
">
    <table width="100%" border="0">
		<tbody>
			<tr>
				<td width="10%"><label for="textfield"><strong>Search:</strong></label></td>
				<td width="10%"><input type="Text" id="keyword" name="keyword" value="<?php echo $_smarty_tpl->getVariable('keyword')->value;?>
"  class="inputbox" /></td>
				<td width="10%" >
					<select name="option" id="option">
						<option  value="vBookingNo" <?php if ($_smarty_tpl->getVariable('option')->value=='vBookingNo'){?> selected <?php }?>>Booking No</option>
						<option  value="concat(vBookerFirstName,' ',vBookerLastName)" <?php if ($_smarty_tpl->getVariable('option')->value=="concat(vBookerFirstName,\' \',vBookerLastName)"){?> selected <?php }?>>Sender Name</option>
						<option  value="concat(vDriverFirstName,' ',vDriverLastName)" <?php if ($_smarty_tpl->getVariable('option')->value=="concat(vDriverFirstName,\' \',vDriverLastName)"){?> selected <?php }?>>Traveler Name</option>
			<!--			<option value="b.ePaymentPaid">Payment Status</option> -->
					</select>
				</td>
				<td width="60%">
      <!--    <input type="button" value="Search" class="btnalt" id="Search" name="Search" title="Search" onclick="Searchoption();"/>
          &nbsp;
          <input type="button" value="Export Trips Booking" onclick="return export_ride();" class="btnalt" /> -->
          </td>
			</tr>	
      <tr>
        <td width="10%"><label for="textfield"><strong>Date :</strong></label></td>
        <td width="10%"> 
          <input type="text" Readonly id="dFDate" name="dFDate" style="width:100px;" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('startdate')->value;?>
" lang="" title="Start Date"/>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
cal-icon.gif" style="cursor:pointer" onclick="displayCalendar(document.getElementById('dFDate'),'yyyy-mm-dd',this)" /> &nbsp;
          To &nbsp; <input type="text" Readonly id="dTDate" name="dTDate" style="width:100px;" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('enddate')->value;?>
" lang="" title="End Date"/>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
cal-icon.gif" style="cursor:pointer" onclick="displayCalendar(document.getElementById('dTDate'),'yyyy-mm-dd',this)" />
        </td>
        <td width="10%"></td>
        <td width="60%"></td>
      </tr>
      <tr>
        <td></td>
        <td>
         <input type="button" value="Search" class="btnalt" id="Search" name="Search" title="Search" onclick="Searchoption();"/>
          &nbsp;
          <input type="button" value="Export Trips Booking" onclick="return export_ride();" class="btnalt" />
        </td>
      </tr>
      <?php if (@PAYMENT_OPTION=='PayPal'){?>
			<tr>
				<td colspan="7" align="center">
          <input type="button" value="Unpaid" onclick="checkvalid(1);" class="<?php if ($_smarty_tpl->getVariable('paymenttype')->value=='No'){?>btn<?php }else{ ?>btnalt<?php }?>" />
          <input type="button" value="Paid" onclick="checkvalid(2);" class="<?php if ($_smarty_tpl->getVariable('paymenttype')->value=='Yes'){?>btn<?php }else{ ?>btnalt<?php }?>" />
          <input type="button" value="Both" onclick="checkvalid(3);" class="<?php if ($_smarty_tpl->getVariable('paymenttype')->value=='both'){?>btn<?php }else{ ?>btnalt<?php }?>" />
				</td>
			</tr>
			<?php }?>
		</tbody>			
		</table> 
    </form>
   
    <form name="frmlist" id="frmlist"  action="index.php?file=bo-bookings_a" method="post">
   	<table width="100%" border="0">
		<input type="hidden" name="action" id="action" value="" />
    <input type="hidden" name="iBookingId" value=""/>
    <thead>
			<tr>
				<th width="11%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=bo-bookings&mode=view&sortby=1&order=<?php if ($_smarty_tpl->getVariable('order')->value==0){?>1<?php }else{ ?>0<?php }?>');">Booking No</a><?php if ($_smarty_tpl->getVariable('sortby')->value==1){?>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
<?php if ($_smarty_tpl->getVariable('order')->value==0){?>icons/up_active.png<?php }else{ ?>icons/down_active.png<?php }?>"><?php }?></th>
				<th width="12%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=bo-bookings&mode=view&sortby=2&order=<?php if ($_smarty_tpl->getVariable('order')->value==0){?>1<?php }else{ ?>0<?php }?>');">Sender Name</a><?php if ($_smarty_tpl->getVariable('sortby')->value==2){?>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
<?php if ($_smarty_tpl->getVariable('order')->value==0){?>icons/up_active.png<?php }else{ ?>icons/down_active.png<?php }?>"><?php }?></th>
				<th width="15%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=bo-bookings&mode=view&sortby=5&order=<?php if ($_smarty_tpl->getVariable('order')->value==0){?>1<?php }else{ ?>0<?php }?>');">Traveler Name</a><?php if ($_smarty_tpl->getVariable('sortby')->value==5){?>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
<?php if ($_smarty_tpl->getVariable('order')->value==0){?>icons/up_active.png<?php }else{ ?>icons/down_active.png<?php }?>"><?php }?></th>
				<!--<th width="10%">No of Seats</th>
				<th width="10%">Trips</th> 
				<th width="10%">Amount</th> -->
				<!--<th width="10%">Categories</th>-->
				<th width="10%">Document</th>
				<th width="10%">Box</th>
				<th width="10%">Luggage</th>
				<th width="10%">Platform fee</th>
				<th width="10%">Vat</th>
				<th width="10%">Amount</th>
				<th width="17%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=bo-bookings&mode=view&sortby=4&order=<?php if ($_smarty_tpl->getVariable('order')->value==0){?>1<?php }else{ ?>0<?php }?>');">Booking Date</a><?php if ($_smarty_tpl->getVariable('sortby')->value==4){?>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
<?php if ($_smarty_tpl->getVariable('order')->value==0){?>icons/down_active.png<?php }else{ ?>icons/up_active.png<?php }?>"><?php }?></th>
		<!--		<th width="15%">SenderPaymentStatus</th> -->
				<th width="15%">PaymentStatus</th>
				<th>Invoice</td>
				<th width="5%">Action</th>
				<th width="5%"><input type="checkbox" id="check_all" name="check_all" onclick="checkAll(document.frmlist);"/></th>
			</tr>
		</thead>
		<tbody>
    <?php if (count($_smarty_tpl->getVariable('db_book_all')->value)>0){?>            
		<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_book_all')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
     <tr>
			<td <?php if ($_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eStatus']=='Cencelled'){?> style="background:none repeat scroll 0 0 #F6CECE;"<?php }?>><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=bo-bookings&mode=edit&iBookingId=<?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iBookingId'];?>
" title=""><?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBookingNo'];?>
</a></td>
			<td <?php if ($_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eStatus']=='Cencelled'){?> style="background:none repeat scroll 0 0 #F6CECE;"<?php }?>><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=m-member&mode=edit&iMemberId=<?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iBookerId'];?>
"><?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBookerFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBookerLastName'];?>
</a></td>
			<td <?php if ($_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eStatus']=='Cencelled'){?> style="background:none repeat scroll 0 0 #F6CECE;"<?php }?>><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=m-member&mode=edit&iMemberId=<?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iDriverId'];?>
"><?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vDriverFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vDriverLastName'];?>
</a></td>
			<!-- <td <?php if ($_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eStatus']=='Cencelled'){?> style="background:none repeat scroll 0 0 #F6CECE;"<?php }?>><div style="text-align:center;"><?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iNoOfSeats'];?>
</div></td>
			<td><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=ri-rideslist&mode=edit&iRideId=<?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iRideId'];?>
">view</a></td> 
			<td <?php if ($_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eStatus']=='Cencelled'){?> style="background:none repeat scroll 0 0 #F6CECE;"<?php }?>><?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBookerCurrencyCode'];?>
 <?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fAmount'];?>
</td> -->
			<td <?php if ($_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eStatus']=='Cencelled'){?> style="background:none repeat scroll 0 0 #F6CECE;"<?php }?>>
			   <?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBookerCurrencyCode'];?>
 <?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fDocumentPrice'];?>

          <!--<?php if ($_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fDocumentPrice']>'0.00'){?>
					   <!-- <img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
document-icon.png">--**
					   <?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBookerCurrencyCode'];?>
 <?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fDocPrice'];?>
 
					<?php }elseif($_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fDocumentPrice']<'0.00'||$_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fDocumentPrice']=='0.00'){?>
					    --
				  <?php }?>-->  
			</td>
			<td <?php if ($_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eStatus']=='Cencelled'){?> style="background:none repeat scroll 0 0 #F6CECE;"<?php }?>>
			   <?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBookerCurrencyCode'];?>
 <?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fBoxPrice'];?>
			   
			   <!--<?php if ($_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fBoxPrice']>'0.00'){?>
					<!-- <img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
box-icon.png">--**
					Box (<?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBookerCurrencyCode'];?>
 <?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fBoxPrice'];?>
) 
				  <?php }?>--->
			</td>
			<td <?php if ($_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eStatus']=='Cencelled'){?> style="background:none repeat scroll 0 0 #F6CECE;"<?php }?>>
			     <?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBookerCurrencyCode'];?>
 <?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fLuggagePrice'];?>

			   <!--<?php if ($_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fLuggagePrice']>'0.00'){?>
					<!-- <img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
luggage-icon.png"> --**
					Luggage (<?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBookerCurrencyCode'];?>
 <?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fLuggagePrice'];?>
) 
				<?php }?> -->
			</td>
			<td <?php if ($_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eStatus']=='Cencelled'){?> style="background:none repeat scroll 0 0 #F6CECE;"<?php }?>>
			     <?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBookerCurrencyCode'];?>
 <?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fCommission'];?>

			</td>
			<td <?php if ($_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eStatus']=='Cencelled'){?> style="background:none repeat scroll 0 0 #F6CECE;"<?php }?>>
			     <?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBookerCurrencyCode'];?>
 <?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fVat'];?>

			</td>
			<td <?php if ($_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eStatus']=='Cencelled'){?> style="background:none repeat scroll 0 0 #F6CECE;"<?php }?>><?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBookerCurrencyCode'];?>
 <?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fAmount'];?>
</td>			
			<td <?php if ($_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eStatus']=='Cencelled'){?> style="background:none repeat scroll 0 0 #F6CECE;"<?php }?>><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dBookingDate'],9);?>
  &nbsp;(<?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dBookingTime'],12);?>
)</td>
			<td <?php if ($_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eStatus']=='Cencelled'){?> style="background:none repeat scroll 0 0 #F6CECE;"<?php }?>><div style="text-align:center;"><?php if ($_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eDriverPaymentPaid']=='Yes'){?> Paid <?php }else{ ?> Unpaid <?php }?></div></td>
		 <!-- <td><a href="javascript:void(0);" onClick="for_print('<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
invoice_print.php?id=<?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iBookingId'];?>
&print=yes');">Invoice</a></td> -->
		  <td <?php if ($_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eStatus']=='Cencelled'){?> style="background:none repeat scroll 0 0 #F6CECE;"<?php }?>><a href="javascript:void(0);" onClick="for_print('<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
invoice_print.php?id=<?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iBookingId'];?>
&print=yes');">view</a></td>
      <td <?php if ($_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eStatus']=='Cencelled'){?> style="background:none repeat scroll 0 0 #F6CECE;"<?php }?>><div style="text-align:center;"><a href="javascript:void(0);" title="Delete" onclick="MakeAction('<?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iBookingId'];?>
','Delete');"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_delete.png" title="Delete" /></a></div></td>			
			<td <?php if ($_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eStatus']=='Cencelled'){?> style="background:none repeat scroll 0 0 #F6CECE;"<?php }?>><input name="iBookingId[]" type="checkbox" id="iId" value="<?php echo $_smarty_tpl->getVariable('db_book_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iBookingId'];?>
"/></td>
	   </tr>
    <?php endfor; endif; ?>
    <?php }else{ ?>
     <tr>
			<td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No Record Found.</td>
	   </tr>
    <?php }?>
		</tbody>
		</table>
    </form>
	
  	<div class="extrabottom">
			<div class="bulkactions">
				<select name="newaction" id="newaction">
					<option value="">Select Action</option>
					<option value="Deletes">Make Delete</option>
					<option value="Show All">Show All</option>
				</select>
				<input type="submit" value="Apply" class="btn" onclick="return Doaction(document.getElementById('newaction').value,'bo-bookings',document.frmlist);"/>
			</div>
		</div>
        <div>
          <div class="pagination">
          <?php if (count($_smarty_tpl->getVariable('db_book_all')->value)>0){?>
	          <span class="switch" style="float: left;"><?php echo $_smarty_tpl->getVariable('recmsg')->value;?>
</span>
	        <?php }?>
           </div>
          <?php echo $_smarty_tpl->getVariable('page_link')->value;?>

        </div>     	
		<div style="clear: both;"></div></div>
</div>

<script>
function for_print(url)
{
    window.open(url,'','width=700,height=900,resizable=yes,scrollbars=yes');
}
function Searchoption(){
    document.frmsearch.action="";
    if(document.frmsearch.dTDate.value!='' && document.frmsearch.dFDate.value!='')
    if(document.frmsearch.dTDate.value < document.frmsearch.dFDate.value){
  		alert("From date should be lesser than To date.")
  		document.frmsearch.dFDate.select();
		return false;
  	}
    
    document.getElementById('frmsearch').submit();
}
function checkvalid(val){
  document.frmsearch.action="";
  if(val==1){
     document.frmsearch.paymenttype.value='No';
     document.getElementById('frmsearch').submit();
  }else if(val==2){
     document.frmsearch.paymenttype.value='Yes';
     document.getElementById('frmsearch').submit();
  }else{
     document.frmsearch.paymenttype.value='both';
     document.getElementById('frmsearch').submit();
  }
}
function AlphaSearch(val){
    var alphavalue = val;
    var file = 'bo-bookings';
    window.location="index.php?file="+file+"&alp="+alphavalue+"&mode=view";
    return false;
}
function MakeAction(loopid,type){
  var r=confirm("Are you sure to want delete this Booking?");
  if (r==true){
    document.frmlist.iBookingId.value = loopid;
    document.frmlist.action.value = type;
    document.frmlist.submit();	
  }else{
    return false;
  }
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
function for_print(url)
{
    window.open(url,'','width=700,height=900,resizable=yes,scrollbars=yes');
}
function export_ride(){
    document.frmsearch.action="index.php?file=bo-export_rides_booking&mode=edit";
    document.getElementById('frmsearch').submit();
//    window.location="index.php?file=bo-export_rides_booking&mode=edit";
    return false;
}
</script>
