<?php /* Smarty version Smarty-3.0.7, created on 2016-02-01 23:22:05
         compiled from "/home4/shipcliq/public_html/bbcsadmin/templates/localization/country.tpl" */ ?>
<?php /*%%SmartyHeaderCode:68180027856b03cfd5fd0f3-70251130%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd75beac01e3da5a069f2678ca55a0d95ec4f74a6' => 
    array (
      0 => '/home4/shipcliq/public_html/bbcsadmin/templates/localization/country.tpl',
      1 => 1453900241,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '68180027856b03cfd5fd0f3-70251130',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="contentcontainer" id="tabs">
	<div class="headings">
		<?php if ($_smarty_tpl->getVariable('mode')->value=="edit"){?>
			<h2 class="left">Edit Country</h2>
		<?php }else{ ?>
			<h2 class="left">Add Country</h2>
		<?php }?>
	</div>
	<div class="contentbox" id="tabs-1">
             <?php if ($_smarty_tpl->getVariable('var_msg')->value!=''){?>
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_error.png" title="Success" />
					<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
</p> 
				</div>     
				<div></div>
			<?php }?>
			<?php if ($_smarty_tpl->getVariable('var_msg_error')->value!=''){?>
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_error.png" title="Success" />
					<?php echo $_smarty_tpl->getVariable('var_msg_error')->value;?>
</p> 
				</div>     
				<div></div>
			<?php }?>
			<form id="frmadd" name="frmadd" action="index.php?file=l-country_a" method="post">
            <input type="hidden" name="iCountryId" id="iCountryId" value="<?php echo $_smarty_tpl->getVariable('iCountryId')->value;?>
" />
            <input type="hidden" name="action" id="action" value="<?php echo $_smarty_tpl->getVariable('mode')->value;?>
" />
				<p>
					<label for="textfield"><strong>Country :<em>*</em></strong></label>
					<input type="text" id="vCountry" name="Data[vCountry]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_city')->value[0]['vCountry'];?>
" lang="*" title="Country"/>
				</p>
				<p>
					<label for="textfield"><strong>Country Code:<em>*</em></strong></label>
					<input type="text" id="vCountryCode" name="Data[vCountryCode]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_city')->value[0]['vCountryCode'];?>
" lang="*" title="Country Code"/>
				</p>
				<p>
					<label for="textfield"><strong>Country Code ISO :<em>*</em></strong></label>
					<input type="text" id="vCountryCodeISO_3" name="Data[vCountryCodeISO_3]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_city')->value[0]['vCountryCodeISO_3'];?>
" lang="*" title="Country Code ISO"/>
				</p>
				<p>
					<label for="textfield"><strong>Phone Code :<em>*</em></strong></label>
					<input type="text" id="vPhoneCode" name="Data[vPhoneCode]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_city')->value[0]['vPhoneCode'];?>
" lang="*" title="Phone Code"/>
				</p>
				<p>
					<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" <?php if ($_smarty_tpl->getVariable('db_city')->value[0]['eStatus']=='Active'){?>selected<?php }?>>Active</option>
						<option value="Inactive" <?php if ($_smarty_tpl->getVariable('db_city')->value[0]['eStatus']=='Inactive'){?>selected<?php }?>>Inactive</option>
					</select>
				</p>
				<?php if ($_smarty_tpl->getVariable('mode')->value=='edit'){?>
				<input type="submit" value="Edit County" class="btn" onclick="return validate(document.frmadd);" title="Edit County"/>
				      <?php }else{ ?>
   				<input type="submit" value="Add County" class="btn" onclick="return validate(document.frmadd);" title="Add County"/>
  				
				      
				      <?php }?>
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
			</form>
	</div>
</div>

<script>
function redirectcancel()
{
    window.location="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=l-country&mode=view";
    return false;
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
