<?php /* Smarty version Smarty-3.0.7, created on 2015-06-19 19:45:46
         compiled from "/home/www/xfetch/xfadmin/templates/administrator/languagemaster.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15681992625584241223e302-18397352%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd90119cc98885048e4252af3b6045356ef28006d' => 
    array (
      0 => '/home/www/xfetch/xfadmin/templates/administrator/languagemaster.tpl',
      1 => 1434723343,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15681992625584241223e302-18397352',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<script language="JavaScript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tcp_javascript'];?>
validate.js"></script>
<div class="contentcontainer" id="tabs">
	<div class="headings">
		<?php if ($_smarty_tpl->getVariable('mode')->value=='edit'){?>
		<h2 class="left">Edit Language Status</h2>
    
    <?php }?>
      	</div>
	<div class="contentbox" id="tabs-1">
            <?php if ($_smarty_tpl->getVariable('var_msg')->value!=''){?>
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_error.png" title="Success" />
					<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
</p> 
				</div>     
				<div></div>
			<?php }?>
			<?php if ($_smarty_tpl->getVariable('var_msg_error')->value!=''){?>
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_error.png" title="Success" />
					<?php echo $_smarty_tpl->getVariable('var_msg_error')->value;?>
</p> 
				</div>     
				<div></div>
			<?php }?>
			<form id="frmadd" name="frmadd" action="index.php?file=ad-languagemaster_a" method="post">
            <input type="hidden" name="iLanguageMasId" id="iLanguageMasId" value="<?php echo $_smarty_tpl->getVariable('iLanguageMasId')->value;?>
" />
            <input type="hidden" name="action" id="action" value="<?php echo $_smarty_tpl->getVariable('mode')->value;?>
" />
       	<p>
					<label for="textfield"><strong>Language :</strong></label>
					<input type="text" id="vTitle" name="Data[vTitle]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_admin')->value[0]['vTitle'];?>
" lang="*" title="Language"/>
				</p>
				<p>
					<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]" disabled>
						<option value="Active" <?php if ($_smarty_tpl->getVariable('db_admin')->value[0]['eStatus']=='Active'){?>selected<?php }?>>Active</option>
						<option value="Inactive" <?php if ($_smarty_tpl->getVariable('db_admin')->value[0]['eStatus']=='Inactive'){?>selected<?php }?>>Inactive</option>
					</select>
				</p>
					<p>
					<label for="textfield"><strong>Front Status :</strong></label>
					<select id="eStatus" name="Data[eFrontStatus]">
						<option value="Yes" <?php if ($_smarty_tpl->getVariable('db_admin')->value[0]['eFrontStatus']=='Yes'){?>selected<?php }?>>Yes</option>
						<option value="No" <?php if ($_smarty_tpl->getVariable('db_admin')->value[0]['eFrontStatus']=='No'){?>selected<?php }?>>No</option>
					</select>
				</p>
				
				
				<?php if ($_smarty_tpl->getVariable('mode')->value=='edit'){?>
				<input type="submit" value="Edit Language Status" class="btn" onclick="return validate(document.frmadd);" title="Edit Administrator"/>
				      <?php }else{ ?>
   				<input type="submit" value="Add Language Status" class="btn" onclick="return validate(document.frmadd);" title="Add Administrator"/>
  				
				      
				      <?php }?>
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
			</form>
	</div>
</div>

<script>

function redirectcancel()
{
    window.location="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=ad-languagemaster&mode=view";
    return false;
}

function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
