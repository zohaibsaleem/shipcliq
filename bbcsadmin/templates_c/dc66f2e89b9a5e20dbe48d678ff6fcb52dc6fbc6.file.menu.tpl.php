<?php /* Smarty version Smarty-3.0.7, created on 2016-01-07 19:36:30
         compiled from "/home/www/cargosharing1/bbcsadmin/templates/menu.tpl" */ ?>
<?php /*%%SmartyHeaderCode:176013204568ebe3ebc81a4-81791054%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dc66f2e89b9a5e20dbe48d678ff6fcb52dc6fbc6' => 
    array (
      0 => '/home/www/cargosharing1/bbcsadmin/templates/menu.tpl',
      1 => 1452195389,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '176013204568ebe3ebc81a4-81791054',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<ul class="sf-menu" id="example">
	<li>
		<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
" style="border-right: 1px solid #525252;">Home</a>
	</li>
	<?php if (in_array('ad-administrator',$_smarty_tpl->getVariable('modlist')->value)||in_array('ad-languagemaster',$_smarty_tpl->getVariable('modlist')->value)){?>
	<li>
		<a href="javascript:void(0);" style="border-right: 1px solid #525252;" class="sf-with-ul">Administrators <span class="sf-sub-indicator"> </span></a>
		<ul style="width:128px;">
			<?php if (in_array('ad-administrator',$_smarty_tpl->getVariable('modlist')->value)){?>
			<li>
				<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=ad-administrator&mode=view">Administrator</a>
			</li>
			<?php }?>
			<?php if (in_array('ad-languagemaster',$_smarty_tpl->getVariable('modlist')->value)){?>
			<li>
				<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=ad-languagemaster&mode=view">Language</a>
			</li>
			<?php }?>
		</ul>
	</li>
	<?php }?>
	<?php if (in_array('m-member',$_smarty_tpl->getVariable('modlist')->value)||in_array('m-member_messages',$_smarty_tpl->getVariable('modlist')->value)){?>
	<li>
		<a href="javascript:void(0);" style="border-right: 1px solid #525252;" class="sf-with-ul">Members Management<span class="sf-sub-indicator">  </span></a>
		<ul> 			 
			<?php if (in_array('m-member',$_smarty_tpl->getVariable('modlist')->value)){?>
			<li style="width:170px;"><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=m-member&mode=view">Members List</a></li>
			<?php }?>
			<?php if (in_array('m-member_messages',$_smarty_tpl->getVariable('modlist')->value)){?>
			<li style="width:170px;"><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=m-member_messages&mode=view">Message Records</a></li>
			<?php }?>
		</ul>
	</li>
	<?php }?>
	<?php if (in_array('ri-rideslist',$_smarty_tpl->getVariable('modlist')->value)||in_array('bo-bookings',$_smarty_tpl->getVariable('modlist')->value)||in_array('ri-member_payment',$_smarty_tpl->getVariable('modlist')->value)){?>
	<li>
		<a href="javascript:void(0);" style="border-right: 1px solid #525252;" class="sf-with-ul">Trip Management<span class="sf-sub-indicator">  </span></a>
		<ul> 
			<?php if (in_array('ri-rideslist',$_smarty_tpl->getVariable('modlist')->value)){?>
			<li style="width:170px;"><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=ri-rideslist&mode=view&paymenttype=all">Trip List</a></li>
			<?php }?>
			<?php if (in_array('bo-bookings',$_smarty_tpl->getVariable('modlist')->value)){?>
			<li style="width:170px;"><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=bo-bookings&mode=view">Trip Booking</a></li>
			<?php }?>
			<?php if (@PAYMENT_OPTION!='Contact'){?>
				<?php if (@PAYMENT_OPTION=='PayPal'&&in_array('ri-member_payment',$_smarty_tpl->getVariable('modlist')->value)){?>
				<li style="width:170px;"><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=ri-member_payment&mode=view">Pay To Traveller</a></li>
				<?php }?>
			<?php }?>
			
		</ul>
	</li>	
	<?php }?>
	
	
	<?php if (in_array('r-member_reports',$_smarty_tpl->getVariable('modlist')->value)||in_array('r-rides_reports',$_smarty_tpl->getVariable('modlist')->value)||in_array('r-payment_reports',$_smarty_tpl->getVariable('modlist')->value)||in_array('r-money_reports',$_smarty_tpl->getVariable('modlist')->value)){?>
	<li>
		<a href="javascript:void(0);" style="border-right: 1px solid #525252;" class="sf-with-ul">Report<span class="sf-sub-indicator">  </span></a>
		<ul>
			<?php if (in_array('r-member_reports',$_smarty_tpl->getVariable('modlist')->value)){?>
			<li style="width:170px;"><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=r-member_reports&mode=view">Members Report</a></li>
			<?php }?>
			<?php if (in_array('r-rides_reports',$_smarty_tpl->getVariable('modlist')->value)){?>
			<li style="width:170px;"><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=r-rides_reports&mode=view">Trip Posted Report</a></li>
			<?php }?>
			<?php if (in_array('r-money_reports',$_smarty_tpl->getVariable('modlist')->value)){?>
			<li style="width:170px;"><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=r-money_reports&mode=view">Money Report</a></li>
			<?php }?>
			<?php if (in_array('r-payment_reports',$_smarty_tpl->getVariable('modlist')->value)){?>
			<li style="width:170px;"><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=r-payment_reports&mode=view">Payment Report</a></li>
			<?php }?>
		</ul>
	</li>
	<?php }?>				 	
	<?php if (in_array('m-member_stories',$_smarty_tpl->getVariable('modlist')->value)||in_array('u-media_partners',$_smarty_tpl->getVariable('modlist')->value)||in_array('u-email_templates',$_smarty_tpl->getVariable('modlist')->value)||in_array('u-faqcategory',$_smarty_tpl->getVariable('modlist')->value)||in_array('u-faq',$_smarty_tpl->getVariable('modlist')->value)||in_array('to-homebanners',$_smarty_tpl->getVariable('modlist')->value)||in_array('to-banners',$_smarty_tpl->getVariable('modlist')->value)||in_array('u-currency',$_smarty_tpl->getVariable('modlist')->value)||in_array('u-newsletter_subscriber',$_smarty_tpl->getVariable('modlist')->value)){?>
	<li style="width:120px;">
		<a href="javascript:void(0);" style="border-right: 1px solid #525252;" class="sf-with-ul"><center>Utility</center ><span class="sf-sub-indicator"> </span></a>
		<ul style="width:160px;">			
			<?php if (in_array('r-member_stories',$_smarty_tpl->getVariable('modlist')->value)){?>	
			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=m-member_stories&mode=view">Members Stories</a></li>
			<?php }?>
			<?php if (in_array('u-media_partners',$_smarty_tpl->getVariable('modlist')->value)){?>
			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-media_partners&mode=view"  title="">Media Partners</a></li>
			<?php }?>
			<?php if (in_array('u-email_templates',$_smarty_tpl->getVariable('modlist')->value)){?>
			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-email_templates&mode=view"  title="">Email templates</a></li>
			<?php }?>
			<?php if (in_array('u-faqcategory',$_smarty_tpl->getVariable('modlist')->value)){?>
			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-faqcategory&mode=view" title="">FAQ Category</a></li>
			<?php }?>
			<?php if (in_array('u-faq',$_smarty_tpl->getVariable('modlist')->value)){?>
			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-faq&mode=view" title="">FAQ</a></li>
			<?php }?>
			<?php if (in_array('to-homebanners',$_smarty_tpl->getVariable('modlist')->value)){?>
			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=to-homebanners&mode=view">Home Banners</a></li>
			<?php }?>
			<?php if (in_array('to-banners',$_smarty_tpl->getVariable('modlist')->value)){?>
			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=to-banners&mode=view">Adv. Banners</a></li>
			<?php }?>
			<?php if (in_array('u-currency',$_smarty_tpl->getVariable('modlist')->value)){?>
			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-currency&mode=view">Currency Rates</a></li>
			<?php }?>
			<?php if (in_array('u-newsletter_subscriber',$_smarty_tpl->getVariable('modlist')->value)){?>
			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-newsletter_subscriber&mode=view">Newsletter Subscriber</a></li>
			<?php }?>
			
			<!---<?php if (in_array('u-preferences',$_smarty_tpl->getVariable('modlist')->value)){?>
				<li><a class="last" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-preferences&mode=view">Manage Preferences</a></li>
			<?php }?>--->
		</ul>
	</li>
	<?php }?>
	<?php if (in_array('l-country',$_smarty_tpl->getVariable('modlist')->value)||in_array('l-state',$_smarty_tpl->getVariable('modlist')->value)||in_array('l-city',$_smarty_tpl->getVariable('modlist')->value)){?>
	<li>
		<a href="javascript:void(0);" style="border-right: 1px solid #525252;" class="sf-with-ul">Localization<span class="sf-sub-indicator"> </span></a>
		<ul>
			<?php if (in_array('l-country',$_smarty_tpl->getVariable('modlist')->value)){?>
			<li>
				<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=l-country&mode=view">Country</a>
			</li>
			<?php }?>
			<?php if (in_array('l-state',$_smarty_tpl->getVariable('modlist')->value)){?>
			<li>
				<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=l-state&mode=view">Province</a>
			</li>
			<?php }?>
			<?php if (in_array('l-city',$_smarty_tpl->getVariable('modlist')->value)){?> 
			<li>
				<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=l-city&mode=view">City</a>
			</li>
			<?php }?>				
		</ul>
	</li>
	<?php }?>
	<?php if (in_array('to-generalsettings',$_smarty_tpl->getVariable('modlist')->value)||in_array('to-languagelabel',$_smarty_tpl->getVariable('modlist')->value)||in_array('pg-staticpages',$_smarty_tpl->getVariable('modlist')->value)){?>
	<li>
		<a href="javascript:void(0);" style="border-right: 1px solid #525252;" class="sf-with-ul">Settings<span class="sf-sub-indicator"></span></a>
		<ul style="width:150px;">
			<?php if (in_array('to-generalsettings',$_smarty_tpl->getVariable('modlist')->value)){?>
			<li>
				<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=to-generalsettings&mode=edit">General Configuration</a>
			</li> 
			<?php }?>
			<?php if (in_array('to-languagelabel',$_smarty_tpl->getVariable('modlist')->value)){?>
			<li>
				<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=to-languagelabel&mode=view" title="">Language Label</a>
			</li>
			<?php }?>
			<?php if (in_array('pg-staticpages',$_smarty_tpl->getVariable('modlist')->value)){?>
			<li>
				<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=pg-staticpages&mode=view">Static Pages</a>
			</li>	
			<?php }?>
		</ul>
	</li>
	<?php }?>
    <li>
		<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=au-logout" style="border-right: 1px solid #525252;" class="sf-with-ul">Logout</a>
	</li>
    <div style="clear:both;"></div>	       
</ul>
