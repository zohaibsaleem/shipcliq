<?php /* Smarty version Smarty-3.0.7, created on 2015-03-20 11:43:56
         compiled from "/home/www/blablaclone/bcadmin/templates/dashboard/view-dashboard.tpl" */ ?>
<?php /*%%SmartyHeaderCode:888194111550bbaa4ed67f6-12423560%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dd9a89ef843a6ed988e73c9ffee87103a4b36d09' => 
    array (
      0 => '/home/www/blablaclone/bcadmin/templates/dashboard/view-dashboard.tpl',
      1 => 1416893171,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '888194111550bbaa4ed67f6-12423560',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="contentcontainer">
	<div class="headings altheading">
		<h2>Site Map</h2>
	</div>
	<div class="contentbox" style="border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
    margin-bottom: 15px;">
	<div>
   <div class="ControlPanelLeft">
    <h1>Administrator</h1>
    <ul>
      <li><a class="last" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=ad-administrator&mode=view">Administrator</a></li>
    </ul>
  </div> 
  
  <div class="ControlPanelLeft">
    <h1>Members Management</h1>
    <ul>
      <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=m-member&mode=view">Members List</a></li>
      <li><a class="last" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=m-member_messages&mode=view">Message Records</a></li>
    </ul>
  </div> 
  
  <div class="ControlPanelLeft">
    <h1>Rides Management</h1>
    <ul>
      <li><a class="last" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=ri-rideslist&mode=view&">Rides List</a></li>
    </ul>
  </div> 
  <?php if (@PAYMENT_OPTION!='Contact'){?>
  <div class="ControlPanelLeft">
    <h1>Booking Payment</h1>
    <ul>
      <li><a class="last" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=bo-bookings&mode=view">Rides Booking Payment</a></li>
    </ul>
  </div>
  <?php }?>
   <div class="ControlPanelLeft">
    <h1>Report</h1>
    <ul>
      <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=r-member_reports&mode=view">Members Report</a></li>
      <li><a class="last" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=r-rides_reports&mode=view">Rides Posted Report</a></li>
    </ul>
  </div> 
  
  <div class="ControlPanelLeft">
    <h1>Utility</h1>
    <ul>
      <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-make&mode=view">Car Make</a></li>
      <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-model&mode=view">Car Model</a></li>
      <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-color&mode=view">Car Colour</a></li>
      <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=m-member_stories&mode=view">Member Stories</a></li>
      <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-media_partners&mode=view">Media Partners</a></li>
      <li><a class="last" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-email_templates&mode=view">Email Templates</a></li>
      </ul>
  </div>
  
  <div class="ControlPanelLeft">
    <h1>Utility</h1>
    <ul>
    <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-faqcategory&mode=view">FAQ Category</a></li>
      <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-faq&mode=view">FAQ</a></li>
      <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=to-banners&mode=view">Banners</a></li>
      <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-currency&mode=view">Currency Rates</a></li>
      <li><a class="last" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-newsletter_subscriber&mode=view">Newsletter Subscriber</a></li>
    </ul>
  </div>
  
  <div class="ControlPanelLeft">
    <h1>Settings</h1>
    <ul>
      <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=to-generalsettings&mode=edit">General Configuration</a></li>
      <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=to-languagelabel&mode=view">Language Label</a></li>
      <li><a class="last" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=pg-staticpages&mode=view">Static Pages</a></li>
    </ul> 
    <br><br>
    <h1>Share Preferences</h1>
    <ul>
      <li><a class="last" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=u-preferences&mode=view">Manage Preferences</a></li>
    </ul>    
   </div>    
                     
   
 <div class="ControlPanelLeft">
    <h1>Localization</h1>
    <ul>
      <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=l-country&mode=view">Country</a></li>
			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=l-state&mode=view">Province</a></li>
			<li><a class="last" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=l-city&mode=view">City</a></li>
    </ul>
  </div>
  
 
  <div style=" clear:both"></div>
	   
	</div>
  
  <br><br><br>
  <div style="clear:both"></div>

</div>  
</div>
  <div class="contentcontainer">
	<div class="headings altheading">
		<h2>Update statistics</h2>
	</div>
	<div class="contentbox">
		<table width="100%">
		<thead>
		<!--
        <tr>
			<th width="33%">Name</th>
			<th width="33%">Email</th>
			<th width="33%">Registration Date</th>
		</tr>
		-->
		</thead>
		<tbody>
			<tr class="alt">
				<td width="20%">Total Admin</td>
				<td width="1%">:</td>
				<td width="79%"><?php echo $_smarty_tpl->getVariable('totadmin')->value;?>
</td>
			</tr>			
			<tr class="alt">
				<td>Total Active Members </td>
				<td>:</td>
				<td><?php echo $_smarty_tpl->getVariable('totstudent')->value;?>
</td>
			</tr>
			<tr class="alt">
				<td>Total Rides Offered </td>
				<td>:</td>
				<td><?php echo $_smarty_tpl->getVariable('totevent')->value;?>
</td>
			</tr>
			 
       
		</tbody>
		</table>
	<div style="clear: both;"></div>
	</div>
  </div>




 