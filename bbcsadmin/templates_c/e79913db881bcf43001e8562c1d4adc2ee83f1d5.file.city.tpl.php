<?php /* Smarty version Smarty-3.0.7, created on 2016-02-01 23:23:40
         compiled from "/home4/shipcliq/public_html/bbcsadmin/templates/localization/city.tpl" */ ?>
<?php /*%%SmartyHeaderCode:212353926556b03d5c236c83-72659144%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e79913db881bcf43001e8562c1d4adc2ee83f1d5' => 
    array (
      0 => '/home4/shipcliq/public_html/bbcsadmin/templates/localization/city.tpl',
      1 => 1453900242,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '212353926556b03d5c236c83-72659144',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="contentcontainer" id="tabs">
	<div class="headings">
  	<?php if ($_smarty_tpl->getVariable('mode')->value=='add'){?>
		<h2 class="left">Add City</h2>
    <?php }else{ ?>
	<h2 class="left">Edit City</h2>
    <?php }?>

  </div>
	<div class="contentbox" id="tabs-1">
         <?php if ($_smarty_tpl->getVariable('var_msg')->value!=''){?>
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_error.png" title="Success" />
					<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
</p> 
				</div>     
				<div></div>
			<?php }?>
			<?php if ($_smarty_tpl->getVariable('var_msg_error')->value!=''){?>
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_error.png" title="Success" />
					<?php echo $_smarty_tpl->getVariable('var_msg_error')->value;?>
</p> 
				</div>     
				<div></div>
			<?php }?>
			<form id="frmadd" name="frmadd" action="index.php?file=l-city_a" method="post">
            <input type="hidden" name="iCityId" id="iCityId" value="<?php echo $_smarty_tpl->getVariable('iCityId')->value;?>
" />
            <input type="hidden" name="action" id="action" value="<?php echo $_smarty_tpl->getVariable('mode')->value;?>
" />
				
        <p>
					<label for="textfield"><strong>Country : *</strong></label>
					<select id="vCountryCode" name="Data[vCountryCode]" lang="*" onchange="get_county_list(this.value);" title="Country">
						<option value=""> -------- Select Country --------- </option>
            <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['name'] = 'rec';
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_country')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['total']);
?>
					   	<option value="<?php echo $_smarty_tpl->getVariable('db_country')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rec']['index']]['vCountryCode'];?>
" <?php if ($_smarty_tpl->getVariable('db_country')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rec']['index']]['vCountryCode']==$_smarty_tpl->getVariable('db_city')->value[0]['vCountryCode']){?>selected <?php }?>><?php echo $_smarty_tpl->getVariable('db_country')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rec']['index']]['vCountry'];?>
</option>
            <?php endfor; endif; ?>
					</select>
				</p>
				<p>
            <label for="textfield"><strong>State : <em>*</em></strong></label>
    <!--        <span id="county_list">    --> 
           <select id="vStateCode" name="Data[vStateCode]" lang="*" title="State" style="width:25%;">
              <option value="">--------Select State---------</option>
            </select>
      <!--        </span>   -->
        </p>
				
        <p>
					<label for="textfield"><strong>City :<em>*</em></strong></label>
					<input type="text" id="vCity" name="Data[vCity]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_city')->value[0]['vCity'];?>
" lang="*" title="City"/>
				</p>
			
				<p>
					<label for="textfield"><strong>Status :</strong></label>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" <?php if ($_smarty_tpl->getVariable('db_city')->value[0]['eStatus']=='Active'){?>selected<?php }?>>Active</option>
						<option value="Inactive" <?php if ($_smarty_tpl->getVariable('db_city')->value[0]['eStatus']=='Inactive'){?>selected<?php }?>>Inactive</option>
					</select>
				</p>
				<?php if ($_smarty_tpl->getVariable('mode')->value=='edit'){?>
				<input type="submit" value="Edit City" class="btn" onclick="return validate(document.frmadd);" title="Edit City"/>
				      <?php }else{ ?>
   				<input type="submit" value="Add City" class="btn" onclick="return validate(document.frmadd);" title="Add City"/>
  				
				      
				      <?php }?>
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
			</form>
	</div>
</div>

<script>
 
function redirectcancel()
{
    window.location="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=l-city&mode=view";
    return false;
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}

function get_county_list(code,selected)
{   
$("#vStateCode").html("Please wait...");
var request = $.ajax({
type: "POST",
url: '<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
'+'/index.php?file=l-getState',
data: "code="+code+"&stcode="+selected,

success: function(data) { 
$("#vStateCode").append(data);
}
});

request.fail(function(jqXHR, textStatus) {
alert( "Request failed: " + textStatus );
});
}

if('<?php echo $_smarty_tpl->getVariable('db_city')->value[0]['vStateCode'];?>
' != '' || '<?php echo $_smarty_tpl->getVariable('db_city')->value[0]['vCountryCode'];?>
' != ''){
get_county_list('<?php echo $_smarty_tpl->getVariable('db_city')->value[0]['vCountryCode'];?>
', '<?php echo $_smarty_tpl->getVariable('db_city')->value[0]['vStateCode'];?>
');
}
</script>
