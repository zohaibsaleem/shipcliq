<?php /* Smarty version Smarty-3.0.7, created on 2016-02-13 22:45:39
         compiled from "/home4/shipcliq/public_html/bbcsadmin/templates/tools/generalsettings.tpl" */ ?>
<?php /*%%SmartyHeaderCode:150990367456c0067333d941-87277704%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ea63df6b6a1a89b184700b00c3318ebbc44e855a' => 
    array (
      0 => '/home4/shipcliq/public_html/bbcsadmin/templates/tools/generalsettings.tpl',
      1 => 1455356896,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '150990367456c0067333d941-87277704',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="contentcontainer">
    <div class="headings alt">
        <h2>Edit General Settings</h2>
	</div>
    <div class="contentbox">
		<?php if ($_smarty_tpl->getVariable('var_msg')->value!=''){?>
		<div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_success.png" title="Success" />
			<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
</p> 
		</div>     
		<div></div>
		<?php }?>
        <form  name="frmadd" id="frmadd"  method="post" action="index.php?file=to-generalsettings_a" enctype="multipart/form-data">
    		<p style="float:right;">
				<select name="eType" id="eType" class="inputbox" style="width:200px;" onChange="go_for_change(this.value);">
					<option value=""> -- Select -- </option>
					<option value="General" <?php if ($_smarty_tpl->getVariable('eType')->value=='General'){?> selected <?php }?>>General</option>
					<option value="Email" <?php if ($_smarty_tpl->getVariable('eType')->value=='Email'){?> selected <?php }?>>Email</option>
					<!--<option value="Apperance" <?php if ($_smarty_tpl->getVariable('eType')->value=='Apperance'){?> selected <?php }?>>Apperance</option>-->
					<?php if (@PAYMENT_OPTION!='Contact'){?>
					<option value="Paypal" <?php if ($_smarty_tpl->getVariable('eType')->value=='Paypal'){?> selected <?php }?>>Paypal</option>
					<option value="SEO" <?php if ($_smarty_tpl->getVariable('eType')->value=='SEO'){?> selected <?php }?>>SEO</option>
					<?php }?>
					<!--<option value="Prices" <?php if ($_smarty_tpl->getVariable('eType')->value=='Prices'){?> selected <?php }?>>Prices</option>-->
				</select>
			</p>
			<input type="hidden" name="action" id="action" value="<?php echo $_smarty_tpl->getVariable('mode')->value;?>
" />
			<input type="hidden" name="imagemode" id="imagemode" value="">
            <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_res')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
			<?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eType']!=$_smarty_tpl->getVariable('currType')->value){?><?php }?>
			<p>
				<label for="textfield"><strong><?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription'];?>
<em>*</em></strong></label>
				
				<?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Site Logo"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Site Favicon"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Footer Logo"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Email Logo"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Ride Background"){?>
				<input type="file" id="<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
" name="Data[<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
]" value="<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue'];?>
" title="<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription'];?>
"/> 
				<?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']!=''&&$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Site Favicon"){?>
				&nbsp;&nbsp;[<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_upload_site_favicon'];?>
/<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue'];?>
" target="_blank" rel="[images]">View</a>]
				&nbsp;&nbsp;[<a href="javascript:void(0);" onclick="confirm_delete('<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
');" title="Delete Image">Delete</a>]
				<?php }?>
				
				<?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']!=''&&$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Site Logo"){?>
				&nbsp;&nbsp;[<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_upload_site_logo'];?>
/1_<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue'];?>
" target="_blank" rel="[images]">View</a>]
				&nbsp;&nbsp;[<a href="javascript:void(0);" onclick="confirm_delete('<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
');" title="Delete Image">Delete</a>]
				<?php }?>
				
				<?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']!=''&&$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Email Logo"){?>
				&nbsp;&nbsp;[<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_upload_email_logo'];?>
/1_<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue'];?>
" target="_blank" rel="[images]">View</a>]
				&nbsp;&nbsp;[<a href="javascript:void(0);" onclick="confirm_delete('<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
');" title="Delete Image">Delete</a>]
				<?php }?>
				
				<?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']!=''&&$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Footer Logo"){?>
				&nbsp;&nbsp;[<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_upload_footer_logo'];?>
/1_<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue'];?>
" target="_blank" rel="[images]">View</a>]
				&nbsp;&nbsp;[<a href="javascript:void(0);" onclick="confirm_delete('<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
');" title="Delete Image">Delete</a>]
				<?php }?>
				
				<?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']!=''&&$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Ride Background"){?>
				&nbsp;&nbsp;[<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_upload_footer_logo'];?>
/1_<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue'];?>
" target="_blank" rel="[images]">View</a>]
				&nbsp;&nbsp;[<a href="javascript:void(0);" onclick="confirm_delete('<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
');" title="Delete Image">Delete</a>]
				<?php }?>
				
				<?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Site Logo"){?>
				<br><br>[Note : Preferable size would be (width:215px; height:55px;)]
				<br>[Note: Supported File Type is *.png,jpg,jpeg,bmp,gif]
				<?php }?>
				<?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Email Logo"){?>
				<br><br>[Note : Preferable size would be (width:215px; height:55px;)]
				<br>[Note: Supported File Type is *.png,jpg,jpeg,bmp,gif]
				<?php }?>
				<?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Site Favicon"){?>
				<br><br>[Note : Preferable size would be (width:16px; height:16px;)]
				<br>[Note: Supported File Type is *.ico]
				<?php }?>
				<?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Footer Logo"){?>
				<br><br>[Note : Preferable size would be (width:16px; height:16px;)]
				<br>[Note: Supported File Type is *.png,jpg,jpeg,bmp,gif]
				<?php }?>
				
				<?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription']=="Ride Background"){?>
				<br><br>[Note : Preferable size would be (width:1150px; height:480px;)]
				<br>[Note: Supported File Type is *.png,jpg,jpeg,bmp,gif]
				<?php }?>
				<?php }elseif($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="COMPANY_ADDRESS"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="COMPANY_CONTACTUS_ADDRESS"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="GOOGLE_MAP"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="GOOGLE_ANALYTIC_CODE"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eFieldType']=="Textarea"){?>
				
				<textarea id="<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
" name="Data[<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
]" class="inputbox" lang="*"><?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue'];?>
</textarea>
				<?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="GOOGLE_MAP"){?><br />[Note: Please upload google iframe code of width 284 an height 211 for display map.]<?php }?>
				<?php }elseif($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="LANG_BOX"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="GEO_LOCATION"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="SHOW_COOKIE_POLICY"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="AIRPORTS_RIDES_SHOW"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="CRON_MEMBERSHIP_EMAIL"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="CRON_SATISFACTION_EMAIL"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="PHONE_VERIFICATION_REQUIRED"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="LICENSE_VERIFICATION_REQUIRED"||$_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="CARPAPER_VERIFICATION_REQUIRED"){?>
				<select name="Data[<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
]" class="inputbox">
					<option value="Yes" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Yes'){?>selected<?php }?>>Yes</option>
					<option value="No" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='No'){?>selected<?php }?>>No</option>
				</select>
				<?php }elseif($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="PAYMENT_OPTION"){?>
				<select name="Data[<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
]" class="inputbox">
					<option value="PayPal" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='PayPal'){?>selected<?php }?>>Online Payment (Payment gateway required)</option>
					<option value="Manual" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Manual'){?>selected<?php }?>>Offline Payment (Payment gateway not required)</option>
					<!-- <option value="Contact" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Contact'){?>selected<?php }?>>No Payment</option>  -->
				</select>	
				<?php }elseif($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName']=="SELECT_THEME"){?>
				<Select name="Data[<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
]" class="inputbox">
					<option value="Theme29-orange-home4" <?php if ($_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue']=='Theme29-orange-home4'){?>selected<?php }?>>Theme 29</option>
				</select>				
				<?php }else{ ?>
				<input type="text" id="<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
" name="Data[<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
]" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vValue'];?>
" lang="*" title="<?php echo $_smarty_tpl->getVariable('db_res')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tDescription'];?>
"/> <br />
				<?php }?>
			</p>
			<?php $_smarty_tpl->tpl_vars["currType"] = new Smarty_variable(($_smarty_tpl->getVariable('db_res')->value)."[i].eType", null, null);?>
            <?php endfor; endif; ?>
			<input type="submit" value="Save Settings" class="btn" title="Save Settings" onclick="return validate(document.frmadd);"/>
		</form>
	</div>
</div>

<script>
	function hidemessage(){
		jQuery("#errormsgdiv").slideUp();
	}
	
	function go_for_change(val){
		window.location="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=to-generalsettings&mode=edit&eType="+val;
		return false;
	}
	function confirm_delete(val)
	{
		//alert(val);
		ans = confirm('Are you sure for Delete Image?');
		if(ans == true)
		{
			document.frmadd.action.value = 'confirm_delete';
			document.frmadd.imagemode.value = val;
			document.frmadd.submit();
		}
		else
		{
			return false;
		}
	}
</script>
        