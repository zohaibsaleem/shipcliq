<?php /* Smarty version Smarty-3.0.7, created on 2016-02-03 21:52:49
         compiled from "/home4/shipcliq/public_html/bbcsadmin/templates/booking/bookings.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19939547956b2cb11b9fca5-64634905%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ea85e1aae882fec4c85729915732105fae5d2884' => 
    array (
      0 => '/home4/shipcliq/public_html/bbcsadmin/templates/booking/bookings.tpl',
      1 => 1453900177,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19939547956b2cb11b9fca5-64634905',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="contentcontainer" id="tabs">
	<div class="headings">
    	<?php if ($_smarty_tpl->getVariable('mode')->value=='edit'){?>
			<h2 class="left">Booking Payment Details</h2>
        <?php }else{ ?>
			<h2 class="left">Booking Payment Details</h2>
        <?php }?>
	</div>
	<div class="contentbox" id="tabs-1">           
			<?php if ($_smarty_tpl->getVariable('var_msg')->value!=''){?>
				<div class="status success" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_success.png" title="Success" />
					<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
</p> 
				</div> 
			<?php }?>
			<?php if ($_smarty_tpl->getVariable('var_msg_err')->value!=''){?>
				<div class="status error" id="errormsgdiv"> 
					<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
					<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_error.png" title="Success" />
					<?php echo $_smarty_tpl->getVariable('var_msg_err')->value;?>
</p> 
				</div> 
			<?php }?>
                <table border="0" cellpadding="1" cellspacing="1" width="100%" align="center" class="oderpage">
									<tr>
										<td valign="top">
											<table class="headingbg" style="border: 1px solid #ccc;" border="0" cellpadding="1" cellspacing="1" width="100%" align="center">
										  <tr>
												<td <?php if ($_smarty_tpl->getVariable('db_book')->value[0]['eStatus']=='Cencelled'){?>colspan="3"<?php }?> width="100%"  class="headingbg wmatterbold" bgcolor="#309ec3" height="22">&nbsp;<b><span style="color:#000000">Booking #<?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vBookingNo'];?>
</span></b></td>
											</tr>
											<tr bgcolor="#ffffff">
												<td valign="top" <?php if ($_smarty_tpl->getVariable('db_book')->value[0]['eStatus']=='Cencelled'){?> width="47%" <?php }?>>
													<table border="0" cellpadding="1" cellspacing="1" width="100%" style="border: 1px solid #ccc;">
													<tbody>
                          <tr>
    												<td colspan="3" width="100%"  class="headingbg wmatterbold" bgcolor="#E6E6E6" height="22">&nbsp;<b><span style="color:#000000">Booking Details</span></b></td>
    											</tr>
                          <tr>
														<td width="34%" valign="top">Booking No&nbsp;</td>
														<td width="1%" valign="top"> : </td>
														<td width="65%" valign="top"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vBookingNo'];?>
</td>
													</tr>
													<tr>
														<td align="left" valign="top">Trips Details&nbsp;</td>
														<td valign="top"> : </td>
														<td align="left">
                              <table>
                                <tr>
                                  <td>From</td>
                                  <td>:</td>
                                  <td>
                                     <?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vFromPlace'];?>

                                  </td>
                                </tr>
                                <tr>
                                  <td>To</td>
                                  <td>:</td>
                                  <td><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vToPlace'];?>
</td>
                                </tr>
                              </table>
                            </td>
													</tr>
													<tbody><tr>
														<td align="left">Main Trips Details</td>
														<td> : </td>
														<td align="left" style="padding-left:12px;"><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=ri-rideslist&mode=edit&iRideId=<?php echo $_smarty_tpl->getVariable('db_book')->value[0]['iRideId'];?>
"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vMainRidePlaceDetails'];?>
</a></td>
													</tr>
													
                          <tr>
														<td align="left" valign="top">Sender Details&nbsp;</td>
														<td valign="top"> : </td>
														<td align="left"><!--<a href="index.php?file=m-member&mode=edit&iMemberId=<?php echo $_smarty_tpl->getVariable('db_book')->value[0]['iBookerId'];?>
"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vBookerFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vBookerLastName'];?>
</a>-->
                              <table>
                                <tr>
                                  <td>Name</td>
                                  <td>:</td>
                                  <td>
                                     <a href="index.php?file=m-member&mode=edit&iMemberId=<?php echo $_smarty_tpl->getVariable('db_book')->value[0]['iBookerId'];?>
"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vBookerFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vBookerLastName'];?>
</a>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Email</td>
                                  <td>:</td>
                                  <td><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vBookerEmail'];?>
</td>
                                </tr>
                                <tr>
                                  <td>Phone</td>
                                  <td>:</td>
                                  <td><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vBookerPhone'];?>
</td>
                                </tr>
                                
                              </table>
                            </td>
													</tr>
													<tr>
														<td align="left" valign="top">Traveler Details&nbsp;</td>
														<td valign="top"> : </td>
														<td align="left">
                              <table>
                                <tr>
                                  <td>Name</td>
                                  <td>:</td>
                                  <td>
                                     <a href="index.php?file=m-member&mode=edit&iMemberId=<?php echo $_smarty_tpl->getVariable('db_book')->value[0]['iDriverId'];?>
"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vDriverFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vDriverLastName'];?>
</a>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Email</td>
                                  <td>:</td>
                                  <td><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vDriverEmail'];?>
</td>
                                </tr>
                                <tr>
                                  <td>Phone</td>
                                  <td>:</td>
                                  <td><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vDriverPhone'];?>
</td>
                                </tr>
                                
                              </table>
                            </td>
													</tr>
													
													<!--<tr>
														<td align="left">No. Of Seats&nbsp;</td>
														<td> : </td>
														<td align="left"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['iNoOfSeats'];?>
</td>
													</tr>--->
													
													<tr>
														<td align="left">Document&nbsp;</td>
														<td> : </td>
														<td align="left"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vBookerCurrencyCode'];?>
 <?php echo $_smarty_tpl->getVariable('db_book')->value[0]['fDocumentPrice'];?>
</td>
													</tr>
													<tr>
														<td align="left">Box&nbsp;</td>
														<td> : </td>
														<td align="left"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vBookerCurrencyCode'];?>
 <?php echo $_smarty_tpl->getVariable('db_book')->value[0]['fBoxPrice'];?>
</td>
													</tr>
													<tr>
														<td align="left">Luggage&nbsp;</td>
														<td> : </td>
														<td align="left"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vBookerCurrencyCode'];?>
 <?php echo $_smarty_tpl->getVariable('db_book')->value[0]['fLuggagePrice'];?>
</td>
													</tr>
													<tr>
														<td align="left">Commission&nbsp;</td>
														<td> : </td>
														<td align="left"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vBookerCurrencyCode'];?>
 <?php echo $_smarty_tpl->getVariable('db_book')->value[0]['fCommission'];?>
</td>
													</tr>
													<tr>
														<td align="left">Vat&nbsp;</td>
														<td> : </td>
														<td align="left"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vBookerCurrencyCode'];?>
 <?php echo $_smarty_tpl->getVariable('db_book')->value[0]['fVat'];?>
</td>
													</tr>
													<tr>
														<td align="left">Amount&nbsp;</td>
														<td> : </td>
														<td align="left"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vBookerCurrencyCode'];?>
 <?php echo $_smarty_tpl->getVariable('db_book')->value[0]['fAmount'];?>
</td>
													</tr>
													<tr>
														<td align="left">Booking Date&nbsp;</td>
														<td> : </td>
														<td align="left"><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_book')->value[0]['dBookingDate'],9);?>
 (<?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_book')->value[0]['dBookingTime'],12);?>
)</td>
													</tr>
													<?php if (@PAYMENT_OPTION=='PayPal'){?>
                          <tr>
														<td align="left">Paypal Transaction Id of Sender&nbsp;</td>
														<td> : </td>
														<td align="left"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vTransactionId'];?>
</td>
													</tr>
													<tr>
														<td align="left">Sender Payment Status&nbsp;</td>
														<td> : </td>
                            <td align="left"><?php if ($_smarty_tpl->getVariable('db_book')->value[0]['eBookerPaymentPaid']=='Yes'){?> Paid <?php }else{ ?> Unpaid <?php }?></td>
													</tr>
                          <tr>
														<td align="left">Paypal Transaction Id of Traveler&nbsp;</td>
														<td> : </td>
														<td align="left"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vDriverPaymentTransactionId'];?>
</td>
													</tr>
													<tr>
														<td align="left">Traveler Payment Status&nbsp;</td>
														<td> : </td>
														<td align="left"><?php if ($_smarty_tpl->getVariable('db_book')->value[0]['eDriverPaymentPaid']=='Yes'){?> Paid <?php }else{ ?> Unpaid <?php }?></td>
													</tr>
													<tr>
														<td align="left">Traveler Payment Vie&nbsp;</td>
														<td> : </td>
														<td align="left"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['eDriverPaymentBy'];?>
</td>
													</tr>
													<?php }?>
													</tbody>
                          </table>
												</td>
												<?php if ($_smarty_tpl->getVariable('db_book')->value[0]['eStatus']=='Cencelled'){?>
                        <td width="1%"></td>
												<td valign="top" width="52%">
													<table border="0" style="border: 1px solid #ccc;" cellpadding="1" cellspacing="1" width="100%">
													<tbody>
                          <tr>
    												<td colspan="3" width="100%"  class="headingbg wmatterbold" bgcolor="#E6E6E6" height="22">&nbsp;<b><span style="color:#000000">Cancellation Details</span></b></td>
    											</tr>
                          <tr>
														<td width="24%" valign="top">Cancelled By</td>
														<td width="1%" valign="top"> : </td>
														<?php if ($_smarty_tpl->getVariable('db_book')->value[0]['eCancelBy']=='Passenger'){?>
                            <td width="75%" valign="top"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vBookerFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vBookerLastName'];?>
 (Sender)</td>
                            <?php }else{ ?>
                            <td width="75%" valign="top"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vDriverFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vDriverLastName'];?>
 (Traveler)</td>
                            <?php }?>
													</tr>
													<tr>
														<td width="24%" valign="top">Cancellation On Date</td>
														<td width="1%" valign="top"> : </td>
														<td width="75%" valign="top"><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_book')->value[0]['dCancelDate'],7);?>
</td>
													</tr>
													<tr>
														<td width="24%" valign="top">Cancellation Reason</td>
														<td width="1%" valign="top"> : </td>
														<td width="75%" valign="top"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['tCancelReason'];?>
</td>
													</tr>
													<?php if (@PAYMENT_OPTION=='PayPal'){?>
                          <tr>
    												<td colspan="3" width="100%"  class="headingbg wmatterbold" bgcolor="#E6E6E6" height="22">&nbsp;<b><span style="color:#000000">Sender Refund</span></b></td>
    											</tr>                           
												  <form name="frmpassengerpay" id="frmpassengerpay" method="post" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=bo-bookings_a">
                          <input type="hidden" name="action" id="action" value="pay_ref_pass">
                          <input type="hidden" name="iBookingId_pass" id="iBookingId_pass" value="<?php echo $_smarty_tpl->getVariable('iBookingId')->value;?>
">
                          <tr>
														<td width="24%" valign="top">Refund Amount</td>
														<td width="1%" valign="top"> : </td>
														<td width="75%" valign="top"><b><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vBookerCurrencyCode'];?>
 <?php echo $_smarty_tpl->getVariable('db_book')->value[0]['fPassengerRefundAmount'];?>
</b></td>
													</tr> 													
													<tr>
														<td width="24%" valign="top">Refund Via</td>
														<td width="1%" valign="top"> : </td>
														<td width="75%" valign="top">
                            <span style="float: left;width: 144px;"><b>Payment Email (Paypal)  </b></span>: <?php echo $_smarty_tpl->getVariable('db_passenger_payment')->value[0]['vPaymentEmail'];?>
<br>
                            <b>Or</b><br>
                            <span style="float: left;width: 144px;"><b>Account Holder Name</b></span>: <?php echo $_smarty_tpl->getVariable('db_passenger_payment')->value[0]['vBankAccountHolderName'];?>
<br>
                            <span style="float: left;width: 144px;"><b>Account Number (IBAN) </b></span>: <?php echo $_smarty_tpl->getVariable('db_passenger_payment')->value[0]['vAccountNumber'];?>
<br>
                            <span style="float: left;width: 144px;"><b>Name of Bank  </b></span>: <?php echo $_smarty_tpl->getVariable('db_passenger_payment')->value[0]['vBankName'];?>
<br>
                            <span style="float: left;width: 144px;"><b>Bank Location  </b></span>: <?php echo $_smarty_tpl->getVariable('db_passenger_payment')->value[0]['vBankLocation'];?>
<br>
                            <span style="float: left;width: 144px;"><b>BIC/SWIFT Code  </b></span>: <?php echo $_smarty_tpl->getVariable('db_passenger_payment')->value[0]['vBIC_SWIFT_Code'];?>
<br>  
                            </td>
													</tr>
													<tr>
														<td width="24%" valign="top">Refund Paid</td>
														<td width="1%" valign="top"> : </td>
														<td width="75%" valign="top"><b><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['ePassengerRefundPaid'];?>
</b></td>
													</tr>
													<tr>
														<td width="24%" valign="top">Refund Date</td>
														<td width="1%" valign="top"> : </td>
														<td width="75%" valign="top"><b><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_book')->value[0]['dPassengerRefundDate'],7);?>
</b></td>
													</tr>
													<tr>
														<td width="24%" valign="top">Refund Paid Via</td>
														<td width="1%" valign="top"> : </td>
														<td width="75%" valign="top">
                            <select name="ePassengerRefundBy" id="ePassengerRefundBy">
                              <option value="">Select</option>
                              <option value="Paypal" <?php if ($_smarty_tpl->getVariable('db_book')->value[0]['ePassengerRefundBy']=='Paypal'){?> selected <?php }?>>Paypal</option>
                              <option  value="Bank" <?php if ($_smarty_tpl->getVariable('db_book')->value[0]['ePassengerRefundBy']=='Bank'){?> selected <?php }?>>Bank</option>
                            </select>
                            </td>
													</tr>
                          <tr>
														<td width="24%" valign="top">&nbsp;</td>
														<td width="1%" valign="top"> &nbsp; </td>
														<td width="75%" valign="top">
                            <input type="checkbox" name="emailtopass" id="emailtopass" value="Yes">Send refund mail to Sender
                            </td>
													</tr>													
													<tr>
														<td width="24%" valign="top">&nbsp;</td>
														<td width="1%" valign="top">&nbsp;</td>
                            <td width="75%" valign="top"><input type="button" value="Refund To Sender" class="btn" onClick="chk_pass_ref();" title="Refund To Sender"/></td>														
													</tr>
													</form>
													<?php if ($_smarty_tpl->getVariable('db_book')->value[0]['eCancelBy']=='Passenger'){?>
													<form name="frmdriverpay" id="frmdriverpay" method="post" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=bo-bookings_a">
                          <input type="hidden" name="action" id="action" value="pay_ref_driver">
                          <input type="hidden" name="iBookingId_driver" id="iBookingId_driver" value="<?php echo $_smarty_tpl->getVariable('iBookingId')->value;?>
">
                          <tr>
    												<td colspan="3" width="100%"  class="headingbg wmatterbold" bgcolor="#E6E6E6" height="22">&nbsp;<b><span style="color:#000000">Traveler Refund</span></b></td>
    											</tr>
													<tr>
														<td width="24%" valign="top">Refund Amount</td>
														<td width="1%" valign="top"> : </td>
														<td width="75%" valign="top"><b><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vBookerCurrencyCode'];?>
 <?php echo $_smarty_tpl->getVariable('db_book')->value[0]['fDriverRefundAmount'];?>
</b></td>
													</tr>   												
													<tr>
														<td width="24%" valign="top">Refund Via</td>
														<td width="1%" valign="top"> : </td>
														<td width="75%" valign="top">
                            <span style="float: left;width: 144px;"><b>Payment Email (Paypal)  </b></span>: <?php echo $_smarty_tpl->getVariable('db_driver_payment')->value[0]['vPaymentEmail'];?>
<br>
                            <b>Or</b><br>
                            <span style="float: left;width: 144px;"><b>Account Holder Name</b></span>: <?php echo $_smarty_tpl->getVariable('db_driver_payment')->value[0]['vBankAccountHolderName'];?>
<br>
                            <span style="float: left;width: 144px;"><b>Account Number (IBAN) </b></span>: <?php echo $_smarty_tpl->getVariable('db_driver_payment')->value[0]['vAccountNumber'];?>
<br>
                            <span style="float: left;width: 144px;"><b>Name of Bank  </b></span>: <?php echo $_smarty_tpl->getVariable('db_driver_payment')->value[0]['vBankName'];?>
<br>
                            <span style="float: left;width: 144px;"><b>Bank Location  </b></span>: <?php echo $_smarty_tpl->getVariable('db_driver_payment')->value[0]['vBankLocation'];?>
<br>
                            <span style="float: left;width: 144px;"><b>BIC/SWIFT Code  </b></span>: <?php echo $_smarty_tpl->getVariable('db_driver_payment')->value[0]['vBIC_SWIFT_Code'];?>
<br>  
                            </td>
													</tr>
													<tr>
														<td width="24%" valign="top">Refund Paid</td>
														<td width="1%" valign="top"> : </td>
														<td width="75%" valign="top"><b><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['eDriverRefundPaid'];?>
</b></td>
													</tr>
													<tr>
														<td width="24%" valign="top">Refund Date</td>
														<td width="1%" valign="top"> : </td>
														<td width="75%" valign="top"><b><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_book')->value[0]['dDriverRefundDate'],7);?>
</b></td>
													</tr>
													<tr>
														<td width="24%" valign="top">Refund Paid Via</td>
														<td width="1%" valign="top"> : </td>
														<td width="75%" valign="top">
                            <select name="eDriverRefundBy" id="eDriverRefundBy">
                              <option value="">Select</option>
                              <option value="Paypal" <?php if ($_smarty_tpl->getVariable('db_book')->value[0]['eDriverRefundBy']=='Paypal'){?> selected <?php }?>>Paypal</option>
                              <option value="Bank" <?php if ($_smarty_tpl->getVariable('db_book')->value[0]['eDriverRefundBy']=='Bank'){?> selected <?php }?>>Bank</option>
                            </select>
                            </td>
													</tr>
													<tr>
														<td width="24%" valign="top">&nbsp;</td>
														<td width="1%" valign="top"> &nbsp; </td>
														<td width="75%" valign="top">
                            <input type="checkbox" name="emailtodriver" id="emailtodriver" value="Yes">Send refund mail to Traveler
                            </td>
													</tr>	
                          <tr>
														<td width="24%" valign="top">&nbsp;</td>
														<td width="1%" valign="top">&nbsp;</td>
                            <td width="75%"><input type="button" value="Refund To Traveler" class="btn" onClick="chk_driver_ref();" title="Refund To Traveler"/></td>														
													</tr> 
                          </form>
                          <?php }?>
                          <?php }?>												
													</tbody>
                          </table>
												</td>
												<?php }?>
											</tr>
										</table>
										</td>
										</tr>
										
		            </table>
    <!--<form id="frmadd" name="frmadd" action="index.php?file=bo-bookings_a" method="post">
			<input type="hidden" name="iBookingId" id="iBookingId" value="<?php echo $_smarty_tpl->getVariable('iBookingId')->value;?>
" />
            <input type="hidden" name="action" id="action" value="<?php echo $_smarty_tpl->getVariable('mode')->value;?>
" />
            <input type="hidden" name="sendmail" id="sendmail" value="" />
            
									<table border="0" cellpadding="1" cellspacing="1" width="100%" align="center" class="oderpage">
									<tr>
										<td valign="top">
											<table class="headingbg" style="border: 1px solid #ccc;" border="0" cellpadding="1" cellspacing="1" width="100%" align="center">
										  <tr>
												<td class="headingbg wmatterbold" bgcolor="#309ec3" height="22">&nbsp;<b><span style="color:#000000">Booking Information</span></b></td>
											</tr>
											<tr bgcolor="#ffffff">
												<td valign="top" width="100%" align="center">
													<table border="0" cellpadding="1" cellspacing="1" width="50%">
													<tbody>
                          <tr>
														<td align="left">Booking No&nbsp;</td>
														<td> : </td>
														<td align="left" style="padding-left:12px;"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vBookingNo'];?>
</td>
													</tr>
													<tr>
														<td align="left" valign="top">Trips Details&nbsp;</td>
														<td valign="top"> : </td>
														<td align="left">
                              <table>
                                <tr>
                                  <td>From</td>
                                  <td>:</td>
                                  <td>
                                     <?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vFromPlace'];?>

                                  </td>
                                </tr>
                                <tr>
                                  <td>To</td>
                                  <td>:</td>
                                  <td><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vToPlace'];?>
</td>
                                </tr>
                              </table>
                            </td>
													</tr>
													<tbody><tr>
														<td align="left">Main Trip Details</td>
														<td> : </td>
														<td align="left" style="padding-left:12px;"><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=ri-rideslist&mode=edit&iRideId=<?php echo $_smarty_tpl->getVariable('db_book')->value[0]['iRideId'];?>
"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vMainRidePlaceDetails'];?>
</a></td>
													</tr>
													
                          <tr>
														<td align="left" valign="top">Sender Details&nbsp;</td>
														<td valign="top"> : </td>
														<td align="left">
                              <table>
                                <tr>
                                  <td>Name</td>
                                  <td>:</td>
                                  <td>
                                     <a href="index.php?file=m-member&mode=edit&iMemberId=<?php echo $_smarty_tpl->getVariable('db_book')->value[0]['iBookerId'];?>
"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vBookerFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vBookerLastName'];?>
</a>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Email</td>
                                  <td>:</td>
                                  <td><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vBookerEmail'];?>
</td>
                                </tr>
                                <tr>
                                  <td>Phone</td>
                                  <td>:</td>
                                  <td><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vBookerPhone'];?>
</td>
                                </tr>
                                
                              </table>
                            </td>
													</tr>
													<tr>
														<td align="left" valign="top">Traveler Details&nbsp;</td>
														<td valign="top"> : </td>
														<td align="left">
                              <table>
                                <tr>
                                  <td>Name</td>
                                  <td>:</td>
                                  <td>
                                     <a href="index.php?file=m-member&mode=edit&iMemberId=<?php echo $_smarty_tpl->getVariable('db_book')->value[0]['iDriverId'];?>
"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vDriverFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vDriverLastName'];?>
</a>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Email</td>
                                  <td>:</td>
                                  <td><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vDriverEmail'];?>
</td>
                                </tr>
                                <tr>
                                  <td>Phone</td>
                                  <td>:</td>
                                  <td><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vDriverPhone'];?>
</td>
                                </tr>
                                
                              </table>
                            </td>
													</tr>
													
													<tr>
														<td align="left">No. Of Seats&nbsp;</td>
														<td> : </td>
														<td align="left"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['iNoOfSeats'];?>
</td>
													</tr>
													<tr>
														<td align="left">Amount&nbsp;</td>
														<td> : </td>
														<td align="left"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vBookerCurrencyCode'];?>
 <?php echo $_smarty_tpl->getVariable('db_book')->value[0]['fAmount'];?>
</td>
													</tr>
													<tr>
														<td align="left">Booking Date&nbsp;</td>
														<td> : </td>
														<td align="left"><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_book')->value[0]['dBookingDate'],9);?>
 (<?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_book')->value[0]['dBookingTime'],12);?>
)</td>
													</tr>
													<tr>
														<td align="left">Paypal Transaction Id of Sender&nbsp;</td>
														<td> : </td>
														<td align="left"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vTransactionId'];?>
</td>
													</tr>
													<tr>
														<td align="left">Sender Payment Status&nbsp;</td>
														<td> : </td>
                            <td align="left"><?php if ($_smarty_tpl->getVariable('db_book')->value[0]['eBookerPaymentPaid']=='Yes'){?> Paid <?php }else{ ?> Unpaid <?php }?></td>
													</tr>
                          <tr>
														<td align="left">Paypal Transaction Id of Traveler&nbsp;</td>
														<td> : </td>
														<td align="left"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['vDriverPaymentTransactionId'];?>
</td>
													</tr>
													<tr>
														<td align="left">Traveler Payment Status&nbsp;</td>
														<td> : </td>
														<td align="left"><?php if ($_smarty_tpl->getVariable('db_book')->value[0]['eDriverPaymentPaid']=='Yes'){?> Paid <?php }else{ ?> Unpaid <?php }?></td>
													</tr>
													<tr>
														<td align="left">Traveler Payment Vie&nbsp;</td>
														<td> : </td>
														<td align="left"><?php echo $_smarty_tpl->getVariable('db_book')->value[0]['eDriverPaymentBy'];?>
</td>
													</tr>
													</tbody></table>
												</td>
											</tr>
												</table>
										</td>
										</tr>
										
		            </table>
                	</form>
                -->
								    
	                    <table width="100%">
                       <tr>                                 
                        <td style="float:right;" align="right">
                        <input type="button" value=" OK " class="btn" onClick="backtolist();" title="List"/>
                        <input type="button" value="Show Invoice" class="btn" onClick="for_print('<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
invoice_print.php?id=<?php echo $_smarty_tpl->getVariable('iBookingId')->value;?>
&print=yes');" title="Print Booking Invoice"/>
    <!--                     &nbsp;<input type="button" value="Print Commercial Invoice" class="btn" onclick="redirectprint('Commercial');" title="Print Commercial Invoice"/>
                        &nbsp;<input type="button" value="Print Booking List" class="btn" onclick="redirectprint('Packing');" title="Print Packing List"/>
                        &nbsp;<input type="button" value="Delete Booking" class="btnalt" onclick="del_ord();" title="Delete Order"/> 
   -->                     </td>
                        </tr>
                      </table>
							        
	
	</div>
</div>
<form name="frmlist" id="frmlist"  action="index.php?file=bo-bookings_a" method="post">
<input type="hidden" name="action" id="action" value="" />
<input  type="hidden" name="iBookingId" value=""/>
</form>

<script>
function for_print(url)
{
    window.open(url,'','width=700,height=900,resizable=yes,scrollbars=yes');
}
function backtolist(){
   window.location="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=bo-bookings&mode=view"
}
/*function redirectprint(type)
{
    var id = document.frmadd.iBookingId.value;
    window.location="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=bo-concertbook_print_admin&iBookingId="+id+"&print_page=Yes&type="+type;
    return false;
} 
function sendemailto()
{   
    document.getElementById("sendmail").value = 'yes';
    document.frmadd.submit();
   // window.location="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=bo-concertbook_print_admin&iBookingId="+id+"&print_page=Yes";
   // return false;
} */
function edit_paymentstatus(){
   document.frmadd.submit();
}
function del_ord(){
  var r=confirm("Are you sure to want delete this Booking?");
  if (r==true){
    document.frmlist.iBookingId.value = '<?php echo $_smarty_tpl->getVariable('iBookingId')->value;?>
';
    document.frmlist.action.value = 'Delete';
    document.frmlist.submit();	
  }else{
    return false;
  }
}

function chk_pass_ref(){
  if($('#ePassengerRefundBy').val() == ''){
    alert('Please select Refund Paid Via option first'); 
    return false;
  }
  
  document.frmpassengerpay.submit();
}

function chk_driver_ref(){
  if($('#eDriverRefundBy').val() == ''){
    alert('Please select Refund Paid Via option first'); 
    return false;
  }
  
  document.frmdriverpay.submit();
}

function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
