<?php /* Smarty version Smarty-3.0.7, created on 2015-10-05 16:55:31
         compiled from "/home/www/cargosharing1/cargoadmin/templates/report/view-rides_reports.tpl" */ ?>
<?php /*%%SmartyHeaderCode:76158924356125e2b92bf79-65665379%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ef9bda4ab4c8b8c8915a724e0a43f9c7fd5f5991' => 
    array (
      0 => '/home/www/cargosharing1/cargoadmin/templates/report/view-rides_reports.tpl',
      1 => 1432908292,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '76158924356125e2b92bf79-65665379',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<link type="text/css" rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
dhtmlgoodies_calendar.css" media="screen">
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
dhtmlgoodies_calendar.js"></script>
<script language="JavaScript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tcp_javascript'];?>
jlist.js"></script>
<div class="contentcontainer">
	<div class="headings altheading">
		<h2>Trips Posted Report</h2>
	</div>
	<div class="contentbox">
    <?php if ($_smarty_tpl->getVariable('var_msg_new')->value!=''){?>
     <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_success.png" title="Success" />
              <?php echo $_smarty_tpl->getVariable('var_msg_new')->value;?>
</p> 
     </div>     
    <div></div>
    <?php }elseif($_smarty_tpl->getVariable('var_msg')->value!=''){?>
    <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_success.png" title="Success" />
              <?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
</p> 
     </div>     
    <div></div>
    <?php }?>
		<form name="frmlist" id="frmlist" method="post" action="index.php?file=r-rides_reports&mode=view">
      <input type="hidden" id="action" name="action" value="datesearch">  
       <table width="100%" cellspacing="0" cellpadding="0" border="0">
										<tbody><tr>
											<td width="50%" class="td-listing" colspan="3">&nbsp;&nbsp;&nbsp;<font class="errormsg">&nbsp;</font>
											</td>
										</tr>
										<tr>	
											<td width="1%">&nbsp;</td>
											<td width="59%" valign="top">
												<b>Search Trips Posted by date</b>
												<table width="100%" cellspacing="1" cellpadding="1" border="0">
																										<tbody><tr>
														<td align="right" width="12%" valign="middle" height="35">Date From </td>
														<td width="1%" valign="middle"> :</td>
														<td align="left" width="26%" valign="middle">
															<input type="text" Readonly id="dFDate" name="dFDate" style="width:100px;" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('startdate')->value;?>
" lang="" title="Start Date"/>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
cal-icon.gif" style="cursor:pointer" onclick="displayCalendar(document.getElementById('dFDate'),'yyyy-mm-dd',this)" />
														</td>
														<td align="right" width="9%" valign="middle">Date To</td>
														<td width="1%" valign="middle"> :</td>
														<td align="left" width="35%" valign="middle">
															<input type="text" Readonly id="dTDate" name="dTDate" style="width:100px;" class="inputbox" value="<?php echo $_smarty_tpl->getVariable('enddate')->value;?>
" lang="" title="End Date"/>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
cal-icon.gif" style="cursor:pointer" onclick="displayCalendar(document.getElementById('dTDate'),'yyyy-mm-dd',this)" />
														</td>
													</tr>
													<tr>
														<td align="left" height="25" colspan="6">
															<b>Search Trips Posted by time period</b>
														</td>
													</tr>
													<tr>
														<td align="left" colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<img width="6" height="8" src="images/arrow.gif">&nbsp;&nbsp;[<a onclick="return todayDate('dFDate','dTDate');" href="javascript:void(0);">Today</a>]
															[<a onclick="return yesterdayDate('dFDate','dTDate');" href="javascript:void(0);">Yesterday</a>]
															<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img width="6" height="8" src="images/arrow.gif">&nbsp;&nbsp;[<a onclick="return currentweekDate('dFDate','dTDate');" href="javascript:void(0);">Current Week</a>]
															[<a onclick="return previousweekDate('dFDate','dTDate');" href="javascript:void(0);">Previous Week</a>]
															<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img width="6" height="8" src="images/arrow.gif">&nbsp;
															[<a onclick="return currentmonthDate('dFDate','dTDate');" href="javascript:void(0);">Current Month</a>]
															[<a onclick="return previousmonthDate('dFDate','dTDate');" href="javascript:void(0);">Previous Month</a>]
															<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	<img width="6" height="8" src="images/arrow.gif">&nbsp;
															[<a onclick="return currentyearDate('dFDate','dTDate');" href="javascript:void(0);">Current Year</a>] 
															[<a onclick="return previousyearDate('dFDate','dTDate');" href="javascript:void(0);">Previous Year</a>]
														</td>
													</tr>
												</tbody></table>
											</td>
											<td width="40%">
												<b>Show Trips Posted in the following Status</b><br><br> 
											 
											 <!--<input type="Checkbox" value="Active" name="sta[]" <?php if (in_array('Active',$_smarty_tpl->getVariable('sta')->value)){?>checked="checked"<?php }?>> Active   <br>
                        <input type="Checkbox" value="Inactive" name="sta[]" <?php if (in_array('Inactive',$_smarty_tpl->getVariable('sta')->value)){?>checked="checked"<?php }?> > Inactive   <br>
                        <input type="Checkbox" value="Pending" name="sta[]" <?php if (in_array('Pending',$_smarty_tpl->getVariable('sta')->value)){?>checked="checked"<?php }?>> Pending   <br> -->
                        <?php echo $_smarty_tpl->getVariable('status_chk')->value;?>

                      </td>
										</tr>
										<tr>
											<td width="5%">&nbsp;</td>
										</tr>
										<tr>
											<td colspan="3">
												<table align="center" width="100%" border="0">
													<tbody><tr>
														<td width="45%">&nbsp;</td>	
														<td width="55%">
														<input type="button" value="Search" class="btn" onclick="return checkvalid(1);" title="Search"/>	
														<input type="button" value="Export Trips" onclick="return checkvalid(2);" class="btnalt" />
                            </td>
													</tr>
												</tbody></table>
											</td>
										</tr>
									
									</tbody></table> 
        </form>
    <form name="frmlistmain" id="frmlistmain"  action="index.php" method="post">        
		<table width="100%" border="0">
		<input type="hidden" name="action" id="action" value="" />
    <input  type="hidden" name="iRideId" value=""/>
        <thead>
			<tr>
				<th width=""><a href="javascript:void(0);" onClick="Redirect('index.php?file=r-rides_reports&mode=view&sortby=1&order=<?php if ($_smarty_tpl->getVariable('order')->value==0){?>1<?php }else{ ?>0<?php }?>');">Title</a><?php if ($_smarty_tpl->getVariable('sortby')->value==1){?>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
<?php if ($_smarty_tpl->getVariable('order')->value==0){?>icons/up_active.png<?php }else{ ?>icons/down_active.png<?php }?>"><?php }?></th>               
        <th width="8%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=r-rides_reports&mode=view&sortby=2&order=<?php if ($_smarty_tpl->getVariable('order')->value==0){?>1<?php }else{ ?>0<?php }?>');">Member</a><?php if ($_smarty_tpl->getVariable('sortby')->value==2){?>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
<?php if ($_smarty_tpl->getVariable('order')->value==0){?>icons/up_active.png<?php }else{ ?>icons/down_active.png<?php }?>"><?php }?></th>               
        <!-- <th width="8%">Price</th>               
    	  <th width="7%">Total Seat</th>-->
    	  <?php if (@PAYMENT_OPTION!='Contact'){?>
        <!-- <th width="7%">Seat Left</th>-->
    	  <th width="5%">Booking</th>
    	  <?php }?> 
		  
    	   <!--   <th width="8%">Car</th>
	  <th width="8%">Good Deal</th> -->
    	  <th width="14%">Departure Time</th>
    	  <th width="14%">Arrival Time</th>
    	  <th width="10%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=r-rides_reports&mode=view&sortby=3&order=<?php if ($_smarty_tpl->getVariable('order')->value==0){?>1<?php }else{ ?>0<?php }?>');">Added Date</a><?php if ($_smarty_tpl->getVariable('sortby')->value==3){?>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
<?php if ($_smarty_tpl->getVariable('order')->value==1){?>icons/down_active.png<?php }else{ ?>icons/up_active.png<?php }?>"><?php }?></th>
        <th width="6%"><a href="javascript:void(0);" onClick="Redirect('index.php?file=r-rides_reports&mode=view&sortby=4&order=<?php if ($_smarty_tpl->getVariable('order')->value==0){?>1<?php }else{ ?>0<?php }?>');">Status</a><?php if ($_smarty_tpl->getVariable('sortby')->value==4){?>&nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
<?php if ($_smarty_tpl->getVariable('order')->value==0){?>icons/up_active.png<?php }else{ ?>icons/down_active.png<?php }?>"><?php }?></th>
	</tr>
		</thead>
		<tbody>
      <?php if (count($_smarty_tpl->getVariable('db_order_all')->value)>0){?>            
		  <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_order_all')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
     <tr>
		<td><a target="_BLANK" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=ri-rideslist&mode=edit&iRideId=<?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iRideId'];?>
" title=""><?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainDeparture'];?>
 - <?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainArrival'];?>
</a></td>
		<td><a target="_BLANK" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=m-member&mode=edit&iMemberId=<?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iMemberId'];?>
" title=""><?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vLastName'];?>
</a></td>
 		<!-- 	<td><?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBookerCurrencyCode'];?>
 &nbsp;<?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fPrice'];?>
</td>
		<td style="text-align:center;"> <?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iSeats'];?>
 </td> -->
      <?php if (@PAYMENT_OPTION!='Contact'){?>
      <!-- <td style="text-align:center;"> <?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iSeats']-$_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['totbooking'];?>
 </td>         -->
      <td><?php if ($_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['totbooking']>0){?><?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['totbooking'];?>
 &nbsp;<a target="_BLANK" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_url'];?>
/index.php?file=bo-bookings&mode=view&iRideId=<?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iRideId'];?>
">view</a><?php }else{ ?>--<?php }?></td>  
			<?php }?>
   <!--   <td><?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMake'];?>
 <?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['model'];?>
</td>
      <td style="text-align:center;"> <?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eGoodDeal'];?>
 </td> -->
      <?php if ($_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eRideType']=='Reccuring'){?>
       <td><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dStartDate'],9);?>
 (<?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainOutBoundTime'],12);?>
)</td>
			 <td><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dEndDate'],9);?>
 (<?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainReturnTime'],12);?>
)</td>
      <?php }else{ ?>
       <td><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dDepartureDate'],9);?>
 (<?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainDepartureTime'],12);?>
)</td>
			 <td><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dArrivalDate'],9);?>
 (<?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainArrivalTime'],12);?>
)</td>
      <?php }?>
      <td> <?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dAddedDate'],9);?>
 </td>  
			<td> <?php echo $_smarty_tpl->getVariable('db_order_all')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eStatus'];?>
 </td>
		</tr>
     <?php endfor; endif; ?>
     <?php }else{ ?>
     <tr>
			<td colspan="11" height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No Record Found.</td>
		</tr>
        <?php }?>
		</tbody>
		</table>
        </form>
		<div class="extrabottom">  			
			<div class="bulkactions">
				<select name="newaction" id="newaction">
					<option value="">Select Action</option>
					<option value="Show All">Show All</option>
				</select>
				<input type="submit" value="Apply" class="btn" onclick="return Doaction(document.getElementById('newaction').value,'r-rides_reports',document.frmlist);"/>
			</div>
		</div>
        <div>
            <div class="pagination">
            <?php if (count($_smarty_tpl->getVariable('db_order_all')->value)>0){?>
	         <span class="switch" style="float: left;"><?php echo $_smarty_tpl->getVariable('recmsg')->value;?>
</span>
	        <?php }?>
            </div>
            <?php echo $_smarty_tpl->getVariable('page_link')->value;?>

        </div>
		
        <!--<ul class="pagination">
			<li class="text">Previous</li>
			<li class="page"><a href="#" title="">1</a></li>
			<li><a href="#" title="">2</a></li>
			<li><a href="#" title="">3</a></li>
			<li><a href="#" title="">4</a></li>
			<li class="text"><a href="#" title="">Next</a></li>
		</ul>-->
		<div style="clear: both;"></div></div>
</div>

<script>
function Searchoption(){
    document.getElementById('frmsearch').submit();
}
function AlphaSearch(val){
    var alphavalue = val;
    var file = 'r-rides_reports';
    window.location="index.php?file="+file+"&alp="+alphavalue+"&mode=view";
    return false;
}
function MakeAction(loopid,type){
    document.frmlist.iRideId.value = loopid;
    document.frmlist.action.value = type;
	document.frmlist.submit();	
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
function checkvalid(val){          
	if(document.frmlist.dTDate.value < document.frmlist.dFDate.value){
  		alert("From date should be lesser than To date.")
  		document.frmlist.dFDate.select();
		return false;
  	}
    if(val==1){
      document.frmlist.action="index.php?file=r-rides_reports&mode=view";
      document.frmlist.submit();	
     }else{
      document.frmlist.action="index.php?file=r-export_rides&mode=edit";
      document.frmlist.submit();	
    }
}

function todayDate(dt,df)
{
	document.frmlist.elements[dt].value='<?php echo date('Y-m-d');?>
';
	document.frmlist.elements[df].value='<?php echo date('Y-m-d');?>
'; 
}
function yesterdayDate(dt,df)
{
	document.frmlist.elements[dt].value='<?php echo $_smarty_tpl->getVariable('yesterday')->value;?>
';
	document.frmlist.elements[df].value='<?php echo $_smarty_tpl->getVariable('yesterday')->value;?>
';	
}
function currentweekDate(dt,df)
{ 
	document.frmlist.elements[dt].value='<?php echo $_smarty_tpl->getVariable('currweekFDate')->value;?>
';
	document.frmlist.elements[df].value='<?php echo $_smarty_tpl->getVariable('currweekTDate')->value;?>
';	
}
function previousweekDate(dt,df)
{
	document.frmlist.elements[dt].value='<?php echo $_smarty_tpl->getVariable('prevweekFDate')->value;?>
';
	document.frmlist.elements[df].value='<?php echo $_smarty_tpl->getVariable('prevweekTDate')->value;?>
';	
}
function currentmonthDate(dt,df)
{
	document.frmlist.elements[dt].value='<?php echo $_smarty_tpl->getVariable('currmonthFDate')->value;?>
';
	document.frmlist.elements[df].value='<?php echo $_smarty_tpl->getVariable('currmonthTDate')->value;?>
';
}
function previousmonthDate(dt,df)
{
	document.frmlist.elements[dt].value='<?php echo $_smarty_tpl->getVariable('prevmonthFDate')->value;?>
';
	document.frmlist.elements[df].value='<?php echo $_smarty_tpl->getVariable('prevmonthTDate')->value;?>
';	
}
function currentyearDate(dt,df)
{
	document.frmlist.elements[dt].value='<?php echo $_smarty_tpl->getVariable('curryearFDate')->value;?>
';
	document.frmlist.elements[df].value='<?php echo $_smarty_tpl->getVariable('curryearTDate')->value;?>
';	
}
function previousyearDate(dt,df)
{
	document.frmlist.elements[dt].value='<?php echo $_smarty_tpl->getVariable('prevyearFDate')->value;?>
';
	document.frmlist.elements[df].value='<?php echo $_smarty_tpl->getVariable('prevyearTDate')->value;?>
';	
}
function checkAll()
{ 
	var rs = (document.frmlist.abc.checked)?true:false;
	
	for(i=0;i<document.frmlist.elements.length;i++)
	{
	  	if(document.frmlist.elements[i].id == 'iId')
  		{
			document.frmlist.elements[i].checked = rs;
		}

	}  
}
</script>
