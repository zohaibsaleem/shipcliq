<?php /* Smarty version Smarty-3.0.7, created on 2015-05-01 12:55:49
         compiled from "/home/www/blablaclone/bcadmin/templates/member/view-member_alert.tpl" */ ?>
<?php /*%%SmartyHeaderCode:96070408055432a7d6a6505-21849283%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fc1640465f7f7a168d19b7c809d97c1d3c291b0d' => 
    array (
      0 => '/home/www/blablaclone/bcadmin/templates/member/view-member_alert.tpl',
      1 => 1395812527,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '96070408055432a7d6a6505-21849283',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="contentcontainer" id="tabs">
	<div class="headings">
		<h2>Member Alert</h2>
	</div>
	<div class="contentbox" id="tabs-1">
	<div id="tabs2" class="VideoTabs">
	  <ul>
    	  <li><a href="javascript:void(0);" onclick="javascript:window.location='index.php?file=m-member&mode=edit&iMemberId=<?php echo $_smarty_tpl->getVariable('iMemberId')->value;?>
';" id="tab1"><em>Member Details</em></a></li>
    	  <li><a href="javascript:void(0);" id="tab2" class="current"><em>Member Alert</em></a></li>			  
    	  <li><a href="javascript:void(0);" onclick="javascript:window.location='index.php?file=m-member_rate&mode=view&iMemberId=<?php echo $_smarty_tpl->getVariable('iMemberId')->value;?>
';" id="tab3"><em>Member Ratings & Reviews</em></a></li>
    </ul>
		<div style="clear:both"></div>
	</div>
	<div id="keyfeatures" class="VideoText">
   	<table width="100%" style="width:100%;border:1px solid #CCCCCC;border-collapse: collapse;">
	  <thead>
			<tr>
				<th width="20%">#Sr. No.</th>               
        <th width="30%">From Location</th>               
        <th width="30%">To Location</th>               
    	  <th width="20%">Travel Date</th>
    	</tr>
		</thead>
		<tbody>
    <?php if (count($_smarty_tpl->getVariable('db_alert')->value)>0){?>
		<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_alert')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
		  <tr>
		    <td style="padding-left:20px;"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index']+1;?>
</td>
		    <td style="padding-left:10px;"><?php echo $_smarty_tpl->getVariable('db_alert')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vStartPoint'];?>
</td>
		    <td style="padding-left:10px;"><?php echo $_smarty_tpl->getVariable('db_alert')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vEndPoint'];?>
</td>
		    <td style="padding-left:10px;"><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_alert')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dDate'],14);?>
</td>
		  </tr>
    <?php endfor; endif; ?>
    <?php }else{ ?>
    <tr>
			<td height="70px;" colspan="4" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No Alert Created.</td>
		</tr>
    <?php }?>
		</tbody>
		</table>
   </div>
</div>
</div>

<script>

function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
$(document).ready(function(){
$(function() {
    $( "#tabs2" ).tabs({ selected: 1 });
  });
});
</script>
