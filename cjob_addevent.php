<?php
  define( '_TEXEC', 1 );
  define('TPATH_BASE', dirname(__FILE__) );
  define( 'DS', DIRECTORY_SEPARATOR );
  
  require_once ( TPATH_BASE .DS.'includes'.DS.'defines.php' ); 
  require_once ( TPATH_BASE .DS.'includes'.DS.'configuration.php' );
  
  #notification mails   
  $sql = "SELECT iMemberId, vFirstName, vLastName, vEmail FROM member WHERE eStatus = 'Active' AND eNotificationMail = 'Yes'";
  $mail_members = $obj->MySQLSelect($sql);
 
  for($i=0;$i<count($mail_members);$i++){ 
    $sql = "SELECT iCategoryId FROM volunteer_interests WHERE iMemberId = '".$mail_members[$i]['iMemberId']."'";
    $member_interest = $obj->MySQLSelect($sql); 
    
    if(count($member_interest) > 0){
      $interest = array_map('current', $member_interest);   
      $interest = implode(', ', $interest);
     
      $sql = "SELECT event.iEventId, event.vTitle, event.tDetails, organization.vOrganizationName 
              FROM event   
              LEFT JOIN organization ON organization.iOrganizationId = event.iOrganizationId 
              LEFT JOIN events_category ON events_category.iEventId = event.iEventId
              WHERE event.eStatus = 'Active' AND event.dAddedDate >= CURRENT_DATE() AND events_category.iCategoryId IN(".$interest.") GROUP BY event.iEventId ORDER BY event.iEventId ASC";
      $events = $obj->MySQLSelect($sql);
      
      if(count($events) > 0){
        $cont = '';
        for($j=0;$j<count($events);$j++){
          $cont .= '<div style="border: 1px solid #CCCCCC;float: left;margin: 0 0 15px;padding: 10px;width: 95%;">';  
          $cont .= '<div style="float: right; margin: 0;padding: 0;width: 100%;">';
          $cont .= '<h2 style="border: medium none;color: #92A20A;float: left;font-size: 19px;margin: 0;padding: 0;width: 100%;"><a href="'.$tconfig['tsite_url'].'event-details/'.$events[$j]['iEventId'].'" target="_blank" style="color: #92A20A;font-size: 19px;">'.$events[$j]['vTitle'].'</a></h2>';
          $cont .= '<h3>'.$events[$j]['vOrganizationName'].'</h3>';
          $cont .= '<p style="float: left;margin: 0;padding: 0;width: 100%;">'.$generalobj->truncate_string($events[$j]['tDetails'], 250).'</p>';
          $cont .= '<span style="float: left;font-size: 14px;margin: 5px 0 0;padding: 0;width: 100%;"><a href="'.$tconfig['tsite_url'].'event-details/'.$events[$j]['iEventId'].'" target="_blank" style="color: #92A20A;float: right;font-size: 14px;margin: 0;padding: 0;">View More</a></span> </div>';
          $cont .= '</div>';
        }
        
        $Data['event_details'] = $cont;
        $Data['name'] = $mail_members[$i]['vFirstName'].' '.$mail_members[$i]['vLastName'];
        $Data['email'] = $mail_members[$i]['vEmail'];
        $generalobj->send_email_user("NEW_EVENT_MAIL",$Data);          
      }
    }     
  }
  echo 'rb';
  exit;  
?>
