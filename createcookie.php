<?php
ob_start();      
session_start();
define( '_TEXEC', 1 );
define('TPATH_BASE', dirname(__FILE__) );
define( 'DS', DIRECTORY_SEPARATOR );

require_once ( TPATH_BASE .DS.'includes'.DS.'defines.php' ); 
require_once ( TPATH_BASE .DS.'includes'.DS.'configuration.php' );

if($_COOKIE['userlatitude'] != '' && $_COOKIE['userlongitude'] != ''){  
  $country_code = $ridesobj->get_user_country($_COOKIE['userlatitude'], $_COOKIE['userlongitude']); 
  if($country_code != ''){   
    if($country_code == 'GB'){                //UK
      $_SESSION['sess_lang'] = "EN";
      $_SESSION['sess_price_ratio'] = 'GBP';
    }else if($country_code == 'NO'){          //Norway    
      $_SESSION['sess_lang'] = "NO";
      $_SESSION['sess_price_ratio'] = 'NOK';
    }else if($country_code == 'PL'){          //Poland
      $_SESSION['sess_lang'] = "PO";
      $_SESSION['sess_price_ratio'] = 'PLN';
    }else if($country_code == 'FI'){          //Finland
      $_SESSION['sess_lang'] = "FI";
      $_SESSION['sess_price_ratio'] = 'EUR';
    }else if($country_code == 'RU'){          //Russia
      $_SESSION['sess_lang'] = "RS";
      $_SESSION['sess_price_ratio'] = 'RUB';
    }else if($country_code == 'SE'){          //Sweden
      $_SESSION['sess_lang'] = "SW";
      $_SESSION['sess_price_ratio'] = 'SEK';
    }else if($country_code == 'DK'){          //Denmark
      //$_SESSION['sess_lang'] = "DN";
      $_SESSION['sess_price_ratio'] = 'DKK';
      $_SESSION['sess_lang'] = "EN";
    }else if($country_code == 'DE'){          //Germany
      $_SESSION['sess_lang'] = "DE";
      $_SESSION['sess_price_ratio'] = 'EUR';
    }else if($country_code == 'FR'){          //France
      $_SESSION['sess_lang'] = "FN";
      $_SESSION['sess_price_ratio'] = 'EUR';
    }else if($country_code == 'ES'){          //Italy
      $_SESSION['sess_lang'] = "ES";
      $_SESSION['sess_price_ratio'] = 'EUR';
    }else{
      $_SESSION['sess_lang'] = "EN";
      //$_SESSION['sess_price_ratio'] = 'EUR';
      $_SESSION['sess_price_ratio'] = 'USD';
    } 
     $sql="select vName from currency where eStatus='Active'";
	$db_currency=$obj->MySQLSelect($sql);
		if(!in_array($_SESSION['sess_price_ratio'],$db_currency))
		{
			$ridesobj->set_default_language_currency();
		}
	$sql="select vCode from language_master where eStatus='Active'";
	$db_lang_master=$obj->MySQLSelect($sql);
		if(!in_array($_SESSION['sess_lang'],$db_lang_master))
		{
			$ridesobj->set_default_language_currency();
		}
    $expire=time()+60*60*24*30;
    setcookie("country_code", $country_code, $expire);     
    echo $country_code;
    exit;
  }
} 

/*  Country wise information
http://maps.googleapis.com/maps/api/geocode/json?latlng=41.892916,12.482520&sensor=true //Italy
http://maps.googleapis.com/maps/api/geocode/json?latlng=48.856614,2.352222&sensor=true  //France
http://maps.googleapis.com/maps/api/geocode/json?latlng=52.520007,13.404954&sensor=true //Germany
http://maps.googleapis.com/maps/api/geocode/json?latlng=55.676097,12.568337&sensor=true //Denmark
http://maps.googleapis.com/maps/api/geocode/json?latlng=59.328930,18.064910&sensor=true //Sweden
http://maps.googleapis.com/maps/api/geocode/json?latlng=55.755826,37.617300&sensor=true //Russia
http://maps.googleapis.com/maps/api/geocode/json?latlng=60.173324,24.941025&sensor=true //Finland
http://maps.googleapis.com/maps/api/geocode/json?latlng=52.229676,21.012229&sensor=true //Poland
http://maps.googleapis.com/maps/api/geocode/json?latlng=59.915666,10.745541&sensor=true //Norway
http://maps.googleapis.com/maps/api/geocode/json?latlng=51.505778,-0.130838&sensor=true //United Kingdom
*/ 
exit;
?>

