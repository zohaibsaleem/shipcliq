<?php 
  session_start();
  define( '_TEXEC', 1 );
  define('TPATH_BASE', dirname(__FILE__) );
  define( 'DS', DIRECTORY_SEPARATOR );
  require_once ( TPATH_BASE .DS.'includes'.DS.'defines.php' );
  require_once ( TPATH_BASE .DS.'includes'.DS.'configuration.php' );
  
  function get_currency_ratio($from_Currency,$to_Currency,$amount) {
    $amount = urlencode($amount);
    $from_Currency = urlencode($from_Currency);
    $to_Currency = urlencode($to_Currency);        
    $url = "http://rate-exchange.appspot.com/currency?from=$from_Currency&to=$to_Currency&q=$amount"; 
    $ch = curl_init();
    $timeout = 0;
    curl_setopt ($ch, CURLOPT_URL, $url);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch,  CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $rawdata = curl_exec($ch);
    curl_close($ch);
    $data = explode(', ', $rawdata);
    $data = explode(': ', $data['3']);
    $var = $data['1'];
    return round($var,6);
  }     
  
  $sql = "SELECT iCurrencyId, vName, GBP, NOK, SEK, DKK, PLN, RUB, EUR, USD FROM currency ORDER BY iCurrencyId ASC";
  $db_currency = $obj->MySQLSelect($sql); 
 
  for($i=0;$i<count($db_currency);$i++){
    $ratio = get_currency_ratio($db_currency[$i]['vName'], 'EUR', 1);
    $ratio_GBP = get_currency_ratio($db_currency[$i]['vName'], 'GBP', 1);
    $ratio_NOK = get_currency_ratio($db_currency[$i]['vName'], 'NOK', 1);
    $ratio_SEK = get_currency_ratio($db_currency[$i]['vName'], 'SEK', 1);
    $ratio_DKK = get_currency_ratio($db_currency[$i]['vName'], 'DKK', 1);
    $ratio_PLN = get_currency_ratio($db_currency[$i]['vName'], 'PLN', 1);
    $ratio_RUB = get_currency_ratio($db_currency[$i]['vName'], 'RUB', 1);
    $ratio_EUR = get_currency_ratio($db_currency[$i]['vName'], 'EUR', 1);
    $ratio_USD = get_currency_ratio($db_currency[$i]['vName'], 'USD', 1);
    
    if($ratio != '' && is_numeric($ratio)){
      $sql = "UPDATE currency SET Ratio = '".$ratio."' WHERE iCurrencyId = '".$db_currency[$i]['iCurrencyId']."' AND vName = '".$db_currency[$i]['vName']."'";
      $db_update = $obj->sql_query($sql);
    }
    if($ratio_GBP != '' && is_numeric($ratio_GBP)){
      $sql = "UPDATE currency SET GBP = '".$ratio_GBP."' WHERE iCurrencyId = '".$db_currency[$i]['iCurrencyId']."' AND vName = '".$db_currency[$i]['vName']."'";
      $db_update = $obj->sql_query($sql);
    }
    if($ratio_NOK != '' && is_numeric($ratio_NOK)){
      $sql = "UPDATE currency SET NOK = '".$ratio_NOK."' WHERE iCurrencyId = '".$db_currency[$i]['iCurrencyId']."' AND vName = '".$db_currency[$i]['vName']."'";
      $db_update = $obj->sql_query($sql);
    }
    if($ratio_SEK != '' && is_numeric($ratio_SEK)){
      $sql = "UPDATE currency SET SEK = '".$ratio_SEK."' WHERE iCurrencyId = '".$db_currency[$i]['iCurrencyId']."' AND vName = '".$db_currency[$i]['vName']."'";
      $db_update = $obj->sql_query($sql);
    }
    if($ratio_DKK != '' && is_numeric($ratio_DKK)){
      $sql = "UPDATE currency SET DKK = '".$ratio_DKK."' WHERE iCurrencyId = '".$db_currency[$i]['iCurrencyId']."' AND vName = '".$db_currency[$i]['vName']."'";
      $db_update = $obj->sql_query($sql);
    }
    if($ratio_PLN != '' && is_numeric($ratio_PLN)){
      $sql = "UPDATE currency SET PLN = '".$ratio_PLN."' WHERE iCurrencyId = '".$db_currency[$i]['iCurrencyId']."' AND vName = '".$db_currency[$i]['vName']."'";
      $db_update = $obj->sql_query($sql);
    }
    if($ratio_RUB != '' && is_numeric($ratio_RUB)){
      $sql = "UPDATE currency SET RUB = '".$ratio_RUB."' WHERE iCurrencyId = '".$db_currency[$i]['iCurrencyId']."' AND vName = '".$db_currency[$i]['vName']."'";
      $db_update = $obj->sql_query($sql);
    }
    if($ratio_EUR != '' && is_numeric($ratio_EUR)){
      $sql = "UPDATE currency SET EUR = '".$ratio_EUR."' WHERE iCurrencyId = '".$db_currency[$i]['iCurrencyId']."' AND vName = '".$db_currency[$i]['vName']."'";
      $db_update = $obj->sql_query($sql);
    }
    if($ratio_USD != '' && is_numeric($ratio_USD)){
      $sql = "UPDATE currency SET USD = '".$ratio_USD."' WHERE iCurrencyId = '".$db_currency[$i]['iCurrencyId']."' AND vName = '".$db_currency[$i]['vName']."'";
      $db_update = $obj->sql_query($sql);
    }    
  }
  
  echo 'rb'; exit;
?>
