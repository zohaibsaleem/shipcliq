<?php
/*
	This is a simple example script that shows how easy is to integrate your application within draugiem.lv.
	Feel free to use it in your own projects.
*/
ob_start();
//session_start();
define( '_TEXEC', 1 );
define('TPATH_BASE', dirname(__FILE__) );
define( 'DS', DIRECTORY_SEPARATOR );
require_once ( TPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once ( TPATH_BASE .DS.'includes'.DS.'configuration.php' );

//include 'DraugiemApi.php';
require ( TPATH_LIBRARIES.'/draugiem/DraugiemApi.php' );

//$app_key = 'd6949661ccde33a65d98653043bc3119';//Application API key of your app goes here
//$app_id = 999;//Application ID of your app goes here
             
$app_key = '6159fd7675b4f300a56565f44e40405a';//Application API key of your app goes here
$app_id = '15016552';//Application ID of your app goes here

include_once(TPATH_LIBRARIES."/general/Imagecrop.class.php");
$thumb = new thumbnail();
$temp_gallery = $tconfig["tsite_temp_gallery"];

$draugiem = new DraugiemApi($app_id, $app_key);//Create Passport object
#echo "<pre>";print_r($draugiem);exit;
session_start(); //Start PHP session

$draugiem->cookieFix(); //Iframe cookie workaround for IE and Safari

$session = $draugiem->getSession();//Authenticate user

if($session){//Authentication successful
  
	$user = $draugiem->getUserData();//Get user info
  $perm = $draugiem->getPermissions();
	#echo "<pre>";print_r($user);exit;

  $Photo_Gallery_folder = $tconfig["tsite_upload_images_member_path"].$_SESSION['sess_iMemberId']."/";
  if(!is_dir($Photo_Gallery_folder))
  {
    mkdir($Photo_Gallery_folder, 0777);
  }
  //	print_r($user);
  $baseurl = $user['imgl'];
  $url = basename($baseurl);
        
  $image_name =  system("wget -O ".$Photo_Gallery_folder.$url." ".$baseurl);
        
    if(is_file($Photo_Gallery_folder.$url))
    {
       include_once(TPATH_LIBRARIES."/general/SimpleImage.class.php");
       $img = new SimpleImage();           
       list($width, $height, $type, $attr)= getimagesize($Photo_Gallery_folder.$url);           

       if($width < $height){
          $final_width = $width;
       }else{
          $final_width = $height;
       }       
       $img->load($Photo_Gallery_folder.$url)->crop(0, 0, $final_width, $final_width)->save($Photo_Gallery_folder.$url);
       $imgname = $generalobj->img_data_upload($Photo_Gallery_folder,$url,$Photo_Gallery_folder, $tconfig["tsite_upload_images_member_size1"], $tconfig["tsite_upload_images_member_size2"], $tconfig["tsite_upload_images_member_size3"],"");                       
    }  
     @unlink($Photo_Gallery_folder.$url);
     
     $sql = "UPDATE member set vImage='".$imgname."' WHERE iMemberId='".$_SESSION['sess_iMemberId']."'";
     $obj->sql_query($sql); 
    
     $db_sql = "select * from member WHERE iMemberId='".$_SESSION['sess_iMemberId']."'";
     $db_data = $obj->MySQLSelect($db_sql);
     $_SESSION["sess_vImage"]= $db_data[0]['vImage'];		
    
    
     header("Location:".$tconfig["tsite_url"]."profile-photo");
     exit;

//	print_r($perm);

	exit;
?><html>
	<head>
		<title>Draugiem API test</title>
<?php
			echo $draugiem->getJavascript('body', 'http://'.$_SERVER['SERVER_NAME'].dirname($_SERVER['PHP_SELF']).'/callback.html'); //Set up JS API + iframe Resize
?>
	</head>
	<body>
		<div id="body">
<?php
			//Print greeting for user
			echo '<h2>Hello, '.$user['name'].' '.$user['surname'].'</h2>';
			if($user['img']){
				echo '<img src="'.$draugiem->imageForSize($user['img'], 'medium').'" alt="" />';
			}

			echo '<hr />';
			
			//Show 10 users of this app with small image
			if($users = $draugiem->getAppUsers(1, 10)){

				echo '<h3>Some users of this application</h3>';

				foreach($users as $friend){
					if($friend['img']){
						echo '<img src="'.$draugiem->imageForSize($friend['img'], 'icon').'" alt="" align="left"  />';
					}
					echo '<a target="_top" href="http://'.$draugiem->getSessionDomain().'/friend/?'.$friend['uid'].'">';
					echo $friend['name'].' '.$friend['surname'].'</a><br />';
					if($friend['nick']){
						echo '('.$friend['nick'].') ';
					}
					echo $friend['place'].'<br style="clear:both" />';
				}
			}

			//Show 10 friends with normal size image
			if($users = $draugiem->getUserFriends(1, 10)){

				echo '<h3>Your friends who also use this application</h3>';

				foreach($users as $friend){
					if($friend['img']){
						echo '<img src="'.$friend['img'].'" alt="" align="left" style="clear:left" />';
					}
					echo '<a target="_top" href="http://'.$draugiem->getSessionDomain().'/friend/?'.$friend['uid'].'">';
					echo $friend['name'].' '.$friend['surname'].'</a><br />';
					if($friend['nick']){
						echo '('.$friend['nick'].') ';
					}
					echo $friend['place'].'<br style="clear:both" />';
				}
			}
?>
		</div>
	</body>
</html>
<?php

} else {//User not logged in, session failed
	echo 'AUTHENTICATION FAILED';
	//session_destroy();         
	
}