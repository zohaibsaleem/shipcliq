<?php
  ob_start();      
  session_start();
  define( '_TEXEC', 1 );
  define('TPATH_BASE', dirname(__FILE__) );
  define( 'DS', DIRECTORY_SEPARATOR );
  
  require_once ( TPATH_BASE .DS.'includes'.DS.'defines.php' ); 
  require_once ( TPATH_BASE .DS.'includes'.DS.'configuration.php' ); 
  
  $curr = date("Y-m-d");
  
  $sql = "SELECT iRideId FROM rides_new WHERE dAddedDate = '".$curr."' AND eStatus = 'Active'";
  $db_rides = $obj->MySQLSelect($sql);
  #echo "<pre>";print_r($db_rides);exit;
  if(count($db_rides) > 0){
    $sql = "SELECT * FROM member_ride_alert WHERE dDate >= '".$curr."'";
    $db_alerts = $obj->MySQLSelect($sql);    
    
    if(count($db_alerts) > 0){
      for($i=0;$i<count($db_alerts);$i++){
        # Get Language preference of member #
        $sql = "SELECT vLanguageCode FROM member WHERE vEmail = '".$db_alerts[$i]['vAlertEmail']."'";
        $db_mem_mail = $obj->MySQLSelect($sql);
        if(count($db_mem_mail) == 0){
           $db_mem_mail[0]['vLanguageCode'] = "EN";
        }
        
        $sql = "SELECT * from language_label WHERE vCode = '".$db_mem_mail[0]['vLanguageCode']."'";
        $db_label = $obj->MySQLSelect($sql);  
        $vLabel = array(); 
        for($j=0;$j<count($db_label);$j++){
      		$vLabel[$db_label[$j]['vLabel']]  = $db_label[$j]["vValue"];
      	}
        # Get Language preference of member #
        $stack = array();
        $sql = "SELECT ride_points_new.iRidePointId, ride_points_new.iRideId, ride_points_new.tRideTime, ride_points_new.eReverse, 
                ( 3959 * acos( cos( radians(".$db_alerts[$i]['vStartLatitude'].") ) * cos( radians( vStartLatitude ) ) * cos( radians( vStartLongitude ) - radians(".$db_alerts[$i]['vStartLongitude'].") ) + sin( radians(".$db_alerts[$i]['vStartLatitude'].") ) * sin( radians( vStartLatitude ) ) ) ) AS distance, 
                ride_dates.dDateOut, ride_dates.dDateRet, rides_new.iMemberId  
                FROM ride_points_new 
                LEFT JOIN rides_new ON rides_new.iRideId = ride_points_new.iRideId 
                LEFT JOIN ride_dates ON ride_dates.iRideId = ride_points_new.iRideId 
                WHERE rides_new.dAddedDate = '".$curr."' 
                AND (ride_dates.dDateOut = '".$db_alerts[$i]['dDate']."' OR ride_dates.dDateRet = '".$db_alerts[$i]['dDate']."')  
                HAVING distance < 5  
                ORDER BY distance";
        $db_start_points = $obj->MySQLSelect($sql);
       
        $sql = "SELECT ride_points_new.iRidePointId, ride_points_new.iRideId, ride_points_new.tRideTime, ride_points_new.eReverse, 
                ( 3959 * acos( cos( radians(".$db_alerts[$i]['vEndLatitude'].") ) * cos( radians( vEndLatitude ) ) * cos( radians( vEndLongitude ) - radians(".$db_alerts[$i]['vEndLongitude'].") ) + sin( radians(".$db_alerts[$i]['vEndLatitude'].") ) * sin( radians( vEndLatitude ) ) ) ) AS distance, 
                ride_dates.dDateOut, ride_dates.dDateRet, rides_new.iMemberId  
                FROM ride_points_new 
                LEFT JOIN rides_new ON rides_new.iRideId = ride_points_new.iRideId 
                LEFT JOIN ride_dates ON ride_dates.iRideId = ride_points_new.iRideId 
                WHERE rides_new.dAddedDate = '".$curr."' 
                AND (ride_dates.dDateOut = '".$db_alerts[$i]['dDate']."' OR ride_dates.dDateRet = '".$db_alerts[$i]['dDate']."')  
                HAVING distance < 5 
                ORDER BY distance";
        $db_end_points = $obj->MySQLSelect($sql);        
        #echo "<pre>";print_r($db_start_points);
        
        for($r=0;$r<count($db_start_points);$r++){
          for($b=0;$b<count($db_end_points);$b++){      
            $newarr = array();
            if($db_end_points[$b]['iRideId'] == $db_start_points[$r]['iRideId'] && $db_end_points[$b]['eReverse'] == $db_start_points[$r]['eReverse']){
              $newarr['iRideId'] = $db_end_points[$b]['iRideId'];
              $newarr['start_iRidePointId'] = $db_start_points[$r]['iRidePointId'];
              $newarr['end_iRidePointId'] = $db_end_points[$b]['iRidePointId'];
              $newarr['return'] = $db_start_points[$r]['eReverse'];
              $newarr['tRideTime'] = $db_start_points[$r]['tRideTime'];
              $newarr['dDateOut'] = $db_start_points[$r]['dDateOut'];
              $newarr['dDateRet'] = $db_start_points[$r]['dDateRet'];
              $newarr['iMemberId'] = $db_start_points[$r]['iMemberId'];
              $newarr['alertDate'] = $db_alerts[$i]['dDate'];             
              array_push($stack,$newarr);  
            }
          }  
        }
        #echo "<pre>";print_r($stack);exit;
        $mailcont = '';
        if(count($stack) > 0){
          $mailcont .= '<table width="590" style="border:6px solid #D6D6D6;border-collapse: collapse;background-color: #FFFFFF;">
                        <tr>
                          <td width="100%" colspan="2" style="background:#FFF;color:#44AA00;padding:10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">'.$vLabel['LBL_EMAIL_ALERT_RIDE'].'</td>
                        </tr>
                        <tr>
                          <td width="100%" colspan="2" style="border-bottom:6px solid #D6D6D6;background:#44AA00;color:#FFF;padding:10px;text-align:center;font-size:20px;font-family:Arial, Helvetica, sans-serif;">'.$db_alerts[$i]['vStartPoint'].' &rarr; '.$db_alerts[$i]['vEndPoint'].'</td>
                        </tr>';  
          
          $mailcont .= $ridesobj->get_alert_mail_cont($stack,$db_mem_mail[0]['vLanguageCode']);           
          $mailcont .= '</table>';
          
          $maildata = array();
          $maildata['Email'] = $db_alerts[$i]['vAlertEmail'];
          $maildata['details'] = $mailcont;
          $generalobj->send_email_user("RIDE_ALERT_EMAIL",$maildata);         
        }       
      }
    }
  }   
  echo 'rb';
  exit;
?>