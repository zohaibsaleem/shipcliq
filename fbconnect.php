<?php 
	ob_start();
	session_start();
	define( '_TEXEC', 1 );
	define('TPATH_BASE', dirname(__FILE__) );
	define( 'DS', DIRECTORY_SEPARATOR );
	require_once ( TPATH_BASE .DS.'includes'.DS.'defines.php' );
	require_once ( TPATH_BASE .DS.'includes'.DS.'configuration.php' );
	
	require TPATH_LIBRARIES.'/facebook/facebook.php';
	// Create our Application instance (replace this with your appId and secret).
	$facebook = new Facebook(array(
	'appId'  => $FACEBOOK_APPID,
	'secret' => $FACEBOOK_SECRET,
	));
	
	include_once(TPATH_LIBRARIES."/general/Imagecrop.class.php");
	$thumb = new thumbnail();
	$temp_gallery = $tconfig["tsite_temp_gallery"];
	
	include_once(TPATH_LIBRARIES."/general/SimpleImage.class.php");
	$img = new SimpleImage();           
	
	$user = $facebook->getUser();
	$ctype=$_REQUEST['ctype'];
	
	if($ctype == ''){
		$ctype = "fblogin";
	}
	
	if($ctype == "fblogin")
	{
		if ($user) 
		{
			try 
			{
				$user_profile = $facebook->api('/me?fields=id,picture,first_name,last_name,email,location,hometown,gender');
				
				$location = isset($user_profile['location']['name'])?$user_profile['location']['name']:'';
				if($location == ""){
					$location = isset($user_profile['hometown']['name'])?$user_profile['hometown']['name']:'';
				}
				$location_arr = array();
				$location_arr = explode(",",$location);
				$city = $location_arr[0];
				$country_long = trim($location_arr[1]); 
				
				if($country_long != '') {
					$sql = "SELECT vCountryCode FROM country WHERE vCountry='".$country_long."'";
					$db_counrtry_code = $obj->MySQLSelect($sql);
				}
				$country_short = isset($db_counrtry_code[0]['vCountryCode'])?$db_counrtry_code[0]['vCountryCode']:'';
				
				$db_user = array();
				
				// check user is already connected with facebook no matter what email id is
				if($user != '') {
					$sql = "SELECT iMemberId,vImage,vEmail FROM member WHERE iFBId = '".$user."'";
					$db_user = $obj->MySQLSelect($sql);
				}
				
				// check user is already connected with facebook no matter what email id is
				if(count($db_user) > 0)
				{
					
					$_SESSION['sess_iMemberId']=$db_user[0]['iMemberId'];		
					$_SESSION["sess_vFirstName"]= isset($user_profile['first_name'])?ucfirst($user_profile['first_name']):'';
					$_SESSION["sess_vLastName"]= isset($user_profile['last_name'])?ucfirst($user_profile['last_name']):'';
					$_SESSION["sess_vEmail"]= isset($db_user[0]['vEmail'])?$db_user[0]['vEmail']:'';
					$_SESSION["sess_vEmail"]= ($_SESSION["sess_vEmail"] == '' && isset($user_profile['email']))?$user_profile['email']:'';
					$_SESSION["sess_eGender"]= isset($user_profile['gender'])?ucfirst($user_profile['gender']):'';
					
					$Photo_Gallery_folder = $tconfig["tsite_upload_images_member_path"].$_SESSION['sess_iMemberId']."/";
					
					unlink($Photo_Gallery_folder.$db_user[0]['vImage']);
					unlink($Photo_Gallery_folder."1_".$db_user[0]['vImage']);
					unlink($Photo_Gallery_folder."2_".$db_user[0]['vImage']);
					unlink($Photo_Gallery_folder."3_".$db_user[0]['vImage']);   
					unlink($Photo_Gallery_folder."4_".$db_user[0]['vImage']);   
					if(!is_dir($Photo_Gallery_folder))
					{
						mkdir($Photo_Gallery_folder, 0777);
					}
					
					$baseurl =  "http://graph.facebook.com/".$user."/picture?type=large";
					$url = $user.".jpg";
					$image_name = system("wget --no-check-certificate -O ".$Photo_Gallery_folder.$url." ".$baseurl);
					
					if(is_file($Photo_Gallery_folder.$url))
					{
						include_once(TPATH_LIBRARIES."/general/SimpleImage.class.php");
						$img = new SimpleImage();           
						list($width, $height, $type, $attr)= getimagesize($Photo_Gallery_folder.$url);           
						
						if($width < $height){
							$final_width = $width;
							}else{
							$final_width = $height;
						}       
						$img->load($Photo_Gallery_folder.$url)->crop(0, 0, $final_width, $final_width)->save($Photo_Gallery_folder.$url);
						$imgname = $generalobj->img_data_upload($Photo_Gallery_folder,$url,$Photo_Gallery_folder, $tconfig["tsite_upload_images_member_size1"], $tconfig["tsite_upload_images_member_size2"], $tconfig["tsite_upload_images_member_size3"],"");                       
					}  
					@unlink($Photo_Gallery_folder.$url);
					
					$friendcount = $facebook->api('v1.0/me/friends?fields=id,picture,first_name,last_name,email,location,hometown,gender');
					$friends_count = isset($friendcount['summary']['total_count'])?$friendcount['summary']['total_count']:0; 
					
					$sql = "UPDATE member set iFBId='".$user."', vImage='".$imgname."',vFbFriendCount='".$friends_count."',eGender='".$_SESSION['sess_eGender']."' WHERE iMemberId='".$_SESSION['sess_iMemberId']."'";
					$obj->sql_query($sql); 
					
					$_SESSION["sess_vImage"]= $imgname;		
					
					$link = $tconfig["tsite_url"]."my-account";
					if(isset($_SESSION["redirect_link"]) && $_SESSION['redirect_link'] != ''){
						$link = $_SESSION["redirect_link"];
					}
					
					header("Location:".$link);
					exit;					
				}
				else // we dont have member with this iFbId
				{
					$db_user =  array();
					if (isset($user_profile['email']) && $user_profile['email'] != '') {
						$sql = "SELECT iMemberId,vImage,vEmail,iFBId FROM member WHERE vEmail = '".$user_profile['email']."'";
						$db_user = $obj->MySQLSelect($sql);
					}
					
					// user is not connected with facebook now checks email id is exists using normal registration or edit email address
					if(count($db_user) > 0)
					{
						//not linked with any other facebook account (case A(123) change email to B and user B(456) tries to login with fb)
						if($db_user[0]['iFBId'] == 0) 
						{ 	
							$_SESSION['sess_iMemberId']=$db_user[0]['iMemberId'];		
							$_SESSION["sess_vFirstName"]= isset($user_profile['first_name'])?ucfirst($user_profile['first_name']):'';
							$_SESSION["sess_vLastName"]= isset($user_profile['last_name'])?ucfirst($user_profile['last_name']):'';
							$_SESSION["sess_vEmail"]= isset($db_user[0]['vEmail'])?$db_user[0]['vEmail']:'';
							$_SESSION["sess_vEmail"]= ($_SESSION["sess_vEmail"] == '' && isset($user_profile['email']))?$user_profile['email']:'';
							$_SESSION["sess_eGender"]= isset($user_profile['gender'])?ucfirst($user_profile['gender']):'';
							
							$Photo_Gallery_folder = $tconfig["tsite_upload_images_member_path"].$_SESSION['sess_iMemberId']."/";
							
							unlink($Photo_Gallery_folder.$db_user[0]['vImage']);
							unlink($Photo_Gallery_folder."1_".$db_user[0]['vImage']);
							unlink($Photo_Gallery_folder."2_".$db_user[0]['vImage']);
							unlink($Photo_Gallery_folder."3_".$db_user[0]['vImage']);   
							unlink($Photo_Gallery_folder."4_".$db_user[0]['vImage']);   
							if(!is_dir($Photo_Gallery_folder))
							{
								mkdir($Photo_Gallery_folder, 0777);
							}
							
							$baseurl =  "http://graph.facebook.com/".$user."/picture?type=large";
							$url = $user.".jpg";
							$image_name = system("wget --no-check-certificate -O ".$Photo_Gallery_folder.$url." ".$baseurl);
							
							if(is_file($Photo_Gallery_folder.$url))
							{
								include_once(TPATH_LIBRARIES."/general/SimpleImage.class.php");
								$img = new SimpleImage();           
								list($width, $height, $type, $attr)= getimagesize($Photo_Gallery_folder.$url);           
								
								if($width < $height)
								{
									$final_width = $width;
								}
								else
								{
									$final_width = $height;
								}       
								$img->load($Photo_Gallery_folder.$url)->crop(0, 0, $final_width, $final_width)->save($Photo_Gallery_folder.$url);
								$imgname = $generalobj->img_data_upload($Photo_Gallery_folder,$url,$Photo_Gallery_folder, $tconfig["tsite_upload_images_member_size1"], $tconfig["tsite_upload_images_member_size2"], $tconfig["tsite_upload_images_member_size3"],"");                       
							}  
							@unlink($Photo_Gallery_folder.$url);
							
							$friendcount = $facebook->api('v1.0/me/friends?fields=id,picture,first_name,last_name,email,location,hometown,gender');
							$friends_count = isset($friendcount['summary']['total_count'])?$friendcount['summary']['total_count']:0; 
							
							$sql = "UPDATE member set iFBId='".$user."', vImage='".$imgname."',vFbFriendCount='".$friends_count."',eGender='".$_SESSION['sess_eGender']."', eEmailVarified = 'Yes' WHERE iMemberId='".$_SESSION['sess_iMemberId']."'";
							$obj->sql_query($sql); 
							
							$_SESSION["sess_vImage"]= $imgname;		
							
							$link = $tconfig["tsite_url"]."my-account";
							if(isset($_SESSION["redirect_link"]) && $_SESSION['redirect_link'] != ''){
								$link = $_SESSION["redirect_link"];
							}
							
							header("Location:".$link);
							exit;	
						}
						else 
						{
							// account is connectd with another facebook account
							header("Location:".$tconfig["tsite_url"].'index.php?file=c-login&invalid=1&msg_code=1');
							exit;
						}
					}
					else  // member account not found with iFbId and vEmail, so doing new member registration
					{
						$friendcount = $facebook->api('v1.0/me/friends?fields=id,picture,first_name,last_name,email,location,hometown,gender');
						$friends_count = isset($friendcount['summary']['total_count'])?$friendcount['summary']['total_count']:0; 
						
						$sql = "SELECT  `vCode` FROM  `language_master` WHERE eStatus = 'Active' AND `eDefault` = 'Yes' ";
						$default_label = $obj->MySQLSelect($sql);
						
						$lcode = (isset($default_label[0]['vCode']) && $default_label[0]['vCode'])?$default_label[0]['vCode']:'EN';
						
						if($user_profile['email'] != "") 
						{
							$sql = "INSERT INTO member (iFBId, vFBUsername, vImage, vFirstName, vLastName, vEmail, dAddedDate, eStatus, vCity, vCountry, vFbFriendCount, vLanguageCode, eGender, eEmailVarified) VALUES ('".$user."', '".$user_profile['username']."','".$user_photo['picture']['data']['url']."', '".$user_profile['first_name']."', '".$user_profile['last_name']."', '".$user_profile['email']."', '".date("Y-m-d H:i:s")."', 'Active', '".$city."','".$country_short."','".$friends_count."','".$lcode."','".$user_profile['gender']."','Yes')";
							$id =  $obj->MySQLInsert($sql);
						}
						else 
						{
							
							$sql = "INSERT INTO member (iFBId, vFBUsername, vImage, vFirstName, vLastName, vEmail, dAddedDate, eStatus, vCity, vCountry, vFbFriendCount, vLanguageCode, eGender) VALUES ('".$user."', '".$user_profile['username']."','".$user_photo['picture']['data']['url']."', '".$user_profile['first_name']."', '".$user_profile['last_name']."', '".$user_profile['email']."', '".date("Y-m-d H:i:s")."', 'Active', '".$city."','".$country_short."','".$friends_count."','".$lcode."','".$user_profile['gender']."')";
							$id =  $obj->MySQLInsert($sql);
						}
						
						$_SESSION['sess_iMemberId']=$id;		
						$_SESSION["sess_vFirstName"]=$user_profile['first_name'];
						$_SESSION["sess_vLastName"]=$user_profile['last_name'];
						$_SESSION["sess_vEmail"]=$user_profile['email'];  
						$_SESSION["sess_eGender"]=$user_profile['gender'];
						
						$Photo_Gallery_folder = $tconfig["tsite_upload_images_member_path"].$_SESSION['sess_iMemberId']."/";
						unlink($Photo_Gallery_folder.$db_user[0]['vImage']);
						unlink($Photo_Gallery_folder."1_".$db_user[0]['vImage']);
						unlink($Photo_Gallery_folder."2_".$db_user[0]['vImage']);
						unlink($Photo_Gallery_folder."3_".$db_user[0]['vImage']);   
						unlink($Photo_Gallery_folder."4_".$db_user[0]['vImage']);   
						
						if(!is_dir($Photo_Gallery_folder))
						{
							mkdir($Photo_Gallery_folder, 0777);
						}
						
						$baseurl =  "http://graph.facebook.com/".$user."/picture?type=large";
						$url = $user.".jpg";
						$image_name =  system("wget --no-check-certificate -O ".$Photo_Gallery_folder.$url." ".$baseurl);
						
						if(is_file($Photo_Gallery_folder.$url))
						{
							include_once(TPATH_LIBRARIES."/general/SimpleImage.class.php");
							$img = new SimpleImage();           
							list($width, $height, $type, $attr)= getimagesize($Photo_Gallery_folder.$url);           
							
							if($width < $height){
								$final_width = $width;
								}else{
								$final_width = $height;
							}       
							$img->load($Photo_Gallery_folder.$url)->crop(0, 0, $final_width, $final_width)->save($Photo_Gallery_folder.$url);
							$imgname = $generalobj->img_data_upload($Photo_Gallery_folder,$url,$Photo_Gallery_folder, $tconfig["tsite_upload_images_member_size1"], $tconfig["tsite_upload_images_member_size2"], $tconfig["tsite_upload_images_member_size3"],"");                       
						}  
						@unlink($Photo_Gallery_folder.$url);
						
						$sql = "UPDATE member set vImage='".$imgname."' WHERE iMemberId='".$_SESSION['sess_iMemberId']."'";
						$obj->sql_query($sql); 
						
						$_SESSION["sess_vImage"]= $imgname;
						
						$sql = "SELECT * FROM preferences WHERE eStatus = 'Active'";
						$db_preferences = $obj->MySQLSelect($sql);
						
						for($i=0;$i<count($db_preferences);$i++){
							$prefid = $db_preferences[$i]['iPreferencesId'];
							
							$Data_pref['vType'] = 'vNO';
							$Data_pref['iMemberId'] = $id;
							$Data_pref['iPreferencesId'] = $prefid;
							$id_pref = $obj->MySQLQueryPerform("member_car_preferences",$Data_pref,'insert');          
						}
						
						$Data_not['iMemberId'] = $id;
						$Data_not['eSuccessPublish'] = 'Yes';
						$Data_not['eSuccessUpdate'] = 'Yes';
						$Data_not['ePrivateMessage'] = 'Yes';
						$Data_not['eRatePassenger'] = 'Yes';
						$Data_not['eNewRating'] = 'Yes';
						$Data_not['eOtherInformation'] = 'Yes';
						$Data_not['dAddedDate'] = date("Y-m-d H:i:s");
						$id_not = $obj->MySQLQueryPerform("member_email_notification",$Data_not,'insert');
						
						header("Location:".$tconfig["tsite_url"]."my-account");
						exit;
					}
					
				}
				
				
			}
			catch (FacebookApiException $e) 
			{ 
				error_log($e);
				$user = null;
			}
			
		}
	}
	
	if($ctype == "fbphoto"){
		if ($user) {
			
			try {
				$user_profile = $facebook->api('/me?fields=id,picture');
				
				$sql = "SELECT iMemberId,vImage FROM member WHERE iFbId='".$user."'";
				$db_user = $obj->MySQLSelect($sql);
				
				$Photo_Gallery_folder = $tconfig["tsite_upload_images_member_path"].$_SESSION['sess_iMemberId']."/";
				
				@unlink($Photo_Gallery_folder.$db_user[0]['vImage']);
				@unlink($Photo_Gallery_folder."1_".$db_user[0]['vImage']);
				@unlink($Photo_Gallery_folder."2_".$db_user[0]['vImage']);
				@unlink($Photo_Gallery_folder."3_".$db_user[0]['vImage']);   
				@unlink($Photo_Gallery_folder."4_".$db_user[0]['vImage']);   
		        
				if(!is_dir($Photo_Gallery_folder))
				{
					mkdir($Photo_Gallery_folder, 0777);
				}
				
				$baseurl =  "http://graph.facebook.com/".$user."/picture?type=large";
				$url = $user.".jpg";
				$image_name =  system("wget --no-check-certificate -O ".$Photo_Gallery_folder.$url." ".$baseurl);
				
				if(is_file($Photo_Gallery_folder.$url))
				{
					include_once(TPATH_LIBRARIES."/general/SimpleImage.class.php");
					$img = new SimpleImage();           
					list($width, $height, $type, $attr)= getimagesize($Photo_Gallery_folder.$url);           
					
					if($width < $height){
						$final_width = $width;
					}
					else
					{
						$final_width = $height;
					}       
					$img->load($Photo_Gallery_folder.$url)->crop(0, 0, $final_width, $final_width)->save($Photo_Gallery_folder.$url);
					$imgname = $generalobj->img_data_upload($Photo_Gallery_folder,$url,$Photo_Gallery_folder, $tconfig["tsite_upload_images_member_size1"], $tconfig["tsite_upload_images_member_size2"], $tconfig["tsite_upload_images_member_size3"],"");                       
				}  
				@unlink($Photo_Gallery_folder.$url);
				
				$sql = "UPDATE member set vImage='".$imgname."' WHERE iMemberId='".$_SESSION['sess_iMemberId']."'";
				$obj->sql_query($sql); 
				
				$_SESSION["sess_vImage"]= $imgname;		
				
				
				header("Location:".$tconfig["tsite_url"]."profile-photo");
				exit;
				
				
				} catch (FacebookApiException $e) {
				error_log($e);
				$user = null;
			}
			
		}
	}
	
	if($ctype == "fbsocial"){
		
		if ($user) {
			
			try {
				$user_profile = $facebook->api('/me?fields=id,picture,username');
				
				$fbusername= $user_profile['username'];
				
				$sql = "UPDATE member set iFBId='".$user."',vFBUsername='".$fbusername."' WHERE iMemberId='".$_SESSION['sess_iMemberId']."'";
				$obj->sql_query($sql); 
				
				header("Location:".$tconfig["tsite_url"]."social-sharrings");
				exit;
				
				} catch (FacebookApiException $e) {
				error_log($e);
				$user = null;
			}
			
		}
		
	}
	
	if ($user) {
		$logoutUrl = $facebook->getLogoutUrl();
		$user_friends = $facebook->api('/me/friends');
	}
	else {
		$params = array(
		'scope' => 'email',
		'redirect_uri'=>$tconfig["tsite_url"].'fbconnect.php?ctype='.$ctype
		);
		$loginUrl = $facebook->getLoginUrl($params);
		header("Location:".$loginUrl);
		exit;  
	}
	
?>




