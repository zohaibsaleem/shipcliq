<?php
/**
 * Copyright 2011 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
ob_start();
session_start();
define( '_TEXEC', 1 );
define('TPATH_BASE', dirname(__FILE__) );
define( 'DS', DIRECTORY_SEPARATOR );
require_once ( TPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once ( TPATH_BASE .DS.'includes'.DS.'configuration.php' );

require TPATH_LIBRARIES.'/facebook/facebook.php';
// Create our Application instance (replace this with your appId and secret).
$facebook = new Facebook(array(
  'appId'  => '1390823504508417',
  'secret' => '46fcb9bdeab9b8c3791204a78b60ee59',
));


include_once(TPATH_LIBRARIES."/general/Imagecrop.class.php");
$thumb = new thumbnail();
$temp_gallery = $tconfig["tsite_temp_gallery"];

include_once(TPATH_LIBRARIES."/general/SimpleImage.class.php");
$img = new SimpleImage();           


// Get User ID
$user = $facebook->getUser();
// We may or may not have this data based on whether the user is logged in.
//
// If we have a $user id here, it means we know the user is logged into
// Facebook, but we don't know if the access token is valid. An access
// token is invalid if the user logged out of Facebook.

//exit;
if ($user) {

  try {
    // Proceed knowing you have a logged in user who's authenticated.
   $user_profile = $facebook->api('/me?fields=id,picture,username,first_name,last_name,email,location,hometown,gender');
  
    $location = $user_profile['location']['name'];
    if($location == ""){
        $location = $user_profile['hometown']['name'];
    }
    $location_arr = array();
    $location_arr = explode(",",$location);
    $city = $location_arr[0];
    $country_long = trim($location_arr[1]); 

    $sql = "SELECT vCountryCode FROM country WHERE vCountry='".$country_long."'";
    $db_counrtry_code = $obj->MySQLSelect($sql);
    $country_short = $db_counrtry_code[0]['vCountryCode'];

    $sql = "SELECT iMemberId,vImage FROM member WHERE vEmail='".$user_profile['email']."'";
    $db_user = $obj->MySQLSelect($sql);
  
    if(count($db_user) > 0){
		 
	     $fbusername= $user_profile['username'];
     
         $sql = "UPDATE member set iFBId='".$user."',vFBUsername='".$fbusername."' WHERE iMemberId='".$_SESSION['sess_iMemberId']."'";
         $obj->sql_query($sql); 
        
              
         header("Location:".$tconfig["tsite_url"]."social-sharrings");
         exit;

	}
  } catch (FacebookApiException $e) {
    error_log($e);
    $user = null;
  }

}

// Login or logout url will be needed depending on current user state.
if ($user) {
  $logoutUrl = $facebook->getLogoutUrl();
  $user_friends = $facebook->api('/me/friends');
  //$friends_count = count($data['data']);
} else {
    $params = array(
      'scope' => 'email,user_about_me,publish_stream'
    );
  $loginUrl = $facebook->getLoginUrl($params);
  header("Location:".$loginUrl);
  exit;  
}

?>




