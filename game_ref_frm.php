<?php
  //require_once ('defines.php');      
  function decrypt($data)
	{
		$data = base64_decode($data);
		for($i = 0, $key = 27, $c = 48; $i <= 255; $i++)
		{
			$c = 255 & ($key ^ ($c << 1));
			$table[$c] = $key;
			$key = 255 & ($key + 1);
		}
		$len = strlen($data);
		for($i = 0; $i < $len; $i++)
		{
			$data[$i] = chr($table[ord($data[$i])]);
		}
		return $data;
	}
	
	//$SQL = "SELECT iTTGameId, iUserId, eStatus FROM tt_game WHERE iTTGameId = '".decrypt($_SESSION['game_ref'])."'";
  //$db_result = $obj->MySQLSelect($SQL);       
  
 ?>
<?php if('Open' == 'Open'){ ?>
<style>
.Button, input[type="submit"] {
	background-color: #DE3926;
	background-image: -moz-linear-gradient(center top, #DE3926 0%, #C11A1A 100%);
	border: 2px solid #D32D1D;
	border-radius: 21px 21px 21px 21px;
	color: #FFFFFF;
	cursor: pointer;
	font-size: 20px;
	height: 42px;
	line-height: 34px;
	margin: 0;
	outline: 0 none;
	padding: 0 20px;
	text-decoration: none;
	text-shadow: 1px 1px 1px #333333;
}
.Button:hover, input[type="submit"]:hover {
	background-image: -moz-linear-gradient(center top, #C11A1A 0%, #DE3926 100%);
}
a.Button {
	display: inline-block;
}
a.Button:hover {
	color: #FFFFFF;
}
/*...............................new-css...........................*/
.game-frame1 {
	border: 3px dashed #AAAAAA;
	margin: 0 auto;
	padding: 15px;
	width: 450px;
}
.game-frame1 span {
	color: #000000;
font-family: font-family: Helvetica, Arial, san-serif;
	font-size: 18px;
	font-weight: normal;
	line-height: 23px;
	margin-bottom: 12px;
}
.game-from {
	margin: 15px 0 0;
	padding: 0px;
	float: left;
	width: 100%;
}
.game-from label {
	margin: 0 0 10px;
	padding: 0px;
	float: left;
	width: 100%;
}
.game-from label em {
	margin: 0px;
	padding: 3px 0 0;
	float: left;
	width: 150px;
	font-style: normal;
}
.game-input {
	background: none repeat scroll 0 0 #FFFFFF;
	border: 1px solid #CCCCCC;
	box-shadow: 0 0 5px #CCCCCC inset;
	height: 30px;
	margin: 2px 0;
	outline: 0 none;
	padding: 0 10px;
	width: 250px;
}
 @media screen and (min-width:1px) and (max-width:500px) {
.game-frame1 {
	width: 92%;
}
}
</style>
<script src="http://dev.foodoozle.com/jquery-1.6.2.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="http://dev.foodoozle.com/validationEngine.jquery.css" type="text/css"/>
<script src="http://dev.foodoozle.com/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="http://dev.foodoozle.com/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<span style="color: #000000;font-family: Bree Serif,serif;font-size: 23px;font-weight: normal;line-height: 23px;margin-bottom: 12px;">Welcome, to the Trick or Treat Game.</span> <br>
<br>
<div class="game-frame1">
  <form name="frmgame" id="frmgame" action="" method="post">
    <input type="hidden" name="iTTGameId" id="iTTGameId" value="<?php echo decrypt($_SESSION['game_ref']); ?>">
    <span>Please submit  your details below and  you could win a TREAT from Foodoozle.com</span>
    <div class="game-from">
      <label><em>Name</em>
        <input type ="text" name="vName" id="vName" value=""  class="validate[required] game-input" >
      </label>
      <label><em>Email</em>
        <input type ="text" name="vEmail" id="vEmail" value="" class="validate[required,custom[email]] game-input" >
      </label>
      <label><em>Zip</em>
        <input type ="text" name="vZip" id="vZip" value="" class="validate[required] game-input" >
      </label>
      <label><em>Ref. From</em>
        <input type ="text" name="vRefFrom" id="vRefFrom" value="<?php echo $_SESSION['from']; ?>" class="validate[required] game-input" readonly>
      </label>
      <!--
     <label><em>&nbsp;</em>
     <input type="checkbox" name="newsletter" id="newsletter" value=""/>&nbsp;
     <label>Send Newsletter</label>
    -->
      <label>
      <p>By entering for a chance to win game, you are opting-in to our newsletter (deals & news).  But we respect your privacy, so you can opt out at any time.</p>
      </label>
      <label>
        <input type="button" value="Submit" name="submit" onClick="checkvalid();" class="Button">
      </label>
    </div>
  </form>
  <div style="clear:both;"></div>
</div>
<div id="exist" style="display:none;color: #000000;font-family: font-family: Helvetica,Arial,san-serif;font-size: 20px;font-weight: normal;line-height: 23px;margin-bottom: 12px;"></div>
<div id="error" style="display:none;color: #000000;font-family: font-family: Helvetica,Arial,san-serif;font-size: 20px;font-weight: normal;line-height: 23px;margin-bottom: 12px;"></div>
<div id="success" style="display:none;color: #000000;font-family: font-family: Helvetica,Arial,san-serif;font-size: 20px;font-weight: normal;line-height: 23px;margin-bottom: 12px;"></div>
<script> 
    var	site_url = 'http://dev.foodoozle.com/';
  	function checkvalid(){        
  		resp = jQuery("#frmgame").validationEngine('validate');  		
  		if(resp == true){
			  var vName = $("#vName").val();
			  var vEmail = $("#vEmail").val();
			  var vZip = $("#vZip").val();
			  var vRefFrom = $("#vRefFrom").val();
			  var iTTGameId = $("#iTTGameId").val();
			  
			  /*if(document.getElementById("newsletter").checked == true){
          var newsletter = 'Yes';
        }else{
          var newsletter = 'No';
        }*/
        
      	var request = $.ajax({  
      	  type: "POST",
      	  url: site_url+'game_a.php',  
      	  data: "vName="+vName+"&vEmail="+vEmail+"&vZip="+vZip+"&vRefFrom="+vRefFrom+"&iTTGameId="+iTTGameId+"&newsletter=Yes", 	  
      	        
      	  success: function(data) { //alert(data); return false;
      		  if(data == 'EXIST'){
              $("#exist").html('You have already participate for this game.'); 
              $("#exist").show(); 
              $("#frmgame").hide();
            }else if(data == 'ERROR'){ 
              $("#error").html('Error in participate. Please try again.');  
              $("#error").show();
              $("#frmgame").hide();
            }else if(data == 'INVITEE'){ 
              $("#error").html('You can not participate in this game because you are host of it.');  
              $("#error").show();
              $("#frmgame").hide();
            }else{
              $("#success").html("Congratulations!  We will let you know if you've been 'Tricked' or 'Teated' when the game ends.<br><br>But if you're hungry no...Just go to our homepage, enter your zip code, browse restaurant menus and place a delivery or takeout order.  We LOVE food...and would love to bring it to you too!");  
              $("#success").show();
              $("#frmgame").hide();
            }                      			
      		}
      	});
      	
      	request.fail(function(jqXHR, textStatus) {
      	  alert( "Request failed: " + textStatus ); 
      	});
  		}else{
  			return false;
  		}		
  	}  
  </script>
<?php }else{ ?>
<span style="color: #000000;font-family: Bree Serif,serif;font-size: 23px;font-weight: normal;line-height: 23px;margin-bottom: 12px;">Sorry! the game is over.</span>
<?php } ?>
