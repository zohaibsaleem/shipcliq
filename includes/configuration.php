<? 
if($_SERVER["HTTP_HOST"] == "localhost")
{
	$tconfig["tsite_folder"] = "/shipcliq/";
}	
else
{
	$tconfig["tsite_folder"] = "/shipcliq/";
}
$tconfig["tsite_folder"] = "/";
$tconfig["tsite_admin"] = "bbcsadmin";
$tconfig["tsite_url"] = "http://".$_SERVER["HTTP_HOST"].$tconfig["tsite_folder"];


$tconfig["tpanel_url"] = "http://".$_SERVER["HTTP_HOST"].$tconfig["tsite_folder"].$tconfig["tsite_admin"];
$tconfig["tpanel_path"] = $_SERVER["DOCUMENT_ROOT"]."".$tconfig["tsite_folder"];
$tconfig["tadmin_path"] = $_SERVER["DOCUMENT_ROOT"]."".$tconfig["tsite_folder"]."/".$tconfig["tsite_admin"];

$tconfig["tsite_libraries"] = $tconfig["tsite_url"]."libraries/";
$tconfig["tsite_plugin"] = $tconfig["tpanel_path"]."plugins/FCKeditor/";
$tconfig["tsite_plugins"] = $tconfig["tpanel_path"]."plugins/ofc/"; 
$tconfig["tsite_plugins_ofc"] = $tconfig["tsite_url"]."plugins/ofc/";
$tconfig["tsite_stylesheets"] = $tconfig["tsite_url"]."public_html/stylesheets/";
$tconfig["tsite_images"] = $tconfig["tsite_url"]."public_html/images/";
$tconfig["tsite_javascript"] = $tconfig["tsite_url"]."public_html/javascripts/";
$tconfig["tpanel_theme"] = $tconfig["tsite_stylesheets"]."cp/theme/blue/";
$tconfig["tpanel_theme_img"] = $tconfig["tsite_stylesheets"]."cp/theme/blue/img/";
$tconfig["tpanel_img"] = $tconfig["tsite_images"]."cp/";
$tconfig["tcp_javascript"] = $tconfig["tsite_url"]."public_html/cp-javascripts/";

$tconfig["tsite_upload_images"] = $tconfig["tsite_url"]."public_html/uploads";
$tconfig["tsite_temp_gallery"] = $tconfig["tsite_url"]."public_html/tmp_imagemagic/";
$tconfig["tsite_temp_gallery"] = $tconfig["tpanel_path"]."/public_html/uploads/tmp_imagemagic";  
$temp_gallery = $tconfig["tsite_temp_gallery"];                         

/* Language Labels */
$tconfig["tsite_label_path"] = $tconfig["tpanel_path"]."/language/";

/* Banner */
$tconfig["tsite_upload_images_banner"] = $tconfig["tsite_url"]."public_html/uploads/banner/";
$tconfig["tsite_upload_images_banner_path"] = $tconfig["tpanel_path"]."/public_html/uploads/banner/";   

/*member Photos*/
$tconfig["tsite_upload_images_member"] = $tconfig["tsite_url"]."/public_html/uploads/member/";
$tconfig["tsite_upload_images_member_path"] = $tconfig["tpanel_path"]."/public_html/uploads/member/";
$tconfig["tsite_upload_images_member_size1"] = "64";
$tconfig["tsite_upload_images_member_size2"] = "130";
//$tconfig["tsite_upload_images_member_size2"] = "150";
$tconfig["tsite_upload_images_member_size3"] = "256";
$tconfig["tsite_upload_images_member_size4"] = "512"; 

/* Story */
$tconfig["tsite_upload_images_memberstory"] = $tconfig["tsite_url"]."/public_html/uploads/member_stories/";
$tconfig["tsite_upload_images_memberstory_path"] = $tconfig["tpanel_path"]."/public_html/uploads/member_stories/";
$tconfig["tsite_upload_images_memberstory_size1"] = "277";
$tconfig["tsite_upload_images_memberstory_size2"] = "400";
$tconfig["tsite_upload_images_memberstory_size3"] = "600"; 

/* Preferences */
$tconfig["tsite_upload_images_preferences"] = $tconfig["tsite_url"]."/public_html/uploads/preferences/";
$tconfig["tsite_upload_images_preferences_path"] = $tconfig["tpanel_path"]."/public_html/uploads/preferences/";  

/* Logo */
$tconfig["tsite_upload_site_logo"] = $tconfig["tsite_url"]."/public_html/uploads/logo/";
$tconfig["tsite_upload_site_logo_path"] = $tconfig["tpanel_path"]."/public_html/uploads/logo/";
$tconfig["tsite_upload_images_site_logo_size1"] = "213";
$tconfig["tsite_upload_images_site_logo_size_bg"] = "1600";

/* Footer Logo */
$tconfig["tsite_upload_footer_logo"] = $tconfig["tsite_url"]."/public_html/uploads/logo/";
$tconfig["tsite_upload_footer_logo_path"] = $tconfig["tpanel_path"]."/public_html/uploads/logo/";
$tconfig["tsite_upload_images_footer_logo_size2"] = "213";


/* Admin Logo */
$tconfig["tsite_upload_admin_logo"] = $tconfig["tsite_url"]."/public_html/uploads/logo/";
$tconfig["tsite_upload_admin_path"] = $tconfig["tpanel_path"]."/public_html/uploads/logo/";
$tconfig["tsite_upload_images_admin_size3"] = "269";

/* Emial Logo */
$tconfig["tsite_upload_email_logo"] = $tconfig["tsite_url"]."/public_html/uploads/logo/";
$tconfig["tsite_upload_email_logo_path"] = $tconfig["tpanel_path"]."/public_html/uploads/logo/";
$tconfig["tsite_upload_images_email_logo_size1"] = "213";

/* Favicon */
$tconfig["tsite_upload_site_favicon"] = $tconfig["tsite_url"];
$tconfig["tsite_upload_site_favicon_path"] = $tconfig["tpanel_path"];
$tconfig["tsite_upload_images_site_favicon_size4"] = "70";

$tconfig["tsite_upload_images_media_partners"] = $tconfig["tsite_url"]."/public_html/uploads/media_partners/";
$tconfig["tsite_upload_media_partners_path"] = $tconfig["tpanel_path"]."/public_html/uploads/media_partners/";
$tconfig["tsite_upload_media_partners_size1"] = "100";
$tconfig["tsite_upload_media_partners_size2"] = "200";
$tconfig["tsite_upload_media_partners_size3"] = "300"; 


$tconfig["tsite_upload_images_tmpphotos"] = $tconfig["tsite_url"]."public_html/uploads/tmp_photos/";
$tconfig["tsite_upload_images_tmpphotos_path"] = $tconfig["tpanel_path"]."/public_html/uploads/tmp_photos/";

#echo $SELECT_THEME ;exit;
$SELECT_THEME = explode("-",'Theme29-orange-home4');
$THEME = $SELECT_THEME[0];
$THEMECSS = $SELECT_THEME[1];
$THEMEHOME = $SELECT_THEME[2];
	##################### Emial Logo Display Start ########################
	$sql = "SELECT vValue FROM configurations WHERE vName = 'EMAIL_LOGO'";
	$db_logo = $obj->MySQLSelect($sql);
	$Emaillogo = '1_'.$db_logo[0]['vValue'];
	//print_r($Faviconlogo); exit;
	if($Emaillogo == "")
	{
		$Emaillogodis = $tconfig['tsite_images']."/logo.png";
	}
	else if($Emaillogo != "")
	{
		$Emaillogodis = $tconfig['tsite_upload_email_logo'].$Emaillogo;
	}
	##################### Emial Logo Display End #########################
	
?> 
