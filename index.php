<?php

	if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip'))
	ob_start("ob_gzhandler");
	else
	ob_start();
	session_start();
	header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
	define( '_TEXEC', 1 );
	define('TPATH_BASE', dirname(__FILE__) );
	define( 'DS', DIRECTORY_SEPARATOR );
	//header('Content-Type: text/html; charset=iso-8859-1');  
	require_once ( TPATH_BASE .DS.'includes'.DS.'defines.php' ); 
	require_once ( TPATH_BASE .DS.'includes'.DS.'configuration.php' );
	require_once ( TPATH_LIBRARIES .DS.'general'.DS.'license'.DS.'license.php' );
	##################### Favicon Logo Display Start ########################
	$sql = "SELECT vValue FROM configurations WHERE vName = 'FAVICON_ICON'";
	$db_logo = $obj->MySQLSelect($sql);
	$Faviconlogo = $db_logo[0]['vValue'];
	//print_r($Faviconlogo); exit;
	if($Faviconlogo == "")
	{
		$Faviconlogodis = $tconfig['tsite_images']."/logo.png";
	}
	else if($Faviconlogo != "")
	{
		$Faviconlogodis = $tconfig['tsite_upload_site_favicon'].$Faviconlogo;
	}
	##################### Favicon Logo Display End #########################
	if($script == "home"){
		$script = $THEMEHOME;
		$homepage = "Yes";
		}else{
		$homepage = "No";
	}  
	
	$sql = "SELECT vTitle, vCode, vCurrencyCode, eDefault FROM language_master WHERE eStatus = 'Active' AND eFrontStatus = 'Yes' ORDER BY iDispOrder ASC";
	$db_lng_mst = $obj->MySQLSelect($sql);
	
	$sql = "SELECT vName, vSymbole, eDefault FROM currency WHERE 1=1 AND eFrontStatus = 'Yes' ORDER BY iDispOrder ASC";
	$db_curr_mst = $obj->MySQLSelect($sql);
	
	for($i=0;$i<count($db_lng_mst);$i++){
		if($db_lng_mst[$i]['eDefault'] == 'Yes'){
			$lang_master_def_lang = $db_lng_mst[$i]['vCode'];
			define("LANG_MASTER_DEF_LANG", $db_lng_mst[$i]['vCode']);
			break;
		}
	}
	
	for($i=0;$i<count($db_curr_mst);$i++){
		if($db_curr_mst[$i]['eDefault'] == 'Yes'){
			$curr_master_def_curr = $db_curr_mst[$i]['vName'];
			define("CURR_MASTER_DEF_CURR", $db_curr_mst[$i]['vName']);
			break;
		}
	}
	
	define("db_lng_mst", serialize ($db_lng_mst));
	define("db_curr_mst", serialize ($db_curr_mst));
	
	$smarty->assign('db_lng_mst', $db_lng_mst);
	$smarty->assign('db_curr_mst', $db_curr_mst);
	
	#default language & currency
	if($_SESSION['sess_lang'] == '' || $_SESSION['sess_lang'] == 'LANG_MASTER_DEF_LANG'){
		$ridesobj->set_default_language_currency(); 
	}
	
	#change language
	$lang = $_REQUEST['lang'];
	if(isset($lang)){
		$ridesobj->set_user_selected_language($lang);
	}
	
	#change currency
	$currency = $_REQUEST['currency'];
	if(isset($currency)){
		$ridesobj->set_user_selected_currncy($currency);
	}
	
	if($_SESSION['sess_lang'] == $_SESSION['lang_master_def_lang']){
		$_SESSION['sess_lang_pref'] = "_".$_SESSION['lang_master_def_lang']; 
		$_SESSION['sess_lbl_pref'] = $_SESSION['lang_master_def_lang']."_"; 
		$_SESSION['sess_lang_master'] = "";
		$_SESSION['LANG'] = $_SESSION['lang_master_def_lang'];
		}else{
		$_SESSION['sess_lang_pref'] = "_".$_SESSION['sess_lang']; 
		$_SESSION['sess_lbl_pref'] = $_SESSION['sess_lang']."_"; 
		$_SESSION['sess_lang_master'] = "_".$_SESSION['sess_lang'];
		$_SESSION['LANG'] = $_SESSION['sess_lang'];
	}
	
	if(!isset($_SESSION['ismobile'])){
		#Mobile Detect
		include_once("Mobile_Detect.php");  
		$detect = new Mobile_Detect; 
		if ($detect->isMobile()) {
			$ismobile = "Yes";
			$browser = 'mob';
			$_SESSION['ismobile'] = $ismobile;
		}
		else if($detect->isTablet()){
			$ismobile = "Yes";
			$browser = 'mob';
			$_SESSION['ismobile'] = $ismobile;
		}
		else{
			$ismobile =  "No";
			$browser = 'simple';
			$_SESSION['ismobile'] = $ismobile;
		}
	}
	
	
	#Include language lable file#
	include_once($tconfig["tsite_label_path"].$_SESSION['sess_lang']."/".$_SESSION['sess_lang'].".php");
	
	include_once(TPATH_MODULES.DS."content".DS."common_sections.php");
	
	if($mode == 'view'){
		$include_script = TPATH_MODULES.DS.$module.DS.$mode."-".$script.".php";
		$include_template = TPATH_TEMPLATES.DS.$module.DS.$mode."-".$script.".tpl";    
		}else if($mode =='add' || $mode =='edit'){
		$include_script = TPATH_MODULES.DS.$module.DS.$script.".php";
		$include_template = TPATH_TEMPLATES.DS.$module.DS.$script.".tpl";
		}else if($mode =='au'){
		$include_script = TPATH_MODULES.DS.$module.DS.$mode."-".$script.".php";
		$include_template = TPATH_TEMPLATES.DS.$module.DS.$mode."-".$script.".tpl";
		}else{
		$include_script = TPATH_MODULES.DS.$module.DS.$script.".php";
	}
	
	//echo "<br>".$include_script."<br>".$include_template;
	
	//$COPY_RIGHT_TEXT = str_replace("#SITE_URL#",'<a href="'.$generalobj->home_base_url().'">'.$SITE_NAME.'</a>',$COPY_RIGHT);
	$COPY_RIGHT_TEXT = str_replace("#SITE_URL#",'<a href="'.$tconfig['tsite_url'].'">'.$SITE_NAME.'</a>',$COPY_RIGHT);
	$smarty->assign("COPY_RIGHT_TEXT",$COPY_RIGHT_TEXT);
	
	require_once(TPATH_CLASS_GEN."Breadcrumb.class.php");
	$breadcrumtrail = new breadcrumb($smarty,$SITE_TITLE);
	$breadcrumtrail->add('Home',$tconfig['tsite_url']);
	require_once($include_script);
	
	/* breadcrumtrail setting */
	$breadcrumstring = $breadcrumtrail->trail(' &gt;&gt; ');
	$smarty->assign("breadcrumstring",$breadcrumstring);
	
	$sql = "SELECT * FROM configurations WHERE eStatus='Active'";
	$db_config = $obj->MySQLSelect($sql);
	
	for($i=0;$i<count($db_config);$i++)
	{
		$smarty->assign($db_config[$i]["vName"],$db_config[$i]['vValue']);
	}
	
	$var_msg = $_REQUEST['var_msg']; 
	
	if(!isset($_SESSION['sess_browser'])){
		$browser = $ridesobj->getBrowser();
		$_SESSION['sess_browser'] = $browser['name'];
	} 
	
	
	
	#echo "<pre>";print_r($_SERVER);exit;
	$smarty->assign("Faviconlogodis",$Faviconlogodis);
	$smarty->assign("Emaillogodis",$Emaillogodis);
	$smarty->assign('ismobile',$_SESSION['ismobile']);
	$smarty->assign('sess_browser',$_SESSION['sess_browser']);  
	$smarty->assign('tplfile',$_REQUEST['file']);
	$smarty->assign('include_template',$include_template);
	$smarty->assign('TPATH_TEMPLATES',TPATH_TEMPLATES);
	$smarty->assign('DS',DS);
	$smarty->assign('generalobj',$generalobj);
	$smarty->assign('admin_images_url',$admin_images_url);
	$smarty->assign('admin_url',$admin_url);
	$smarty->assign('plugins_url',$plugins_url);
	$smarty->assign('script',$script);
	$smarty->assign('sess_iMemberId',$_SESSION["sess_iMemberId"]); 
	$smarty->assign('sess_vFirstName',$_SESSION["sess_vFirstName"]);
	$smarty->assign('sess_vLastName',$_SESSION["sess_vLastName"]);
	$smarty->assign('sess_iOrganizationId',$_SESSION["sess_iOrganizationId"]); 
	$smarty->assign('sess_vOrganizationName',$_SESSION["sess_vOrganizationName"]); 
	$smarty->assign('var_msg',$var_msg);     
	$smarty->assign('script',$script);
	$smarty->assign('homepage',$homepage); 
	$smarty->assign('THEME',$THEME);
	$smarty->assign('THEMECSS',$THEMECSS);
	$smarty->assign('THEMEHOME',$THEMEHOME);   
	$smarty->assign('sess_lang',$_SESSION['sess_lang']);  
	$smarty->assign('sess_price_ratio',$_SESSION['sess_price_ratio']);   
	$smarty->debug_smarty('layout.tpl');  
	$smarty->force_compile = true;
	$smarty->compile_check = true;
	$smarty->clearAllCache();
	 // $smarty->compileAllTemplates('.tpl',true);

?>
