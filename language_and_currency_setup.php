<?php
  ob_start();      
  session_start();    
  define( '_TEXEC', 1 );
  define('TPATH_BASE', dirname(__FILE__) );
  define( 'DS', DIRECTORY_SEPARATOR );    
  require_once ( TPATH_BASE .DS.'includes'.DS.'defines.php' ); 
  require_once ( TPATH_BASE .DS.'includes'.DS.'configuration.php' ); 
  
  if($_REQUEST['action'] == 'setup'){  
    $REPLACE_FROM = $_REQUEST['replace_from'];
    $REPLACE_TO = $_REQUEST['replace_to'];
  
    $languages = $_REQUEST['language'];
  
    $sql = "DROP TABLE IF EXISTS pages";
    $db_sql = $obj->sql_query($sql);
  
    $sql = "CREATE TABLE pages(iPageId INT(5) NOT NULL AUTO_INCREMENT,
            vPageName VARCHAR(500),               
            iFrameCode TEXT,
            vImage VARCHAR(500),
            vTitle VARCHAR(500),
            tMetaKeyword TEXT,
            tMetaDescription TEXT,
            PRIMARY KEY ( iPageId ))";  
    $db_sql = $obj->sql_query($sql);
    
    $sql = "DROP TABLE IF EXISTS email_templates";
    $db_sql = $obj->sql_query($sql);
    
    $sql = "CREATE TABLE email_templates(iEmailId INT(5) NOT NULL AUTO_INCREMENT,
            vEmail_Code VARCHAR(500),
            eMIME enum('html', 'text'),
            vSection enum('Job Seeker','Artist','Franchisee','Employer'),
            PRIMARY KEY ( iEmailId ))";  
    $db_sql = $obj->sql_query($sql);
    
    $sql = "DROP TABLE IF EXISTS car_colour";
    $db_sql = $obj->sql_query($sql);
    
    $sql = "CREATE TABLE car_colour (iColourId INT(5) NOT NULL AUTO_INCREMENT,
            eStatus enum('Active', 'Inactive') DEFAULT 'Active',
            PRIMARY KEY ( iColourId ))"; 
    $db_sql = $obj->sql_query($sql);
    
    $sql = "DROP TABLE IF EXISTS car_type";
    $db_sql = $obj->sql_query($sql);
    
    $sql = "CREATE TABLE car_type (iCarTypeId INT(5) NOT NULL AUTO_INCREMENT,
            eStatus enum('Active', 'Inactive') DEFAULT 'Active',
            PRIMARY KEY ( iCarTypeId ))"; 
    $db_sql = $obj->sql_query($sql);
    
    $sql = "DROP TABLE IF EXISTS faq_categories";
    $db_sql = $obj->sql_query($sql);
    
    $sql = "CREATE TABLE faq_categories (iFaqcategoryId INT(5) NOT NULL AUTO_INCREMENT,
            eStatus enum('Active', 'Inactive') DEFAULT 'Active',
            iDisplayOrder INT(5) NOT NULL, 
            PRIMARY KEY ( iFaqcategoryId ))"; 
    $db_sql = $obj->sql_query($sql);
     
    $sql = "DROP TABLE IF EXISTS faqs";
    $db_sql = $obj->sql_query($sql); 
   
    $sql = "CREATE TABLE faqs (iFaqId INT(5) NOT NULL AUTO_INCREMENT,
            iFaqcategoryId INT(5) NOT NULL,
            eStatus enum('Active', 'Inactive') DEFAULT 'Active',
            iDisplayOrder INT(5) NOT NULL, 
            PRIMARY KEY ( iFaqId ))"; 
    $db_sql = $obj->sql_query($sql);
    
    $sql = "TRUNCATE TABLE language_master;";
    $db_sql = $obj->sql_query($sql);
    
    $sql = "TRUNCATE TABLE language_label;";
    $db_sql = $obj->sql_query($sql);
    
    $sql = "TRUNCATE TABLE currency;";
    $db_sql = $obj->sql_query($sql);
   
    for($i=0;$i<count($languages);$i++){
      $lcd = $languages[$i];
      $sql = "SELECT * FROM language_setup WHERE vCode = '".$lcd."'";
      $db_lang_ope = $obj->MySQLSelect($sql);
      
      $Data['vTitle'] = $db_lang_ope[0]['vTitle'];
      $Data['vCode'] = $lcd;
      $Data['vCurrencyCode'] = $_REQUEST['language_currency'.$lcd];
      $Data['iDispOrder'] = $_REQUEST['display_order'.$lcd];
      if($_REQUEST['default_language'] == $lcd){
        $Data['eDefault'] = 'Yes';
      }else{
        $Data['eDefault'] = 'No';
      }               
      $language_id = $obj->MySQLQueryPerform("language_master",$Data,'insert');
      
      $sql = "SELECT vLabel, $lcd as vValue FROM language_labels_setup WHERE 1=1";
      $db_lang_lbl = $obj->MySQLSelect($sql);
      
      for($j=0;$j<count($db_lang_lbl);$j++){
        $Data_lbl['vCode'] = $lcd;
        $Data_lbl['vLabel'] = $db_lang_lbl[$j]['vLabel'];
        $Data_lbl['vValue'] = $db_lang_lbl[$j]['vValue'];
        $label_id = $obj->MySQLQueryPerform("language_label",$Data_lbl,'insert');
      }
      
      $lcd = $languages[$i];
      $sql = "ALTER TABLE pages ADD vPageTitle_$lcd VARCHAR(500)";
      $db_sql = $obj->sql_query($sql);
      
      $sql = "ALTER TABLE pages ADD tPageDesc_$lcd TEXT";
      $db_sql = $obj->sql_query($sql);
      
      $sql = "ALTER TABLE email_templates ADD vSubject_$lcd VARCHAR(500)";
      $db_sql = $obj->sql_query($sql);
      
      $sql = "ALTER TABLE email_templates ADD vBody_$lcd TEXT";
      $db_sql = $obj->sql_query($sql);
      
      $sql = "ALTER TABLE car_colour ADD vColour_$lcd VARCHAR(100)";
      $db_sql = $obj->sql_query($sql);
      
      $sql = "ALTER TABLE car_type ADD vTitle_$lcd VARCHAR(100)";
      $db_sql = $obj->sql_query($sql);
      
      $sql = "ALTER TABLE faq_categories ADD vTitle_$lcd VARCHAR(100)";
      $db_sql = $obj->sql_query($sql);
      
      $sql = "ALTER TABLE faqs ADD vTitle_$lcd TEXT";
      $db_sql = $obj->sql_query($sql);   
      
      $sql = "ALTER TABLE faqs ADD tAnswer_$lcd TEXT";
      $db_sql = $obj->sql_query($sql);
    }
    
    $sql = "SELECT * FROM pages_setup WHERE 1=1";
    $db_pages = $obj->MySQLSelect($sql); 
    
    for($i=0;$i<count($db_pages);$i++){
      $Data_page['vPageName'] = str_replace($REPLACE_FROM, $REPLACE_TO, $db_pages[$i]['vPageName']);
      $Data_page['iFrameCode'] = $db_pages[$i]['iFrameCode'];
      $Data_page['vImage'] = $db_pages[$i]['vImage'];
      $Data_page['vTitle'] = str_replace($REPLACE_FROM, $REPLACE_TO, $db_pages[$i]['vTitle']);
      $Data_page['tMetaKeyword'] = str_replace($REPLACE_FROM, $REPLACE_TO, $db_pages[$i]['tMetaKeyword']);
      $Data_page['tMetaDescription'] = str_replace($REPLACE_FROM, $REPLACE_TO, $db_pages[$i]['tMetaDescription']);          
      for($j=0;$j<count($languages);$j++){
        $lcd = $languages[$j];
        $Data_page['vPageTitle_'.$lcd] = str_replace($REPLACE_FROM, $REPLACE_TO, $db_pages[$i]['vPageTitle_'.$lcd]);
        $Data_page['tPageDesc_'.$lcd] = str_replace($REPLACE_FROM, $REPLACE_TO, $db_pages[$i]['vPageTitle_'.$lcd]);         
      }         
      $language_id = $obj->MySQLQueryPerform("pages",$Data_page,'insert'); 
    }
    
    $sql = "SELECT * FROM email_templates_setup WHERE 1=1";
    $db_emails = $obj->MySQLSelect($sql);
    
    for($i=0;$i<count($db_emails);$i++){
      $Data_emails['vEmail_Code'] = $db_emails[$i]['vEmail_Code'];
      $Data_emails['eMIME'] = str_replace($REPLACE_FROM, $REPLACE_TO, $db_emails[$i]['eMIME']);
      $Data_emails['vSection'] = $db_emails[$i]['vSection'];              
      for($j=0;$j<count($languages);$j++){
        $lcd = $languages[$j];
        $Data_emails['vBody_'.$lcd] = str_replace($REPLACE_FROM, $REPLACE_TO, $db_emails[$i]['vBody_'.$lcd]);
        $Data_emails['vSubject_'.$lcd] = str_replace($REPLACE_FROM, $REPLACE_TO, $db_emails[$i]['vSubject_'.$lcd]);         
      }         
      $email_id = $obj->MySQLQueryPerform("email_templates",$Data_emails,'insert'); 
    }
    
    $sql = "SELECT * FROM car_colour_setup WHERE 1=1";
    $db_car_color = $obj->MySQLSelect($sql);
    
    for($i=0;$i<count($db_car_color);$i++){
      $Data_color['eStatus'] = 'Active';
      for($j=0;$j<count($languages);$j++){
        $lcd = $languages[$j];
        $Data_color['vColour_'.$lcd] = $db_car_color[$i]['vColour_'.$lcd];         
      }
      $color_id = $obj->MySQLQueryPerform("car_colour",$Data_color,'insert');          
    }
    
    $sql = "SELECT * FROM car_type_setup WHERE 1=1";
    $db_car_type = $obj->MySQLSelect($sql);
    
    for($i=0;$i<count($db_car_type);$i++){
      $Data_type['eStatus'] = 'Active';
      for($j=0;$j<count($languages);$j++){
        $lcd = $languages[$j];
        $Data_type['vTitle_'.$lcd] = $db_car_type[$i]['vTitle_'.$lcd];         
      }
      $type_id = $obj->MySQLQueryPerform("car_type",$Data_type,'insert');          
    }
    
    $sql = "SELECT * FROM faq_categories_setup WHERE 1=1";
    $db_faq_cat = $obj->MySQLSelect($sql);
    
    for($i=0;$i<count($db_faq_cat);$i++){
      $Data_fcat['eStatus'] = 'Active';
      $Data_fcat['iDisplayOrder'] = $db_faq_cat[$i]['iDisplayOrder'];
      for($j=0;$j<count($languages);$j++){
        $lcd = $languages[$j];
        $Data_fcat['vTitle_'.$lcd] = str_replace($REPLACE_FROM, $REPLACE_TO, $db_faq_cat[$i]['vTitle_'.$lcd]);         
      }
      $fcat_id = $obj->MySQLQueryPerform("faq_categories",$Data_fcat,'insert');
      
      $sql = "SELECT * FROM faqs_setup WHERE 1=1 AND iFaqcategoryId = '".$db_faq_cat[$i]['iFaqcategoryId']."'";
      $db_faq = $obj->MySQLSelect($sql);       
      
      $Data_faq['iFaqcategoryId'] = $fcat_id;
      $Data_faq['eStatus'] = 'Active';
      $Data_faq['iDisplayOrder'] = $db_faq[$i]['iDisplayOrder'];
      for($j=0;$j<count($languages);$j++){
        $lcd = $languages[$j];
        $Data_faq['vTitle_'.$lcd] = str_replace($REPLACE_FROM, $REPLACE_TO, $db_faq[$i]['vTitle_'.$lcd]); 
        $Data_faq['tAnswer_'.$lcd] = str_replace($REPLACE_FROM, $REPLACE_TO, $db_faq[$i]['tAnswer_'.$lcd]);        
      }
      $faq_id = $obj->MySQLQueryPerform("faqs",$Data_faq,'insert');          
    }      
    
    $currency = $_REQUEST['currency'];
    
    for($i=0;$i<count($currency);$i++){
      $ckr = $_REQUEST['currency'][$i];
     
      $Data_cur['vName'] = $ckr;
      $Data_cur['vSymbole'] = $_REQUEST['currency_symbol'.$ckr];
      $Data_cur['Ratio'] = $_REQUEST['currency_ration'.$ckr];
      $Data_cur['iDispOrder'] = $_REQUEST['currency_display_order'.$ckr];
      if($_REQUEST['default_currency'] == $ckr){
        $Data_cur['eDefault'] = 'Yes';
      }else{
        $Data_cur['eDefault'] = 'No';
      }
      $currency_id = $obj->MySQLQueryPerform("currency",$Data_cur,'insert');
      
      //$sql = "ALTER TABLE rides_new DROP fRatio_$ckr";
      //$db_sql = $obj->sql_query($sql);
      
      $sql = "ALTER TABLE rides_new ADD fRatio_$ckr FLOAT(10,2)";
      $db_sql = $obj->sql_query($sql);
      
      //$sql = "DELETE FROM configurations WHERE vName = 'HUNDREDMILES_PRICE_".$ckr."'";
      //$db_sql = $obj->sql_query($sql);
      
      $Data_config['tDescription'] = 'Price per 100 Miles in '.$ckr;
      $Data_config['vName'] = 'HUNDREDMILES_PRICE_'.$ckr;
      $Data_config['vValue'] = 0.00;
      $Data_config['vOrder'] = 0;
      $Data_config['eType'] = 'Prices';
      $Data_config['eStatus'] = 'Active';
      $Data_config_id = $obj->MySQLQueryPerform("configurations",$Data_config,'insert');  
    }
  }      
  
  $sql = "SELECT vTitle, vCode FROM language_setup ORDER BY vTitle ASC";
  $db_language = $obj->MySQLSelect($sql);
  
  $sql = "SELECT vName, Ratio, vSymbole FROM currency_setup ORDER BY vName ASC";
  $db_currency = $obj->MySQLSelect($sql);
  
?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style>
@import url(http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic);
body {
font-family: "Source Sans Pro", "helvetica", "sans-serif";
font-style: normal;
font-weight: normal;
text-align: center; 
}
</style>
</head>
<body> 
<form name="frm_setup" id="frm_setup" method="post" action="">
<input type="hidden" name="action" id="action" value="setup">
<table width="100%">
  <tr>
    <td width="50%" valign="top">        
      <table width="100%" style="border-collapse: collapse;">
        <tr>
          <th width="5%" style="border-right:1px solid #FFF;text-align:center;background: none repeat scroll 0 0 #00a1ff;color: white;font-size: 13px;padding: 15px 0 14px;">#</th>
          <th width="5%" style="border-right:1px solid #FFF;text-align:center;background: none repeat scroll 0 0 #00a1ff;color: white;font-size: 13px;padding: 15px 0 14px;"></th>
          <th width="20%" style="border-right:1px solid #FFF;text-align:center;background: none repeat scroll 0 0 #00a1ff;color: white;font-size: 13px;padding: 15px 0 14px;">Language</th>
          <th width="10%" style="border-right:1px solid #FFF;text-align:center;background: none repeat scroll 0 0 #00a1ff;color: white;font-size: 13px;padding: 15px 0 14px;">Default</th>
          <th width="10%" style="border-right:1px solid #FFF;text-align:center;background: none repeat scroll 0 0 #00a1ff;color: white;font-size: 13px;padding: 15px 0 14px;">Disp. Order</th>
          <th width="15%" style="border-right:1px solid #FFF;text-align:center;background: none repeat scroll 0 0 #00a1ff;color: white;font-size: 13px;padding: 15px 0 14px;">Curency</th> 
          <th width="35%" style="text-align:center;background: none repeat scroll 0 0 #00a1ff;color: white;font-size: 13px;padding: 15px 0 14px;">&nbsp;</th>     
        </tr>
        <? for($i=0;$i<count($db_language);$i++){?>
        <tr>
          <td width="5%" style="text-align:center;padding:5px;color: rgba(39, 65, 90, 0.5);font-size: 13px;border:1px solid #ccc;"><?=$i+1;?></td>
          <td width="5%" style="text-align:left;padding:5px;color: rgba(39, 65, 90, 0.5);font-size: 13px;border:1px solid #ccc;"><input type="checkbox" name="language[]" id="language_<?=$db_language[$i]['vCode'];?>" value="<?=$db_language[$i]['vCode'];?>"></td>
          <td width="20%" style="text-align:left;padding:5px;color: rgba(39, 65, 90, 0.5);font-size: 13px;border:1px solid #ccc;"><?=$db_language[$i]['vTitle'].' ['.$db_language[$i]['vCode'].']'; ?></td>
          <td width="10%" style="text-align:center;padding:5px;color: rgba(39, 65, 90, 0.5);font-size: 13px;border:1px solid #ccc;"><input type="radio" name="default_language" id="default_language<?=$db_language[$i]['vCode'];?>" value="<?=$db_language[$i]['vCode'];?>"></td>
          <td width="10%" style="text-align:center;padding:5px;color: rgba(39, 65, 90, 0.5);font-size: 13px;border:1px solid #ccc;"><Select name="display_order<?=$db_language[$i]['vCode'];?>" id="display_order<?=$db_language[$i]['vCode'];?>" style="border: 1px solid #ccc;color: rgba(39, 65, 90, 0.5);font-size: 13px;padding: 5px;"><? for($j=1;$j<=count($db_language);$j++){?><option value="<?=$j;?>"><?=$j;?></option><? } ?></select></td>
          <td width="15%" style="text-align:center;padding:5px;color: rgba(39, 65, 90, 0.5);font-size: 13px;border:1px solid #ccc;"><Select name="language_currency<?=$db_language[$i]['vCode'];?>" id="language_currency<?=$db_language[$i]['vCode'];?>" style="border: 1px solid #ccc;color: rgba(39, 65, 90, 0.5);font-size: 13px;padding: 5px;"><? for($j=0;$j<count($db_currency);$j++){?><option value="<?=$db_currency[$j]['vName'];?>"><?=$db_currency[$j]['vName'];?></option><? } ?></select></td>
          <td width="35%" style="text-align:center;padding:5px;color: rgba(39, 65, 90, 0.5);font-size: 13px;border:1px solid #ccc;"></td>      
        </tr>  
        <? } ?>
      </table>       
    </td>
    <td width="50%" valign="top">
      <table width="100%" style="border-collapse: collapse;">
        <tr>
          <th width="5%" style="border-right:1px solid #FFF;text-align:center;background: none repeat scroll 0 0 #00a1ff;color: white;font-size: 13px;padding: 15px 0 14px;">#</th>
          <th width="5%" style="border-right:1px solid #FFF;text-align:center;background: none repeat scroll 0 0 #00a1ff;color: white;font-size: 13px;padding: 15px 0 14px;"></th>
          <th width="20%" style="border-right:1px solid #FFF;text-align:center;background: none repeat scroll 0 0 #00a1ff;color: white;font-size: 13px;padding: 15px 0 14px;">Currency</th>
          <th width="10%" style="border-right:1px solid #FFF;text-align:center;background: none repeat scroll 0 0 #00a1ff;color: white;font-size: 13px;padding: 15px 0 14px;">Default</th>
          <th width="10%" style="border-right:1px solid #FFF;text-align:center;background: none repeat scroll 0 0 #00a1ff;color: white;font-size: 13px;padding: 15px 0 14px;">Disp. Order</th>
          <th width="10%" style="border-right:1px solid #FFF;text-align:center;background: none repeat scroll 0 0 #00a1ff;color: white;font-size: 13px;padding: 15px 0 14px;">Ratio</th>
          <th width="10%" style="border-right:1px solid #FFF;text-align:center;background: none repeat scroll 0 0 #00a1ff;color: white;font-size: 13px;padding: 15px 0 14px;">Symbol</th> 
          <th width="30%" style="text-align:center;background: none repeat scroll 0 0 #00a1ff;color: white;font-size: 13px;padding: 15px 0 14px;">&nbsp;</th>     
        </tr>
        <? for($i=0;$i<count($db_currency);$i++){?>
        <tr>
          <td width="5%" style="text-align:center;padding:5px;color: rgba(39, 65, 90, 0.5);font-size: 13px;border:1px solid #ccc;"><?=$i+1;?></td>
          <td width="5%" style="text-align:left;padding:5px;color: rgba(39, 65, 90, 0.5);font-size: 13px;border:1px solid #ccc;"><input type="checkbox" name="currency[]" id="currency_<?=$db_currency[$i]['vName'];?>" value="<?=$db_currency[$i]['vName'];?>"></td>
          <td width="20%" style="text-align:left;padding:5px;color: rgba(39, 65, 90, 0.5);font-size: 13px;border:1px solid #ccc;"><?=$db_currency[$i]['vName']; ?></td>
          <td width="10%" style="text-align:center;padding:5px;color: rgba(39, 65, 90, 0.5);font-size: 13px;border:1px solid #ccc;"><input type="radio" name="default_currency" id="default_currency<?=$db_currency[$i]['vName'];?>" value="<?=$db_currency[$i]['vName'];?>"></td>
          <td width="10%" style="text-align:center;padding:5px;color: rgba(39, 65, 90, 0.5);font-size: 13px;border:1px solid #ccc;"><Select name="currency_display_order<?=$db_currency[$i]['vName'];?>" id="currency_display_order<?=$db_currency[$i]['vName'];?>" style="border: 1px solid #ccc;color: rgba(39, 65, 90, 0.5);font-size: 13px;padding: 5px;"><? for($j=1;$j<=count($db_currency);$j++){?><option value="<?=$j;?>"><?=$j;?></option><? } ?></select></td>
          <td width="10%" style="text-align:center;padding:5px;color: rgba(39, 65, 90, 0.5);font-size: 13px;border:1px solid #ccc;"><input type="text" name="currency_ration<?=$db_currency[$i]['vName'];?>" id="currency_ration<?=$db_currency[$i]['vName'];?>" style="border: 1px solid #ccc;color: rgba(39, 65, 90, 0.5);font-size: 13px;padding: 5px;width:70px;" value="<?=$db_currency[$i]['Ratio'];?>"></td>
          <td width="10%" style="text-align:center;padding:5px;color: rgba(39, 65, 90, 0.5);font-size: 13px;border:1px solid #ccc;"><input type="text"name="currency_symbol<?=$db_currency[$i]['vName'];?>" id="currency_symbol<?=$db_currency[$i]['vName'];?>" style="border: 1px solid #ccc;color: rgba(39, 65, 90, 0.5);font-size: 13px;padding: 5px;width:70px;" value="<?=$db_currency[$i]['vSymbole'];?>"></td>
          <td width="30%" style="text-align:center;padding:5px;color: rgba(39, 65, 90, 0.5);font-size: 13px;border:1px solid #ccc;"></td>      
        </tr>  
        <? } ?>
      </table>
    </td>
  </tr>
</table>
Replace Word <input type="text"name="replace_from" id="replace_from" style="border: 1px solid #ccc;color: rgba(39, 65, 90, 0.5);font-size: 13px;padding: 5px;width:150px;" value="<?=$db_currency[$i]['vSymbole'];?>">
 To <input type="text"name="replace_from" id="replace_from" style="border: 1px solid #ccc;color: rgba(39, 65, 90, 0.5);font-size: 13px;padding: 5px;width:150px;" value="<?=$db_currency[$i]['vSymbole'];?>">
<input type="submit" name="setup" value="Setup">  
</form>   
</body>
</html>
