<?php
class Customer
{                                                         


/**
*   @desc Variable Declaration with default value
*/

	protected $iCustomerId;   // KEY ATTR. WITH AUTOINCREMENT
   	public $error_msg;
	protected $_iCustomerId;  
	protected $_eTitle;  
	protected $_vFirstName;  
	protected $_vLastName;  
	protected $_vAddress;  
	protected $_vCity;  
	protected $_vState;  
	protected $_vCountry;  
	protected $_vZip;  
	protected $_vPhone;  
	protected $_vEmail;  
	protected $_vUserName;  
	protected $_vPassword;  
	protected $_dDate;  
	protected $_vFromIP;  
	protected $_dLastAccess;  
	protected $_iTotLogin;
	protected $_fDiscount;  
	protected $_eStatus;  
	protected $_eNewsletter;  



/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj,$GeneralObj;
		$this->_obj = $obj;
		$this->_genobj = $GeneralObj;
		
		$this->_iCustomerId = null; 
		$this->_eTitle = null; 
		$this->_vFirstName = null; 
		$this->_vLastName = null; 
		$this->_vAddress = null; 
		$this->_vCity = null; 
		$this->_vState = null; 
		$this->_vCountry = null; 
		$this->_vZip = null; 
		$this->_vPhone = null; 
		$this->_vEmail = null; 
		$this->_vUserName = null; 
		$this->_vPassword = null; 
		$this->_dDate = null; 
		$this->_vFromIP = null; 
		$this->_dLastAccess = null; 
		$this->_iTotLogin = null;
		$this->_fDiscount = null; 
		$this->_eStatus = null; 
		$this->_eNewsletter = null;  
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getiCustomerId()
	{
		return $this->_iCustomerId;
	}

	public function geteTitle()
	{
		return $this->_eTitle;
	}

	public function getvFirstName()
	{
		return $this->_vFirstName;
	}

	public function getvLastName()
	{
		return $this->_vLastName;
	}
	
	public function getvAddress()
	{
		return $this->_vAddress;
	}

	public function getvCity()
	{
		return $this->_vCity;
	}

	public function getvState()
	{
		return $this->_vState;
	}

	public function getvCountry()
	{
		return $this->_vCountry;
	}

	public function getvZip()
	{
		return $this->_vZip;
	}

	public function getvPhone()
	{
		return $this->_vPhone;
	}

	public function getvEmail()
	{
		return $this->_vEmail;
	}

	public function getvUserName()
	{
		return $this->_vUserName;
	}

	public function getvPassword()
	{
		//return $this->_genobj->decrypt(vPassword);
		return $this->_vPassword;
	}

	public function getdDate()
	{
		return $this->_dDate;
	}

	public function getvFromIP()
	{
		return $this->_vFromIP;
	}

	public function getdLastAccess()
	{
		return $this->_dLastAccess;
	}

	public function getiTotLogin()
	{
		return $this->_iTotLogin;
	}
	
	public function getfDiscount()
	{
		return $this->_fDiscount;
	}
	
	public function geteStatus()
	{
		return $this->_eStatus;
	}

	public function geteNewsletter()
	{
		return $this->_eNewsletter;
	}


/**
*   @desc   SETTER METHODS
*/


	public function setiCustomerId($val)
	{
		 $this->_iCustomerId =  $val;
	}

	public function seteTitle($val)
	{
		 $this->_eTitle =  $val;
	}

	public function setvFirstName($val)
	{
		 $this->_vFirstName =  $val;
	}

	public function setvLastName($val)
	{
		 $this->_vLastName =  $val;
	}
	
	public function setvAddress($val)
	{
		 $this->_vAddress =  $val;
	}

	public function setvCity($val)
	{
		 $this->_vCity =  $val;
	}

	public function setvState($val)
	{
		 $this->_vState =  $val;
	}

	public function setvCountry($val)
	{
		 $this->_vCountry =  $val;
	}

	public function setvZip($val)
	{
		 $this->_vZip =  $val;
	}

	public function setvPhone($val)
	{
		 $this->_vPhone =  $val;
	}

	public function setvEmail($val)
	{
		 $this->_vEmail =  $val;
	}

	public function setvUserName($val)
	{
		 $this->_vUserName =  $val;
	}

	public function setvPassword($val)
	{
		 $this->_vPassword =  $this->_genobj->encrypt($val);
	}

	public function setdDate($val)
	{
		 $this->_dDate =  $val;
	}

	public function setvFromIP($val)
	{
		 $this->_vFromIP =  $val;
	}

	public function setdLastAccess($val)
	{
		 $this->_dLastAccess =  $val;
	}

	public function setiTotLogin($val)
	{
		 $this->_iTotLogin =  $val;
	}
	
	public function setfWeight($val)
	{
		 $this->_fWeight =  $val;
	}
	
	public function seteStatus($val)
	{
		 $this->_eStatus =  $val;
	}

	public function seteNewsletter($val)
	{
		 $this->_eNewsletter =  $val;
	}


/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id)
	{
		 $sql =  "SELECT * FROM customer WHERE iCustomerId = $id";
		 $row =  $this->_obj->select($sql); 

		 $this->_iCustomerId = $row[0]['iCustomerId'];
		 $this->_eTitle = $row[0]['eTitle'];
		 $this->_vFirstName = $row[0]['vFirstName'];
		 $this->_vLastName = $row[0]['vLastName'];
		 $this->_vAddress = $row[0]['vAddress'];
		 $this->_vCity = $row[0]['vCity'];
		 $this->_vState = $row[0]['vState'];
		 $this->_vCountry = $row[0]['vCountry'];
		 $this->_vZip = $row[0]['vZip'];
		 $this->_vPhone = $row[0]['vPhone'];
		 $this->_vEmail = $row[0]['vEmail'];
		 $this->_vUserName = $row[0]['vUserName'];
		 $this->_vPassword = $row[0]['vPassword'];
		 $this->_dDate = $row[0]['dDate'];
		 $this->_vFromIP = $row[0]['vFromIP'];
		 $this->_dLastAccess = $row[0]['dLastAccess'];
		 $this->_iTotLogin = $row[0]['iTotLogin'];
		 $this->_fDiscount = $row[0]['fDiscount'];
		 $this->_eStatus = $row[0]['eStatus'];
		 $this->_eNewsletter = $row[0]['eNewsletter'];
		 return $row;
	}


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM customer WHERE iCustomerId = $id";
		 $result = $this->_obj->sql_query($sql);
		 return $result;
	}


/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->iCustomerId = ""; // clear key for autoincrement

		$sql = "INSERT INTO customer ( eTitle, vFirstName, vLastName, vAddress, vCity, vState, vCountry, vZip, vPhone, vEmail, vUserName, vPassword, dDate, vFromIP, dLastAccess, iTotLogin, eStatus, eNewsletter ) VALUES ( '".$this->_eTitle."', '".$this->_vFirstName."', '".$this->_vLastName."', '".$this->_vAddress."', '".$this->_vCity."', '".$this->_vState."', '".$this->_vCountry."', '".$this->_vZip."', '".$this->_vPhone."', '".$this->_vEmail."', '".$this->_vUserName."', '".$this->_vPassword."', '".$this->_dDate."', '".$this->_vFromIP."', '".$this->_dLastAccess."', '".$this->_iTotLogin."', '".$this->_eStatus."','".$this->_eNewsletter."' )";
		 
		 $result = $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id,$data)
	{

		 $sql = " UPDATE customer SET " ;
		 while (list($columns, $value) = each($data))                                 
			{
				switch ((string)$value) 
				{
	         			case 'null':
	           			$query .= $columns .= ' = null, ';
	           			break;
	         			default:
	           			$query .= $columns . ' = \'' . $value . '\',';
	           		break;
	       		}
			}
		 $query=substr($query,0,-1);
		 $sql.= $query;
		 $sql.=" WHERE iCustomerId = $id " ;
		 $result = $this->_obj->sql_query($sql);
      
     return $result;
	}
	
		function setAllVar()
	{
		$MethodArr = get_class_methods($this);
		foreach($_REQUEST AS $KEY => $VAL)
		{
			$method = "set".$KEY;
			if(in_array($method , $MethodArr))
			{
				$evalStr = '$this->'.$method."('".$VAL."');";
				eval("\$this->".$method."('".$VAL."');");
			}
		}
	}

	function getAllVar()
	{
		$MethodArr = get_class_methods($this);
		$method_notArr=Array('getAllVar'); 
		$evalStr='';
		for($i=0;$i<count($MethodArr);$i++)
		{
			if(substr($MethodArr[$i] , 0 ,3) == 'get' && (!(in_array($MethodArr[$i],$method_notArr))))
			{
				$var_name = substr($MethodArr[$i] , 3 );
				$evalStr.= 'global $'.$var_name.'; $'.$var_name.' = $this->'.$MethodArr[$i]."();";
			}
		}
		eval($evalStr);
	}

  function return_state_bill($vState)
	{
		$sql = "SELECT iStateId,vState FROM state_master WHERE iStateId = '$vState' ";
		$data = $this->_obj->select($sql);
		return $data;
	}
	function return_state_ship($vSState)
	{
		$sql = "SELECT iStateId,vState FROM state_master WHERE iStateId = '$vSState' ";
		$data = $this->_obj->select($sql);
		return $data;
	}
	
	function return_state()
	{
		$sql = "SELECT iStateId,vState,vStateCode,vCountryCode FROM state_master WHERE eStatus='Active' ORDER BY vCountryCode,vState ";
		$data = $this->_obj->select($sql);
		return $data;
	}
	
	function return_switch_customer()
	{
		$sql="SELECT iCustomerId,vFirstName,vLastName FROM customer ORDER BY vFirstName";
		$data = $this->_obj->select($sql);
		return $data;
	}
	
	function DisplayBillingCountry($selcountry)
	{
	$db = $this->_obj->select("select iCountryId,vCountry,vCountryCode from country_master where eStatus = 'Active' and eBilling='Yes' ORDER BY vCountry asc");
		if(count($db) > 0)
		{
			for($i=0;$i<count($db);$i++)
			{
				$longname[$i] = $db[$i]['vCountry'];
				$shortname[$i] = $db[$i]['vCountryCode'];
			}
		}
		if(empty($selcountry))
			$selcountry = "GB";
		$countrycombo = '';
		for($i=0;$i< count($shortname);$i++) 
		{
			if (trim($selcountry) == trim($shortname[$i]))
			{
				$countrycombo .= "<option value='".$shortname[$i]."' selected>".$longname[$i]."</option>";
			} 
			else
			{
				$countrycombo .= "<option value='".$shortname[$i]."'>".$longname[$i]."</option>";
			}
		}
		
    return $countrycombo;
	}
	
	function DisplayShippingCountry($selcountry) 
	{
		$db = $this->_obj->select("select iCountryId,vCountry,vCountryCode from country_master where eStatus = 'Active' and eShipping='Yes' ORDER BY vCountry asc");
		if(count($db) > 0)
		{
			for($i=0;$i<count($db);$i++)
			{
				$longname[$i] = $db[$i]['vCountry'];
				$shortname[$i] = $db[$i]['vCountryCode'];
			}
		}
		if(empty($selcountry))
			$selcountry = "GB";
		$countrycombo = "";
		for($i=0;$i< count($shortname);$i++) 
		{
			if (trim($selcountry)==trim($shortname[$i]))
    	    {
				$countrycombo .= "<option value='".$shortname[$i]."' selected>".$longname[$i]."</option>";
			}
			else 
			{
				$countrycombo .= "<option value='".$shortname[$i]."'>".$longname[$i]."</option>";
			}
		}
		return $countrycombo;
}

  function check_duplicate_email($vEmail,$iCustomerId = '')
	{
		if($iCustomerId == '')
			$cus_sel = "SELECT vEmail FROM customer WHERE vEmail='$vEmail'";
		else
			$cus_sel = "SELECT vEmail FROM customer WHERE vEmail='$vEmail' AND iCustomerId <> '$iCustomerId'";
		$data = $this->_obj->select($cus_sel);
		return $data;
	}
	
	function totrecord($id,$cond)
	{
		$sql="select count(iMemberId) as tot from log_history  where iMemberId = $id and eType='Member' $cond";
		$row = $this->_obj->select($sql); 
		return $row;
	}
	
	function countlogindate($id,$cond,$var="")
	{
		$sql = "select iLogId, iMemberId, eType, vIP, dLoginDate, dLogoutDate from log_history  where iMemberId = $id and eType='Member' $cond order by dLoginDate DESC $var";
		$row = $this->_obj->select($sql); 
		return $row;
	}
	
	function getMember($iId="")
	{
		global $obj;
		$sql_customer = "select * from members where iMemberId='".$iId."'";
		$db_customer = $this->_obj->MySQLSelect($sql_customer); 
		return $db_customer;
	}
  function getstate_name($field="" , $table="" ,$whereclouse="", $code="", $countrytext="", $countrycode="")
  {
	 global $obj;
	 $sqlsta="select ".$field." from ".$table." where ".$whereclouse."='".$code."' and ".$countrytext."='".$countrycode."'";
	 $db_sta=$this->_obj->select($sqlsta);
	 $state = $db_sta[0]["$field"];
	 return $state;
  }
  function getcountry($field="" , $table="" ,$whereclouse="", $code="")
  {
		global $obj;	 
	 $sqlsta="select ".$field." from ".$table."  where ".$whereclouse."='".$code."'";
	 $db_sta=$this->_obj->select($sqlsta);
	 $state = $db_sta[0]["$field"];
	 return $state;
  }
  
  function report_select($iCustomerId,$keyword,$setlatter,$option)
	{
			global $obj;
		if(!isset($iCustomerId) && empty($iCustomerId))
		{
			if(!isset($keyword))
			{
				$sql = "select vFirstName, vLastName, vEmail, SUM(fNetTotal), count(iOrderId) as totalorder, iCustomerId from orders WHERE iOrderStatusId != '5' GROUP BY iCustomerId";
			}
			else
			{
				if($setlatter == "true")
				{
					$sql = "select vFirstName, vLastName, vEmail, SUM(fNetTotal) as amount_sum, count(iOrderId) as totalorder, iCustomerId from orders  where $option like '$keyword%' AND iOrderStatusId != '5' GROUP BY iCustomerId";

				}
				else
				{
					$sql = "select vFirstName, vLastName, vEmail, SUM(fNetTotal), count(iOrderId) as totalorder, iCustomerId from orders where $option like '%$keyword%' AND iOrderStatusId != '5' GROUP BY iCustomerId";
				}
			}
		}
		else
		{
			if(isset($iCustomerId) && !empty($iCustomerId))
			{
				$sql = "select vFirstName, vLastName, vEmail, SUM(fNetTotal), count(iOrderId) as totalorder, iCustomerId from orders where iCustomerId = '" .$iCustomerId. "' AND iOrderStatusId != '5' GROUP BY iCustomerId";
			}
		}
		$result = $this->_obj->select($sql); 
		return $result;
	}
	
	function report_db_query($iCustomerId,$keyword,$setlatter,$sort,$var_limit,$option)
	{
		if(!isset($iCustomerId) && empty($iCustomerId))
		{
			if(!isset($keyword))
			{
				$sql = "select vFirstName, vLastName, vEmail, SUM(fNetTotal) as amount_sum, count(iOrderId) as totalorder, iCustomerId from orders WHERE iOrderStatusId != '5' GROUP BY iCustomerId order by $sort $var_limit";
			}
			else
			{
				$keyword = stripslashes($keyword);
				$option = stripslashes($option);
				if($setlatter=="true")
				{
					$sql = "select vFirstName, vLastName, vEmail, SUM(fNetTotal) as amount_sum, count(iOrderId) as totalorder, iCustomerId from orders  where $option like '$keyword%' AND iOrderStatusId != '5' GROUP BY iCustomerId order by $sort $var_limit";
				}
				else
				{
					$sql = "select vFirstName, vLastName, vEmail, SUM(fNetTotal) as amount_sum, count(iOrderId) as totalorder, iCustomerId from orders  where $option like '%$keyword%' AND iOrderStatusId != '5' GROUP BY iCustomerId order by $sort $var_limit";
				}
			}
		}
		else
		{
			$sql = "select vFirstName, vLastName, vEmail, SUM(fNetTotal) as amount_sum, count(iOrderId) as totalorder, iCustomerId from orders where iCustomerId = '" .$iCustomerId. "' AND iOrderStatusId != '5' GROUP BY iProductId order by $sort $var_limit";
		}

		$result = $this->_obj->select($sql); 
		return $result;
		
	}
	
	function return_customer_data()
	{
		$sql = "select distinct(substring(c.vFirstName,1,1)) as alphaSearch from customer c, orders o WHERE o.iCustomerId = c.iCustomerId";
		$data = $this->_obj->select($sql);
		return $data;
	}
	
	function return_customer_report()
	{
		$sql="SELECT * FROM customer WHERE eStatus='Active' ORDER BY vFirstName";
		$result = $this->_obj->select($sql);
		return $result;
	}
	function select_all_customers()
	{
		$cust_sql="select iCustomerId,vFirstName,vLastName,vEmail from customer where eStatus = 'Active' AND eNewsletter='Y' order by vFirstName ASC";
		$db_cust=$this->_obj->select($cust_sql);
		
		$customer = "";
		$customer .= "<select name=\"iCustomerId[]\" id=\"iCustomerId\" multiple style=\"width:300px\">";
		for($i=0;$i<count($db_cust);$i++)
		{
			if($db_cust[$i]["vFirstName"] != "")
			{
				$customer .= "<option value=".$db_cust[$i]["iCustomerId"]." >".stripslashes($db_cust[$i]["vFirstName"])."&nbsp;".stripslashes($db_cust[$i]["vLastName"])."&nbsp;&nbsp;[".$db_cust[$i]["vEmail"]."]";
			}
		}
		$customer .= "</select>";
		return $customer;
	}
    
    function getstate(){
	   global $obj;
       $sql="select iStateId,vState from state_master where vCountryCode='US' and eStatus = 'Active'";
       $db_state = $obj->MySQLSelect($sql);
       return $db_state;
}

	function DoGuestLogin()
	{  
		global $obj,$site_url,$generalobj,$tconfig;
	
	//echo "<pre>";print_r($_POST);exit;
   	$sql="select iCustomerId, vEmail, vCustomerName, eStatus ,dPasswordDate from customer where vEmail = '".$_POST['vEmail']."' and vPassword = '".$_POST['vPassword']."' and eType='Customer' "; 
		$db_login = $obj->MySQLSelect($sql);
    $dPasswordDate = $db_login[0]['dPasswordDate'];       	
	//echo "<pre>";print_r($db_login);exit;
    
//echo  $differance = $generalobj->Hours_Difference($dPasswordDate);exit;
   $differance = $generalobj->diff_hours($dPasswordDate);
    if($differance < 48) 
		{		
			$_SESSION['sess_iCustomerId']=$db_login[0]['iCustomerId'];
			$_SESSION["sess_vCustomerName"]=$db_login[0]['vCustomerName'];
			$_SESSION["sess_vEmail"]=$db_login[0]['vEmail'];			
			#----- cookie for login is set for one month -----#
			if($_POST['remember_login'] == "Yes")
			{
				setcookie ("member_login_cookie", $_POST['vloginEmail'], time()+2592000);
				setcookie ("member_password_cookie", $_POST['vLoginPassword'], time()+2592000);
			}
			else
			{
				setcookie ("member_login_cookie", "", time());
				setcookie ("member_password_cookie", "", time());
			}
			$msg = LBL_WELCOME_USER;
			 
				header("Location:".$tconfig['tsite_url']."index.php?file=m-order_history&msg_code=1&var_msg_login=".$msg);
				exit;
			 
		}
		else
		{
		    $msg = "Your login failed. This happens due to your Password has been expired.";
        	  
			    header("Location:".$tconfig['tsite_url']."index.php?file=cu-login&msg_code=1&var_msg_login=".$msg);
          exit;  
			 
    }
	}
	
	function DoMemberLogin()
	{  		
    global $obj,$site_url,$generalobj,$tconfig; 
  	$sql="select iMemberId, vEmail, vFirstName , vLastName, eStatus from members where vEmail = '".$_POST['vEmail']."' and vPassword = '".$generalobj->encrypt($_POST['vPassword'])."'"; 
  	$db_login = $obj->MySQLSelect($sql);   
    if($db_login[0]['eStatus'] == "Inactive") 
		{
        $var_msg = "Your account is inactivated. Please contact administrator.";
        header("Location:".$tconfig['tsite_url']."index.php?file=m-login&var_msg_fail=$var_msg");
		    exit;  
    }	
    if($db_login[0]['eStatus'] == "Pending") 
		{
        $var_msg = "Your account is Pending now. Please contact administrator.";
        header("Location:".$tconfig['tsite_url']."index.php?file=m-login&var_msg_fail=$var_msg");
		    exit;  
    }	
    if(count($db_login) > 0) 
		{		
			$_SESSION['sess_iMemberId']=$db_login[0]['iMemberId'];
			$_SESSION["sess_vFirstName"]=$db_login[0]['vFirstName'];
			$_SESSION["sess_vLastName"]=$db_login[0]['vLastName'];
			$_SESSION["sess_vEmail"]=$db_login[0]['vEmail'];			
			#----- cookie for login is set for one month -----#
			if($_POST['remember_login'] == "Yes")
			{
				setcookie ("member_login_cookie", $_POST['vloginEmail'], time()+2592000);
				setcookie ("member_password_cookie", $_POST['vLoginPassword'], time()+2592000);
			}
			else
			{
				setcookie ("member_login_cookie", "", time());
				setcookie ("member_password_cookie", "", time());
			}
			$msg = "Welcome! You are successfully Logged in.";
			header("Location:".$tconfig['tsite_url']."edit-profile");
			exit;
			 
		}
		else
		{
      $msg = "Your login failed. This happens due to -Incorrect Username or Password.";
      header("Location:".$tconfig['tsite_url']."index.php?file=m-login&var_msg_fail=".$msg);
      exit; 		 
    }

  
	}
	
	function check_member_login()
	{
		global $site_url,$tconfig;
		$site_url = isset($tconfig['tsite_url'])?$tconfig['tsite_url']:$site_url;
		
		if($_SESSION['sess_iMemberId'] == "")
		{
			$url = (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] != '') ?  $_SERVER['REQUEST_URI']:'';
			if($url != '') {
				$arr = explode("/",$url);
				$_SESSION['redirect_link'] = $tconfig['tsite_url'].$arr[count($arr)-1];
			}
    		header("Location:".$tconfig['tsite_url']."index.php?file=c-login");
			exit;
		}
	}
	
	function check_cancel_booking($iDriverId)
  {
    global $obj,$site_url,$generalobj,$tconfig; 
  	$sql="select iDriverId, dCancelDate, eCancelBy from booking_new where iDriverId = '".$iDriverId."' AND eStatus = 'Cencelled' AND eCancelBy = 'Driver' ORDER BY dCancelDate DESC LIMIT 0,1 "; 
  	$db_cancel_booking = $obj->MySQLSelect($sql);
  	if(count($db_cancel_booking) > 0){
       $currenttime = time();
       $bookingcanceltime = strtotime($db_cancel_booking[0]['dCancelDate']);
       $difference = $currenttime-$bookingcanceltime; 
       if($difference < 86400)
       {
          header("Location:".$tconfig["tsite_url"]."index.php?file=m-blockbooking");
          exit;
       } 
    }
  }
	
	function check_Guest_login()
	{
		global $site_url;
		if($_SESSION['sess_iMemberId'] == ""){
    		header("Location:".$site_url."index.php?file=cu-login");
			exit;
		}
	}
    
  function MemberDeleteAccount()
	{
		global $obj,$site_url;
		
		if($_SESSION['sess_iECommerceCustomerId'])
		{
			$sql="update customer set eStatus='Delete' where iCustomerId = '".$_SESSION['sess_iECommerceCustomerId']."'";
			$db_MemUpdate = $obj->sql_query($sql);
			session_destroy();
		}
		header("Location:".$site_url."index.php?file=m-login&var_msg=Your Account has been deleted successfully.");
		exit;
	}
      
      
function GuestForgetPwd()
	{	  
		global $obj,$generalobj;		 
		$sql="select iCustomerId, vEmail, vCustomerName, vPassword from customer where vEmail = '".$_POST['vPassEmail']."' and eStatus='Active'";
		$db_member = $obj->MySQLSelect($sql); 
    //print_r($db_member); exit;	
		if(count($db_member)>0)
		{
			$status = $generalobj->send_email_user("CUSTOMER_FORGETPASSWORD",$db_member);
			if($status==1)
			{
				$var_msg = LBL_PASSWORD_SENT;
                $this->error_msg = "0";
			}
			else
			{
				$var_msg = LBL_SENDING_PASSWORD;
                $this->error_msg = "1";
			}
		}
		else
		{
			$var_msg = LBL_EMAIL_NOT_FOUND;
             $this->error_msg = "1";
		}		
		return $var_msg;
	}
	
	
	function MemberForgetPwd()
	{	  
		global $obj,$generalobj;         	                                                                      
		$sql="select iMemberId, vEmail, vFirstName, vLastName, vPassword from member where vEmail = '".$_POST['vPassEmail']."' and eStatus='Active'";
		$db_member = $obj->MySQLSelect($sql);
		
		if(count($db_member)>0)
		{
			if($db_member[0]['iFBId'] != 0) 
			{
				$var_msg = LBL_FORGOT_PASSWORD_FB_USER;     			
				$this->error_msg = "1";
			} 
			else {
				$status = $generalobj->send_email_user("CUSTOMER_FORGETPASSWORD",$db_member);
				if($status==1)
				{
					$var_msg = LBL_PASSWORD_SENT_SUCCESSFULLY;
					$this->error_msg = "0";
				}
				else
				{
					$var_msg = LBL_PASSWORD_SENT_ERROR;
					$this->error_msg = "1";
				}
			}			
		}
		else
		{
			$var_msg = LBL_EMAIL_NOT_FOUND;
             $this->error_msg = "1";
		}		
		return $var_msg;
	}

	
	function checkMemberExists()
	{
		global $obj;
		$user_sel = "select vEmail from members where vEmail='".$_POST['vEmail']."'";
		$sel_db = $obj->select($user_sel);
	
		if(count($sel_db)> 0)
		{
			$error_msg=USERNAME_EXIST;
			getPostForm($_POST,$error_msg,"index.php?file=m-registration");
			exit;
		}
	}
	
	function MemberChangePwd()
	{
		global $obj,$generalobj,$tconfig;
		$vOldPassword = $generalobj->encrypt(trim($_POST[vOldPassword]));
		$sql="select iMemberId from member where vPassword = '".$vOldPassword."' and  iMemberId ='".$_SESSION['sess_iMemberId']."'";
		$db_member = $obj->MySQLSelect($sql);
	  if($db_member)
		{
			$sql="update member set vPassword ='".$generalobj->encrypt(trim($_POST['vPassword']))."' where vPassword = '".$vOldPassword."' and iMemberId = '".$_SESSION['sess_iMemberId']."'";
			$db_MemUpdate = $obj->sql_query($sql);
			if($db_MemUpdate)
			{
                $msg = LBL_PASSWORD_CHANGED_SUCCESSFULLY;
                $error_msg = "1";
			}
			else
			{
				$msg = LBL_PASSWORD_CHANGED_ERROR;
                $error_msg = "0";
			}
		}
		else
		{
			$msg = LBL_PASSWORD_CHANGED_INVALID;
            $error_msg = "0";
		}
		header("location:".$tconfig['tsite_url']."index.php?file=m-changepassword&var_msg=".$msg."&msg_code=".$error_msg);
		exit;
	}
	
	function get_logged_memberid()
	{
		return $_SESSION['sess_iCustomerId'];
	}


    function check_plan($id,$facility_field="")
    {
    	global $obj;
    	$check_flag=1;
    	$sql = "select * from jobseeker_plan_purchase where iJobSeekerId='".$id."' and eStatus='Approved' order by iJSPlanPurchId desc";
    	$db_rec = $obj->MySQLSelect($sql);
    	if(count($db_rec)>0)
    	{
    		if($db_rec[0]['fPrice'] == "0.00")
    		{
    			if($db_rec[0]['iDuration'] == "-1")
    			{
    					$check_flag=1;
    			}
    			else
    			{
    				if(strtotime($db_rec[0]['dExpireDate']) < time())
    				{
    					$check_flag=0;
    				}
    			}
    		}
    		else
    		{
    			if(strtotime($db_rec[0]['dExpireDate']) < time())
    			{
    				$check_flag=0;
    			}
    			/* check plan's facility */
    			if($facility_field != "")
    			{
    				if($db_rec[0][$facility_field] == "No")
    				{
    					$check_flag=0;
    				}
    			}
    		}
    	}
    	else
    	{
    		$check_flag=2;
    	}
    	return $check_flag;
    }

/**
*   @desc   Destroy Object
*/

	function destroyObj($obj)
	{
		 unset($obj) ;
	}


} 

?>
