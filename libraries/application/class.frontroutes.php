<?
class FrontRoutes
{
	var $file;
	var $mode;

	function __construct()
	{
		if(isset($_REQUEST['file']) && $_REQUEST['file']!="")
		{

			$this->file = $_REQUEST['file'];
			
			if($_REQUEST['mode'] == 'add' || $_REQUEST['mode'] == 'edit')
			{             
				$this->mode = $_REQUEST['mode'];             
			}
			else if($_REQUEST['mode'] == 'view')
			{
				$this->mode = "view";                     
			}
			else
			{
				$this->mode = "view";
			}
				
			$sc = explode("-",$this->file);
			switch(	$sc[0] )
			{
				case "c":
						$module = "content";
						break;
				case "ec":
						$module = "cart";
						break;
				case "m":
						$module = "members";
						break;
				case "o":
						$module = "organizations";
						break;
				case "ax":
						$module = "ajax_file";
						$this->mode = "";
						break;
				default:
						$module = "content";
						$this->module = "content";
						$this->script = "home";
						$this->mode = "view";
						break;
			}
            
			$this->module = $module;
			$this->script = $sc[1];
		}
		else
		{
			$this->module = "content";
			$this->script = "home";
			$this->mode = "view";
		}

	}

	function GetModule()
	{
		return $this->module;
	}

	function GetMode()
	{
		return $this->mode;
	}

	function GetScript()
	{
		return $this->script;
	}

	

}
?>
