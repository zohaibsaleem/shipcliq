<?php
class Order
{
	protected $iOrderId;   // KEY ATTR. WITH AUTOINCREMENT

	protected $_iOrderId;  
	protected $_iCustomerId;  
	protected $_vOrderNo;  
	protected $_vEmail;  
	protected $_fSubTotal;  
	protected $_fShipping;  
	protected $_fTax;  
	protected $_vDiscount;  
	protected $_fNetTotal;  
	protected $_eTitle;  
	protected $_vFirstName;  
	protected $_vLastName;  
	protected $_vAddress;  
	protected $_vCity;  
	protected $_vState;  
	protected $_vCountry;  
	protected $_vZip;  
	protected $_vMobile;  
	protected $_vPhone;  
	protected $_eSTitle;  
	protected $_vSFirstName;  
	protected $_vSLastName;  
	protected $_vSCompany;  
	protected $_vSAddress;  
	protected $_vSCity;  
	protected $_vSState;  
	protected $_vSCountry;  
	protected $_vSZip;  
	protected $_vSPhone;  
	protected $_vPaypalId;  
	protected $_ePaid;  
	protected $_iTransactionId;  
	protected $_vRemarks;  
	protected $_tResponse;  
	protected $_vInstruction;  
	protected $_vFromIP;  
	protected $_vCouponCode;  
	protected $_dDate;  
	protected $_ePaymentOption;  
	protected $_iOrderStatusId;  
	protected $_vShowOrder;  
	protected $_dShippingDate;  
	protected $_vShipOption;
	protected $_fPackagePrice;
	protected $_vGiftCertCode;
	protected $_fGiftCerDisc;
	protected $_vTrackingNo;
	protected $_eXmlStatus;


/**
*   @desc   CONSTRUCTOR METHOD
*/

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;

		$this->_iOrderId = null; 
		$this->_iCustomerId = null; 
		$this->_vOrderNo = null; 
		$this->_vEmail = null; 
		$this->_fSubTotal = null; 
		$this->_fShipping = null; 
		$this->_fTax = null; 
		$this->_vDiscount = null; 
		$this->_fNetTotal = null; 
		$this->_eTitle = null; 
		$this->_vFirstName = null; 
		$this->_vLastName = null; 
		$this->_vAddress = null; 
		$this->_vCity = null; 
		$this->_vState = null; 
		$this->_vCountry = null; 
		$this->_vZip = null; 
		$this->_vMobile = null; 
		$this->_vPhone = null; 
		$this->_eSTitle = null; 
		$this->_vSFirstName = null; 
		$this->_vSLastName = null; 
		$this->_vSCompany = null; 
		$this->_vSAddress = null; 
		$this->_vSCity = null; 
		$this->_vSState = null; 
		$this->_vSCountry = null; 
		$this->_vSZip = null; 
		$this->_vSPhone = null; 
		$this->_vPaypalId = null; 
		$this->_ePaid = null; 
		$this->_iTransactionId = null; 
		$this->_vRemarks = null; 
		$this->_tResponse = null; 
		$this->_vInstruction = null; 
		$this->_vFromIP = null; 
		$this->_vCouponCode = null; 
		$this->_dDate = null; 
		$this->_ePaymentOption = null; 
		$this->_iOrderStatusId = null; 
		$this->_vShowOrder = null; 
		$this->_dShippingDate = null;
		$this->_fPackagePrice = null;
		$this->_vGiftCertCode = null;
		$this->_fGiftCerDisc = null;
		$this->_vTrackingNo = null;
		$this->_vShipOption = null;
    $this->_eXmlStatus = null;  
	}

/**
*   @desc   DECONSTRUCTOR METHOD
*/

	function __destruct()
	{
		unset($this->_obj);
	}



/**
*   @desc   GETTER METHODS
*/


	public function getiOrderId()
	{
		return $this->_iOrderId;
	}

	public function getiCustomerId()
	{
		return $this->_iCustomerId;
	}

	public function getvOrderNo()
	{
		return $this->_vOrderNo;
	}

	public function getvEmail()
	{
		return $this->_vEmail;
	}

	public function getfSubTotal()
	{
		return $this->_fSubTotal;
	}

	public function getfShipping()
	{
		return $this->_fShipping;
	}

	public function getfTax()
	{
		return $this->_fTax;
	}

	public function getvDiscount()
	{
		return $this->_vDiscount;
	}

	public function getfNetTotal()
	{
		return $this->_fNetTotal;
	}

	public function geteTitle()
	{
		return $this->_eTitle;
	}

	public function getvFirstName()
	{
		return $this->_vFirstName;
	}

	public function getvLastName()
	{
		return $this->_vLastName;
	}

	public function getvAddress()
	{
		return $this->_vAddress;
	}

	public function getvCity()
	{
		return $this->_vCity;
	}
	
	public function getvGiftCertCode()
	{
		return $this->_vGiftCertCode;
	}

	public function getvState()
	{
		return $this->_vState;
	}

	public function getvCountry()
	{
		return $this->_vCountry;
	}

	public function getvZip()
	{
		return $this->_vZip;
	}

	public function getvMobile()
	{
		return $this->_vMobile;
	}

	public function getvPhone()
	{
		return $this->_vPhone;
	}

	public function geteSTitle()
	{
		return $this->_eSTitle;
	}

	public function getvSFirstName()
	{
		return $this->_vSFirstName;
	}

	public function getvSLastName()
	{
		return $this->_vSLastName;
	}

	public function getvSCompany()
	{
		return $this->_vSCompany;
	}

	public function getvSAddress()
	{
		return $this->_vSAddress;
	}

	public function getvSCity()
	{
		return $this->_vSCity;
	}

	public function getvSState()
	{
		return $this->_vSState;
	}

	public function getvSCountry()
	{
		return $this->_vSCountry;
	}

	public function getvSZip()
	{
		return $this->_vSZip;
	}

	public function getvSPhone()
	{
		return $this->_vSPhone;
	}

	public function getvPaypalId()
	{
		return $this->_vPaypalId;
	}

	public function getePaid()
	{
		return $this->_ePaid;
	}

	public function getiTransactionId()
	{
		return $this->_iTransactionId;
	}

	public function getvRemarks()
	{
		return $this->_vRemarks;
	}

	public function gettResponse()
	{
		return $this->_tResponse;
	}

	public function getvInstruction()
	{
		return $this->_vInstruction;
	}

	public function getvFromIP()
	{
		return $this->_vFromIP;
	}

	public function getvCouponCode()
	{
		return $this->_vCouponCode;
	}

	public function getdDate()
	{
		return $this->_dDate;
	}

	public function getePaymentOption()
	{
		return $this->_ePaymentOption;
	}

	public function getiOrderStatusId()
	{
		return $this->_iOrderStatusId;
	}

	public function getvShowOrder()
	{
		return $this->_vShowOrder;
	}

	public function getdShippingDate()
	{
		return $this->_dShippingDate;
	}
	
	public function getfPackagePrice()
	{
		return $this->_fPackagePrice;
	}
	
	public function getvTrackingNo()
	{
		return $this->_vTrackingNo;
	}
	
	public function getfGiftCerDisc()
	{
		return $this->_fGiftCerDisc;
	}
	
	public function getvShipOption()
	{
		return $this->_vShipOption;
	}
  
  public function geteXmlStatus()
	{
		return $this->_eXmlStatus;
	}

/**
*   @desc   SETTER METHODS
*/


	public function setiOrderId($val)
	{
		 $this->_iOrderId =  $val;
	}

	public function setiCustomerId($val)
	{
		 $this->_iCustomerId =  $val;
	}

	public function setvOrderNo($val)
	{
		 $this->_vOrderNo =  $val;
	}

	public function setvEmail($val)
	{
		 $this->_vEmail =  $val;
	}

	public function setfSubTotal($val)
	{
		 $this->_fSubTotal =  $val;
	}

	public function setfShipping($val)
	{
		 $this->_fShipping =  $val;
	}

	public function setfTax($val)
	{
		 $this->_fTax =  $val;
	}

	public function setvDiscount($val)
	{
		 $this->_vDiscount =  $val;
	}

	public function setfNetTotal($val)
	{
		 $this->_fNetTotal =  $val;
	}

	public function seteTitle($val)
	{
		 $this->_eTitle =  $val;
	}

	public function setvFirstName($val)
	{
		 $this->_vFirstName =  $val;
	}

	public function setvLastName($val)
	{
		 $this->_vLastName =  $val;
	}

	public function setvAddress($val)
	{
		 $this->_vAddress =  $val;
	}

	public function setvCity($val)
	{
		 $this->_vCity =  $val;
	}

	public function setvState($val)
	{
		 $this->_vState =  $val;
	}

	public function setvCountry($val)
	{
		 $this->_vCountry =  $val;
	}
	
	public function setvGiftCertCode($val)
	{
		 $this->_vGiftCertCode =  $val;
	}

	public function setvZip($val)
	{
		 $this->_vZip =  $val;
	}

	public function setvMobile($val)
	{
		 $this->_vMobile =  $val;
	}

	public function setvPhone($val)
	{
		 $this->_vPhone =  $val;
	}

	public function seteSTitle($val)
	{
		 $this->_eSTitle =  $val;
	}

	public function setvSFirstName($val)
	{
		 $this->_vSFirstName =  $val;
	}

	public function setvSLastName($val)
	{
		 $this->_vSLastName =  $val;
	}

	public function setvSCompany($val)
	{
		 $this->_vSCompany =  $val;
	}

	public function setvSAddress($val)
	{
		 $this->_vSAddress =  $val;
	}

	public function setvSCity($val)
	{
		 $this->_vSCity =  $val;
	}

	public function setvSState($val)
	{
		 $this->_vSState =  $val;
	}

	public function setvSCountry($val)
	{
		 $this->_vSCountry =  $val;
	}

	public function setvSZip($val)
	{
		 $this->_vSZip =  $val;
	}

	public function setvSPhone($val)
	{
		 $this->_vSPhone =  $val;
	}

	public function setvPaypalId($val)
	{
		 $this->_vPaypalId =  $val;
	}

	public function setePaid($val)
	{
		 $this->_ePaid =  $val;
	}

	public function setiTransactionId($val)
	{
		 $this->_iTransactionId =  $val;
	}

	public function setvRemarks($val)
	{
		 $this->_vRemarks =  $val;
	}

	public function settResponse($val)
	{
		 $this->_tResponse =  $val;
	}

	public function setvInstruction($val)
	{
		 $this->_vInstruction =  $val;
	}

	public function setvFromIP($val)
	{
		 $this->_vFromIP =  $val;
	}

	public function setvCouponCode($val)
	{
		 $this->_vCouponCode =  $val;
	}

	public function setdDate($val)
	{
		 $this->_dDate =  $val;
	}

	public function setePaymentOption($val)
	{
		 $this->_ePaymentOption =  $val;
	}

	public function setiOrderStatusId($val)
	{
		 $this->_iOrderStatusId =  $val;
	}

	public function setvShowOrder($val)
	{
		 $this->_vShowOrder =  $val;
	}

	public function setdShippingDate($val)
	{
		 $this->_dShippingDate =  $val;
	}
	
	public function setvShipOption($val)
	{
		 $this->_vShipOption =  $val;
	}
	
	public function setfPackagePrice($val)
	{
		 $this->_fPackagePrice =  $val;
	}
	
	public function setfGiftCerDisc($val)
	{
		 $this->_fGiftCerDisc =  $val;
	}
	
	public function setvTrackingNo($val)
	{
		 $this->_vTrackingNo =  $val;
	}
  
  public function seteXmlStatus($val)
	{
		 $this->_eXmlStatus =  $val;
	}
	
/**
*   @desc   SELECT METHOD / LOAD
*/

	function select($id)
	{
		 $sql =  "SELECT * FROM orders WHERE iOrderId = $id";
		 $row =  $this->_obj->select($sql); 

		 $this->_iOrderId = $row[0]['iOrderId'];
		 $this->_iCustomerId = $row[0]['iCustomerId'];
		 $this->_vOrderNo = $row[0]['vOrderNo'];
		 $this->_vEmail = $row[0]['vEmail'];
		 $this->_fSubTotal = $row[0]['fSubTotal'];
		 $this->_fShipping = $row[0]['fShipping'];
		 $this->_fTax = $row[0]['fTax'];
		 $this->_vDiscount = $row[0]['vDiscount'];
		 $this->_fNetTotal = $row[0]['fNetTotal'];
		 $this->_eTitle = $row[0]['eTitle'];
		 $this->_vFirstName = $row[0]['vFirstName'];
		 $this->_vLastName = $row[0]['vLastName'];
		 $this->_vAddress = $row[0]['vAddress'];
		 $this->_vCity = $row[0]['vCity'];
		 $this->_vState = $row[0]['vState'];
		 $this->_vCountry = $row[0]['vCountry'];
		 $this->_vZip = $row[0]['vZip'];
		 $this->_vMobile = $row[0]['vMobile'];
		 $this->_vPhone = $row[0]['vPhone'];
		 $this->_eSTitle = $row[0]['eSTitle'];
		 $this->_vSFirstName = $row[0]['vSFirstName'];
		 $this->_vSLastName = $row[0]['vSLastName'];
		 $this->_vSCompany = $row[0]['vSCompany'];
		 $this->_vSAddress = $row[0]['vSAddress'];
		 $this->_vSCity = $row[0]['vSCity'];
		 $this->_vSState = $row[0]['vSState'];
		 $this->_vSCountry = $row[0]['vSCountry'];
		 $this->_vSZip = $row[0]['vSZip'];
		 $this->_vSPhone = $row[0]['vSPhone'];
		 $this->_vPaypalId = $row[0]['vPaypalId'];
		 $this->_ePaid = $row[0]['ePaid'];
		 $this->_iTransactionId = $row[0]['iTransactionId'];
		 $this->_vRemarks = $row[0]['vRemarks'];
		 $this->_tResponse = $row[0]['tResponse'];
		 $this->_vInstruction = $row[0]['vInstruction'];
		 $this->_vFromIP = $row[0]['vFromIP'];
		 $this->_vCouponCode = $row[0]['vCouponCode'];
		 $this->_dDate = $row[0]['dDate'];
		 $this->_ePaymentOption = $row[0]['ePaymentOption'];
		 $this->_iOrderStatusId = $row[0]['iOrderStatusId'];
		 $this->_vShowOrder = $row[0]['vShowOrder'];
		 $this->_dShippingDate = $row[0]['dShippingDate'];
		 $this->_vShipOption = $row[0]['vShipOption'];
		 $this->_fPackagePrice = $row[0]['fPackagePrice'];
		 $this->_vGiftCertCode = $row[0]['vGiftCertCode'];
		 $this->_fGiftCerDisc = $row[0]['fGiftCerDisc'];
		 $this->_vTrackingNo = $row[0]['vTrackingNo'];
		 $this->_eXmlStatus = $row[0]['eXmlStatus'];
	}


/**
*   @desc   DELETE
*/

	function delete($id)
	{
		 $sql = "DELETE FROM orders WHERE iOrderId = $id";
		 $result = $this->_obj->sql_query($sql);
	}

  function delete_orderdetail($id)
	{
		 $sql = "DELETE FROM order_detail WHERE iOrderId = $id";
		 $result = $this->_obj->sql_query($sql);
	}
/**
*   @desc   INSERT
*/

	function insert()
	{
		 $this->iOrderId = ""; // clear key for autoincrement

		 $sql = "INSERT INTO orders ( iCustomerId,vOrderNo,vEmail,fSubTotal,fShipping,fTax,vDiscount,fNetTotal,eTitle,vFirstName,vLastName,vAddress,vCity,vState,vCountry,vZip,vMobile,vPhone,eSTitle,vSFirstName,vSLastName,vSCompany,vSAddress,vSCity,vSState,vSCountry,vSZip,vSPhone,vPaypalId,ePaid,iTransactionId,vRemarks,tResponse,vInstruction,vFromIP,vCouponCode,dDate,ePaymentOption,iOrderStatusId,vShowOrder,dShippingDate,eXmlStatusvShipOption,fPackagePrice,vGiftCertCode,fGiftCerDisc) VALUES ( '".$this->_iCustomerId."','".$this->_vOrderNo."','".$this->_vEmail."','".$this->_fSubTotal."','".$this->_fShipping."','".$this->_fTax."','".$this->_vDiscount."','".$this->_fNetTotal."','".$this->_eTitle."','".$this->_vFirstName."','".$this->_vLastName."','".$this->_vAddress."','".$this->_vCity."','".$this->_vState."','".$this->_vCountry."','".$this->_vZip."','".$this->_vMobile."','".$this->_vPhone."','".$this->_eSTitle."','".$this->_vSFirstName."','".$this->_vSLastName."','".$this->_vSCompany."','".$this->_vSAddress."','".$this->_vSCity."','".$this->_vSState."','".$this->_vSCountry."','".$this->_vSZip."','".$this->_vSPhone."','".$this->_vPaypalId."','".$this->_ePaid."','".$this->_iTransactionId."','".$this->_vRemarks."','".$this->_tResponse."','".$this->_vInstruction."','".$this->_vFromIP."','".$this->_vCouponCode."','".$this->_dDate."','".$this->_ePaymentOption."','".$this->_iOrderStatusId."','".$this->_vShowOrder."','".$this->_dShippingDate."','".$this->_eXmlStatus."','".$this->_vShipOption."','".$this->_fPackagePrice."','".$this->_vGiftCertCode."','".$this->_fGiftCerDisc."')";
		 $result = $this->_obj->insert($sql);
		 return $result;
	}


/**
*   @desc   UPDATE
*/

	function update($id,$data)
	{

		 $sql = " UPDATE orders SET " ;
		 while (list($columns, $value) = each($data)) 
			{
				switch ((string)$value) 
				{
	         			case 'null':
	           			$query .= $columns .= ' = null, ';
	           			break;
	         			default:
	           			$query .= $columns . ' = \'' . $value . '\',';
	           		break;
	       		}
			}
		 $query=substr($query,0,-1);
		 $sql.= $query;
		 $sql.=" WHERE iOrderId = $id " ;
		 $result = $this->_obj->sql_query($sql);
		 return $result;

	}
  function updateStatusId($id,$data)
	{

		 $sql = " UPDATE orders SET " ;
		 while (list($columns, $value) = each($data)) 
			{
				switch ((string)$value) 
				{
	         			case 'null':
	           			$query .= $columns .= ' = null, ';
	           			break;
	         			default:
	           			$query .= $columns . ' = \'' . $value . '\',';
	           		break;
	       		}
			}
		 $query=substr($query,0,-1);
		 $sql.= $query;
		// $sql.=" WHERE iOrderDetailId IN(".@impode(',',$id) " ;
	
		$sql.=" WHERE iOrderId = $id " ;
				 $result = $this->_obj->sql_query($sql);
		 

	}
	function getTotalOrders($iCustomerId)
	{
		$sql="select count(iOrderId) as tot from orders where iCustomerId='".$iCustomerId."'"; 
		$result=$this->_obj->select($sql);
		return $result[0]['tot'];
	}
  function generateOrderno($orderid)
	{
		$orderno='FASH';
		/*
    switch (strlen(strval($orderid))) {
		case 1:
			$orderno.='200';
			break;
		case 2:
			$orderno.='20';
			break;
		case 3:
			$orderno.='2';
			break;
		}
		*/
		$orderno.=$orderid;
		return $orderno;
	}
  
 	function get_ordermail_content($iOrderId, $status,$exe_from="")
	{
		global $ADMIN_EMAIL,$SITE_NAME,$ORDER_EMAIL,$EMAIL_FROM_NAME, $EMAIL_CUST_SERVICE,$STORE_NAME,$STORE_ADDRESS,$STORE_CONTACT_NO,$generalobj;
		global $obj,$tconfig;
		ob_start();
		//echo $admin_path;exit;
		require_once($tconfig['tadmin_path']."/modules/orders/order_print.php");
		$mail_content=ob_get_clean();
		return $mail_content;
	}
	
	function sendMailToAllCust($iOrderId, $status,$exe_from="")
	{
		global $admin_url,$admin_path,$ADMIN_EMAIL,$SITE_NAME,$ORDER_EMAIL,$EMAIL_FROM_NAME, $EMAIL_CUST_SERVICE,$STORE_NAME,$STORE_ADDRESS,$STORE_CONTACT_NO,$site_url,$order_mail_content,$ORDER_EMAIL;
		global $obj;
	
		$vBody=$order_mail_content;
	
		$custsql = "select vEmail from orders where iOrderId ='".$iOrderId."'";
		
		$db_cust=$obj->MySQLSelect($custsql);
		$cust_email=$db_cust[0]["vEmail"];
		
		if($status == "Shipped")
			$subject= "Your order has been shipped";
		else
			$subject= $SITE_NAME." Order Confirmation";
		//echo $cust_email.'<hr>'.$subject.'<hr>'.$vBody.'<hr>'.$headers;echo '<hr>';  exit;
		$headers = "MIME-Version: 1.0\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\nContent-Transfer-Encoding: 8bit\nX-Priority: 1\nX-MSMail-Priority: High\n";
		$headers .= "From: ".$ADMIN_EMAIL."\n" . "Reply-To: ".$ADMIN_EMAIL."\n" . "X-Mailer: PHP/" .phpversion() . "\nX-originating-IP: " . $_SERVER['REMOTE_ADDR'] . "\n";
		$emailsend = @mail($cust_email, $subject, $vBody, $headers);
	}
	
	function sendMailToAdmin($iOrderId, $status,$exe_from="")
	{
		global $admin_url,$ADMIN_EMAIL,$SITE_NAME,$ORDER_EMAIL,$EMAIL_FROM_NAME, $EMAIL_CUST_SERVICE,$admin_path,$STORE_NAME,$STORE_ADDRESS,$STORE_CONTACT_NO,$site_url,$order_mail_content,$ORDER_EMAIL;
		global $obj;
	
		$vBody=$order_mail_content;
	
		$subject = "An order has been placed at ".$SITE_NAME;
	
		$headers = "MIME-Version: 1.0\n";
		$headers .= "Content-type: text/html; charset=iso-8859-2\nContent-Transfer-Encoding: 8bit\nX-Priority: 1\nX-MSMail-Priority: High\n";
		$headers .= "From: ".$ADMIN_EMAIL."\n" . "Reply-To: ".$ADMIN_EMAIL."\n" . "X-Mailer: PHP/" . phpversion() . "\nX-originating-IP: " . $_SERVER['REMOTE_ADDR'] . "\n";
		$emailsend = @mail($ADMIN_EMAIL, $subject, $vBody, $headers);
	}

  function get_orderdetail_listing_mail($iOrderId)
	{
		$orddetsql = "select * from order_details where iOrderId ='".$iOrderId."'";
		$db_rec = $this->_obj->select($orddetsql);
		//echo "<pre>";
		//print_r($db_rec);exit;
		return $db_rec;
	}
	
	function get_order_listing($iOrderId)
	{
		$ordsql = "select * from orders where iOrderId ='".$iOrderId."'";
		$db_rec = $this->_obj->select($ordsql);
		return $db_rec;
	}
	function get_tcomments($iOrderId,$vStatus)
	{
		$comment_sql = "select tComments from order_notification where iOrderId ='".$iOrderId."' and vStatus='".$vStatus."'";
		$db_rec = $this->_obj->select($comment_sql);
		return $db_rec;
	}
	function get_order_status($id)
	{
		$sql = "select vStatus from order_status where iOrderStatusId = '$id'";
		$db_rec = $this->_obj->MySQLSelect($sql);
		return $db_rec[0][vStatus];
	}
	
	function report_option($option,$dFDate,$dTDate,$ssql)
	{
		if(isset($option) && ($option!= ""))
		{
			$option = stripslashes($option);
			if($option=="QC")
			{
				$sql = "select count(iOrderId) as tot from orders  where iCustomerId = '0' AND dDate >= '".$dFDate." 00:00:00' and dDate <= '".$dTDate." 23:59:59'";
			}
			else
			{
				$sql = "select count(iOrderId) as tot from orders  where iCustomerId != '0' AND dDate >= '".$dFDate." 00:00:00' and dDate <= '".$dTDate." 23:59:59'";
			}
		}
		else
			$sql="select count(iOrderId) as tot  from orders where dDate >= '".$dFDate." 00:00:00' and dDate <= '".$dTDate." 23:59:59' $ssql";

		$result=$this->_obj->select($sql);
		return $result;
	}
	
	function report_optiondisplay($option,$dFDate,$dTDate,$sort,$var_limit,$ssql)
	{
		if(isset($option) && ($option!= ""))
		{
			$option = stripslashes($option);
			if($option=="QC")
			{
				$sql = "select *  from orders  where iCustomerId = '0' AND dDate >= '".$dFDate." 00:00:00' and dDate <= '".$dTDate." 23:59:59' order by $sort $var_limit";
			}
			else
			{
				$sql = "select *  from orders where iCustomerId != '0' AND dDate >= '".$dFDate." 00:00:00' and dDate <= '".$dTDate." 23:59:59' order by $sort $var_limit";
			}
		}
		else
		{
			$sql="select *  from orders where dDate >= '".$dFDate." 00:00:00' and dDate <= '".$dTDate." 23:59:59' $ssql order by $sort $var_limit";
		}
		$result=$this->_obj->select($sql);
		return $result;
	}
	function get_grand_total($dFDate,$dTDate,$ssql)
	{
    $sql="select sum(fNetTotal) as grandtotal  from orders where dDate >= '".$dFDate." 00:00:00' and dDate <= '".$dTDate." 23:59:59' $ssql";
    $data = $this->_obj->select($sql);
    return $data[0]['grandtotal'];
  }
	function return_order_status()
	{
		$sql = "select * from order_status order by iDispOrder";
		$data = $this->_obj->select($sql);
		return $data;
	}
	
	function orderstatus()
	{
		$sql = "select * from order_status order by iDispOrder";
		$result=$this->_obj->select($sql);
		return $result;
	}
	
	function return_total_order_history_query($iCustomerId)
	{
		$sql_ord = "SELECT o.iOrderId, o.vOrderNo, os.vStatus, o.dDate, o.fSubTotal, o.fNetTotal from orders o LEFT JOIN order_status os ON (o.iOrderStatusId = os.iOrderStatusId) WHERE o.iCustomerId = '".$iCustomerId."' order by o.iOrderId desc";
		return $sql_ord;
	}
	function return_total_savedorder_history_query($iCustomerId)
	{
		$sql_ord = "SELECT o.iOrderId, o.vOrderNo, os.vStatus, o.dDate, o.fSubTotal, o.fNetTotal from orders o LEFT JOIN order_status os ON (o.iOrderStatusId = os.iOrderStatusId) WHERE o.iCustomerId = '".$iCustomerId."' AND o.eSaveOrder = 'Yes' order by o.iOrderId desc";
		echo $sql_ord;exit;
		return $sql_ord;
	}
	
	function return_total_order_history($iCustomerId = "", $var_limit = "")
	{
		if($iCustomerId=="")
		{
			$sql_ord = "SELECT o.iOrderId, o.vOrderNo, os.vStatus, o.dDate, o.ePaymentOption, o.fSubTotal, o.fNetTotal from orders o LEFT JOIN order_status os ON (o.iOrderStatusId = os.iOrderStatusId) order by o.iOrderId desc";
		}
		else
		{
			$sql_ord = "SELECT o.iOrderId, o.vOrderNo, os.vStatus, o.dDate, o.ePaymentOption, o.fSubTotal, o.fNetTotal from orders o LEFT JOIN order_status os ON (o.iOrderStatusId = os.iOrderStatusId) WHERE o.iCustomerId = $iCustomerId order by o.iOrderId desc $var_limit";
		}
	
  	$data = $this->_obj->select($sql_ord);
		return $data;
	}
	
	function getcustomer_orderdetails($val)
	{
		$sql_order = "select iCustomerId,vCity from orders where iOrderId='".$val."'";
		$db_order = $this->_obj->select($sql_order);
		if($db_order[0]['iCustomerId'] == $_SESSION['sess_hco_iCustomerId'])
			$get_id = $val;
		return $get_id;
	}
	
	function order_detail($val)
	{
		$sql_order_detail = "select * from orders where iOrderId = '".$val."' ";
		$data = $this->_obj->select($sql_order_detail);
		return $data;
	}
	
	function getstate_name($field , $table ,$whereclouse, $code, $countrytext, $countrycode)
	{
		$sqlsta = "select ".$field." from ".$table." where ".$whereclouse."='".$code."' and ".$countrytext."='".$countrycode."'";
		$db_sta=$this->_obj->select($sqlsta);
		$state = $db_sta[0]["$field"];
		return $state;
	}
	function getcountry($field , $table ,$whereclouse, $code)
	{
		$sqlsta = "select ".$field." from ".$table."  where ".$whereclouse."='".$code."'";
		$db_sta=$this->_obj->select($sqlsta);
		$state = $db_sta[0]["$field"];
		return $state;
	}
	
	function product_details($val)
	{
		$sql = "select * from order_details where iOrderId = $val";
		$data = $this->_obj->select($sql);
		return $data;
	}
	
	function get_currentstock_fromid($iId)
	{
		$sql = "SELECT iCurrentStock FROM products WHERE eStatus='Active' and iProductId = '".$iId."'";
		$cat_info = $this->_obj->select($sql); 	
		return $cat_info[0]['iCurrentStock'];
	}
	
	function getStatusName($status_id)
	{
		$sql = "select vStatus from order_status where iOrderStatusId = '$status_id'";
		$db_rec = $this->_obj->select($sql);
		return $db_rec[0][vStatus];
	}
	function totrecordlist($id)
	{
		$sql="select count(iOrderId) as tot from orders where iCustomerId = $id";
		$row = $this->_obj->select($sql); 
		return $row;
	}
	function SelectOrder()
	{
		$sql="select iOrderId,vOrderNo from orders order by iOrderId desc";
		$db_orderno=$this->_obj->select($sql);
		return 	$db_orderno;
	}
	function Order_notification($iOrderId)
	{
		$sql = "select * from order_notification where iOrderId='$iOrderId' order by dDate desc";
		$db_rec = $this->_obj->select($sql);
		return $db_rec;
	}
	function orderlisting($iId) 
	{
		$db_query = "select od.*,os.vStatus from order_details od left join  order_status os on od.iStatusId = os.iOrderStatusId LEFT JOIN products p on od.iProductId = p.iProductId where od.iOrderId = '".$iId."' order by od.vProductName";
		//echo $db_query;exit; 
		$db_res = $this->_obj->select($db_query);
		//echo "<pre>";
		//print_r($db_res);exit;
		return $db_res;
	}
	function getiProAttrId($attr,$prodid)
	{
    $sql = "SELECT iProAttrId FROM product_attributes WHERE vOptions = '".$attr."' AND vAttribute='Color' AND iProductId='".$prodid."'";        
    $iProAttrId = $this->_obj->select($sql);
    return $iProAttrId[0]['iProAttrId'];
  }
  function getSizeiProAttrId($attr,$prodid)
	{
    $sql = "SELECT iProAttrId FROM product_attributes WHERE vOptions = '".$attr."' AND vAttribute='Size' AND iProductId='".$prodid."'";        
    $iProAttrId = $this->_obj->select($sql);
    return $iProAttrId[0]['iProAttrId'];
  }
  
  
  function getSmallImage($id,$prodid)
  {
    $sql = "SELECT vSmallImage FROM product_images WHERE iProAttrId='".$id."' AND eMain='Yes' AND iProductId = '".$prodid."'";
    $image = $this->_obj->select($sql);
    return $image[0]['vSmallImage'];
  }
  
	function getorder_xml($date)
	{
		$sql = "SELECT iOrderId FROM orders WHERE eXmlStatus NOT LIKE 'SUCCESS%' AND DATE(dDate) = '".$date."' and iOrderStatusId = '2'";
		$db_sql = $this->_obj->select($sql);
		
		return $db_sql;
	}
  
	function pay_paypal_form($amount,$iMemberId)
	{
		extract ($GLOBALS);
	
		$returnURL =$tconfig['tsite_url']."index.php?file=ec-process&x_response_code=1&iOrderId=".$_SESSION['sess_iOrderId'];
	
		$cancleURL =$tconfig['tsite_url']."index.php?file=ec-process&x_response_code=0&iOrderId=".$_SESSION['sess_iOrderId'];
	
		$logoURL = $tconfig['tsite_images']."paypal_logo.gif";
		$notifyURL = $tconfig['tsite_url']."index.php?file=ec-paypal_ipn";
	
		if($PAYPAL_TRANSACTION_MODE == "Live") {
			$paymentUrl	=	"https://www.paypal.com/cgi-bin/webscr";
		}else {
			$paymentUrl	=	"https://www.sandbox.paypal.com/cgi-bin/webscr";
		}
	
		$frmPaypal = '
		<FORM ACTION="'.$paymentUrl.'" METHOD="post" name="frmPaypal">
		<!-- <INPUT TYPE="hidden" NAME="cmd" VALUE="_xclick-subscriptions"> -->
		<INPUT TYPE="hidden" NAME="cmd" VALUE="_xclick">
		<INPUT TYPE="hidden" NAME="business" VALUE="'.$PAYPAL_ID.'">
		<INPUT TYPE="hidden" NAME="item_name" VALUE="'.$COMPANY_NAME.' Order">
		<INPUT TYPE="hidden" NAME="item_number" VALUE="'.$_SESSION['sess_iOrderId'].'">
		<input type="hidden" name="no_shipping" value="1">
		<input type="hidden" name="return" value="'.$returnURL.'">
		<input type="hidden" name="cancel_return" value="'.$cancleURL.'">
		<input type="Hidden" name="custom" value="'.$_SESSION['sess_iOrderId'].'">
		<input type="Hidden" name="orderType" value="New">
		<input type="hidden" name="amount" value="'.number_format($amount,2).'">
		<input type="Hidden" name="currency_code" value="GBP">
		<input type="Hidden" name="cpp_header_image" value="'.$logoURL.'">
		<input type="hidden" name="notify_url" value="'.$notifyURL.'">
	  <!-- <INPUT type="Submit" name="sub_mit" value="Submit">     -->
		<script>
			document.frmPaypal.submit();
		</script>
			';
		return $frmPaypal;
	}
	
	function UpateCurrentStock($id,$qty)
    {
   	  global $obj;

       $aid = explode('_',$id);
     
    	//$sql="update product_sizes set iStock=iStock-$qty where iProductId='".$aid[0]."' and iAttributeId = '".$aid[1]."'";
    	 $sql = "update products set iCurrentStock = iCurrentStock-$qty where iProductId='".$aid[0]."'";
      $rec=$obj->sql_query($sql);
    	
    }
    
    
/**
*   @desc   Destroy Object
*/

	function destroyObj($obj)
	{
		 unset($obj) ;
	}


/********************************************************************************************************************/
	function customer_order_history($iCustomerId)
	{
		$sql_ord = "SELECT o.iOrderId, o.vOrderNo, os.vStatus, o.dDate, o.fSubTotal, o.fNetTotal,od.iProductId,od.vProductName,od.iQty,od.fTotalPrice, od.vAttribute,p.vImage,p.iProductId from products as p, order_details as od, orders o LEFT JOIN order_status os ON (o.iOrderStatusId = os.iOrderStatusId) WHERE o.iCustomerId = '".$iCustomerId."' AND o.iOrderId = od.iOrderId AND p.iProductId = od.iProductId order by o.iOrderId desc";
		$rec=$this->_obj->sql_query($sql_ord);
		for($i=0;$i<count($rec);$i++)
		{
			$sql = "select c.iCategoryId, c.vCategory from category as c, product_category as pc where c.eStatus = 'Active' AND c.iCategoryId = pc.iCategoryId AND iProductId = '".$rec[$i]['iProductId']."'";
	        $db_sql = $this->_obj->select($sql); 
			$rec[$i]['iCategoryId'] = $db_sql[0]['iCategoryId'];
			$rec[$i]['vCategory'] = $db_sql[0]['vCategory'];		
		}
		return $rec;
	}
} 

?>
