<?php
class Products
{

	function __construct()
	{
		global $obj;
		$this->_obj = $obj;
	}

	function __destruct()
	{
		unset($this->_obj);
	}

  
  function get_productdetail($id='',$iAttrOptionId='',$ssql='')
  {
  		$sql = "SELECT distinct p.iProductId, p.eType, p.vProductName, p.tProductDetail, p.vSKU, p.eDiscount,p.eShowPrice, p.fDiscount, p.vTitle, p.vMetaDescription, p.tMetaKeyword, pi.vImage as vImage, pi.iProImageId, pi.iAttrOptionId,p.eShopping from products p LEFT JOIN product_images pi ON (pi.iProductId = $id) LEFT JOIN product_attributes pa ON (pa.iProductId = $id) WHERE pa.eStatus ='Active' AND p.eStatus ='Active' $ssql AND p.iProductId = '".$id."' AND pi.iAttrOptionId = '".$iAttrOptionId."' order by pa.eDefault ASC";
		$data = $this->_obj->select($sql);
		
		return $data;
  }
  
  function get_productdetail_woi($id='',$iAttrOptionId='')
  {
  		$sql = "SELECT distinct p.iProductId, p.vProductName, p.tProductDetail, p.vSKU, p.eDiscount,p.eShowPrice, p.fDiscount as vImage,pa.iAttrOptionId4 as iAttrOptionId from products p LEFT JOIN product_attributes pa ON (pa.iProductId = $id) WHERE pa.eStatus ='Active' AND p.eStatus ='Active' AND p.iProductId = '".$id."' AND pa.iAttrOptionId4 = '".$iAttrOptionId."' order by pa.eDefault ASC";
		$data = $this->_obj->select($sql);
		
		return $data;
  }
	
	function getenginetypes(){
		global $obj;
		$sql = "SELECT DISTINCT vEngineType FROM equipment WHERE eStatus='Active' ORDER BY vEngineType";
		$db_rec = $obj->MySQLSelect($sql);
		return $db_rec;
	}

	function gettransmissiontypes(){
		global $obj;
		$sql = "SELECT DISTINCT vTransmissionType FROM equipment WHERE eStatus='Active' ORDER BY vTransmissionType";
		$db_rec = $obj->MySQLSelect($sql);
		return $db_rec;
	}
	
	function getlisting($sql) {
		global $obj, $iCategoryId;
		$db_rec = $obj->MySQLSelect($sql);
		$db_rec_temp = $db_rec;
		for($i=0;$i<count($db_rec);$i++) {
			$db_rec[$i][iEquipmentId] = $db_rec_temp[$i][iEquipmentId];
			$db_rec[$i][product_price] = $db_rec_temp[$i][fPrice];
			$db_rec[$i][fPrice] = $this->setPriceFormat($db_rec_temp[$i][fPrice]);
			$db_rec[$i][vEquipmentName] = stripslashes($db_rec_temp[$i]['vEquipmentName']);
			if($db_rec_temp[$i][tDetails] != "")
			{
				$db_rec[$i][tDetails] = stripslashes($db_rec_temp[$i][tDetails]);
			}
	
			/* get gallery images to show in listing*/
			/*
			if($db_rec_temp[$i][eShowGallery] == "Yes"){
				$gallery = $this->getProductGallery($db_rec[$i][iProductId],$db_rec[$i][vImage],$db_rec[$i][vProductName]);
				$db_rec[$i]['gallery']=$gallery;
			}
			*/
	
			$db_rec[$i][url] = $this->getProductdetailUrl($db_rec[$i][vEquipmentName],$db_rec[$i][vEquipmentCategory],$db_rec[$i][iEquipmentId],$db_rec[$i][iEquipmentCategoryId]);
		}
		$this->product_arr = $db_rec;
		return $db_rec;
	}

	function setPhoto($size,$image_url,$image_path,$image_magic,$img_height="",$img_width="") {
		$product_rec = $this->product_arr;
		$product_rec_temp = $this->product_arr;
		for($i=0;$i<count($product_rec);$i++) {

			if($image_magic == "1")	{
				if(@file_exists($image_path.$size."_".$product_rec[$i]["vImage"]) && $product_rec[$i]["vImage"]!="")	{
					$product_rec[$i][vImage] =  $image_url.$size."_".$product_rec_temp[$i][vImage];
				}
				else {
					$product_rec[$i][vImage] =  $image_url.$size."_noimage.gif";
				}
			}
			else {
					if(@file_exists($image_path.$product_rec[$i]["vImage"]) && $product_rec[$i]["vImage"]!="")	{
						$imagehw_fea = @GetImageSize($image_path.$product_rec[$i]['vImage']);
						$act_width_fea = $imagehw_fea[0];
						$act_height_fea = $imagehw_fea[1];
						$size_fea=getImageWithRatio($act_width_fea, $act_height_fea,$img_width,$img_height);
						$product_rec[$i][width]=$size_fea[0];
						$product_rec[$i][height]=$size_fea[1];
					}
					else {
						$product_rec[$i][vImage] =  $image_url.$size."_noimage.gif";
					}
				}
		}
		return $product_rec;
	}

	function setCurrency($symbol)
	{
		$this->currency_symbol = $symbol;
	}

	function setPriceFormat($price)
	{
		return $this->currency_symbol." ".number_format($price,2,'.',',');
	}

	function setPaging($count_sql,$nstart,$start)
	{
		global $smarty,$sorton,$sort;
		extract($GLOBALS);
		$rec_count = $obj->MySQLSelect($count_sql);
		$num_totrec = $rec_count[0]['tot'];
		require_once(TPATH_CLASS_GEN.DS."site_paging.php");

		if(isset($sorton))
		{
			switch ($sorton)
			{
				case "1":
					$sort = "pa.fPrice ASC";
					break;
				case "2":
					$sort = "pa.fPrice DESC";
					break;
				case "3":
					$sort = "p.vProductName ASC";
					break;
				case "4":
					$sort = "p.vProductName DESC";
					break;
			}
		}
		else 
		{
			$sorton = "3";
			$sort ="p.vEquipmentName".$_SESSION['sess_lang_master']." ASC";
		}

		if(!isset($start)) $start = 1;
		$num_limit = ($start-1) * $rec_limit;
		$startrec = $num_limit;
		$lastrec = $startrec + $rec_limit;
		$startrec = $startrec + 1;
		if($lastrec > $num_totrec) $lastrec = $num_totrec;
		$smarty->assign("sort_img",$sort_img);
		$smarty->assign("var_pgs",$var_pgs.$var_filter);
		if($num_totrec > 0)
		{
			$smarty->assign('pagebreak',LBL_SHOWING." <b>".$startrec."</b> - <b>".$lastrec."</b> ".LBL_RECORDSOF." <b>".$num_totrec."</b>");
			$smarty->assign('page_link',"<div class='Filta_By5'>".LBL_PAGES." : </div> ".$page_link."");
		}
		else
		{
		//$smarty->assign('page_break',"There are no products in this category.");
		}
		return $var_limit;
	}

	function getProductdetailUrl($name,$cat_name,$productid,$iCategoryId) {
        global $generalobj;
		$str = "productdetail/".$generalobj->replace_content($cat_name)."/".$generalobj->replace_content($name)."/".$productid."/".$iCategoryId;
		return $str;
	}

	function getProductGallery($id,$first_image,$first_image_title)
	{
		global $obj,$product_path,$product_url,$product_gallery,$javaScriptArr,$k;
		$sql = "select iProductId, vImage, vTitle from product_gallery where iProductId = '".$id."' and eStatus = 'Active'";
		$gallery_record = $obj->select($sql);
		if(!isset($k)){
		$k=0;
		}
		$product_gallery = array();

		/* For showing first image of gallery (product's main image itself) */
	if(@file_exists($product_path.$id."/5_".$first_image) && $first_image!=""){
			$product_gallery[0]["vImage"] = $product_url.$id."/5_".$first_image;
			$product_gallery[0]["medium_image_url"] = $product_url.$id."/1_".$first_image;
			$product_gallery[0]["vTitle"] = $first_image_title;
			//$product_gallery[0]["large_image_url"] = $product_url.$id."/3_".$first_image;
			//$product_gallery[0]["resize"] = "0";
			$start_image = 1;
		}else{
			$start_image = 0;
		}

		for($i=0;$i<count($gallery_record);$i++) 
		{
			$j = $i+$start_image;
	
	if(@file_exists($product_path.$gallery_record[$i]["iProductId"]."/5_".$gallery_record[$i]["vImage"]))
			{
				$product_gallery[$j]["vImage"] = $product_url.$gallery_record[$i][iProductId]."/5_".$gallery_record[$i][vImage];
				$product_gallery[$j]["medium_image_url"] = $product_url.$gallery_record[$i][iProductId]."/1_".$gallery_record[$i][vImage];
				//$product_gallery[$i]["large_image_url"] = $product_url.$gallery_record[$i][iProductId]."/3_".$gallery_record[$i][vImage];
				//$product_gallery[$i]["resize"] = "0";
				$product_gallery[$j]["vTitle"] = $gallery_record[$i]['vTitle'];
				$javaScriptArr .= "messages[$k] = new Array('".$product_gallery[$j]["medium_image_url"]."');";
		$k++;
			}
		}

		return $product_gallery;
	}

	function destroyObj($obj)
	{
		 unset($obj) ;
	}
} 

?>
