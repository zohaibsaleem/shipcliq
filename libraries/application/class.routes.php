<?
class Routes
{
	var $file;
	var $mode;

	function __construct()
	{
		if(isset($_REQUEST['file']) && $_REQUEST['file']!="")
		{

			$this->file = $_REQUEST['file'];
			
			if($_REQUEST['mode'] == 'add' || $_REQUEST['mode'] == 'edit')
			{             
				$this->mode = $_REQUEST['mode'];             
			}
			else if($_REQUEST['mode'] == 'view')
			{
				$this->mode = "view";                     
			}
			else
			{
				$this->mode = "";
			}
				
			$sc = explode("-",$this->file);
			switch(	$sc[0] )
			{
				case "ad":
						$module = "administrator";
						break;
				case "au":
						$module = "authentication";
						break;
				case "to":
						$module = "tools";
						break;
				case "u":
						$module = "utility";
						break;
				case "pg":
						$module = "pages";
						break;
				case "m":
						$module = "member";
						break;
				case "l":
						$module = "localization";
						break;
				case "s":
						$module = "scholar_mgt";
						break;
				case "r":
						$module = "report";
						break;
				case "bo":
						$module = "booking";
						break; 
        case "ri":
						$module = "rides";
						break;    						
				default:
						$module = "dashboard";
						$this->module = "dashboard";
						$this->script = "dashboard";
						$this->mode = "view";
						break;
			}
            
			$this->module = $module;
			$this->script = $sc[1];
		}
		else
		{
				if(isset($_SESSION["sess_iAdminId"]) && $_SESSION["sess_iAdminId"] !="")
				{
					$this->module = "dashboard";
					$this->script = "dashboard";
					$this->mode = "view";
				}
				else
				{
					$this->module = "authentication";
					$this->script = "login";
					$this->mode = "au";
				}
		}

	}

	function GetModule()
	{
		return $this->module;
	}

	function GetMode()
	{
		return $this->mode;
	}

	function GetScript()
	{
		return $this->script;
	}

	

}
?>
