<?php
  Class Events
  {
    function upcoming_events(){
      global $obj, $tconfig; 
      
      $curr = date("Y-m-d H:i:s");
      $sql = "SELECT iEventId, vTitle, vImage FROM event WHERE eStatus = 'Active' ORDER BY RAND() LIMIT 0,3";
      $db_events = $obj->MySQLSelect($sql);
      
      if(count($db_events) > 0){
        for($i=0;$i<count($db_events);$i++){    
          if(is_file($tconfig["tsite_upload_images_events_path"].$db_events[$i]['iEventId']."/1_".$db_events[$i]['vImage']))
        	{
        		$db_events[$i]['img_url'] = $tconfig["tsite_upload_images_events"].$db_events[$i]['iEventId']."/1_".$db_events[$i]['vImage'];
        	} 
        	else
        	{
        		$db_events[$i]['img_url'] = $tconfig['tsite_images'].'277x143.jpg';
        	}
        	$db_events[$i]['start_time'] = strtotime($db_events[$k]['dStartDate']);
        }
      }
      return $db_events;
    }
    
    function all_events($ssql, $ordby){
      global $obj, $tconfig;  
      
      $curr = date("Y-m-d H:i:s");
      $sql = "SELECT event.iEventId, event.vTitle, event.tDetails, event.vCity, event.vImage, organization.vOrganizationName, state.vState                                 
              FROM event                
              LEFT JOIN organization ON organization.iOrganizationId = event.iOrganizationId                
              LEFT JOIN state ON state.vStateCode = event.vState AND state.vCountryCode = event.vCountry 
              LEFT JOIN events_category ON events_category.iEventId = event.iEventId 
              LEFT JOIN category ON category.iCategoryId = events_category.iCategoryId 
              LEFT JOIN events_suitabletype ON events_suitabletype.iEventId = event.iEventId 
              LEFT JOIN suitable_type ON suitable_type.iSuitableTypeId = events_suitabletype.iSuitableTypeId               
              WHERE event.eStatus = 'Active' $ssql GROUP BY iEventId $ordby";
      $db_events = $obj->MySQLSelect($sql);  
      
      return $db_events;         
    }
    
    function events_list($ssql, $ordby, $var_limit){
      global $obj, $tconfig;  
      
      $curr = date("Y-m-d H:i:s");
      $sql = "SELECT event.iEventId, event.vTitle, event.tDetails, event.vCity, event.vImage, organization.vOrganizationName, state.vState                                 
              FROM event                
              LEFT JOIN organization ON organization.iOrganizationId = event.iOrganizationId                
              LEFT JOIN state ON state.vStateCode = event.vState AND state.vCountryCode = event.vCountry 
              LEFT JOIN events_category ON events_category.iEventId = event.iEventId 
              LEFT JOIN category ON category.iCategoryId = events_category.iCategoryId 
              LEFT JOIN events_suitabletype ON events_suitabletype.iEventId = event.iEventId 
              LEFT JOIN suitable_type ON suitable_type.iSuitableTypeId = events_suitabletype.iSuitableTypeId               
              WHERE event.eStatus = 'Active' $ssql GROUP BY iEventId $ordby $var_limit";
      $db_events = $obj->MySQLSelect($sql); 
      
      if(count($db_events) > 0){
        for($i=0;$i<count($db_events);$i++){    
          if(is_file($tconfig["tsite_upload_images_events_path"].$db_events[$i]['iEventId']."/4_".$db_events[$i]['vImage']))
        	{
        		$db_events[$i]['img_url'] = $tconfig["tsite_upload_images_events"].$db_events[$i]['iEventId']."/4_".$db_events[$i]['vImage'];
        	} 
        	else
        	{
        		$db_events[$i]['img_url'] = $tconfig['tsite_images'].'129x103.jpg';
        	}
        }
      }   
        
      return $db_events;          
    }
    
    function event_catagory(){
      global $obj, $tconfig;
      
      $sql = "SELECT iCategoryId, vCategory FROM category WHERE eStatus = 'Active' ORDER BY vCategory ASC";
      $db_category = $obj->MySQLSelect($sql);
      return $db_category;
    }
    
    function event_suitable_for(){
      global $obj, $tconfig;
      
      $sql = "SELECT iSuitableTypeId, vSuitableTitle FROM suitable_type WHERE eStatus = 'Active' ORDER BY vSuitableTitle ASC";
      $db_suitabletype = $obj->MySQLSelect($sql);
      return $db_suitabletype;
    }
    
    function event_check($iEventId){
      global $obj, $tconfig;
      
      $curr = date("Y-m-d H:i:s");
      $sql = "SELECT vTitle, eStatus FROM event WHERE event.eStatus = 'Active' AND iEventId = '".$iEventId."' ";
      $db_event = $obj->MySQLSelect($sql);
      
      if(count($db_event) == 0){ 
        header("Location:".$tconfig["tsite_url"]."events");       
      }
    }
    
    function event_common_details($iEventId){
      global $obj, $tconfig;
      
      $curr = date("Y-m-d H:i:s");
      $sql = "SELECT event.iEventId, event.iOrganizationId , event.vTitle, event.tDetails, event.tRequirements, event.dStartDate, event.dEndDate, event.vStartTime, event.vEndTime, event.vAddress, event.vCity, event.vImage, event.eEventType, organization.vOrganizationName,  
              (SELECT state.vState FROM state WHERE state.vStateCode = event.vState AND state.vCountryCode = event.vCountry) as vState,  
              (SELECT country.vCountry FROM country WHERE country.vCountryCode = event.vCountry) as vCountry                   
              FROM event               
              LEFT JOIN organization ON organization.iOrganizationId = event.iOrganizationId                
              WHERE event.eStatus = 'Active' AND event.iEventId = '".$iEventId."'";
      $db_events = $obj->MySQLSelect($sql);
      
      if(count($db_events) > 0){
        for($i=0;$i<count($db_events);$i++){    
          if(is_file($tconfig["tsite_upload_images_events_path"].$db_events[$i]['iEventId']."/3_".$db_events[$i]['vImage']))
        	{
        		$db_events[$i]['img_url'] = $tconfig["tsite_upload_images_events"].$db_events[$i]['iEventId']."/3_".$db_events[$i]['vImage'];
        	} 
        	else
        	{
        		$db_events[$i]['img_url'] = '';
        	}
        }
      }
      return $db_events; 
    }
    
    function event_skills_list($iEventId){
      global $obj, $tconfig;
      
      $sql = "SELECT skills.vTitle 
              FROM event_skills 
              LEFT JOIN skills ON skills.iSkillId = event_skills.iSkillId  
              WHERE event_skills.iEventId = '".$iEventId."'";
      $db_skills = $obj->MySQLSelect($sql);
      
      return $db_skills;
    }
    
    function event_suitable_for_list($iEventId){
      global $obj, $tconfig;
      
      $sql = "SELECT suitable_type.vSuitableTitle 
              FROM events_suitabletype 
              LEFT JOIN suitable_type ON suitable_type.iSuitableTypeId = events_suitabletype.iSuitableTypeId  
              WHERE events_suitabletype.iEventId = '".$iEventId."'";
      $db_suitable = $obj->MySQLSelect($sql);
      
      return $db_suitable;
    }
    
    function event_category_list($iEventId){
      global $obj, $tconfig;
      
      $sql = "SELECT events_category.iCategoryId, category.vCategory 
              FROM events_category 
              LEFT JOIN category ON category.iCategoryId = events_category.iCategoryId  
              WHERE events_category.iEventId = '".$iEventId."'";
      $db_categories = $obj->MySQLSelect($sql);
      
      return $db_categories;
    }
    
    function event_students_details($iEventId){
      global $obj, $tconfig;
      
      $sql = "SELECT event_volunteer.iMemberId, member.vFirstName, member.vLastName, member.vImage, member.vCity, state.vState 
              FROM event_volunteer 
              LEFT JOIN member ON member.iMemberId = event_volunteer.iMemberId 
              LEFT JOIN state ON state.vStateCode = member.vState 
              WHERE event_volunteer.eStatus = 'Accepted' AND state.vCountryCode = member.vCountry AND event_volunteer.iEventId = '".$iEventId."'";
      $db_students = $obj->MySQLSelect($sql);
      
      if(count($db_students) > 0){
        for($i=0;$i<count($db_students);$i++){    
          if(is_file($tconfig["tsite_upload_images_member_path"].$db_students[$i]['iMemberId']."/4_".$db_students[$i]['vImage']))
        	{
        		$db_students[$i]['img_url'] = $tconfig["tsite_upload_images_member"].$db_students[$i]['iMemberId']."/4_".$db_students[$i]['vImage'];
        	} 
        	else
        	{
        		$db_students[$i]['img_url'] = '';
        	}
        }
      }
      
      return $db_students;
    }
    
    function event_attending_students($iEventId){
      global $obj, $tconfig;
      
      $sql = "SELECT count(iMemberId) as tot_students FROM event_volunteer WHERE iEventId = '".$iEventId."' AND eStatus = 'Accepted'";
      $db_students_tot = $obj->MySQLSelect($sql);
      
      return $db_students_tot[0]['tot_students'];
    }
    
    function organization_reviews($iOrganizationId){
      global $obj, $tconfig;
      
      $sql = "SELECT COUNT(iOrganizationReview) as iOrganizationReview FROM organization_reviews WHERE iOrganizationId = '".$iOrganizationId."' AND eStatus = 'Active'";
      $db_reviews_tot = $obj->MySQLSelect($sql);
      
      return $db_reviews_tot[0]['iOrganizationReview'];
    }
    
    function organization_retings($iOrganizationId){
      global $obj, $tconfig;
      
      $sql = "SELECT iRate FROM organization WHERE iOrganizationId = '".$iOrganizationId."'" ;
      $db_rate_tot = $obj->MySQLSelect($sql);
      
      return $db_rate_tot[0]['iRate'];
    }
    
    function event_skills(){ 
      global $obj, $tconfig;
      
      $sql = "SELECT iSkillId, vTitle FROM skills WHERE eStatus = 'Active'";
      $db_skills = $obj->MySQLSelect($sql); 
      
      return $db_skills;
    } 
    
    function good_matches(){
      global $obj, $tconfig;
      
      $sql = "SELECT iSuitableTypeId, vSuitableTitle FROM suitable_type WHERE eStatus = 'Active'";
      $db_suitables = $obj->MySQLSelect($sql);
      
      return $db_suitables;  
    }
    
    function event_edit_details($iEventId){
      global $obj, $tconfig;
      
      $sql = "SELECT * FROM event WHERE iEventId = '".$iEventId."'";
      $db_event = $obj->MySQLSelect($sql);
      
      if(is_file($tconfig["tsite_upload_images_events_path"].$iEventId."/1_".$db_event[0]['vImage']))
    	{
    		$db_event[0]['img_url'] = $tconfig["tsite_upload_images_events"].$iEventId."/1_".$db_event[0]['vImage'];
    	} 
    	else
    	{
    		$db_event[0]['img_url'] = '';
    	}
      
      return $db_event; 
    }
    
    function event_skills_list_edit($iEventId){
      global $obj, $tconfig;
      
      $sql = "SELECT iSkillId 
              FROM event_skills                
              WHERE iEventId = '".$iEventId."'";
      $db_skills = $obj->MySQLSelect($sql);
      
      return $db_skills;
    }
    
    function event_suitable_for_list_edit($iEventId){
      global $obj, $tconfig;
      
      $sql = "SELECT iSuitableTypeId 
              FROM events_suitabletype               
              WHERE iEventId = '".$iEventId."'";
      $db_suitable = $obj->MySQLSelect($sql);
      
      return $db_suitable;
    }
    
    function event_category_list_edit($iEventId){
      global $obj, $tconfig;
      
      $sql = "SELECT iCategoryId 
              FROM events_category                
              WHERE iEventId = '".$iEventId."'";
      $db_categories = $obj->MySQLSelect($sql);
      
      return $db_categories;
    }
    
    function check_event($iEventId, $iOrganizationId){
      global $obj, $tconfig;
      
      $sql = "SELECT iEventId FROM event WHERE iEventId = '".$iEventId."' AND iOrganizationId = '".$iOrganizationId."'";
      $db_event = $obj->MySQLSelect($sql);
      
      if(count($db_event) > 0){
        return 1;
      }else{
        header("Location:".$tconfig["tsite_url"]);
        exit;
      }
    }
  }
?> 