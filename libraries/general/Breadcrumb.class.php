<?php
/*
  Date Modified : 6-1-2007
  Modified by : Chirag Darji
  Description : Use to generate breadcrumtrail on every page
*/

  class breadcrumb {
    var $_trail;
    var $site_title=null;
    function breadcrumb() {
	  global $SITE_TITLE;
      $this->reset();
	  $this->site_title = $SITE_TITLE;
    }

    function reset() {
      $this->_trail = array();
    }

    function add($title, $link = '') {
      $this->_trail[] = array('title' => $title, 'link' => $link);
    }

    function trail($separator = ' - ') {
	  global $smarty;
      $trail_string = '';

      for ($i=0, $n=sizeof($this->_trail); $i<$n; $i++) {
        if (isset($this->_trail[$i]['link']) && $this->_trail[$i]['link'] != "") {
          $trail_string .= '<span><a  href="' . $this->_trail[$i]['link'] . '" title="'.$this->_trail[$i]['title'].'">' . $this->_trail[$i]['title'] . '</a></span>';
        } else {
          $trail_string .= "<span>".$this->_trail[$i]['title']."</span>";
		  $get_title = $this->_trail[$i]['title'];
        }
        if (($i+1) < $n) $trail_string .= $separator;
      }
	  if($get_title != "")
	  {
	  	$smarty->assign("SITE_TITLE",$get_title." - ".$this->site_title);
	  	$smarty->assign("module_title",$get_title);
	  }
	  else
	  {
	  	$smarty->assign("SITE_TITLE",$this->site_title);
	  }
      return $trail_string;
    }
  }
?>
