<?php
class Cart {
	var $total = 0;
	var $shipping_total = 0;
	var $weight_total = 0;
	var $taxable_total = 0;
	var $itemcount = 0;
	var $items = array();
	var $product_id = array();
	var $itemprices = array();
	var $itemqtys = array();
	var $iteminfo = array();
	var $itemattribute = array();  
	var $producttype;

	function Cart() {} // constructor function
	

	function get_contents()
	{
		
  
    // gets cart contents
		$items = array();
		foreach($this->items as $tmp_item)
		{
			$item = FALSE;

			$item['id'] = $tmp_item;
            $item['qty'] = $this->itemqtys[$tmp_item];
			$item['price'] = $this->itemprices[$tmp_item];
			$item['info'] = $this->iteminfo[$tmp_item];
			$item['product_id'] = $this->product_id[$tmp_item];
			$item['attribute_id'] = $this->itemattribute[$tmp_item];
      $item['subtotal'] = $item['qty'] * $item['price'];
      $items[] = $item;
		}
		return $items;
	}

	function get_all_contents()
	{
		
    global $obj,$tconfig,$generalobj;
		$items = array();
		$this->taxable_total = 0;
		$this->shipping_total = 0;
		$this->weight_total=0;
		$this->producttype="product";
		
		foreach($this->items as $tmp_item)
		{
			$item = FALSE;
			/* get product image to show in shopping cart */
			$product_sql = "select distinct p.iProductId, p.vProductName as vProductName, p.vSKU, p.fPrice, p.fRetailPrice,c.iCategoryId, c.vCategory_EN as vCategory, 
			p.vImage as vImage, p.eStatus from products p 
			LEFT JOIN product_category pc ON p.iProductId = pc.iProductId LEFT JOIN category c on c.iCategoryId=pc.iCategoryId 
			where p.eStatus = 'Active' and c.eStatus = 'Active' and p.iProductId = '".$this->product_id[$tmp_item]."'";
			$db_product = $obj->MySQLSelect($product_sql);
      
     
			if(@file_exists($tconfig['tsite_upload_images_product_images_path'].$this->product_id[$tmp_item]."/1_".$db_product[0]["vImage"]) && $db_product[0]["vImage"]!="")
			{
				$product_image =  $tconfig['tsite_upload_images_product_images'].$this->product_id[$tmp_item]."/1_".$db_product[0]["vImage"];
			}
			else
			{
				$product_image =  $tconfig['tsite_images']."/2_no-image.gif";
			}            
			$item['id'] = $tmp_item;
			$item['product_id'] = $this->product_id[$tmp_item];
			$item['product_attribute_id'] = $this->itemattribute[$tmp_item];		
      $item['qty'] = $this->itemqtys[$tmp_item];
			$item['fPrice'] = $this->itemprices[$tmp_item];
			$item['price'] = $generalobj->Make_Currency($this->itemprices[$tmp_item]);
			$item['fRetailPrice'] = $db_product[0]["fRetailPrice"];
			$item['productname'] = $this->iteminfo[$tmp_item];
			$item['sku'] = $db_product[0]["vSKU"];
			$item['category_id'] = $db_product[0]["iCategoryId"];
			//$item['etype'] = $db_product[0]["eType"];
			$item['image'] = $product_image;
			$item['url'] =$site_url."product-details/".$generalobj->replace_content($db_product[0]["vCategory"])."/".$generalobj->replace_content(ucwords($this->iteminfo[$tmp_item]))."/".$db_product[0]["iCategoryId"]."/".$this->product_id[$tmp_item];
        $item['subtotal'] = $item['qty'] * (double)$item['fPrice'];
			$this->weight_total=$this->weight_total+$item['qty'] * $db_product[0]["fWeight"];

      /* generate attribute string */
			$attribute_str = "";
			$color_size_arr = explode("_",$tmp_item);   		
			$sql = "SELECT vTitle FROM attributes WHERE iAttributeId='".$color_size_arr[1]."'";
			$db_size = $obj->MySQLSelect($sql);
      $attribute_str.="<b>Size:</b> ".$db_size[0]['vTitle'].", ";

			$sql = "SELECT vTitle FROM attributes WHERE iAttributeId='".$color_size_arr[2]."'";
			$db_size = $obj->MySQLSelect($sql);
      $attribute_str.="<b>Color:</b> ".$db_size[0]['vTitle'].", ";
      
			$attribute_str=substr($attribute_str,0,-2);
			$item['attribute'] = $attribute_str;

            /*
			if($tmp_item){
                $db_attribute = $generalobj->getProductAttribute($tmp_item);
                $attribute_str = "";
                for($i = 0; $i<count($db_attribute); $i++){
                    $attribute_str.="<b>".$db_attribute[$i]['vTitle'].":</b> ".$db_attribute[$i]['vValue'].", ";
                }
                $attribute_str=substr($attribute_str,0,-2);
                $item['attribute'] = $attribute_str;
			}
			*/
            $items[] = $item;
		} //echo "<pre>";print_r($item);exit;      
		return $items;
	}   
	
	function add_item($itemid,$product_id,$qty=1,$price = FALSE, $info = FALSE,$attribute_id = FALSE)
	{  		
    // adds an item to cart
		if(!$price)
		{
			$price = $this->get_product_price($product_id,$itemid);
		}
		if($this->itemqtys[$itemid] > 0)
		{ 
			// the item is already in the cart so we'll just increase the quantity..
			$this->itemqtys[$itemid] = $qty + $this->itemqtys[$itemid];
			$this->_update_total();
		}
		else
		{
			$this->items[]=$itemid;
			$this->product_id[$itemid]=$product_id;
			$this->itemqtys[$itemid] = $qty;
			$this->itemprices[$itemid] = $price;
			$this->iteminfo[$itemid] = $info;
			$this->itemattribute[$itemid] = $attribute_id;
		}
		$this->_update_total();
	}

	function edit_item($itemid,$qty)
	{
		// changes an items quantity
		if($qty < 1)
		{
			$this->del_item($itemid);
		}
		else
		{
			$this->itemqtys[$itemid] = $qty;
			// uncomment this line if using 
			// the wf_get_price function
			// $this->itemprices[$itemid] = wf_get_price($itemid,$qty);
		}
		$this->_update_total();
	}


	function del_item($itemid)
	{
		// removes an item from cart
		$ti = array();
		$this->itemqtys[$itemid] = 0;
		foreach($this->items as $item)
		{
			if($item != $itemid)
			{
				$ti[] = $item;
			}
		}
		$this->items = $ti;
        $this->_update_total();
	}

	function empty_cart()
	{
		// empties / resets the cart
        $this->total = 0;
	    $this->itemcount = 0;
        $this->shipping_total = 0;
        $this->taxable_total = 0;
		$this->weight_total=0;
	    $this->items = array();
        $this->itemprices = array();
	    $this->itemqtys = array();
        $this->itemdescs = array();
        $this->product_id = array();
        $this->iteminfo = array();
        $this->itemattribute = array();
	}

	function _update_total()
	{
		// internal function to update the total in the cart
	    $this->itemcount = 0;
		$this->total = 0;
        if(sizeof($this->items > 0))
		{
			foreach($this->items as $item)
			{
            	$this->total = (double)$this->total + ((double)$this->itemprices[$item] * $this->itemqtys[$item]);
				$this->itemcount++;
			}
		}
	}
	
	function get_product_price($itemid,$attribute_id)
	{
		global $obj,$ProductsObj;
		$sql = "SELECT fPrice,fRetailPrice FROM products  WHERE iProductId in ($itemid)";
	
		$db_rec = $obj->MySQLSelect($sql);
       
      $price_sql = "select vPrefix, fPrice from product_attributes where iProdAttrId  in ($attribute_id)";
        
        $db_price_sql = $obj->MySQLSelect($price_sql);
        $attr_price = 0;
        for($i = 0; $i<count($db_price_sql);$i++){
            if($db_price_sql[$i]['vPrefix'] == "+"){
                $attr_price += (double)$db_price_sql[$i]['fPrice'] ;
            }
            if($db_price_sql[$i]['vPrefix'] == "-"){
               $attr_price -= (double)$db_price_sql[$i]['fPrice'] ;
            }
        }
        $attr_price = (double)$db_rec[0]['fPrice'] + $attr_price;
     
		return $attr_price;
	}
	function getTaxCharge($amount, $country, $state="",$producttax)
	{
		global $sess_iQty, $obj, $xcart_country, $xcart_state;
	
		$sql="select t.fTaxPrice from tax t where t.vCountry='$country' and t.vState='$state' and t.eStatus='Active'";
		$sh_rs=$obj->MySQLSelect($sql);
		$tax = $sh_rs[0]["fTaxPrice"]+$producttax;
		$tax_ch=(double)((double)($tax*$amount)/100);
	//echo $tax_ch.":".$sh_rs[0]["fTaxPrice"].":".$amount;
		return $tax_ch;
	}
	
	function get_product_type(){
		return $producttype;
	}
}
?>
