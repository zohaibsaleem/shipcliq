<?php
class media_handler
{
    function convert_media($filename, $rootpath, $inputpath, $outputpath, $width, $height, $bitrate, $samplingrate)
    {
        $outfile = "";
        // root directory path, where FFMPEG folder exist in your application.
        $rPath = $rootpath."\ffmpeg";
        // which shows FFMPEG folder exist on the root.
        // Set Media Size that is width and hieght
        $size = $width."x".$height;
        // remove origination extension from file adn add .flv extension, becuase we must give output file name to ffmpeg command.
        $outfile =$filename;
        $out=explode(".",$outfile);
        
        
        // Media Size
        //$size = Width & "x" & Height;
        
        // remove origination extenstion from file and add .flv extension , becuase we must give output filename to ffmpeg command.
        
        $outfile = $out[0].".flv";
        // Use exec command to access command prompt to execute the following FFMPEG Command and convert video to flv format.
        
        $ffmpegcmd1 = "/usr/bin/ffmpeg -i ".$inputpath."/".$filename. " -r 25 -ac 2 -ab 448 -ar 44100 -f flv -s ".$size." ".$outputpath."/".$outfile;
        //$ffmpegcmd1 = "/usr/local/bin/ffmpeg -i ".$inputpath."/".$filename. " -b 500 -r 25 -s 320�240 -hq -deinterlace -ab 56 -ar 22050 -ac 1 ".$outputpath."/".$outfile." 2>&1";
        
        
        $ret = shell_exec($ffmpegcmd1);
        
        // return output file name for other operations
        return $ffmpegcmd1;
    }

    function grab_image($filename, $rootpath, $inputpath,$outputpath, $no_of_thumbs, $frame_number, $image_format, $width, $height)
    {
        // root directory path
        $_rootpath = $rootpath."\ffmpeg";
        // Media Size
        $size = $width."x".$height;
        
        // I am using static image, you can dynamic it with your own choice.
        $out=explode(".",$filename);
        
        $outfile = $out[0].".jpg";
        
        $ffmpegcmd1 = "/usr/bin/ffmpeg -i ".$inputpath."/".$filename." -vframes ".$no_of_thumbs." -ss 00:00:03 -an -vcodec ". $image_format." -f rawvideo -s ".$size. " ". $outputpath."/".$outfile;
        $ret = shell_exec($ffmpegcmd1);
        
        // Execute this command using exec command or any other tool to grab image from converted flv file.
        return $ffmpegcmd1;
    }
}
?>
                                                