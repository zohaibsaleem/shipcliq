<?php
  Class Organizations
  {
    function participating_organizations(){
      global $obj, $tconfig;  
            
      $sql = "SELECT COUNT(iOrganizationId) as tot_organizations FROM organization WHERE eStatus = 'Active' AND eType = 'Organization'";
      $db_org = $obj->MySQLSelect($sql);
      
      return $db_org[0]['tot_organizations'];
    }
    
    function featured_organizations(){
      global $obj, $tconfig;
      
      $sql = "SELECT iOrganizationId, vOrganizationName, vImage FROM organization WHERE eStatus = 'Active' AND eType = 'Organization' AND eFeatured = 'Yes' ORDER BY RAND() LIMIT 0,4";
      $db_org = $obj->MySQLSelect($sql);
      
      if(count($db_org) > 0){
        for($i=0;$i<count($db_org);$i++){    
          if(is_file($tconfig["tsite_upload_images_organization_path"].$db_org[$i]['iOrganizationId']."/1_".$db_org[$i]['vImage']))
        	{
        		$db_org[$i]['img_url'] = $tconfig["tsite_upload_images_organization"].$db_org[$i]['iOrganizationId']."/1_".$db_org[$i]['vImage'];
        	} 
        	else
        	{
        		$db_org[$i]['img_url'] = $tconfig['tsite_images'].'233x192.jpg';
        	}
        }
      }          
      return $db_org;
    }
    
    function interest_areas_registration(){
      global $obj, $tconfig;
      
      $sql = "SELECT iCategoryId, vCategory FROM category WHERE eStatus = 'Active'";
      $db_categories = $obj->MySQLSelect($sql);
      
      return $db_categories;
    } 
    
    function get_countires(){
      global $obj, $tconfig;
      
      $sql = "SELECT vCountryCode, vCountry FROM country WHERE eStatus = 'Active'";
      $db_countries = $obj->MySQLSelect($sql);
      
      return $db_countries;
    }
    
    function check_organization(){
      global $site_url; 
      
      if($_SESSION['sess_iOrganizationId'] == ""){
        header("Location:".$site_url."index.php?file=o-logout");
        exit;
			}
    }
    
    function profile_details($iOrganizationId){
      global $obj, $tconfig;
      
      $sql = "SELECT * FROM organization WHERE iOrganizationId = '".$iOrganizationId."'";
      $db_organization = $obj->MySQLSelect($sql);
      
      if(is_file($tconfig["tsite_upload_images_organization_path"].$db_organization[0]['iOrganizationId']."/1_".$db_organization[0]['vImage']))
    	{
    		$db_organization[0]['img_url'] = $tconfig["tsite_upload_images_organization"].$db_organization[0]['iOrganizationId']."/1_".$db_organization[0]['vImage'];
    	} 
    	else
    	{
    		$db_organization[0]['img_url'] = '';
    	}
    	
    	return $db_organization; 
    } 
    
    function organization_interest_areas($iOrganizationId){
      global $obj, $tconfig;
      
      $sql = "SELECT iCategoryId FROM organization_interests WHERE iOrganizationId = '".$iOrganizationId."'";
      $db_organization_interest = $obj->MySQLSelect($sql);
      
      return $db_organization_interest; 
    }
    
    function OrganizationChangePwd()
  	{
  		global $obj,$generalobj,$tconfig;
  		$vOldPassword = $generalobj->encrypt(trim($_POST[vOldPassword]));
  		$sql="select iOrganizationId from organization where vPassword = '".$vOldPassword."' and  iOrganizationId ='".$_SESSION['sess_iOrganizationId']."'";
  		$db_member = $obj->MySQLSelect($sql);
  	  if($db_member)
  		{
  			$sql="update organization set vPassword ='".$generalobj->encrypt(trim($_POST['vPassword']))."' where vPassword = '".$vOldPassword."' and iOrganizationId = '".$_SESSION['sess_iOrganizationId']."'";
  			$db_MemUpdate = $obj->sql_query($sql);
  			if($db_MemUpdate)
  			{
          $msg = "Your password has been changed successfully.";
          $error_msg = "0";
  			}
  			else
  			{
  				$msg = "Error occured during changing your password.Try again.";
          $error_msg = "1";
  			}
  		}
  		else
  		{
  			$msg = "Sorry..You have provided invalid old password.Please try again.";
        $error_msg = "1";
  		}
  		header("location:".$tconfig['tsite_url']."index.php?file=o-changepassword&var_msg=".$msg."&msg_code=".$error_msg);
  		exit;
  	}
  	
  	function organization_events($iOrganizationId){
      global $obj, $tconfig;
      
      $sql = "SELECT iEventId, vTitle, dStartDate, dEndDate, eStatus, eEventType, 
              (SELECT COUNT(iEventVolunteerId) FROM event_volunteer WHERE event_volunteer.iEventId = event.iEventId AND eStatus = 'Accepted') as tot_attending,
              (SELECT COUNT(iEventVolunteerId) FROM event_volunteer WHERE event_volunteer.iEventId = event.iEventId AND eStatus = 'Pending') as tot_pendings  
              FROM event 
              WHERE iOrganizationId = '".$iOrganizationId."' ORDER BY dStartDate DESC";
      $db_organization_events = $obj->MySQLSelect($sql);
      
      return $db_organization_events; 
    }
    
    function event_details_subscription($iEventId, $iOrganizationId){
      global $obj, $tconfig;
      
      $sql = "SELECT vTitle, dEndDate FROM event WHERE iEventId = '".$iEventId."' AND iOrganizationId = '".$iOrganizationId."' AND eStatus = 'Active'";
      $db_subscription_events = $obj->MySQLSelect($sql);
      
      if(count($db_subscription_events) > 0){
        return $db_subscription_events;
      }else{
        header("location:".$tconfig['tsite_url']."my-events");
  		  exit;
      }
    }
    
    function subscriptions($iEventId, $iOrganizationId){
      global $obj, $tconfig;
      
      $sql = "SELECT event_volunteer.iEventVolunteerId, event_volunteer.iMemberId, member.vFirstName, member.vLastName, member.vImage, member.vCity, state.vState 
              FROM event_volunteer 
              LEFT JOIN member ON member.iMemberId = event_volunteer.iMemberId 
              LEFT JOIN state ON state.vStateCode = member.vState AND state.vCountryCode = member.vCountry
              WHERE event_volunteer.iEventId = '".$iEventId."' AND event_volunteer.eStatus = 'Pending'  
              ORDER BY event_volunteer.dDate ASC";
      $subscriptions = $obj->MySQLSelect($sql);
      
      for($i=0;$i<count($subscriptions);$i++){
        if(is_file($tconfig["tsite_upload_images_member_path"].$subscriptions[$i]['iMemberId']."/1_".$subscriptions[$i]['vImage']))
      	{
      		$subscriptions[$i]['img_url'] = $tconfig["tsite_upload_images_member"].$subscriptions[$i]['iMemberId']."/1_".$subscriptions[$i]['vImage'];
      	} 
      	else
      	{
      		$subscriptions[$i]['img_url'] = $tconfig["tsite_images"].'volunteer.jpg';
      	}  
      } 
      
      return $subscriptions;      
    }
    
    
    
    function all_organizations($ssql, $ordby){
      global $obj, $tconfig;
      
      $sql = "SELECT DISTINCT organization.iOrganizationId, vOrganizationName, vFirstName, vLastName, vImage, tDescription, vCity, iRate, state.vState
              FROM organization 
              LEFT JOIN state ON state.vStateCode = organization.vState AND state.vCountryCode = organization.vCountry 
              LEFT JOIN organization_interests ON organization_interests.iOrganizationId = organization.iOrganizationId 
              LEFT JOIN category ON category.iCategoryId = organization_interests.iCategoryId  
              WHERE organization.eStatus = 'Active'$ssql $ordby";
      $db_organization = $obj->MySQLSelect($sql);
      
      return $db_organization;   
    }
    
    function organization_list($ssql, $ordby, $var_limit){
      global $obj, $tconfig;
      
      $sql = "SELECT DISTINCT organization.iOrganizationId, vOrganizationName, vFirstName, vLastName, vImage, tDescription, vCity, iRate, state.vState
              FROM organization 
              LEFT JOIN state ON state.vStateCode = organization.vState AND state.vCountryCode = organization.vCountry 
              LEFT JOIN organization_interests ON organization_interests.iOrganizationId = organization.iOrganizationId 
              LEFT JOIN category ON category.iCategoryId = organization_interests.iCategoryId  
              WHERE organization.eStatus = 'Active'$ssql $ordby". $var_limit;
      $db_organization = $obj->MySQLSelect($sql);
     
      for($i=0;$i<count($db_organization);$i++){
        if(is_file($tconfig["tsite_upload_images_organization_path"].$db_organization[$i]['iOrganizationId']."/1_".$db_organization[$i]['vImage']))
      	{
      		$db_organization[$i]['img_url'] = $tconfig["tsite_upload_images_organization"].$db_organization[$i]['iOrganizationId']."/1_".$db_organization[$i]['vImage'];
      	} 
      	else
      	{
      		$db_organization[$i]['img_url'] = $tconfig['tsite_images'].'233x192.jpg';
      	}  
      } 
          
      return $db_organization;   
    }
    
    function organization_city(){
      global $obj, $tconfig;
      
      $sql = "SELECT DISTINCT vCity FROM organization WHERE vCity != '' AND eStatus = 'Active' ORDER BY vCity ASC";
      $db_city = $obj->MySQLSelect($sql);
      
      for($i=0;$i<count($db_city);$i++){
        if($i == count($db_city)-1)
        $city_cont .= '"'.$db_city[$i]['vCity'].'"'; 
        else 
        $city_cont .= '"'.$db_city[$i]['vCity'].'",'; 
      }
      
      return $city_cont;
    }
    
    function event_attending($iEventId){
      global $obj, $tconfig;
      
      $sql = "SELECT event_volunteer.iEventVolunteerId, event_volunteer.iMemberId, event_volunteer.dHours, member.vFirstName, member.vLastName, member.vImage, member.vCity, state.vState 
              FROM event_volunteer 
              LEFT JOIN member ON member.iMemberId = event_volunteer.iMemberId 
              LEFT JOIN state ON state.vStateCode = member.vState AND state.vCountryCode = member.vCountry 
              WHERE event_volunteer.iEventId = '".$iEventId."' AND event_volunteer.eStatus = 'Accepted'  
              ORDER BY event_volunteer.dDate ASC";
      $attending = $obj->MySQLSelect($sql);
      
      for($i=0;$i<count($attending);$i++){
        if(is_file($tconfig["tsite_upload_images_member_path"].$attending[$i]['iMemberId']."/1_".$attending[$i]['vImage']))
      	{
      		$attending[$i]['img_url'] = $tconfig["tsite_upload_images_member"].$attending[$i]['iMemberId']."/1_".$attending[$i]['vImage'];
      	} 
      	else
      	{
      		$attending[$i]['img_url'] = $tconfig["tsite_images"].'volunteer.jpg';
      	}  
      } 
      
      return $attending;
    }
    
    function home_page_banners(){
      global $obj, $tconfig;
       
      $sql_banner = "SELECT vImage, vTitle, iBannerId, iDisplayOrder, tURL FROM banners WHERE ePortion='Top' AND eStatus = 'Active' ORDER BY iDisplayOrder ASC";
      $db_banners = $obj->MySQLSelect($sql_banner);
      for($i=0;$i<count($db_banners);$i++)
      {
        if(is_file($tconfig["tsite_upload_images_banner_path"].$db_banners[$i]['vImage']))
        {
        $db_banners[$i]['vImage'] = $tconfig["tsite_upload_images_banner"].$db_banners[$i]['vImage'];
        } 
        else
        {
        $db_banners[$i]['vImage'] = '';
        }  	
      }
      return $db_banners; 
    }
    
    function static_page_details($id){
      global $obj, $tconfig;
      
      $sql = "SELECT * FROM pages WHERE iPageId = '".$id."'";
      $db_page = $obj->MySQLSelect($sql);
      
      return $db_page; 
    }
    
    function member_of_theweek(){
      global $obj, $tconfig;
      
      $sql = "SELECT iMemberId, vFirstName, vLastName, vImage FROM member WHERE eMemberOfWeek = 'Yes' LIMIT 0,1";
      $db_memberweek = $obj->MySQLSelect($sql);
      
      $sql = "SELECT SUM(dHours) as tothrs FROM event_volunteer WHERE iMemberId = '".$db_memberweek[0]['iMemberId']."'";
      $db_hours = $obj->MySQLSelect($sql);
      
      $db_memberweek[0]['dHours'] = $db_hours[0]['tothrs'];
      if(is_file($tconfig["tsite_upload_images_member_path"].$db_memberweek[0]['iMemberId']."/1_".$db_memberweek[0]['vImage']))
    	{
    		$db_memberweek[0]['img_url'] = $tconfig["tsite_upload_images_member"].$db_memberweek[0]['iMemberId']."/1_".$db_memberweek[0]['vImage'];
    	} 
    	else
    	{
    		$db_memberweek[0]['img_url'] = $tconfig["tsite_images"].'volunteer.jpg';
    	}  
  
      return $db_memberweek;
    }
  }
?> 
 