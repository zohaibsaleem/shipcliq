<?php
	Class Rides
	{
		function DateTimenew($text,$format='')
		{
			if($text =="" || $text =="0000-00-00 00:00:00" || $text =="0000-00-00")
    		return "---";
			switch($format)
			{
				//us formate
				case "1":
    			return date('M j, Y',strtotime($text));
    			break;

				case "2":
    			return date('M j, y  [G:i] ',strtotime($text));
    			break;

				case "3":
    			return date("M j, Y", $text);
    			break;

				case "4":
    			return date('Y,n,j,G,',$text).intval(date('i',$text)).','.intval(date('s',$text));
    			break;

				case "5":
    			return date('l, F j, Y',strtotime($text));
    			break;

				case "6":
    			return date('g:i:s',$text);
    			break;

				case "7":
    			return date('F j, Y  h:i A',strtotime($text));
    			break;

				case "8":
    			return date('Y-m-d',strtotime($text));
    			break;
				case "9":
    			return date('F j, Y',strtotime($text));
    			break;
				case "10":
    			return date('d/m/Y',strtotime($text));
    			break;
				case "11":
    			return date('m/d/y',strtotime($text));
    			break;
				case "12":
    			return date('H:i',strtotime($text));
    			break;
				case "13":
    			return date('F j, Y (H:i:s)',strtotime($text));
    			break;
				case "14":
    			return date('j M Y',strtotime($text));
    			break;
				case "15":
    			return date('D',strtotime($text));
    			break;
				case "16":
    			return date('d',strtotime($text));
    			break;
				case "17":
    			return date('M Y',strtotime($text));
    			break;
				case "18":
    			return date('h:i A',strtotime($text));
    			break;
				default :
    			return date('M j, Y',strtotime($text));
    			break;
			}
		}

		function get_featuredin_details(){
			global $obj, $tconfig;

			$sql = "SELECT vTitle, vImage, vURL FROM media_partners WHERE eStatus = 'Active' ORDER BY RAND()";
			$db_media_partners = $obj->MySQLSelect($sql);

			if(count($db_media_partners) > 0){
				for($i=0;$i<count($db_media_partners);$i++){
					if(is_file($tconfig["tsite_upload_media_partners_path"]."/2_".$db_media_partners[$i]['vImage']))
					{
						$db_media_partners[$i]['img_url'] = $tconfig["tsite_upload_images_media_partners"]."/2_".$db_media_partners[$i]['vImage'];
					}
					else
					{
						$db_media_partners[$i]['img_url'] = '';
					}
				}
			}

			return $db_media_partners;
		}

		function pay_paypal_form($booking_id){
			global $obj, $tconfig, $PAYPAL_ID, $PAYPAL_TRANSACTION_MODE, $THEME;

			$sql = "SELECT iBookingId, fAmount, vBookerCurrencyCode, vFromPlace, vToPlace, iNoOfSeats, vBookingNo FROM temp_booking WHERE iBookingId = '".$booking_id."'";
			$db_booking = $obj->MySQLSelect($sql);

			$fAmount = $db_booking[0]['fAmount'] / $db_booking[0]['iNoOfSeats'];

			$itemname = 'Booking for: '.$db_booking[0]['vFromPlace'].' - '.$db_booking[0]['vToPlace'];

			$returnURL =$tconfig['tsite_url']."index.php?file=m-payment_a&x_response_code=1&iBookingId=".$db_booking[0]['iBookingId'];
			$cancleURL =$tconfig['tsite_url']."index.php?file=m-payment_a&x_response_code=0&iBookingId=".$db_booking[0]['iBookingId'];

			$logoURL = $tconfig['tsite_images'].'home/'.$THEME.'/logo.png';
			//$notifyURL = $tconfig['tsite_url']."payment_ipn.php";

			if($PAYPAL_TRANSACTION_MODE == "Live") {
				$paymentUrl	=	"https://www.paypal.com/cgi-bin/webscr";
				}else {
				$paymentUrl	=	"https://www.sandbox.paypal.com/cgi-bin/webscr";
			}
			/* currency validated by Paypal support
				https://developer.paypal.com/docs/integration/direct/rest_api_payment_country_currency_support/
			*/
			$currency_supported = array('AUD', 'BRL', 'CAD', 'CZK', 'DKK', 'EUR', 'HKD', 'HUF', 'ILS', 'JPY', 'MYR', 'MXN', 'TWD', 'NZD', 'NOK', 'PHP', 'PLN', 'GBP', 'RUB', 'SGD', 'SEK', 'CHF', 'THB', 'TRY', 'USD');
			if(!in_array(strtoupper($db_booking[0]['vBookerCurrencyCode']),$currency_supported))
			{
				$sql = "SELECT * FROM `currency` WHERE `vName` = '".$db_booking[0]['vBookerCurrencyCode']."'";
				$data = $obj->MySQLSelect($sql);

				$ratio = isset($data[0]['Ratio'])?$data[0]['Ratio']:'1.00';
				$db_booking[0]['vBookerCurrencyCode'] = 'USD';
				$fAmount  = $fAmount * $ratio;
			}
			/* currency validated by Paypal support */

			$frmPaypal = '
			<form ACTION="'.$paymentUrl.'" METHOD="post" name="frmPaypal">
			<INPUT TYPE="hidden" NAME="cmd" VALUE="_xclick">
			<INPUT TYPE="hidden" NAME="business" VALUE="'.$PAYPAL_ID.'">
			<INPUT TYPE="hidden" NAME="item_name" VALUE="'.$itemname.' Order">
			<INPUT TYPE="hidden" NAME="quantity" VALUE="'.$db_booking[0]['iNoOfSeats'].'">
			<INPUT TYPE="hidden" NAME="item_number" VALUE="'.$db_booking[0]['vBookingNo'].'">
			<input type="hidden" name="no_shipping" value="1">
			<input type="hidden" name="return" value="'.$returnURL.'">
			<input type="hidden" name="cancel_return" value="'.$cancleURL.'">
			<input type="Hidden" name="custom" value="'.$booking_id.'">
			<input type="Hidden" name="orderType" value="New">
			<input type="hidden" name="amount" value="'.number_format($fAmount,2).'">
			<input type="Hidden" name="currency_code" value="'.$db_booking[0]['vBookerCurrencyCode'].'">
			<input type="Hidden" name="cpp_header_image" value="'.$logoURL.'">
			<input type="hidden" name="notify_url" value="'.$notifyURL.'">
			<!-- <INPUT type="Submit" name="sub_mit" value="Submit">     -->
			<script>
			document.frmPaypal.submit();
			</script>
			</form>
  			';
			return $frmPaypal;

		}

		function email_cont($Data, $sms_code_send = 0){
			global $obj, $tconfig, $generalobj;
			$sql = "SELECT vImage, eGender FROM member WHERE iMemberId = '".$Data['iBookerId']."'";
			$db_booker = $obj->MySQLSelect($sql);


			if(is_file($tconfig["tsite_upload_images_member_path"].$Data['iBookerId']."/1_".$db_booker[0]['vImage']))
			{
				$bookerimage = $tconfig["tsite_upload_images_member"].$Data['iBookerId']."/1_".$db_booker[0]['vImage'];
			}
			else
			{
				if($db_booker[0]['eGender']=='Male'){
					$bookerimage = $tconfig["tsite_images"].'64x64male.png';
					}else{
					$bookerimage = $tconfig["tsite_images"].'64x64female.png';
				}
			}

			$sql = "SELECT vImage, eGender FROM member WHERE iMemberId = '".$Data['iDriverId']."'";
			$db_driver = $obj->MySQLSelect($sql);

			if(is_file($tconfig["tsite_upload_images_member_path"].$Data['iDriverId']."/1_".$db_driver[0]['vImage']))
			{
				$driverimage = $tconfig["tsite_upload_images_member"].$Data['iDriverId']."/1_".$db_driver[0]['vImage'];
			}
			else
			{
				if($db_driver[0]['eGender']=='Male'){
					$driverimage = $tconfig["tsite_images"].'64x64male.png';
					}else{
					$driverimage = $tconfig["tsite_images"].'64x64female.png';
				}
			}

			$smsInfo = ($sms_code_send == 1)?'<tr>
			<td width="100%" colspan="2" valign="top" style="color:'.SITE_COLOR.';padding: 5px 0 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;font-weight:bold;">'.LBL_CONFIRM_BOOKING_CODE.': </span> '.$Data['vVerificationCode'].'
			</td>
			</tr>':''; // Driver will not receive secure code

			$mailcont .= '<table width="100%" style="border:1px solid #cccccc;border-collapse: collapse;background-color: #FFFFFF;">
			<tr>
			<td width="100%" colspan="2" style="background:#FFF;color:#000;padding:10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">'.LBL_RIDE_BOOKING.'</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" style="background:'.SITE_COLOR.';color:#FFF;padding:10px;text-align:left;font-size:17px;font-family:Arial, Helvetica, sans-serif;">'.LBL_BOOKINGDETAIL_NO.'&nbsp;#'.$Data['vBookingNo'].'</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" valign="top" style="color:'.SITE_COLOR.';padding: 5px 0 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.LBL_FROM_LOCATION.': </span> <img src="'.$tconfig['tsite_images'].'/search-from-plot1.png">&nbsp;'.$Data['vFromPlace'].'
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" valign="top" style="color:'.SITE_COLOR.';padding: 5px 0 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.LBL_TO_LOCATION.': </span> <img src="'.$tconfig['tsite_images'].'/search-to-plot1.png">&nbsp;'.$Data['vToPlace'].'
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" valign="top" style="color:'.SITE_COLOR.';padding: 5px 0 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.LBL_BOOKING_DATE.': </span> '.$generalobj->DateTimeFormat($Data['dBookingDate']).'&nbsp;&nbsp;<span style="color:#000;">'.LBL_TIME.': </span> '.$this->DateTimenew($Data['dBookingTime'],12).' ('.LBL_APPROX_TIME.')
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" valign="top" style="color:'.SITE_COLOR.';padding: 5px 0 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">';
			if($Data['vDocumentWeight']!=''){$mailcont.='<span style="color:#000;">'.LBL_BOOKING_DOC.': </span> '.$Data['vDocumentWeight'].' '.$Data['vDocumentUnit'].':'.$Data['vBookerCurrencyCode'].' '.$Data['fDocumentPrice']*$Data['vDocumentWeight'];}if($Data['vBoxWeight'] != ''){ $mailcont.='&nbsp;&nbsp;&nbsp;<br><span style="color:#000;">'.LBL_BOOKING_BOX.': </span>'.$Data['vBoxWeight'].' '.$Data['vBoxUnit'].':'.$Data['vBookerCurrencyCode'].' '.$Data['fBoxPrice']*$Data['vBoxWeight'];}if($Data['vLuggageWeight']!=''){$mailcont.='&nbsp;&nbsp;&nbsp;<br><span style="color:#000;">'.LBL_BOOKING_WEIGHT.': </span> '.$Data['vLuggageWeight'].' '.$Data['vLuggageUnit'].':'.$Data['vBookerCurrencyCode'].' '.$Data['fLuggagePrice']*$Data['vLuggageWeight'];}$mailcont.='&nbsp;&nbsp;&nbsp;<br><span style="color:#000;">'.LBL_PRICE.': </span> '.$Data['vBookerCurrencyCode'].' '.$Data['fAmount'].'
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" valign="top" style="color:'.SITE_COLOR.';padding: 5px 0 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.LBL_MAIN_TRIP_ROUTE.': </span> '.$Data['vMainRidePlaceDetails'].'
			</td>
			</tr>
			'.$smsInfo.'
			<tr>
			<td width="100%" colspan="2" style="background:'.SITE_COLOR.';color:#FFF;padding:10px;text-align:left;font-size:17px;font-family:Arial, Helvetica, sans-serif;">'.LBL_BOOKER_DETAILS.'</td>
			</tr>
			<tr>
			<td width="12%" valign="top" style="border-bottom:0px solid #cccccc;">
			<img style="margin-top: 5px;border-radius: 3px;border-bottom: 5px solid '.SITE_COLOR.';" alt="" src="'.$bookerimage.'">
			</td>
			<td width="88%" valign="top" style="border-bottom:0px solid #cccccc;">
			<table width="100%">
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.LBL_NAME.':</span> '.$Data['vBookerFirstName'].' '.$Data['vBookerLastName'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.LBL_EMAIL.':</span> '.$Data['vBookerEmail'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.LBL_PHONE.':</span> '.$Data['vBookerPhone'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;"><a href="'.$tconfig['tsite_url'].'index.php?file=c-user_profile&iMemberId='.$Data['iBookerId'].'">'.LBL_CLICK_HERE.'</a> '.LBL_PROFILE_BOOKER.'</span>
			</td>
			</tr>
			</table>
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" style="background:'.SITE_COLOR.';color:#FFF;padding:10px;text-align:left;font-size:17px;font-family:Arial, Helvetica, sans-serif;">'.LBL_DRIVER_DETAILS.'</td>
			</tr>
			<tr>
			<td width="12%" valign="top" style="border-bottom:1px solid #cccccc;">
			<img style="margin-top: 5px;border-radius: 3px;border-bottom: 5px solid '.SITE_COLOR.';" alt="" src="'.$driverimage.'">
			</td>
			<td width="88%" valign="top" style="border-bottom:0px solid #cccccc;">
			<table width="100%">
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.LBL_NAME.':</span>'.$Data['vDriverFirstName'].' '.$Data['vDriverLastName'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.LBL_EMAIL.':</span> '.$Data['vDriverEmail'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.LBL_PHONE.':</span> '.$Data['vDriverPhone'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;"><a href="'.$tconfig['tsite_url'].'index.php?file=c-user_profile&iMemberId='.$Data['iDriverId'].'">'.LBL_CLICK_HERE.'</a> '.LBL_PROFILE_DRIVER.'</span>
			</td>
			</tr>
			</table>
			</td>
			</tr>
			</table>';
			return $mailcont;
		}

		function email_cont_driver($Data){
			global $obj, $tconfig, $generalobj;

			$sql = "SELECT vImage, eGender FROM member WHERE iMemberId = '".$Data['iBookerId']."'";
			$db_booker = $obj->MySQLSelect($sql);

			if(is_file($tconfig["tsite_upload_images_member_path"].$Data['iBookerId']."/1_".$db_booker[0]['vImage']))
			{
				$bookerimage = $tconfig["tsite_upload_images_member"].$Data['iBookerId']."/1_".$db_booker[0]['vImage'];
			}
			else
			{
				if($db_booker[0]['eGender']=='Male'){
					$bookerimage = $tconfig["tsite_images"].'64x64male.png';
					}else{
					$bookerimage = $tconfig["tsite_images"].'64x64female.png';
				}
			}

			$sql = "SELECT vImage, eGender, vLanguageCode FROM member WHERE iMemberId = '".$Data['iDriverId']."'";
			$db_driver = $obj->MySQLSelect($sql);

			$sql = "SELECT * from language_label WHERE vCode = '".$db_driver[0]['vLanguageCode']."'";
			$db_label = $obj->MySQLSelect($sql);

			$vLabel = array();
			for($i=0;$i<count($db_label);$i++){
				$vLabel[$db_label[$i]['vLabel']]  = $db_label[$i]["vValue"];
			}

			if(is_file($tconfig["tsite_upload_images_member_path"].$Data['iDriverId']."/1_".$db_driver[0]['vImage']))
			{
				$driverimage = $tconfig["tsite_upload_images_member"].$Data['iDriverId']."/1_".$db_driver[0]['vImage'];
			}
			else
			{
				if($db_driver[0]['eGender']=='Male'){
					$driverimage = $tconfig["tsite_images"].'64x64male.png';
					}else{
					$driverimage = $tconfig["tsite_images"].'64x64female.png';
				}
			}

			$mailcont .= '<table width="600" style="border:1px solid #cccccc;border-collapse: collapse;background-color: #FFFFFF;">
			<tr>
			<td width="100%" colspan="2" style="background:#FFF;color:#000;padding:10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">'.$vLabel['LBL_RIDE_BOOKING'].'</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" style="background:'.SITE_COLOR.';color:#FFF;padding:10px;text-align:left;font-size:17px;font-family:Arial, Helvetica, sans-serif;">'.$vLabel['LBL_BOOKINGDETAIL_NO'].'&nbsp;#'.$Data['vBookingNo'].'</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" valign="top" style="color:'.SITE_COLOR.';padding: 5px 0 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_FROM_LOCATION'].': </span> <img src="'.$tconfig['tsite_images'].'/search-from-plot1.png">&nbsp;'.$Data['vFromPlace'].'
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" valign="top" style="color:'.SITE_COLOR.';padding: 5px 0 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_TO_LOCATION'].': </span> <img src="'.$tconfig['tsite_images'].'/search-to-plot1.png">&nbsp;'.$Data['vToPlace'].'
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" valign="top" style="color:'.SITE_COLOR.';padding: 5px 0 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_BOOKING_DATE'].': </span> '.$generalobj->DateTimeFormat($Data['dBookingDate']).'&nbsp;&nbsp;<span style="color:#000;">'.$vLabel['LBL_TIME'].': </span> '.$this->DateTimenew($Data['dBookingTime'],12).' ('.$vLabel['LBL_APPROX_TIME'].')
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" valign="top" style="color:'.SITE_COLOR.';padding: 5px 0 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_BOOKING_SEATS'].': </span> '.$Data['iNoOfSeats'].'&nbsp;&nbsp;&nbsp;<span style="color:#000;">'.$vLabel['LBL_PRICE'].': </span> '.$Data['vBookerCurrencyCode'].' '.$Data['fAmount'].'
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" valign="top" style="color:'.SITE_COLOR.';padding: 5px 0 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_MAIN_TRIP_ROUTE'].': </span> '.$Data['vMainRidePlaceDetails'].'
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" style="background:'.SITE_COLOR.';color:#FFF;padding:10px;text-align:left;font-size:17px;font-family:Arial, Helvetica, sans-serif;">'.$vLabel['LBL_BOOKER_DETAILS'].'</td>
			</tr>
			<tr>
			<td width="12%" valign="top" style="border-bottom:0px solid #cccccc;">
			<img style="margin-top: 5px;border-radius: 3px;border-bottom: 5px solid '.SITE_COLOR.';" alt="" src="'.$bookerimage.'">
			</td>
			<td width="88%" valign="top" style="border-bottom:0px solid #cccccc;">
			<table width="100%">
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_NAME'].':</span> '.$Data['vBookerFirstName'].' '.$Data['vBookerLastName'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_EMAIL'].':</span> '.$Data['vBookerEmail'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_PHONE'].':</span> '.$Data['vBookerPhone'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;"><a href="'.$tconfig['tsite_url'].'index.php?file=c-user_profile&iMemberId='.$Data['iBookerId'].'">'.$vLabel['LBL_CLICK_HERE'].'</a> '.$vLabel['LBL_PROFILE_BOOKER'].'</span>
			</td>
			</tr>
			</table>
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" style="background:'.SITE_COLOR.';color:#FFF;padding:10px;text-align:left;font-size:17px;font-family:Arial, Helvetica, sans-serif;">'.$vLabel['LBL_DRIVER_DETAILS'].'</td>
			</tr>
			<tr>
			<td width="12%" valign="top" style="border-bottom:1px solid #cccccc;">
			<img style="margin-top: 5px;border-radius: 3px;border-bottom: 5px solid '.SITE_COLOR.';" alt="" src="'.$driverimage.'">
			</td>
			<td width="88%" valign="top" style="border-bottom:0px solid #cccccc;">
			<table width="100%">
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_NAME'].':</span>'.$Data['vDriverFirstName'].' '.$Data['vDriverLastName'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_EMAIL'].':</span> '.$Data['vDriverEmail'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_PHONE'].':</span> '.$Data['vDriverPhone'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;"><a href="'.$tconfig['tsite_url'].'index.php?file=c-user_profile&iMemberId='.$Data['iDriverId'].'">'.$vLabel['LBL_CLICK_HERE'].'</a> '.$vLabel['LBL_PROFILE_DRIVER'].'</span>
			</td>
			</tr>
			</table>
			</td>
			</tr>
			</table>';
			return $mailcont;
		}

		function email_cont_booker($Data){
			global $obj, $tconfig, $generalobj;

			$sql = "SELECT vImage, eGender, vLanguageCode FROM member WHERE iMemberId = '".$Data['iBookerId']."'";
			$db_booker = $obj->MySQLSelect($sql);

			$sql = "SELECT * from language_label WHERE vCode = '".$db_booker[0]['vLanguageCode']."'";
			$db_label = $obj->MySQLSelect($sql);
			$vLabel = array();
			for($i=0;$i<count($db_label);$i++){
				$vLabel[$db_label[$i]['vLabel']]  = $db_label[$i]["vValue"];
			}
			if(is_file($tconfig["tsite_upload_images_member_path"].$Data['iBookerId']."/1_".$db_booker[0]['vImage']))
			{
				$bookerimage = $tconfig["tsite_upload_images_member"].$Data['iBookerId']."/1_".$db_booker[0]['vImage'];
			}
			else
			{
				if($db_booker[0]['eGender']=='Male'){
					$bookerimage = $tconfig["tsite_images"].'64x64male.png';
					}else{
					$bookerimage = $tconfig["tsite_images"].'64x64female.png';
				}
			}

			$sql = "SELECT vImage, eGender FROM member WHERE iMemberId = '".$Data['iDriverId']."'";
			$db_driver = $obj->MySQLSelect($sql);

			if(is_file($tconfig["tsite_upload_images_member_path"].$Data['iDriverId']."/1_".$db_driver[0]['vImage']))
			{
				$driverimage = $tconfig["tsite_upload_images_member"].$Data['iDriverId']."/1_".$db_driver[0]['vImage'];
			}
			else
			{
				if($db_driver[0]['eGender']=='Male'){
					$driverimage = $tconfig["tsite_images"].'64x64male.png';
					}else{
					$driverimage = $tconfig["tsite_images"].'64x64female.png';
				}
			}

			$mailcont .= '<table width="600" style="border:1px solid #cccccc;border-collapse: collapse;background-color: #FFFFFF;">
			<tr>
			<td width="100%" colspan="2" style="background:#FFF;color:#000;padding:10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">'.$vLabel['LBL_RIDE_BOOKING'].'</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" style="background:'.SITE_COLOR.';color:#FFF;padding:10px;text-align:left;font-size:17px;font-family:Arial, Helvetica, sans-serif;">'.$vLabel['LBL_BOOKINGDETAIL_NO'].'&nbsp;#'.$Data['vBookingNo'].'</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" valign="top" style="color:'.SITE_COLOR.';padding: 5px 0 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_FROM_LOCATION'].': </span> <img src="'.$tconfig['tsite_images'].'/search-from-plot1.png">&nbsp;'.$Data['vFromPlace'].'
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" valign="top" style="color:'.SITE_COLOR.';padding: 5px 0 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_TO_LOCATION'].': </span> <img src="'.$tconfig['tsite_images'].'/search-to-plot1.png">&nbsp;'.$Data['vToPlace'].'
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" valign="top" style="color:'.SITE_COLOR.';padding: 5px 0 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_BOOKING_DATE'].': </span> '.$generalobj->DateTimeFormat($Data['dBookingDate']).'&nbsp;&nbsp;<span style="color:#000;">'.$vLabel['LBL_TIME'].': </span> '.$this->DateTimenew($Data['dBookingTime'],12).' ('.$vLabel['LBL_APPROX_TIME'].')
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" valign="top" style="color:'.SITE_COLOR.';padding: 5px 0 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_BOOKING_SEATS'].': </span> '.$Data['iNoOfSeats'].'&nbsp;&nbsp;&nbsp;<span style="color:#000;">'.$vLabel['LBL_PRICE'].': </span> '.$Data['vBookerCurrencyCode'].' '.$Data['fAmount'].'
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" valign="top" style="color:'.SITE_COLOR.';padding: 5px 0 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_MAIN_TRIP_ROUTE'].': </span> '.$Data['vMainRidePlaceDetails'].'
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" style="background:'.SITE_COLOR.';color:#FFF;padding:10px;text-align:left;font-size:17px;font-family:Arial, Helvetica, sans-serif;">'.$vLabel['LBL_BOOKER_DETAILS'].'</td>
			</tr>
			<tr>
			<td width="12%" valign="top" style="border-bottom:0px solid #cccccc;">
			<img style="margin-top: 5px;border-radius: 3px;border-bottom: 5px solid '.SITE_COLOR.';" alt="" src="'.$bookerimage.'">
			</td>
			<td width="88%" valign="top" style="border-bottom:0px solid #cccccc;">
			<table width="100%">
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_NAME'].':</span> '.$Data['vBookerFirstName'].' '.$Data['vBookerLastName'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_EMAIL'].':</span> '.$Data['vBookerEmail'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_PHONE'].':</span> '.$Data['vBookerPhone'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;"><a href="'.$tconfig['tsite_url'].'index.php?file=c-user_profile&iMemberId='.$Data['iBookerId'].'">'.$vLabel['LBL_CLICK_HERE'].'</a> '.$vLabel['LBL_PROFILE_BOOKER'].'</span>
			</td>
			</tr>
			</table>
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" style="background:'.SITE_COLOR.';color:#FFF;padding:10px;text-align:left;font-size:17px;font-family:Arial, Helvetica, sans-serif;">'.$vLabel['LBL_DRIVER_DETAILS'].'</td>
			</tr>
			<tr>
			<td width="12%" valign="top" style="border-bottom:1px solid #cccccc;">
			<img style="margin-top: 5px;border-radius: 3px;border-bottom: 5px solid '.SITE_COLOR.';" alt="" src="'.$driverimage.'">
			</td>
			<td width="88%" valign="top" style="border-bottom:0px solid #cccccc;">
			<table width="100%">
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_NAME'].':</span>'.$Data['vDriverFirstName'].' '.$Data['vDriverLastName'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_EMAIL'].':</span> '.$Data['vDriverEmail'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_PHONE'].':</span> '.$Data['vDriverPhone'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;"><a href="'.$tconfig['tsite_url'].'index.php?file=c-user_profile&iMemberId='.$Data['iDriverId'].'">'.$vLabel['LBL_CLICK_HERE'].'</a> '.$vLabel['LBL_PROFILE_DRIVER'].'</span>
			</td>
			</tr>
			</table>
			</td>
			</tr>
			</table>';
			return $mailcont;
		}

		function email_cont_admin($Data){
			global $obj, $tconfig, $generalobj;

			$sql = "SELECT vImage, eGender FROM member WHERE iMemberId = '".$Data['iBookerId']."'";
			$db_booker = $obj->MySQLSelect($sql);

			if(is_file($tconfig["tsite_upload_images_member_path"].$Data['iBookerId']."/1_".$db_booker[0]['vImage']))
			{
				$bookerimage = $tconfig["tsite_upload_images_member"].$Data['iBookerId']."/1_".$db_booker[0]['vImage'];
			}
			else
			{
				if($db_booker[0]['eGender']=='Male'){
					$bookerimage = $tconfig["tsite_images"].'64x64male.png';
					}else{
					$bookerimage = $tconfig["tsite_images"].'64x64female.png';
				}
			}

			$sql = "SELECT vImage, eGender FROM member WHERE iMemberId = '".$Data['iDriverId']."'";
			$db_driver = $obj->MySQLSelect($sql);

			if(is_file($tconfig["tsite_upload_images_member_path"].$Data['iDriverId']."/1_".$db_driver[0]['vImage']))
			{
				$driverimage = $tconfig["tsite_upload_images_member"].$Data['iDriverId']."/1_".$db_driver[0]['vImage'];
			}
			else
			{
				if($db_driver[0]['eGender']=='Male'){
					$driverimage = $tconfig["tsite_images"].'64x64male.png';
					}else{
					$driverimage = $tconfig["tsite_images"].'64x64female.png';
				}
			}

			$mailcont .= '<table width="600" style="border:1px solid #cccccc;border-collapse: collapse;background-color: #FFFFFF;">
			<tr>
			<td width="100%" colspan="2" style="background:#FFF;color:#000;padding:10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">Ride booking successful</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" style="background:'.SITE_COLOR.';color:#FFF;padding:10px;text-align:left;font-size:17px;font-family:Arial, Helvetica, sans-serif;">Booking Details of No.#'.$Data['vBookingNo'].'</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" valign="top" style="color:'.SITE_COLOR.';padding: 5px 0 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">From Location: </span> <img src="'.$tconfig['tsite_images'].'/search-from-plot1.png">&nbsp;'.$Data['vFromPlace'].'
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" valign="top" style="color:'.SITE_COLOR.';padding: 5px 0 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">To Location: </span> <img src="'.$tconfig['tsite_images'].'/search-to-plot1.png">&nbsp;'.$Data['vToPlace'].'
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" valign="top" style="color:'.SITE_COLOR.';padding: 5px 0 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">Booking Date: </span> '.$generalobj->DateTimeFormat($Data['dBookingDate']).'&nbsp;&nbsp;<span style="color:#000;">Time: </span> '.$this->DateTimenew($Data['dBookingTime'],12).' (Approx)
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" valign="top" style="color:'.SITE_COLOR.';padding: 5px 0 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">No. of Seat(s): </span> '.$Data['iNoOfSeats'].'&nbsp;&nbsp;&nbsp;<span style="color:#000;">Price: </span> '.$Data['vBookerCurrencyCode'].' '.$Data['fAmount'].'
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" valign="top" style="color:'.SITE_COLOR.';padding: 5px 0 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">Main Trip route: </span> '.$Data['vMainRidePlaceDetails'].'
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" style="background:'.SITE_COLOR.';color:#FFF;padding:10px;text-align:left;font-size:17px;font-family:Arial, Helvetica, sans-serif;">Passenger(Booker) Details</td>
			</tr>
			<tr>
			<td width="12%" valign="top" style="border-bottom:0px solid #cccccc;">
			<img style="margin-top: 5px;border-radius: 3px;border-bottom: 5px solid '.SITE_COLOR.';" alt="" src="'.$bookerimage.'">
			</td>
			<td width="88%" valign="top" style="border-bottom:0px solid #cccccc;">
			<table width="100%">
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">Name:</span> '.$Data['vBookerFirstName'].' '.$Data['vBookerLastName'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">Email:</span> '.$Data['vBookerEmail'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">Phone:</span> '.$Data['vBookerPhone'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;"><a href="'.$tconfig['tsite_url'].'index.php?file=c-user_profile&iMemberId='.$Data['iBookerId'].'">Click here</a> to view profile of passenger.</span>
			</td>
			</tr>
			</table>
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" style="background:'.SITE_COLOR.';color:#FFF;padding:10px;text-align:left;font-size:17px;font-family:Arial, Helvetica, sans-serif;">Driver Details</td>
			</tr>
			<tr>
			<td width="12%" valign="top" style="border-bottom:1px solid #cccccc;">
			<img style="margin-top: 5px;border-radius: 3px;border-bottom: 5px solid '.SITE_COLOR.';" alt="" src="'.$driverimage.'">
			</td>
			<td width="88%" valign="top" style="border-bottom:0px solid #cccccc;">
			<table width="100%">
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">Name:</span>'.$Data['vDriverFirstName'].' '.$Data['vDriverLastName'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">Email:</span> '.$Data['vDriverEmail'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">Phone:</span> '.$Data['vDriverPhone'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;"><a href="'.$tconfig['tsite_url'].'index.php?file=c-user_profile&iMemberId='.$Data['iDriverId'].'">Click here</a> to view profile of driver.</span>
			</td>
			</tr>
			</table>
			</td>
			</tr>
			</table>';
			return $mailcont;
		}

		function get_time_zone($lat, $long){
			$url = 'https://maps.googleapis.com/maps/api/timezone/json?location='.$lat.','.$long.'&timestamp=1331766000&sensor=true';

			$ch = curl_init();
			$timeout = 0;

			curl_setopt ($ch, CURLOPT_URL, $url);
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch,  CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
			curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

			$rawdata = curl_exec($ch);
			curl_close($ch);
			$data = json_decode($rawdata, true);

			return $data['timeZoneId'];
		}

		function email_cont_booker_confirmation($Data, $type){
			global $obj, $tconfig, $generalobj;

			$sql = "SELECT vImage, eGender FROM member WHERE iMemberId = '".$Data['iBookerId']."'";
			$db_booker = $obj->MySQLSelect($sql);

			if(is_file($tconfig["tsite_upload_images_member_path"].$Data['iBookerId']."/1_".$db_booker[0]['vImage']))
			{
				$bookerimage = $tconfig["tsite_upload_images_member"].$Data['iBookerId']."/1_".$db_booker[0]['vImage'];
			}
			else
			{
				if($db_booker[0]['eGender']=='Male'){
					$bookerimage = $tconfig["tsite_images"].'64x64male.png';
					}else{
					$bookerimage = $tconfig["tsite_images"].'64x64female.png';
				}
			}

			$sql = "SELECT vImage, eGender, vLanguageCode FROM member WHERE iMemberId = '".$Data['iDriverId']."'";
			$db_driver = $obj->MySQLSelect($sql);

			# Get Driver Language #
			if($type=="Driver")
			{
				$vLanguageCode = $db_driver[0]['vLanguageCode'];
				}else{
				$vLanguageCode = "EN";
			}

			$sql = "SELECT * from language_label WHERE vCode = '".$vLanguageCode."'";
			$db_label = $obj->MySQLSelect($sql);

			$vLabel = array();
			for($i=0;$i<count($db_label);$i++){
				$vLabel[$db_label[$i]['vLabel']]  = $db_label[$i]["vValue"];
			}
			# Get Driver Language #

			if(is_file($tconfig["tsite_upload_images_member_path"].$Data['iDriverId']."/1_".$db_driver[0]['vImage']))
			{
				$driverimage = $tconfig["tsite_upload_images_member"].$Data['iDriverId']."/1_".$db_driver[0]['vImage'];
			}
			else
			{
				if($db_driver[0]['eGender']=='Male'){
					$driverimage = $tconfig["tsite_images"].'64x64male.png';
					}else{
					$driverimage = $tconfig["tsite_images"].'64x64female.png';
				}
			}

			if($Type == 'Admin'){
				$headercont = 'Hello Administrator, <br>Booker send confirmation of his/her ride completion. For payment process of driver please review below given ride details and booking details.';
				}else{
				$headercont = ''.$vLabel['LBL_HELLO'].' '.$Data['vDriverFirstName'].', <br>'.$Data['vBookerFirstName'].' '.$vLabel['LBL_CONFIRMATION_RIDE'].' ';
			}

			$mailcont .= '<table width="600" style="border:1px solid #cccccc;border-collapse: collapse;background-color: #FFFFFF;">
			<tr>
			<td width="100%" colspan="2" style="background:#FFF;color:'.SITE_COLOR.';padding:10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">'.$headercont.'</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" style="background:'.SITE_COLOR.';color:#FFF;padding:10px;text-align:left;font-size:17px;font-family:Arial, Helvetica, sans-serif;">'.$vLabel['LBL_BOOKINGDETAIL_NO'].'&nbsp;#'.$Data['vBookingNo'].'</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" valign="top" style="color:'.SITE_COLOR.';padding: 5px 0 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_FROM_LOCATION'].': </span> <img src="'.$tconfig['tsite_images'].'/search-from-plot1.png">&nbsp;'.$Data['vFromPlace'].'
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" valign="top" style="color:'.SITE_COLOR.';padding: 5px 0 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_TO_LOCATION'].': </span> <img src="'.$tconfig['tsite_images'].'/search-to-plot1.png">&nbsp;'.$Data['vToPlace'].'
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" valign="top" style="color:'.SITE_COLOR.';padding: 5px 0 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_BOOKING_DATE'].': </span> '.$generalobj->DateTimeFormat($Data['dBookingDate']).'&nbsp;&nbsp;<span style="color:#000;">'.$vLabel['LBL_TIME'].': </span> '.$this->DateTimenew($Data['dBookingTime'],12).' ('.$vLabel['LBL_APPROX_TIME'].')
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" valign="top" style="color:'.SITE_COLOR.';padding: 5px 0 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_BOOKING_SEATS'].': </span> 1&nbsp;&nbsp;&nbsp;<span style="color:#000;">'.$vLabel['LBL_PRICE'].': </span> '.$Data['vBookerCurrencyCode'].' '.$Data['fAmount'].'
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" valign="top" style="color:'.SITE_COLOR.';padding: 5px 0 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_MAIN_TRIP_ROUTE'].': </span> '.$Data['vMainRidePlaceDetails'].'
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" style="background:'.SITE_COLOR.';color:#FFF;padding:10px;text-align:left;font-size:17px;font-family:Arial, Helvetica, sans-serif;">'.$vLabel['LBL_BOOKER_DETAILS'].'</td>
			</tr>
			<tr>
			<td width="12%" valign="top" style="border-bottom:0px solid #cccccc;">
			<img style="margin-top: 5px;border-radius: 3px;border-bottom: 5px solid '.SITE_COLOR.';" alt="" src="'.$bookerimage.'">
			</td>
			<td width="88%" valign="top" style="border-bottom:0px solid #cccccc;">
			<table width="100%">
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_NAME'].':</span> '.$Data['vBookerFirstName'].' '.$Data['vBookerLastName'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_EMAIL'].':</span> '.$Data['vBookerEmail'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_PHONE'].':</span> '.$Data['vBookerPhone'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;"><a href="'.$tconfig['tsite_url'].'index.php?file=c-user_profile&iMemberId='.$Data['iBookerId'].'">'.$vLabel['LBL_CLICK_HERE'].'</a> '.$vLabel['LBL_PROFILE_BOOKER'].'</span>
			</td>
			</tr>
			</table>
			</td>
			</tr>
			<tr>
			<td width="100%" colspan="2" style="background:'.SITE_COLOR.';color:#FFF;padding:10px;text-align:left;font-size:17px;font-family:Arial, Helvetica, sans-serif;">'.$vLabel['LBL_DRIVER_DETAILS'].'</td>
			</tr>
			<tr>
			<td width="12%" valign="top" style="border-bottom:1px solid #cccccc;">
			<img style="margin-top: 5px;border-radius: 3px;border-bottom: 5px solid '.SITE_COLOR.';" alt="" src="'.$driverimage.'">
			</td>
			<td width="88%" valign="top" style="border-bottom:0px solid #cccccc;">
			<table width="100%">
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_NAME'].':</span>'.$Data['vDriverFirstName'].' '.$Data['vDriverLastName'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_EMAIL'].':</span> '.$Data['vDriverEmail'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;">'.$vLabel['LBL_PHONE'].':</span> '.$Data['vDriverPhone'].'
			</td>
			</tr>
			<tr>
			<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
			<span style="color:#000;"><a href="'.$tconfig['tsite_url'].'index.php?file=c-user_profile&iMemberId='.$Data['iDriverId'].'">'.$vLabel['LBL_CLICK_HERE'].'</a> '.$vLabel['LBL_PROFILE_DRIVER'].'</span>
			</td>
			</tr>
			</table>
			</td>
			</tr>
			</table>';
			return $mailcont;
		}

		function get_user_country($lat, $long){
			//$lat = '59.913869';
			//$long = '10.752245';
			$url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.$lat.','.$long.'&sensor=true';
			$ch = curl_init();
			$timeout = 0;

			curl_setopt ($ch, CURLOPT_URL, $url);
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch,  CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
			curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

			$rawdata = curl_exec($ch);
			curl_close($ch);
			$data = json_decode($rawdata, true);
			$rb = $data['results'][0]['address_components'];
			for($i=0;$i<count($rb);$i++){
				if($rb[$i]['types'][0] == 'country'){
					return $rb[$i]['short_name'];
				}
			}

			$country = $data['results'][0]['address_components'][7]['long_name'];
			$country_code = $data['results'][0]['address_components'][7]['short_name'];
			return $country_code;
		}

		function get_alert_mail_cont($stack,$vLanguageCode){
			global $obj, $tconfig, $generalobj;

			# For user language label
			$sql = "SELECT * from language_label WHERE vCode = '".$vLanguageCode."'";
			$db_label = $obj->MySQLSelect($sql);
			$vLabel = array();
			for($i=0;$i<count($db_label);$i++){
				$vLabel[$db_label[$i]['vLabel']]  = $db_label[$i]["vValue"];
			}
			# For user language label

			$cont = '';
			for($a=0;$a<count($stack);$a++){
				$sql = "SELECT * FROM rides_new WHERE iRideId = '".$stack[$a]['iRideId']."'";
				$db_ride_data = $obj->MySQLSelect($sql);

				$stack[$a]['seats'] = $db_ride_data[0]['iSeats'];
				$stack[$a]['ridestatus'] = $db_ride_data[0]['eStatus'];
				$stack[$a]['eLadiesOnly'] = $db_ride_data[0]['eLadiesOnly'];
				$stack[$a]['eRidePlaceType'] = $db_ride_data[0]['eRidePlaceType'];
				$stack[$a]['vBookerCurrencyCode'] = $db_ride_data[0]['vBookerCurrencyCode'];

				$sql = "SELECT member.vFirstName, member.vLastName, member.vImage, member.eGender, member.iBirthYear
                FROM member
                WHERE iMemberId = '".$db_ride_data[0]['iMemberId']."'";
				$db_member = $obj->MySQLSelect($sql);

				$stack[$a]['name'] = $db_member[0]['vFirstName'].' '.$db_member[0]['vLastName'];

				if(is_file($tconfig["tsite_upload_images_member_path"].$db_ride_data[0]['iMemberId']."/1_".$db_member[0]['vImage']))
				{
					$stack[$a]['image'] = $tconfig["tsite_upload_images_member"].$db_ride_data[0]['iMemberId']."/1_".$db_member[0]['vImage'];
					$stack[$a]['imgavail'] = 'Yes';
				}
				else
				{
					$stack[$a]['imgavail'] = 'No';

					if($db_member[0]['eGender'] == 'Male'){
						$stack[$a]['image'] = $tconfig['tsite_images'].'64x64male.png';
						}else{
						$stack[$a]['image'] = $tconfig['tsite_images'].'64x64female.png';
					}
				}

				$rb = range($stack[$a]['start_iRidePointId'], $stack[$a]['end_iRidePointId']);

				$rb1 = array($stack[$a]['start_iRidePointId'], $stack[$a]['end_iRidePointId']);

				$sql = "SELECT iRidePointId, vStartPoint, vEndPoint, vDistance, vDuration, fPrice, ePriceType, fOriginalPrice FROM ride_points_new WHERE iRideId = '".$stack[$a]['iRideId']."' AND eReverse = '".$stack[$a]['return']."' ORDER BY iRidePointId ASC";
				$db_point_data = $obj->MySQLSelect($sql);

				$places_str = '';
				$price = 0;
				$originalprice = 0;
				$minutes = 0;
				for($x=0;$x<count($db_point_data);$x++){
					if($stack[$a]['start_iRidePointId'] == $db_point_data[$x]['iRidePointId']){
						$stack[$a]['departure'] = $db_point_data[$x]['vStartPoint'];
					}

					if($stack[$a]['end_iRidePointId'] == $db_point_data[$x]['iRidePointId']){
						$stack[$a]['arrival'] = $db_point_data[$x]['vEndPoint'];
					}

					if(count($db_point_data) == 1){
						$places_str = $db_point_data[$x]['vStartPoint'].' &rarr; '.$db_point_data[$x]['vEndPoint'];
						}else{
						if(count($db_point_data)-1 == $x){
							$places_str .= ' &rarr; '.$db_point_data[$x]['vStartPoint'].' &rarr; '.$db_point_data[$x]['vEndPoint'];
							}else{
							if($x != 0){
								$arrrow = ' &rarr; ';
								}else{
								$arrrow = '';
							}
							$places_str .= $arrrow.$db_point_data[$x]['vStartPoint'];
						}
					}


					if (in_array($db_point_data[$x]['iRidePointId'], $rb)){
						$price = $price + $db_point_data[$x]['fPrice'];
						$originalprice = $originalprice + $db_point_data[$x]['fOriginalPrice'];
						$dur = explode(' ',$db_point_data[$x]['vDuration']);
						if (strpos($db_point_data[$x]['vDuration'],'hour')) {
							$dur = explode(' ',$db_point_data[$x]['vDuration']);
							$minutes = $minutes + ($dur[0]*60) + $dur[2];
							}else{
							$dur = explode(' ',$db_point_data[$x]['vDuration']);
							$minutes = $minutes + $dur[0];
						}

						if($ePriceType != 'High'){
							$ePriceType = $db_point_data[$x]['ePriceType'];
						}
					}
				}
				$stack[$a]['main_addr'] = $places_str;

				$price = $price * $db_ride_data[0]['fRatio_'.$_SESSION['sess_price_ratio']];
				$stack[$a]['pricesort'] = $price;
				$stack[$a]['price'] = $stack[$a]['vBookerCurrencyCode'].' '.round($price);
				$stack[$a]['duration'] = $minutes;
				$stack[$a]['ePriceType'] = $ePriceType;

				if($db_ride_data[0]['eRideType'] == 'Reccuring'){
					if($stack[$a]['return'] == 'Yes'){
						$time = $db_ride_data[0]['vMainReturnTime'];
						}else{
						$time = $db_ride_data[0]['vMainOutBoundTime'];
					}
					}else{
					if($stack[$a]['return'] == 'Yes'){
						$time = $db_ride_data[0]['vMainArrivalTime'];
						}else{
						$time = $db_ride_data[0]['vMainDepartureTime'];
					}
				}
				$stack[$a]['time'] = date("H:i", strtotime($stack[$a]['tRideTime']));
				$stack[$a]['link'] = $tconfig['tsite_url'].'index.php?file=c-ride_details&from=home&id='.$stack[$a]['iRideId'].'&dt='.strtotime($stack[$a]['alertDate']).'&ret='.$stack[$a]['return'];
				$cont .= '<tr>
				<td width="100%" colspan="2" style="background:'.SITE_COLOR.';color:#FFF;padding:10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">'.$stack[$a]['main_addr'].'</td>
                </tr>
                <tr>
				<td width="12%" valign="top" style="border-bottom:6px solid #D6D6D6;">
				<img style="margin-top: 5px;border-radius: 3px;" alt="" src="'.$stack[$a]['image'].'">
				</td>
				<td width="88%" valign="top" style="border-bottom:6px solid #D6D6D6;">
				<table width="100%">
				<tr>
				<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
				<span style="color:#000;">'.$vLabel['LBL_NEAREST_LOCATION'].':</span> '.$stack[$a]['departure'].'
				</td>
				</tr>
				<tr>
				<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
				<span style="color:#000;">'.$vLabel['LBL_NEAREST_ARRIVAL_LOCATION'].':</span> '.$stack[$a]['arrival'].'
				</td>
				</tr>
				<tr>
				<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
				<span style="color:#000;">'.$vLabel['LBL_DATE'].':</span> '.$generalobj->DateTimeFormat($stack[$a]['alertDate']).' <span style="color:#D6D6D6;">&#8226;</span> <span style="color:#000;">'.$vLabel['LBL_TIME'].': </span>'.$stack[$a]['time'].' <span style="color:#D6D6D6;">&#8226;</span> <span style="color:#000;">'.$vLabel['LBL_PRICE'].':</span> '.$stack[$a]['price'].'
				</td>
				</tr>
				<tr>
				<td valign="top" style="color:'.SITE_COLOR.';padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
				<span style="color:#000;">'.$vLabel['LBL_OFFERED_BY'].' :</span> '.$stack[$a]['name'].' <span style="float:right;"><a href="'.$stack[$a]['link'].'" style="border-radius: 3px;background: none repeat scroll 0 0 #000000;color: #FFFFFF;float: right;font-size: 14px;margin: 0;padding: 5px 10px;text-decoration: none;border-bottom: 5px solid '.SITE_COLOR.';" target="_blank">'.$vLabel['LBL_VIEW_DETAILS'].' &raquo;</a></span>
				</td>
				</tr>
				</table>
				</td>
                </tr>';
			}
			return $cont;
		}

		function getBrowser()
		{
			$u_agent = $_SERVER['HTTP_USER_AGENT'];
			$bname = 'Unknown';
			$platform = 'Unknown';
			$version= "";

			//First get the platform?
			if (preg_match('/linux/i', $u_agent)) {
				$platform = 'linux';
			}
			elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
				$platform = 'mac';
			}
			elseif (preg_match('/windows|win32/i', $u_agent)) {
				$platform = 'windows';
			}

			// Next get the name of the useragent yes seperately and for good reason
			if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
			{
				$bname = 'Internet Explorer';
				$ub = "MSIE";
			}
			elseif(preg_match('/Firefox/i',$u_agent))
			{
				$bname = 'Mozilla Firefox';
				$ub = "Firefox";
			}
			elseif(preg_match('/Chrome/i',$u_agent))
			{
				$bname = 'Google Chrome';
				$ub = "Chrome";
			}
			elseif(preg_match('/Safari/i',$u_agent))
			{
				$bname = 'Apple Safari';
				$ub = "Safari";
			}
			elseif(preg_match('/Opera/i',$u_agent))
			{
				$bname = 'Opera';
				$ub = "Opera";
			}
			elseif(preg_match('/Netscape/i',$u_agent))
			{
				$bname = 'Netscape';
				$ub = "Netscape";
			}

			// finally get the correct version number
			$known = array('Version', $ub, 'other');
			$pattern = '#(?<browser>' . join('|', $known) .
			')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
			if (!preg_match_all($pattern, $u_agent, $matches)) {
				// we have no matching number just continue
			}

			// see how many we have
			$i = count($matches['browser']);
			if ($i != 1) {
				//we will have two since we are not using 'other' argument yet
				//see if version is before or after the name
				if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
					$version= $matches['version'][0];
				}
				else {
					$version= $matches['version'][1];
				}
			}
			else {
				$version= $matches['version'][0];
			}

			// check if we have a number
			if ($version==null || $version=="") {$version="?";}

			return array(
            'userAgent' => $u_agent,
            'name'      => $bname,
            'version'   => $version,
            'platform'  => $platform,
            'pattern'   => $pattern
			);
		}

		function set_default_language_currency(){
			/*$extension = pathinfo($_SERVER['SERVER_NAME'], PATHINFO_EXTENSION);
				if($extension == 'uk.com'){           //UK
				$_SESSION['sess_lang'] = "EN";
				$_SESSION['sess_price_ratio'] = 'GBP';
				}else if($extension == 'no'){        //Norway
				$_SESSION['sess_lang'] = "NO";
				$_SESSION['sess_price_ratio'] = 'NOK';
				}else if($extension == 'pl'){        //Poland
				$_SESSION['sess_lang'] = "PO";
				$_SESSION['sess_price_ratio'] = 'PLN';
				}else if($extension == 'fi'){        //Finland
				$_SESSION['sess_lang'] = "FI";
				$_SESSION['sess_price_ratio'] = 'EUR';
				}else if($extension == 'com.ru'){    //Russia
				$_SESSION['sess_lang'] = "RS";
				$_SESSION['sess_price_ratio'] = 'RUB';
				}else if($extension == 'se'){        //Sweden
				$_SESSION['sess_lang'] = "SW";
				$_SESSION['sess_price_ratio'] = 'SEK';
				}else if($extension == 'dk'){        //Denmark
				$_SESSION['sess_lang'] = "DN";
				$_SESSION['sess_price_ratio'] = 'DKK';
				}else if($extension == 'de'){        //Germany
				$_SESSION['sess_lang'] = "DE";
				$_SESSION['sess_price_ratio'] = 'EUR';
				}else if($extension == 'fr'){        //France
				$_SESSION['sess_lang'] = "FN";
				$_SESSION['sess_price_ratio'] = 'EUR';
				}else if($extension == 'es'){     //Italy
				$_SESSION['sess_lang'] = "ES";
				$_SESSION['sess_price_ratio'] = 'EUR';
				}else{
				$_SESSION['sess_lang'] = "EN";
				$_SESSION['sess_price_ratio'] = 'USD';
			}*/

			$_SESSION['sess_lang'] = LANG_MASTER_DEF_LANG;
			$_SESSION['sess_price_ratio'] = CURR_MASTER_DEF_CURR;
		}

		function set_user_selected_language($lang){
			$_SESSION['sess_lang']	=	$lang;
			/*if($_SESSION['sess_lang'] == 'EN'){              //UK
				//$_SESSION['sess_price_ratio'] = 'GBP';
				$_SESSION['sess_price_ratio'] = 'USD';
				}else if($_SESSION['sess_lang'] == 'NO'){        //Norway
				$_SESSION['sess_price_ratio'] = 'NOK';
				}else if($_SESSION['sess_lang'] == 'PO'){        //Poland
				$_SESSION['sess_price_ratio'] = 'PLN';
				}else if($_SESSION['sess_lang'] == 'FI'){        //Finland
				$_SESSION['sess_price_ratio'] = 'EUR';
				}else if($_SESSION['sess_lang'] == 'RS'){        //Russia
				$_SESSION['sess_price_ratio'] = 'RUB';
				}else if($_SESSION['sess_lang'] == 'SW'){        //Sweden
				$_SESSION['sess_price_ratio'] = 'SEK';
				}else if($_SESSION['sess_lang'] == 'DN'){        //Denmark
				$_SESSION['sess_price_ratio'] = 'DKK';
				}else if($_SESSION['sess_lang'] == 'DE'){        //Germany
				$_SESSION['sess_price_ratio'] = 'EUR';
				}else if($_SESSION['sess_lang'] == 'FN'){        //France
				$_SESSION['sess_price_ratio'] = 'EUR';
				}else if($_SESSION['sess_lang'] == 'ES'){        //Italy
				$_SESSION['sess_price_ratio'] = 'EUR';
				}else{
				$_SESSION['sess_price_ratio'] = 'USD';
			}*/

			$db_lng_mst = unserialize(db_lng_mst);

			for($i=0;$i<count($db_lng_mst);$i++){
				if($_SESSION['sess_lang'] == $db_lng_mst[$i]['vCode']){
					$_SESSION['sess_price_ratio'] = $db_lng_mst[$i]['vCurrencyCode'];
					break;
				}
			}

			//$_SESSION['sess_price_ratio'] = 'EUR';
			if($_SESSION['ride_offer']['second_page'] == 'Yes'){
				$_SESSION['ride_offer']['go_to_second'] = 'Yes';
				header("Location:".$tconfig['tsite_url'].'offer-ride');
				exit;
				}else{
				$posturi = $_SERVER['HTTP_REFERER'];
				header("Location:".$posturi);
				exit;
			}
		}

		function set_user_selected_currncy($currency){
			$_SESSION['sess_price_ratio']	= $currency;
			//$_SESSION['ride_offer']			= array();

			$posturi = $_SERVER['HTTP_REFERER'];
			header("Location:".$posturi);
			exit;
			#echo "fdgdfg".$currency; exit;
			/*$_SESSION['sess_price_ratio']	=	$currency;
				if($_SESSION['ride_offer']['second_page'] == 'Yes'){
				$_SESSION['ride_offer']['go_to_second'] = 'Yes';
				header("Location:".$tconfig['tsite_url'].'offer-ride');
				exit;
				}else{
				$posturi = $_SERVER['HTTP_REFERER'];
				header("Location:".$posturi);
				exit;
				}
			}*/
			/*  $_SESSION['sess_price_ratio']	=	$currency;
				//echo "<pre>";print_r($_SERVER);
			$str = explode('-',$_SERVER['HTTP_REFERER']); */
			//echo '<pre>'; print_r($str);
			/* if($_SESSION['ride_offer']['second_page'] == 'Yes'){
				$_SESSION['ride_offer']['go_to_second'] = 'Yes';
				if($str[1] == 'offer_ride_second')
				{
				header("Location:".$tconfig['tsite_url'].'offer-ride');
				}
				if($str[1] == 'offer_ride_second_edit')
				{
				header("Location:index.php?file=m-offer_ride_edit");
				}
				exit;
				}else{
				$posturi = $_SERVER['HTTP_REFERER'];
				header("Location:".$posturi);
				exit;
			}   */
		}
	}
?>
