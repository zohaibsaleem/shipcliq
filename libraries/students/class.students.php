<?php
  Class Students
  {
    function active_volonteers(){
      global $obj, $tconfig;  
      
      $sql = "SELECT COUNT(iMemberId) as tot_members FROM member WHERE eStatus = 'Active'";
      $db_memb = $obj->MySQLSelect($sql);
      
      return $db_memb[0]['tot_members'];
    }
    
    function hours_served(){
      global $obj, $tconfig;  
      
      $sql = "SELECT SUM(dHours) as tot_hours FROM event_volunteer";
      $db_hours = $obj->MySQLSelect($sql);
      
      return round($db_hours[0]['tot_hours']);
    } 
    
    function voluteer_total_points($iMemberId){
      global $obj, $tconfig;  
      
      $sql = "SELECT SUM(fPoints) as tot_points FROM event_volunteer WHERE eStatus = 'Accepted' AND iMemberId = '".$iMemberId."'";
      $db_points = $obj->MySQLSelect($sql);
      
      return round($db_points[0]['tot_points']);
    }  
      
     
    function get_countires(){
      global $obj, $tconfig;
      
      $sql = "SELECT vCountryCode, vCountry FROM country WHERE eStatus = 'Active'";
      $db_countries = $obj->MySQLSelect($sql);
      //echo "<pre>"; print_r($db_countries);
      return $db_countries;
    }
    
    function profile_details($iMemberId){
      global $obj, $tconfig;
      
      $sql = "SELECT * FROM member WHERE iMemberId = '".$iMemberId."'";
      $db_volunteer = $obj->MySQLSelect($sql);
    	
      if($db_volunteer[0]['vCountry'] != '')
      {
         $vCountry = $db_volunteer[0]['vCountry'];
      }
      else
      {
        $vCountry = 'US';
      }
      
    /*  if($db_volunteer[0]['vCountry'] = '')
      {
         $db_volunteer[0]['vCountry'] = 'US';
      } */
      
      
      if(is_file($tconfig["tsite_upload_images_member_path"].$db_volunteer[0]['iMemberId']."/1_".$db_volunteer[0]['vImage']))
    	{
    		$db_volunteer[0]['img_url'] = $tconfig["tsite_upload_images_member"].$db_volunteer[0]['iMemberId']."/1_".$db_volunteer[0]['vImage'];
    	} 
    	else
    	{
    		$db_volunteer[0]['img_url'] = $tconfig["tsite_images"].'volunteer.jpg';
    	}  
          
      return $db_volunteer; 
     
    } 
    
    function check_volunteer(){
      global $site_url; 
      
      if($_SESSION['sess_iMemberId'] == ""){
        header("Location:".$site_url."index.php?file=m-logout");
        exit;
			}
     }
    
    function skills(){
      global $obj, $tconfig;
      
      $sql = "SELECT iSkillId, vTitle FROM skills WHERE eStatus = 'Active'";
      $db_skills = $obj->MySQLSelect($sql);
      
      return $db_skills;
    } 
    
     function volunteer_skills($iMemberId){
      global $obj, $tconfig;
      
      $sql = "SELECT iSkillId FROM volunteer_skills WHERE iMemberId = '".$iMemberId."'";
      $volunteer_skills = $obj->MySQLSelect($sql);
      
      return $volunteer_skills; 
    }
   
    function my_profile_skills($iMemberId){
     global $obj, $tconfig;
                 
      $sql = "SELECT skills.vTitle FROM skills LEFT JOIN volunteer_skills ON skills.iSkillId =  volunteer_skills.iSkillId WHERE iMemberID = '".$iMemberId."'";                
      $my_profile_skills = $obj->MySQLSelect($sql);
                    
       return $my_profile_skills; 
      }                   
   
    function interest_areas_registration(){
     global $obj, $tconfig;
      
    $sql = "SELECT iCategoryId, vCategory FROM category WHERE eStatus = 'Active'";
    $db_categories = $obj->MySQLSelect($sql);
      
    return $db_categories;
    } 
    
    function volunteer_interest_areas($iMemberId){
      global $obj, $tconfig;
      
      $sql = "SELECT volunteer_interests.iCategoryId as iCategoryId,vCategoryImage FROM volunteer_interests left join category on category.iCategoryId  = volunteer_interests.iCategoryId WHERE iMemberId = '".$iMemberId."'";
      $db_volunteer_interest = $obj->MySQLSelect($sql);
      
      if(count($db_volunteer_interest) > 0){
      for($i=0;$i<count($db_volunteer_interest);$i++){
      if(is_file($tconfig["tsite_upload_images_category_images_path"].$db_volunteer_interest[$i]['vCategoryImage']))
    	{
    		$db_volunteer_interest[$i]['img_url'] = $tconfig["tsite_upload_images_category_images"].$db_volunteer_interest[$i]['vCategoryImage'];
    	} 
    	else
    	{
    		$db_volunteer_interest[$i]['img_url'] = $tconfig["tsite_images"].'volunteer.jpg';
    	}     
     }
    }
    return $db_volunteer_interest;
    }
    
     
    function VolunteerChangePwd()
  	{
  		global $obj,$generalobj,$tconfig;
  		$vOldPassword = $generalobj->encrypt(trim($_POST[vOldPassword]));
  		$sql="select iMemberId from member where vPassword = '".$vOldPassword."' and iMemberId ='".$_SESSION['sess_iMemberId']."'";
  		$db_member = $obj->MySQLSelect($sql);
  	  if($db_member)
  		{
  			$sql="update member set vPassword ='".$generalobj->encrypt(trim($_POST['vPassword']))."' where vPassword = '".$vOldPassword."' and iMemberId = '".$_SESSION['sess_iMemberId']."'";
  			$db_MemUpdate = $obj->sql_query($sql);
  			if($db_MemUpdate)
  			{
          $msg = "Your password has been changed successfully.";
          $error_msg = "0";
  			}
  			else
  			{
  				$msg = "Error occured during changing your password.Try again.";
          $error_msg = "1";
  			}
  		}
  		else
  		{
  			$msg = "Sorry..You have provided invalid old password.Please try again.";
        $error_msg = "1";
  		}
  		header("location:".$tconfig['tsite_url']."index.php?file=m-changepassword&var_msg=".$msg."&msg_code=".$error_msg);
  		exit;
  	}         
  
     function attended_events_list($iMemberId){
      global $obj, $tconfig;  
      
      $curr = date("Y-m-d H:i:s");
      $sql = "SELECT event.iEventId, event.vTitle, event.tDetails, event.vCity, event.vImage, event.dEndDate, event_volunteer.iMemberId,event_volunteer.fPoints,event_volunteer.dHours,event_volunteer.eStatus,organization.vOrganizationName,(SELECT state.vState FROM state WHERE state.vStateCode = event.vState AND state.vCountryCode = event.vCountry) as vState                   
              FROM event               
              LEFT JOIN event_volunteer ON event_volunteer.iEventId = event.iEventId    
              LEFT JOIN organization ON organization.iOrganizationId = event.iOrganizationId
              WHERE event_volunteer.eStatus = 'Accepted' and event.dEndDate <= '".$curr."' and event_volunteer.iMemberId = '".$iMemberId."'";           
          $db_events_attended = $obj->MySQLSelect($sql); 
          
      if(count($db_events_attended) > 0){
        for($i=0;$i<count($db_events_attended);$i++){    
          if(is_file($tconfig["tsite_upload_images_events_path"].$db_events_attended[$i]['iEventId']."/4_".$db_events_attended[$i]['vImage']))
        	{
        		$db_events_attended[$i]['img_url'] = $tconfig["tsite_upload_images_events"].$db_events_attended[$i]['iEventId']."/4_".$db_events_attended[$i]['vImage'];
        	} 
        	else
        	{
        		$db_events_attended[$i]['img_url'] = $tconfig['tsite_images'].'event-no-image.png';
        	}
        }
      }   
        
      return $db_events_attended;          
    }
  
   function event_subscriptions($iMemberId){
      global $obj, $tconfig;  
    
      $sql = "SELECT event.iEventId, event.vTitle, event.dStartDate, event.eEventType, event_volunteer.iMemberId,event_volunteer.eStatus,organization.vOrganizationName                 
              FROM event               
              LEFT JOIN event_volunteer ON event_volunteer.iEventId = event.iEventId    
              LEFT JOIN organization ON organization.iOrganizationId = event.iOrganizationId
              WHERE event_volunteer.iMemberId = '".$iMemberId."'";           
          $db_event_subscriptions = $obj->MySQLSelect($sql);                 
        return $db_event_subscriptions;          
    } 
    
   function points_history($iMemberId){
      global $obj, $tconfig;  
    
      $sql = "SELECT event.iEventId, event.vTitle, event.dStartDate, event.eEventType, event_volunteer.iMemberId, event_volunteer.eStatus,event_volunteer.fPoints,event_volunteer.dHours,organization.vOrganizationName                 
              FROM event               
              LEFT JOIN event_volunteer ON event_volunteer.iEventId = event.iEventId    
              LEFT JOIN organization ON organization.iOrganizationId = event.iOrganizationId
              WHERE event_volunteer.eStatus = 'Accepted' and event_volunteer.iMemberId = '".$iMemberId."'";           
          $db_points_history = $obj->MySQLSelect($sql); 
                   
        return $db_points_history;          
    }   
  
  }
?> 
 