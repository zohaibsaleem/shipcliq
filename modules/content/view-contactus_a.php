<?php
  #Server side validation start
  include_once(TPATH_LIBRARIES."/general/validation.class.php");
  $validobj = new validation();

  $validobj->add_fields($_POST['Data']['vFirstName'], 'req', LBL_ENTER_NAME_SURNAME);
  $validobj->add_fields($_POST['Data']['vEmail'], 'req', LBL_ENTER_EMAIL_ADDRESS);
  //$validobj->add_fields($_POST['Data']['cellno'], 'req', LBL_ENTER_PHONE);
  $validobj->add_fields($_POST['Data']['tSubject'], 'req', LBL_MESSAGE_REQUIRE);

  $error = $validobj->validate();
  if($error){
  	$generalobj->getPostFormData($_POST,$error,$tconfig['tsite_url']."contactus");
  }
  if($_POST)
	{
    	$Data = $_POST['Data'];
		$Data['dDate']=Date("Y-m-d H:i:s");
		/*---------insert into table-------------------*/
	    $sql="select * from member where vEmail ='".$Data['vEmail']."'";
		$db_member = $obj->MySQLSelect($sql);

		if(count($db_member)>0)
		{
			$Data['eUserType']='User';
		}
		else
		{
			/*$sql="select * from administrators where vEmail ='".$Data['vEmail']."'";
			$db_member = $obj->MySQLSelect($sql);*/

			$Data['eUserType']='Admin';
		}
		$id = $obj->MySQLQueryPerform("contact_us",$Data,'insert');
		if($id)
		{
			$generalobj->send_email_user("CONTACTUS",$Data);
		}
	}

	$msg = LBL_YOUR_INQUIRY_SUCC;
	header("Location:".$tconfig["tsite_url"]."index.php?file=c-contactus&msg_code=1&var_msg=".$msg);
	exit;
?>
