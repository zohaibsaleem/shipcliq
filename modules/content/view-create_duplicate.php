<?php  
  include_once(TPATH_CLASS_APP."class.customer.php");
  $CustomerObj = new Customer;
  $CustomerObj->check_member_login();   
  $sess_iMemberId = $_SESSION["sess_iMemberId"];
  
  $iRideId = $_REQUEST['id'];
  
  $sql = "SELECT * FROM rides_new WHERE iRideId = '".$iRideId."'";
  $db_ride = $obj->MySQLSelect($sql);
  
  $sql = "SELECT * FROM ride_points_new WHERE iRideId = '".$iRideId."' AND eReverse = 'No'";
  $db_ride_points = $obj->MySQLSelect($sql);
  
  //echo "<pre>";
  //print_r($db_ride);
  
  $newarr = array();
  
  $newarr['from'] = $db_ride[0]['vMainDeparture'];
  $newarr['to'] = $db_ride[0]['vMainArrival'];
  
  for($i=0;$i<count($db_ride_points);$i++){
    if($i != 0){
      $newarr['loc'.$i] = $db_ride_points[$i]['vStartPoint'];
    }
  }
  
  if($db_ride[0]['eRideType'] == 'One Time'){
    $newarr['triptype'] = 'onetime';
    $newarr['sdateone'] = ''; //$db_ride[0]['dDepartureDate'];
    $time = array();
    $time = explode(':', $db_ride[0]['vMainDepartureTime']);
    $newarr['onetihourstart'] = $time[0];
    $newarr['onetimeminstart'] = $time[1];
    
    if($db_ride[0]['eRoundTrip'] == 'No'){
      $newarr['roundtriponetime'] = 'No';        
    }else{
      $newarr['roundtriponetime'] = 'Yes';
      $newarr['edateone'] = ''; //$db_ride[0]['dArrivalDate'];
      $time = array();
      $time = explode(':', $db_ride[0]['vMainArrivalTime']);
      $newarr['onetihourend'] = $time[0];
      $newarr['onetimeminend'] = $time[1];
    }
  }else{
    $newarr['triptype'] = 'recurring';
    $newarr['sdate'] = ''; //$db_ride[0]['dStartDate'];
    $newarr['edate'] = ''; //$db_ride[0]['dEndDate'];
    $time = array();
    $time = explode(':', $db_ride[0]['vMainOutBoundTime']);
    $newarr['richourstart'] = $time[0];
    $newarr['ricminstart'] = $time[1];      
    $newarr['outbound'] = explode(",", $db_ride[0]['vOutBoundDays']);
    if($db_ride[0]['eRoundTrip'] == 'No'){
      $newarr['roundtripric'] = 'No';             
    }else{
      $newarr['roundtripric'] = 'Yes';
      $time = array();
      $time = explode(':', $db_ride[0]['vMainReturnTime']);
      $newarr['richourend'] = $time[0];
      $newarr['ricminend'] = $time[1];
      $newarr['retturn'] = explode(",", $db_ride[0]['vReturnDays']); 
    }
  }
  $_SESSION['sess_price_ratio'] = $db_ride[0]['vBookerCurrencyCode'];
  $newarr['second_page'] = 'Yes';
  
  //echo "<pre>";
  //print_r($newarr); exit;
  
  $_SESSION['ride_offer'] = '';
  $_SESSION['ride_offer'] = $newarr;     
  header("Location:".$tconfig["tsite_url"]."offer-ride");
	exit;
?>