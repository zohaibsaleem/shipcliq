<?php 
  $iEventId = $_REQUEST['iEventId'];
  
  $event_check = $eventsobj->event_check($iEventId);    
  $events = $eventsobj->event_common_details($iEventId); 
  $skills = $eventsobj->event_skills_list($iEventId);
  $suitable_for = $eventsobj->event_suitable_for_list($iEventId);  
  $categories = $eventsobj->event_category_list($iEventId); 
  $attending_students = $eventsobj->event_attending_students($iEventId); 
  $organization_reviews = $eventsobj->organization_reviews($events[0]['iOrganizationId']);
  $organization_rerings = $eventsobj->organization_retings($events[0]['iOrganizationId']);
 
  $smarty->assign("iEventId",$iEventId);
  $smarty->assign("events",$events);
  $smarty->assign("skills",$skills);
  $smarty->assign("suitable_for",$suitable_for);
  $smarty->assign("categories",$categories);
  $smarty->assign("attending_students",$attending_students);
  $smarty->assign("organization_reviews",$organization_reviews);  
  $smarty->assign("organization_rerings",$organization_rerings);    
?>