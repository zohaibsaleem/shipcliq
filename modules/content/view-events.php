<?php 
  if($_REQUEST['iCategoryId'] != ''){
    $ssql .= " AND events_category.iCategoryId = '".$_REQUEST['iCategoryId']."'";
  }
  
  if($_REQUEST['iSuitableTypeId'] != ''){
    $ssql .= " AND events_suitabletype.iSuitableTypeId = '".$_REQUEST['iSuitableTypeId']."'";
  }
  
  if($_REQUEST['srchkeyword'] != ''){
    $ssql .= " AND (event.vTitle LIKE '%".stripslashes($_REQUEST['srchkeyword'])."%' OR event.tDetails LIKE '%".stripslashes($_REQUEST['srchkeyword'])."%' OR event.vCity LIKE '%".stripslashes($_REQUEST['srchkeyword'])."%' OR state.vState LIKE '%".stripslashes($_REQUEST['srchkeyword'])."%' OR organization.vOrganizationName LIKE '%".stripslashes($_REQUEST['srchkeyword'])."%')";
  }
  
  if($_REQUEST['ordby'] == ''){
    $ordby = " ORDER BY event.iEventId DESC";
  }else{
    if($_REQUEST['ordby'] == 'event_date'){
      $ordby = " ORDER BY event.dStartDate ASC";
    }else{
      $ordby = " ORDER BY event.dAddedDate DESC";
    }
  }
  
  $sorton 	= $_GET['sorton'];
	$stat  	 	= $_GET['stat'];
	$nstart   = $_GET['nstart'];
	$start   	= $_GET['start'];
	
	if($screen == 'simple'){
    $vImage = 'p.vImage';
  }else{
    $vImage = 'p.vImageRetina';
  }
	
  $events_all = $eventsobj->all_events($ssql, $ordby);     
	$num_totrec = count($events_all);
	$rec_limit = $ACC_REC_LIMIT;
  include(TPATH_LIBRARIES."/general/front.paging.inc.php");
	if(!isset($start))
		$start = 1;
	$num_limit = ($start-1)*$rec_limit;
	$startrec = $num_limit;
	$lastrec = $startrec + $rec_limit;
	$startrec = $startrec + 1;
	if($lastrec > $num_totrec)
		$lastrec = $num_totrec;
	if($num_totrec > 0 )
	{
		$recmsg = "Showing ".$startrec." - ".$lastrec." Event(s) Of ".$num_totrec;
	}
       
  $events = $eventsobj->events_list($ssql, $ordby, $var_limit);     
  $category = $eventsobj->event_catagory();
  $suitables = $eventsobj->event_suitable_for(); 
       
  $smarty->assign("events",$events);  
  $smarty->assign("category",$category);  
  $smarty->assign("suitables",$suitables);  
  $smarty->assign("recmsg",$recmsg);
  $smarty->assign("page_link",$page_link);
  $smarty->assign("srchkeyword",$_REQUEST['srchkeyword']);
  $smarty->assign("iCategoryId",$_REQUEST['iCategoryId']);
  $smarty->assign("iSuitableTypeId",$_REQUEST['iSuitableTypeId']);
  $smarty->assign("ordby",$_REQUEST['ordby']); 
?>