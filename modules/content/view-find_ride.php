<?
function get_prefrence_img($id, $type){
    global $obj, $tconfig;
    
    $sql = "SELECT iPreferencesId, vTitle".$_SESSION['sess_lang_pref']." as vTitle, ".$type."_Lable".$_SESSION['sess_lang_pref']." as Lable, ".$type." FROM preferences WHERE iPreferencesId = '".$id."' ORDER BY iDispOrder ASC";
    $db_preferences = $obj->MySQLSelect($sql);
   
    if(file_exists($tconfig["tsite_upload_images_preferences_path"].'/'.$db_preferences[0][$type])){
      $img_url = $tconfig["tsite_upload_images_preferences"].$db_preferences[0][$type];
      $img_url = '<img alt="" src="'.$tconfig["tsite_upload_images_preferences"].$db_preferences[0][$type].'" title="'.$db_preferences[0]['Lable'].'">';
        
    }else{
      $img_url = ''; 
    }
    
    return $img_url;
  }    

$todaydate = date('Y-m-d');
   
  function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
      $sort_col = array();
      foreach ($arr as $key=> $row) {
          $sort_col[$key] = $row[$col];
      }    
      array_multisort($sort_col, $dir, $arr); 
  }     
  
  $userlatitude =  $_COOKIE['userlatitude'];
  $userlongitude = $_COOKIE['userlongitude'];
  
  if($userlatitude != '' && $userlongitude != ''){
    $sql = "SELECT ride_points_new.iRidePointId, ride_points_new.iRideId, ride_points_new.eReverse, ride_points_new.vStartPoint, ride_points_new.vEndPoint, ride_points_new.fPrice, ride_dates.dDateOut, ride_dates.dDateRet,  
            ( 3959 * acos( cos( radians(".$userlatitude.") ) * cos( radians( ride_points_new.vStartLatitude ) ) * cos( radians( ride_points_new.vStartLongitude ) - radians(".$userlongitude.") ) + sin( radians(".$userlatitude.") ) * sin( radians( ride_points_new.vStartLatitude ) ) ) ) AS distance 
            FROM ride_points_new 
            LEFT JOIN rides_new ON rides_new.iRideId = ride_points_new.iRideId  
            LEFT JOIN ride_dates ON ride_dates.iRideId = ride_points_new.iRideId 
            WHERE rides_new.eStatus = 'Active'  
            HAVING distance < 25 
            ORDER BY RAND()   
            LIMIT 0,5";
    $db_ride_points = $obj->MySQLSelect($sql);
   
    if(count($db_ride_points) > 0){
      $stack = array();
      for($i=0;$i<count($db_ride_points);$i++){      
        $newarr = array(); 
        if($db_ride_points[$i]['eReverse'] == 'No'){
          if($db_ride_points[$i]['dDateOut'] != '0000-00-00' && strtotime($db_ride_points[$i]['dDateOut']) >= strtotime($todaydate)){
            $newarr['dDate'] = $db_ride_points[$i]['dDateOut'];
            $newarr['strtotime'] = strtotime($db_ride_points[$i]['dDateOut']);
            $newarr['iRideId'] = $db_ride_points[$i]['iRideId'];         
            $newarr['return'] = $db_ride_points[$i]['eReverse'];  
            array_push($stack,$newarr);
          }             
        }else{
          if($db_ride_points[$i]['dDateRet'] != '0000-00-00' && strtotime($db_ride_points[$i]['dDateOut']) >= strtotime($todaydate)){
            $newarr['dDate'] = $db_ride_points[$i]['dDateRet'];
            $newarr['strtotime'] = strtotime($db_ride_points[$i]['dDateRet']);
            $newarr['iRideId'] = $db_ride_points[$i]['iRideId'];         
            $newarr['return'] = $db_ride_points[$i]['eReverse'];  
            array_push($stack,$newarr);
          }            
        }
        
      } 
     
      for($i=0;$i<count($stack);$i++){
        $sql = "SELECT rides_new.iMemberId, rides_new.iMemberCarId, rides_new.eRideType, rides_new.vMainDeparture, rides_new.vMainArrival, rides_new.vMainOutBoundTime, rides_new.vMainReturnTime, rides_new.vMainDepartureTime, rides_new.vMainArrivalTime, rides_new.fRatio_".$_SESSION['sess_price_ratio'].", member.eGender, member.vImage   
                FROM rides_new 
                LEFT JOIN member ON member.iMemberId = rides_new.iMemberId   
                WHERE rides_new.iRideId = '".$stack[$i]['iRideId']."' 
                AND rides_new.eStatus = 'Active'";
        $db_ride = $obj->MySQLSelect($sql); 
        
        $sql = "SELECT SUM(fPrice) as tot_price FROM ride_points_new WHERE iRideId = '".$stack[$i]['iRideId']."' AND eReverse = '".$stack[$i]['return']."'";
        $db_price = $obj->MySQLSelect($sql);
        
        $price = $db_price[0]['tot_price'] * $db_ride[0]['fRatio_'.$_SESSION['sess_price_ratio']];       
        $stack[$i]['price'] = $generalobj->booking_currency($price, $_SESSION['sess_price_ratio']);
        
        if(is_file($tconfig["tsite_upload_images_member_path"].$db_ride[0]['iMemberId']."/1_".$db_ride[0]['vImage']))
        {
          $stack[$i]['img'] = $tconfig["tsite_upload_images_member"].$db_ride[0]['iMemberId']."/1_".$db_ride[0]['vImage'];
        }
        else
        {     
          if($db_ride[0]['eGender'] == 'Male'){
              $stack[$i]['img'] = $tconfig["tsite_images"].'64x64male.png';
            }else{
              $stack[$i]['img'] = $tconfig["tsite_images"].'64x64female.png';
            }
        }  
        
        if($stack[$i]['return'] == 'No'){    
         $stack[$i]['vMainDeparture'] = $db_ride[0]['vMainDeparture'];
         $stack[$i]['vMainArrival'] = $db_ride[0]['vMainArrival'];
         if($db_ride[0]['eRideType'] == 'Reccuring'){
           $stack[$i]['time'] = $db_ride[0]['vMainOutBoundTime']; 
         }else{
           $stack[$i]['time'] = $db_ride[0]['vMainDepartureTime'];
         }
        }else{
         //$stack[$i]['address'] = $db_ride[0]['vMainArrival'].' - '.$db_ride[0]['vMainDeparture'];
         $stack[$i]['vMainDeparture'] = $db_ride[0]['vMainDeparture'];
         $stack[$i]['vMainArrival'] = $db_ride[0]['vMainArrival'];
          if($db_ride[0]['eRideType'] == 'Reccuring'){
           $stack[$i]['time'] = $db_ride[0]['vMainReturnTime']; 
         }else{
           $stack[$i]['time'] = $db_ride[0]['vMainArrivalTime'];
         }
        } 
        
        $sql = "SELECT count(iRateId) as tot_count_rating,SUM(iRate) as tot_sum_rating 
               FROM member_rating WHERE iMemberToId = ".$db_ride[0]['iMemberId']; 
        $db_member_ratings = $obj->MySQLSelect($sql);
        
        $review = $db_member_ratings[0]['tot_sum_rating'];
        $review = $review / $db_member_ratings[0]['tot_count_rating'];
        $rating_width = ($review * 100) / 5;
        
        $stack[$i]['rates'] = $rating_width;
        
        $sql = "SELECT member_car.iMemberCarId, member_car.iSeats, make.vMake, model.vTitle 
                FROM member_car 
                LEFT JOIN make ON make.iMakeId = member_car.iMakeId 
                LEFT JOIN model ON model.iModelId = member_car.iModelId
                WHERE  member_car.iMemberCarId = '".$db_ride[0]['iMemberCarId']."'";
        $db_car = $obj->MySQLSelect($sql);
        $stack[$i]['membercar'] = $db_car[0]['vMake']." ".$db_car[0]['vTitle'];
        
        $sql = "SELECT * FROM member_car_preferences WHERE iMemberId = '".$db_ride[0]['iMemberId']."'";
        $db_pref = $obj->MySQLSelect($sql); 
        
        $pref = '';
    
      for($op=0;$op<count($db_pref);$op++){
        $img = get_prefrence_img($db_pref[$op]['iPreferencesId'], $db_pref[$op]['vType']);       
        if($img != ''){
          $pref .= $img;
        }
      } 
        $stack[$i]['pref'] = $pref;
      }
      
      array_sort_by_column($stack, 'strtotime');
    }    
	$smarty->assign("ridesarroundyou",$stack); 
  } 
  #echo "<pre>";print_r($stack);exit;
  if($_COOKIE['country_code'] != ''){
    $ccsql = " AND rides_new.vCountryCode = '".$_COOKIE['country_code']."' ";
  }  

  # Get details of rides  of Airport#
  
  if($AIRPORTS_RIDES_SHOW == 'Yes'){   
    $sql = "SELECT rides_new.iRideId, rides_new.iMemberId, rides_new.vMainDeparture, rides_new.vMainArrival, rides_new.eRideType, rides_new.vMainOutBoundTime,rides_new.vMainDepartureTime, rides_new.vMainReturnTime, rides_new.vMainArrivalTime,  rides_new.iMemberCarId, ride_dates.dDateOut, ride_dates.dDateRet,member.vImage,member.eGender, rides_new.fRatio_".$_SESSION['sess_price_ratio']."   
            FROM rides_new 
            LEFT JOIN ride_dates ON ride_dates.iRideId = rides_new.iRideId
            LEFT JOIN member ON member.iMemberId = rides_new.iMemberId 
            WHERE rides_new.eRidePlaceType = 'Airport'
            AND rides_new.eStatus = 'Active' $ccsql   
            AND (ride_dates.dDateOut >= '".$todaydate."'  OR ride_dates.dDateRet >= '".$todaydate."')
            ORDER BY RAND() 
            LIMIT 0,3";
    $db_ride_details = $obj->MySQLSelect($sql);
   
    $mainarr = array();     
    for($i=0;$i<count($db_ride_details);$i++){
      $newarr = array();   
      if(is_file($tconfig["tsite_upload_images_member_path"].$db_ride_details[$i]['iMemberId']."/1_".$db_ride_details[$i]['vImage']))
      {
        $db_ride_details[$i]['img'] = $tconfig["tsite_upload_images_member"].$db_ride_details[$i]['iMemberId']."/1_".$db_ride_details[$i]['vImage'];
      }
      else
      {     
        if($db_ride_details[$i]['eGender']=='Male'){
            $db_ride_details[$i]['img'] = $tconfig["tsite_images"].'64x64male.png';
          }else{
            $db_ride_details[$i]['img'] = $tconfig["tsite_images"].'64x64female.png';
          }
      } 
      $sql = "SELECT count(iRateId) as tot_count_rating,SUM(iRate) as tot_sum_rating 
               FROM member_rating WHERE iMemberToId = ".$db_ride_details[$i]['iMemberId']; 
      $db_member_ratings = $obj->MySQLSelect($sql);
      
      $review = $db_member_ratings[0]['tot_sum_rating'];
      $review = $review / $db_member_ratings[0]['tot_count_rating'];         
      $review = ($review * 100) / 5;
      
      $sql = "SELECT member_car.iMemberCarId, member_car.iSeats, make.vMake, model.vTitle 
                FROM member_car 
                LEFT JOIN make ON make.iMakeId = member_car.iMakeId 
                LEFT JOIN model ON model.iModelId = member_car.iModelId
                WHERE  member_car.iMemberCarId = '".$db_ride_details[$i]['iMemberCarId']."'";
      $db_car = $obj->MySQLSelect($sql);
      
      $sql = "SELECT * FROM member_car_preferences WHERE iMemberId = '".$db_ride_details[$i]['iMemberId']."'";
      $db_pref = $obj->MySQLSelect($sql); 
      
      $pref = '';
      
      for($op=0;$op<count($db_pref);$op++){
        $img = get_prefrence_img($db_pref[$op]['iPreferencesId'], $db_pref[$op]['vType']);       
        if($img != ''){
          $pref .= $img;
        }
      } 
      
      $pref = $pref;
      
      if($db_ride_details[$i]['dDateOut'] != '0000-00-00' && strtotime($db_ride_details[$i]['dDateOut']) >= strtotime($todaydate)){
        $sql="select SUM(fPrice) as tot_price from ride_points_new where iRideId='".$db_ride_details[$i]['iRideId']."' and eReverse='No'";
        $db_total_price = $obj->MySQLSelect($sql); 
              
        $price = $db_total_price[0]['tot_price'] * $db_ride_details[$i]['fRatio_'.$_SESSION['sess_price_ratio']];       
        $newarr['totprice'] = $generalobj->booking_currency($price, $_SESSION['sess_price_ratio']);      
       
        $newarr['iRideId'] = $db_ride_details[$i]['iRideId'];
        $newarr['vMainDeparture'] = $db_ride_details[$i]['vMainDeparture'];
        $newarr['vMainArrival'] = $db_ride_details[$i]['vMainArrival'];
        $newarr['dDateOut'] = $db_ride_details[$i]['dDateOut'];
        $newarr['strtotime'] = strtotime($db_ride_details[$i]['dDateOut']);
        $newarr['memberimage'] = $db_ride_details[$i]['img'];
        $newarr['rating'] = $review; 
        $newarr['membercar'] = $db_car[0]['vMake']." ".$db_car[0]['vTitle'];
        if($db_ride_details[$i]['eRideType'] == 'Reccuring'){
          $newarr['deptime'] = $db_ride_details[$i]['vMainOutBoundTime'];
        }else{
          $newarr['deptime'] = $db_ride_details[$i]['vMainDepartureTime'];
        }
        $newarr['return'] = 'No'; 
        $newarr['pref'] = $pref;
          
        array_push($mainarr,$newarr);  
      }
      
      if($db_ride_details[$i]['dDateRet'] != '0000-00-00' && strtotime($db_ride_details[$i]['dDateRet']) >= strtotime($todaydate)){
        $sql="select SUM(fPrice) as tot_price from ride_points_new where iRideId='".$db_ride_details[$i]['iRideId']."' and eReverse='Yes'";
        $db_total_price_return = $obj->MySQLSelect($sql);
        $price = $db_total_price_return[0]['tot_price'] * $db_ride_details[$i]['fRatio_'.$_SESSION['sess_price_ratio']];       
        $newarr['totprice'] = $generalobj->booking_currency($price, $_SESSION['sess_price_ratio']); 
        
        $newarr['iRideId'] = $db_ride_details[$i]['iRideId'];
        $newarr['vMainDeparture'] = $db_ride_details[$i]['vMainArrival'];
        $newarr['vMainArrival'] = $db_ride_details[$i]['vMainDeparture'];
        $newarr['dDateOut'] = $db_ride_details[$i]['dDateRet'];
        $newarr['strtotime'] = strtotime($db_ride_details[$i]['dDateRet']);
        $newarr['memberimage'] = $db_ride_details[$i]['img'];
        $newarr['rating'] = round($review);
        $newarr['membercar'] = $db_car[0]['vMake']." ".$db_car[0]['vTitle'];
        if($db_ride_details[$i]['eRideType'] == 'Reccuring'){
          $newarr['deptime'] = $db_ride_details[$i]['vMainReturnTime'];
        }else{                                           
          $newarr['deptime'] = $db_ride_details[$i]['vMainArrivalTime'];
        }
        $newarr['return'] = 'Yes'; 
        $newarr['pref'] = $pref;
        array_push($mainarr,$newarr);                 
      }                                             
    } 
    $smarty->assign("airportmainarr",$mainarr); 
  }
 
  //array_sort_by_column($mainarr, 'strtotime');

  $featuredin = $ridesobj->get_featuredin_details();
  $smarty->assign("featuredin",$featuredin);
  
  #echo "<pre>";print_r($mainarr);exit;
  # Get details of rides  of Airport Ends#
  
  # Get details of rides of Ladies Only#
  
  if($LADIES_ONLY_SHOW == 'Yes'){   
    $sql = "SELECT rides_new.iRideId, rides_new.iMemberId, rides_new.vMainDeparture, rides_new.vMainArrival, rides_new.eRideType, rides_new.vMainOutBoundTime,rides_new.vMainDepartureTime, rides_new.vMainReturnTime, rides_new.vMainArrivalTime,  rides_new.iMemberCarId, ride_dates.dDateOut, ride_dates.dDateRet,member.vImage,member.eGender, rides_new.fRatio_".$_SESSION['sess_price_ratio']."     
            FROM rides_new 
            LEFT JOIN ride_dates ON ride_dates.iRideId = rides_new.iRideId
            LEFT JOIN member ON member.iMemberId = rides_new.iMemberId 
            WHERE rides_new.eLadiesOnly = 'Yes'
            AND rides_new.eStatus = 'Active' $ccsql  
            AND (ride_dates.dDateOut >= '".$todaydate."'  OR ride_dates.dDateRet >= '".$todaydate."')
            ORDER BY RAND() 
            LIMIT 0,3";
    $db_ride_details_ladies = $obj->MySQLSelect($sql);
    #echo "<pre>";print_r($db_ride_details); exit;
    
    $mainarr_ladies = array();     
    for($i=0;$i<count($db_ride_details_ladies);$i++){
      $newarr_ladies = array();
      #member_photo
        if(is_file($tconfig["tsite_upload_images_member_path"].$db_ride_details_ladies[$i]['iMemberId']."/1_".$db_ride_details_ladies[$i]['vImage']))
        {
          $db_ride_details_ladies[$i]['img'] = $tconfig["tsite_upload_images_member"].$db_ride_details_ladies[$i]['iMemberId']."/1_".$db_ride_details_ladies[$i]['vImage'];
        }
        else
        {
          if($db_ride_details_ladies[$i]['eGender']=='Male'){
            $db_ride_details_ladies[$i]['img'] = $tconfig["tsite_images"].'64x64male.png';
          }else{
            $db_ride_details_ladies[$i]['img'] = $tconfig["tsite_images"].'64x64female.png';
          }
      //    $db_ride_details_ladies[$i]['img'] = $tconfig["tsite_images"].'user-man-54.png';
            
        }
        
        # member_rating
        $sql = "SELECT count(iRateId) as tot_count_rating,SUM(iRate) as tot_sum_rating 
                 FROM member_rating WHERE iMemberToId = ".$db_ride_details_ladies[$i]['iMemberId']; 
        $db_member_ratings_lad_rides = $obj->MySQLSelect($sql);
        
        $review_lad_rides = $db_member_ratings_lad_rides[0]['tot_sum_rating'];
        $review_lad_rides = $review_lad_rides / $db_member_ratings_lad_rides[0]['tot_count_rating'];       
        $review_lad_rides = ($review_lad_rides * 100) / 5;
        
        #member_car
        $sql = "SELECT member_car.iMemberCarId, member_car.iSeats, make.vMake, model.vTitle 
                  FROM member_car 
                  LEFT JOIN make ON make.iMakeId = member_car.iMakeId 
                  LEFT JOIN model ON model.iModelId = member_car.iModelId
                  WHERE  member_car.iMemberCarId = '".$db_ride_details_ladies[$i]['iMemberCarId']."'";
        $db_car_lad_rides = $obj->MySQLSelect($sql); 
        
      $sql = "SELECT * FROM member_car_preferences WHERE iMemberId = '".$db_ride_details_ladies[$i]['iMemberId']."'";
      $db_pref = $obj->MySQLSelect($sql); 
      
      $pref = '';
      
      for($op=0;$op<count($db_pref);$op++){
        $img = get_prefrence_img($db_pref[$op]['iPreferencesId'], $db_pref[$op]['vType']);       
        if($img != ''){
          $pref .= $img;
        }
      } 
      
      $pref = $pref;

        
      if($db_ride_details_ladies[$i]['dDateOut'] != '0000-00-00' && strtotime($db_ride_details_ladies[$i]['dDateOut']) >= strtotime($todaydate)){
        # ride price 
        $sql="select SUM(fPrice) as tot_price from ride_points_new where iRideId='".$db_ride_details_ladies[$i]['iRideId']."' and eReverse='No'";
        $db_total_price_ladies_rides = $obj->MySQLSelect($sql);
        
        $price = $db_total_price_ladies_rides[0]['tot_price'] * $db_ride_details_ladies[$i]['fRatio_'.$_SESSION['sess_price_ratio']];       
        $newarr_ladies['totprice'] = $generalobj->booking_currency($price, $_SESSION['sess_price_ratio']); 
        
        $newarr_ladies['iRideId'] = $db_ride_details_ladies[$i]['iRideId'];
        $newarr_ladies['vMainDeparture'] = $db_ride_details_ladies[$i]['vMainDeparture'];
        $newarr_ladies['vMainArrival'] = $db_ride_details_ladies[$i]['vMainArrival'];
        $newarr_ladies['dDateOut'] = $db_ride_details_ladies[$i]['dDateOut'];
        $newarr_ladies['strtotime'] = strtotime($db_ride_details_ladies[$i]['dDateOut']);
        $newarr_ladies['memberimage'] = $db_ride_details_ladies[$i]['img'];
        $newarr_ladies['rating'] = $review_lad_rides; 
        $newarr_ladies['membercar'] = $db_car_lad_rides[0]['vMake']." ".$db_car_lad_rides[0]['vTitle'];
        if($db_ride_details_ladies[$i]['eRideType'] == 'Reccuring'){
          $newarr_ladies['deptime'] = $db_ride_details_ladies[$i]['vMainOutBoundTime'];
        }else{
          $newarr_ladies['deptime'] = $db_ride_details_ladies[$i]['vMainDepartureTime'];
        }
        $newarr_ladies['return'] = 'No';
        $newarr_ladies['pref'] = $pref;           
        array_push($mainarr_ladies,$newarr_ladies);  
      }
      
      if($db_ride_details_ladies[$i]['dDateRet'] != '0000-00-00' && strtotime($db_ride_details_ladies[$i]['dDateRet']) >= strtotime($todaydate)){
        # ride price
        $sql="select SUM(fPrice) as tot_price from ride_points_new where iRideId='".$db_ride_details_ladies[$i]['iRideId']."' and eReverse='Yes'";
        $db_total_price_ladies_rides_ret = $obj->MySQLSelect($sql);
        
        $price = $db_total_price_ladies_rides_ret[0]['tot_price'] * $db_ride_details_ladies[$i]['fRatio_'.$_SESSION['sess_price_ratio']];       
        $newarr_ladies['totprice'] = $generalobj->booking_currency($price, $_SESSION['sess_price_ratio']); 
        
        $newarr_ladies['iRideId'] = $db_ride_details_ladies[$i]['iRideId'];
        $newarr_ladies['vMainDeparture'] = $db_ride_details_ladies[$i]['vMainArrival'];
        $newarr_ladies['vMainArrival'] = $db_ride_details_ladies[$i]['vMainDeparture'];
        $newarr_ladies['dDateOut'] = $db_ride_details_ladies[$i]['dDateRet'];
        $newarr_ladies['strtotime'] = strtotime($db_ride_details_ladies[$i]['dDateRet']);
        $newarr_ladies['memberimage'] = $db_ride_details_ladies[$i]['img'];
        $newarr_ladies['rating'] = round($review_lad_rides);
        $newarr_ladies['membercar'] = $db_car_lad_rides[0]['vMake']." ".$db_car_lad_rides[0]['vTitle'];
        if($db_ride_details_ladies[$i]['eRideType'] == 'Reccuring'){
          $newarr_ladies['deptime'] = $db_ride_details_ladies[$i]['vMainReturnTime'];
        }else{                                                     
          $newarr_ladies['deptime'] = $db_ride_details_ladies[$i]['vMainArrivalTime'];
        }
        $newarr_ladies['return'] = 'Yes';
        $newarr_ladies['pref'] = $pref;  
        array_push($mainarr_ladies,$newarr_ladies);               
      }
    }
   
    //array_sort_by_column($mainarr_ladies, 'strtotime');
    
    
    #echo "<pre>";print_r($mainarr_ladies); exit;
    $smarty->assign("ladiesmainarr",$mainarr_ladies);
  }
  # Get details of rides of Ladies Only Ends# 
       
  # Get details of shopping rides #
  if($SHOPPING_RIDES_SHOW == 'Yes'){   
    $sql = "SELECT rides_new.iRideId, rides_new.iMemberId, rides_new.vMainDeparture, rides_new.vMainArrival, rides_new.eRideType, rides_new.vMainOutBoundTime,rides_new.vMainDepartureTime, rides_new.vMainReturnTime, rides_new.vMainArrivalTime,  rides_new.iMemberCarId, ride_dates.dDateOut, ride_dates.dDateRet,member.vImage,member.eGender, rides_new.fRatio_".$_SESSION['sess_price_ratio']."     
            FROM rides_new 
            LEFT JOIN ride_dates ON ride_dates.iRideId = rides_new.iRideId
            LEFT JOIN member ON member.iMemberId = rides_new.iMemberId 
            WHERE rides_new.eRidePlaceType = 'Shopping'
            AND rides_new.eStatus = 'Active' $ccsql   
            AND (ride_dates.dDateOut >= '".$todaydate."'  OR ride_dates.dDateRet >= '".$todaydate."')
            ORDER BY RAND() 
            LIMIT 0,3";
    $db_ride_details_shopping = $obj->MySQLSelect($sql);
    #echo "<pre>";print_r($db_ride_details_shopping); exit;
    
    $mainarr_shopping = array();     
    for($i=0;$i<count($db_ride_details_shopping);$i++){
      $newarr_shopping = array();
      #member_photo
        if(is_file($tconfig["tsite_upload_images_member_path"].$db_ride_details_shopping[$i]['iMemberId']."/1_".$db_ride_details_shopping[$i]['vImage']))
        {
          $db_ride_details_shopping[$i]['img'] = $tconfig["tsite_upload_images_member"].$db_ride_details_shopping[$i]['iMemberId']."/1_".$db_ride_details_shopping[$i]['vImage'];
        }
        else
        {
         // $db_ride_details_shopping[$i]['img'] = $tconfig["tsite_images"].'user-man-54.png';
         if($db_ride_details_shopping[$i]['eGender']=='Male'){
            $db_ride_details_shopping[$i]['img'] = $tconfig["tsite_images"].'64x64male.png';
          }else{
            $db_ride_details_shopping[$i]['img'] = $tconfig["tsite_images"].'64x64female.png';
          }
        }
        
        # member_rating
        $sql = "SELECT count(iRateId) as tot_count_rating,SUM(iRate) as tot_sum_rating 
                 FROM member_rating WHERE iMemberToId = ".$db_ride_details_shopping[$i]['iMemberId']; 
        $db_member_ratings_shopping = $obj->MySQLSelect($sql);
        
        $review_shopping = $db_member_ratings_shopping[0]['tot_sum_rating'];
        $review_shopping = $review_shopping / $db_member_ratings_shopping[0]['tot_count_rating'];
        
        $review_shopping = ($review_shopping * 100) / 5;
        
        #member_car
        $sql = "SELECT member_car.iMemberCarId, member_car.iSeats, make.vMake, model.vTitle 
                  FROM member_car 
                  LEFT JOIN make ON make.iMakeId = member_car.iMakeId 
                  LEFT JOIN model ON model.iModelId = member_car.iModelId
                  WHERE  member_car.iMemberCarId = '".$db_ride_details_shopping[$i]['iMemberCarId']."'";
        $db_car_shopping = $obj->MySQLSelect($sql);
        
      $sql = "SELECT * FROM member_car_preferences WHERE iMemberId = '".$db_ride_details_shopping[$i]['iMemberId']."'";
      $db_pref = $obj->MySQLSelect($sql); 
      
      $pref = '';
       for($op=0;$op<count($db_pref);$op++){
        $img = get_prefrence_img($db_pref[$op]['iPreferencesId'], $db_pref[$op]['vType']);       
        if($img != ''){
          $pref .= $img;
        }
      } 
      
      $pref = $pref; 
        
      if($db_ride_details_shopping[$i]['dDateOut'] != '0000-00-00' && strtotime($db_ride_details_shopping[$i]['dDateOut']) >= strtotime($todaydate)){
        # ride price 
        $sql="select SUM(fPrice) as tot_price from ride_points_new where iRideId='".$db_ride_details_shopping[$i]['iRideId']."' and eReverse='No'";
        $db_total_price_shopping = $obj->MySQLSelect($sql);
        
        $price = $db_total_price_shopping[0]['tot_price'] * $db_ride_details_shopping[$i]['fRatio_'.$_SESSION['sess_price_ratio']];       
        $newarr_shopping['totprice'] = $generalobj->booking_currency($price, $_SESSION['sess_price_ratio']);
        
        $newarr_shopping['iRideId'] = $db_ride_details_shopping[$i]['iRideId'];
        $newarr_shopping['vMainDeparture'] = $db_ride_details_shopping[$i]['vMainDeparture'];
        $newarr_shopping['vMainArrival'] = $db_ride_details_shopping[$i]['vMainArrival'];
        $newarr_shopping['dDateOut'] = $db_ride_details_shopping[$i]['dDateOut'];
        $newarr_shopping['strtotime'] = strtotime($db_ride_details_shopping[$i]['dDateOut']);
        $newarr_shopping['memberimage'] = $db_ride_details_shopping[$i]['img'];
        $newarr_shopping['rating'] = $review_shopping; 
        $newarr_shopping['membercar'] = $db_car_shopping[0]['vMake']." ".$db_car_shopping[0]['vTitle'];
        if($db_ride_details_shopping[$i]['eRideType'] == 'Reccuring'){
          $newarr_shopping['deptime'] = $db_ride_details_shopping[$i]['vMainOutBoundTime'];
        }else{
          $newarr_shopping['deptime'] = $db_ride_details_shopping[$i]['vMainDepartureTime'];
        }
        $newarr_shopping['return'] = 'No';
        $newarr_shopping['pref'] = $pref;
        array_push($mainarr_shopping,$newarr_shopping);  
      }
      
      if($db_ride_details_shopping[$i]['dDateRet'] != '0000-00-00' && strtotime($db_ride_details_shopping[$i]['dDateRet']) >= strtotime($todaydate)){
        # ride price
        $sql="select SUM(fPrice) as tot_price from ride_points_new where iRideId='".$db_ride_details_shopping[$i]['iRideId']."' and eReverse='Yes'";
        $db_total_price_shopping_ret = $obj->MySQLSelect($sql);
        
        $price = $db_total_price_shopping_ret[0]['tot_price'] * $db_ride_details_shopping[$i]['fRatio_'.$_SESSION['sess_price_ratio']];       
        $newarr_shopping['totprice'] = $generalobj->booking_currency($price, $_SESSION['sess_price_ratio']);
        
        $newarr_shopping['iRideId'] = $db_ride_details_shopping[$i]['iRideId'];
        $newarr_shopping['vMainDeparture'] = $db_ride_details_shopping[$i]['vMainArrival'];
        $newarr_shopping['vMainArrival'] = $db_ride_details_shopping[$i]['vMainDeparture'];
        $newarr_shopping['dDateOut'] = $db_ride_details_shopping[$i]['dDateRet'];
        $newarr_shopping['strtotime'] = strtotime($db_ride_details_shopping[$i]['dDateRet']);
        $newarr_shopping['memberimage'] = $db_ride_details_shopping[$i]['img'];
        $newarr_shopping['rating'] = round($review_shopping);
        $newarr_shopping['membercar'] = $db_car_shopping[0]['vMake']." ".$db_car_shopping[0]['vTitle'];
        if($db_ride_details_shopping[$i]['eRideType'] == 'Reccuring'){
          $newarr_shopping['deptime'] = $db_ride_details_shopping[$i]['vMainReturnTime'];
        }else{
          $newarr_shopping['deptime'] = $db_ride_details_shopping[$i]['vMainArrivalTime'];
        }
        $newarr_shopping['return'] = 'Yes';
        $newarr_shopping['pref'] = $pref;
        array_push($mainarr_shopping,$newarr_shopping);                 
      }
    }
    
    //array_sort_by_column($mainarr_shopping, 'strtotime');
    
    
    #echo "<pre>";print_r($mainarr_shopping); exit;
    $smarty->assign("shoppingmainarr",$mainarr_shopping);
  }
  # Get details of shopping rides ends#
?>