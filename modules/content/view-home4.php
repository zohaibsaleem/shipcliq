<?php    
	function get_prefrence_img($id, $type){
		global $obj, $tconfig;
		
		$sql = "SELECT iPreferencesId, vTitle".$_SESSION['sess_lang_pref']." as vTitle, ".$type."_Lable".$_SESSION['sess_lang_pref']." as Lable, ".$type." FROM preferences WHERE iPreferencesId = '".$id."' ORDER BY iDispOrder ASC";
		$db_preferences = $obj->MySQLSelect($sql);
		
		if(file_exists($tconfig["tsite_upload_images_preferences_path"].'/'.$db_preferences[0][$type])){
			$img_url = $tconfig["tsite_upload_images_preferences"].$db_preferences[0][$type];
			$img_url = '<img alt="" src="'.$tconfig["tsite_upload_images_preferences"].$db_preferences[0][$type].'" title="'.$db_preferences[0]['Lable'].'">&nbsp;';
			
			}else{
			$img_url = ''; 
		}
		
		return $img_url;
	}
	
	
	
	$sql = "SELECT * FROM banners WHERE eStatus = 'Active' AND ePortion = 'Right' ORDER BY RAND() LIMIT 0,1";
	$db_right_banner = $obj->MySQLSelect($sql);
	
	if($db_right_banner[0]['eType'] == 'Image'){
		if(file_exists($tconfig["tsite_upload_images_banner_path"].$db_right_banner[0]['vImage'])){
			$db_right_banner[0]['source'] = '<img src="'.$tconfig["tsite_upload_images_banner"].$db_right_banner[0]['vImage'].'" alt="" width="112px" />';
			}else{
			$db_right_banner[0]['source'] = '';
		}
		}else{
		$db_right_banner[0]['source'] = $db_right_banner[0]['tCode'];  
	}
	
	
	
	
	$sql = "SELECT * FROM banners WHERE eStatus = 'Active' AND ePortion = 'Left' ORDER BY RAND() LIMIT 0,1";
	$db_left_banner = $obj->MySQLSelect($sql);
	
	if($db_left_banner[0]['eType'] == 'Image'){
		if(file_exists($tconfig["tsite_upload_images_banner_path"].$db_left_banner[0]['vImage'])){
			$db_left_banner[0]['source'] = '<img src="'.$tconfig["tsite_upload_images_banner"].$db_left_banner[0]['vImage'].'" alt="" width="112px" />';
			}else{
			$db_left_banner[0]['source'] = '';
		}
		}else{
		$db_left_banner[0]['source'] = $db_left_banner[0]['tCode'];  
	}
	#echo "<pre>";print_r($db_left_banner);exit;
	
	########################## Slider start #################################
	$videosql = "SELECT etype,tCode FROM home_banners WHERE eStatus = 'Active' AND eType = 'Video'";
	$videodb_middle_banner = $obj->MySQLSelect($videosql); 
	$videocount = count($videodb_middle_banner);
	
	if($videocount == 1)
	{
		$videocode = $videodb_middle_banner[0]['tCode'];
	}
	else if($videocount == 0)
	{
		$sql = "SELECT * FROM home_banners WHERE eStatus = 'Active' AND eType = 'Image' ORDER BY RAND() LIMIT 0,4";
		$db_middle_banner = $obj->MySQLSelect($sql);
		
		for($i=0;$i<count($db_middle_banner);$i++)
		{
			if(file_exists($tconfig["tsite_upload_images_banner_path"].$db_middle_banner[$i]['vImage']))
			{
				$db_middle_banner[$i]['source'] = '<img src="'.$tconfig["tsite_upload_images_banner"].$db_middle_banner[$i]['vImage'].'" alt="" />';
			}
			else
			{
				$db_middle_banner[$i]['source'] = '';
			}
			if ($i == 0) 
			{
				$bannerArr .= '[';
			}
			if ($i == count($db_middle_banner) - 1) 
			{
				$bannerArr .= '"'.$tconfig['tsite_upload_images_banner'].$db_middle_banner[$i]['vImage']. '"'; 
			}
			else 
			{
				$bannerArr .= '"'.$tconfig['tsite_upload_images_banner'].$db_middle_banner[$i]['vImage']. '",';
			}
			if ($i == count($db_middle_banner) - 1) 
			{
				$bannerArr .= ']';
			}  
		}
	}
	########################## Slider end #################################
	#echo $bannerArr;exit;
	#echo "<pre>";print_r($db_middle_banner);exit;
	$smarty->assign("bannerArr",$bannerArr);
	$smarty->assign("db_middle_banner",$db_middle_banner);
	$smarty->assign("videocount",$videocount);
	$smarty->assign("videocode",$videocode);
	$smarty->assign("db_left_banner",$db_left_banner);
	$smarty->assign("db_right_banner",$db_right_banner);
	
	$todaydate = date('Y-m-d');
	
	function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
		$sort_col = array();
		foreach ($arr as $key=> $row) {
			$sort_col[$key] = $row[$col];
		}    
		array_multisort($sort_col, $dir, $arr); 
	}     
	
	$userlatitude =  $_COOKIE['userlatitude'];
	$userlongitude = $_COOKIE['userlongitude'];
	
	if($userlatitude != '' && $userlongitude != ''){
		$sql = "SELECT ride_points_new.iRidePointId, ride_points_new.iRideId, ride_points_new.eReverse, ride_points_new.vStartPoint, ride_points_new.vEndPoint, ride_points_new.fPrice, ride_points_new.fTotPrice, ride_dates.dDateOut, ride_dates.dDateRet,  
		( 3959 * acos( cos( radians(".$userlatitude.") ) * cos( radians( ride_points_new.vStartLatitude ) ) * cos( radians( ride_points_new.vStartLongitude ) - radians(".$userlongitude.") ) + sin( radians(".$userlatitude.") ) * sin( radians( ride_points_new.vStartLatitude ) ) ) ) AS distance 
		FROM ride_points_new 
		LEFT JOIN rides_new ON rides_new.iRideId = ride_points_new.iRideId  
		LEFT JOIN ride_dates ON ride_dates.iRideId = ride_points_new.iRideId 
		WHERE rides_new.eStatus = 'Active'  
		HAVING distance < 25 
		GROUP BY rides_new.iMemberId 
		ORDER BY RAND()   
		LIMIT 0,10";
		$db_ride_points = $obj->MySQLSelect($sql);
		
		if(count($db_ride_points) > 0){
			$stack = array();
			for($i=0;$i<count($db_ride_points);$i++){      
				$newarr = array(); 
				if($db_ride_points[$i]['eReverse'] == 'No'){
					if($db_ride_points[$i]['dDateOut'] != '0000-00-00' && strtotime($db_ride_points[$i]['dDateOut']) >= strtotime($todaydate)){
						$newarr['dDate'] = $db_ride_points[$i]['dDateOut'];
						$newarr['strtotime'] = strtotime($db_ride_points[$i]['dDateOut']);
						$newarr['iRideId'] = $db_ride_points[$i]['iRideId'];         
						$newarr['return'] = $db_ride_points[$i]['eReverse'];  
						array_push($stack,$newarr);
					}             
					}else{
					if($db_ride_points[$i]['dDateRet'] != '0000-00-00' && strtotime($db_ride_points[$i]['dDateOut']) >= strtotime($todaydate)){
						$newarr['dDate'] = $db_ride_points[$i]['dDateRet'];
						$newarr['strtotime'] = strtotime($db_ride_points[$i]['dDateRet']);
						$newarr['iRideId'] = $db_ride_points[$i]['iRideId'];         
						$newarr['return'] = $db_ride_points[$i]['eReverse'];  
						array_push($stack,$newarr);
					}            
				} 
				
			} 
			
			for($i=0;$i<count($stack);$i++){
				$sql = "SELECT rides_new.iMemberId, rides_new.iMemberCarId, rides_new.eRideType, rides_new.vMainDeparture, rides_new.vMainArrival, rides_new.vMainOutBoundTime, rides_new.vMainReturnTime, rides_new.vMainDepartureTime, rides_new.vMainArrivalTime, rides_new.fRatio_".$_SESSION['sess_price_ratio'].", member.eGender, member.vImage   
                FROM rides_new 
                LEFT JOIN member ON member.iMemberId = rides_new.iMemberId   
                WHERE rides_new.iRideId = '".$stack[$i]['iRideId']."' 
                AND rides_new.eStatus = 'Active'";
				$db_ride = $obj->MySQLSelect($sql); 
				
				$sql = "SELECT SUM(fPrice) as tot_price FROM ride_points_new WHERE iRideId = '".$stack[$i]['iRideId']."' AND eReverse = '".$stack[$i]['return']."'";
				$db_price = $obj->MySQLSelect($sql);
				
				$price = $db_price[0]['tot_price'] * $db_ride[0]['fRatio_'.$_SESSION['sess_price_ratio']];       
				$stack[$i]['price'] = $generalobj->booking_currency($price, $_SESSION['sess_price_ratio']);
				
				if(is_file($tconfig["tsite_upload_images_member_path"].$db_ride[0]['iMemberId']."/2_".$db_ride[0]['vImage']))
				{
					$stack[$i]['img'] = $tconfig["tsite_upload_images_member"].$db_ride[0]['iMemberId']."/2_".$db_ride[0]['vImage'];
				}
				else
				{     
					if($db_ride[0]['eGender'] == 'Male'){
						$stack[$i]['img'] = $tconfig["tsite_images"].'150x150male.png';
						}else{
						$stack[$i]['img'] = $tconfig["tsite_images"].'150x150female.png';
					}
				}  
				
				if($stack[$i]['return'] == 'No'){    
					$stack[$i]['vMainDeparture'] = $db_ride[0]['vMainDeparture'];
					$stack[$i]['vMainArrival'] = $db_ride[0]['vMainArrival'];
					if($db_ride[0]['eRideType'] == 'Reccuring'){
						$stack[$i]['time'] = $db_ride[0]['vMainOutBoundTime']; 
						}else{
						$stack[$i]['time'] = $db_ride[0]['vMainDepartureTime'];
					}
					}else{
					//$stack[$i]['address'] = $db_ride[0]['vMainArrival'].' - '.$db_ride[0]['vMainDeparture'];
					$stack[$i]['vMainDeparture'] = $db_ride[0]['vMainArrival'];
					$stack[$i]['vMainArrival'] = $db_ride[0]['vMainDeparture'];
					if($db_ride[0]['eRideType'] == 'Reccuring'){
						$stack[$i]['time'] = $db_ride[0]['vMainReturnTime']; 
						}else{
						$stack[$i]['time'] = $db_ride[0]['vMainArrivalTime'];
					}
				} 
				
				$sql = "SELECT count(iRateId) as tot_count_rating,SUM(iRate) as tot_sum_rating 
				FROM member_rating WHERE iMemberToId = ".$db_ride[0]['iMemberId']; 
				$db_member_ratings = $obj->MySQLSelect($sql);
				
				$review = $db_member_ratings[0]['tot_sum_rating'];
				$review = $review / $db_member_ratings[0]['tot_count_rating'];
				$rating_width = ($review * 100) / 5;
				
				$stack[$i]['rates'] = $rating_width;
				
				$sql = "SELECT member_car.iMemberCarId, member_car.iSeats, make.vMake, model.vTitle 
                FROM member_car 
                LEFT JOIN make ON make.iMakeId = member_car.iMakeId 
                LEFT JOIN model ON model.iModelId = member_car.iModelId
                WHERE  member_car.iMemberCarId = '".$db_ride[0]['iMemberCarId']."'";
				$db_car = $obj->MySQLSelect($sql);
				$stack[$i]['membercar'] = $db_car[0]['vMake']." ".$db_car[0]['vTitle'];
				
				$sql = "SELECT * FROM member_car_preferences WHERE iMemberId = '".$db_ride[0]['iMemberId']."'";
				$db_pref = $obj->MySQLSelect($sql); 
				
				if($db_pref[0]['eChattiness'] == 'YES'){
					$eChattiness = $tconfig['tsite_images'].'chat1.png';
					$titlechat=LBL_I_LOVE_CHAT;
				}
				if($db_pref[0]['eChattiness'] == 'MAYBE'){
					$eChattiness = $tconfig['tsite_images'].'chat2.png';
					$titlechat=LBL_TALK_MOOD;
				}
				if($db_pref[0]['eChattiness'] == 'NO'){
					$eChattiness = $tconfig['tsite_images'].'chat3.png';
					$titlechat=LBL_QUIET_TYPE;
				}
				
				if($db_pref[0]['eMusic'] == 'YES'){
					$eMusic = $tconfig['tsite_images'].'music1.png';
					$titlemusic=LBL_ABOUT_PLAYLIST;
				}
				if($db_pref[0]['eMusic'] == 'MAYBE'){
					$eMusic = $tconfig['tsite_images'].'music2.png';
					$titlemusic="Music- ".LBL_DEPEND_MOOD;
				}
				if($db_pref[0]['eMusic'] == 'NO'){
					$eMusic = $tconfig['tsite_images'].'music3.png';
					$titlemusic=LBL_SILENCE_GOLDEN;
				}
				
				
				if($db_pref[0]['eSmoking'] == 'YES'){
					$eSmoking = $tconfig['tsite_images'].'smoke1.png';
					$titlesmok=LBL_CIGARETTE_SMOKE;
				}
				if($db_pref[0]['eSmoking'] == 'MAYBE'){
					$eSmoking = $tconfig['tsite_images'].'smoke2.png';
					$titlesmok="Cigerrate- ".LBL_DEPEND_MOOD;
				}
				if($db_pref[0]['eSmoking'] == 'NO'){
					$eSmoking = $tconfig['tsite_images'].'smoke3.png';
					$titlesmok=LBL_NO_SMOKE;
				}
				
				if($db_pref[0]['eEcig'] == 'YES'){
					$eEcig = $tconfig['tsite_images'].'ecig1.png';
					$titlesmok1="e".LBL_CIGARETTE_SMOKE;
				}
				if($db_pref[0]['eEcig'] == 'MAYBE'){
					$eEcig = $tconfig['tsite_images'].'ecig2.png';
					$titlesmok1="eCigerrate- ".LBL_DEPEND_MOOD;
				}
				if($db_pref[0]['eEcig'] == 'NO'){
					$eEcig = $tconfig['tsite_images'].'ecig3.png';
					$titlesmok1="e".LBL_NO_SMOKE;
				}
				
				if($db_pref[0]['ePets'] == 'YES'){
					$ePets = $tconfig['tsite_images'].'pets1.png';
					$titlepet = LBL_PETS1;
				}
				if($db_pref[0]['ePets'] == 'MAYBE'){
					$ePets = $tconfig['tsite_images'].'pets2.png';
					$titlepet = "Pets -".LBL_DEPEND_MOOD;
				}
				if($db_pref[0]['ePets'] == 'NO'){
					$ePets = $tconfig['tsite_images'].'pets3.png';
					$titlepet = LBL_PETS2;
				}       
				$stack[$i]['pref'] = '<img alt="" src="'.$eChattiness.'" style="width:16%;" title="'.$titlechat.'">&nbsp;<img alt="" src="'.$eMusic.'" style="width:16%;" title="'.$titlemusic.'">&nbsp;<img alt="" src="'.$eSmoking.'" style="width:16%;" title="'.$titlesmok.'">&nbsp;<img alt="" src="'.$eEcig.'" style="width:16%;" title="'.$titlesmok1.'">&nbsp;<img alt="" src="'.$ePets.'" style="width:16%;" title="'.$titlepet.'">';
			}
			
			array_sort_by_column($stack, 'strtotime');
		}    
		$smarty->assign("ridesarroundyou",$stack); 
	} 
	
	if($_COOKIE['country_code'] != ''){
		$ccsql = " AND rides_new.vCountryCode = '".$_COOKIE['country_code']."' ";
	}  
	
	# Get details of rides  of Airport#
	
	$sql = "SELECT rides_new.iRideId, rides_new.iMemberId, rides_new.vMainDeparture, rides_new.vMainArrival, rides_new.eRideType, rides_new.vMainOutBoundTime,rides_new.vMainDepartureTime, rides_new.vMainReturnTime, rides_new.vMainArrivalTime,  rides_new.iMemberCarId, ride_dates.dDateOut, ride_dates.dDateRet,member.vImage,member.eGender, rides_new.fRatio_".$_SESSION['sess_price_ratio']."   
	FROM rides_new 
	LEFT JOIN ride_dates ON ride_dates.iRideId = rides_new.iRideId
	LEFT JOIN member ON member.iMemberId = rides_new.iMemberId 
	WHERE rides_new.eRidePlaceType = 'Airport'
	AND rides_new.eStatus = 'Active' $ccsql   
	AND (ride_dates.dDateOut >= '".$todaydate."'  OR ride_dates.dDateRet >= '".$todaydate."')
	ORDER BY RAND() 
	LIMIT 0,15";
	$db_ride_details = $obj->MySQLSelect($sql);
	
	$mainarr = array();     
	for($i=0;$i<count($db_ride_details);$i++){
		$newarr = array();   
		if(is_file($tconfig["tsite_upload_images_member_path"].$db_ride_details[$i]['iMemberId']."/2_".$db_ride_details[$i]['vImage']))
		{
			$db_ride_details[$i]['img'] = $tconfig["tsite_upload_images_member"].$db_ride_details[$i]['iMemberId']."/2_".$db_ride_details[$i]['vImage'];
		}
		else
		{     
			if($db_ride_details[$i]['eGender']=='Male'){
				$db_ride_details[$i]['img'] = $tconfig["tsite_images"].'150x150male.png';
				}else{
				$db_ride_details[$i]['img'] = $tconfig["tsite_images"].'150x150female.png';
			}
		} 
		$sql = "SELECT count(iRateId) as tot_count_rating,SUM(iRate) as tot_sum_rating 
		FROM member_rating WHERE iMemberToId = ".$db_ride_details[$i]['iMemberId']; 
		$db_member_ratings = $obj->MySQLSelect($sql);
		
		$review = $db_member_ratings[0]['tot_sum_rating'];
		$review = $review / $db_member_ratings[0]['tot_count_rating'];         
		$review = ($review * 100) / 5;
		
		$sql = "SELECT member_car.iMemberCarId, member_car.iSeats, make.vMake, model.vTitle 
		FROM member_car 
		LEFT JOIN make ON make.iMakeId = member_car.iMakeId 
		LEFT JOIN model ON model.iModelId = member_car.iModelId
		WHERE  member_car.iMemberCarId = '".$db_ride_details[$i]['iMemberCarId']."'";
		$db_car = $obj->MySQLSelect($sql);
		
		$sql = "SELECT * FROM member_car_preferences WHERE iMemberId = '".$db_ride_details[$i]['iMemberId']."'";
		$db_pref = $obj->MySQLSelect($sql); 
		
		$pref = '';
		for($op=0;$op<count($db_pref);$op++){
			$img = get_prefrence_img($db_pref[$op]['iPreferencesId'], $db_pref[$op]['vType']);       
			if($img != ''){
				$pref .= $img;
			}
		}
		
		if($db_ride_details[$i]['dDateOut'] != '0000-00-00' && strtotime($db_ride_details[$i]['dDateOut']) >= strtotime($todaydate)){        
			$sql="select SUM(fPrice) as tot_price from ride_points_new where iRideId='".$db_ride_details[$i]['iRideId']."' and eReverse='No'";
			$db_total_price = $obj->MySQLSelect($sql); 
            
			$price = $db_total_price[0]['tot_price'] * $db_ride_details[$i]['fRatio_'.$_SESSION['sess_price_ratio']];       
			$newarr['totprice'] = $generalobj->booking_currency($price, $_SESSION['sess_price_ratio']);      
			
			$newarr['iRideId'] = $db_ride_details[$i]['iRideId'];
			$newarr['vMainDeparture'] = $db_ride_details[$i]['vMainDeparture'];
			$newarr['vMainArrival'] = $db_ride_details[$i]['vMainArrival'];
			$newarr['dDateOut'] = $db_ride_details[$i]['dDateOut'];
			$newarr['strtotime'] = strtotime($db_ride_details[$i]['dDateOut']);
			$newarr['memberimage'] = $db_ride_details[$i]['img'];
			$newarr['rating'] = $review; 
			$newarr['membercar'] = $db_car[0]['vMake']." ".$db_car[0]['vTitle'];
			if($db_ride_details[$i]['eRideType'] == 'Reccuring'){
				$newarr['deptime'] = $db_ride_details[$i]['vMainOutBoundTime'];
				}else{
				$newarr['deptime'] = $db_ride_details[$i]['vMainDepartureTime'];
			}
			$newarr['return'] = 'No'; 
			$newarr['pref'] = $pref;
			
			array_push($mainarr,$newarr);  
		}
		
		if($db_ride_details[$i]['dDateRet'] != '0000-00-00' && strtotime($db_ride_details[$i]['dDateRet']) >= strtotime($todaydate)){      
			$sql="select SUM(fPrice) as tot_price from ride_points_new where iRideId='".$db_ride_details[$i]['iRideId']."' and eReverse='Yes'";
			$db_total_price_return = $obj->MySQLSelect($sql);
			$price = $db_total_price_return[0]['tot_price'] * $db_ride_details[$i]['fRatio_'.$_SESSION['sess_price_ratio']];       
			$newarr['totprice'] = $generalobj->booking_currency($price, $_SESSION['sess_price_ratio']); 
			
			$newarr['iRideId'] = $db_ride_details[$i]['iRideId'];
			$newarr['vMainDeparture'] = $db_ride_details[$i]['vMainArrival'];
			$newarr['vMainArrival'] = $db_ride_details[$i]['vMainDeparture'];
			$newarr['dDateOut'] = $db_ride_details[$i]['dDateRet'];
			$newarr['strtotime'] = strtotime($db_ride_details[$i]['dDateRet']);
			$newarr['memberimage'] = $db_ride_details[$i]['img'];
			$newarr['rating'] = round($review);
			$newarr['membercar'] = $db_car[0]['vMake']." ".$db_car[0]['vTitle'];
			if($db_ride_details[$i]['eRideType'] == 'Reccuring'){
				$newarr['deptime'] = $db_ride_details[$i]['vMainReturnTime'];
				}else{                                           
				$newarr['deptime'] = $db_ride_details[$i]['vMainArrivalTime'];
			}
			$newarr['return'] = 'Yes'; 
			$newarr['pref'] = $pref;
			array_push($mainarr,$newarr);                 
		}                                             
	}  
	
	shuffle($mainarr);
	//array_sort_by_column($mainarr, 'strtotime');
	
	$featuredin = $ridesobj->get_featuredin_details();
	$smarty->assign("featuredin",$featuredin);
	$smarty->assign("airportmainarr",$mainarr);
	#echo "<pre>";print_r($mainarr);exit;
	# Get details of rides  of Airport Ends#
	
	# Get details of rides of Ladies Only#
	$sql = "SELECT rides_new.iRideId, rides_new.iMemberId, rides_new.vMainDeparture, rides_new.vMainArrival, rides_new.eRideType, rides_new.vMainOutBoundTime,rides_new.vMainDepartureTime, rides_new.vMainReturnTime, rides_new.vMainArrivalTime,  rides_new.iMemberCarId, ride_dates.dDateOut, ride_dates.dDateRet,member.vImage,member.eGender, rides_new.fRatio_".$_SESSION['sess_price_ratio']."     
	FROM rides_new 
	LEFT JOIN ride_dates ON ride_dates.iRideId = rides_new.iRideId
	LEFT JOIN member ON member.iMemberId = rides_new.iMemberId 
	WHERE rides_new.eLadiesOnly = 'Yes'
	AND rides_new.eStatus = 'Active' $ccsql  
	AND (ride_dates.dDateOut >= '".$todaydate."'  OR ride_dates.dDateRet >= '".$todaydate."')
	ORDER BY RAND() 
	LIMIT 0,15";
	$db_ride_details_ladies = $obj->MySQLSelect($sql);
	#echo "<pre>";print_r($db_ride_details_ladies); exit;
	
	$mainarr_ladies = array();     
	for($i=0;$i<count($db_ride_details_ladies);$i++){
		$newarr_ladies = array();
		#member_photo
		if(is_file($tconfig["tsite_upload_images_member_path"].$db_ride_details_ladies[$i]['iMemberId']."/2_".$db_ride_details_ladies[$i]['vImage']))
		{
			$db_ride_details_ladies[$i]['img'] = $tconfig["tsite_upload_images_member"].$db_ride_details_ladies[$i]['iMemberId']."/2_".$db_ride_details_ladies[$i]['vImage'];
		}
		else
		{
			if($db_ride_details_ladies[$i]['eGender']=='Male'){
				$db_ride_details_ladies[$i]['img'] = $tconfig["tsite_images"].'150x150male.png';
				}else{
				$db_ride_details_ladies[$i]['img'] = $tconfig["tsite_images"].'150x150female.png';
			}
			//    $db_ride_details_ladies[$i]['img'] = $tconfig["tsite_images"].'user-man-54.png';
			
		}
		
		# member_rating
		$sql = "SELECT count(iRateId) as tot_count_rating,SUM(iRate) as tot_sum_rating 
		FROM member_rating WHERE iMemberToId = ".$db_ride_details_ladies[$i]['iMemberId']; 
		$db_member_ratings_lad_rides = $obj->MySQLSelect($sql);
		
		$review_lad_rides = $db_member_ratings_lad_rides[0]['tot_sum_rating'];
		$review_lad_rides = $review_lad_rides / $db_member_ratings_lad_rides[0]['tot_count_rating'];       
		$review_lad_rides = ($review_lad_rides * 100) / 5;
		
		#member_car
		$sql = "SELECT member_car.iMemberCarId, member_car.iSeats, make.vMake, model.vTitle 
		FROM member_car 
		LEFT JOIN make ON make.iMakeId = member_car.iMakeId 
		LEFT JOIN model ON model.iModelId = member_car.iModelId
		WHERE  member_car.iMemberCarId = '".$db_ride_details_ladies[$i]['iMemberCarId']."'";
		$db_car_lad_rides = $obj->MySQLSelect($sql); 
		
		$sql = "SELECT * FROM member_car_preferences WHERE iMemberId = '".$db_ride_details_ladies[$i]['iMemberId']."'";
		$db_pref = $obj->MySQLSelect($sql); 
		
		$pref = '';
		for($op=0;$op<count($db_pref);$op++){
			$img = get_prefrence_img($db_pref[$op]['iPreferencesId'], $db_pref[$op]['vType']);       
			if($img != ''){
				$pref .= $img;
			}
		}
		
		
		if($db_ride_details_ladies[$i]['dDateOut'] != '0000-00-00' && strtotime($db_ride_details_ladies[$i]['dDateOut']) >= strtotime($todaydate)){
			# ride price 
			$sql="select SUM(fPrice) as tot_price from ride_points_new where iRideId='".$db_ride_details_ladies[$i]['iRideId']."' and eReverse='No'";
			$db_total_price_ladies_rides = $obj->MySQLSelect($sql);
			
			$price = $db_total_price_ladies_rides[0]['tot_price'] * $db_ride_details_ladies[$i]['fRatio_'.$_SESSION['sess_price_ratio']];       
			$newarr_ladies['totprice'] = $generalobj->booking_currency($price, $_SESSION['sess_price_ratio']); 
			
			$newarr_ladies['iRideId'] = $db_ride_details_ladies[$i]['iRideId'];
			$newarr_ladies['vMainDeparture'] = $db_ride_details_ladies[$i]['vMainDeparture'];
			$newarr_ladies['vMainArrival'] = $db_ride_details_ladies[$i]['vMainArrival'];
			$newarr_ladies['dDateOut'] = $db_ride_details_ladies[$i]['dDateOut'];
			$newarr_ladies['strtotime'] = strtotime($db_ride_details_ladies[$i]['dDateOut']);
			$newarr_ladies['memberimage'] = $db_ride_details_ladies[$i]['img'];
			$newarr_ladies['rating'] = $review_lad_rides; 
			$newarr_ladies['membercar'] = $db_car_lad_rides[0]['vMake']." ".$db_car_lad_rides[0]['vTitle'];
			if($db_ride_details_ladies[$i]['eRideType'] == 'Reccuring'){
				$newarr_ladies['deptime'] = $db_ride_details_ladies[$i]['vMainOutBoundTime'];
				}else{
				$newarr_ladies['deptime'] = $db_ride_details_ladies[$i]['vMainDepartureTime'];
			}
			$newarr_ladies['return'] = 'No';
			$newarr_ladies['pref'] = $pref;           
			array_push($mainarr_ladies,$newarr_ladies);  
		}
		
		if($db_ride_details_ladies[$i]['dDateRet'] != '0000-00-00' && strtotime($db_ride_details_ladies[$i]['dDateRet']) >= strtotime($todaydate)){
			# ride price
			$sql="select SUM(fPrice) as tot_price from ride_points_new where iRideId='".$db_ride_details_ladies[$i]['iRideId']."' and eReverse='Yes'";
			$db_total_price_ladies_rides_ret = $obj->MySQLSelect($sql);
			
			$price = $db_total_price_ladies_rides_ret[0]['tot_price'] * $db_ride_details_ladies[$i]['fRatio_'.$_SESSION['sess_price_ratio']];       
			$newarr_ladies['totprice'] = $generalobj->booking_currency($price, $_SESSION['sess_price_ratio']); 
			
			$newarr_ladies['iRideId'] = $db_ride_details_ladies[$i]['iRideId'];
			$newarr_ladies['vMainDeparture'] = $db_ride_details_ladies[$i]['vMainArrival'];
			$newarr_ladies['vMainArrival'] = $db_ride_details_ladies[$i]['vMainDeparture'];
			$newarr_ladies['dDateOut'] = $db_ride_details_ladies[$i]['dDateRet'];
			$newarr_ladies['strtotime'] = strtotime($db_ride_details_ladies[$i]['dDateRet']);
			$newarr_ladies['memberimage'] = $db_ride_details_ladies[$i]['img'];
			$newarr_ladies['rating'] = round($review_lad_rides);
			$newarr_ladies['membercar'] = $db_car_lad_rides[0]['vMake']." ".$db_car_lad_rides[0]['vTitle'];
			if($db_ride_details_ladies[$i]['eRideType'] == 'Reccuring'){
				$newarr_ladies['deptime'] = $db_ride_details_ladies[$i]['vMainReturnTime'];
				}else{                                                     
				$newarr_ladies['deptime'] = $db_ride_details_ladies[$i]['vMainArrivalTime'];
			}
			$newarr_ladies['return'] = 'Yes';
			$newarr_ladies['pref'] = $pref;  
			array_push($mainarr_ladies,$newarr_ladies);               
		}
	}
	
	//array_sort_by_column($mainarr_ladies, 'strtotime');
	
	
	#echo "<pre>";print_r($mainarr_ladies); exit;
	$smarty->assign("ladiesmainarr",$mainarr_ladies);
	
	# Get details of rides of Ladies Only Ends#
	
	# Get details of shopping rides #
	$sql = "SELECT rides_new.iRideId, rides_new.iMemberId, rides_new.vMainDeparture, rides_new.vMainArrival, rides_new.eRideType, rides_new.vMainOutBoundTime,rides_new.vMainDepartureTime, rides_new.vMainReturnTime, rides_new.vMainArrivalTime,  rides_new.iMemberCarId, ride_dates.dDateOut, ride_dates.dDateRet,member.vImage,member.eGender, rides_new.fRatio_".$_SESSION['sess_price_ratio']."     
	FROM rides_new 
	LEFT JOIN ride_dates ON ride_dates.iRideId = rides_new.iRideId
	LEFT JOIN member ON member.iMemberId = rides_new.iMemberId 
	WHERE rides_new.eRidePlaceType = 'Shopping'
	AND rides_new.eStatus = 'Active' $ccsql   
	AND (ride_dates.dDateOut >= '".$todaydate."'  OR ride_dates.dDateRet >= '".$todaydate."')
	ORDER BY RAND() 
	LIMIT 0,15";
	$db_ride_details_shopping = $obj->MySQLSelect($sql);
	#echo "<pre>";print_r($db_ride_details_shopping); exit;
	
	$mainarr_shopping = array();     
	for($i=0;$i<count($db_ride_details_shopping);$i++){
		$newarr_shopping = array();
		#member_photo
		if(is_file($tconfig["tsite_upload_images_member_path"].$db_ride_details_shopping[$i]['iMemberId']."/2_".$db_ride_details_shopping[$i]['vImage']))
		{
			$db_ride_details_shopping[$i]['img'] = $tconfig["tsite_upload_images_member"].$db_ride_details_shopping[$i]['iMemberId']."/2_".$db_ride_details_shopping[$i]['vImage'];
		}
		else
		{
			// $db_ride_details_shopping[$i]['img'] = $tconfig["tsite_images"].'user-man-54.png';
			if($db_ride_details_shopping[$i]['eGender']=='Male'){
				$db_ride_details_shopping[$i]['img'] = $tconfig["tsite_images"].'150x150male.png';
				}else{
				$db_ride_details_shopping[$i]['img'] = $tconfig["tsite_images"].'150x150female.png';
			}
		}
		
		# member_rating
		$sql = "SELECT count(iRateId) as tot_count_rating,SUM(iRate) as tot_sum_rating 
		FROM member_rating WHERE iMemberToId = ".$db_ride_details_shopping[$i]['iMemberId']; 
		$db_member_ratings_shopping = $obj->MySQLSelect($sql);
		
		$review_shopping = $db_member_ratings_shopping[0]['tot_sum_rating'];
		$review_shopping = $review_shopping / $db_member_ratings_shopping[0]['tot_count_rating'];
		
		$review_shopping = ($review_shopping * 100) / 5;
		
		#member_car
		$sql = "SELECT member_car.iMemberCarId, member_car.iSeats, make.vMake, model.vTitle 
		FROM member_car 
		LEFT JOIN make ON make.iMakeId = member_car.iMakeId 
		LEFT JOIN model ON model.iModelId = member_car.iModelId
		WHERE  member_car.iMemberCarId = '".$db_ride_details_shopping[$i]['iMemberCarId']."'";
		$db_car_shopping = $obj->MySQLSelect($sql);
		
		$sql = "SELECT * FROM member_car_preferences WHERE iMemberId = '".$db_ride_details_shopping[$i]['iMemberId']."'";
		$db_pref = $obj->MySQLSelect($sql); 
		
		$pref = '';
		for($op=0;$op<count($db_pref);$op++){
			$img = get_prefrence_img($db_pref[$op]['iPreferencesId'], $db_pref[$op]['vType']);       
			if($img != ''){
				$pref .= $img;
			}
		}
		
		
		if($db_ride_details_shopping[$i]['dDateOut'] != '0000-00-00' && strtotime($db_ride_details_shopping[$i]['dDateOut']) >= strtotime($todaydate)){
			# ride price 
			$sql="select SUM(fPrice) as tot_price from ride_points_new where iRideId='".$db_ride_details_shopping[$i]['iRideId']."' and eReverse='No'";
			$db_total_price_shopping = $obj->MySQLSelect($sql);
			
			$price = $db_total_price_shopping[0]['tot_price'] * $db_ride_details_shopping[$i]['fRatio_'.$_SESSION['sess_price_ratio']];       
			$newarr_shopping['totprice'] = $generalobj->booking_currency($price, $_SESSION['sess_price_ratio']);
			
			$newarr_shopping['iRideId'] = $db_ride_details_shopping[$i]['iRideId'];
			$newarr_shopping['vMainDeparture'] = $db_ride_details_shopping[$i]['vMainDeparture'];
			$newarr_shopping['vMainArrival'] = $db_ride_details_shopping[$i]['vMainArrival'];
			$newarr_shopping['dDateOut'] = $db_ride_details_shopping[$i]['dDateOut'];
			$newarr_shopping['strtotime'] = strtotime($db_ride_details_shopping[$i]['dDateOut']);
			$newarr_shopping['memberimage'] = $db_ride_details_shopping[$i]['img'];
			$newarr_shopping['rating'] = $review_shopping; 
			$newarr_shopping['membercar'] = $db_car_shopping[0]['vMake']." ".$db_car_shopping[0]['vTitle'];
			if($db_ride_details_shopping[$i]['eRideType'] == 'Reccuring'){
				$newarr_shopping['deptime'] = $db_ride_details_shopping[$i]['vMainOutBoundTime'];
				}else{
				$newarr_shopping['deptime'] = $db_ride_details_shopping[$i]['vMainDepartureTime'];
			}
			$newarr_shopping['return'] = 'No';
			$newarr_shopping['pref'] = $pref;
			array_push($mainarr_shopping,$newarr_shopping);  
		}
		
		if($db_ride_details_shopping[$i]['dDateRet'] != '0000-00-00' && strtotime($db_ride_details_shopping[$i]['dDateRet']) >= strtotime($todaydate)){
			# ride price
			$sql="select SUM(fPrice) as tot_price from ride_points_new where iRideId='".$db_ride_details_shopping[$i]['iRideId']."' and eReverse='Yes'";
			$db_total_price_shopping_ret = $obj->MySQLSelect($sql);
			
			$price = $db_total_price_shopping_ret[0]['tot_price'] * $db_ride_details_shopping[$i]['fRatio_'.$_SESSION['sess_price_ratio']];       
			$newarr_shopping['totprice'] = $generalobj->booking_currency($price, $_SESSION['sess_price_ratio']);
			
			$newarr_shopping['iRideId'] = $db_ride_details_shopping[$i]['iRideId'];
			$newarr_shopping['vMainDeparture'] = $db_ride_details_shopping[$i]['vMainArrival'];
			$newarr_shopping['vMainArrival'] = $db_ride_details_shopping[$i]['vMainDeparture'];
			$newarr_shopping['dDateOut'] = $db_ride_details_shopping[$i]['dDateRet'];
			$newarr_shopping['strtotime'] = strtotime($db_ride_details_shopping[$i]['dDateRet']);
			$newarr_shopping['memberimage'] = $db_ride_details_shopping[$i]['img'];
			$newarr_shopping['rating'] = round($review_shopping);
			$newarr_shopping['membercar'] = $db_car_shopping[0]['vMake']." ".$db_car_shopping[0]['vTitle'];
			if($db_ride_details_shopping[$i]['eRideType'] == 'Reccuring'){
				$newarr_shopping['deptime'] = $db_ride_details_shopping[$i]['vMainReturnTime'];
				}else{
				$newarr_shopping['deptime'] = $db_ride_details_shopping[$i]['vMainArrivalTime'];
			}
			$newarr_shopping['return'] = 'Yes';
			$newarr_shopping['pref'] = $pref;
			array_push($mainarr_shopping,$newarr_shopping);                 
		}
	}
	
	//array_sort_by_column($mainarr_shopping, 'strtotime');
	
	
	#echo "<pre>";print_r($mainarr_shopping); exit;
	$smarty->assign("shoppingmainarr",$mainarr_shopping);
	
	# Get details of shopping rides ends#
	
	# Get details of today's rides #
	$sql = "SELECT rides_new.iRideId, rides_new.iMemberId, rides_new.vMainDeparture, rides_new.vMainArrival, rides_new.eRideType, rides_new.vMainOutBoundTime,rides_new.vMainDepartureTime, rides_new.vMainReturnTime, rides_new.vMainArrivalTime,  rides_new.iMemberCarId, rides_new.iSeats, ride_dates.dDateOut, ride_dates.dDateRet, rides_new.fRatio_".$_SESSION['sess_price_ratio']."     
	FROM rides_new 
	LEFT JOIN ride_dates ON ride_dates.iRideId = rides_new.iRideId
	WHERE rides_new.eStatus = 'Active'  $ccsql  
	AND (ride_dates.dDateOut = '".$todaydate."' OR ride_dates.dDateRet = '".$todaydate."')
	GROUP BY rides_new.iMemberId
	ORDER BY RAND() 
	LIMIT 0,5";
	$db_ride_today = $obj->MySQLSelect($sql);
	#echo "<pre>";print_r($db_ride_today); exit;
	
	$mainarr_today = array();     
	for($i=0;$i<count($db_ride_today);$i++){
		$newarr_today = array();
		# member_rating
		$sql = "SELECT count(iRateId) as tot_count_rating,SUM(iRate) as tot_sum_rating 
		FROM member_rating WHERE iMemberToId = ".$db_ride_today[$i]['iMemberId']; 
		$db_member_ratings_today = $obj->MySQLSelect($sql);
		
		$review_today = $db_member_ratings_today[0]['tot_sum_rating'];
		$review_today = $review_today / $db_member_ratings_today[0]['tot_count_rating'];
		
		$review_today = ($review_today * 100) / 5;
		
		#member_car
		$sql = "SELECT member_car.iMemberCarId, member_car.iSeats, make.vMake, model.vTitle 
		FROM member_car 
		LEFT JOIN make ON make.iMakeId = member_car.iMakeId 
		LEFT JOIN model ON model.iModelId = member_car.iModelId
		WHERE  member_car.iMemberCarId = '".$db_ride_today[$i]['iMemberCarId']."'";
		$db_car_today = $obj->MySQLSelect($sql);
		if($db_ride_today[$i]['dDateOut'] != '0000-00-00'){
			# ride price
			$sql="select SUM(fPrice) as tot_price from ride_points_new where iRideId='".$db_ride_today[$i]['iRideId']."' and eReverse='No'";
			$db_total_price_today = $obj->MySQLSelect($sql);
			
			$price = $db_total_price_today[0]['tot_price'] * $db_ride_today[$i]['fRatio_'.$_SESSION['sess_price_ratio']];       
			$newarr_today['totprice'] = $generalobj->booking_currency($price, $_SESSION['sess_price_ratio']);
			
			$newarr_today['iRideId'] = $db_ride_today[$i]['iRideId'];
			$newarr_today['vMainDeparture'] = $db_ride_today[$i]['vMainDeparture'];
			$newarr_today['vMainArrival'] = $db_ride_today[$i]['vMainArrival'];
			$newarr_today['dDateOut'] = $db_ride_today[$i]['dDateOut'];
			$newarr_today['strtotime'] = strtotime($db_ride_today[$i]['dDateOut']);
			$newarr_today['rating'] = $review_today; 
			$newarr_today['membercar'] = $db_car_today[0]['vMake']." ".$db_car_today[0]['vTitle'];
			$newarr_today['iSeats'] = $db_ride_today[$i]['iSeats'];
			if($db_ride_today[$i]['eRideType'] == 'Reccuring'){
				$newarr_today['deptime'] = $db_ride_today[$i]['vMainOutBoundTime'];
				}else{
				$newarr_today['deptime'] = $db_ride_today[$i]['vMainDepartureTime'];
			}
			$newarr_today['return'] = 'No';
			array_push($mainarr_today,$newarr_today);  
		}
		
		if($db_ride_today[$i]['dDateRet'] != '0000-00-00'){
			
			# ride price
			$sql="select SUM(fPrice) as tot_price from ride_points_new where iRideId='".$db_ride_today[$i]['iRideId']."' and eReverse='Yes'";
			$db_total_price_today_ret = $obj->MySQLSelect($sql);
			
			$price = $db_total_price_today_ret[0]['tot_price'] * $db_ride_today[$i]['fRatio_'.$_SESSION['sess_price_ratio']];       
			$newarr_today['totprice'] = $generalobj->booking_currency($price, $_SESSION['sess_price_ratio']);
			
			$newarr_today['iRideId'] = $db_ride_today[$i]['iRideId'];
			$newarr_today['vMainDeparture'] = $db_ride_today[$i]['vMainArrival'];
			$newarr_today['vMainArrival'] = $db_ride_today[$i]['vMainDeparture'];
			$newarr_today['dDateOut'] = $db_ride_today[$i]['dDateRet'];
			$newarr_today['strtotime'] = strtotime($db_ride_today[$i]['dDateRet']);
			$newarr_today['rating'] = round($review_today);
			$newarr_today['membercar'] = $db_car_today[0]['vMake']." ".$db_car_today[0]['vTitle'];
			$newarr_today['iSeats'] = $db_ride_today[$i]['iSeats'];
			if($db_ride_today[$i]['eRideType'] == 'Reccuring'){
				$newarr_today['deptime'] = $db_ride_today[$i]['vMainReturnTime'];
				}else{
				$newarr_today['deptime'] = $db_ride_today[$i]['vMainArrivalTime'];
			}
			$newarr_today['return'] = 'Yes';
			array_push($mainarr_today,$newarr_today);                 
		}
	}
	
	//array_sort_by_column($mainarr_today, 'strtotime');
	
	
	#echo "<pre>";print_r($mainarr_today); exit;
	$smarty->assign("todaymainarr",$mainarr_today);
	# Get details of today's rides  ends# 
	
	# Latest Out Rides
	$sql = "SELECT iRideId,dDateOut from ride_dates WHERE dDateOut >= '".$todaydate."' 
	GROUP BY iRideId ORDER BY dDateOut ASC";
	$db_latest_out = $obj->MySQLSelect($sql);
	$mainarr_latest_out = array();      
	for($i=0;$i<count($db_latest_out);$i++){
		$newarr_latest_out = array();
		$sql = "SELECT rides_new.iRideId, rides_new.iMemberId, rides_new.vMainDeparture, rides_new.vMainArrival, rides_new.eRideType, rides_new.vMainOutBoundTime,rides_new.vMainDepartureTime, rides_new.vMainReturnTime, rides_new.vMainArrivalTime,  rides_new.iMemberCarId, rides_new.iSeats, member.vImage,member.eGender, rides_new.fRatio_".$_SESSION['sess_price_ratio'].", member_car.iMemberCarId, member_car.iSeats, make.vMake, model.vTitle        
		FROM rides_new 
		LEFT JOIN member ON member.iMemberId = rides_new.iMemberId
		LEFT JOIN member_car ON member_car.iMemberCarId=rides_new.iMemberCarId
		LEFT JOIN make ON make.iMakeId = member_car.iMakeId 
		LEFT JOIN model ON model.iModelId = member_car.iModelId 
		WHERE rides_new.eStatus = 'Active' $ccsql   
		AND rides_new.iRideId = '".$db_latest_out[$i]['iRideId']."'";
		$db_ride_latest_out = $obj->MySQLSelect($sql);    
		
		if(count($db_ride_latest_out) > 0){  
			$sql="select SUM(fPrice) as tot_price_out from ride_points_new where iRideId='".$db_latest_out[$i]['iRideId']."' and eReverse='No'";
			$db_total_price_latest_out = $obj->MySQLSelect($sql); 
			
			$priceout = $db_total_price_latest_out[0]['tot_price_out'] * $db_ride_latest_out[0]['fRatio_'.$_SESSION['sess_price_ratio']];       
			$newarr_latest_out['latpriceout'] = $generalobj->booking_currency($priceout, $_SESSION['sess_price_ratio']);
			
			#member_photo
			if(is_file($tconfig["tsite_upload_images_member_path"].$db_ride_latest_out[0]['iMemberId']."/2_".$db_ride_latest_out[0]['vImage']))
			{
				$db_ride_latest_out[0]['img'] = $tconfig["tsite_upload_images_member"].$db_ride_latest_out[0]['iMemberId']."/2_".$db_ride_latest_out[0]['vImage'];
			}
			else
			{
				// $db_ride_details_shopping[$i]['img'] = $tconfig["tsite_images"].'user-man-54.png';
				if($db_ride_latest_out[0]['eGender']=='Male'){
					$db_ride_latest_out[0]['img'] = $tconfig["tsite_images"].'150x150male.png';
					}else{
					$db_ride_latest_out[0]['img'] = $tconfig["tsite_images"].'150x150female.png';
				}
			}
			$newarr_latest_out['iRideId'] = $db_latest_out[$i]['iRideId'];
			$newarr_latest_out['vMainDeparture'] = $db_ride_latest_out[0]['vMainDeparture'];
			//$vmaindep = explode(",",$db_ride_latest[$i]['vMainDeparture']);
			//$newarr_latest['vMainDeparture'] = $vmaindep[0];
			$newarr_latest_out['vMainArrival'] = $db_ride_latest_out[0]['vMainArrival'];
			$newarr_latest_out['dDateOut'] = $db_latest_out[$i]['dDateOut'];
			$newarr_latest_out['strtotime'] = strtotime($db_latest_out[$i]['dDateOut']);
			$newarr_latest_out['membercar'] = $db_ride_latest_out[0]['vMake']." ".$db_ride_latest_out[0]['vTitle'];
			$newarr_latest_out['iSeats'] = $db_ride_latest_out[0]['iSeats'];
			$newarr_latest_out['memberimage'] = $db_ride_latest_out[0]['img'];
			if($db_ride_latest_out[0]['eRideType'] == 'Reccuring'){
				$newarr_latest_out['deptime'] = $db_ride_latest_out[0]['vMainOutBoundTime'];
				}else{
				$newarr_latest_out['deptime'] = $db_ride_latest_out[0]['vMainDepartureTime'];
			}
			$newarr_latest_out['return'] = 'No';
			$newarr_latest_out['rideprice'] = $db_total_price_latest_out[0]['tot_price_out'];
			array_push($mainarr_latest_out,$newarr_latest_out);
		}  
	} 
	#echo "<pre>";print_r($mainarr_latest_out);exit;
	$smarty->assign("latestmainarrout",$mainarr_latest_out);
	# Get details of latest's out rides #
	
	
	/*
		# Latest RET Rides
		$sql = "SELECT iRideId,dDateRet from ride_dates WHERE dDateRet >= '".$todaydate."' 
		GROUP BY iRideId ORDER BY dDateRet ASC LIMIT 0,2";
		$db_latest_ret = $obj->MySQLSelect($sql);
		$mainarr_latest_ret = array();      
		for($i=0;$i<count($db_latest_ret);$i++){
		$newarr_latest_ret = array();
		$sql = "SELECT rides_new.iRideId, rides_new.iMemberId, rides_new.vMainDeparture, rides_new.vMainArrival, rides_new.eRideType, rides_new.vMainOutBoundTime,rides_new.vMainDepartureTime, rides_new.vMainReturnTime, rides_new.vMainArrivalTime,  rides_new.iMemberCarId, rides_new.iSeats, member.vImage,member.eGender, rides_new.fRatio_".$_SESSION['sess_price_ratio'].", member_car.iMemberCarId, member_car.iSeats, make.vMake, model.vTitle        
		FROM rides_new 
		LEFT JOIN member ON member.iMemberId = rides_new.iMemberId
		LEFT JOIN member_car ON member_car.iMemberCarId=rides_new.iMemberCarId
		LEFT JOIN make ON make.iMakeId = member_car.iMakeId 
		LEFT JOIN model ON model.iModelId = member_car.iModelId 
		WHERE rides_new.eStatus = 'Active' $ccsql  
		AND rides_new.iRideId = '".$db_latest_ret[$i]['iRideId']."'";
		$db_ride_latest_ret = $obj->MySQLSelect($sql);    
		
		if(count($db_ride_latest_ret) > 0){  
		$sql="select SUM(fPrice) as tot_price_ret from ride_points_new where iRideId='".$db_latest_ret[$i]['iRideId']."' and eReverse='No'";
		$db_total_price_latest_ret = $obj->MySQLSelect($sql); 
        
		$priceret = $db_total_price_latest_ret[0]['tot_price_ret'] * $db_ride_latest_ret[0]['fRatio_'.$_SESSION['sess_price_ratio']];       
		$newarr_latest_ret['latpriceout'] = $generalobj->booking_currency($priceret, $_SESSION['sess_price_ratio']);
		
		#member_photo
		if(is_file($tconfig["tsite_upload_images_member_path"].$db_ride_latest_ret[0]['iMemberId']."/2_".$db_ride_latest_ret[0]['vImage']))
		{
        $db_ride_latest_ret[0]['img'] = $tconfig["tsite_upload_images_member"].$db_ride_latest_ret[0]['iMemberId']."/2_".$db_ride_latest_ret[0]['vImage'];
		}
		else
		{
		// $db_ride_details_shopping[$i]['img'] = $tconfig["tsite_images"].'user-man-54.png';
		if($db_ride_latest_ret[0]['eGender']=='Male'){
		$db_ride_latest_ret[0]['img'] = $tconfig["tsite_images"].'150x150male.png';
		}else{
		$db_ride_latest_ret[0]['img'] = $tconfig["tsite_images"].'150x150female.png';
		}
		}
		$newarr_latest_ret['iRideId'] = $db_latest_ret[$i]['iRideId'];
		$newarr_latest_ret['vMainDeparture'] = $db_ride_latest_ret[0]['vMainArrival'];
		//$vmaindep = explode(",",$db_ride_latest[$i]['vMainDeparture']);
		//$newarr_latest['vMainDeparture'] = $vmaindep[0];
		$newarr_latest_ret['vMainArrival'] = $db_ride_latest_ret[0]['vMainDeparture'];
		$newarr_latest_ret['dDateOut'] = $db_latest_ret[$i]['dDateRet'];
		$newarr_latest_ret['strtotime'] = strtotime($db_latest_ret[$i]['dDateRet']);
		$newarr_latest_ret['membercar'] = $db_ride_latest_ret[0]['vMake']." ".$db_ride_latest_ret[0]['vTitle'];
		$newarr_latest_ret['iSeats'] = $db_ride_latest_ret[0]['iSeats'];
		$newarr_latest_ret['memberimage'] = $db_ride_latest_ret[0]['img'];
		if($db_ride_latest_ret[0]['eRideType'] == 'Reccuring'){
        $newarr_latest_ret['deptime'] = $db_ride_latest_ret[0]['vMainOutBoundTime'];
		}else{
        $newarr_latest_ret['deptime'] = $db_ride_latest_ret[0]['vMainDepartureTime'];
		}
		$newarr_latest_ret['return'] = 'No';
		array_push($mainarr_latest_ret,$newarr_latest_ret);
		}   
		} 
		#echo "<pre>";print_r($mainarr_latest_ret);exit;
		$smarty->assign("latestmainarrret",$mainarr_latest_ret);
		# Get details of latest's out rides #
	*/
	
	# Get details of latest's rides #
	$sql = "SELECT rides_new.iRideId, rides_new.iMemberId, rides_new.vMainDeparture, rides_new.vMainArrival, rides_new.eRideType, rides_new.vMainOutBoundTime,rides_new.vMainDepartureTime, rides_new.vMainReturnTime, rides_new.vMainArrivalTime,rides_new.iMemberCarId, rides_new.iSeats, rides_new.fBoxPrice,rides_new.fDocumentPrice,rides_new.fLuggagePrice,ride_dates.dDateOut, ride_dates.dDateRet,member.vImage,member.eGender, rides_new.fRatio_".$_SESSION['sess_price_ratio']."        
	FROM ride_dates 
	LEFT JOIN rides_new ON ride_dates.iRideId = rides_new.iRideId
	LEFT JOIN member ON member.iMemberId = rides_new.iMemberId 
	WHERE rides_new.eStatus = 'Active' $ccsql  
	AND (ride_dates.dDateOut >= '".$todaydate."')
	GROUP BY ride_dates.dDateOut
	ORDER BY ride_dates.dDateOut ASC   
	LIMIT 0,6";
	$db_ride_latest = $obj->MySQLSelect($sql);  
	#echo "<pre>";print_r($db_ride_latest); exit; 
	
	$mainarr_latest = array();     
	for($i=0;$i<count($db_ride_latest);$i++){
		$newarr_latest = array();
		#member_photo
		if(is_file($tconfig["tsite_upload_images_member_path"].$db_ride_latest[$i]['iMemberId']."/2_".$db_ride_latest[$i]['vImage']))
		{
			$db_ride_latest[$i]['img'] = $tconfig["tsite_upload_images_member"].$db_ride_latest[$i]['iMemberId']."/2_".$db_ride_latest[$i]['vImage'];
		}
		else
		{
			// $db_ride_details_shopping[$i]['img'] = $tconfig["tsite_images"].'user-man-54.png';
			if($db_ride_latest[$i]['eGender']=='Male'){
				$db_ride_latest[$i]['img'] = $tconfig["tsite_images"].'150x150male.png';
				}else{
				$db_ride_latest[$i]['img'] = $tconfig["tsite_images"].'150x150female.png';
			}
		}
		
		# member_rating
		$sql = "SELECT count(iRateId) as tot_count_rating,SUM(iRate) as tot_sum_rating 
		FROM member_rating WHERE iMemberToId = ".$db_ride_latest[$i]['iMemberId']; 
		$db_member_ratings_latest = $obj->MySQLSelect($sql);
		
		$review_latest = $db_member_ratings_latest[0]['tot_sum_rating'];
		$review_latest = $review_latest / $db_member_ratings_latest[0]['tot_count_rating'];
		$review_latest = ($review_latest * 100) / 5;
		
		#member_car
		$sql = "SELECT member_car.iMemberCarId, member_car.iSeats, make.vMake, model.vTitle 
		FROM member_car 
		LEFT JOIN make ON make.iMakeId = member_car.iMakeId 
		LEFT JOIN model ON model.iModelId = member_car.iModelId
		WHERE  member_car.iMemberCarId = '".$db_ride_latest[$i]['iMemberCarId']."'";
		$db_car_latest = $obj->MySQLSelect($sql);
		
		$sql = "SELECT * FROM member_car_preferences WHERE iMemberId = '".$db_ride_latest[$i]['iMemberId']."'";
		$db_pref_latest = $obj->MySQLSelect($sql);
		
		$pref = '';
		for($op=0;$op<count($db_pref);$op++){
			$img = get_prefrence_img($db_pref[$op]['iPreferencesId'], $db_pref[$op]['vType']);       
			if($img != ''){
				$pref .= $img;
			}
		} 
		
		$Remainload = $generalobj->parcel_count($db_ride_latest[$i]['iRideId']);
		$newarr_latest['vDocRemain'] = $Remainload['vDoc'];  
		$newarr_latest['vBoxRemain'] = $Remainload['vBox'];  
		$newarr_latest['vLuggageRemain'] = $Remainload['vLuggage'];
		$newarr_latest['fDocumentPrice'] = $generalobj->booking_currency($db_ride_latest[$i]['fDocumentPrice'], $_SESSION['sess_price_ratio']);
		$newarr_latest['fBoxPrice'] = $generalobj->booking_currency($db_ride_latest[$i]['fBoxPrice'], $_SESSION['sess_price_ratio']);
		$newarr_latest['fLuggagePrice'] = $generalobj->booking_currency($db_ride_latest[$i]['fLuggagePrice'], $_SESSION['sess_price_ratio']);
		
		if($db_ride_latest[$i]['dDateOut'] != '0000-00-00'){
			# ride price
			$sql="select SUM(fPrice) as tot_price from ride_points_new where iRideId='".$db_ride_latest[$i]['iRideId']."' and eReverse='No'";
			$db_total_price_latest = $obj->MySQLSelect($sql); 
			
			$price = $db_total_price_latest[0]['tot_price'] * $db_ride_latest[$i]['fRatio_'.$_SESSION['sess_price_ratio']];       
			$newarr_latest['totprice'] = $generalobj->booking_currency($price, $_SESSION['sess_price_ratio']);
			
			
			$newarr_latest['iRideId'] = $db_ride_latest[$i]['iRideId'];
			$newarr_latest['vMainDeparture'] = $db_ride_latest[$i]['vMainDeparture'];
			//$vmaindep = explode(",",$db_ride_latest[$i]['vMainDeparture']);
			//$newarr_latest['vMainDeparture'] = $vmaindep[0];
			$newarr_latest['vMainArrival'] = $db_ride_latest[$i]['vMainArrival'];
			$newarr_latest['dDateOut'] = $db_ride_latest[$i]['dDateOut'];
			$newarr_latest['strtotime'] = strtotime($db_ride_latest[$i]['dDateOut']);
			$newarr_latest['rating'] = round($review_latest); 
			$newarr_latest['membercar'] = $db_car_latest[0]['vMake']." ".$db_car_latest[0]['vTitle'];
			$newarr_latest['iSeats'] = $db_ride_latest[$i]['iSeats'];
			$newarr_latest['memberimage'] = $db_ride_latest[$i]['img'];
			$newarr_latest['rating'] = $review_latest;
			if($db_ride_latest[$i]['eRideType'] == 'Reccuring'){
				$newarr_latest['deptime'] = $db_ride_latest[$i]['vMainOutBoundTime'];
				}else{
				$newarr_latest['deptime'] = $db_ride_latest[$i]['vMainDepartureTime'];
			}
			$newarr_latest['return'] = 'No';
			$newarr_latest['pref'] = $pref;
			
			/*### validate date ###*/
			$valid = 1;		
			if($newarr_latest['dDateOut'] < date("Y-m-d")) { //2015-10-28 < 2015-10-28
				$valid = 0;
			}
			
			if($newarr_latest['dDateOut'] == date("Y-m-d")) {
				if($newarr_latest['deptime'] < date("H:m")) {
					$valid = 0;
				}			
			}
			#echo '<pre>'; print_R($newarr); echo '</pre>';
			#echo "<BR>==".$valid."---".$newarr['deptime']."===".$newarr[$i]['dDateOut'] ."==out==".date("Y-m-d")."==".date("H:m");
			if($valid ==1)
			{
				array_push($mainarr_latest,$newarr_latest);  
			}
			/*### validate date end ###*/
		}
		
		if($db_ride_latest[$i]['dDateRet'] != '0000-00-00'){
			
			# ride price
			$sql="select SUM(fPrice) as tot_price from ride_points_new where iRideId='".$db_ride_latest[$i]['iRideId']."' and eReverse='Yes'";
			$db_total_price_latest_ret = $obj->MySQLSelect($sql);
			
			$price = $db_total_price_latest_ret[0]['tot_price'] * $db_ride_latest[$i]['fRatio_'.$_SESSION['sess_price_ratio']];       
			$newarr_latest['totprice'] = $generalobj->booking_currency($price, $_SESSION['sess_price_ratio']);       
			
			$newarr_latest['iRideId'] = $db_ride_latest[$i]['iRideId'];
			$newarr_latest['vMainDeparture'] = $db_ride_latest[$i]['vMainArrival'];
			$newarr_latest['vMainArrival'] = $db_ride_latest[$i]['vMainDeparture'];
			$newarr_latest['dDateOut'] = $db_ride_latest[$i]['dDateRet'];   
			$newarr_latest['strtotime'] = strtotime($db_ride_latest[$i]['dDateRet']);
			$newarr_latest['rating'] = round($review_latest);
			$newarr_latest['membercar'] = $db_car_latest[0]['vMake']." ".$db_car_latest[0]['vTitle'];
			$newarr_latest['iSeats'] = $db_ride_latest[$i]['iSeats'];
			$newarr_latest['memberimage'] = $db_ride_latest[$i]['img'];      
			if($db_ride_latest[$i]['eRideType'] == 'Reccuring'){
				$newarr_latest['deptime'] = $db_ride_latest[$i]['vMainReturnTime'];
				}else{
				$newarr_latest['deptime'] = $db_ride_latest[$i]['vMainArrivalTime'];
			}
			$newarr_latest['return'] = 'Yes';
			$newarr_latest['pref'] = $pref;
			
			/*### validate date ###*/
			$valid = 1;		
			if($newarr_latest['dDateOut'] < date("Y-m-d")) { //2015-10-28 < 2015-10-28
				$valid = 0;
			}
			
			if($newarr_latest['dDateOut'] == date("Y-m-d")) {
				if($newarr_latest['deptime'] < date("H:m")) {
					$valid = 0;
				}			
			}
			#echo '<pre>'; print_R($newarr); echo '</pre>';
			#echo "<BR>==".$valid."---".$newarr['deptime']."===".$newarr[$i]['dDateOut'] ."==try==".date("Y-m-d")."==".date("H:m");
			if($valid ==1)
			{
				array_push($mainarr_latest,$newarr_latest); 
			}
			/*### validate date ###*/
		}
	} 
	
	array_sort_by_column($mainarr_latest, 'strtotime');
	
	
	#echo "<pre>";print_r($mainarr_latest); exit;
	$smarty->assign("latestmainarr",$mainarr_latest);
	
	# Get details of latest's rides  ends#
	
	$sql = "SELECT iPageId, vPageTitle".$_SESSION['sess_lang_pref']." AS vPageTitle,tPageDesc".$_SESSION['sess_lang_pref']." as tPageDesc, iFrameCode, vImage FROM pages WHERE iPageId IN (7,8)";
	$db_box = $obj->MySQLSelect($sql); 
	
	for($i=0;$i<count($db_box);$i++){
		if($db_box[$i]['iPageId'] == 7){
			$smarty->assign("box1title",$db_box[$i]['vPageTitle']);
			$smarty->assign("box1video",$db_box[$i]['iFrameCode']);
			}else if($db_box[$i]['iPageId'] == 8){
			if(is_file($tconfig["tsite_upload_media_partners_path"]."/".$db_box[$i]['vImage']))
			{
				$db_box[$i]['vImage'] = $tconfig["tsite_upload_images_media_partners"]."/".$db_box[$i]['vImage'];
			} 
			else
			{
				$db_box[$i]['vImage'] = '';
			}
			
			$smarty->assign("box2title",$db_box[$i]['vPageTitle']);
			$smarty->assign("box2image",$db_box[$i]['vImage']);
			$smarty->assign("box2tPageDesc",$db_box[$i]['tPageDesc']);
		}
	}
	
	# Total Rides
	$sql = "SELECT count(iRideId) as tot_rides FROM rides_new WHERE 1=1"; 
	$db_total_rides = $obj->MySQLSelect($sql);
	$tot_rides = $db_total_rides[0]['tot_rides']; 
	$smarty->assign("tot_rides",$tot_rides);
	# Total Rides
	# Total Members
	$sql = "SELECT count(iMemberId) as tot_members FROM member WHERE 1=1"; 
	$db_total_members = $obj->MySQLSelect($sql);
	$tot_members = $db_total_members[0]['tot_members']; 
	$smarty->assign("tot_members",$tot_members);
	# Total Members
	# Total Bookings
	$sql = "SELECT count(iBookingId) as tot_booking FROM booking_new WHERE 1=1"; 
	$db_total_booking = $obj->MySQLSelect($sql);
	$tot_booking = $db_total_booking[0]['tot_booking']; 
	$smarty->assign("tot_booking",$tot_booking);
	# Total Bookings
	
	$sql = "SELECT iPageId, vPageTitle".$_SESSION['sess_lang_pref']." AS vPageTitle,tPageDesc".$_SESSION['sess_lang_pref']." as tPageDesc FROM pages WHERE iPageId = '11'";
	$db_welcome = $obj->MySQLSelect($sql);
	$smarty->assign("db_welcome",$db_welcome);
	
	$find_a_match = $generalobj->getpagedetails("13");
	$smarty->assign("find_a_match",$find_a_match);
	#echo "<pre>";print_r($find_a_match);exit;
	
	$start_your_trip = $generalobj->getpagedetails("14");
	$smarty->assign("start_your_trip",$start_your_trip);
	#echo "<pre>";print_r($start_your_trip);exit;
	
	$all_done = $generalobj->getpagedetails("15");
	$smarty->assign("all_done",$all_done);
	#echo "<pre>";print_r($all_done);exit;
	//echo $HOME_BLOG_LIMIT;exit;
	$sel = "SELECT * FROM  wp_posts WHERE  post_status =  'publish' AND  post_type LIKE  'post' ORDER BY post_date DESC LIMIT 0 , 4";
	$db_post = $obj->sql_query($sel);
	$smarty->assign("db_post",$db_post);
	
	##################### Logo Display Start ########################
	$sql = "SELECT vValue FROM configurations WHERE tDescription = 'Site Logo'";
	$db_logo = $obj->MySQLSelect($sql);
	$logo = $db_logo[0]['vValue'];
	if($logo == "")
	{
		$logodis = $tconfig['tsite_images']."home/".$THEME."/logo.png";
	}
	else if($logo != "")
	{
		$logodis = $tconfig['tsite_upload_site_logo'].$logo;
	}
	##################### Logo Dispalay End #########################
	$smarty->assign("logodis",$logodis);
?>