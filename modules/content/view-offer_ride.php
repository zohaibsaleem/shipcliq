<?php
  include_once(TPATH_CLASS_APP."class.customer.php");
  $CustomerObj = new Customer;
  $CustomerObj->check_member_login();
  $sess_iMemberId = $_SESSION["sess_iMemberId"];
  $CustomerObj->check_cancel_booking($sess_iMemberId);
  //echo "<pre>";
  //print_r($_SESSION); exit;


   $sql = "SELECT eEmailVarified, ePaymentEmailVerified,eStatus, vPaymentEmail,eCarPaperStatus,eLicenseStatus, ePhoneVerified,vBankAccountHolderName,vAccountNumber,vBankLocation,vBankName,vBIC_SWIFT_Code FROM member WHERE iMemberId = '".$sess_iMemberId."'";
  $db_mailvarification = $obj->MySQLSelect($sql);

  if($db_mailvarification[0]['eStatus'] == 'Pending'){
    header("Location:".$tconfig["tsite_url"]."index.php?file=m-dashboard&msg_code=0&var_msg=Your account is inactive. Please ensure your profile and member verifications are complete.");
    exit;
  }

  if($db_mailvarification[0]['eEmailVarified'] == 'No'){
    header("Location:".$tconfig["tsite_url"]."index.php?file=m-verification&msg_code=0&var_msg=Your email is not verified.");
    exit;
  }
  /*
  if($db_mailvarification[0]['ePaymentEmailVerified'] == 'No'){
    header("Location:".$tconfig["tsite_url"]."index.php?file=m-verification&msg_code=0&var_msg=Your Paypal email is not verify. Please verified your paypal email for offer new ride.");
    exit;
  }
  */
  if($PAYMENT_OPTION == "PayPal") {
		if($db_mailvarification[0]['vPaymentEmail'] == '')
		{
			if(!($db_mailvarification[0]['vBankAccountHolderName'] != '' && $db_mailvarification[0]['vAccountNumber'] != '' && $db_mailvarification[0]['vBankLocation'] != '' && $db_mailvarification[0]['vBankName'] != '' && $db_mailvarification[0]['vBIC_SWIFT_Code'] != ''))
			{
				header("Location:".$tconfig["tsite_url"]."index.php?file=m-edit_profile&msg_code=0&var_msg=".LBL_PAYPAL_ERR_OFFER_RIDE);
				exit;
			}
		}
	}
  if($LICENSE_VERIFICATION_REQUIRED == 'Yes'){
	  if($db_mailvarification[0]['eLicenseStatus'] == 'Pending' || $db_mailvarification[0]['eLicenseStatus'] == 'Unapproved'){
		header("Location:".$tconfig["tsite_url"]."index.php?file=m-verification&msg_code=0&var_msg=You did not complete your member verifications. Please verify your ID.");
		exit;
	  }
  }
  if($CARPAPER_VERIFICATION_REQUIRED == 'Yes'){
	  if($db_mailvarification[0]['eCarPaperStatus'] == 'Pending' || $db_mailvarification[0]['eCarPaperStatus'] == 'Unapproved'){
		header("Location:".$tconfig["tsite_url"]."index.php?file=m-verification&msg_code=0&var_msg=Your Car Paper is not verify. Please verified your Car Paper for offer.");
		exit;
	  }
  }
  if($PHONE_VERIFICATION_REQUIRED == 'Yes'){
    if($db_mailvarification[0]['ePhoneVerified'] == 'No'){
      header("Location:".$tconfig["tsite_url"]."index.php?file=m-verification&msg_code=0&var_msg=Your phone number is not verified.");
      exit;
    }
  }

  /* if user dont have car
	if($sess_iMemberId != '') {
		$sql = "SELECT * FROM member_car WHERE iMemberId = '".$sess_iMemberId."'";
		$db_car = $obj->MySQLSelect($sql);

		if(count($db_car) == 0) {
			header("Location:".$tconfig["tsite_url"]."index.php?file=m-car_form&msg_code=0&var_msg=".LBL_ADD_NEW_CAR);
			exit;
		}

	}*/


  if($_SESSION['dates'] == ''){
    $_SESSION['dates'] = array("dt" => "2000-01-01","type" => "noone");
  }
  if($_SESSION['ride_offer']['second_page'] == 'Yes'){
    $smarty->assign("triptype",$_SESSION['ride_offer']['triptype']);
    $smarty->assign("from",$_SESSION['ride_offer']['from']);
    $smarty->assign("to",$_SESSION['ride_offer']['to']);
    $smarty->assign("loc1",$_SESSION['ride_offer']['loc1']);
    $smarty->assign("loc2",$_SESSION['ride_offer']['loc2']);
    $smarty->assign("loc3",$_SESSION['ride_offer']['loc3']);
    $smarty->assign("loc4",$_SESSION['ride_offer']['loc4']);
    $smarty->assign("loc5",$_SESSION['ride_offer']['loc5']);
    $smarty->assign("loc6",$_SESSION['ride_offer']['loc6']);
    $smarty->assign("distance",$_SESSION['ride_offer']['distance']);
    $smarty->assign("duration",$_SESSION['ride_offer']['duration']);
    $smarty->assign("from_lat_long",$_SESSION['ride_offer']['from_lat_long']);
    $smarty->assign("to_lat_long",$_SESSION['ride_offer']['to_lat_long']);
    $smarty->assign("loc1_lat_long",$_SESSION['ride_offer']['loc1_lat_long']);
    $smarty->assign("loc2_lat_long",$_SESSION['ride_offer']['loc2_lat_long']);
    $smarty->assign("loc3_lat_long",$_SESSION['ride_offer']['loc3_lat_long']);
    $smarty->assign("loc4_lat_long",$_SESSION['ride_offer']['loc4_lat_long']);
    $smarty->assign("loc5_lat_long",$_SESSION['ride_offer']['loc5_lat_long']);
    $smarty->assign("loc6_lat_long",$_SESSION['ride_offer']['loc6_lat_long']);

    if($_SESSION['ride_offer']['triptype'] == 'onetime'){
      $smarty->assign("roundtriponetime",$_SESSION['ride_offer']['roundtriponetime']);
      $smarty->assign("sdateone",$_SESSION['ride_offer']['sdateone']);
      $smarty->assign("edateone",$_SESSION['ride_offer']['edateone']);
      $smarty->assign("onetihourstart",$_SESSION['ride_offer']['onetihourstart']);
      $smarty->assign("onetimeminstart",$_SESSION['ride_offer']['onetimeminstart']);
      $smarty->assign("onetihourend",$_SESSION['ride_offer']['onetihourend']);
      $smarty->assign("onetimeminend",$_SESSION['ride_offer']['onetimeminend']);
    }else{
      $smarty->assign("roundtripric",$_SESSION['ride_offer']['roundtripric']);
      $smarty->assign("sdate",$_SESSION['ride_offer']['sdate']);
      $smarty->assign("edate",$_SESSION['ride_offer']['edate']);
      $smarty->assign("richourstart",$_SESSION['ride_offer']['richourstart']);
      $smarty->assign("ricminstart",$_SESSION['ride_offer']['ricminstart']);
      $smarty->assign("richourend",$_SESSION['ride_offer']['richourend']);
      $smarty->assign("ricminend",$_SESSION['ride_offer']['ricminend']);

      if(count($_SESSION['ride_offer']['outbound']) > 0){
         $smarty->assign('dateshow','Yes');
         for($i=0;$i<count($_SESSION['ride_offer']['outbound']);$i++){
           $smarty->assign('out_'.$_SESSION['ride_offer']['outbound'][$i],'Yes');
         }

      }
      if(count($_SESSION['ride_offer']['retturn']) > 0){
         $smarty->assign('dateshow','Yes');
         for($i=0;$i<count($_SESSION['ride_offer']['retturn']);$i++){
           $smarty->assign('ret_'.$_SESSION['ride_offer']['retturn'][$i],'Yes');
         }
      }
    }
  }


  $smarty->assign("var_err_msg",$_REQUEST['var_err_msg']);
  $smarty->assign("startdatepicker",date("Y-m-d"));
  $smarty->assign("enddatepicker",date('Y-m-d', strtotime($Date. ' + 180 days')));
  $smarty->assign('go_to_second',$_SESSION['ride_offer']['go_to_second']);
  $_SESSION['ride_offer'] = '';
?>
