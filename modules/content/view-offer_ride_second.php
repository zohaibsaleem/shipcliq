<?php
	  include_once(TPATH_CLASS_APP . "class.customer.php");
$CustomerObj = new Customer;
$CustomerObj->check_member_login();
	$triptype = $_REQUEST['triptype'];
	foreach($_SESSION['ride_offer'] as $key => $val)
	{
		if($_REQUEST[$key] == "")
		$_REQUEST[$key] = $val;	
	}
	if(isset($_POST['from']))
	{
		$_SESSION['ride_offer'] = $_POST;
	}
	$_SESSION['ride_offer']['second_page'] = 'Yes';
	
	if($triptype != ''){
		if($triptype == 'onetime')
		{ 
			$Roundtrip = $_REQUEST['roundtriponetime']; 
			if($Roundtrip == 'Yes')
			{
				$_SESSION['ride_offer']['roundtriponetime'] = 'Yes';
			}
			else
			{
				$_SESSION['ride_offer']['roundtriponetime'] = 'No';
			}  
            
			#Server side validation start 
			include_once(TPATH_LIBRARIES."/general/validation.class.php");
			$validobj = new validation();
			
			$validobj->add_fields($_POST['from'], 'req', LBL_SELECT_DEPARTURE_POINT);
			$validobj->add_fields($_POST['to'], 'req', LBL_SELECT_ARRIVAL_POINT);
			/*### For blank lat long  ###*/
			$arr_loc = array("loc1","loc2","loc3","loc4","loc5","loc6");
			$str_loc = '';
			foreach($arr_loc as $val)
			{
				$str_loc .= (isset($_POST[$val]) && $_POST[$val] != '')?$_POST[$val.'_lat_long']."|":'';
			}
			if($_POST['from_lat_long'] == '' && $_POST['to_lat_long'] == ''){
				$_POST['from_lat_long'] = $_SESSION['ride_offer']['from_lat_long'];
				$_POST['from_lat_long'] = $_SESSION['ride_offer']['to_lat_long'];
			} 
			$validobj->add_fields($_POST['from_lat_long'].'|'.$str_loc.$_POST['to_lat_long'], 'latlongblank', LBL_SOMETHIG_WRONG_IN_LOC);
			/* ### */
			$validobj->add_fields($_POST['triptype'], 'req', LBL_SELECT_TRIP_TYPE);
			$validobj->add_fields($_POST['sdateone'], 'req', LBL_SELECT_DEPARTURE_DATE);
			$validobj->add_fields($_POST['onetihourstart'], 'req', LBL_SELECT_DEPARTURE_HOUR); 
			$validobj->add_fields($_POST['onetimeminstart'], 'req', LBL_SELECT_DEPARTURE_MINUTES);
			if($Roundtrip == 'Yes')
			{
				$validobj->add_fields($_POST['edateone'], 'req', LBL_SELECT_RETURN_DATE);
				$validobj->add_fields($_POST['onetihourend'], 'req', LBL_SELECT_RETURN_HOUR);
				$validobj->add_fields($_POST['onetimeminend'], 'req', LBL_SELECT_RETURN_MINUTES);
				
				$validobj->add_fields($_POST['sdateone'].'|'.$_POST['edateone'], 'daterange', LBL_RANGE_DATE);
				
				$validobj->add_fields($_POST['sdateone'].'|'.$_POST['edateone'].'|'.$_POST['onetihourstart'].':'.$_POST['onetimeminstart'].'|'.$_POST['onetihourend'].':'.$_POST['onetimeminend'], 'timerange', LBL_RANGE_TIME);
			}  
			else 
			{
				$validobj->add_fields($_POST['sdateone'].'|'.$_POST['edateone'].'|'.$_POST['onetihourstart'].':'.$_POST['onetimeminstart'].'|'.$_POST['onetihourend'].':'.$_POST['onetimeminend'], 'singledaterange', LBL_RANGE_TIME);
			} 
            
			$error = $validobj->validate();
			if($error)
			{
				$generalobj->getPostFormData($_POST,$error,$tconfig['tsite_url']."offer-ride");
			}       
		}
		else{
			$Roundtrip = $_REQUEST['roundtripric']; 
			if($Roundtrip == 'Yes')
			{
				$_SESSION['ride_offer']['roundtripric'] = 'Yes';
				}
			else
			{
				$_SESSION['ride_offer']['roundtripric'] = 'No';
			}
			#Server side validation start 
			include_once(TPATH_LIBRARIES."/general/validation.class.php");
			$validobj = new validation();
			$validobj->add_fields($_POST['from'], 'req', LBL_SELECT_DEPARTURE_POINT);
			$validobj->add_fields($_POST['to'], 'req', LBL_SELECT_ARRIVAL_POINT);
			/*### For blank lat long  ###*/
			$arr_loc = array("loc1","loc2","loc3","loc4","loc5","loc6");
			$str_loc = '';
			foreach($arr_loc as $val)
			{
				$str_loc .= (isset($_POST[$val]) && $_POST[$val] != '')?$_POST[$val.'_lat_long']."|":'';
			}
			if($_POST['from_lat_long'] == '' && $_POST['to_lat_long'] == ''){
				$_POST['from_lat_long'] = $_SESSION['ride_offer']['from_lat_long'];
				$_POST['from_lat_long'] = $_SESSION['ride_offer']['to_lat_long'];
			} 
			$validobj->add_fields($_POST['from_lat_long'].'|'.$str_loc.$_POST['to_lat_long'], 'latlongblank', LBL_SOMETHIG_WRONG_IN_LOC);
			/* ### */
			$validobj->add_fields($_POST['triptype'], 'req', LBL_SELECT_TRIP_TYPE);
			$validobj->add_fields($_POST['sdate'], 'req', LBL_SELECT_DEPARTURE_DATE);
			$validobj->add_fields($_POST['richourstart'], 'req', LBL_SELECT_DEPARTURE_HOUR); 
			$validobj->add_fields($_POST['ricminstart'], 'req', LBL_SELECT_DEPARTURE_MINUTES);
			
			if(count($_POST['outbound']) == 0)
			{
				$validobj->add_fields($_POST['outbound'], 'req', 'Select Outbound days');
			}
			
			if($Roundtrip == 'Yes')
			{
				$validobj->add_fields($_POST['edate'], 'req', LBL_SELECT_RETURN_DATE);
				$validobj->add_fields($_POST['richourend'], 'req', LBL_SELECT_RETURN_HOUR);
				$validobj->add_fields($_POST['ricminend'], 'req', LBL_SELECT_RETURN_MINUTES);
				if(count($_POST['retturn']) == 0)
				{
					$validobj->add_fields($_POST['retturn'], 'req', 'Select Return days');
				}
			}  
            
			$error = $validobj->validate();
			if($error)
			{
				$generalobj->getPostFormData($_POST,$error,$tconfig['tsite_url']."offer-ride");
			}
		}
	}
	
	$points_arr = array();
	$new_arr = array();
	$new_arr['addr'] = $_REQUEST['from'];
	$lat_long = explode(', ',substr($_REQUEST['from_lat_long'], 1, -1)); 
	$new_arr['lat'] = number_format($lat_long[0],6,'.','');
	$new_arr['lon'] = number_format($lat_long[1],6,'.','');
	array_push($points_arr, $new_arr); 
	for($i=1;$i<=6;$i++)
	{
		$new_arr = array();
		if($_REQUEST['loc'.$i] != '')
		{       
			$new_arr['addr'] = $_REQUEST['loc'.$i]; 
			$lat_long = explode(', ',substr($_REQUEST['loc'.$i.'_lat_long'], 1, -1)); 
			$new_arr['lat'] = number_format($lat_long[0],6,'.','');
			$new_arr['lon'] = number_format($lat_long[1],6,'.','');
			array_push($points_arr, $new_arr);
		} 
	}
	$new_arr = array();
	$new_arr['addr'] = $_REQUEST['to']; 
	$lat_long = explode(', ',substr($_REQUEST['to_lat_long'], 1, -1)); 
	$new_arr['lat'] = number_format($lat_long[0],6,'.','');
	$new_arr['lon'] = number_format($lat_long[1],6,'.','');
	array_push($points_arr, $new_arr);
	$loop = count($points_arr)-1;
	
	$counter = 1;
	$main_points_arr = array();
	for($i=0;$i<$loop;$i++)
	{
		$new_arr = array();
		
		$new_arr['addr'] = $points_arr[$i]['addr'].' &rarr; '.$points_arr[$counter]['addr'];
		$new_arr['addr_post'] = $points_arr[$i]['addr'].'|'.$points_arr[$counter]['addr'];
		$new_arr['start_lat'] = $points_arr[$i]['lat'].'|'.$points_arr[$i]['lon'];
		$new_arr['end_lat'] = $points_arr[$counter]['lat'].'|'.$points_arr[$counter]['lon'];
		
		array_push($main_points_arr, $new_arr);
		$counter++; 
	}
	#echo "<pre>";print_r($main_points_arr);exit;
	$dest = end($main_points_arr); 
	$destination_country = $dest['end_lat'];
	if($_REQUEST['loc'] == '' && $_REQUEST['loc1'] == '' && $_REQUEST['loc2'] == '' && $_REQUEST['loc3'] == '' && $_REQUEST['loc4'] == '' && $_REQUEST['loc5'] == '' && $_REQUEST['loc6'] == '')
	{
		$no_other_points = 'Yes';
	}
	
	$sql = "SELECT member_car.iMemberCarId, member_car.iSeats, make.vMake, model.vTitle 
	FROM member_car 
	LEFT JOIN make ON make.iMakeId = member_car.iMakeId 
	LEFT JOIN model ON model.iModelId = member_car.iModelId 
	WHERE iMemberId = '".$_SESSION['sess_iMemberId']."' 
	ORDER BY iMemberCarId ASC";
	$db_car = $obj->MySQLSelect($sql); 
	
	/*if($_SESSION['sess_price_ratio'] == 'GBP'){
		$Hundradrmilespice = $HUNDREDMILES_PRICE_GBP;
		}else if($_SESSION['sess_price_ratio'] == 'NOK'){
		$Hundradrmilespice = $HUNDREDMILES_PRICE_NOK;
		}else if($_SESSION['sess_price_ratio'] == 'SEK'){
		$Hundradrmilespice = $HUNDREDMILES_PRICE_SEK;
		}else if($_SESSION['sess_price_ratio'] == 'DKK'){
		$Hundradrmilespice = $HUNDREDMILES_PRICE_DKK;
		}else if($_SESSION['sess_price_ratio'] == 'PLN'){
		$Hundradrmilespice = $HUNDREDMILES_PRICE_PLN;
		}else if($_SESSION['sess_price_ratio'] == 'RUB'){
		$Hundradrmilespice = $HUNDREDMILES_PRICE_RUB;
		}else if($_SESSION['sess_price_ratio'] == 'EUR'){
		$Hundradrmilespice = $HUNDREDMILES_PRICE_EUR;
		}else if($_SESSION['sess_price_ratio'] == 'USD'){
		$Hundradrmilespice = $HUNDREDMILES_PRICE_USD;
	}*/
	$db_curr_mst = unserialize(db_curr_mst);
	#echo '<pre>'; print_R($db_curr_mst);  print_R($_SESSION); echo '</pre>';
	//echo $_SESSION['sess_price_ratio'];exit;
	$Hundradrmilespice = '';
	for($i=0;$i<count($db_curr_mst);$i++)
	{
		$sess_price_ratio = isset($_SESSION['sess_price_ratio'])?$_SESSION['sess_price_ratio']:CURR_MASTER_DEF_CURR;
		if($sess_price_ratio == $db_curr_mst[$i]['vName'])
		{
			$str = 'HUNDREDMILES_PRICE_'.$db_curr_mst[$i]['vName'];
			$Hundradrmilespice = $$str;  
			//$Hundradrmilespice = $HUNDREDMILES_PRICE_.$db_curr_mst[$i]['vName']; 
			break;
		}
	}
	
	$sql = "SELECT ".$_SESSION['sess_price_ratio']." as ratio_price FROM currency WHERE vName = 'EUR'";
	$db_currency = $obj->MySQLSelect($sql);
	
	$sql = "SELECT * FROM unit";
	$db_unit = $obj->MySQLSelect($sql);
	#echo "<pre>"; print_r($db_unit); exit;
	$DocUnit = explode(',', $db_unit[0]['vValue']);
	$BoxUnit = explode(',', $db_unit[1]['vValue']);
	$LuggageUnit = explode(',', $db_unit[2]['vValue']);
	#echo "<pre>"; print_R($LuggageUnit); exit;
	
	//$min_price_any_ride = round($db_currency[0]['ratio_price'] * 5.50);
	$min_price_any_ride = round($db_currency[0]['ratio_price'] * $MINIMUM_RIDE_PRICE);   
	$smarty->assign("min_price_any_ride",$min_price_any_ride);    
	
	$smarty->assign("from",$_SESSION['ride_offer']['from']);
	$smarty->assign("to",$_SESSION['ride_offer']['to']);
	$smarty->assign("loc1",$_SESSION['ride_offer']['loc1']);
	$smarty->assign("loc2",$_SESSION['ride_offer']['loc2']);
	$smarty->assign("loc3",$_SESSION['ride_offer']['loc3']);
	$smarty->assign("loc4",$_SESSION['ride_offer']['loc4']);
	$smarty->assign("loc5",$_SESSION['ride_offer']['loc5']);
	$smarty->assign("loc6",$_SESSION['ride_offer']['loc6']);
	$smarty->assign("distance",$_SESSION['ride_offer']['distance']);
	$smarty->assign("duration",$_SESSION['ride_offer']['duration']);     
	$smarty->assign("main_points_arr",$main_points_arr);   
	$smarty->assign("Price_per_passenger",$Price_per_passenger);
	$smarty->assign("no_other_points",$no_other_points);  
	$smarty->assign("db_car",$db_car); 
	$smarty->assign("Hundradrmilespice",$Hundradrmilespice);
	$smarty->assign("destination_country",$destination_country);  
	$smarty->assign("db_unit",$db_unit);  
	$smarty->assign("DocUnit",$DocUnit);  
	$smarty->assign("BoxUnit",$BoxUnit);  
	$smarty->assign("LuggageUnit",$LuggageUnit);  
	
	#echo '<pre>'; print_R($_SESSION); echo '</pre>';
?>