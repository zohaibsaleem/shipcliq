<?php    
  $iOrganizationId = $_REQUEST['iOrganizationId'];

  if($iOrganizationId == ''){
    header("Location:".$tconfig["tsite_url"]); exit;    
  }

  $sql = "SELECT * from organization WHERE eStatus = 'Active' AND iOrganizationId = '".$iOrganizationId."'";
  $db_organization = $obj->MySQLSelect($sql);
 
  if(is_file($tconfig["tsite_upload_images_organization_path"].$iOrganizationId."/1_".$db_organization[0]['vImage'])){
	  $db_organization[0]['small_url'] = $tconfig["tsite_upload_images_organization"].$iOrganizationId."/1_".$db_organization[0]['vImage'];
	}
  else{
	  $db_organization[0]['small_url'] = $tconfig['tsite_images'].'237x257.jpg';
	} 
  
  $sql = "SELECT vCategory FROM organization_interests LEFT JOIN category ON category.iCategoryId = organization_interests.iCategoryId WHERE organization_interests.iOrganizationId = '".$iOrganizationId."'";
  $db_organization_cat = $obj->MySQLSelect($sql);
  
  $db_organization_cat_str = '';
  if(count($db_organization_cat)>0){
    for($k=0;$k<count($db_organization_cat);$k++){
       if($k == count($db_organization_cat)-1){
         $db_organization_cat_str .= $db_organization_cat[$k]['vCategory'];
       }else{
         $db_organization_cat_str .= $db_organization_cat[$k]['vCategory'].', ';
       } 
    }
  }
 
/*-----------------for events---------------------*/   
  $sql = "SELECT * FROM event WHERE eStatus = 'Active' AND iOrganizationId = '".$iOrganizationId."' ORDER BY iEventId DESC";
  $db_events = $obj->MySQLSelect($sql);
  
  if(count($db_events) > 0){
    for($k=0;$k<count($db_events);$k++)
    {
    	if(is_file($tconfig["tsite_upload_images_events_path"].$db_events[$k]['iEventId']."/4_".$db_events[$k]['vImage']))
     {
       $db_events[$k]['thumb_url'] = $tconfig['tsite_upload_images_events'].$db_events[$k]['iEventId']."/4_".$db_events[$k]['vImage'];
     }
     else
     {
       $db_events[$k]['thumb_url'] = $tconfig['tsite_images'].'129x103.jpg';
     }
     
     $db_events[$k]['start_time'] = strtotime($db_events[$k]['dStartDate']);
    }  
  }    
/*--------------------------Reviews---------------------*/

  $sql = "SELECT count(organization_reviews.iOrganizationReview) as tot_reviews 
          FROM organization_reviews 
          LEFT JOIN member ON member.iMemberId = organization_reviews.iMemberId 
          WHERE organization_reviews.eStatus = 'Active' AND organization_reviews.iOrganizationId = ".$iOrganizationId; 
  $db_organization_reviews = $obj->MySQLSelect($sql);
 
 
  $smarty->assign("db_organization_cat_str", $db_organization_cat_str); 
  $smarty->assign("db_organization", $db_organization); 
  $smarty->assign("iOrganizationId", $iOrganizationId); 
  $smarty->assign("db_events", $db_events); 
  $smarty->assign("db_interests", $db_interests); 
  $smarty->assign("reviews_total", $db_organization_reviews[0]['tot_reviews']);
  $smarty->assign("iEventId", $iEventId);
  $smarty->assign("created_tot", $created_tot); 
  $smarty->assign("comments_tot", $comments_tot); 
  $smarty->assign("backed_tot", $backed_tot);     
  $smarty->assign("current", strtotime("Y-m-d H:i:s"));         
?>