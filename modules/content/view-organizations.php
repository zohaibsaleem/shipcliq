<?php 
  if($_REQUEST['srchkeyword1'] != ''){
    $ssql .= " AND (organization.vCity LIKE '%".stripslashes($_REQUEST['srchkeyword1'])."%')";
  }
  
  if($_REQUEST['srchkeyword2'] != ''){
    $ssql .= " AND (organization.tDescription LIKE '%".stripslashes($_REQUEST['srchkeyword2'])."%' OR organization.vOrganizationName LIKE '%".stripslashes($_REQUEST['srchkeyword2'])."%' OR category.vCategory LIKE '%".stripslashes($_REQUEST['srchkeyword2'])."%')";
  }
  
  if($_REQUEST['ordby'] == ''){
    $ordby = " ORDER BY organization.iOrganizationId DESC";
  }else{
    if($_REQUEST['ordby'] == 'nameasc'){
      $ordby = " ORDER BY organization.vOrganizationName ASC";
    }else if($_REQUEST['ordby'] == 'namedesc'){
      $ordby = " ORDER BY organization.vOrganizationName DESC";
    }else if($_REQUEST['ordby'] == 'rateasc'){
      $ordby = " ORDER BY organization.iRate ASC";
    }else{
      $ordby = " ORDER BY organization.iRate DESC";
    }
  }
  
  $sorton 	= $_GET['sorton'];
	$stat  	 	= $_GET['stat'];
	$nstart   = $_GET['nstart'];
	$start   	= $_GET['start']; 	
	
  $organizations_all = $organizationsobj->all_organizations($ssql, $ordby);     
	$num_totrec = count($organizations_all);
	$rec_limit = $ACC_REC_LIMIT;
  include(TPATH_LIBRARIES."/general/front.paging.inc.php");
	if(!isset($start))
		$start = 1;
	$num_limit = ($start-1)*$rec_limit;
	$startrec = $num_limit;
	$lastrec = $startrec + $rec_limit;
	$startrec = $startrec + 1;
	if($lastrec > $num_totrec)
		$lastrec = $num_totrec;
	if($num_totrec > 0 )
	{
		$recmsg = "Showing ".$startrec." - ".$lastrec." Organization(s) Of ".$num_totrec;
	}
       
  $organizations = $organizationsobj->organization_list($ssql, $ordby, $var_limit);
  $cities = $organizationsobj->organization_city();   
  
  $smarty->assign("organizations",$organizations);   
  $smarty->assign("recmsg",$recmsg);
  $smarty->assign("page_link",$page_link);
  $smarty->assign("srchkeyword1",$_REQUEST['srchkeyword1']);  
  $smarty->assign("srchkeyword2",$_REQUEST['srchkeyword2']);    
  $smarty->assign("ordby",$_REQUEST['ordby']); 
  $smarty->assign("cities",$cities); 
?>