<?php
   $mode = $_REQUEST['mode'];
   if($mode == 'register')
   {
	  // Captcha image verification checl
	  if($_SESSION['security_code'] != $_REQUEST['capthca']){
      $msg = LBL_CODE_NOT_MATCH;
      $generalobj->getPostForm($_POST,$msg,"index.php?file=c-registration");
      exit;
    }
    else
    {
	  // User duplocation check on email field

      $generalobj->checkDuplicateFront('vEmail', "member" , Array('vEmail'),$tconfig["tsite_url"]."index.php?file=c-registration", LBL_EMAIL_ENTERED_ALLREAD_EXIST, "");
      $generalobj->checkDuplicateFront('vPhone', "member" , Array('vPhone'),$tconfig["tsite_url"]."index.php?file=c-registration", LBL_PHONE_ENTERED_ALLREAD_EXIST, "");
      $Data = $_POST["Data"];
      $Data['dAddedDate'] = date("Y-m-d H:i:s");
      $Data['eStatus'] = 'Pending';
      $Data['vLanguageCode'] = $_REQUEST['vLanguageCode'];
      $Data['vNickName'] = $_REQUEST['vNickName'];
      $Data['vFirstName'] = $_REQUEST['vFirstName'];
      $Data['vLastName'] = $_REQUEST['vLastName'];
      $Data['vEmail'] = $_REQUEST['vEmail'];
      $Data['vPhone'] = $_REQUEST['vPhone'];
      $Data['vPaymentEmail'] = $_REQUEST['vPaymentEmail'];
      $Data['ePaymentEmailVerified'] = 'No';
	  $Data['vCountry'] = $_REQUEST['vCountry'];
	  $Data['vState'] = $_REQUEST['vState'];
      $Data['vPassword'] = $generalobj->encrypt($_REQUEST['vPassword']);
      //$Data['eStatus'] = 'Active';
      $Data['vState'] = $_POST['vState'];
      // Add user in database
	  $id = $obj->MySQLQueryPerform("member",$Data,'insert');

      if($id){
         $sql = "SELECT * FROM preferences WHERE eStatus = 'Active'";
         $db_preferences = $obj->MySQLSelect($sql);

         for($i=0;$i<count($db_preferences);$i++){
          $prefid = $db_preferences[$i]['iPreferencesId'];

          $Data_pref['vType'] = 'vNO';
          $Data_pref['iMemberId'] = $id;
          $Data_pref['iPreferencesId'] = $prefid;
          // Add data in member car preferences
		  $id_pref = $obj->MySQLQueryPerform("member_car_preferences",$Data_pref,'insert');
         }

         $Data_not['iMemberId'] = $id;
         $Data_not['eSuccessPublish'] = 'Yes';
         $Data_not['eSuccessUpdate'] = 'Yes';
         $Data_not['ePrivateMessage'] = 'Yes';
         $Data_not['eRatePassenger'] = 'Yes';
         $Data_not['eNewRating'] = 'Yes';
         $Data_not['eOtherInformation'] = 'Yes';
         $Data_not['dAddedDate'] = date("Y-m-d H:i:s");
		 // Add data in email notification
         $id_not = $obj->MySQLQueryPerform("member_email_notification",$Data_not,'insert');


        $sql = "SELECT iMemberId, vFirstName, vLastName, vEmail, eGender FROM member WHERE iMemberId = '".$id."'";
        $db_login = $obj->MySQLSelect($sql);

        $_SESSION['sess_iMemberId']=$db_login[0]['iMemberId'];
        $_SESSION["sess_vFirstName"]=$db_login[0]['vFirstName'];
        $_SESSION["sess_vLastName"]=$db_login[0]['vLastName'];
        $_SESSION["sess_vEmail"]=$db_login[0]['vEmail'];
        $_SESSION["sess_eGender"]=$db_login[0]['eGender'];

        $maildata['EMAIL'] = $_SESSION["sess_vEmail"];
        $maildata['NAME'] = $_SESSION["sess_vFirstName"];
        $maildata['PASSWORD'] = $_REQUEST['vPassword'];
        // Send registration email to user
        $generalobj->send_email_user("MEMBER_REGISTRATION_USER",$maildata);

		//Paypal Email Address confirmation link (Hemali)
		/*mail sending code start */
		$dt = date("Y-m-d H:i:s");
		$random = substr(number_format(time() * rand(),0,'',''),0,20);
		$Datas['vPaymentEmailVerificationCode'] = $random.strtotime($dt);

		$where = " iMemberId = '".$_SESSION['sess_iMemberId']."'";
		$res = $obj->MySQLQueryPerform("member",$Datas,'update',$where);

		if($res)
		{
			$Data_Mail['vEmail'] = $_SESSION["sess_vEmail"];
			$Data_Mail['vName'] = $_REQUEST['vFirstName'];
			$Data_Mail['act_link'] = $tconfig['tsite_url']."index.php?file=c-verifymail&act=".$Datas['vPaymentEmailVerificationCode']."&pay=1";
			$generalobj->send_email_user("EMAIL_VERIFICATION_USER",$Data_Mail);
		}
		/*mail sending code end */

		

        header("Location:".$tconfig["tsite_url"]."my-account");
        exit;
      }
    }
   }
?>
