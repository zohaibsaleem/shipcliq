<?php
  function get_months($date1, $date2) {
    //convert dates to UNIX timestamp
    $time1  = strtotime($date1);
    $time2  = strtotime($date2);
    $tmp     = date('mY', $time2);
    
    $monthtime1 = date('m', $time1); 
    $monthtime2 = date('m', $time2); 
    
    if($monthtime1 != $monthtime2){
      $months[] = array("month"    => date('n', $time1), "year"    => date('Y', $time1));
    
      while($time1 < $time2) {
        $time1 = strtotime(date('Y-m-d', $time1).' +1 month');
        if(date('mY', $time1) != $tmp && ($time1 < $time2)) {
           $months[] = array("month"    => date('n', $time1), "year"    => date('Y', $time1));
        }
      }
      $months[] = array("month"    => date('n', $time2), "year"    => date('Y', $time2));
      return $months; //returns array of month names with year
    }else{
      $months[] = array("month"    => date('n', $time1), "year"    => date('Y', $time1));
      return $months; //returns array of month names with year
    }
  }
  
  function whatever($array, $key, $key1, $val, $type){
    if(count($array) > 0){       
      foreach ($array as $item){
        if (isset($item[$key]) && $item[$key] == $val){
         return $class = $item[$key1];
        }            
      }
      return $class = $type;
    }else{
     return $class = $type;
    }
  }
  
  $list  =  get_months($_POST['sdate'], $_POST['edate']);     
  
  $monthNames = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
  
  $str_month .= '<div style="width:100%;">';
?>  

<? for($r=0;$r<count($list);$r++){ ?>

<? 
  $cMonth = $list[$r]['month'];
  $cYear = $list[$r]['year'];
  
  $str_month .= '<table width="100%" style="border:1px solid #cccccc;background: none repeat scroll 0 0 #fff;margin-bottom:10px;">    
                <tr>
                <td align="center">
                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                <tr align="center">
                <td colspan="7" class="wday">'.$monthNames[$cMonth-1].' '.$cYear.'</td>
                </tr>
                <tr>
                <td align="center" class="wday">S</td>
                <td align="center" class="wday">M</td>
                <td align="center" class="wday">T</td>
                <td align="center" class="wday">W</td>
                <td align="center" class="wday">T</td>
                <td align="center" class="wday">F</td>
                <td align="center" class="wday">S</td>
                </tr>';
                
                $timestamp = mktime(0,0,0,$cMonth,1,$cYear);
                $maxday = date("t",$timestamp);
                $thismonth = getdate ($timestamp);
                $startday = $thismonth['wday'];
                for ($i=0; $i<($maxday+$startday); $i++) {
                    if(($i % 7) == 0 ) $str_month .= '<tr>';
                    if($i < $startday){ 
                      $str_month .= '<td></td>';
                    }else{ 
                      $day = $i - $startday + 1;
                      $rbdate = $cYear.'-'.$cMonth.'-'.$day;
                      
                      $rbdate = date('l', strtotime($rbdate));
                      
                      $day_date = date('Y-m-d',strtotime($cYear."-".$cMonth."-".($i - $startday + 1)));
                      $selected_date = "'".$day_date."'";
                      $class_date = $day_date;
                      
                      if(strtotime($cYear."-".$cMonth."-".($i - $startday + 1)) >= strtotime($_POST['sdate']) && strtotime($cYear."-".$cMonth."-".($i - $startday + 1)) <= strtotime($_POST['edate'])){
                        if(in_array($rbdate, $_POST['outbound']) && in_array($rbdate, $_POST['retturn'])){                       
                         $class_ype = whatever($_SESSION['dates'], 'dt', 'type', $class_date, 'both');
                         $type = "'".$class_ype."'";                         
                         $str_month .= '<td align="center" valign="middle" height="20px" class="'.$class_ype.'" onClick="select_date('.$type.', '.$selected_date.');">'. ($i - $startday + 1) . '</td>';
                        }else if(in_array($rbdate, $_POST['outbound'])){                          
                         $class_ype = whatever($_SESSION['dates'], 'dt', 'type', $class_date, 'out');
                         $type = "'".$class_ype."'";  
                         $str_month .= '<td align="center" valign="middle" height="20px" class="'.$class_ype.'" onClick="select_date('.$type.', '.$selected_date.');">'. ($i - $startday + 1) . '</td>';
                        }else if(in_array($rbdate, $_POST['retturn'])){                          
                         $class_ype = whatever($_SESSION['dates'], 'dt', 'type', $class_date, 'ret');
                         $type = "'".$class_ype."'";                        
                         $str_month .= '<td align="center" valign="middle" height="20px" class="'.$class_ype.'" onClick="select_date('.$type.', '.$selected_date.');">'. ($i - $startday + 1) . '</td>';
                        }else{
                         $class_ype = whatever($_SESSION['dates'], 'dt', 'type', $class_date, 'noone');
                         $type = "'".$class_ype."'";                          
                         $str_month .= '<td align="center" valign="middle" height="20px" class="'.$class_ype.'" onClick="select_date('.$type.', '.$selected_date.');">'. ($i - $startday + 1) . '</td>';
                        }
                      }else{
                        $str_month .= '<td align="center" valign="middle" height="20px" class="exp">'. ($i - $startday + 1) . '</td>';
                      }          
                    }
                    if(($i % 7) == 6 ) $str_month .= '</tr>';
                }
                
                $str_month .= '</table>
                </td>
                </tr>
                </table>';
?>

<? } 
  $str_month .= '</div>';
?>
<? 
  //$_SESSION['dates'] = '';
  echo $str_month;
?>
<? exit; ?>