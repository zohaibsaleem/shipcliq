<?php
	function get_prefrence_img($id, $type){
		global $obj, $tconfig;

		$sql = "SELECT iPreferencesId, vTitle".$_SESSION['sess_lang_pref']." as vTitle, ".$type."_Lable".$_SESSION['sess_lang_pref']." as Lable, ".$type." FROM preferences WHERE iPreferencesId = '".$id."' ORDER BY iDispOrder ASC";
		$db_preferences = $obj->MySQLSelect($sql);

		if(file_exists($tconfig["tsite_upload_images_preferences_path"].'/'.$db_preferences[0][$type])){
			$img_url = $tconfig["tsite_upload_images_preferences"].$db_preferences[0][$type];
			$img_url = '<img alt="" src="'.$tconfig["tsite_upload_images_preferences"].$db_preferences[0][$type].'" title="'.$db_preferences[0]['Lable'].'">&nbsp;';

			}else{
			$img_url = '';
		}

		return $img_url;
	}

	if($_REQUEST['from'] == 'home'){
		$id = $_REQUEST['id'];
		$iRideId = $id;

		$sql = "SELECT * FROM rides_new WHERE iRideId = '".$iRideId."'";
		$db_ride_details = $obj->MySQLSelect($sql);

		$newarrsess['iRideId'] = $iRideId;
		if($_REQUEST['ret'] == 'No'){
			$newarrsess['mainaddress'] = $db_ride_details[0]['vMainDeparture'].' &rarr; '.$db_ride_details[0]['vMainArrival'];
			if($db_ride_details[0]['eRideType'] == 'Reccuring'){
				$newarrsess['time'] = $db_ride_details[0]['vMainOutBoundTime'];
				}else{
				$newarrsess['time'] = $db_ride_details[0]['vMainDepartureTime'];
			}

			$sql = "SELECT iRidePointId, fPrice, fTotPrice FROM ride_points_new WHERE iRideId = '".$iRideId."' AND eReverse = 'No' ORDER BY iRidePointId ASC";
			$db_ride_point_details = $obj->MySQLSelect($sql);

			$totpoint = count($db_ride_point_details)-1;

			$newarrsess['start_iRidePointId'] = $db_ride_point_details[0]['iRidePointId'];
			$newarrsess['end_iRidePointId'] = $db_ride_point_details[$totpoint]['iRidePointId'];
			}else{
			$newarrsess['mainaddress'] = $db_ride_details[0]['vMainArrival'].' &rarr; '.$db_ride_details[0]['vMainDeparture'];
			if($db_ride_details[0]['eRideType'] == 'Reccuring'){
				$newarrsess['time'] = $db_ride_details[0]['vMainReturnTime'];
				}else{
				$newarrsess['time'] = $db_ride_details[0]['vMainArrivalTime'];
			}

			$sql = "SELECT iRidePointId, fPrice, fTotPrice FROM ride_points_new WHERE iRideId = '".$iRideId."' AND eReverse = 'Yes' ORDER BY iRidePointId ASC";
			$db_ride_point_details = $obj->MySQLSelect($sql);

			$totpoint = count($db_ride_point_details)-1;

			$newarrsess['start_iRidePointId'] = $db_ride_point_details[0]['iRidePointId'];
			$newarrsess['end_iRidePointId'] = $db_ride_point_details[$totpoint]['iRidePointId'];
		}
		$newarrsess['return'] = $_REQUEST['ret'];
		$newarrsess['date'] = date('Y-m-d',$_REQUEST['dt']);
		$newarrsess['strtotime'] = $_REQUEST['dt'];

		$price = 0;
		for($r=0;$r<count($db_ride_point_details);$r++){
			$price = $price + $db_ride_point_details[$r]['fPrice'];
		}

		$price = $price * $db_ride_details[0]['fRatio_'.$_SESSION['sess_price_ratio']];
		$newarrsess['price'] = $generalobj->booking_currency($price, $_SESSION['sess_price_ratio']);

		$sql = "SELECT iRidePointId, iNoOfSeats FROM booking_new WHERE dBookingDate = '".$newarrsess['date']."' AND iRideId = '".$newarrsess['iRideId']."' AND eTripReturn = '".$newarrsess['return']."' AND eStatus != 'Cencelled'";
		$db_booking = $obj->MySQLSelect($sql);

		$pointrange = range($newarrsess['start_iRidePointId'], $newarrsess['end_iRidePointId']);

		$seats = $db_ride_details[0]['iSeats'];
		for($r=0;$r<count($db_booking);$r++){
			$bookingpoints = explode(",", $db_booking[$r]['iRidePointId']);

			$conf = array_intersect($bookingpoints, $pointrange);
			if (count($conf) > 0){
				$seats = $seats - $db_booking[$r]['iNoOfSeats'];
			}
		}
		$newarrsess['seats'] = $seats;
		$mainsessionarr = array();
		array_push($mainsessionarr,$newarrsess);
		$newarrsess = array();
		$_SESSION['searcharr'] = serialize($mainsessionarr);
		$id = 0;
		}else{
		$id = $_REQUEST['id'];
	}

	$main_arr = unserialize($_SESSION['searcharr']);

	#echo "<pre>"; print_r($main_arr); exit;

	$tot_res = count($main_arr) - 1;
	if($id == 0){
		$previous = '';
		}else{
		$previous = $id - 1;
	}

	if($id < $tot_res)
    $next = $id + 1;
	else
    $next = '';

	$iRideId = $main_arr[$id]['iRideId'];
	$start_iRidePointId = $main_arr[$id]['start_iRidePointId'];

	$end_iRidePointId = $main_arr[$id]['end_iRidePointId'];
	$return = $main_arr[$id]['return'];
	$date = $main_arr[$id]['date'];
	$strtotime = $main_arr[$id]['strtotime'];
	$main_address = strip_tags($main_arr[$id]['mainaddress']);
	$time = $main_arr[$id]['time'];
	$price = $main_arr[$id]['price'];
	$seats = $main_arr[$id]['seats'];

	$sql = "SELECT * FROM rides_new WHERE iRideId = '".$iRideId."'";
	$db_ride = $obj->MySQLSelect($sql);
	$Remainload = $generalobj->parcel_count($iRideId);
	$Remaindoc = $Remainload['vDoc'];
	$Remainbox = $Remainload['vBox'];
	$Remainluggage = $Remainload['vLuggage'];
	//echo "<pre>";
	//print_r($db_ride); exit;
	######### For Document Luggage Box start ###########
	$sess_price_ratio = isset($_SESSION['sess_price_ratio'])?$_SESSION['sess_price_ratio']:CURR_MASTER_DEF_CURR;
	if($db_ride[0]['eDocument'] == "Yes" && $db_ride[0]['vDocumentWeight'] != 0)
	{
		$doc = "Yes";
		$docprice1 = $db_ride[0]['fDocumentPrice'] * $db_ride[0]['fRatio_'.$sess_price_ratio];
		$docprice = $generalobj->booking_currency($docprice1, $sess_price_ratio);
		$docweight = $db_ride[0]['vDocumentWeight'];
		$docunit = $db_ride[0]['vDocumentUnit'];
	}
	else if($db_ride[0]['eDocument'] == "No")
	{
		$doc = "No";
	}

	if($db_ride[0]['eBox'] == "Yes" && $db_ride[0]['vBoxWeight']!=0)
	{
		$box = "Yes";
		$boxprice1 = $db_ride[0]['fBoxPrice'] * $db_ride[0]['fRatio_'.$sess_price_ratio];
		$boxprice = $generalobj->booking_currency($boxprice1, $sess_price_ratio);
		$boxweight = $db_ride[0]['vBoxWeight'];
		$boxunit = $db_ride[0]['vBoxUnit'];
	}
	else if($db_ride[0]['eBox'] == "No")
	{
		$box = "No";
	}

	if($db_ride[0]['eLuggage'] == "Yes" && $db_ride[0]['vLuggageWeight']!=0)
	{
		$lug = "Yes";
		$lugprice1 = $db_ride[0]['fLuggagePrice'] * $db_ride[0]['fRatio_'.$sess_price_ratio];
		$lugprice = $generalobj->booking_currency($lugprice1, $sess_price_ratio);
		$lugweight = $db_ride[0]['vLuggageWeight'];
		$lugunit = $db_ride[0]['vLuggageUnit'];
	}
	else if($db_ride[0]['eLuggage'] == "No")
	{
		$lug = "No";
	}
	######### For Document Luggage Box end ###########

	if($db_ride[0]['eLeaveTime'] == 'ON_TIME'){
		$db_ride[0]['eLeaveTime'] = LBL_RIGHT_ON_TIME;
		}else if($db_ride[0]['eLeaveTime'] == 'FIFTEEN_MINUTES'){
		$db_ride[0]['eLeaveTime'] = LBL_15_MNT_WINDOW;
		}else if($db_ride[0]['eLeaveTime'] == 'THIRTY_MINUTES'){
		$db_ride[0]['eLeaveTime'] = LBL_30_MNT_WINDOW;
		}else if($db_ride[0]['eLeaveTime'] == 'ONE_HOUR'){
		$db_ride[0]['eLeaveTime'] = LBL_1_HOUR_WINDOW;
		}else if($db_ride[0]['eLeaveTime'] == 'TWO_HOURS'){
		$db_ride[0]['eLeaveTime'] = LBL_2_HOUR_WINDOW;
	}

	if($db_ride[0]['eWaitTime'] == 'FIFTEEN_MINUTES'){
		$db_ride[0]['eWaitTime'] = LBL_15_MNT_DETOUR_MAX;
		}else if($db_ride[0]['eWaitTime'] == 'THIRTY_MINUTES'){
		$db_ride[0]['eWaitTime'] = LBL_30_MNT_DETOUR_MAX;
		}else if($db_ride[0]['eWaitTime'] == 'WHATEVER_IT_TAKES'){
		$db_ride[0]['eWaitTime'] = LBL_ANYTHING_IS_FINE;
		}else if($db_ride[0]['eWaitTime'] == 'NONE'){
		$db_ride[0]['eWaitTime'] = LBL_NONE;
	}

	if($db_ride[0]['eLuggageSize'] == 'Small'){
		$db_ride[0]['eLuggageSize'] = LBL_SMALL;
		}else if($db_ride[0]['eLuggageSize'] == 'Medium'){
		$db_ride[0]['eLuggageSize'] = LBL_MEDIUM;
		}else if($db_ride[0]['eLuggageSize'] == 'Big'){
		$db_ride[0]['eLuggageSize'] = LBL_BIG;
	}


	$sql = "SELECT * FROM ride_points_new WHERE iRideId = '".$iRideId."' AND eReverse = '".$return."' ORDER BY iRidePointId ASC";
	$db_point_data = $obj->MySQLSelect($sql);

	//echo "<pre>";
	//print_r($db_point_data); exit;

	$rb = range($start_iRidePointId, $end_iRidePointId);

	$places_str = $main_address;
	$counter = 1;
	$price = 0;
	$originalprice = 0;
	for($j=0;$j<count($db_point_data);$j++){
		if($db_point_data[$j]['iRidePointId'] == $start_iRidePointId){
			$startpoint = $db_point_data[$j]['vStartPoint'];
		}
		if($db_point_data[$j]['iRidePointId'] == $end_iRidePointId){
			$endpoint = $db_point_data[$j]['vEndPoint'];
		}

		if($j != 0){
			$smarty->assign("loc".$counter,$db_point_data[$j]['vStartPoint']);
		}

		if (in_array($db_point_data[$j]['iRidePointId'], $rb)){
			$price = $price + $db_point_data[$j]['fPrice'];
			$originalprice = $originalprice + $db_point_data[$j]['fOriginalPrice'];
		}

		$counter++;
	}

	$price = $price * $db_ride[0]['fRatio_'.$_SESSION['sess_price_ratio']];

	$originalprice = $originalprice * $db_ride[0]['fRatio_'.$_SESSION['sess_price_ratio']];
	$price_diff = $price - $originalprice;
	$price_diff = abs($price_diff);

	if($price_diff > 2 && $price > $originalprice){
		$price_color = '#FF9802';
		}else if($price_diff > 2 && $price < $originalprice){
		$price_color = '#68BE57';
		}else{
		$price_color = '#000000';
	}

	$price = $generalobj->booking_currency($price, $_SESSION['sess_price_ratio']);

	$stack = array();

	$sql = "SELECT vFirstName, vLastName, vNickName, vImage, eGender, iBirthYear, eCarPaperStatus,eLicenseStatus, ePhoneVerified, dAddedDate, vFbFriendCount ,tLastLogin
	FROM member
	WHERE iMemberId = '".$db_ride[0]['iMemberId']."'";
	$db_member = $obj->MySQLSelect($sql);

	$sql = "SELECT member_rating.iRateId, member_rating.iMemberFromId, member_rating.iRate, member_rating.tFeedback , member_rating.dAddedDate, member_rating.eSkill, member.vFirstName, member.vLastName, member.vImage, member.eGender
	FROM member_rating
	LEFT JOIN member ON member.iMemberId = member_rating.iMemberFromId
	WHERE member_rating.iMemberToId = '".$db_ride[0]['iMemberId']."'";
	$db_ratings = $obj->MySQLSelect($sql);

	if(count($db_ratings) > 0){
		for($i=0;$i<count($db_ratings);$i++){
			$rating_width = ($db_ratings[$i]['iRate'] * 100) / 5;
			$db_ratings[$i]['rating'] = '<span style="float: right;display: block; width: 65px; height: 13px; background: url('.$tconfig['tsite_images'].'star-rating-sprite.png) 0 0;">
			<span style="margin: 0;display: block; width: '.$rating_width.'%; height: 13px; background: url('.$tconfig['tsite_images'].'star-rating-sprite.png) 0 -13px;"></span>
			</span>';
			if(is_file($tconfig["tsite_upload_images_member_path"].$db_ratings[$i]['iMemberFromId']."/1_".$db_ratings[$i]['vImage']))
			{
				$db_ratings[$i]['image'] = $tconfig["tsite_upload_images_member"].$db_ratings[$i]['iMemberFromId']."/1_".$db_ratings[$i]['vImage'];
			}
			else
			{
				if($db_ratings[$i]['eGender'] == 'Male'){
					$db_ratings[$i]['image'] = $tconfig['tsite_images'].'64x64male.png';
					}else{
					$db_ratings[$i]['image'] = $tconfig['tsite_images'].'64x64female.png';
				}
			}
		}
	}

	//echo "<pre>";
	//print_r($db_ratings); exit;

	$stack[0]['iMemberId'] = $db_ride[0]['iMemberId'];
	$stack[0]['eCarPaperStatus']=$db_member[0]['eCarPaperStatus'];
	$stack[0]['eLicenseStatus']=$db_member[0]['eLicenseStatus'];
	$stack[0]['tLastLogin']=$db_member[0]['tLastLogin'];
	if($db_member[0]['vNickName'] != ""){
		$stack[0]['drivername'] = $db_member[0]['vNickName'];
		}else{
		$stack[0]['drivername'] = $db_member[0]['vFirstName'].' '.$db_member[0]['vLastName'];
	}
	$stack[0]['age'] = date('Y') - $db_member[0]['iBirthYear'];
	$stack[0]['eEmailVarified'] = $db_member[0]['eEmailVarified'];
	$stack[0]['ePhoneVerified'] = $db_member[0]['ePhoneVerified'];
	//$stack[0]['JoinDate'] = $generalobj->DateTime($db_member[0]['dAddedDate'],9);
	$stack[0]['JoinDate'] = $generalobj->DateTimeFormat($db_member[0]['dAddedDate']);
	if($db_member[0]['vFbFriendCount']>0)
	{
		$stack[0]['vFbFriendCount'] = $db_member[0]['vFbFriendCount'];
	}
	else{
		$stack[0]['vFbFriendCount']="0";
	}
	if(is_file($tconfig["tsite_upload_images_member_path"].$db_ride[0]['iMemberId']."/1_".$db_member[0]['vImage']))
	{
		$stack[0]['image'] = $tconfig["tsite_upload_images_member"].$db_ride[0]['iMemberId']."/1_".$db_member[0]['vImage'];
	}
	else
	{
		if($db_member[0]['eGender'] == 'Male'){
			$stack[0]['image'] = $tconfig['tsite_images'].'64x64male.png';
			}else{
			$stack[0]['image'] = $tconfig['tsite_images'].'64x64female.png';
		}
	}

	$sql = "SELECT * FROM member_car_preferences WHERE iMemberId = '".$db_ride[0]['iMemberId']."'";
	$db_pref = $obj->MySQLSelect($sql);

	$pref = '';
    for($op=0;$op<count($db_pref);$op++){
		$img = get_prefrence_img($db_pref[$op]['iPreferencesId'], $db_pref[$op]['vType']);
        if($img != ''){
			$pref .= $img;
		}
	}

	$stack[0]['pref1'] = $pref;

	$sql = "SELECT iMemberCarId, eComfort , vImage, model.vTitle as model, make.vMake as make
	FROM member_car
	LEFT JOIN model ON model.iModelId = member_car.iModelId
	LEFT JOIN make ON make.iMakeId = member_car.iMakeId
	WHERE iMemberCarId = '".$db_ride[0]['iMemberCarId']."'";
	$db_car_details = $obj->MySQLSelect($sql);

	if(is_file($tconfig["tsite_upload_images_member_path"].$db_ride[0]['iMemberId']."/1_".$db_car_details[0]["vImage"]))
	{
		$stack[0]['car_image'] = $tconfig["tsite_upload_images_member"].$db_ride[0]['iMemberId']."/1_".$db_car_details[0]["vImage"];
	}
	else
	{
		$stack[0]['car_image'] = '';
	}

	$stack[0]['carname'] = $db_car_details[0]['model'].', '.$db_car_details[0]['make'];
	$stack[0]['carcomfort'] = $db_car_details[0]['eComfort'];

	if($db_pref[0]['eChattiness'] == 'YES'){
		$eChattiness = $tconfig['tsite_images'].'chat1.png';
		$titlechat=LBL_I_LOVE_CHAT;
	}
	if($db_pref[0]['eChattiness'] == 'MAYBE'){
		$eChattiness = $tconfig['tsite_images'].'chat2.png';
		$titlechat=LBL_TALK_MOOD;
	}
	if($db_pref[0]['eChattiness'] == 'NO'){
		$eChattiness = $tconfig['tsite_images'].'chat3.png';
		$titlechat=LBL_QUIET_TYPE;
	}

	if($db_pref[0]['eMusic'] == 'YES'){
		$eMusic = $tconfig['tsite_images'].'music1.png';
		$titlemusic=LBL_ABOUT_PLAYLIST;
	}
	if($db_pref[0]['eMusic'] == 'MAYBE'){
		$eMusic = $tconfig['tsite_images'].'music2.png';
		$titlemusic="Music- ".LBL_DEPEND_MOOD;
	}
	if($db_pref[0]['eMusic'] == 'NO'){
		$eMusic = $tconfig['tsite_images'].'music3.png';
		$titlemusic=LBL_SILENCE_GOLDEN;
	}


	if($db_pref[0]['eSmoking'] == 'YES'){
		$eSmoking = $tconfig['tsite_images'].'smoke1.png';
		$titlesmok=LBL_CIGARETTE_SMOKE;
	}
	if($db_pref[0]['eSmoking'] == 'MAYBE'){
		$eSmoking = $tconfig['tsite_images'].'smoke2.png';
		$titlesmok="Cigerrate- ".LBL_DEPEND_MOOD;
	}
	if($db_pref[0]['eSmoking'] == 'NO'){
		$eSmoking = $tconfig['tsite_images'].'smoke3.png';
		$titlesmok=LBL_NO_SMOKE;
	}

	if($db_pref[0]['eEcig'] == 'YES'){
		$eEcig = $tconfig['tsite_images'].'ecig1.png';
		$titlesmok1="e".LBL_CIGARETTE_SMOKE;
	}
	if($db_pref[0]['eEcig'] == 'MAYBE'){
		$eEcig = $tconfig['tsite_images'].'ecig2.png';
		$titlesmok1="eCigerrate- ".LBL_DEPEND_MOOD;
	}
	if($db_pref[0]['eEcig'] == 'NO'){
		$eEcig = $tconfig['tsite_images'].'ecig3.png';
		$titlesmok1="e".LBL_NO_SMOKE;
	}

	if($db_pref[0]['ePets'] == 'YES'){
		$ePets = $tconfig['tsite_images'].'pets1.png';
		$titlepet = LBL_PETS1;
	}
	if($db_pref[0]['ePets'] == 'MAYBE'){
		$ePets = $tconfig['tsite_images'].'pets2.png';
		$titlepet = "Pets -".LBL_DEPEND_MOOD;
	}
	if($db_pref[0]['ePets'] == 'NO'){
		$ePets = $tconfig['tsite_images'].'pets3.png';
		$titlepet = LBL_PETS2;
	}

	//$stack[0]['pref'] = '<img alt="" src="'.$eChattiness.'" style="width:12%;" title="'.$titlechat.'">&nbsp;<img alt="" src="'.$eMusic.'" style="width:12%;" title="'.$titlemusic.'">&nbsp;<img alt="" src="'.$eSmoking.'" style="width:12%;" title="'.$titlesmok.'">&nbsp;<img alt="" src="'.$eEcig.'" style="width:12%;" title="'.$titlesmok1.'">&nbsp;<img alt="" src="'.$ePets.'" style="width:12%;" title="'.$titlepet.'">';
	//$stack[0]['pref1'] = '<img alt="" src="'.$eChattiness.'" style="width:5%;" title="'.$titlechat.'">&nbsp;<img alt="" src="'.$eMusic.'" style="width:5%;" title="'.$titlemusic.'">&nbsp;<img alt="" src="'.$eSmoking.'" style="width:5%;" title="'.$titlesmok.'">&nbsp;<img alt="" src="'.$eEcig.'" style="width:5%;" title="'.$titlesmok1.'">&nbsp;<img alt="" src="'.$ePets.'" style="width:5%;" title="'.$titlepet.'">';


	$sql = "SELECT count(iRateId) as tot_count_rating,SUM(iRate) as tot_sum_rating
	FROM member_rating WHERE iMemberToId = ".$db_ride[0]['iMemberId'];
	$db_member_ratings = $obj->MySQLSelect($sql);

	$rating = $db_member_ratings[0]['tot_sum_rating'];
	$rating = $rating / $db_member_ratings[0]['tot_count_rating'];

	$rating_width = ($rating * 100) / 5;
	$stack[0]['rating'] = '<span style="display: block; width: 65px; height: 13px; background: url('.$tconfig['tsite_images'].'star-rating-sprite.png) 0 0;">
	<span style="margin: 0;display: block; width: '.$rating_width.'%; height: 13px; background: url('.$tconfig['tsite_images'].'star-rating-sprite.png) 0 -13px;"></span>
	</span>';

	$sql = "SELECT count(iRideId) as tot_ride  FROM rides_new WHERE iMemberId = ".$db_ride[0]['iMemberId'];
	$db_tot_rides = $obj->MySQLSelect($sql);

	$stack[0]['tot_ride'] = $db_tot_rides[0]['tot_ride'];
	#echo  date('Y-m-d H:i:s',strtotime('-60 minutes', strtotime(date('Y-m-d H:i:s'))));echo "<br />";
	#echo date('Y-m-d H:i:s');
	$timebeforeonehour = date('Y-m-d H:i:s',strtotime('-60 minutes', strtotime(date('Y-m-d H:i:s'))));
	if(strtotime($date.' '.$time) < strtotime($timebeforeonehour)){
		$button_show = 'No';
		}else{
		$button_show = 'Yes';
	}

	$smarty->assign("button_show",$button_show);
	$smarty->assign("price",$price);
	$smarty->assign("price_color",$price_color);
	$smarty->assign("startpoint",$startpoint);
	$smarty->assign("endpoint",$endpoint);
	$smarty->assign("places_str",$places_str); //$db_ride[0]['vMainDeparture'].$places_str.' &rarr; '.$db_ride[0]['vMainArrival']
	//$smarty->assign("date",$generalobj->DateTime($date,9));
	$smarty->assign("date",$generalobj->DateTimeFormat($date));
	$smarty->assign("db_ride",$db_ride);
	$smarty->assign("stack",$stack);
	$smarty->assign("previous",$previous);
	$smarty->assign("next",$next);
	$smarty->assign("id",$id);
	$smarty->assign("tot_res",$tot_res);
	$smarty->assign("time",$time);
	$smarty->assign("seats",$seats);
	$smarty->assign("db_ratings",$db_ratings);
	$smarty->assign("from",$_REQUEST['from']);
	$smarty->assign("doc",$doc);
	$smarty->assign("docprice",$docprice);
	$smarty->assign("docweight",$docweight);
	$smarty->assign("docunit",$docunit);
	$smarty->assign("box",$box);
	$smarty->assign("boxprice",$boxprice);
	$smarty->assign("boxweight",$boxweight);
	$smarty->assign("boxunit",$boxunit);
	$smarty->assign("lug",$lug);
	$smarty->assign("lugprice",$lugprice);
	$smarty->assign("lugweight",$lugweight);
	$smarty->assign("lugunit",$lugunit);
	$smarty->assign("Remaindoc",$Remaindoc);
	$smarty->assign("Remainbox",$Remainbox);
	$smarty->assign("Remainluggage",$Remainluggage);

?>
