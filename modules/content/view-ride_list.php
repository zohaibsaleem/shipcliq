<?php 
	$curr_page = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];	
	$last_url = $_SERVER['HTTP_REFERER'];
	
	if($_REQUEST['From'] == '' && $_REQUEST['To'] == '')
	$action = ($curr_page==$last_url)?'old':$_REQUEST['action']; 
	else
	$action = $_REQUEST['action'];
	/* to remove slashes */
	$strip_slashes_arr = array('from','to','loc1','loc2','loc3','loc4','loc5','loc6');
	foreach($strip_slashes_arr as $val)
	{
		$_POST[$val] = isset($_POST[$val])?stripslashes($_POST[$val]):'';
		$_REQUEST[$val] = isset($_REQUEST[$val])?stripslashes($_REQUEST[$val]):'';
	}
	
	$zoom_level = 3;
	
	if($_REQUEST['searchdate'] != ''){
		$fromdate = $_REQUEST['searchdate'];
		$todate = $_REQUEST['searchdate'];
		}else{
		$fromdate = date("Y-m-d");
		$todate = date('Y-m-d', strtotime($fromdate. ' + 180 days'));
	}    
	
	if($_REQUEST['fromhr'] != ''){
		$fromassign = $_REQUEST['fromhr']; 
		if($_REQUEST['fromhr'] == '24'){
			$tohr = '23:59:59';
			}else{
			$fromhr = $_REQUEST['fromhr'].":00:00";
		}     
		//$fromhr = str_pad($fromhr,5,'0',STR_PAD_LEFT);
		}else{
		$fromassign = 1;      
		$fromhr = '01:00:00';
	}     
	
	if($_REQUEST['tohr'] != ''){
		$toassign = $_REQUEST['tohr']; 
		if($_REQUEST['tohr'] == '24'){
			$tohr = '23:59:59';
			}else{
			$tohr = $_REQUEST['tohr'].":00:00";
		}      
		//$tohr = str_pad($tohr,5,'0',STR_PAD_LEFT);
		}else{        
		$toassign = 24;   
		$tohr = '23:59:59';
	}
	
	if($_REQUEST['srtby'] != ''){
		$srtby = $_REQUEST['srtby'];
		}else{
		$srtby = 0;
	}
	
	if($_REQUEST['schedule'] != ''){
		if($_REQUEST['schedule'] == 'DESC'){
			$ordscheduleby = 'DESC';
			}else{
			$ordscheduleby = 'ASC';
		}
		}else{
		$ordscheduleby = 'ASC';
	}
	
	if($_REQUEST['priceord'] != ''){
		if($_REQUEST['priceord'] == 'DESC'){
			$ordpriceby = 'DESC';
			}else{
			$ordpriceby = 'ASC';
		}
		}else{
		$ordpriceby = 'ASC';
	}
	
	if($_REQUEST['comfort'] != ''){
		$comfort = $_REQUEST['comfort'];
		if($_REQUEST['comfort'] == 'All'){
			$comfortfilltering = 1;
			}else{
			$comfortfilltering = $_REQUEST['comfort'];
		}     
		}else{
		$comfort = 'All';
		$comfortfilltering = 1;
	}
	
	if($_REQUEST['pricetype'] != ''){
		$pricetype = $_REQUEST['pricetype'];
		if($_REQUEST['pricetype'] == 'All'){
			$pricetypefilltering = 1;
			}else{
			$pricetypefilltering = $_REQUEST['pricetype'];
		}     
		}else{
		$pricetype = 'All';
		$pricetypefilltering = 1;
	}
	
	
	
	if($_REQUEST['fltphoto'] != '')
	{
		$fltphoto = $_REQUEST['fltphoto'];
		if($_REQUEST['fltphoto'] == 'Allphoto')
		{
			$photofilltering = 1;
		}
		else
		{
			$photofilltering = 'Yes';
		}
	}
	else
	{
		$fltphoto = 'Allphoto';
		$photofilltering = 1;
	}
	
	
	if($_REQUEST['fltmemrating'] != ''){
		$fltmemrating = $_REQUEST['fltmemrating'];
		if($_REQUEST['fltmemrating'] == 'All'){
			$ratingfilltering = 'All';
			}else{
			$ratingfilltering = $_REQUEST['fltmemrating'];
		}
		}else{
		$fltmemrating = 'All';
		$ratingfilltering = 'All';
	}
	
	########### Shipment Type Start #############
	
	######### From Home Start ##################
	$eDocument  = (isset($_REQUEST['category']) && $_REQUEST['category'] == 'document')?'Yes':'';
	$eLuggage  = (isset($_REQUEST['category']) && $_REQUEST['category'] == 'luggage')?'Yes':'';
	$eBox	  = (isset($_REQUEST['category']) && $_REQUEST['category'] == 'box')?'Yes':'';
	
	if(isset($_REQUEST['category']) && $_REQUEST['category'] == 'all')
	{
		$eDocument = "No";
		$eLuggage = "No";
		$eBox = "No";
	}
	
	if(!isset($_REQUEST['category']))
	{
		if(isset($_REQUEST['eDocument']) && $_REQUEST['eDocument'] != '')
		{
			$eDocument = $_REQUEST['eDocument'];
		}
		else
		{
			$eDocument = "No";
		}
		
		if(isset($_REQUEST['eBox']) && $_REQUEST['eBox'] != '')
		{
			$eBox  =  $_REQUEST['eBox'];
		}
		else
		{
			$eBox  = "No";
		}
		
		if(isset($_REQUEST['eLuggage']) && $_REQUEST['eLuggage'] != '')
		{
			$eLuggage  = $_REQUEST['eLuggage'];
		}
		else
		{
			$eLuggage  = "No";
		}
	}
	######### From Home End ##################
	
	########### Shipment Type End ###############
	
	#echo "<pre>"; print_r($_REQUEST); exit;
	
	if($action == 'old')
	{ 
		$mysearch = $_SESSION['search_peram'];
		$mysearch = unserialize($mysearch);
		
		$From = $mysearch['From']; 
		$To = $mysearch['To']; 
		$search = $mysearch['search']; 
		$type = $mysearch['type']; 
		
		$from_lat_long = explode(', ',substr($mysearch['from_lat_long'], 1, -1)); 
		$from_lat = number_format($from_lat_long[0],6,'.','');
		$from_long = number_format($from_lat_long[1],6,'.',''); 
		
		$to_lat_long = explode(', ',substr($mysearch['to_lat_long'], 1, -1)); 
		$to_lat = number_format($to_lat_long[0],6,'.','');
		$to_long = number_format($to_lat_long[1],6,'.','');  
		
		$searcharr = array();
		$searcharr['type'] = $type;
		$searcharr['search'] = $search;
		$searcharr['From'] = $From;
		$searcharr['To'] = $To;
		$searcharr['from_lat_long'] = $mysearch['from_lat_long'];
		$searcharr['to_lat_long'] = $mysearch['to_lat_long'];
		
		//echo "<pre>";
		//print_r($searcharr); exit; 
		
		$_SESSION['search_peram'] = serialize($searcharr);
	}
	else
	{
		
		$From = $_REQUEST['From']; 
		$To = $_REQUEST['To']; 
		$search = $_REQUEST['search']; 
		$type = $_REQUEST['type']; 
		
		$from_lat_long = explode(', ',substr($_REQUEST['From_lat_long'], 1, -1)); 
		$from_lat = number_format($from_lat_long[0],6,'.','');
		$from_long = number_format($from_lat_long[1],6,'.',''); 
		
		$to_lat_long = explode(', ',substr($_REQUEST['To_lat_long'], 1, -1)); 
		$to_lat = number_format($to_lat_long[0],6,'.','');
		$to_long = number_format($to_lat_long[1],6,'.','');
		
		$searcharr = array();
		$searcharr['type'] = $type;
		$searcharr['search'] = $search;
		$searcharr['From'] = $From;
		$searcharr['To'] = $To;
		$searcharr['from_lat_long'] = $_REQUEST['From_lat_long'];
		$searcharr['to_lat_long'] = $_REQUEST['To_lat_long']; 
		
		$_SESSION['search_peram'] = serialize($searcharr);
	}
	
	if($search == 'place')
	{ 
		
		$sql = "SELECT iRidePointId, iRideId, tRideTime, eReverse, ( 3959 * acos( cos( radians(".$from_lat.") ) * cos( radians( vStartLatitude ) ) * cos( radians( vStartLongitude ) - radians(".$from_long.") ) + sin( radians(".$from_lat.") ) * sin( radians( vStartLatitude ) ) ) ) AS distance FROM ride_points_new HAVING distance < 500 ORDER BY distance";
		$db_start_points = $obj->MySQLSelect($sql);
		
		$sql = "SELECT iRidePointId, iRideId , tRideTime, eReverse, ( 3959 * acos( cos( radians(".$to_lat.") ) * cos( radians( vEndLatitude ) ) * cos( radians( vEndLongitude ) - radians(".$to_long.") ) + sin( radians(".$to_lat.") ) * sin( radians( vEndLatitude ) ) ) ) AS distance FROM ride_points_new HAVING distance < 500 ORDER BY distance";
		$db_end_points = $obj->MySQLSelect($sql); 
		
		$stack = array();
		for($i=0;$i<count($db_start_points);$i++){
			for($j=0;$j<count($db_end_points);$j++){      
				$newarr = array();
				if($db_end_points[$j]['iRideId'] == $db_start_points[$i]['iRideId'] && $db_end_points[$j]['eReverse'] == $db_start_points[$i]['eReverse'])
				{
					$newarr['iRideId'] = $db_end_points[$j]['iRideId'];
					$newarr['start_iRidePointId'] = $db_start_points[$i]['iRidePointId'];
					$newarr['end_iRidePointId'] = $db_end_points[$j]['iRidePointId'];
					$newarr['return'] = $db_start_points[$i]['eReverse'];
					$newarr['tRideTime'] = $db_start_points[$i]['tRideTime'];
					array_push($stack,$newarr);  
				}
			}  
		}    
	} 
	#echo "<pre>"; print_r($stack); exit;  
	
	if($search == 'simple'){
		
		$simplesql = "";
		if($eDocument == "Yes")
		$simplesql = " eDocument = '$eDocument'";
		
		if($eLuggage == "Yes")
		$simplesql .= ($simplesql == '')?"eLuggage = '$eLuggage'":" OR eLuggage = '$eLuggage'";
		
		if($eBox == "Yes")
		$simplesql .= ($simplesql == '')?"eBox = '$eBox'":" OR eBox = '$eBox'";
		
		$simplesql = ($simplesql == '')?'1': $simplesql;
		
		
		if($_COOKIE['country_code'] != ''){
			$ccsql = " AND rides_new.vCountryCode = '".$_COOKIE['country_code']."' ";
		}
		
		if($type == 'ladiesonly'){
			$ssql .= "AND eLadiesOnly = 'Yes'";
			}else if($type == 'airport'){
			$ssql .= "AND eRidePlaceType = 'Airport'";
			}else if($type == 'shopping'){
			$ssql .= "AND eRidePlaceType = 'Shopping'";
			}else if($type == 'latvia'){
			$ssql .= "AND vCountryCode = 'LV'";
			}else if($type == 'estonian'){
			$ssql .= "AND vCountryCode = 'EE'";
			}else if($type == 'lithuanian'){
			$ssql .= "AND vCountryCode = 'LT'";
			}else if($type == 'baltic'){
			//$ssql .= "AND vCountryCode IN ('LV','LT','EE')";
			$ssql .= "AND vCountryCode != vCountryCodeTo";
			}else if($type == 'latest'){
			$ssql .= "ORDER BY iRideId DESC";
		}      
		$sql = "SELECT iRideId, eRoundTrip FROM rides_new WHERE 1=1  AND ($simplesql) $ccsql $ssql";
		$db_rides = $obj->MySQLSelect($sql);
		
		$stack = array();
		for($i=0;$i<count($db_rides);$i++){
			$newarr = array();
			$sql = "SELECT iRidePointId, eReverse, tRideTime FROM ride_points_new WHERE iRideId = '".$db_rides[$i]['iRideId']."' AND eReverse = 'No' ORDER BY iRidePointId ASC";
			$no_points = $obj->MySQLSelect($sql);
			
			$rb = count($no_points);
			$lastpoint = $rb-1;              
			if($rb > 0){          
				$newarr['iRideId'] = $db_rides[$i]['iRideId'];
				$newarr['start_iRidePointId'] = $no_points[0]['iRidePointId'];
				$newarr['end_iRidePointId'] = $no_points[$lastpoint]['iRidePointId'];
				$newarr['return'] = $no_points[0]['eReverse'];
				$newarr['tRideTime'] = $no_points[0]['tRideTime'];
				array_push($stack,$newarr);
			}         
			
			if($db_rides[$i]['eRoundTrip'] == 'Yes'){
				$sql = "SELECT iRidePointId, eReverse, tRideTime FROM ride_points_new WHERE iRideId = '".$db_rides[$i]['iRideId']."' AND eReverse = 'Yes' ORDER BY iRidePointId ASC";
				$yes_points = $obj->MySQLSelect($sql);
				
				$rb = count($yes_points); 
				$lastpoint = $rb-1;       
				if($rb > 0){         
					$newarr['iRideId'] = $db_rides[$i]['iRideId'];
					$newarr['start_iRidePointId'] = $yes_points[0]['iRidePointId'];
					$newarr['end_iRidePointId'] = $yes_points[$lastpoint]['iRidePointId'];
					$newarr['return'] = $yes_points[0]['eReverse'];
					$newarr['tRideTime'] = $yes_points[0]['tRideTime'];
					array_push($stack,$newarr); 
				}
			}
		}
	}
	
	if($search == 'arround_you'){
		$userlatitude =  $_COOKIE['userlatitude'];
		$userlongitude = $_COOKIE['userlongitude'];
		
		if($_COOKIE['country_code'] != ''){
			$ccsql = " AND rides_new.vCountryCode = '".$_COOKIE['country_code']."' ";
		}
		
		//$sql = "SELECT iRidePointId, iRideId, tRideTime, eReverse, ( 3959 * acos( cos( radians(".$userlatitude.") ) * cos( radians( vStartLatitude ) ) * cos( radians( vStartLongitude ) - radians(".$userlongitude.") ) + sin( radians(".$userlatitude.") ) * sin( radians( vStartLatitude ) ) ) ) AS distance FROM ride_points_new HAVING distance < 5 ORDER BY distance";
		//$db_end_points = $obj->MySQLSelect($sql);
		
		//$sql = "SELECT iRidePointId, iRideId , tRideTime, eReverse, ( 3959 * acos( cos( radians(".$userlatitude.") ) * cos( radians( vEndLatitude ) ) * cos( radians( vEndLongitude ) - radians(".$userlongitude.") ) + sin( radians(".$userlatitude.") ) * sin( radians( vEndLatitude ) ) ) ) AS distance FROM ride_points_new HAVING distance < 5 ORDER BY distance";
		//$db_start_points = $obj->MySQLSelect($sql);
		
		$sql = "SELECT ride_points_new.iRidePointId, ride_points_new.iRideId, ride_points_new.eReverse, ride_points_new.vStartPoint, ride_points_new.vEndPoint, ride_points_new.fPrice,ride_points_new.fTotPrice, ride_points_new.tRideTime,    
		( 3959 * acos( cos( radians(".$userlatitude.") ) * cos( radians( ride_points_new.vStartLatitude ) ) * cos( radians( ride_points_new.vStartLongitude ) - radians(".$userlongitude.") ) + sin( radians(".$userlatitude.") ) * sin( radians( ride_points_new.vStartLatitude ) ) ) ) AS distance 
		FROM ride_points_new 
		LEFT JOIN rides_new ON rides_new.iRideId = ride_points_new.iRideId 
		WHERE rides_new.eStatus = 'Active' 
		HAVING distance < 25";
		$db_ride_points = $obj->MySQLSelect($sql); 
		
		$stack = array();
		$newarr = array();
		for($i=0;$i<count($db_ride_points);$i++){
			$sql = "SELECT iRidePointId FROM ride_points_new WHERE iRideId = '".$db_ride_points[$i]['iRideId']."' AND eReverse = '".$db_ride_points[$i]['eReverse']."' ORDER BY iRidePointId DESC LIMIT 0,1";
			$db_last_point = $obj->MySQLSelect($sql); 
			$newarr['iRideId'] = $db_ride_points[$i]['iRideId'];
			$newarr['start_iRidePointId'] = $db_ride_points[$i]['iRidePointId'];
			$newarr['end_iRidePointId'] = $db_last_point[0]['iRidePointId'];
			$newarr['return'] = $db_ride_points[$i]['eReverse'];
			$newarr['tRideTime'] = $db_ride_points[$i]['tRideTime'];
			array_push($stack,$newarr);  
		}    
		
		$sql = "SELECT ride_points_new.iRidePointId, ride_points_new.iRideId, ride_points_new.eReverse, ride_points_new.vStartPoint, ride_points_new.vEndPoint, ride_points_new.fPrice, ride_points_new.fTotPrice ,ride_points_new.tRideTime,    
		( 3959 * acos( cos( radians(".$userlatitude.") ) * cos( radians( ride_points_new.vEndLatitude ) ) * cos( radians( ride_points_new.vEndLongitude ) - radians(".$userlongitude.") ) + sin( radians(".$userlatitude.") ) * sin( radians( ride_points_new.vEndLatitude ) ) ) ) AS distance 
		FROM ride_points_new 
		LEFT JOIN rides_new ON rides_new.iRideId = ride_points_new.iRideId 
		WHERE rides_new.eStatus = 'Active'  
		HAVING distance < 25";
		$db_ride_points = $obj->MySQLSelect($sql);      
		$newarr = array();
		for($i=0;$i<count($db_ride_points);$i++){
			$sql = "SELECT iRidePointId FROM ride_points_new WHERE iRideId = '".$db_ride_points[$i]['iRideId']."' AND eReverse = '".$db_ride_points[$i]['eReverse']."' ORDER BY iRidePointId DESC LIMIT 0,1";
			$db_last_point = $obj->MySQLSelect($sql); 
			$newarr['iRideId'] = $db_ride_points[$i]['iRideId'];
			$newarr['start_iRidePointId'] = $db_ride_points[$i]['iRidePointId'];
			$newarr['end_iRidePointId'] = $db_last_point[0]['iRidePointId'];
			$newarr['return'] = $db_ride_points[$i]['eReverse'];
			$newarr['tRideTime'] = $db_ride_points[$i]['tRideTime'];
			array_push($stack,$newarr);  
			
		} 
	}
	
	// All ride details filter not calculated
	for($i=0;$i<count($stack);$i++)
	{
		$ssql = "";
		if($eDocument == "Yes")
		$ssql = " eDocument = '$eDocument'";
		
		if($eLuggage == "Yes")
		$ssql .= ($ssql == '')?"eLuggage = '$eLuggage'":" OR eLuggage = '$eLuggage'";
		
		if($eBox == "Yes")
		$ssql .= ($ssql == '')?"eBox = '$eBox'":" OR eBox = '$eBox'";
		
		$ssql = ($ssql == '')?'1': $ssql;
		
		
		$sql = "SELECT * FROM rides_new WHERE iRideId = '".$stack[$i]['iRideId']."' AND ($ssql)";
		$db_ride_data = $obj->MySQLSelect($sql);
		
		$stack[$i]['seats'] = $db_ride_data[0]['iSeats']; 
		$stack[$i]['ridestatus'] = $db_ride_data[0]['eStatus']; 
		$stack[$i]['eLadiesOnly'] = $db_ride_data[0]['eLadiesOnly'];
		$stack[$i]['eRidePlaceType'] = $db_ride_data[0]['eRidePlaceType']; 
		
		######### For Document Luggage Box start ###########
		$stack[$i]['des'] 				= '';
		$stack[$i]['fDocumentPrice'] 	= '';
		$stack[$i]['docprice'] 			= '';
		$stack[$i]['vDocumentWeight'] 	= '';
		$stack[$i]['fBoxPrice'] 		= '';
		$stack[$i]['boxPrice'] 			= '';
		$stack[$i]['vBoxWeight'] 		= '';
		$stack[$i]['fLuggagePrice'] 	= '';
		$stack[$i]['lugprice'] 			= '';
		$stack[$i]['vLuggageWeight'] 	= '';
		
		#echo '<pre>'; print_R($db_ride_data[0]); echo '</pre>';
		$sess_price_ratio = isset($_SESSION['sess_price_ratio'])?$_SESSION['sess_price_ratio']:CURR_MASTER_DEF_CURR;
		if(isset($db_ride_data[0]['eDocument']) && $db_ride_data[0]['eDocument'] == "Yes")
		{
			$stack[$i]['fDocumentPrice'] 	= $docprice = $db_ride_data[0]['fDocumentPrice'] * $db_ride_data[0]['fRatio_'.$sess_price_ratio];
			$stack[$i]['docprice'] 			= $docprice = $generalobj->booking_currency($docprice, $sess_price_ratio);
			$stack[$i]['vDocumentWeight'] 	= $docweight = $db_ride_data[0]['vDocumentWeight'];
			$stack[$i]['vDocumentUnit'] 	= $docunit = $db_ride_data[0]['vDocumentUnit'];
			$stack[$i]['des'] .= '<p><Strong>Document:</Strong> <em> '.$docprice .' ('.$docweight.') '.$docunit.'</em></p>';
		}
		
		if(isset($db_ride_data[0]['eBox']) && $db_ride_data[0]['eBox'] == "Yes")
		{
			$stack[$i]['fBoxPrice'] 	= $boxprice = $db_ride_data[0]['fBoxPrice'] * $db_ride_data[0]['fRatio_'.$sess_price_ratio];	
			$stack[$i]['boxPrice'] 		= $boxprice = $generalobj->booking_currency($boxprice, $sess_price_ratio);
			$stack[$i]['vBoxWeight'] 	= $boxweight = $db_ride_data[0]['vBoxWeight'];
			$stack[$i]['vBoxUnit'] 	= $boxunit = $db_ride_data[0]['vBoxUnit'];
			$stack[$i]['des'] .= '<p><Strong>Box:</Strong> <em>'.$boxprice .' ('.$boxweight.') '.$boxunit.'</em></p>';
		}
		
		if(isset($db_ride_data[0]['eLuggage']) && $db_ride_data[0]['eLuggage'] == "Yes")
		{
			$stack[$i]['fLuggagePrice'] 	= $lugprice = $db_ride_data[0]['fLuggagePrice'] * $db_ride_data[0]['fRatio_'.$sess_price_ratio];
			$stack[$i]['lugprice'] 			= $lugprice = $generalobj->booking_currency($lugprice, $sess_price_ratio);
			$stack[$i]['vLuggageWeight'] 	= $lugweight = $db_ride_data[0]['vLuggageWeight'];
			$stack[$i]['vLuggageUnit'] 	= $lugunit = $db_ride_data[0]['vLuggageUnit'];
			$stack[$i]['des'] .= '<p><Strong>Luggage:</Strong> <em> '.$lugprice .' ('.$lugweight.') '.$lugunit.'</em></p>';
		}  
		######### For Document Luggage Box end ###########
		
		$sql = "SELECT member.vFirstName, member.vFbFriendCount, member.vLastName, member.vNickName, member.vImage, member.eGender, member.iBirthYear, member.iRating, count(member_rating.iRateId) as tot_count_rating, SUM(member_rating.iRate) as tot_sum_rating  
		FROM member 
		LEFT JOIN member_rating ON member_rating.iMemberToId = member.iMemberId 
		WHERE iMemberId = '".$db_ride_data[0]['iMemberId']."'";
		$db_member = $obj->MySQLSelect($sql);  
		
		$rating = $db_member[0]['tot_sum_rating'];
		$rating = $rating / $db_member[0]['tot_count_rating'];     
		$rating = round($rating);
		
		if($rating > 5){
			$rating = 5;
		}
		$stack[$i]['memberrating'] = $db_member[0]['iRating'];
		//$stack[$i]['drivername'] = $db_member[0]['vFirstName'].' '.$db_member[0]['vLastName'];
		if($db_member[0]['vNickName'] != ""){
			$stack[$i]['drivername'] = $db_member[0]['vNickName']; 
			$stack[$i]['driverStatus'] = $db_member[0]['eStatus'];
		}
		else
		{
			$stack[$i]['drivername'] = $db_member[0]['vFirstName'].' '.$db_member[0]['vLastName'];
			$stack[$i]['driverStatus'] = $db_member[0]['eStatus'];
		}       
		
		if($db_member[0]['iBirthYear'] > 0 )
			$stack[$i]['age'] = date('Y') - $db_member[0]['iBirthYear'];
		else
			$stack[$i]['age'] = "---";
		$stack[$i]['vFbFriendCount'] = $db_member[0]['vFbFriendCount'];
	
		
		$rating_width = ($rating * 100) / 5; 
		$rating_width = '<span style="display: block; width: 65px; height: 13px; background: url('.$tconfig['tsite_images'].'star-rating-sprite.png) 0 0;">
		<span style="display: block; width: '.$rating_width.'%; height: 13px; background: url('.$tconfig['tsite_images'].'star-rating-sprite.png) 0 -13px;"></span>
		</span>&nbsp;&nbsp;'.$rating.' Rating'; 
		
		$stack[$i]['rating'] = $rating_width;   
		
		if(is_file($tconfig["tsite_upload_images_member_path"].$db_ride_data[0]['iMemberId']."/1_".$db_member[0]['vImage']))
		{
			$stack[$i]['image'] = $tconfig["tsite_upload_images_member"].$db_ride_data[0]['iMemberId']."/1_".$db_member[0]['vImage'];
			$stack[$i]['imgavail'] = 'Yes';
		} 
		else
		{
			$stack[$i]['imgavail'] = 'No';
			
			if($db_member[0]['eGender'] == 'Male'){
				$stack[$i]['image'] = $tconfig['tsite_images'].'64x64male.png';
				}else{
				$stack[$i]['image'] = $tconfig['tsite_images'].'64x64female.png';
			}
		}
		
		$sql = "SELECT * FROM member_car_preferences WHERE iMemberId = '".$db_ride_data[0]['iMemberId']."'";
		$db_pref = $obj->MySQLSelect($sql);
		
	    $sql = "SELECT iMemberCarId, eComfort , model.vTitle as model, make.vMake as make ,make.eStatus 
		FROM member_car 
		LEFT JOIN model ON model.iModelId = member_car.iModelId 
		LEFT JOIN make ON make.iMakeId = member_car.iMakeId 
		WHERE iMemberCarId = '".$db_ride_data[0]['iMemberCarId']."'";
		$db_car_details = $obj->MySQLSelect($sql);
		
		$stack[$i]['carname'] = $db_car_details[0]['model'].', '.$db_car_details[0]['make'];
		$stack[$i]['carcomfort'] = $db_car_details[0]['eComfort']; 
        $stack[$i]['carstatus'] = $db_car_details[0]['eStatus'];  
		
		$eChattiness = '';
		$eMusic = '';
		$eSmoking = '';
		$eEcig = '';
		$ePets = '';
		
		if($db_pref[0]['eChattiness'] == 'YES'){
			$eChattiness = $tconfig['tsite_images'].'chat1.png';
		}
		if($db_pref[0]['eChattiness'] == 'MAYBE'){
			$eChattiness = $tconfig['tsite_images'].'chat2.png';
		}
		if($db_pref[0]['eChattiness'] == 'NO'){
			$eChattiness = $tconfig['tsite_images'].'chat3.png';
		}
		
		if($db_pref[0]['eMusic'] == 'YES'){
			$eMusic = $tconfig['tsite_images'].'music1.png';
		}
		if($db_pref[0]['eMusic'] == 'MAYBE'){
			$eMusic = $tconfig['tsite_images'].'music2.png';
		}
		if($db_pref[0]['eMusic'] == 'NO'){
			$eMusic = $tconfig['tsite_images'].'music3.png';
		}
		
		
		if($db_pref[0]['eSmoking'] == 'YES'){
			$eSmoking = $tconfig['tsite_images'].'smoke1.png';
		}
		if($db_pref[0]['eSmoking'] == 'MAYBE'){
			$eSmoking = $tconfig['tsite_images'].'smoke2.png';
		}
		if($db_pref[0]['eSmoking'] == 'NO'){
			$eSmoking = $tconfig['tsite_images'].'smoke3.png';
		}
		
		if($db_pref[0]['eEcig'] == 'YES'){
			$eEcig = $tconfig['tsite_images'].'ecig1.png';
		}
		if($db_pref[0]['eEcig'] == 'MAYBE'){
			$eEcig = $tconfig['tsite_images'].'ecig2.png';
		}
		if($db_pref[0]['eEcig'] == 'NO'){
			$eEcig = $tconfig['tsite_images'].'ecig3.png';
		}
		
		if($db_pref[0]['ePets'] == 'YES'){
			$ePets = $tconfig['tsite_images'].'pets1.png';
		}
		if($db_pref[0]['ePets'] == 'MAYBE'){
			$ePets = $tconfig['tsite_images'].'pets2.png';
		}
		if($db_pref[0]['ePets'] == 'NO'){
			$ePets = $tconfig['tsite_images'].'pets3.png';
		}
		
		$stack[$i]['pref'] = ($eChattiness != '')?'<img alt="" src="'.$eChattiness.'" style="width:12%;">&nbsp':'';
		$stack[$i]['pref'] .= ($eMusic != '')?'<img alt="" src="'.$eMusic.'" style="width:12%;">&nbsp':'';
		$stack[$i]['pref'] .= ($eSmoking != '')?'<img alt="" src="'.$eSmoking.'" style="width:12%;">&nbsp':'';
		$stack[$i]['pref'] .= ($eEcig != '')?'<img alt="" src="'.$eEcig.'" style="width:12%;">&nbsp':'';
		
		$rb = range($stack[$i]['start_iRidePointId'], $stack[$i]['end_iRidePointId']);
		
		$rb1 = array($stack[$i]['start_iRidePointId'], $stack[$i]['end_iRidePointId']);
		
		$sql = "SELECT iRidePointId, vStartPoint, vEndPoint, vDistance, vDuration, fPrice, fTotPrice, ePriceType, fOriginalPrice FROM ride_points_new WHERE iRideId = '".$stack[$i]['iRideId']."' AND eReverse = '".$stack[$i]['return']."' ORDER BY iRidePointId ASC";
		$db_point_data = $obj->MySQLSelect($sql);
		
		$places_str = '';
		$price = 0;
		$originalprice = 0;
		$minutes = 0;
		for($j=0;$j<count($db_point_data);$j++){
			if(count($db_point_data) == 1){
				$places_str = '<strong>'.$db_point_data[$j]['vStartPoint'].' &rarr; '.$db_point_data[$j]['vEndPoint'].'</strong>';
				}else{
				if(count($db_point_data)-1 == $j){
					if (in_array($db_point_data[$j]['iRidePointId'], $rb1)){
						$places_str .= '<strong>';
					}
					$places_str .= ' &rarr; '.$db_point_data[$j]['vStartPoint'].' &rarr; '.$db_point_data[$j]['vEndPoint']; 
					if (in_array($db_point_data[$j]['iRidePointId'], $rb1)){
						$places_str .= '</strong>';
					}
					}else{
					if (in_array($db_point_data[$j]['iRidePointId'], $rb1)){
						$places_str .= '<strong>';
					}
					if($j != 0){
						$arrrow = ' &rarr; ';
						}else{
						$arrrow = '';
					}
					$places_str .= $arrrow.$db_point_data[$j]['vStartPoint'];
					if (in_array($db_point_data[$j]['iRidePointId'], $rb1)){
						$places_str .= '</strong>';
					}
				} 
			} 
			
			if (in_array($db_point_data[$j]['iRidePointId'], $rb)){
				$price = $price + $db_point_data[$j]['fPrice'];
				//$price = $price + $db_point_data[$j]['fTotPrice']; 
				$originalprice = $originalprice + $db_point_data[$j]['fOriginalPrice'];
				$dur = explode(' ',$db_point_data[$j]['vDuration']);
				if (strpos($db_point_data[$j]['vDuration'],'hour')) {
					$dur = explode(' ',$db_point_data[$j]['vDuration']);
					$minutes = $minutes + ($dur[0]*60) + $dur[2];
					}else{
					$dur = explode(' ',$db_point_data[$j]['vDuration']);
					$minutes = $minutes + $dur[0];
				}
				
				if($ePriceType != 'High'){
					$ePriceType = $db_point_data[$j]['ePriceType'];
				}                     
			}                 
		}
		$stack[$i]['main_addr'] = $places_str;        
        
		$price = $price * $db_ride_data[0]['fRatio_'.$_SESSION['sess_price_ratio']];
		$stack[$i]['pricesort'] = $price;
		$stack[$i]['price'] = $generalobj->booking_currency($price, $_SESSION['sess_price_ratio']);
		$stack[$i]['duration'] = $minutes;
		
		$originalprice = $originalprice * $db_ride_data[0]['fRatio_'.$_SESSION['sess_price_ratio']];
		$price_diff = $price - $originalprice;
		$price_diff = abs($price_diff);
		
		if($price_diff > 2 && $price > $originalprice){
			$stack[$i]['price_color'] = '#FF9802';
			$ePriceType = 'High';
			}else if($price_diff > 2 && $price < $originalprice){
			$stack[$i]['price_color'] = '#68BE57';
			$ePriceType = 'Low';
			}else{
			$stack[$i]['price_color'] = '#000000';
			$ePriceType = 'Average';
		}
		
		$stack[$i]['ePriceType'] = $ePriceType;  
		
		if($db_ride_data[0]['eRideType'] == 'Reccuring'){
			if($stack[$i]['return'] == 'Yes'){
				$time = $db_ride_data[0]['vMainReturnTime'];
				}else{
				$time = $db_ride_data[0]['vMainOutBoundTime'];
			}
			}else{
			if($stack[$i]['return'] == 'Yes'){
				$time = $db_ride_data[0]['vMainArrivalTime'];
				}else{
				$time = $db_ride_data[0]['vMainDepartureTime'];
			}
		}      
		$stack[$i]['time'] = date("H:i", strtotime($stack[$i]['tRideTime']));               
	}  
	
	//echo "<pre>";
	//print_r($stack); exit;
	
	$mainarr = array();
	$mainsessionarr = array();
	//date is current active or not
	for($i=0;$i<count($stack);$i++){    
		$newarrdate = array();
		$newarrsess = array();
		if($stack[$i]['return'] == 'No'){
			$datetype = "dDateOut";
			}else{
			$datetype = "dDateRet";
		}
		
		if($type == 'today'){
			$abc = '';
			$abc = " AND $datetype = '".date("Y-m-d")."'";
		}
		
		$sql = "SELECT iRideDateId, ".$datetype." as date FROM ride_dates WHERE iRideId = '".$stack[$i]['iRideId']."' $abc";
		$db_ride_dates = $obj->MySQLSelect($sql);  
		
		for($j=0;$j<count($db_ride_dates);$j++){ 
			
			$valid = 1;
			if($db_ride_dates[$j]['date'] == date("Y-m-d")) {
				if($stack[$i]['time'] < date("H:m")) {
					$valid = 0;
				}			
			}
			if($valid) { 
				if(strtotime($stack[$i]['time']) >= strtotime($fromhr) && strtotime($stack[$i]['time']) <= strtotime($tohr)){          
					if($db_ride_dates[$j]['date'] != '0000-00-00' && strtotime($db_ride_dates[$j]['date']) >= strtotime($fromdate) && strtotime($db_ride_dates[$j]['date']) <= strtotime($todate)){ 
						$sql = "SELECT iRidePointId, iNoOfSeats FROM booking_new WHERE dBookingDate = '".$db_ride_dates[$j]['date']."' AND iRideId = '".$stack[$i]['iRideId']."' AND eTripReturn = '".$stack[$i]['return']."' AND eStatus != 'Cencelled'";
						$db_booking = $obj->MySQLSelect($sql);
						
						$pointrange = range($stack[$i]['start_iRidePointId'], $stack[$i]['end_iRidePointId']);
						
						$seats = $stack[$i]['seats'];
						for($r=0;$r<count($db_booking);$r++){
							$bookingpoints = explode(",", $db_booking[$r]['iRidePointId']);
							
							$conf = array_intersect($bookingpoints, $pointrange);            
							if (count($conf) > 0){
								$seats = $seats - $db_booking[$r]['iNoOfSeats'];
							}
						}
						
						$Remainload = $generalobj->parcel_count($stack[$i]['iRideId']);
						#echo "<pre>"; print_r($Remainload).'<br>'; 
						$newarrsess['iRideId'] = $stack[$i]['iRideId'];  
						$newarrsess['vDocRemain'] = $Remainload['vDoc'];  
						$newarrsess['vBoxRemain'] = $Remainload['vBox'];  
						$newarrsess['vLuggageRemain'] = $Remainload['vLuggage'];
						$newarrsess['start_iRidePointId'] = $stack[$i]['start_iRidePointId'];
						$newarrsess['end_iRidePointId'] = $stack[$i]['end_iRidePointId'];
						$newarrsess['return'] = $stack[$i]['return']; 
						$newarrsess['date'] = $db_ride_dates[$j]['date'];
						$newarrsess['strtotime'] = strtotime($db_ride_dates[$j]['date'].$stack[$i]['time'].':00');
						$newarrsess['mainaddress'] = $stack[$i]['main_addr'];
						$newarrsess['time'] = $stack[$i]['time'];
						$newarrsess['price'] = $stack[$i]['price'];
						$newarrsess['seats'] = $seats;
						$newarrsess['pricesort'] = $stack[$i]['pricesort'];
						$newarrsess['carcomfort'] = $stack[$i]['carcomfort'];
						$newarrsess['carname'] = $stack[$i]['carname'];
						$newarrsess['image'] = $stack[$i]['image'];
						$newarrsess['rating'] = $stack[$i]['rating'];
						$newarrsess['pref'] = $stack[$i]['pref'];
						$newarrsess['drivername'] = $stack[$i]['drivername'];
						$newarrsess['vFbFriendCount'] = $stack[$i]['vFbFriendCount'];
						$newarrsess['age'] = $stack[$i]['age']; 
						$newarrsess['ridestatus'] = $stack[$i]['ridestatus'];
						$newarrsess['eLadiesOnly'] = $stack[$i]['eLadiesOnly'];
						$newarrsess['eRidePlaceType'] = $stack[$i]['eRidePlaceType']; 
						$newarrsess['price_color'] = $stack[$i]['price_color'];
						$newarrsess['memberrating'] = $stack[$i]['memberrating'];           
						$newarrsess['des'] 					= $stack[$i]['des'];  
						$newarrsess['fDocumentPrice'] 	= $stack[$i]['fDocumentPrice'];
						$newarrsess['docprice'] 			= $stack[$i]['docprice'];  
						$newarrsess['vDocumentWeight'] 	= $stack[$i]['vDocumentWeight']; 
						$newarrsess['fBoxPrice'] 			= $stack[$i]['fBoxPrice']; 
						$newarrsess['boxPrice'] 			= $stack[$i]['boxPrice'];
						$newarrsess['vBoxWeight'] 		= $stack[$i]['vBoxWeight'];
						$newarrsess['fLuggagePrice'] 		= $stack[$i]['fLuggagePrice'];
						$newarrsess['lugprice'] 			= $stack[$i]['lugprice'];	  
						$newarrsess['vLuggageWeight'] 	= $stack[$i]['vLuggageWeight'];
						
						if($comfortfilltering == 1){
							$comfortfillteringdata = 1; 
							}else{
							$comfortfillteringdata = $stack[$i]['carcomfort']; 
						}
						
						if($pricetypefilltering == 1){
							$pricetypefillteringdata = 1; 
							}else{
							$pricetypefillteringdata = $stack[$i]['ePriceType']; 
						}
						
						if($photofilltering == 1){
							$photofillteringdata = 1;
							$stack[$i]['imgavail'] = 1;  
							}else{
							$photofillteringdata = 'Yes'; 
						}
						
						if($ratingfilltering == 'All'){
							$ratingfillteringdata = 'All';
							}else{
							$ratingfillteringdata = $stack[$i]['memberrating'];
						}
						
						if($comfortfilltering == $comfortfillteringdata && $stack[$i]['imgavail'] == $photofillteringdata && $pricetypefilltering == $pricetypefillteringdata && $ratingfilltering == $ratingfillteringdata){
							if($stack[$i]['ridestatus'] == 'Active'){
								array_push($mainarr,$newarrdate);
								array_push($mainsessionarr,$newarrsess);
							}
						}
					}
				}
			}
		}
	} 
	
	function array_sort_ASC_by_column(&$arr, $col, $dir = SORT_ASC) {
		$sort_col = array();
		foreach ($arr as $key=> $row) {
			$sort_col[$key] = $row[$col];
		}    
		array_multisort($sort_col, $dir, $arr); 
	}
	
	function array_sort_DESC_by_column(&$arr, $col, $dir = SORT_DESC) {
		$sort_col = array();
		foreach ($arr as $key=> $row) {
			$sort_col[$key] = $row[$col];
		}    
		array_multisort($sort_col, $dir, $arr); 
	}
	
	//right side sort by
	if($srtby == 0){
		if($ordscheduleby == 'ASC'){
			array_sort_ASC_by_column($mainarr, 'strtotime');
			array_sort_ASC_by_column($mainsessionarr, 'strtotime');
			}else{
			array_sort_DESC_by_column($mainarr, 'strtotime');
			array_sort_DESC_by_column($mainsessionarr, 'strtotime');  
		}
		}else{
		if($ordpriceby == 'ASC'){
			array_sort_ASC_by_column($mainarr, 'pricesort');
			array_sort_ASC_by_column($mainsessionarr, 'pricesort');
			}else{
			array_sort_DESC_by_column($mainarr, 'pricesort');
			array_sort_DESC_by_column($mainsessionarr, 'pricesort');
		}
	}  
	
	#echo "<pre>"; print_r($mainsessionarr); echo "</pre>";
	
	$mainarr = array();
	for($i=0;$i<count($mainsessionarr);$i++){  
		$ladiesimg = '';
		$RidePlaceTypeimg = '';
		if($mainsessionarr[$i]['eLadiesOnly'] == 'Yes'){
			$ladiesimg = '<img src="'.$tconfig['tsite_images'].'ladies.png" title="Ladies Only">';
		}
		
		if($mainsessionarr[$i]['eRidePlaceType'] == 'Airport'){
			$RidePlaceTypeimg = '<img src="'.$tconfig['tsite_images'].'airport.png" title="Airport Ride">';
			}else if($mainsessionarr[$i]['eRidePlaceType'] == 'Shopping'){
			$RidePlaceTypeimg = '<img src="'.$tconfig['tsite_images'].'shopping.png" title="shopping Ride">';
		}
		if($mainsessionarr[$i]['vFbFriendCount']==null)
		{
			$mainsessionarr[$i]['vFbFriendCount']="0";
		}
		$ride_details = '';
		$ride_details .= '<div class="sharing-result-box">                           
		<div class="sharing-result-box-left">
			<h2>'.$generalobj->DateTimeFormat($mainsessionarr[$i]['date']).' - '.$mainsessionarr[$i]['time'].'</h2>
			<h3>'.$mainsessionarr[$i]['mainaddress'].'</h3>   
			<div class="sharing-box-left-left">
				<b>Total</b>                       
				'.$mainsessionarr[$i]['des'].'
			</div>
			<div class="sharing-box-left-right">
				<b>Remain</b>
				<p>Remain Document load :'.$mainsessionarr[$i]['vDocRemain'].'</p>
				<p>Remain Box load :'.$mainsessionarr[$i]['vBoxRemain'].'</p>
				<p>Remain Luggage load :'.$mainsessionarr[$i]['vLuggageRemain'].'</p>
			</div>
		</div>                           
		<div class="sharing-result-box-right">
			<span>
				<img src="'.$mainsessionarr[$i]['image'].'" alt=""/>
				<strong>'.$mainsessionarr[$i]['drivername'].'</strong>
			</span>                       
			'.$mainsessionarr[$i]['rating'].'<span>'.$mainsessionarr[$i]['pref'].'</span>
			<br />'.$mainsessionarr[$i]['vFbFriendCount'].' Facebook Friends
			
		</div>
		</div>';
		
		$newarrarfinal['details'] = $ride_details;
		$mainsessionarr[$i]['pref'] = '';         
		array_push($mainarr,$newarrarfinal);
	} 
	
	$_SESSION['searcharr'] = serialize($mainsessionarr);    
	$smarty->assign("mainarr",$mainarr); 
	$smarty->assign("fromassign",$fromassign);
	$smarty->assign("toassign",$toassign);
	$smarty->assign("searchdate",$_REQUEST['searchdate']); 
	$smarty->assign("totrec",count($mainarr));
	$smarty->assign("ordscheduleby",$ordscheduleby);   
	$smarty->assign("ordpriceby",$ordpriceby); 
	$smarty->assign("srtby",$srtby); 
	$smarty->assign("comfort",$comfort);
	$smarty->assign("pricetype",$pricetype);
	$smarty->assign("fltphoto",$fltphoto);
	$smarty->assign("search",$search);
	$smarty->assign("fltmemrating",$fltmemrating);
	$smarty->assign("eDocument",$eDocument);
	$smarty->assign("eLuggage",$eLuggage);
	$smarty->assign("eBox",$eBox);
	$smarty->assign("From",$From);
	$smarty->assign("To",$To);
	$smarty->assign("sess_vEmail",$_SESSION["sess_vEmail"]);
	$smarty->assign("from_lat",$from_lat);
	$smarty->assign("from_long",$from_long);
	$smarty->assign("to_lat",$to_lat);
	$smarty->assign("to_long",$to_long);   
?>	