<?php
	include_once(TPATH_CLASS_APP."class.customer.php");
	$CustomerObj = new Customer;
	$CustomerObj->check_member_login(); 
	function get_prefrence_img($id, $type){
    global $obj, $tconfig;
    
    $sql = "SELECT iPreferencesId, vTitle".$_SESSION['sess_lang_pref']." as vTitle, ".$type."_Lable".$_SESSION['sess_lang_pref']." as Lable, ".$type." FROM preferences WHERE iPreferencesId = '".$id."' ORDER BY iDispOrder ASC";
    $db_preferences = $obj->MySQLSelect($sql);
   
    if(file_exists($tconfig["tsite_upload_images_preferences_path"].'/'.$db_preferences[0][$type])){
      $img_url = $tconfig["tsite_upload_images_preferences"].$db_preferences[0][$type];
      $img_url = '<img alt="" src="'.$tconfig["tsite_upload_images_preferences"].$db_preferences[0][$type].'" title="'.$db_preferences[0]['Lable'].'">&nbsp;';
        
    }else{
      $img_url = ''; 
    }
    
    return $img_url;
  }
 $iMemberId=$_REQUEST['iMemberId'];
 if($iMemberId!=''){
  $sql = "SELECT eEmailVarified, ePhoneVerified, dAddedDate,tLastLogin FROM member WHERE iMemberId = '".$iMemberId."'";
  $db_email_verification = $obj->MySQLSelect($sql);
  
  if(count($db_email_verification)<=0){
    header("Location:".$tconfig["tsite_url"]."index.php");
    exit;
  }
                                                                                                         
  $sql = "SELECT member_car.iMemberCarId, member_car.iSeats, make.vMake, model.vTitle, member_car.vImage, car_colour.vColour".$_SESSION['sess_lang_pref']." as vColour, member_car.eComfort       
          FROM member_car 
          LEFT JOIN make ON make.iMakeId = member_car.iMakeId 
          LEFT JOIN model ON model.iModelId = member_car.iModelId
          LEFT JOIN car_colour ON car_colour.iColourId=member_car.iColourId            
          WHERE iMemberId = '".$iMemberId."' 
          ORDER BY RAND() 
          LIMIT 0,1";
  $db_car = $obj->MySQLSelect($sql);
  
  $sql = "SELECT count(iRideId) as tot_ride  FROM rides_new WHERE iMemberId = ".$iMemberId;
  $db_tot_rides = $obj->MySQLSelect($sql);
  $tot_rides = $db_tot_rides[0]['tot_ride'];

 
  if(is_file($tconfig["tsite_upload_images_member_path"].$_SESSION['sess_iMemberId']."/1_".$db_car[0]["vImage"]))
  {
    $db_car[0]['img'] = $tconfig["tsite_upload_images_member"].$_SESSION['sess_iMemberId']."/1_".$db_car[0]["vImage"];
  } 
  else
  {
    $db_car[0]['img'] = '';
   
  }
  
  $sql = "SELECT * FROM member_car_preferences WHERE iMemberId = '".$iMemberId."'";
  $db_pref = $obj->MySQLSelect($sql);
  
  $pref = '';
    for($op=0;$op<count($db_pref);$op++){
       $img = get_prefrence_img($db_pref[$op]['iPreferencesId'], $db_pref[$op]['vType']);       
        if($img != ''){
        $pref .= $img;
        }
    }
  
  $sql="select vFirstName,vLastName,vImage,iBirthYear,tDescription,eGender,vFbFriendCount from member where iMemberId='".$iMemberId."'";
  $db_member = $obj->MySQLSelect($sql);
  if($db_member[0]['vFbFriendCount']==null)
  {
	  $db_member[0]['vFbFriendCount']="0";
  }
  if(is_file($tconfig["tsite_upload_images_member_path"].$iMemberId."/2_".$db_member[0]["vImage"]))
  {
    $db_member[0]['img'] = $tconfig["tsite_upload_images_member"].$iMemberId."/2_".$db_member[0]["vImage"];
  } 
  else
  {
   // $db_member[0]['img'] = '';
   if($db_member[0]['eGender']=='Male'){
        $db_member[0]['img'] = $tconfig["tsite_images"].'150x150male.png';
   }else{
        $db_member[0]['img'] = $tconfig["tsite_images"].'150x150female.png';
   }
  }
 
  $y=date('Y'); 
  $member_age=$y-$db_member[0]['iBirthYear']; 
  
      /* rating */
  $sql = "SELECT count(iRateId) as tot_count_rating,SUM(iRate) as tot_sum_rating 
          FROM member_rating WHERE iMemberToId = ".$iMemberId; 
  $db_member_ratings = $obj->MySQLSelect($sql);
  #echo "<pre>"; print_r($db_member_ratings); exit;
  
  $review = $db_member_ratings[0]['tot_sum_rating'];
  $review = $review / $db_member_ratings[0]['tot_count_rating']; 
  $rating_width = ($review * 100) / 5; 
  $rating = round($review);  
  
  for($i=1;$i<=5;$i++)
  {
    $sql = "SELECT count(iRateId) as totrat FROM member_rating WHERE iRate = '".$i."' AND iMemberToId = ".$iMemberId;
    $db_count_rating = $obj->MySQLSelect($sql);
    $totrating[$i] = $db_count_rating[0]['totrat'];
    $totalwidth[$i] = (($totrating[$i]/$db_member_ratings[0]['tot_count_rating'])*100);
  }
  
  /* rating from */
  $sql="select m.vFirstName,m.vLastName,mr.iRate,mr.eSkill,mr.dAddedDate,m.vImage,mr.iMemberFromId,mr.tFeedback from member_rating as mr LEFT JOIN member as m ON m.iMemberId=mr.iMemberFromId where mr.iMemberToId='".$iMemberId."'";
  $db_rating_from = $obj->MySQLSelect($sql);
  for($i=0;$i<count($db_rating_from);$i++){
      
      $db_rating_from[$i]['rating_width'] = ($db_rating_from[$i]['iRate'] * 100) / 5;
      
      if(is_file($tconfig["tsite_upload_images_member_path"].$db_rating_from[$i]['iMemberFromId']."/1_".$db_rating_from[$i]["vImage"]))
      {
        $db_rating_from[$i]['img'] = $tconfig["tsite_upload_images_member"].$db_rating_from[$i]['iMemberFromId']."/1_".$db_rating_from[$i]["vImage"];
      } 
      else
      {
        //$db_rating_from[$i]['img'] = '';
        if($db_rating_from[$i]['eGender']=='Male'){
          $db_rating_from[$i]['img'] = $tconfig["tsite_images"].'64x64male.png';
       }else{
          $db_rating_from[$i]['img'] = $tconfig["tsite_images"].'64x64female.png';
       }
      }
  }
 //echo "<pre>"; print_r($db_rating_from); exit;
 
  if($iMemberId==$_SESSION['sess_iMemberId']){
    $same_member='1';
  }else{
    $same_member='0';
  }
  
 }else{
  	header("Location:".$tconfig["tsite_url"]."index.php");
    exit;
 }
     
 
  $smarty->assign("db_member",$db_member);
  $smarty->assign("tot_rides",$tot_rides);
  $smarty->assign("db_rating_from",$db_rating_from);
  $smarty->assign("db_email_verification",$db_email_verification);
  $smarty->assign("db_car",$db_car);
  $smarty->assign("eChattiness",$eChattiness);
  $smarty->assign("eMusic",$eMusic);
  $smarty->assign("eSmoking",$eSmoking);
  $smarty->assign("eEcig",$eEcig);   
  $smarty->assign("ePets",$ePets);
  $smarty->assign("rating",$rating);
  $smarty->assign("rating_width",$rating_width);     
  $smarty->assign("review",$review);
  $smarty->assign("totrating",$totrating);
  $smarty->assign("totalwidth",$totalwidth);
  $smarty->assign("member_age",$member_age);
  $smarty->assign("same_member",$same_member);
  $smarty->assign("db_pref",$db_pref);
  $smarty->assign("iMemberId",$iMemberId);
  $smarty->assign("megop",$_REQUEST['megop']);
  $smarty->assign("pref",$pref);
  
?>