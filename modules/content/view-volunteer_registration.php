<?php  
  include_once(TPATH_LIBRARIES."/general/validation.class.php");
  $validobj = new validation();    
  
  $validobj->add_fields($_REQUEST['vFirstName_reg'], 'req', 'Please enter Contact Person First Name');
  $validobj->add_fields($_REQUEST['vLastName_reg'], 'req', 'Please enter Contact Person Last Name');
  $validobj->add_fields($_REQUEST['vEmail_reg'], 'req', 'Please enter E-mail');
  $validobj->add_fields($_REQUEST['vPassword_reg'], 'req', 'Please enter Password');  
  
  $error = $validobj->validate();
  if($error){
  	echo 0; exit;
  }
  
  $sql = "SELECT iMemberId FROM member WHERE vEmail = '".$_REQUEST['vEmail_reg']."'";
  $db_member = $obj->MySQLSelect($sql);
  
  if(count($db_member) > 0){
     echo 1; exit;
  }else{
    $Data['vFirstName'] = $_REQUEST['vFirstName_reg'];
    $Data['vLastName'] = $_REQUEST['vLastName_reg'];
    $Data['vEmail'] = $_REQUEST['vEmail_reg'];
    $Data['vPassword'] = $generalobj->encrypt($_REQUEST['vPassword_reg']);
    $Data['eStatus'] = 'Pending';
    $Data['dAddedDate'] = date("Y-m-d H:i:s");
    
    $id = $obj->MySQLQueryPerform("member",$Data,'insert');
    
    if($id){
      $random = substr(number_format(time() * rand(),0,'',''),0,20);      
      $code = $random.$id.strtotime($dt);
      $sql = "UPDATE member set vActivationCode='".$code."' WHERE iMemberId='".$id."' ";
      $obj->sql_query($sql);             
      $Data['activate_account'] = $tconfig["tsite_url"]."index.php?file=c-activation&act=".$code;
      
      $generalobj->send_email_user("VOLUNTEER_REGISTRATION_USER",$Data);        
      echo 2; exit;
    }else{
       echo 3;
       exit;
    }      
  }
  exit;   
?>