<?php    
  // Send an empty HTTP 200 OK response to acknowledge receipt of the notification 
  header('HTTP/1.1 200 OK'); 
   
  // Assign payment notification values to local variables
  $item_name        = $_POST['item_name'];
  $item_number      = $_POST['item_number'];
  $payment_status   = $_POST['payment_status'];
  $payment_amount   = $_POST['mc_gross'];
  $payment_currency = $_POST['mc_currency'];
  $txn_id           = $_POST['txn_id'];
  $receiver_email   = $_POST['receiver_email'];
  $payer_email      = $_POST['payer_email'];
  $custom           = $_POST['custom'];
  
  // Build the required acknowledgement message out of the notification just received
  $req = 'cmd=_notify-validate';               // Add 'cmd=_notify-validate' to beginning of the acknowledgement

  foreach ($_POST as $key => $value) {         // Loop through the notification NV pairs
    $value = urlencode(stripslashes($value));  // Encode these values
    $req  .= "&$key=$value";                   // Add the NV pairs to the acknowledgement
  }
  
  // Set up the acknowledgement request headers
  $header  = "POST /cgi-bin/webscr HTTP/1.1\r\n";                    // HTTP POST request
  $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
  $header .= "Content-Length: " . strlen($req) . "\r\n\r\n";

  // Open a socket for the acknowledgement request
  $fp = fsockopen('ssl://www.sandbox.paypal.com', 443, $errno, $errstr, 30);

  // Send the HTTP POST request back to PayPal for validation
  fputs($fp, $header . $req);
  
  while (!feof($fp)) {                     // While not EOF
    $res = fgets($fp, 1024);               // Get the acknowledgement response
    if (strcmp ($res, "VERIFIED") == 0) {  // Response contains VERIFIED - process notification
      
      if($payment_status == "Completed" || $payment_status == "Pending"){
        $sql = "SELECT * FROM temp_booking WHERE iBookingId = '".$custom."'";
        $db_booking = $obj->MySQLSelect($sql);
        
        $Data['vBookingNo'] = $db_booking[0]['vBookingNo'];
        $Data['iRideId'] = $db_booking[0]['iRideId'];
        $Data['iRidePointId'] = $db_booking[0]['iRidePointId'];
        $Data['vFromPlace'] = $db_booking[0]['vFromPlace'];
        $Data['vToPlace'] = $db_booking[0]['vToPlace'];
        $Data['vMainRidePlaceDetails'] = $db_booking[0]['vMainRidePlaceDetails'];
        $Data['iNoOfSeats'] = $db_booking[0]['iNoOfSeats'];
        $Data['fAmount'] = $db_booking[0]['fAmount'];
        $Data['dBookingDate'] = $db_booking[0]['dBookingDate'];
        $Data['dBookingTime'] = $db_booking[0]['dBookingTime'];
        $Data['iBookerId'] = $db_booking[0]['iBookerId'];
        $Data['iDriverId'] = $db_booking[0]['iDriverId'];
        $Data['vBookerFirstName'] = $db_booking[0]['vBookerFirstName'];
        $Data['vBookerLastName'] = $db_booking[0]['vBookerLastName'];
        $Data['vBookerAddress'] = $db_booking[0]['vBookerAddress'];
        $Data['vBookerCity'] = $db_booking[0]['vBookerCity'];
        $Data['vBookerState'] = $db_booking[0]['vBookerState'];
        $Data['vBookerCountry'] = $db_booking[0]['vBookerCountry'];
        $Data['vBookerZip'] = $db_booking[0]['vBookerZip'];
        $Data['vBookerPhone'] = $db_booking[0]['vBookerPhone'];
        $Data['vBookerEmail'] = $db_booking[0]['vBookerEmail'];
        $Data['vDriverFirstName'] = $db_booking[0]['vDriverFirstName'];
        $Data['vDriverLastName'] = $db_booking[0]['vDriverLastName'];
        $Data['vDriverPhone'] = $db_booking[0]['vDriverPhone'];
        $Data['vDriverEmail'] = $db_booking[0]['vDriverEmail'];
        $Data['vBookerCurrencyCode'] = $db_booking[0]['vBookerCurrencyCode'];
        $Data['vTransactionId'] = $db_booking[0]['vTransactionId'];
        $Data['eBookerPaymentPaid'] = $db_booking[0]['eBookerPaymentPaid'];
        $Data['eDriverPaymentPaid'] = $db_booking[0]['eDriverPaymentPaid'];
        $Data['eBookerConfirmation'] = $db_booking[0]['eBookerConfirmation'];
        $Data['ePaymentType'] = $db_booking[0]['ePaymentType'];
        $Data['eTripReturn'] = $db_booking[0]['eTripReturn'];
        $Data['dPaymentDate'] = $db_booking[0]['dPaymentDate'];
        $Data['eStatus'] = $db_booking[0]['eStatus'];
        $Data['fDocumentPrice'] = $db_booking[0]['fDocumentPrice'];
        $Data['fBoxPrice'] = $db_booking[0]['fBoxPrice'];
        $Data['fLuggagePrice'] = $db_booking[0]['fLuggagePrice'];
        
        $id = $obj->MySQLQueryPerform("booking_new",$Data,'insert');
        
        if($id){
            $mailcont = $ridesobj->email_cont($Data);
            
            $maildata['vBookerEmail'] = $Data['vBookerEmail'];
            $maildata['vDriverEmail'] = $Data['vDriverEmail'];
            $maildata['details'] = $mailcont;
                      
            $generalobj->send_email_user("BOOKING_PASSENGER",$maildata);
            $generalobj->send_email_user("BOOKING_DRIVER",$maildata);
            $generalobj->send_email_user("BOOKING_ADMIN",$maildata);
        }  
      }else{
      
      }         
    }else if (strcmp ($res, "INVALID") == 0) {

    }
  }
  
  fclose($fp);  // Close the file
  exit;
?>