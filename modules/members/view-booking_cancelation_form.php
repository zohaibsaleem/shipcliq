<?php
  include_once(TPATH_CLASS_APP."class.customer.php");
  $CustomerObj = new Customer;
  $CustomerObj->check_member_login();

  $sess_iMemberId = $_SESSION['sess_iMemberId'];
  $iBookingId = $_REQUEST['id'];

  if($iBookingId == ''){
    header("Location:".$tconfig["tsite_url"]."index.php?file=m-mybooking");
    exit;
  }

  $sql = "SELECT * FROM booking_new WHERE iBookingId = '".$iBookingId."'";
  $db_booking_details = $obj->MySQLSelect($sql);

  if(count($db_booking_details) <= 0){
    header("Location:".$tconfig["tsite_url"]."index.php?file=m-mybooking");
    exit;
  }

  if($_REQUEST['action'] == 'cancel_booking'){
    $Data = $_POST['Data'];
    $Data_Payment = $_POST['Data_Payment'];

    if($Data['tCancelReason'] == '' || $_REQUEST['RefundPolicy'] != 'Yes'){
      $msg = LBL_CANCELLATION_REASON;
      header("Location:".$tconfig["tsite_url"]."index.php?file=m-mybooking&err_msg=".$msg);
      exit;
    }

    if($db_booking_details[0]['iBookerId'] == $sess_iMemberId){
      if(PAYMENT_OPTION == 'PayPal'){
        $where = " iMemberId = '".$sess_iMemberId."'";
        $res = $obj->MySQLQueryPerform("member",$Data_Payment,'update',$where);
      }

      $Data['eCancelBy'] = 'Passenger';
      $Data['fDriverRefundAmount'] = $db_booking_details[0]['fAmount'] / 4;
      $Data['fPassengerRefundAmount'] = $db_booking_details[0]['fAmount'] / 2;
      $Data['fSiteRefundAmount'] = $db_booking_details[0]['fAmount'] / 4;
    }else{
      $Data['eCancelBy'] = 'Driver';
      $Data['fPassengerRefundAmount'] = $db_booking_details[0]['fAmount'];
    }

    $Data['dCancelDate'] = date("Y-m-d H:i:s");
    $Data['eStatus'] = 'Cancelled';

    $where = " iBookingId = '".$iBookingId."'";
    $res = $obj->MySQLQueryPerform("booking_new",$Data,'update',$where);

    if($res){
      #Cancelation emails
      if($sess_iMemberId == $db_booking_details[0]['iBookerId']){
        $name = $db_booking_details[0]['vDriverFirstName'].' '.$db_booking_details[0]['vDriverLastName'];
        $enttype = 'Passenger';
        $Cancelledby = $db_booking_details[0]['vBookerFirstName'].' '.$db_booking_details[0]['vBookerLastName'];
        $email = $db_booking_details[0]['vDriverEmail'];
      }else{
        $name = $db_booking_details[0]['vBookerFirstName'].' '.$db_booking_details[0]['vBookerLastName'];
        $enttype = 'Driver';
        $Cancelledby = $db_booking_details[0]['vDriverFirstName'].' '.$db_booking_details[0]['vDriverLastName'];
        $email = $db_booking_details[0]['vBookerEmail'];
      }

      $user_cont .= 'Dear '.$name.',';
      $user_cont .= '<br>';
      $user_cont .= 'This email is just to inform you that your trips booking has been cancelled by <b>'.$enttype.'</b> on ShipCliq.';
      $user_cont .= '<br>';
      $user_cont .= 'Below is cancelled booking details.';
      $user_cont .= '<br>';
      $user_cont .= '<br>';
      $user_cont .= 'Booking No.: #'.$db_booking_details[0]['vBookingNo'];
      $user_cont .= '<br>';
      $user_cont .= 'Booking Date & Time: '.$generalobj->DateTime($db_booking_details[0]['dBookingDate'],14).' @ '.$generalobj->DateTime($db_booking_details[0]['dBookingTime'],18);
      $user_cont .= '<br>';
      $user_cont .= 'Booking From: '.$db_booking_details[0]['vFromPlace'];
      $user_cont .= '<br>';
      $user_cont .= 'Booking To: '.$db_booking_details[0]['vToPlace'];
      $user_cont .= '<br>';
      $user_cont .= 'Driver Name: '.$db_booking_details[0]['vDriverFirstName'].' '.$db_booking_details[0]['vDriverLastName'];
      $user_cont .= '<br>';
      $user_cont .= 'Sender Name: '.$db_booking_details[0]['vBookerFirstName'].' '.$db_booking_details[0]['vBookerLastName'];
      $user_cont .= '<br>';
      $user_cont .= '<br>';
      $user_cont .= 'Please refer to our <a href="'.$tconfig['tsite_url'].'refund-cancellation-policy">Refund & Cancellation Policy</a> to get more details.';

	  $EmailData['email'] = $email;
	  	  $EmailData['ENTTYPE']=$enttype;
	   $EmailData['details'] = $user_cont;
	   $generalobj->send_email_user("CANCELLATION_USER",$EmailData);
      /*$EmailData['email'] = $email;

	  $EmailData['email'] = $email;
      $EmailData['NAME']=$name;
	  $EmailData['ENTTYPE']=$enttype;
	  $EmailData['SITE_NAME']=$SITE_NAME;
	  $EmailData['BOOKINGNO']=$db_booking_details[0]['vBookingNo'];
	  $EmailData['DATE']=$generalobj->DateTime($db_booking_details[0]['dBookingDate'],14);
	  $EmailData['TIME']=$generalobj->DateTime($db_booking_details[0]['dBookingTime'],18);
	  $EmailData['FROMPLACE']=$db_booking_details[0]['vFromPlace'];
	  $EmailData['TOPLACE']=$db_booking_details[0]['vToPlace'];
	  $EmailData['DRIVERFIRSTNAME']=$db_booking_details[0]['vDriverFirstName'];
	  $EmailData['DRIVERLASTNAME']=$db_booking_details[0]['VDriverLastName'];
	  $EmailData['BOOKERFIRSTNAME']=$db_booking_details[0]['vBookerFirstName'];
	  $EmailData['BOOKERLASTNAME']=$db_booking_details[0]['vBookerLastName'];
	  $EmailData['LINK']='<a href="'.$tconfig['tsite_url'].'refund-cancellation-policy">'.LBL_REFUND_CANCEL_POLICY.'</a>';*/

      //$generalobj->send_email_user("CANCELLATION_USER",$EmailData);

      $admin_cont .= 'Dear Administrator,';
      $admin_cont .= '<br>';
      $user_cont .= 'This email is just to inform you that a trips booking has been cancelled by <b>'.$enttype.'</b> on ShipCliq.';
      $admin_cont .= '<br>';
      $admin_cont .= 'Below is cancelled booking details.';
      $admin_cont .= '<br>';
      $admin_cont .= '<br>';
      $admin_cont .= 'Booking No.: #'.$db_booking_details[0]['vBookingNo'];
      $admin_cont .= '<br>';
      $admin_cont .= 'Booking Date & Time: '.$generalobj->DateTime($db_booking_details[0]['dBookingDate'],14).' @ '.$generalobj->DateTime($db_booking_details[0]['dBookingTime'],18);
      $admin_cont .= '<br>';
      $admin_cont .= 'Booking From: '.$db_booking_details[0]['vFromPlace'];
      $admin_cont .= '<br>';
      $admin_cont .= 'Booking To: '.$db_booking_details[0]['vToPlace'];
      $admin_cont .= '<br>';
      $admin_cont .= 'Driver Name: '.$db_booking_details[0]['vDriverFirstName'].' '.$db_booking_details[0]['vDriverLastName'];
      $admin_cont .= '<br>';
      $admin_cont .= 'Sender Name: '.$db_booking_details[0]['vBookerFirstName'].' '.$db_booking_details[0]['vBookerLastName'];

	 $EmailData['ENTTYPE']=$enttype;
      $EmailData['details'] = $admin_cont;

	 /* $EmailData['ENTTYPE']=$enttype;
	  $EmailData['SITE_NAME']=$SITE_NAME;
	  $EmailData['BOOKINGNO']=$db_booking_details[0]['vBookingNo'];
	  $EmailData['DATE']=$generalobj->DateTime($db_booking_details[0]['dBookingDate'],14);
	  $EmailData['TIME']=$generalobj->DateTime($db_booking_details[0]['dBookingTime'],18);
	  $EmailData['FROMPLACE']=$db_booking_details[0]['vFromPlace'];
	  $EmailData['TOPLACE']=$db_booking_details[0]['vToPlace'];
	  $EmailData['DRIVERFIRSTNAME']=$db_booking_details[0]['vDriverFirstName'];
	  $EmailData['DRIVERLASTNAME']=$db_booking_details[0]['VDriverLastName'];
	  $EmailData['BOOKERFIRSTNAME']=$db_booking_details[0]['vBookerFirstName'];
	  $EmailData['BOOKERLASTNAME']=$db_booking_details[0]['vBookerLastName'];*/

      $generalobj->send_email_user("CANCELLATION_ADMIN",$EmailData);

      $msg = LBL_CANCEL_BOOKING_SUCC;
      header("Location:".$tconfig["tsite_url"]."index.php?file=m-mybooking&var_msg=".$msg);
      exit;
    }else{
      $msg = LBL_SOMETHING_WRONG;
      header("Location:".$tconfig["tsite_url"]."index.php?file=m-mybooking&err_msg=".$msg);
      exit;
    }
    exit;
  }


  if($sess_iMemberId == $db_booking_details[0]['iBookerId']){
    $db_booking_details[0]['enttype'] = 'Passenger';

    $sql = "SELECT vPaymentEmail, vBankAccountHolderName, vAccountNumber, vBankLocation, vBankName, vBIC_SWIFT_Code FROM member WHERE iMemberId = '".$sess_iMemberId."'";
    $db_member = $obj->MySQLSelect($sql);
  }else{
    $db_booking_details[0]['enttype'] = 'Driver';
  }

 $smarty->assign("db_booking_details",$db_booking_details);
 $smarty->assign("iBookingId",$iBookingId);
 $smarty->assign("err_msg",$_REQUEST['err_msg']);
 $smarty->assign("var_msg",$_REQUEST['var_msg']);
 $smarty->assign("db_member",$db_member);
?>
