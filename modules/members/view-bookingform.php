<?php 
	$previous_link = $_SERVER['HTTP_REFERER']; 		
	include_once(TPATH_CLASS_APP."class.customer.php");
	$CustomerObj = new Customer;
	
	$CustomerObj->check_member_login();    
	$sess_iMemberId = $_SESSION["sess_iMemberId"];
	
	$sql = "SELECT eEmailVarified,eLicenseStatus FROM member WHERE iMemberId = '".$sess_iMemberId."'";
	$db_mailvarification = $obj->MySQLSelect($sql);
	
	if($db_mailvarification[0]['eEmailVarified'] == 'No'){
		header("Location:".$tconfig["tsite_url"]."index.php?file=m-varification_notification");
		exit;  
	}
	if($LICENSE_VERIFICATION_REQUIRED == 'Yes'){
		  if($db_mailvarification[0]['eLicenseStatus'] == 'Pending' || $db_mailvarification[0]['eLicenseStatus'] == 'Unapproved'){
			header("Location:".$tconfig["tsite_url"]."index.php?file=m-verification&msg_code=0&var_msg=You did not complete your member verifications.");
			exit;
		  }
	}
	
	$id = $_REQUEST['id'];
	$main_arr = unserialize($_SESSION['searcharr']);
	#echo "<pre>"; print_r($main_arr); exit;
	
	$iRideId = $main_arr[$id]['iRideId'];
	$RemainWeight = $generalobj->parcel_count($iRideId);
	$start_iRidePointId = $main_arr[$id]['start_iRidePointId']; 
	$end_iRidePointId = $main_arr[$id]['end_iRidePointId'];  
	$return = $main_arr[$id]['return'];
	$date = $main_arr[$id]['date'];
	$strtotime = $main_arr[$id]['strtotime'];
	$main_address = strip_tags($main_arr[$id]['mainaddress']);  
	$time = $main_arr[$id]['time'];
	$price = $main_arr[$id]['price'];
	$seats = $main_arr[$id]['seats'];
	
	$sql = "SELECT * FROM rides_new WHERE iRideId = '".$iRideId."'";
	$db_ride = $obj->MySQLSelect($sql);
	#echo "<pre>"; print_r($db_ride); exit;
	if($db_ride[0]['iMemberId'] == $sess_iMemberId)
	{
		header('Location: '.$previous_link);
	}
	$sql = "SELECT * FROM ride_points_new WHERE iRideId = '".$iRideId."' ORDER BY iRidePointId ASC";
	$db_point_data = $obj->MySQLSelect($sql);
	
	$rb = range($start_iRidePointId, $end_iRidePointId);
	
	$places_str = $main_address;
	$price = 0;  
	$originalprice = 0;   
	for($j=0;$j<count($db_point_data);$j++){    
		if($db_point_data[$j]['iRidePointId'] == $start_iRidePointId){
			$startpoint = $db_point_data[$j]['vStartPoint'];
		} 
		if($db_point_data[$j]['iRidePointId'] == $end_iRidePointId){
			$endpoint = $db_point_data[$j]['vEndPoint'];
		}
		if (in_array($db_point_data[$j]['iRidePointId'], $rb)){
			$price = $price + $db_point_data[$j]['fPrice'];
			//$price = $price + $db_point_data[$j]['fTotPrice'];
			$originalprice = $originalprice + $db_point_data[$j]['fOriginalPrice'];                        
		}        
	}    
	#echo "<pre>";print_r($db_point_data);exit;
	$price = $price * $db_ride[0]['fRatio_'.$_SESSION['sess_price_ratio']];
	
	$sess_price_ratio = isset($_SESSION['sess_price_ratio'])?$_SESSION['sess_price_ratio']:CURR_MASTER_DEF_CURR;
	$fDocumentPrice = ($db_ride[0]['eDocument'] == 'Yes')?$db_ride[0]['fDocumentPrice']:0;
	$vDocumentWeight = ($db_ride[0]['eDocument'] == 'Yes')?$db_ride[0]['vDocumentWeight']:'';
	$vDocumentUnit = ($db_ride[0]['eDocument'] == 'Yes')?$db_ride[0]['vDocumentUnit']:'';
	$fDocumentPrice *=  $db_ride[0]['fRatio_'.$sess_price_ratio];
	$fBoxPrice = ($db_ride[0]['eBox'] == 'Yes')?$db_ride[0]['fBoxPrice']:0;
	$vBoxWeight = ($db_ride[0]['eBox'] == 'Yes')?$db_ride[0]['vBoxWeight']:'';
	$vBoxUnit = ($db_ride[0]['eBox'] == 'Yes')?$db_ride[0]['vBoxUnit']:'';
	$fBoxPrice *=  $db_ride[0]['fRatio_'.$sess_price_ratio];
	$fLuggagePrice = ($db_ride[0]['eLuggage'] == 'Yes')?$db_ride[0]['fLuggagePrice']:0;
	$vLuggageWeight = ($db_ride[0]['eLuggage'] == 'Yes')?$db_ride[0]['vLuggageWeight']:'';
	$vLuggageUnit = ($db_ride[0]['eLuggage'] == 'Yes')?$db_ride[0]['vLuggageUnit']:'';
	$fLuggagePrice *=  $db_ride[0]['fRatio_'.$sess_price_ratio];
	
	//$price = $fDocumentPrice + $fBoxPrice + $fLuggagePrice; //as per site requirement
	$price = 0;
	
	//$fCommission = $db_point_data[0]['fSiteFixFee']+(($price*$db_point_data[0]['fCommission'])/100);
	$fCommission=	$db_point_data[0]['fCommission'];
	$fSiteFixFee=$db_point_data[0]['fSiteFixFee'];
	$vatprice    = $price+$fCommission;  
	$vatprice=$db_point_data[0]['fVat'];
	//$vatprice    = ($vatprice*$db_point_data[0]['fVat'])/100;
	
	$totprice    = $price+$fCommission+$vatprice;
	
	$originalprice = $originalprice * $db_ride[0]['fRatio_'.$sess_price_ratio]; 
	$price_diff = $price - $originalprice;
	$price_diff = abs($price_diff);
	
	if($price_diff > 2 && $price > $originalprice){
		$price_color = '#FF9802';
		}else if($price_diff > 2 && $price < $originalprice){
		$price_color = '#68BE57';
		}else{
		$price_color = '#000000';
	}      
	//echo $fCommission;exit;
	$price = $generalobj->booking_currency($price, $sess_price_ratio);
	$fCommission = $generalobj->booking_currency($fCommission, $sess_price_ratio);
	$vatprice = $generalobj->booking_currency($vatprice, $sess_price_ratio);
	$totprice = $generalobj->booking_currency($totprice, $sess_price_ratio);
	$stack = array();
	//echo $fCommission;exit;
	$sql = "SELECT vFirstName, vLastName, vImage, vNickName, eGender, iBirthYear, eEmailVarified, ePhoneVerified FROM member WHERE iMemberId = '".$db_ride[0]['iMemberId']."'";
	$db_member = $obj->MySQLSelect($sql);  
	
	//$stack[0]['drivername'] = $db_member[0]['vFirstName'].' '.$db_member[0]['vLastName'];
	if($db_member[0]['vNickName'] != ""){
		$stack[0]['drivername'] = $db_member[0]['vNickName'];  
		}else{
		$stack[0]['drivername'] = $db_member[0]['vFirstName'].' '.$db_member[0]['vLastName'];
	}
	
		$stack[0]['age'] = date('Y') - $db_member[0]['iBirthYear'];
	
	$stack[0]['eEmailVarified'] = $db_member[0]['eEmailVarified'];
	$stack[0]['ePhoneVerified'] = $db_member[0]['ePhoneVerified'];
	
	if(is_file($tconfig["tsite_upload_images_member_path"].$db_ride[0]['iMemberId']."/1_".$db_member[0]['vImage']))
	{
		$stack[0]['image'] = $tconfig["tsite_upload_images_member"].$db_ride[0]['iMemberId']."/1_".$db_member[0]['vImage'];
	} 
	else
	{
		$stack[0]['image'] = '';
	}
	
	$sql = "SELECT * FROM member_car_preferences WHERE iMemberId = '".$db_ride[0]['iMemberId']."'";
	$db_pref = $obj->MySQLSelect($sql);
	
	$sql = "SELECT iMemberCarId, eComfort , vImage, model.vTitle as model, make.vMake as make 
	FROM member_car 
	LEFT JOIN model ON model.iModelId = member_car.iModelId 
	LEFT JOIN make ON make.iMakeId = member_car.iMakeId 
	WHERE iMemberCarId = '".$db_ride[0]['iMemberCarId']."'";
	$db_car_details = $obj->MySQLSelect($sql);
	
	if(is_file($tconfig["tsite_upload_images_member_path"].$db_ride[0]['iMemberId']."/1_".$db_car_details[0]["vImage"]))
	{
		$stack[0]['car_image'] = $tconfig["tsite_upload_images_member"].$db_ride[0]['iMemberId']."/1_".$db_car_details[0]["vImage"];
	} 
	else
	{
		$stack[0]['car_image'] = '';
	} 
	
	$stack[0]['carname'] = $db_car_details[0]['model'].', '.$db_car_details[0]['make'];
	$stack[0]['carcomfort'] = $db_car_details[0]['eComfort'];         
    
	if($db_pref[0]['eChattiness'] == 'YES'){
		$eChattiness = $tconfig['tsite_images'].'chat1.png';
	}
	if($db_pref[0]['eChattiness'] == 'MAYBE'){
		$eChattiness = $tconfig['tsite_images'].'chat2.png';
	}
	if($db_pref[0]['eChattiness'] == 'NO'){
		$eChattiness = $tconfig['tsite_images'].'chat3.png';
	}
	
	if($db_pref[0]['eMusic'] == 'YES'){
		$eMusic = $tconfig['tsite_images'].'music1.png';
	}
	if($db_pref[0]['eMusic'] == 'MAYBE'){
		$eMusic = $tconfig['tsite_images'].'music2.png';
	}
	if($db_pref[0]['eMusic'] == 'NO'){
		$eMusic = $tconfig['tsite_images'].'music3.png';
	}
	
	
	if($db_pref[0]['eSmoking'] == 'YES'){
		$eSmoking = $tconfig['tsite_images'].'smoke1.png';
	}
	if($db_pref[0]['eSmoking'] == 'MAYBE'){
		$eSmoking = $tconfig['tsite_images'].'smoke2.png';
	}
	if($db_pref[0]['eSmoking'] == 'NO'){
		$eSmoking = $tconfig['tsite_images'].'smoke3.png';
	}
	
	if($db_pref[0]['eEcig'] == 'YES'){
		$eEcig = $tconfig['tsite_images'].'ecig1.png';
	}
	if($db_pref[0]['eEcig'] == 'MAYBE'){
		$eEcig = $tconfig['tsite_images'].'ecig2.png';
	}
	if($db_pref[0]['eEcig'] == 'NO'){
		$eEcig = $tconfig['tsite_images'].'ecig3.png';
	}
	
	if($db_pref[0]['ePets'] == 'YES'){
		$ePets = $tconfig['tsite_images'].'pets1.png';
	}
	if($db_pref[0]['ePets'] == 'MAYBE'){
		$ePets = $tconfig['tsite_images'].'pets2.png';
	}
	if($db_pref[0]['ePets'] == 'NO'){
		$ePets = $tconfig['tsite_images'].'pets3.png';
	}
	
	$stack[0]['pref']  = ($eChattiness!='')?'<img alt="" src="'.$eChattiness.'" style="width:12%;">&nbsp;':'';
	$stack[0]['pref'] .= ($eMusic!='')?'<img alt="" src="'.$eMusic.'" style="width:12%;">&nbsp;':'';
	$stack[0]['pref'] .= ($eSmoking!='')?'<img alt="" src="'.$eSmoking.'" style="width:12%;">&nbsp;':'';
	$stack[0]['pref'] .= ($eEcig!='')?'<img alt="" src="'.$eEcig.'" style="width:12%;">&nbsp;':'';
	$stack[0]['pref'] .= ($ePets!='')?'<img alt="" src="'.$ePets.'" style="width:12%;">&nbsp;':'';
	
	
	$sql = "SELECT count(iRateId) as tot_count_rating,SUM(iRate) as tot_sum_rating 
	FROM member_rating WHERE iMemberToId = ".$db_ride[0]['iMemberId']; 
	$db_member_ratings = $obj->MySQLSelect($sql);
	
	$review = $db_member_ratings[0]['tot_sum_rating'];
	$review = $review / $db_member_ratings[0]['tot_count_rating'];     
	$review = round($review);
	
	$rating_width = ($review * 100) / 5; 
	$rating_width = '<span style="display: block; width: 65px; height: 13px; background: url('.$tconfig['tsite_images'].'star-rating-sprite.png) 0 0;">
	<span style="display: block; width: '.$rating_width.'%; height: 13px; background: url('.$tconfig['tsite_images'].'star-rating-sprite.png) 0 -13px;"></span>
	</span>&nbsp;&nbsp;'.$review.' '.LBL_RATING;
	$stack[0]['rating'] = $rating_width;
	
	$drop .= '<select name="" id="" style="border: 1px solid #CCCCCC;margin: 0 3px;padding: 2px;" onChange="change_seats(this.value);">';
	for($i=1;$i<= $seats;$i++){
		$drop .= '<option>'.$i.'</option>';
	}
	$drop .= '</select>';
	
	$ride_details = '';
	$ride_details .= '<div class="sharing-result-box">                           
	<div class="sharing-result-box-left">
	<h2>'.$generalobj->DateTime($date, 9).' - '.$time.'</h2>
	<h3>'.$main_address.'</h3><span style="width:50%;">';
	
	#echo "<pre>"; print_r($RemainWeight); exit;
	foreach($RemainWeight as $key => $val) {
		if($key == 'vDoc')
		{
			$ride_details .= ($fDocumentPrice and $val > 0)?'<p><input type="checkbox" name="DocumentPrice" id="DocumentPrice" value="'.$fDocumentPrice.'" onchange="showtag()">
			<input type="hidden" name="vDocPrice" id="vDocPrice" value="'.$fDocumentPrice.'">
			'.LBL_DOCUMENT.' '.$generalobj->booking_currency($fDocumentPrice, $sess_price_ratio).' ('.$val.'/'.$vDocumentWeight.') '.$vDocumentUnit.'</p>':'<p>'.LBL_DOCUMENT.' '.$generalobj->booking_currency($fDocumentPrice, $sess_price_ratio).' ('.$val.'/'.$vDocumentWeight.') '.$vDocumentUnit.'</p>';
			
			$ride_details .= ($val > 0)?'<p style="display:none" id="bookhideshow"><input type="hidden" name="'.$key.'" id="'.$key.'" value="'.$val.'"> '.LBL_PIECES.' <input type="text" name="'.$key.'_val" id="'.$key.'_val" value="" onblur="booking_alert(\''.$key.'\')"></p><p>You can book Maximum '.$val.' '.$vDocumentUnit.'</p>':'';
		}
	}
	
	
	foreach($RemainWeight as $key => $val) {
		if($key == 'vBox')
		{
			$ride_details .= ($val > 0)?'<p><input type="checkbox" name="BoxPrice" id="BoxPrice" value="" onchange="showtag()"><input type="hidden" name="vBoxPrice" id="vBoxPrice" value="'.$fBoxPrice.'" ">'.LBL_BOX.' '.$generalobj->booking_currency($fBoxPrice, $sess_price_ratio).' ('.$val.'/'.$vBoxWeight.') '.$vBoxUnit.'</p>':'<p>'.LBL_BOX.' '.$generalobj->booking_currency($fBoxPrice, $sess_price_ratio).' ('.$val.'/'.$vBoxWeight.') '.$vBoxUnit.'</p>';
			
			$ride_details .= ($val > 0)?'<p style="display:none" id="boxhideshow"><input type="hidden" name="'.$key.'" id="'.$key.'" value="'.$val.'"> '.LBL_PIECES.' <input type="text" name="'.$key.'_val" id="'.$key.'_val" value="" onblur="booking_alert(\''.$key.'\')"></p><p>You can book Maximum '.$val.' '.$vBoxUnit.'</p>':'';
		}
	}
	
	
	foreach($RemainWeight as $key => $val) {
		if($key == 'vLuggage')
		{
			$ride_details .= ($fLuggagePrice > 0 and $val > 0)?'<p><input type="checkbox" name="LuggagePrice" id="LuggagePrice" value="'.$fLuggagePrice.'" onchange="showtag()"><input type="hidden" name="vLuggagePrice" id="vLuggagePrice" value="'.$fLuggagePrice.'" >'.LBL_LUGGAGE.' '.$generalobj->booking_currency($fLuggagePrice, $sess_price_ratio).' ('.$val.'/'.$vLuggageWeight.') '.$vLuggageUnit.'</p>':'<p>'.LBL_LUGGAGE.' '.$generalobj->booking_currency($fLuggagePrice, $sess_price_ratio).' ('.$val.'/'.$vLuggageWeight.') '.$vLuggageUnit.'</p>';
			
			$ride_details .= ($val > 0)?'<p style="display:none" id="luggagehideshow"><input type="hidden" name="'.$key.'" id="'.$key.'" value="'.$val.'"> '.LBL_PIECES.' <input type="text" name="'.$key.'_val" id="'.$key.'_val" value="" onblur="booking_alert(\''.$key.'\')"></p><p>You can book Maximum '.$val.' '.$vLuggageUnit.'</p>':'';
		}
	}
	
$ride_details .= '</span><span style="width:50%">
<table border="0" width="100%" class="booking-price">
	<tr><td width="50%">'.LBL_DRIVERS_FEE.'</td>
		<td width="10%">:</td>
		<td width="40%" id="ride_price">'.$price.'</td>
	</tr>
	<tr><td width="50%">'.LBL_PLATFORM_FEE.'</td>
		<td width="10%">:</td>
		<td width="40%" id="commision_price">'.$fCommission.'</td>
	</tr>
	<tr><td width="50%">'.LBL_VAT.'</td>
		<td width="10%">:</td>
		<td width="40%" id="vat_price">'.$vatprice.'</td>
	</tr>
	<tr><td width="50%">'.LBL_GRAND_TOTAL.'</td>
		<td width="10%">:</td>
		<td width="40%" id="tot_price">'.$totprice.'</td>
	</tr>
</table>
</span>
</div>                           
<div class="sharing-result-box-right"><span><img src="'.$stack[0]['image'].'" alt="" style="width:40%;"/><strong>'.$stack[0]['drivername'].'</strong><br />';
if($db_member[0]['iBirthYear']!=0){$ride_details .=$stack[0]['age'].' '.LBL_YEARS_OLD;}$ride_details .='</span>'.$stack[0]['rating'].'<span>'.$stack[0]['pref'].'</span> 
</div>
</div>';
/*$ride_details .= '<div class="sharing-result-box">                           
	<div class="sharing-result-box-left">
	<h2>'.$generalobj->DateTime($date, 9).' - '.$time.'</h2>
	<h3>'.$main_address.'</h3>
	<span><strong id="seatprice" style="color:'.$price_color.';">'.$price.'</strong>'.LBL_PER_PESSENGER1.'</span> 
	<span><strong class="yel">'.$drop.'</strong>'.LBL_SEATS_LEFT.'</span>
	<span style="width:45%">
	<!--<strong id="tot_price">'.$price.'</strong>Tot. Price-->
	<table border="0" width="100%" class="booking-price">
	<tr><td width="50%">'.LBL_DRIVERS_FEE.'</td>
	<td width="10%">:</td>
	<td width="40%" id="ride_price">'.$price.'</td>
	</tr>
	<tr><td width="50%">'.LBL_PLATFORM_FEE.'</td>
	<td width="10%">:</td>
	<td width="40%" id="commision_price">'.$fCommission.'</td>
	</tr>
	<tr><td width="50%">'.LBL_VAT.'</td>
	<td width="10%">:</td>
	<td width="40%" id="vat_price">'.$vatprice.'</td>
	</tr>
	<tr><td width="50%">'.LBL_GRAND_TOTAL.'</td>
	<td width="10%">:</td>
	<td width="40%" id="tot_price">'.$totprice.'</td>
	</tr>
	</table>
	</span>
	<p>'.LBL_CAR.' : <em>'.$stack[0]['carname'].'</em>  '.$stack[0]['carcomfort'].'</p>
	</div>                           
	<div class="sharing-result-box-right"><span><img src="'.$stack[0]['image'].'" alt="" style="width:40%;"/><strong>'.$stack[0]['drivername'].'</strong><br />
	'.$stack[0]['age'].' '.LBL_YEARS_OLD.'</span>'.$stack[0]['rating'].'<span>'.$stack[0]['pref'].'</span> 
	</div>
	</div>';
*/
$sql = "SELECT * FROM member WHERE iMemberId = '".$sess_iMemberId."'";
$db_booker_details = $obj->MySQLSelect($sql); 

$sql = "SELECT * FROM country WHERE eStatus = 'Active' ORDER BY vCountry ASC";
$db_country = $obj->MySQLSelect($sql);


$smarty->assign("mainprice",$price);
$price = explode(' ', $price);
$smarty->assign("totprice",$totprice);
$totprice = explode(' ', $totprice);
$smarty->assign("totamount",$totprice[1]);
$smarty->assign("commisionprice",$fCommission);
$smarty->assign("vatprice",$vatprice);
$smarty->assign("ride_details",$ride_details);
$smarty->assign("stack",$stack);
$smarty->assign("db_booker_details",$db_booker_details);
$smarty->assign("db_country",$db_country);
$smarty->assign("id",$id);
$smarty->assign("price",$price);
$smarty->assign("pointsid",implode(',',$rb));
$smarty->assign("startpoint",$startpoint);
$smarty->assign("endpoint",$endpoint);
$smarty->assign("price",$price[1]);  
$smarty->assign("fDocumentPrice",$fDocumentPrice);  
$smarty->assign("vDocumentWeight",$vDocumentWeight);  
$smarty->assign("vDocumentUnit",$vDocumentUnit);  
$smarty->assign("fBoxPrice",$fBoxPrice);  
$smarty->assign("vBoxWeight",$vBoxWeight);  
$smarty->assign("vBoxUnit",$vBoxUnit);  
$smarty->assign("fLuggagePrice",$fLuggagePrice);  
$smarty->assign("vLuggageWeight",$vLuggageWeight);  
$smarty->assign("vLuggageUnit",$vLuggageUnit);  
$fCommission = explode(' ', $fCommission);
$smarty->assign("fCommission",$fCommission[1]);
$vatprice = explode(' ', $vatprice);
$smarty->assign("vat",$vatprice[1]);
$smarty->assign("fSiteFixFee",$fSiteFixFee);
    
?>
