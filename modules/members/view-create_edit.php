<?php  
  include_once(TPATH_CLASS_APP."class.customer.php");
  $CustomerObj = new Customer;
  $CustomerObj->check_member_login();   
  $sess_iMemberId = $_SESSION["sess_iMemberId"];
  
  if($_REQUEST['for'] == 'first'){
    $iRideId = $_REQUEST['id'];
    
    $sql = "SELECT iBookingId FROM booking_new WHERE iRideId = '".$iRideId."'";
    $db_check_booking = $obj->MySQLSelect($sql);
    
    if(count($db_check_booking) > 0){
      $msg = LBL_BOOKING_AVAIL;
	    header("Location:".$tconfig["tsite_url"]."index.php?file=m-list_rides_offer&var_err_msg=".$msg);
	    exit;
    }
  
    $sql = "SELECT * FROM rides_new WHERE iRideId = '".$iRideId."'";
    $db_ride = $obj->MySQLSelect($sql);
    
    $sql = "SELECT * FROM ride_points_new WHERE iRideId = '".$iRideId."' AND eReverse = 'No' ORDER BY iRidePointId ASC";
    $db_ride_points = $obj->MySQLSelect($sql);   
   
    $newarr = array();
    
    $newarr['from'] = $db_ride[0]['vMainDeparture'];
    $newarr['to'] = $db_ride[0]['vMainArrival'];
    
    for($i=0;$i<count($db_ride_points);$i++){
      if($i != 0){
        $newarr['loc'.$i] = $db_ride_points[$i]['vStartPoint'];
      }
    }
    
    if($db_ride[0]['eRideType'] == 'One Time'){
      $newarr['triptype'] = 'onetime';
      $newarr['sdateone'] = $db_ride[0]['dDepartureDate'];
      $time = array();
      $time = explode(':', $db_ride[0]['vMainDepartureTime']);
      $newarr['onetihourstart'] = $time[0];
      $newarr['onetimeminstart'] = $time[1];
      
      if($db_ride[0]['eRoundTrip'] == 'No'){
        $newarr['roundtriponetime'] = 'No';        
      }else{
        $newarr['roundtriponetime'] = 'Yes';
        $newarr['edateone'] = $db_ride[0]['dArrivalDate'];
        $time = array();
        $time = explode(':', $db_ride[0]['vMainArrivalTime']);
        $newarr['onetihourend'] = $time[0];
        $newarr['onetimeminend'] = $time[1];
      }
    }else{
      $newarr['triptype'] = 'recurring';
      $newarr['sdate'] = $db_ride[0]['dStartDate'];
      $newarr['edate'] = $db_ride[0]['dEndDate'];
      $time = array();
      $time = explode(':', $db_ride[0]['vMainOutBoundTime']);
      $newarr['richourstart'] = $time[0];
      $newarr['ricminstart'] = $time[1];      
      $newarr['outbound'] = explode(",", $db_ride[0]['vOutBoundDays']);
      if($db_ride[0]['eRoundTrip'] == 'No'){
        $newarr['roundtripric'] = 'No';             
      }else{
        $newarr['roundtripric'] = 'Yes';
        $time = array();
        $time = explode(':', $db_ride[0]['vMainReturnTime']);
        $newarr['richourend'] = $time[0];
        $newarr['ricminend'] = $time[1];
        $newarr['retturn'] = explode(",", $db_ride[0]['vReturnDays']); 
      }
    }
    $_SESSION['sess_price_ratio'] = $db_ride[0]['vBookerCurrencyCode'];
    $newarr['second_page'] = 'Yes';
    $newarr['iRideId'] = $iRideId;
    
    $_SESSION['ride_offer'] = '';
    $_SESSION['ride_offer'] = $newarr;     
    header("Location:".$tconfig["tsite_url"]."index.php?file=m-offer_ride_edit");
  	exit;
  }else{

  }     
  exit;
?>