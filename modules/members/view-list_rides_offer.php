<?php     
  include_once(TPATH_CLASS_APP."class.customer.php");
  $CustomerObj = new Customer;
  $CustomerObj->check_member_login();
  
  $sess_iMemberId = $_SESSION['sess_iMemberId'];  
  
  if($_REQUEST['action'] == 'delete'){
    if($_REQUEST['id'] != ''){
      $sql = "SELECT iBookingId FROM booking_new WHERE iRideId = '".$_REQUEST['id']."'";
      $db_ride_booking = $obj->MySQLSelect($sql);
      
      if(COUNT($db_ride_booking) > 0){
        $msg = LBL_CANT_DELTE_RIDE1." ".COUNT($db_ride_booking)." ".LBL_CANT_DELTE_RIDE2;
        header("Location:".$tconfig["tsite_url"]."index.php?file=m-list_rides_offer&var_err_msg=".$msg);
        exit;
      }else{              
        $sql = "UPDATE rides_new SET eStatus = 'Deleted' WHERE iRideId = '".$_REQUEST['id']."' AND iMemberId = '".$sess_iMemberId."'";
        $db_sql=$obj->sql_query($sql);
        
        if($db_sql){
          $msg = "Your trip is successfully deleted.";
          header("Location:".$tconfig["tsite_url"]."index.php?file=m-list_rides_offer&var_msg=".$msg);
          exit;
        }else{
          $msg = "Something goes wrong. Please try again.";
          header("Location:".$tconfig["tsite_url"]."index.php?file=m-list_rides_offer&var_err_msg=".$msg);
          exit;
        }  
      }
    }
  }
  
  $avail_count=0;
  $past_count=0;
  if($sess_iMemberId!=''){   
      $sql="select * from rides_new where iMemberId='".$sess_iMemberId."' and eStatus != 'Deleted'";
      $db_ride_list = $obj->MySQLSelect($sql); 
      
      $CurrDate = date("Y-m-d");
      $past = 0;
      $avail = 0;
      for($i=0;$i<count($db_ride_list);$i++)
      {
         $sql = "SELECT dDateOut, dDateRet FROM ride_dates WHERE iRideId = '".$db_ride_list[$i]['iRideId']."' ORDER BY iRideDateId DESC LIMIT 0,1";
         $db_ride_date = $obj->MySQLSelect($sql); 
         
         if($db_ride_date[0]['dDateRet'] != '0000-00-00'){
          if(strtotime($CurrDate) <= strtotime($db_ride_date[0]['dDateRet'])) {
            $db_ride_list[$i]['type'] = 'Available';
            $avail++;
          }else{
            $db_ride_list[$i]['type'] = 'Past';
            $past++;
          }
         }else{
          if(strtotime($CurrDate) <= strtotime($db_ride_date[0]['dDateOut'])) {
            $db_ride_list[$i]['type'] = 'Available';
            $avail++;
          }else{
            $db_ride_list[$i]['type'] = 'Past';
            $past++;
          }
         }
         
         $sql = "SELECT vStartPoint, vEndPoint, fPrice FROM ride_points_new WHERE iRideId = '".$db_ride_list[$i]['iRideId']."' AND eReverse = 'No' ORDER BY iRidePointId ASC";
         $db_ride_points = $obj->MySQLSelect($sql);
        
         $price = 0; 
         $pointstr = '';
         for($j=0;$j<count($db_ride_points);$j++){
          
          if(count($db_ride_points) > 1){
            if($j > 0){
             $pointstr .= '&nbsp;'.$db_ride_points[$j]['vStartPoint'].'&nbsp;&rarr;';
            }   
          }                   
          $price = $price + $db_ride_points[$j]['fPrice'];            
         }
         $db_ride_list[$i]['stop_over'] = substr($pointstr,0,-7);
         $db_ride_list[$i]['totprice'] = $generalobj->booking_currency($price,$db_ride_list[$i]['vBookerCurrencyCode']);                  
      }         
  }
  
  $smarty->assign("db_ride_list", $db_ride_list);
  $smarty->assign("db_make", $db_make);
  $smarty->assign("db_type", $db_type);
  $smarty->assign("db_color", $db_color);
  $smarty->assign("iMemberId", $_SESSION['sess_iMemberId']);
  $smarty->assign("img", $img);
  $smarty->assign("avail_count", $avail);
  $smarty->assign("past_count", $past); 
  $smarty->assign("var_msg", $_REQUEST['var_msg']);
  $smarty->assign("var_err_msg", $_REQUEST['var_err_msg']);  
?>