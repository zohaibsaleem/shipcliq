<?php
	include_once(TPATH_CLASS_APP."class.customer.php");
	$CustomerObj = new Customer;
	$CustomerObj->check_member_login();
	$sess_iMemberId = $_SESSION['sess_iMemberId'];
	
	$iMessageId = $_REQUEST['iMessageId'];

	if($iMessageId == ''){
    header("Location: ".$tconfig["tsite_url"]."index.php?file=m-logout");
    exit;
  }

	if($iMessageId !=''){
    if($_REQUEST['from'] == 'received'){
      $sql = "SELECT iThreadId, iFromMemberId FROM messages WHERE iToMemberId = '".$sess_iMemberId."' AND iMessageId = '".$iMessageId."'";
      $db_mess_iThreadId = $obj->MySQLSelect($sql);
    }else{
      $sql = "SELECT iThreadId, iFromMemberId FROM messages WHERE iFromMemberId = '".$sess_iMemberId."' AND iMessageId = '".$iMessageId."'";
      $db_mess_iThreadId = $obj->MySQLSelect($sql);
    }
    
    if(count($db_mess_iThreadId) <= 0){
      header("Location: ".$tconfig["tsite_url"]."index.php?file=m-logout");
      exit;
    }
    
    if($db_mess_iThreadId[0]['iThreadId'] != 0){
      if($_REQUEST['from'] == 'received'){
        $sql = "UPDATE messages SET eStatus='Read' WHERE iThreadId = '".$db_mess_iThreadId[0]['iThreadId']."' AND iToMemberId = '".$sess_iMemberId."'";
  		  $obj->sql_query($sql);
      }
  		
      $sql = "SELECT iMessageId, iReplyId, iThreadId, iFromMemberId, iToMemberId, tMessage, messages.dAddedDate , vFirstName, vLastName, vImage    
              FROM messages 
              LEFT JOIN member ON member.iMemberId = messages.iFromMemberId  
              WHERE iThreadId = '".$db_mess_iThreadId[0]['iThreadId']."' 
              ORDER BY dAddedDate DESC";
      $db_messages = $obj->MySQLSelect($sql); 
    }else{
      if($_REQUEST['from'] == 'received'){
        $sql = "UPDATE messages SET eStatus='Read' WHERE iMessageId = '".$iMessageId."'";
  		  $obj->sql_query($sql);
      }
  		
      $sql = "SELECT iMessageId, iReplyId, iThreadId, iFromMemberId, iToMemberId, tMessage, messages.dAddedDate , vFirstName, vLastName, vImage  
              FROM messages 
              LEFT JOIN member ON member.iMemberId = messages.iFromMemberId  
              WHERE iMessageId = '".$iMessageId."' 
              ORDER BY dAddedDate DESC";
      $db_messages = $obj->MySQLSelect($sql); 
    }
  }
  
  for($i=0;$i<count($db_messages);$i++)
  {
    if(is_file($tconfig["tsite_upload_images_member_path"].$db_messages[$i]['iFromMemberId']."/1_".$db_messages[$i]['vImage']))
    {
      $db_messages[$i]['img'] = $tconfig["tsite_upload_images_member"].$db_messages[$i]['iFromMemberId']."/1_".$db_messages[$i]['vImage'];
    } 
    else
    {
      $db_messages[$i]['img'] = $tconfig["tsite_images"].'user-man-54.png';
    } 
  }  
  
  
  if($db_mess_iThreadId[0]['iThreadId'] == 0){
    $iThreadId = $iMessageId;
  }else{
    $iThreadId = $db_mess_iThreadId[0]['iThreadId'];
  }   
  
  $sql = "SELECT messages.iMessageId, messages.iReplyId, messages.iThreadId, messages.iFromMemberId, messages.tMessage, messages.dAddedDate, messages.eStatus, member.vFirstName, member.vLastName, member.vImage      
          FROM messages 
          LEFT JOIN member ON member.iMemberId = messages.iFromMemberId   
          WHERE messages.iToMemberId = '".$sess_iMemberId."' 
          AND messages.eStatus = 'Unread'";
  $db_received_all = $obj->MySQLSelect($sql);  
    
  $smarty->assign("iMessageId",$iMessageId);
  $smarty->assign("db_messages",$db_messages);  
  $smarty->assign("iThreadId",$iThreadId); 
  $smarty->assign("iFromMemberId",$db_mess_iThreadId[0]['iFromMemberId']);  
  $smarty->assign("from",$_REQUEST['from']);
  $smarty->assign("tot_unread",count($db_received_all));   
?>