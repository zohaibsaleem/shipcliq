<?php
	include_once(TPATH_CLASS_APP."class.customer.php");
	$CustomerObj = new Customer;
	$CustomerObj->check_member_login();
	$action=$_REQUEST['action'];
	$sess_iMemberId=$_SESSION['sess_iMemberId'];
	//echo "<pre>"; print_r($_POST); exit;
	if($action == 'send_equest')
	{
		$iBookingId = $_POST['iBookingId'];
		//echo "<pre>";print_r($iBookingId);
		if(is_array($iBookingId))
		{
			$iBookingId = @implode(",",$iBookingId);
		}
		$sql="select iBookingId,vBookingNo,vBookerCurrencyCode,vMainRidePlaceDetails,vLuggageWeight,fLuggagePrice,vBoxWeight,fBoxPrice,vDocumentWeight,fDocumentPrice,fAmount,dBookingDate,iBookerId,iDriverId,fCommission,fVat from booking_new where iBookingId IN (".$iBookingId.")";
		$db_ride = $obj->MySQLSelect($sql);
		$data = array('eTransRequest'=>'Yes');
		$where = " iBookingId IN (".$iBookingId.")";
		$res = $obj->MySQLQueryPerform("booking_new",$data,'update',$where);
		$sql="select vFirstName,vLastName,vEmail from member where iMemberId=$sess_iMemberId";
		$db_name = $obj->MySQLSelect($sql);
		$total1=0;

		for($i=0;$i<count($db_ride);$i++)
		{
			$db_ride[$i]['fAmount']=($db_ride[$i]['vLuggageWeight']*$db_ride[$i]['fLuggagePrice'])+($db_ride[$i]['vBoxWeight']*$db_ride[$i]['fBoxPrice'])+($db_ride[$i]['vDocumentWeight']*$db_ride[$i]['fDocumentPrice']);
			$total1=$total1+$db_ride[$i]['fAmount'];
		}
		if($res)
		{
			$Data['subject']="Money Transfer Request";
			$mail_body='<p>Dear Administrator</p>
				<p>'.$db_name[0]['vFirstName']." ".$db_name[0]['vLastName'].' has submitted request for transfer.</p>
				<p>Below is the details.</p>
				<p>Customer Name :: '.$db_name[0]['vFirstName']." ".$db_name[0]['vLastName'].'</p>
				<p>E-mail :: '.$db_name[0]['vEmail'].'</p>';
				$total=0;
			for($i=0;$i<count($db_ride);$i++)
			{
				$mail_body.='<p>--------------------------------------------</p><p>Booking No. :: '.$db_ride[$i]['vBookingNo'].'</p>
					<p>Detail :: '.$db_ride[$i]['vMainRidePlaceDetails'].'</p>
					<p>Booking Date :: '.$db_ride[$i]['dBookingDate'].'</p>
					<p>Amount :: '.$db_ride[$i]['fAmount'].' '.$db_ride[$i]['vBookerCurrencyCode'].'</p>';
			}
			$mail_body.='<p>Total Amount:: '.$total1.' '.$db_ride[0]['vBookerCurrencyCode'].'</p>';
			$message = '<table cellpadding="6" cellspacing="0" border="0" width="100%">
							<tbody>
								<tr>
									<td>&nbsp;</td>
								</tr>
							</tbody>
						</table>
						<table width="600" align="center" border="0">
							<tbody>
								<tr >
								  <td style=" border:2px solid #e4e4e4;  border-radius: 5px 5px 5px 5px;"><table cellspacing="5" cellpadding="5" width="100%" border="0">
									  <tbody>
										<tr>
										  <td style="FONT-SIZE: 11px"><font face="Verdana" color="#333333">'.$mail_body.'</font></td>
										</tr>
									  </tbody>
									</table></td>
								</tr>
							  </tbody>
						</table>';
			$Data['message']= $message;
			$generalobj->send_email_user("TRANSFER_REQUEST",$Data);
		}
		$var_msg = "Your money transfer request has been sent. ";
		header("Location: ".$tconfig["tsite_url"]."index.php?file=m-money_detail&type=available&var_msg=".$var_msg);
		exit;
	}
	$type=$_REQUEST['type'];
	if($sess_iMemberId!='')
	{
		$type=$_REQUEST['type'];
		if($type == 'available')
		{
			$sql="select iBookingId,vBookingNo,vMainRidePlaceDetails,vBookerCurrencyCode,fAmount,vLuggageWeight,fLuggagePrice,vBoxWeight,fBoxPrice,vDocumentWeight,fDocumentPrice,dBookingDate,iBookerId,iDriverId,eStatus,fCommission,fVat,eDriverPaymentPaid,eTransRequest from booking_new where eStatus='Active' and eDriverPaymentPaid='No' and eBookerConfirmation='Yes' and iDriverId=$sess_iMemberId";
			$db_money = $obj->MySQLSelect($sql);
			$total=0;
			for($i=0;$i<count($db_money);$i++)
			{
				if($i%2 == 0)
				{
					$db_money[$i]['trstyle'] = "style='background:#fff'";
				}
				else if($i%2 != 0)
				{
					$db_money[$i]['trstyle'] = "style='background:#efefef'";
				}
				$db_money[$i]['fAmount']=($db_money[$i]['vLuggageWeight']*$db_money[$i]['fLuggagePrice'])+($db_money[$i]['vBoxWeight']*$db_money[$i]['fBoxPrice'])+($db_money[$i]['vDocumentWeight']*$db_money[$i]['fDocumentPrice']);
				$total=$total+$db_money[$i]['fAmount'];
			}
		}
		if($type == 'history')
		{
			$sql="select iBookingId,vBookingNo,vMainRidePlaceDetails,fAmount,vLuggageWeight,fLuggagePrice,vBoxWeight,fBoxPrice,vDocumentWeight,fDocumentPrice,dBookingDate,iBookerId,iDriverId,fCommission,fVat,eStatus,eDriverPaymentPaid,eTransRequest from booking_new where eStatus='Active' and eDriverPaymentPaid='Yes' and iDriverId=$sess_iMemberId";
			$db_money = $obj->MySQLSelect($sql);
			$total=0;
			for($i=0;$i<count($db_money);$i++)
			{
				if($i%2 == 0)
				{
					$db_money[$i]['trstyle'] = "style='background:#fff'";
				}
				else if($i%2 != 0)
				{
					$db_money[$i]['trstyle'] = "style='background:#efefef'";
				}
				$db_money[$i]['fAmount']=($db_money[$i]['vLuggageWeight']*$db_money[$i]['fLuggagePrice'])+($db_money[$i]['vBoxWeight']*$db_money[$i]['fBoxPrice'])+($db_money[$i]['vDocumentWeight']*$db_money[$i]['fDocumentPrice']);
				$total=$total+$db_money[$i]['fAmount'];
			}
		}
		if($type == 'refund')
		{
			$sql="select iBookingId,vBookingNo,vMainRidePlaceDetails,fAmount,vLuggageWeight,fLuggagePrice,vBoxWeight,fBoxPrice,vDocumentWeight,fDocumentPrice,dBookingDate,iBookerId,iDriverId,eStatus,fCommission,fVat,eDriverPaymentPaid,eTransRequest from booking_new where eStatus='Active' and eDriverRefundPaid='Yes' and iDriverId=$sess_iMemberId";
			$db_money = $obj->MySQLSelect($sql);
			$total=0;
			for($i=0;$i<count($db_money);$i++)
			{
				if($i%2 == 0)
				{
					$db_money[$i]['trstyle'] = "style='background:#fff'";
				}
				else if($i%2 != 0)
				{
					$db_money[$i]['trstyle'] = "style='background:#efefef'";
				}
				$db_money[$i]['fAmount']=($db_money[$i]['vLuggageWeight']*$db_money[$i]['fLuggagePrice'])+($db_money[$i]['vBoxWeight']*$db_money[$i]['fBoxPrice'])+($db_money[$i]['vDocumentWeight']*$db_money[$i]['fDocumentPrice']);
				$total=$total+$db_money[$i]['fAmount'];
			}
		}
		# Code for Booking Cancel #
		$action = $_REQUEST['action'];
	}
	else
	{
		header("Location:".$tconfig["tsite_url"]."index.php?file=c-login");
		exit;
	}

	$smarty->assign("type", $type);
	$smarty->assign("BookDate", $BookDate);
	$smarty->assign("db_money", $db_money);
	$smarty->assign("iMemberId", $_SESSION['sess_iMemberId']);
	$smarty->assign("page_link",$page_link);
	$smarty->assign("var_msg",$_REQUEST['var_msg']);
	$smarty->assign("total", $total);
?>
