<?php
	include_once(TPATH_CLASS_APP."class.customer.php");
	$CustomerObj = new Customer;
	$CustomerObj->check_member_login();

	$sess_iMemberId=$_SESSION['sess_iMemberId'];
	# echo "<pre>"; print_r($_SESSION); exit;

	if($sess_iMemberId!=''){
		$Member=$_REQUEST['Member'];
		$BookDate=$_REQUEST['BookDate'];
		$Date = Date('Y-m-d');
		$Curr_Date = Date('Y-m-d H:i:s');
		$Cancel_time = Date('Y-m-d H:i:s', strtotime('-1 hour'));

		if($Member=="Booker" && $Member!=''){
			$ssql.=" and iBookerId='".$sess_iMemberId."'";
			}else{
			$ssql.=" and iDriverId='".$sess_iMemberId."'";
			$Member="Driver";
		}

		if(isset($BookDate) && $BookDate=="Past"){
			$ssql.=" and dBookingDate < '".$Date."' and eStatus='Active' ";
			$ord= " order by dBookingDate DESC";
			}else if(isset($BookDate) && $BookDate=="Futur"){
			$ssql.=" and dBookingDate > '".$Date."' and eStatus='Active' ";
			$ord= " order by dBookingDate ASC";
			}else if(isset($BookDate) && $BookDate=="Today"){
			$ssql.=" and dBookingDate = '".$Date."' and eStatus='Active' ";
			$ord= " order by dBookingDate DESC";
			$BookDate='Today';
			}else if(isset($BookDate) && $BookDate=="Canc"){
			$ssql.="and eStatus='Cancelled' ";
			$ord= " order by dBookingDate DESC";
			}else{
			//$ssql.=" and eStatus='Active'";
			$ssql.="";
			$ord= " order by dBookingDate DESC";
			$BookDate='All';
		}


		$sql="select iBookingId,vBookingNo,iRideId,vFromPlace,vToPlace,vMainRidePlaceDetails,iNoOfSeats,dBookingDate,dBookingTime,iBookerId,iDriverId,vBookerFirstName,vBookerLastName,vBookerPhone,vBookerEmail,eBookerPaymentPaid,eStatus,eDriverPaymentPaid,fAmount,vDriverFirstName,vDriverLastName,vDriverPhone,vDriverEmail,vBookerCurrencyCode,eCancelBy,tCancelreason,dCancelDate,eBookerConfirmation from booking_new where 1 $ssql $ord";
		$db_ride_list_all = $obj->MySQLSelect($sql);
		#echo "<pre>";print_r($db_ride_list);exit;

		$num_totrec = count($db_ride_list_all);
    	$rec_limit = $ACC_REC_LIMIT;
		include(TPATH_LIBRARIES."/general/front.paging.inc.php");
    	if(!isset($start))
    	$start = 1;
    	$num_limit = ($start-1)*$rec_limit;
    	$startrec = $num_limit;
    	$lastrec = $startrec + $rec_limit;
    	$startrec = $startrec + 1;
    	if($lastrec > $num_totrec){
			$lastrec = $num_totrec;
		}
    	if($num_totrec > 0 )
    	{
    		$recmsg = "Showing ".$startrec." - ".$lastrec." Product(s) Of ".$num_totrec;
		}

    	$sql="select iBookingId,vBookingNo,iRideId,vFromPlace,vToPlace,vMainRidePlaceDetails,iNoOfSeats,dBookingDate,dBookingTime,iBookerId,iDriverId,vBookerFirstName,vBookerLastName,vBookerPhone,vBookerEmail,eBookerPaymentPaid,eStatus,eDriverPaymentPaid,fAmount,vDriverFirstName,vDriverLastName,vDriverPhone,vDriverEmail,vBookerCurrencyCode,eCancelBy,tCancelreason,dCancelDate,eBookerConfirmation,fDocumentPrice,fBoxPrice,fLuggagePrice,vSmsCode ,vVerificationCode, vDocumentWeight,vBoxWeight,vLuggageWeight, vDocumentUnit, vBoxUnit, vLuggageUnit from booking_new where 1 $ssql $ord $var_limit";
		$db_ride_list = $obj->MySQLSelect($sql);

		for($i=0;$i<count($db_ride_list);$i++){
			//$db_ride_list[$i]['vMainRidePlaceDetails']=str_replace("<strong>","", $db_ride_list[$i]['vMainRidePlaceDetails']);
			//$db_ride_list[$i]['vMainRidePlaceDetails']=str_replace("</strong>","", $db_ride_list[$i]['vMainRidePlaceDetails']);

			$db_ride_list[$i]['vMainRidePlaceDetails']=strip_tags($db_ride_list[$i]['vMainRidePlaceDetails']);

			if(strtotime($db_ride_list[$i]['dBookingDate'] . " " . $db_ride_list[$i]['dBookingTime']) >= strtotime($Curr_Date))
			{
				$db_ride_list[$i]['bookingtype']  = 'Avail';
			}
			else
			{
				$db_ride_list[$i]['bookingtype']  = 'Past';
			}

			if(strtotime($db_ride_list[$i]['dBookingDate'] . " " . $db_ride_list[$i]['dBookingTime']) >= strtotime($Cancel_time))
			{
				$db_ride_list[$i]['cancelbutton']  = 'Yes';
			}
		}

		# Code for Booking Cancel #
		$action = $_REQUEST['action'];
		if($action == "cancelbooking"){

			$iBookingId = $_REQUEST['ibookid'];
			$Data['eStatus'] = "Cancelled";
			$Data['eCancelBy'] = $_REQUEST['eCancelBy'];
			$Data['tCancelreason'] = $_REQUEST['tCancelreason'];
			$Data['dCancelDate'] = date("Y-m-d H:i:s");
			$where = " iBookingId = '".$iBookingId."'";
			$res = $obj->MySQLQueryPerform("booking_new",$Data,'update',$where);
			if($res){
				if($_REQUEST['eCancelBy'] == "Driver"){
					$sql="select iBookingId,vBookingNo,vFromPlace,vToPlace,dBookingDate,dBookingTime,iBookerId,iDriverId,vBookerFirstName,vBookerLastName,vBookerPhone,vBookerEmail,eStatus,vDriverFirstName,vDriverLastName,vDriverPhone,vDriverEmail,tCancelreason,dCancelDate from booking_new where iBookingId = '".$iBookingId."' ";
					$db_mail_passenger = $obj->MySQLSelect($sql);
					$Data_Mail_Passenger['vBookerEmail'] = $db_mail_passenger[0]['vBookerEmail'];
					$Data_Mail_Passenger['vBookerName']  = $db_mail_passenger[0]['vBookerFirstName']." ".$db_mail_passenger[0]['vBookerLastName'];
					$Data_Mail_Passenger['vBookingNo']   = $db_mail_passenger[0]['vBookingNo'];
					$Data_Mail_Passenger['vFromPlace']   = $db_mail_passenger[0]['vFromPlace'];
					$Data_Mail_Passenger['vToPlace']     = $db_mail_passenger[0]['vToPlace'];
					$Data_Mail_Passenger['tCancelreason']     = $db_mail_passenger[0]['tCancelreason'];
					//$Data_Mail_Passenger['dBookingDate'] = $generalobj->DateTime($db_mail_passenger[0]['dBookingDate'],14)."@".$generalobj->DateTime($db_mail_passenger[0]['dBookingTime'],18);
					$Data_Mail_Passenger['dBookingDate'] = $generalobj->DateTimeFormat($db_mail_passenger[0]['dBookingDate'])."@".$generalobj->DateTime($db_mail_passenger[0]['dBookingTime'],18);
					$Data_Mail_Passenger['vDriverName']  = $db_mail_passenger[0]['vDriverFirstName']." ".$db_mail_passenger[0]['vDriverLastName'];
					$generalobj->send_email_user("DRIVER_CANCEL_BOOKING_TO_PASSENGER",$Data_Mail_Passenger);
				}
				if($_REQUEST['eCancelBy'] == "Booker"){
					$sql="select iBookingId,vBookingNo,vFromPlace,vToPlace,dBookingDate,dBookingTime,iBookerId,iDriverId,vBookerFirstName,vBookerLastName,vBookerPhone,vBookerEmail,eStatus,vDriverFirstName,vDriverLastName,vDriverPhone,vDriverEmail,tCancelreason,dCancelDate from booking_new where iBookingId = '".$iBookingId."' ";
					$db_mail_driver = $obj->MySQLSelect($sql);
					$Data_Mail_Driver['vDriverEmail'] = $db_mail_driver[0]['vDriverEmail'];
					$Data_Mail_Driver['vDriverName']  = $db_mail_driver[0]['vDriverFirstName']." ".$db_mail_driver[0]['vDriverLastName'];
					$Data_Mail_Driver['vBookingNo']   = $db_mail_driver[0]['vBookingNo'];
					$Data_Mail_Driver['vFromPlace']   = $db_mail_driver[0]['vFromPlace'];
					$Data_Mail_Driver['vToPlace']     = $db_mail_driver[0]['vToPlace'];
					$Data_Mail_Driver['tCancelreason']= $db_mail_driver[0]['tCancelreason'];
					//$Data_Mail_Driver['dBookingDate'] = $generalobj->DateTime($db_mail_driver[0]['dBookingDate'],14)."@".$generalobj->DateTime($db_mail_driver[0]['dBookingTime'],18);
					$Data_Mail_Driver['dBookingDate'] = $generalobj->DateTimeFormat($db_mail_driver[0]['dBookingDate'])."@".$generalobj->DateTime($db_mail_driver[0]['dBookingTime'],18);
					$Data_Mail_Driver['vBookerName']  = $db_mail_driver[0]['vBookerFirstName']." ".$db_mail_driver[0]['vBookerLastName'];
					$generalobj->send_email_user("PASSENGER_CANCEL_BOOKING_TO_DRIVER",$Data_Mail_Driver);
				}
				$msg = LBL_CANCEL_BOOKING_SUCC;
				header("Location: ".$tconfig["tsite_url"]."index.php?file=m-mybooking&var_msg=".$msg."&msg_code=1");
				exit;
				}else{
				$msg = LBL_ERROR_BOOKING;
				header("Location: ".$tconfig["tsite_url"]."index.php?file=m-mybooking&var_msg=".$msg."&msg_code=0");
				exit;
			}
		}
		# Code for Booking Cancel  ends#

		# Code For validate ride by booker #
		if($action == "validbookerride"){

			$iBookingId = isset($_REQUEST['bookingid'])?$_REQUEST['bookingid']:'';
			$vCodeDriver = isset($_REQUEST['vCode'])?$_REQUEST['vCode']:'';

			if($iBookingId != '') {
				$sql="select vVerificationCode from booking_new where iBookingId = '".$iBookingId."' ";
				$db_Booking = $obj->MySQLSelect($sql);

				$vVerificationCode = isset($db_Booking[0]['vVerificationCode'])?$db_Booking[0]['vVerificationCode']:'';

				if($vVerificationCode == $vCodeDriver){
					$sql = "UPDATE booking_new  SET eBookerConfirmation = 'Yes' WHERE iBookingId = '".$iBookingId."'";
					$db_sql=$obj->sql_query($sql);
					if($db_sql){
						$msg = LBL_RIDE_SUCCESS;
						header("Location: ".$tconfig["tsite_url"]."index.php?file=m-mybooking&var_msg=".$msg."&msg_code=1");
						exit;
						}else{
						$msg = LBL_ERROR_RIDE;
						header("Location: ".$tconfig["tsite_url"]."index.php?file=m-mybooking&var_msg=".$msg."&msg_code=0");
						exit;
					}
					}else{
					$msg = LBL_ERROR_RIDE;
					header("Location: ".$tconfig["tsite_url"]."index.php?file=m-mybooking&var_msg=".$msg."&msg_code=0");
					exit;
				}
				}else{
				$msg = LBL_ERROR_RIDE;
				header("Location: ".$tconfig["tsite_url"]."index.php?file=m-mybooking&var_msg=".$msg."&msg_code=0");
				exit;
			}

		}
		# Code For validate ride by booker ends#
	}
	else{
		header("Location:".$tconfig["tsite_url"]."index.php?file=c-login");
		exit;
	}

	$smarty->assign("Member", $Member);
	$smarty->assign("BookDate", $BookDate);
	$smarty->assign("db_ride_list", $db_ride_list);
	$smarty->assign("iMemberId", $_SESSION['sess_iMemberId']);
	$smarty->assign("page_link",$page_link);
	$smarty->assign("var_msg",$_REQUEST['var_msg']);

?>
