<?php  
  $mainlatlongfor_countrycode = explode('|', $_REQUEST['start_lat_0']);
  $latitude_forcountrycode = $mainlatlongfor_countrycode[0];
  $longitude_forcountrycode = $mainlatlongfor_countrycode[1];
  
  $country_code = $ridesobj->get_user_country($latitude_forcountrycode, $longitude_forcountrycode);

  if($country_code == ''){
  
  }
  
  $mainlatlongfor_tocountrycode = explode('|', $_REQUEST['vCountryCodeTo']);
  $latitude_fortocountrycode = $mainlatlongfor_tocountrycode[0];
  $longitude_fortocountrycode = $mainlatlongfor_tocountrycode[1];
  $to_country_code = $ridesobj->get_user_country($latitude_fortocountrycode, $longitude_fortocountrycode);

  function getDatesFromRange($startDate, $endDate)
  {
    $return = array($startDate);
    $start = $startDate;
    $i=1;
    if (strtotime($startDate) < strtotime($endDate))
    {
       while (strtotime($start) < strtotime($endDate))
        {
            $start = date('Y-m-d', strtotime($startDate.'+'.$i.' days'));
            $return[] = $start;
            $i++;
        }
    } 
    return $return;
  }
  
  function in_multiarray($elem, $array, $field)
  {
    $yes = 'Yes';
    $no = 'No';
    $top = sizeof($array) - 1;
    $bottom = 0;
    while($bottom <= $top)
    {
        if($array[$bottom][$field] == $elem)
            return $yes."|".$array[$bottom]['type'];
        else 
            if(is_array($array[$bottom][$field]))
                if(in_multiarray($elem, ($array[$bottom][$field])))
                    return $yes."|".$array[$bottom]['type'];

        $bottom++;
    }        
    return $no."|";
  }   
    
  $triptype = $_SESSION['ride_offer']['triptype'];
  
  ###### insert ride data start ###### 
  $Data['iMemberId'] = $_SESSION['sess_iMemberId'];
  $Data['vMainDeparture'] = $_SESSION['ride_offer']['from'];
  $Data['vMainArrival'] = $_SESSION['ride_offer']['to'];
  
  #reccuring start
  if($triptype == 'recurring'){
    $mailtriptype = 'Reccuring';
    $mailfromdate = '<span style="color:#000;">From Date:</span> '.$generalobj->DateTime($_SESSION['ride_offer']['sdate'],10).'&nbsp;&nbsp;';
    $mailtodate = '<span style="color:#000;">To Date:</span> '.$generalobj->DateTime($_SESSION['ride_offer']['edate'],10).'&nbsp;&nbsp;';
    
    $Data['eRideType'] = 'Reccuring';
    $Data['eRoundTrip'] = $_SESSION['ride_offer']['roundtripric'];
    $Data['dStartDate'] = $_SESSION['ride_offer']['sdate'];
    $Data['dEndDate'] = $_SESSION['ride_offer']['edate'];
    $Data['vOutBoundDays'] = implode(',', $_SESSION['ride_offer']['outbound']);
    $Data['vMainOutBoundTime'] = $_SESSION['ride_offer']['richourstart'].":".  $_SESSION['ride_offer']['ricminstart'].":00";
    if($_SESSION['ride_offer']['roundtripric'] == 'Yes'){          
      $Data['vReturnDays'] = implode(',', $_SESSION['ride_offer']['retturn']);  
      $Data['vMainReturnTime'] = $_SESSION['ride_offer']['richourend'].":".  $_SESSION['ride_offer']['ricminend'].":00";
      
      $mailfromtime = '<span style="color:#000;">Outbound Time:</span> '.$generalobj->DateTime($Data['vMainOutBoundTime'],12).'&nbsp;&nbsp;';
      $mailtotime = '<span style="color:#000;">Return Time:</span> '.$generalobj->DateTime($Data['vMainReturnTime'],12).'&nbsp;&nbsp;';
    }else{
      $mailfromtime = '<span style="color:#000;">Outbound Time:</span> '.$generalobj->DateTime($Data['vMainOutBoundTime'],12).'&nbsp;&nbsp;';
    }    
  }#reccuring end
  
  #onetime start
  if($triptype == 'onetime'){
    $mailtriptype = 'One Time';
    
    if($_SESSION['ride_offer']['roundtriponetime'] == 'Yes'){
      $mailfromdate = '<span style="color:#000;">From Date:</span> '.$generalobj->DateTime($_SESSION['ride_offer']['sdateone'],10).'&nbsp;&nbsp;';
      $mailtodate = '<span style="color:#000;">To Date:</span> '.$generalobj->DateTime($_SESSION['ride_offer']['edateone'],10).'&nbsp;&nbsp;';
    }else{
      $mailfromdate = '<span style="color:#000;">Date:</span> '.$generalobj->DateTime($_SESSION['ride_offer']['sdateone'],10).'&nbsp;&nbsp;';
    }
    
    
    $Data['eRideType'] = 'One Time';
    $Data['eRoundTrip'] = $_SESSION['ride_offer']['roundtriponetime'];
    $Data['dDepartureDate'] = $_SESSION['ride_offer']['sdateone'];
    $Data['vMainDepartureTime'] = $_SESSION['ride_offer']['onetihourstart'].":".  $_SESSION['ride_offer']['onetimeminstart'].":00";
    if($_SESSION['ride_offer']['roundtriponetime'] == 'Yes'){
      $Data['dArrivalDate'] = $_SESSION['ride_offer']['edateone'];         
      $Data['vMainArrivalTime'] = $_SESSION['ride_offer']['onetihourend'].":".  $_SESSION['ride_offer']['onetimeminend'].":00";
      
      $mailfromtime = '<span style="color:#000;">Outbound Time:</span> '.$generalobj->DateTime($Data['vMainDepartureTime'],12).'&nbsp;&nbsp;';
      $mailtotime = '<span style="color:#000;">Return Time:</span> '.$generalobj->DateTime($Data['vMainArrivalTime'],12).'&nbsp;&nbsp;';
    }else{
      $mailfromtime = '<span style="color:#000;">Outbound Time:</span> '.$generalobj->DateTime($Data['vMainDepartureTime'],12).'&nbsp;&nbsp;';
    }         
  }#onetime end
  
  $Data['dAddedDate'] = date("Y-m-d H:i:s"); 
  $Data['vMainDistance'] = $_SESSION['ride_offer']['distance'];
  $Data['vMainTimeDuration'] = $_SESSION['ride_offer']['duration'];
  $Data['iMemberCarId'] = $_REQUEST['iMemberCarId'];
  $Data['iSeats'] = $_REQUEST['seats'];   
  $Data['tDetails'] = $generalobj->replace_phone_email_url($_REQUEST['tDetails']); 
  $Data['eLuggageSize'] = $_REQUEST['luggage_size'];
  $Data['eLeaveTime'] = $_REQUEST['leave_time']; 
  $Data['eWaitTime'] = $_REQUEST['wait_time'];
  $Data['eLadiesOnly'] = $_REQUEST['eLadiesOnly'];
  $Data['eRidePlaceType']  = $_REQUEST['eRidePlaceType'];
  $Data['eDocument']  = (isset($_REQUEST['fDocumentPrice']) && $_REQUEST['fDocumentPrice'] >0)?'Yes':'No';
  $Data['fDocumentPrice']  = $_REQUEST['fDocumentPrice'];
  $Data['vDocumentWeight']  = $_REQUEST['vDocumentWeight'];
  $Data['eBox']  = (isset($_REQUEST['fBoxPrice']) && $_REQUEST['fBoxPrice'] >0)?'Yes':'No';  
  $Data['fBoxPrice']  = $_REQUEST['fBoxPrice'];
  $Data['vBoxWeight']  = $_REQUEST['vBoxWeight'];
  $Data['eLuggage']  = (isset($_REQUEST['fLuggagePrice'])&& $_REQUEST['fLuggagePrice'] >0)?'Yes':'No';
  $Data['fLuggagePrice']  = $_REQUEST['fLuggagePrice'];
  $Data['vLuggageWeight']  = $_REQUEST['vLuggageWeight'];
  $Data['eStatus'] = 'Active';
  $Data['fCommissionRate'] = $SITE_COMMISSION;
  
  
  $sql = "SELECT * FROM currency WHERE vName = '".$_SESSION['sess_price_ratio']."'";
  $db_currency = $obj->MySQLSelect($sql);
  
  /*$Data['fRatio_GBP'] = $db_currency[0]['GBP'];
  $Data['fRatio_NOK'] = $db_currency[0]['NOK'];
  $Data['fRatio_SEK'] = $db_currency[0]['SEK'];
  $Data['fRatio_DKK'] = $db_currency[0]['DKK'];
  $Data['fRatio_PLN'] = $db_currency[0]['PLN'];
  $Data['fRatio_RUB'] = $db_currency[0]['RUB'];
  $Data['fRatio_EUR'] = $db_currency[0]['EUR'];
  $Data['fRatio_USD'] = $db_currency[0]['USD'];*/
  
  $db_curr_mst = unserialize(db_curr_mst);
  for($i=0;$i<count($db_curr_mst);$i++){
    $currmst = $db_curr_mst[$i]['vName'];
    $Data['fRatio_'.$currmst] = $db_currency[0][$currmst];  
  }
  
  $Data['vBookerCurrencyCode'] = $_SESSION['sess_price_ratio'];
  $Data['vCountryCode'] = $country_code;
  $Data['vCountryCodeTo'] = $to_country_code;
  
  $iRideId = $_REQUEST['iRideId'];
	$where = " iRideId = '".$iRideId."'";   

	$ride_id = $obj->MySQLQueryPerform("rides_new",$Data,'update',$where);  
  ###### insert ride data end ###### 
  $ride_id = $iRideId;
  if($ride_id){
    $sql="Delete from ride_dates where iRideId = '".$ride_id."'"; 
	  $db_sql=$obj->sql_query($sql);
	  
	  $sql="Delete from ride_points_new where iRideId = '".$ride_id."'"; 
	  $db_sql=$obj->sql_query($sql);
    
    $mailprice = 0;
    ###### insert ride point data start ###### 
    for($i=0;$i<20;$i++){
      $Data_ride_point = array();
      if($_REQUEST['points_'.$i] != ''){       
        if($i != 0){
          $abc = $i - 1; 
          $dur = explode(' ',$_REQUEST['duration_'.$abc]);
          if (strpos($_REQUEST['duration_'.$abc],'hour')) {
              $dur = explode(' ',$_REQUEST['duration_'.$abc]);
              $minutes = $minutes + ($dur[0]*60) + $dur[2];
          }else{
              $dur = explode(' ',$_REQUEST['duration_'.$abc]);
              $minutes = $minutes + $dur[0];
          }
          $input_time = date("H:i", strtotime($pointtime)+($minutes*60)).':00';        
        }else{
          if($triptype == 'recurring'){
            $pointtime = $_SESSION['ride_offer']['richourstart'].":".  $_SESSION['ride_offer']['ricminstart'].":00";
          }else{
            $pointtime = $_SESSION['ride_offer']['onetihourstart'].":".  $_SESSION['ride_offer']['onetimeminstart'].":00";
          }
          $input_time = $pointtime;
        }
        
        $Data_ride_point['iRideId'] = $ride_id; 
        $places = explode("|", $_REQUEST['points_'.$i]);
        $Data_ride_point['vStartPoint'] = $places[0];
        $Data_ride_point['vEndPoint'] = $places[1];
        $startlat = explode("|", $_REQUEST['start_lat_'.$i]);
        $endlat = explode("|", $_REQUEST['end_lat_'.$i]);
        $Data_ride_point['vStartLatitude'] = $startlat[0];
        $Data_ride_point['vStartLongitude'] = $startlat[1]; 
        $Data_ride_point['vEndLatitude'] = $endlat[0]; 
        $Data_ride_point['vEndLongitude'] = $endlat[1]; 
        $Data_ride_point['vDistance'] = $_REQUEST['distance_'.$i]; 
        $Data_ride_point['vDuration'] = $_REQUEST['duration_'.$i];
        $tot=$_REQUEST['fBoxPrice'] + $_REQUEST['fDocumentPrice'] + $_REQUEST['fLuggagePrice'];
        //$Data_ride_point['fPrice'] = $_REQUEST['price_'.$i];
        $Data_ride_point['fPrice'] = $tot;
        $Data_ride_point['fOriginalPrice'] = $_REQUEST['price_orig_'.$i];
        $Data_ride_point['eReverse'] = 'No';
        $Data_ride_point['ePriceType'] = $_REQUEST['price_type_'.$i];
        $Data_ride_point['tRideTime'] = $input_time;        
        
        # calculate commision , vat and totprice #
        $Data_ride_point['fCommission'] = $SITE_COMMISSION;
        $Data_ride_point['fSiteFixFee'] = $SITE_FIXED_FEE;
        $Data_ride_point['fVat']        = $SITE_VAT;
        # calculate commision , vat and totprice #
        
        $id = $obj->MySQLQueryPerform("ride_points_new",$Data_ride_point,'insert');
        
        $mailprice = $mailprice + $_REQUEST['price_'.$i];
        if($i != 0){
          $stopoverpoints .= '<img src="'.$tconfig['tsite_images'].'/location-stage.png">&nbsp;'.$Data_ride_point['vStartPoint'].'&nbsp;&nbsp;';
        }               
      }
    }
    
    //exit;
    
    if($triptype == 'recurring'){
     if($_SESSION['ride_offer']['roundtripric'] == 'Yes'){
       $round = 'Yes';
     }else{
       $round = 'No';
     }
    }else{
     if($_SESSION['ride_offer']['roundtriponetime'] == 'Yes'){
        $round = 'Yes';
     }else{
        $round = 'No';
     }
    }
    
    if($round == 'Yes'){
      $reverse_arr = array();
      for($i=0;$i<20;$i++){
      $new_arr = array();
        if($_REQUEST['points_'.$i] != ''){          
          $new_arr['iRideId'] = $ride_id;
          $places = explode("|", $_REQUEST['points_'.$i]);          
          $new_arr['vStartPoint'] = $places[1];
          $new_arr['vEndPoint'] = $places[0];
          $startlat = explode("|", $_REQUEST['end_lat_'.$i]);
          $endlat = explode("|", $_REQUEST['start_lat_'.$i]);
          $new_arr['vStartLatitude'] = $startlat[0];
          $new_arr['vStartLongitude'] = $startlat[1]; 
          $new_arr['vEndLatitude'] = $endlat[0]; 
          $new_arr['vEndLongitude'] = $endlat[1]; 
          $new_arr['vDistance'] = $_REQUEST['distance_'.$i]; 
          $new_arr['vDuration'] = $_REQUEST['duration_'.$i];
           $tot=$_REQUEST['fBoxPrice'] + $_REQUEST['fDocumentPrice'] + $_REQUEST['fLuggagePrice'];
          //$new_arr['fPrice'] = $_REQUEST['price_'.$i];
          $new_arr['fPrice'] = $tot;
          $new_arr['fOriginalPrice'] = $_REQUEST['price_orig_'.$i];
          $new_arr['ePriceType'] = $_REQUEST['price_type_'.$i];
          $new_arr['eReverse'] = 'Yes';
          
          # calculate commision , vat and totprice #
          $new_arr['fCommission'] = $SITE_COMMISSION;
          $new_arr['fSiteFixFee'] = $SITE_FIXED_FEE;
          $new_arr['fVat']        = $SITE_VAT;
          # calculate commision , vat and totprice #
         
          array_push($reverse_arr, $new_arr);
        }
      }
      
      $reverse_arr = array_reverse($reverse_arr);
      for($i=0;$i<count($reverse_arr);$i++){
        if($i != 0){
          $abc = $i - 1; 
          $dur = explode(' ',$reverse_arr[$abc]['vDuration']);
          if (strpos($reverse_arr[$abc]['vDuration'],'hour')) {
              $dur = explode(' ',$reverse_arr[$abc]['vDuration']);
              $minutes = $minutes + ($dur[0]*60) + $dur[2];
          }else{
              $dur = explode(' ',$reverse_arr[$abc]['vDuration']);
              $minutes = $minutes + $dur[0];
          }
          $input_time = date("H:i", strtotime($pointtime)+($minutes*60)).':00';        
        }else{
          if($triptype == 'recurring'){
            $pointtime = $_SESSION['ride_offer']['richourend'].":".  $_SESSION['ride_offer']['ricminend'].":00";
          }else{
            $pointtime = $_SESSION['ride_offer']['onetihourend'].":".  $_SESSION['ride_offer']['onetimeminend'].":00";
          }
          $input_time = $pointtime;
        }
      
        $Data_ride_point = array();
        $Data_ride_point['iRideId'] = $reverse_arr[$i]['iRideId'];
        $Data_ride_point['vStartPoint'] = $reverse_arr[$i]['vStartPoint'];
        $Data_ride_point['vEndPoint'] = $reverse_arr[$i]['vEndPoint'];        
        $Data_ride_point['vStartLatitude'] = $reverse_arr[$i]['vStartLatitude'];
        $Data_ride_point['vStartLongitude'] = $reverse_arr[$i]['vStartLongitude']; 
        $Data_ride_point['vEndLatitude'] = $reverse_arr[$i]['vEndLatitude']; 
        $Data_ride_point['vEndLongitude'] = $reverse_arr[$i]['vEndLongitude']; 
        $Data_ride_point['vDistance'] = $reverse_arr[$i]['vDistance']; 
        $Data_ride_point['vDuration'] = $reverse_arr[$i]['vDuration'];
        $Data_ride_point['fPrice'] = $reverse_arr[$i]['fPrice'];
        $Data_ride_point['fOriginalPrice'] = $reverse_arr[$i]['fOriginalPrice'];
        $Data_ride_point['eReverse'] = $reverse_arr[$i]['eReverse']; 
        $Data_ride_point['ePriceType'] = $reverse_arr[$i]['ePriceType'];
        $Data_ride_point['tRideTime'] = $input_time;
        
        # calculate commision , vat and totprice #
        $Data_ride_point['fCommission'] = $SITE_COMMISSION;
        $Data_ride_point['fSiteFixFee'] = $SITE_FIXED_FEE;
        $Data_ride_point['fVat']        = $SITE_VAT;
        # calculate commision , vat and totprice #
       
        $id = $obj->MySQLQueryPerform("ride_points_new",$Data_ride_point,'insert');
      }
      
    }
  
    ###### insert ride point data end ######
    
    ###### insert ride dates data start ######
    if($triptype == 'onetime'){
      $Data_dates['iRideId'] = $ride_id;
      $Data_dates['dDateOut'] = $_SESSION['ride_offer']['sdateone'];
      if($round == 'Yes'){          
        $Data_dates['dDateRet'] = $_SESSION['ride_offer']['edateone']; 
      }
      $id = $obj->MySQLQueryPerform("ride_dates",$Data_dates,'insert'); 
    }else{
      $start = $_SESSION['ride_offer']['sdate'];
      $end = $_SESSION['ride_offer']['edate'];  
      $all_dates = getDatesFromRange($start, $end);
      
      $myarr = array();
      for($i=0;$i<count($all_dates);$i++){ 
        $newarr = array();
        $rb = in_multiarray($all_dates[$i], $_SESSION['dates'], 'dt');
        $rb = explode('|', $rb);
        if($rb[0] == 'Yes'){
          $newarr['date'] = $all_dates[$i];
          $newarr['type'] = $rb[1];
          array_push($myarr, $newarr);
        }else{
          if (in_array(date('l', strtotime($all_dates[$i])), $_SESSION['ride_offer']['outbound']) && in_array(date('l', strtotime($all_dates[$i])), $_SESSION['ride_offer']['retturn'])) {
            $newarr['date'] = $all_dates[$i];
            $newarr['type'] = 'both';
          }else if (in_array(date('l', strtotime($all_dates[$i])), $_SESSION['ride_offer']['outbound'])) {
            $newarr['date'] = $all_dates[$i];
            $newarr['type'] = 'out';
          }else if (in_array(date('l', strtotime($all_dates[$i])), $_SESSION['ride_offer']['retturn'])) {
            $newarr['date'] = $all_dates[$i];
            $newarr['type'] = 'ret';
          }else{
            $newarr['date'] = $all_dates[$i];
            $newarr['type'] = 'noone';
          }
          array_push($myarr, $newarr);   
        }
      }
      
      for($i=0;$i<count($myarr);$i++){
        $Data_dates = array();
        if($myarr[$i]['type'] == 'ret'){
          $Data_dates['iRideId'] = $ride_id;
          $Data_dates['dDateRet'] = $myarr[$i]['date'];
          $id = $obj->MySQLQueryPerform("ride_dates",$Data_dates,'insert');          
        }else if($myarr[$i]['type'] == 'out'){
          $Data_dates['iRideId'] = $ride_id;
          $Data_dates['dDateOut'] = $myarr[$i]['date'];
          $id = $obj->MySQLQueryPerform("ride_dates",$Data_dates,'insert');
        }else if($myarr[$i]['type'] == 'both'){
          $Data_dates['iRideId'] = $ride_id;
          $Data_dates['dDateOut'] = $myarr[$i]['date'];
          $Data_dates['dDateRet'] = $myarr[$i]['date'];
          $id = $obj->MySQLQueryPerform("ride_dates",$Data_dates,'insert');
        } 
      }
    }     
    ###### insert ride dates data end ######
    
    ###### user mail ######
    $mailprice = $generalobj->booking_currency($mailprice, $_SESSION['sess_price_ratio']);
    
    $sql = "SELECT member.iMemberId, member.vFirstName, member.vLastName, member.eGender, member.vImage, member.vEmail, member_email_notification.eSuccessPublish, member.eOffrer_RidePost, member.iFBId, member.vLanguageCode   
            FROM member 
            LEFT JOIN member_email_notification ON member_email_notification.iMemberId = member.iMemberId  
            WHERE member.iMemberId = '".$_SESSION["sess_iMemberId"]."'";
    $db_member = $obj->MySQLSelect($sql);
    
    $maildata['membername'] = $db_member[0]['vFirstName'].' '.$db_member[0]['vLastName'];
    $maildata['vEmail'] = $db_member[0]['vEmail'];
    
    $maildata_member['membername'] = $db_member[0]['vFirstName'].' '.$db_member[0]['vLastName'];
    $maildata_member['vEmail'] = $db_member[0]['vEmail'];
    # For user language label
    $sql = "SELECT * from language_label WHERE vCode = '".$db_member[0]['vLanguageCode']."'";
    $db_label = $obj->MySQLSelect($sql);  
    $vLabel = array(); 
    for($i=0;$i<count($db_label);$i++){
  		$vLabel[$db_label[$i]['vLabel']]  = $db_label[$i]["vValue"];
  	} 
    # For user language label
    
    if(is_file($tconfig["tsite_upload_images_member_path"].$db_member[0]['iMemberId']."/1_".$db_member[0]['vImage']))
    {
      $maildata['image'] = $tconfig["tsite_upload_images_member"].$db_member[0]['iMemberId']."/1_".$db_member[0]['vImage'];
    }
    else
    {     
      if($db_member[0]['eGender']=='Male'){
          $maildata['image'] = $tconfig["tsite_images"].'64x64male.png';
        }else{
          $maildata['image'] = $tconfig["tsite_images"].'64x64female.png';
        }
    }
    # User Email below code
    $mailcont_member .= '<table width="100%" style="border:1px solid #cccccc;border-collapse: collapse;background-color: #FFFFFF;">
                <tr>
                  <td width="100%" colspan="2" style="background:#FFF;color:#E83100;padding:10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">'.$vLabel['LBL_NEW_RIDES_OFFER1'].' '.$maildata['membername'].' '.$vLabel['LBL_NEW_RIDES_OFFER2'].'</td>
                </tr>
                <tr>
                  <td width="100%" colspan="2" style="background:#E83100;color:#FFF;padding:10px;text-align:center;font-size:20px;font-family:Arial, Helvetica, sans-serif;"><img src="'.$tconfig['tsite_images'].'/search-from-plot1.png">&nbsp;'.$_SESSION['ride_offer']['from'].'&nbsp;&nbsp;<img src="'.$tconfig['tsite_images'].'/search-to-plot1.png">&nbsp;'.$_SESSION['ride_offer']['to'].'</td>
                </tr>
                <tr>
                  <td width="100%" colspan="2">&nbsp;</td>
                </tr>
                <tr>
                  <td width="15%" valign="top" style="border-bottom:1px solid #cccccc;">
                    <img style="margin-top: 8px;border-radius: 3px;border: 3px solid #E83100;" alt="" src="'.$maildata['image'].'">
                  </td>
                  <td width="85%" valign="top" style="border-bottom:0px solid #cccccc;">
                    <table width="100%">
                      <tr>
                        <td valign="top" style="color:#E83100;padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
                          <span style="color:#000;">'.$vLabel['LBL_STOPOVER_POINT'].':</span> '.$stopoverpoints.' 
                        </td>
                      </tr>
                      <tr>
                        <td valign="top" style="color:#E83100;padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
                          <span style="color:#000;">'.$vLabel['LBL_TRIP_TYPE'].':</span> '.$mailtriptype.' 
                        </td>
                      </tr>
                      <tr>
                        <td valign="top" style="color:#E83100;padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
                          '.$mailfromdate.$mailtodate.'
                        </td>
                      </tr>
                      <tr>
                        <td valign="top" style="color:#E83100;padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
                          '.$mailfromtime.$mailtotime.' 
                        </td>
                      </tr>
                      <tr>
                        <td valign="top" style="color:#E83100;padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
                          <span style="color:#000;">'.$vLabel['LBL_PRICE'].':</span> '.$mailprice.'&nbsp;&nbsp;<span style="color:#000;">'.$vLabel['LBL_AVAILABLE_SEATS'].': </span>'.$_REQUEST['seats'].'
                        </td>
                      </tr>
                      <tr>
                        <td valign="top" style="color:#E83100;padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
                          <span style="color:#000;">'.$vLabel['LBL_OFFERED_BY'].' :</span> '.$maildata['membername'].' 
                        </td>
                      </tr>                       
                    </table>
                  </td>
                </tr>                                                
              </table>';
    $maildata_member['details'] = $mailcont_member;
    
    # Admin Mail below code
    
    $mailcont .= '<table width="100%" style="border:1px solid #cccccc;border-collapse: collapse;background-color: #FFFFFF;">
                <tr>
                  <td width="100%" colspan="2" style="background:#FFF;color:#05A2DB;padding:10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">Hello, new ride is offered by '.$maildata['membername'].' at Balticcar for</td>
                </tr>
                <tr>
                  <td width="100%" colspan="2" style="background:#05A2DB;color:#FFF;padding:10px;text-align:center;font-size:20px;font-family:Arial, Helvetica, sans-serif;"><img src="'.$tconfig['tsite_images'].'/search-from-plot1.png">&nbsp;'.$_SESSION['ride_offer']['from'].'&nbsp;&nbsp;<img src="'.$tconfig['tsite_images'].'/search-to-plot1.png">&nbsp;'.$_SESSION['ride_offer']['to'].'</td>
                </tr>
                <tr>
                  <td width="100%" colspan="2">&nbsp;</td>
                </tr>
                <tr>
                  <td width="15%" valign="top" style="border-bottom:1px solid #cccccc;">
                    <img style="margin-top: 8px;border-radius: 3px;border: 3px solid #05A2DB;" alt="" src="'.$maildata['image'].'">
                  </td>
                  <td width="85%" valign="top" style="border-bottom:0px solid #cccccc;">
                    <table width="100%">
                      <tr>
                        <td valign="top" style="color:#05A2DB;padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
                          <span style="color:#000;">Stopover points:</span> '.$stopoverpoints.' 
                        </td>
                      </tr>
                      <tr>
                        <td valign="top" style="color:#05A2DB;padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
                          <span style="color:#000;">Trip Type:</span> '.$mailtriptype.' 
                        </td>
                      </tr>
                      <tr>
                        <td valign="top" style="color:#05A2DB;padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
                          '.$mailfromdate.$mailtodate.'
                        </td>
                      </tr>
                      <tr>
                        <td valign="top" style="color:#05A2DB;padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
                          '.$mailfromtime.$mailtotime.' 
                        </td>
                      </tr>
                      <tr>
                        <td valign="top" style="color:#05A2DB;padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
                          <span style="color:#000;">Price:</span> '.$mailprice.'&nbsp;&nbsp;<span style="color:#000;">Available Seats: </span>'.$_REQUEST['seats'].'
                        </td>
                      </tr>
                      <tr>
                        <td valign="top" style="color:#05A2DB;padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
                          <span style="color:#000;">Offerred By :</span> '.$maildata['membername'].' 
                        </td>
                      </tr>                       
                    </table>
                  </td>
                </tr>                                                
              </table>';
    $maildata['details'] = $mailcont;
    
    if($db_member[0]['eSuccessPublish'] == 'Yes'){
      //$generalobj->send_email_user("NEWRIDEOFFER_MEMBER",$maildata);
    }             
    //$generalobj->send_email_user("NEWRIDEOFFER_ADMIN",$maildata);
    
    /* if($db_member[0]['iFBId'] != 0 && $db_member[0]['eOffrer_RidePost'] == 'Yes'){
      $sql = "SELECT dDateOut, dDateRet FROM ride_dates WHERE iRideId = '".$ride_id."' ORDER BY iRideDateId ASC LIMIT 0,1";
      $db_ride_date = $obj->MySQLSelect($sql);
      
      if($db_ride_date[0]['dDateOut'] != '0000-00-00'){
        $timepost = strtotime($db_ride_date[0]['dDateOut']);
        $ret_post = 'No';
      }else{
        $timepost = strtotime($db_ride_date[0]['dDateRet']);
        $ret_post = 'Yes';
      } 
    } */  
                                    
    $iMemberId = $_SESSION['sess_iMemberId'];
    $vFirstName = $_SESSION["sess_vFirstName"];
    $vLastName = $_SESSION["sess_vLastName"];
    $vEmail = $_SESSION["sess_vEmail"];
    $vImage = $_SESSION["sess_vImage"];
    $eGender = $_SESSION["sess_eGender"];
    $price_ratio = $_SESSION['sess_price_ratio'];
    $sess_lang = $_SESSION["sess_lang"];
    
    session_unset();
    
    $_SESSION['sess_iMemberId'] = $iMemberId;
    $_SESSION["sess_vFirstName"] = $vFirstName;
    $_SESSION["sess_vLastName"] = $vLastName;
    $_SESSION["sess_vEmail"] = $vEmail;
    $_SESSION["sess_vImage"] = $vImage;
    $_SESSION["sess_eGender"] = $eGender;
    $_SESSION["sess_price_ratio"] = $price_ratio;
    $_SESSION["sess_lang"] = $sess_lang;
    
    $msg = LBL_RIDE_UPDATE_SUCCESS;
	  header("Location:".$tconfig["tsite_url"]."index.php?file=m-list_rides_offer&var_msg=".$msg);
  }else{
    $iMemberId = $_SESSION['sess_iMemberId'];
    $vFirstName = $_SESSION["sess_vFirstName"];
    $vLastName = $_SESSION["sess_vLastName"];
    $vEmail = $_SESSION["sess_vEmail"];
    $vImage = $_SESSION["sess_vImage"];
    $eGender = $_SESSION["sess_eGender"];
    $price_ratio = $_SESSION['sess_price_ratio'];
    $sess_lang = $_SESSION["sess_lang"];
    
    session_unset();
    
    $_SESSION['sess_iMemberId'] = $iMemberId;
    $_SESSION["sess_vFirstName"] = $vFirstName;
    $_SESSION["sess_vLastName"] = $vLastName;
    $_SESSION["sess_vEmail"] = $vEmail;
    $_SESSION["sess_vImage"] = $vImage;
    $_SESSION["sess_eGender"] = $eGender;
    $_SESSION["sess_price_ratio"] = $price_ratio;
    $_SESSION["sess_lang"] = $sess_lang;
  
    $msg = LBL_RIDE_UPDATE_ERROR;
	  header("Location:".$tconfig["tsite_url"]."index.php?file=m-list_rides_offer&var_err_msg=".$msg);
  }    
  exit;
?>