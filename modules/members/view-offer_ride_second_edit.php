<?php 
	include_once(TPATH_CLASS_APP."class.customer.php");
	$CustomerObj = new Customer;
	$CustomerObj->check_member_login();  
	$strip_slashes_arr = array('from','to','loc1','loc2','loc3','loc4','loc5','loc6');
	
	foreach($strip_slashes_arr as $val)
	{
		$_POST[$val] = isset($_POST[$val])?stripslashes($_POST[$val]):'';
		$_REQUEST[$val] = isset($_REQUEST[$val])?stripslashes($_REQUEST[$val]):'';
	}
	
	$triptype = $_REQUEST['triptype'];   
	$_SESSION['ride_offer'] = $_POST;
	$_SESSION['ride_offer']['second_page'] = 'Yes';
	
	if($triptype != ''){
		if($triptype == 'onetime'){ 
			$Roundtrip = $_REQUEST['roundtriponetime']; 
			if($Roundtrip == 'Yes'){
				$_SESSION['ride_offer']['roundtriponetime'] = 'Yes';
				}else{
				$_SESSION['ride_offer']['roundtriponetime'] = 'No';
			}  
            
			#Server side validation start 
			include_once(TPATH_LIBRARIES."/general/validation.class.php");
			$validobj = new validation();
			
			$validobj->add_fields($_POST['from'], 'req', LBL_SELECT_DEPARTURE_POINT);
			$validobj->add_fields($_POST['to'], 'req', LBL_SELECT_ARRIVAL_POINT);
			$validobj->add_fields($_POST['triptype'], 'req', LBL_SELECT_TRIP_TYPE);
			$validobj->add_fields($_POST['sdateone'], 'req', LBL_SELECT_DEPARTURE_DATE);
			$validobj->add_fields($_POST['onetihourstart'], 'req', LBL_SELECT_DEPARTURE_HOUR); 
			$validobj->add_fields($_POST['onetimeminstart'], 'req', LBL_SELECT_DEPARTURE_MINUTES);
			if($Roundtrip == 'Yes'){
				$validobj->add_fields($_POST['edateone'], 'req', LBL_SELECT_RETURN_DATE);
				$validobj->add_fields($_POST['onetihourend'], 'req', LBL_SELECT_RETURN_HOUR);
				$validobj->add_fields($_POST['onetimeminend'], 'req', LBL_SELECT_RETURN_MINUTES);
			}  
            
			$error = $validobj->validate();
			if($error){
				$generalobj->getPostFormData($_POST,$error,$tconfig['tsite_url']."offer-ride");
			}       
			}else{
			$Roundtrip = $_REQUEST['roundtripric']; 
			if($Roundtrip == 'Yes'){
				$_SESSION['ride_offer']['roundtripric'] = 'Yes';
				}else{
				$_SESSION['ride_offer']['roundtripric'] = 'No';
			}
			#Server side validation start 
			include_once(TPATH_LIBRARIES."/general/validation.class.php");
			$validobj = new validation();
			
			$validobj->add_fields($_POST['from'], 'req', LBL_SELECT_DEPARTURE_POINT);
			$validobj->add_fields($_POST['to'], 'req', LBL_SELECT_ARRIVAL_POINT);
			$validobj->add_fields($_POST['triptype'], 'req', LBL_SELECT_TRIP_TYPE);
			$validobj->add_fields($_POST['sdate'], 'req', LBL_SELECT_DEPARTURE_DATE);
			$validobj->add_fields($_POST['richourstart'], 'req', LBL_SELECT_DEPARTURE_HOUR); 
			$validobj->add_fields($_POST['ricminstart'], 'req', LBL_SELECT_DEPARTURE_MINUTES);
			
			if(count($_POST['outbound']) == 0){
				$validobj->add_fields($_POST['outbound'], 'req', 'Select Outbound days');
			}
			
			if($Roundtrip == 'Yes'){
				$validobj->add_fields($_POST['edate'], 'req', LBL_SELECT_RETURN_DATE);
				$validobj->add_fields($_POST['richourend'], 'req', LBL_SELECT_RETURN_HOUR);
				$validobj->add_fields($_POST['ricminend'], 'req', LBL_SELECT_RETURN_MINUTES);
				if(count($_POST['retturn']) == 0){
					$validobj->add_fields($_POST['retturn'], 'req', 'Select Return days');
				}
			}  
            
			$error = $validobj->validate();
			if($error){
				$generalobj->getPostFormData($_POST,$error,$tconfig['tsite_url']."offer-ride");
			}
		}
	}
	
	$sql = "SELECT * FROM rides_new WHERE iRideId = '".$_REQUEST['iRideId']."'";
	$db_ride = $obj->MySQLSelect($sql);
	
	$sql = "SELECT * FROM ride_points_new WHERE iRideId = '".$_REQUEST['iRideId']."' AND eReverse = 'No'";
	$db_ride_points = $obj->MySQLSelect($sql);
	
	$_SESSION['sess_price_ratio'] = $db_ride[0]['vBookerCurrencyCode'];
	
	$new_arr = array();
	$old_points_arr = array();
	if(count($db_ride_points) > 1){      
		for($i=0;$i<count($db_ride_points);$i++){
			if($i != (count($db_ride_points)-1)){
				$new_arr['old_point_address'] = $db_ride_points[$i]['vStartPoint'];
				$new_arr['old_latitude'] = $db_ride_points[$i]['vStartLatitude'];
				$new_arr['old_longitude'] = $db_ride_points[$i]['vStartLongitude'];
				$new_arr['old_price'] = $db_ride_points[$i]['fPrice'];
				$new_arr['old_original_price'] = $db_ride_points[$i]['fOriginalPrice'];
				array_push($old_points_arr, $new_arr);
				}else{
				$new_arr['old_point_address'] = $db_ride_points[$i]['vStartPoint'];
				$new_arr['old_latitude'] = $db_ride_points[$i]['vStartLatitude'];
				$new_arr['old_longitude'] = $db_ride_points[$i]['vStartLongitude'];
				$new_arr['old_price'] = $db_ride_points[$i]['fPrice'];
				$new_arr['old_original_price'] = $db_ride_points[$i]['fOriginalPrice'];
				array_push($old_points_arr, $new_arr);
				
				$new_arr['old_point_address'] = $db_ride_points[$i]['vEndPoint'];
				$new_arr['old_latitude'] = $db_ride_points[$i]['vEndLatitude'];
				$new_arr['old_longitude'] = $db_ride_points[$i]['vEndLongitude'];
				$new_arr['old_price'] = $db_ride_points[$i]['fPrice'];
				$new_arr['old_original_price'] = $db_ride_points[$i]['fOriginalPrice'];
				array_push($old_points_arr, $new_arr);
			}
		}
		}else{
		$new_arr['old_point_address'] = $db_ride_points[0]['vStartPoint'];
		$new_arr['old_latitude'] = $db_ride_points[0]['vStartLatitude'];
		$new_arr['old_longitude'] = $db_ride_points[0]['vStartLongitude'];
		$new_arr['old_price'] = $db_ride_points[0]['fPrice'];
		$new_arr['old_original_price'] = $db_ride_points[0]['fOriginalPrice'];
		array_push($old_points_arr, $new_arr);
		
		$new_arr['old_point_address'] = $db_ride_points[0]['vEndPoint'];
		$new_arr['old_latitude'] = $db_ride_points[0]['vEndLatitude'];
		$new_arr['old_longitude'] = $db_ride_points[0]['vEndLongitude'];
		$new_arr['old_price'] = $db_ride_points[0]['fPrice'];
		$new_arr['old_original_price'] = $db_ride_points[0]['fOriginalPrice'];
		array_push($old_points_arr, $new_arr);
		
	}
	
	//echo "<pre>";
	//print_r($db_ride_points);   exit;
	
	$points_arr = array();
	$new_arr = array();
	$new_arr['addr'] = $_REQUEST['from'];
	$lat_long = explode(', ',substr($_REQUEST['from_lat_long'], 1, -1)); 
	$new_arr['lat'] = number_format($lat_long[0],6,'.','');
	$new_arr['lon'] = number_format($lat_long[1],6,'.','');
	array_push($points_arr, $new_arr); 
	for($i=1;$i<=6;$i++){
		$new_arr = array();
		if($_REQUEST['loc'.$i] != ''){       
			$new_arr['addr'] = $_REQUEST['loc'.$i]; 
			$lat_long = explode(', ',substr($_REQUEST['loc'.$i.'_lat_long'], 1, -1)); 
			$new_arr['lat'] = number_format($lat_long[0],6,'.','');
			$new_arr['lon'] = number_format($lat_long[1],6,'.','');
			array_push($points_arr, $new_arr);
		} 
	}
	$new_arr = array();
	$new_arr['addr'] = $_REQUEST['to']; 
	$lat_long = explode(', ',substr($_REQUEST['to_lat_long'], 1, -1)); 
	$new_arr['lat'] = number_format($lat_long[0],6,'.','');
	$new_arr['lon'] = number_format($lat_long[1],6,'.','');
	array_push($points_arr, $new_arr);
	$loop = count($points_arr)-1; 
	
	$counter = 1;
	$main_points_arr = array();
	for($i=0;$i<$loop;$i++){ 
		$new_arr = array();
		
		$new_arr['addr'] = $points_arr[$i]['addr'].' &rarr; '.$points_arr[$counter]['addr'];
		$new_arr['addr_post'] = $points_arr[$i]['addr'].'|'.$points_arr[$counter]['addr'];
		$new_arr['start_lat'] = $points_arr[$i]['lat'].'|'.$points_arr[$i]['lon'];
		$new_arr['end_lat'] = $points_arr[$counter]['lat'].'|'.$points_arr[$counter]['lon'];
		$new_arr['fPrice'] = 0;
		$new_arr['fOriginalPrice'] = 0;
		for($j=0;$j<count($db_ride_points);$j++){
			if($points_arr[$i]['lat'] == $db_ride_points[$j]['vStartLatitude'] && $points_arr[$i]['lon'] == $db_ride_points[$j]['vStartLongitude'] && $points_arr[$counter]['lat'] == $db_ride_points[$j]['vEndLatitude'] && $points_arr[$counter]['lon'] == $db_ride_points[$j]['vEndLongitude'])
			{
				$new_arr['fPrice'] = $db_ride_points[$j]['fPrice'];
				$new_arr['fOriginalPrice'] = $db_ride_points[$j]['fOriginalPrice'];
			}
		}
		
		array_push($main_points_arr, $new_arr);
		$counter++; 
	}       
	
	//echo "<pre>";
	//print_r($main_points_arr); exit;
	$dest = end($main_points_arr); 
	$destination_country = $dest['end_lat'];
	if($_REQUEST['loc'] == '' && $_REQUEST['loc1'] == '' && $_REQUEST['loc2'] == '' && $_REQUEST['loc3'] == '' && $_REQUEST['loc4'] == '' && $_REQUEST['loc5'] == '' && $_REQUEST['loc6'] == ''){
		$no_other_points = 'Yes';
	}
	
	$sql = "SELECT member_car.iMemberCarId, member_car.iSeats, make.vMake, model.vTitle 
	FROM member_car 
	LEFT JOIN make ON make.iMakeId = member_car.iMakeId 
	LEFT JOIN model ON model.iModelId = member_car.iModelId 
	WHERE iMemberId = '".$_SESSION['sess_iMemberId']."' 
	ORDER BY iMemberCarId ASC";
	$db_car = $obj->MySQLSelect($sql); 
	
	/*if($_SESSION['sess_price_ratio'] == 'GBP'){
		$Hundradrmilespice = $HUNDREDMILES_PRICE_GBP;
		}else if($_SESSION['sess_price_ratio'] == 'NOK'){
		$Hundradrmilespice = $HUNDREDMILES_PRICE_NOK;
		}else if($_SESSION['sess_price_ratio'] == 'SEK'){
		$Hundradrmilespice = $HUNDREDMILES_PRICE_SEK;
		}else if($_SESSION['sess_price_ratio'] == 'DKK'){
		$Hundradrmilespice = $HUNDREDMILES_PRICE_DKK;
		}else if($_SESSION['sess_price_ratio'] == 'PLN'){
		$Hundradrmilespice = $HUNDREDMILES_PRICE_PLN;
		}else if($_SESSION['sess_price_ratio'] == 'RUB'){
		$Hundradrmilespice = $HUNDREDMILES_PRICE_RUB;
		}else if($_SESSION['sess_price_ratio'] == 'EUR'){
		$Hundradrmilespice = $HUNDREDMILES_PRICE_EUR;
		}else if($_SESSION['sess_price_ratio'] == 'USD'){
		$Hundradrmilespice = $HUNDREDMILES_PRICE_USD;
	}*/
	
	$db_curr_mst = unserialize(db_curr_mst);
	for($i=0;$i<count($db_curr_mst);$i++){
		$sess_price_ratio = isset($_SESSION['sess_price_ratio'])?$_SESSION['sess_price_ratio']:CURR_MASTER_DEF_CURR;
		if($sess_price_ratio == $db_curr_mst[$i]['vName']){
			
			$str = 'HUNDREDMILES_PRICE_'.$db_curr_mst[$i]['vName'];
			$Hundradrmilespice = $$str; 
			// $Hundradrmilespice = $HUNDREDMILES_PRICE_.$db_curr_mst[$i]['vName']; // $HUNDREDMILES_PRICE_EUR = 10
			break;
		}
	}
	
	$sql = "SELECT ".$_SESSION['sess_price_ratio']." as ratio_price FROM currency WHERE vName = 'EUR'";
	$db_currency = $obj->MySQLSelect($sql); 
	##echo '<pre>';print_r($_SESSION['ride_offer']);exit;
	$min_price_any_ride = round($db_currency[0]['ratio_price'] * 4);   
	$smarty->assign("min_price_any_ride",$min_price_any_ride);      
	
	$smarty->assign("from",$_SESSION['ride_offer']['from']);
	$smarty->assign("to",$_SESSION['ride_offer']['to']);
	$smarty->assign("loc1",$_SESSION['ride_offer']['loc1']);
	$smarty->assign("loc2",$_SESSION['ride_offer']['loc2']);
	$smarty->assign("loc3",$_SESSION['ride_offer']['loc3']);
	$smarty->assign("loc4",$_SESSION['ride_offer']['loc4']);
	$smarty->assign("loc5",$_SESSION['ride_offer']['loc5']);
	$smarty->assign("loc6",$_SESSION['ride_offer']['loc6']);
	$smarty->assign("distance",$_SESSION['ride_offer']['distance']);
	$smarty->assign("duration",$_SESSION['ride_offer']['duration']);     
	$smarty->assign("main_points_arr",$main_points_arr);   
	$smarty->assign("Price_per_passenger",$Price_per_passenger);
	$smarty->assign("no_other_points",$no_other_points);  
	$smarty->assign("db_car",$db_car); 
	$smarty->assign("Hundradrmilespice",$Hundradrmilespice);    
	$smarty->assign("iRideId",$_REQUEST['iRideId']);
	$smarty->assign("db_ride",$db_ride);
	$smarty->assign("destination_country",$destination_country);        
	#echo '<pre>'; print_R($_POST); print_R($db_ride); echo '</pre>';
?>