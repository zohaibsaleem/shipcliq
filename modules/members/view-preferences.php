<?php  
  include_once(TPATH_CLASS_APP."class.customer.php");
  $CustomerObj = new Customer;
  $CustomerObj->check_member_login();
  $sess_iMemberId = $_SESSION['sess_iMemberId'];
  
  $sql = "SELECT * FROM preferences WHERE eStatus = 'Active' ORDER BY iDispOrder ASC";
  $db_preferences = $obj->MySQLSelect($sql);
  
  if($_REQUEST['action'] == 'edit'){     
    $sql = "DELETE FROM member_car_preferences WHERE iMemberId = '".$_SESSION['sess_iMemberId']."'";
    $db_sql = $obj->sql_query($sql);
    
    for($i=0;$i<count($db_preferences);$i++){
      $prefid = $db_preferences[$i]['iPreferencesId'];
      
      if($_REQUEST['vYes_'.$prefid] == 'vYes'){
        $Data_vYes['vType'] = $_REQUEST['vYes_'.$prefid];
        $Data_vYes['iMemberId'] = $_SESSION['sess_iMemberId'];
        $Data_vYes['iPreferencesId'] = $prefid;
        $id = $obj->MySQLQueryPerform("member_car_preferences",$Data_vYes,'insert');
      }
      
      if($_REQUEST['vMAYBE_'.$prefid] == 'vMAYBE'){
        $Data_vMAYBE['vType'] = $_REQUEST['vMAYBE_'.$prefid];
        $Data_vMAYBE['iMemberId'] = $_SESSION['sess_iMemberId'];
        $Data_vMAYBE['iPreferencesId'] = $prefid;
        $id = $obj->MySQLQueryPerform("member_car_preferences",$Data_vMAYBE,'insert');
      }
      
      if($_REQUEST['vNO_'.$prefid] == 'vNO'){
        $Data_vNO['vType'] = $_REQUEST['vNO_'.$prefid];
        $Data_vNO['iMemberId'] = $_SESSION['sess_iMemberId'];
        $Data_vNO['iPreferencesId'] = $prefid;
        $id = $obj->MySQLQueryPerform("member_car_preferences",$Data_vNO,'insert');
      }
    }
    
    $var_msg = LBL_MEMBER_CAR_PREF." ".LBL_UPD_SUCC;
    header("Location:".$tconfig["tsite_url"]."index.php?file=m-preferences&var_msg=".$var_msg);
    exit;   
  }
 
  for($i=0;$i<count($db_preferences);$i++){
    if(file_exists($tconfig["tsite_upload_images_preferences_path"].'/'.$db_preferences[$i]['vYes'])){
      $db_preferences[$i]['vYes_img'] = $tconfig["tsite_upload_images_preferences"].$db_preferences[$i]['vYes'];  
    }else{
      $db_preferences[$i]['vYes_img'] = ''; 
    }
    
    if(file_exists($tconfig["tsite_upload_images_preferences_path"].'/'.$db_preferences[$i]['vMAYBE'])){
      $db_preferences[$i]['vMAYBE_img'] = $tconfig["tsite_upload_images_preferences"].$db_preferences[$i]['vMAYBE'];  
    }else{
      $db_preferences[$i]['vMAYBE_img'] = ''; 
    }
    
    if(file_exists($tconfig["tsite_upload_images_preferences_path"].'/'.$db_preferences[$i]['vNO'])){
      $db_preferences[$i]['vNO_img'] = $tconfig["tsite_upload_images_preferences"].$db_preferences[$i]['vNO'];  
    }else{
      $db_preferences[$i]['vNO_img'] = ''; 
    }
    $db_preferences[$i]['vTitle'] = $db_preferences[$i]['vTitle'.$_SESSION['sess_lang_pref']];
  }   
  
  $sql = "SELECT * FROM member_car_preferences WHERE iMemberId = '".$_SESSION['sess_iMemberId']."'";
  $db_member_preferences = $obj->MySQLSelect($sql);
 
  $smarty->assign("var_err_msg",$_REQUEST['var_err_msg']);
  $smarty->assign("var_msg",$_REQUEST['var_msg']);    
  $smarty->assign("db_preferences",$db_preferences);
  $smarty->assign("db_member_preferences",$db_member_preferences);
?>