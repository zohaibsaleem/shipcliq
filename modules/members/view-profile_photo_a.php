<?php  
    include_once(TPATH_LIBRARIES."/general/Imagecrop.class.php");
    $thumb = new thumbnail();
    $temp_gallery = $tconfig["tsite_temp_gallery"]; 
    

 $action=$_REQUEST['action']; 
 
 if($action=="delete_image"){
     $sql = "select vImage from member where iMemberId='".$_SESSION['sess_iMemberId']."'";
     $db_gallery = $obj->MySQLSelect($sql);    
     $Photo_Gallery_folder = $tconfig["tsite_upload_images_member_path"].$_SESSION['sess_iMemberId']."/";
     unlink($Photo_Gallery_folder.$db_gallery[0]['vImage']);
     unlink($Photo_Gallery_folder."1_".$db_gallery[0]['vImage']);
     unlink($Photo_Gallery_folder."2_".$db_gallery[0]['vImage']);
     unlink($Photo_Gallery_folder."3_".$db_gallery[0]['vImage']);   
     $sql="UPDATE member SET vImage = '' where iMemberId='".$_SESSION['sess_iMemberId']."'"; 
      $db_delete=$obj->sql_query($sql);
    if($db_delete){
      $var_msg = LBL_IMAGE_DEL_SUCC;
    	header("Location: ".$tconfig["tsite_url"]."index.php?file=m-profile_photo&var_msg=".$var_msg."&msg_code=1");
    	exit;
    }else{
        	$var_msg = LBL_ERROR_DEL;
        	header("Location: ".$tconfig["tsite_url"]."index.php?file=m-profile_photo&var_msg=".$var_msg."&msg_code=0");
        	exit;
      }
                 
 }
 if($action=="edit")
 {
  $image_object = $_FILES['vImage']['tmp_name'];  
  $image_name   = $_FILES['vImage']['name'];
  if($image_name != ""){
                $sql = "select vImage from member where iMemberId='".$_SESSION['sess_iMemberId']."' ";
                $db_gallery = $obj->MySQLSelect($sql);    
                $Photo_Gallery_folder = $tconfig["tsite_upload_images_member_path"].$_SESSION['sess_iMemberId']."/";
                if(!is_dir($Photo_Gallery_folder)){
	                   	mkdir($Photo_Gallery_folder, 0777);
	              }
                unlink($Photo_Gallery_folder.$db_gallery[0]['vImage']);
                unlink($Photo_Gallery_folder."1_".$db_gallery[0]['vImage']);
                unlink($Photo_Gallery_folder."2_".$db_gallery[0]['vImage']);
                unlink($Photo_Gallery_folder."3_".$db_gallery[0]['vImage']);
               
                $Data1['vImage'] = $generalobj->general_upload_image($image_object,$image_name,$Photo_Gallery_folder,'','','','','','','Y','');
                if($Data1['vImage']!=''){
                    
                    if(is_file($Photo_Gallery_folder.$Data1['vImage']))
                    {
                       include_once(TPATH_LIBRARIES."/general/SimpleImage.class.php");
                       $img = new SimpleImage();           
                       list($width, $height, $type, $attr)= getimagesize($Photo_Gallery_folder.$Data1['vImage']);           
              
                       if($width < $height){
                          $final_width = $width;
                       }else{
                          $final_width = $height;
                       }       
                       $img->load($Photo_Gallery_folder.$Data1['vImage'])->crop(0, 0, $final_width, $final_width)->save($Photo_Gallery_folder.$Data1['vImage']);
                       $Data1['vImage'] = $generalobj->img_data_upload($Photo_Gallery_folder,$Data1['vImage'],$Photo_Gallery_folder, $tconfig["tsite_upload_images_member_size1"], $tconfig["tsite_upload_images_member_size2"], $tconfig["tsite_upload_images_member_size3"],"");                       
                    }                     
                    
                    $where = " iMemberId = '".$_SESSION['sess_iMemberId']."'";
                    $res = $obj->MySQLQueryPerform("member",$Data1,'update',$where);
                    if($res){ 
                      $msg_code="1";
                      $var_msg=LBL_IMAGE_UPLOAD_SUCC;
                       $_SESSION["sess_vImage"]=$Data1['vImage'];
                    }else{
                      $sql = "select vImage from member where iMemberId='".$_SESSION['sess_iMemberId']."' ";
                      $db_gallery = $obj->MySQLSelect($sql);    
                      $Photo_Gallery_folder = $tconfig["tsite_upload_images_member_path"].$_SESSION['sess_iMemberId']."/";
                        unlink($Photo_Gallery_folder.$db_gallery[0]['vImage']);
                        unlink($Photo_Gallery_folder."1_".$db_gallery[0]['vImage']);
                        unlink($Photo_Gallery_folder."2_".$db_gallery[0]['vImage']);
                        unlink($Photo_Gallery_folder."3_".$db_gallery[0]['vImage']);
                        $msg_code="0";
                        $var_msg= LBL_IMAGE_NOT_UPLOAD." ".LBL_TRY_AGAIN;
                    }
                }else{
                   $msg_code="0";
                   $var_msg = LBL_IMAGE_NOT_UPLOAD." ".LBL_TRY_AGAIN;
                }
   }else{
   
    $msg_code="0";
    $var_msg=LBL_IMG_NOT_SELECT." ".LBL_TRY_AGAIN;
   }
   
   header("Location: ".$tconfig["tsite_url"]."index.php?file=m-profile_photo&var_msg=".$var_msg."&msg_code=$msg_code");
   exit;
 }
?>