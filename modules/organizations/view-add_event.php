<?php   
  $check_organization = $organizationsobj->check_organization($iOrganizationId); 
  $iOrganizationId = $_SESSION["sess_iOrganizationId"];
  $iEventId = $_REQUEST['iEventId'];
  
  $event_category = $eventsobj->event_catagory();
  $event_skills = $eventsobj->event_skills();
  $event_suitables = $eventsobj->good_matches();
  $countries = $organizationsobj->get_countires();
  
  if($iEventId == ''){
    $event[0]['vTitle'] = $_REQUEST['vTitle']; 
    $event[0]['eEventType'] = $_REQUEST['eEventType']; 
    $event[0]['dStartDate'] = $_REQUEST['dStartDate'];
    $event[0]['dEndDate'] = $_REQUEST['dEndDate'];
    $event[0]['vStartTime'] = $_REQUEST['vStartTime'];
    $event[0]['vEndTime'] = $_REQUEST['vEndTime'];
    $event[0]['vAddress'] = $_REQUEST['vAddress'];
    $event[0]['iSkillId'] = $_REQUEST['iSkillId'];
    $event[0]['iSuitableTypeId'] = $_REQUEST['iSuitableTypeId'];
    $event[0]['tRequirements'] = $_REQUEST['tRequirements'];
    $event[0]['iCategoryId'] = $_REQUEST['iCategoryId'];
    $event[0]['vAddress'] = $_REQUEST['vAddress'];
    $event[0]['tDetails'] = $_REQUEST['tDetails'];
    $event[0]['uplogo'] = $_REQUEST['uplogo'];
    $event[0]['vCity'] = $_REQUEST['vCity'];
    $event[0]['vCountry'] = $_REQUEST['vCountry'];
    $event[0]['vState'] = $_REQUEST['vState'];
    
    if($_SESSION['iTempUserId'] == ''){
      $_SESSION['iTempUserId'] = substr(number_format(time() * rand(),0,'',''),0,10);
    } 
    
    if($event[0]['uplogo'] != ''){
    $target_path = $tconfig['tsite_upload_images_tmpphotos_path'].$_SESSION['iTempUserId']."/";
    if(is_file($target_path.'/'.$event[0]['uplogo']))
    {
      $rrb = 'yes';
      list($width, $height, $type, $attr)= getimagesize($target_path.'/'.$event[0]['uplogo']);     
      $percent_resizing = 30;    
      $new_width = round((($percent_resizing/100)*$width));
      $new_height = round((($percent_resizing/100)*$height));  
      
      $event[0]['imageurl'] = $tconfig['tsite_upload_images_tmpphotos'].$_SESSION['iTempUserId'].DS.$event[0]['uplogo'];
      $event[0]['width'] = $new_width;
      $event[0]['height'] = $new_height;
    } 
  }
  }else{
    $check_event = $eventsobj->check_event($iEventId, $iOrganizationId);     
    $event = $eventsobj->event_edit_details($iEventId);
    $skills = $eventsobj->event_skills_list_edit($iEventId);
    $suitable_for = $eventsobj->event_suitable_for_list_edit($iEventId);  
    $categories = $eventsobj->event_category_list_edit($iEventId); 
    
    $event[0]['iCategoryId'] = array_map('current', $categories);
    $event[0]['iSuitableTypeId'] = array_map('current', $suitable_for);
    $event[0]['iSkillId'] = array_map('current', $skills);
  }
  
  //echo "<pre>";
  //print_r($event); exit;
  
  
  $smarty->assign("iEventId",$iEventId);
  $smarty->assign("iOrganizationId",$iOrganizationId);
  $smarty->assign("event_category",$event_category);  
  $smarty->assign("event_skills",$event_skills); 
  $smarty->assign("event_suitables",$event_suitables); 
  $smarty->assign("var_msg",$_REQUEST['var_msg']);
  $smarty->assign("event",$event);
  $smarty->assign("id",$_SESSION['iTempUserId']);
  $smarty->assign("db_country",$countries);
?>