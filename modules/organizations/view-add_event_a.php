<?php   
  if($_REQUEST['iEventId'] != ''){
    include_once(TPATH_LIBRARIES."/general/validation.class.php");
    $validobj = new validation();
    
    $validobj->add_fields($_POST['eEventType'], 'req', 'Please select Event Date Type');
    $validobj->add_fields($_POST['vTitle'], 'req', 'Please enter Event Title');
    $validobj->add_fields($_POST['vAddress'], 'req', 'Please enter Event Address');  
    
    $error = $validobj->validate();
    if($error){
    	$generalobj->getPostFormData($_POST,$error,$tconfig['tsite_url']."edit-event/".$_REQUEST['iEventId']);
    } 
        
    $Data['vTitle'] = $_REQUEST['vTitle'];
    $Data['eEventType'] = $_REQUEST['eEventType'];
    $Data['dStartDate'] = $_REQUEST['dStartDate'];
    $Data['dEndDate'] = $_REQUEST['dEndDate'];
    $Data['vStartTime'] = $_REQUEST['vStartTime'];
    $Data['vEndTime'] = $_REQUEST['vEndTime'];
    $Data['vAddress'] = $_REQUEST['vAddress'];
    $Data['tDetails'] = $_REQUEST['tDetails'];
    $Data['tRequirements'] = $_REQUEST['tRequirements'];
    $Data['vCountry'] = $_REQUEST['vCountry'];
    $Data['vCity'] = $_REQUEST['vCity'];
    $Data['vState'] = $_REQUEST['vState'];
        
    $where = " iEventId = '".$_REQUEST['iEventId']."'";
    $res = $obj->MySQLQueryPerform("event",$Data,'update',$where);      
    
    $sql="delete from events_category where iEventId='".$_REQUEST['iEventId']."'";
  	$db_sql=$obj->sql_query($sql);
  	
    for($i=0;$i<count($_REQUEST['iCategoryId']);$i++){
      $CatData['iEventId'] = $_REQUEST['iEventId'];
      $CatData['iCategoryId'] = $_REQUEST['iCategoryId'][$i];
      $catid = $obj->MySQLQueryPerform("events_category",$CatData,'insert');
    } 
    
    $sql="delete from event_skills where iEventId='".$_REQUEST['iEventId']."'";
  	$db_sql=$obj->sql_query($sql);   
    
    for($i=0;$i<count($_REQUEST['iSkillId']);$i++){
      $skillData['iEventId'] = $_REQUEST['iEventId'];
      $skillData['iSkillId'] = $_REQUEST['iSkillId'][$i];
      $skillid = $obj->MySQLQueryPerform("event_skills",$skillData,'insert');
    } 
    
    $sql="delete from events_suitabletype where iEventId='".$_REQUEST['iEventId']."'";
  	$db_sql=$obj->sql_query($sql);  
    
    for($i=0;$i<count($_REQUEST['iSuitableTypeId']);$i++){
      $suitData['iEventId'] = $_REQUEST['iEventId'];
      $suitData['iSuitableTypeId'] = $_REQUEST['iSuitableTypeId'][$i];
      $suitid = $obj->MySQLQueryPerform("events_suitabletype",$suitData,'insert');
    }
    
    $var_msg = "Event updated successfully.";
    header("Location:".$tconfig["tsite_url"]."index.php?file=o-myevents&var_msg=".$var_msg);
    exit;
      
  }else{
    /* Server side validation start */
    include_once(TPATH_LIBRARIES."/general/validation.class.php");
    $validobj = new validation();
    
    $validobj->add_fields($_POST['eEventType'], 'req', 'Please select Event Date Type');
    $validobj->add_fields($_POST['vTitle'], 'req', 'Please enter Event Title');
    $validobj->add_fields($_POST['vAddress'], 'req', 'Please enter Event Address');  
    
    $error = $validobj->validate();
    if($error){
    	$generalobj->getPostFormData($_POST,$error,$tconfig['tsite_url']."add-event");
    }
    
    $Data['dAddedDate'] = date("Y-m-d H:i:s");
    $Data['iOrganizationId'] = $_SESSION['sess_iOrganizationId'];     
    $Data['vTitle'] = $_REQUEST['vTitle'];
    $Data['eEventType'] = $_REQUEST['eEventType'];
    $Data['dStartDate'] = $_REQUEST['dStartDate'];
    $Data['dEndDate'] = $_REQUEST['dEndDate'];
    $Data['vStartTime'] = $_REQUEST['vStartTime'];
    $Data['vEndTime'] = $_REQUEST['vEndTime'];
    $Data['vAddress'] = $_REQUEST['vAddress'];
    $Data['tDetails'] = $_REQUEST['tDetails'];
    $Data['tRequirements'] = $_REQUEST['tRequirements'];
    $Data['vCountry'] = $_REQUEST['vCountry'];
    $Data['vCity'] = $_REQUEST['vCity'];
    $Data['vState'] = $_REQUEST['vState'];
    
    $id = $obj->MySQLQueryPerform("event",$Data,'insert');
    
    if($_REQUEST['uplogo'] != '' && $id != ''){
      include_once(TPATH_LIBRARIES."/general/Imagecrop.class.php");
      $thumb = new thumbnail(); 
      
      $Photo_folder = $tconfig["tsite_upload_images_events_path"].$id."/";
      $target_path = $tconfig['tsite_upload_images_tmpphotos_path'].$_SESSION['iTempUserId']."/";
      if(!is_dir($Photo_folder)){
      	mkdir($Photo_folder, 0777);
      }        
      $image = $generalobj->img_data_upload($target_path,$_REQUEST['uplogo'],$Photo_folder, $tconfig["tsite_upload_images_events_size1"], $tconfig["tsite_upload_images_events_size2"], $tconfig["tsite_upload_images_events_size3"],$tconfig["tsite_upload_images_events_size4"], "y");
      unlink($target_path.$_REQUEST['uplogo']);
      rmdir($tconfig['tsite_upload_images_tmpphotos_path'].$_SESSION['iTempUserId']);
      
      $sql = "UPDATE event SET vImage = '".$image."' WHERE iEventId = '".$id."'";
      $db_sql=$obj->sql_query($sql);	
    }
    
    if($id != '' && count($_REQUEST['iCategoryId']) > 0){
      for($i=0;$i<count($_REQUEST['iCategoryId']);$i++){
        $CatData['iEventId'] = $id;
        $CatData['iCategoryId'] = $_REQUEST['iCategoryId'][$i];
        $catid = $obj->MySQLQueryPerform("events_category",$CatData,'insert');
      }     
    }
    
    if($id != '' && count($_REQUEST['iSkillId']) > 0){
      for($i=0;$i<count($_REQUEST['iSkillId']);$i++){
        $skillData['iEventId'] = $id;
        $skillData['iSkillId'] = $_REQUEST['iSkillId'][$i];
        $skillid = $obj->MySQLQueryPerform("event_skills",$skillData,'insert');
      }     
    }
    
    if($id != '' && count($_REQUEST['iSuitableTypeId']) > 0){
      for($i=0;$i<count($_REQUEST['iSuitableTypeId']);$i++){
        $suitData['iEventId'] = $id;
        $suitData['iSuitableTypeId'] = $_REQUEST['iSuitableTypeId'][$i];
        $suitid = $obj->MySQLQueryPerform("events_suitabletype",$suitData,'insert');
      }     
    } 
    $var_msg = "New event added successfully.";
    header("Location:".$tconfig["tsite_url"]."index.php?file=o-myevents&var_msg=".$var_msg);
    exit;
  }
exit;       
?>