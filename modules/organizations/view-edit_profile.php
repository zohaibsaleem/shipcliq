<?php   
  $check_organization = $organizationsobj->check_organization($iOrganizationId); 
  $iOrganizationId = $_SESSION["sess_iOrganizationId"];
  
  $organization_details = $organizationsobj->profile_details($iOrganizationId);
  $countries = $organizationsobj->get_countires();
  $interest_areas = $organizationsobj->interest_areas_registration();
  $organization_interest_areas = $organizationsobj->organization_interest_areas($iOrganizationId);
  
  $smarty->assign("var_msg",$_REQUEST['var_msg']);
  $smarty->assign("organization",$organization_details);
  $smarty->assign("db_country",$countries);
  $smarty->assign("iOrganizationId",$iOrganizationId);
  $smarty->assign("interest_areas",$interest_areas);
  $smarty->assign("organization_interest_areas",array_map('current', $organization_interest_areas));
?>