<?php   
  $check_organization = $organizationsobj->check_organization($iOrganizationId); 
  $iOrganizationId = $_SESSION["sess_iOrganizationId"];
  $iEventId = $_REQUEST['iEventId'];
  
  $event_details_subscription = $organizationsobj->event_details_subscription($iEventId, $iOrganizationId);
  $event_attending = $organizationsobj->event_attending($iEventId, $iOrganizationId);   
  
  $smarty->assign("event_details_subscription",$event_details_subscription);
  $smarty->assign("subscriptions",$event_attending);     
  $smarty->assign("iEventId",$iEventId);        
?>