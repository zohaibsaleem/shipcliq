<?php 
 
  $interest_areas = $organizationsobj->interest_areas_registration();
  $countries = $organizationsobj->get_countires();
  
  if($_SESSION['iTempUserId'] == ''){
    $_SESSION['iTempUserId'] = substr(number_format(time() * rand(),0,'',''),0,10);
  }     
  
  $Data['uplogo'] = $_REQUEST['uplogo']; 
  $Data['vEmail'] = $_REQUEST['vEmail'];
  $Data['vOrganizationName'] = $_REQUEST['vOrganizationName'];
  $Data['vFirstName'] = $_REQUEST['vFirstName'];
  $Data['vLastName'] = $_REQUEST['vLastName'];
  $Data['vAddress'] = $_REQUEST['vAddress'];
  $Data['vCity'] = $_REQUEST['vCity'];
  $Data['vCountry'] = $_REQUEST['vCountry'];
  $Data['vState'] = $_REQUEST['vState'];
  $Data['vPhone'] = $_REQUEST['vPhone'];
  $Data['vFax'] = $_REQUEST['vFax'];
  $Data['vURL'] = $_REQUEST['vURL'];
  $Data['tDescription'] = $_REQUEST['tDescription'];
  $Data['iCategoryId'] = $_REQUEST['Data'];
  
  if($Data['uplogo'] != ''){
    $target_path = $tconfig['tsite_upload_images_tmpphotos_path'].$_SESSION['iTempUserId']."/";
    if(is_file($target_path.'/'.$Data['uplogo']))
    {
      $rrb = 'yes';
      list($width, $height, $type, $attr)= getimagesize($target_path.'/'.$Data['uplogo']);     
      $percent_resizing = 30;    
      $new_width = round((($percent_resizing/100)*$width));
      $new_height = round((($percent_resizing/100)*$height));  
      
      $Data['imageurl'] = $tconfig['tsite_upload_images_tmpphotos'].$_SESSION['iTempUserId'].DS.$Data['uplogo'];
      $Data['width'] = $new_width;
      $Data['height'] = $new_height;
    } 
  } 

  $smarty->assign("interest_areas",$interest_areas);
  $smarty->assign("db_country",$countries);
  $smarty->assign("iTempUserId",$_SESSION['iTempUserId']);
  $smarty->assign("Data",$Data);
  $smarty->assign("var_msg",$_REQUEST['var_msg']);
?>