<?php  
  $generalobj->checkDuplicateFront('vEmail', "organization" , Array('vEmail'),$tconfig["tsite_url"]."organization-registration", "Email address you have entered is already exists. Please try with another E-mail.", "");
  
  /* Server side validation start */
  include_once(TPATH_LIBRARIES."/general/validation.class.php");
  $validobj = new validation();
  
  $validobj->add_fields($_POST['vOrganizationName'], 'req', 'Please enter Organization Name');
  $validobj->add_fields($_POST['vFirstName'], 'req', 'Please enter Contact Person First Name');
  $validobj->add_fields($_POST['vLastName'], 'req', 'Please enter Contact Person Last Name');
  $validobj->add_fields($_POST['vEmail'], 'req', 'Please enter E-mail');
  $validobj->add_fields($_POST['vPassword'], 'req', 'Please enter Password');
  $validobj->add_fields($_POST['vAddress'], 'req', 'Please enter Address');
  $validobj->add_fields($_POST['vCity'], 'req', 'Please enter City');
  $validobj->add_fields($_POST['vCountry'], 'req', 'Please select Country');
  $validobj->add_fields($_POST['vState'], 'req', 'Please select State');  
  
  $error = $validobj->validate();
  if($error){
  	$generalobj->getPostFormData($_POST,$error,$tconfig['tsite_url']."organization-registration");
  }
  
  $Data['vEmail'] = $_REQUEST['vEmail'];
  $Data['vPassword'] = $generalobj->encrypt($_REQUEST['vPassword']);
  $Data['vOrganizationName'] = $_REQUEST['vOrganizationName'];
  $Data['vFirstName'] = $_REQUEST['vFirstName'];
  $Data['vLastName'] = $_REQUEST['vLastName'];
  $Data['vAddress'] = $_REQUEST['vAddress'];
  $Data['vCity'] = $_REQUEST['vCity'];
  $Data['vCountry'] = $_REQUEST['vCountry'];
  $Data['vState'] = $_REQUEST['vState'];
  $Data['vPhone'] = $_REQUEST['vPhone'];
  $Data['vFax'] = $_REQUEST['vFax'];
  $Data['vURL'] = $_REQUEST['vURL'];
  $Data['tDescription'] = $_REQUEST['tDescription']; 
  $Data['eType'] = 'Organization'; 
  $Data['eStatus'] = 'Pending';
  $Data['dAddedDate'] = date("Y-m-d H:i:s");
  
  $id = $obj->MySQLQueryPerform("organization",$Data,'insert');
  
  if($_REQUEST['uplogo'] != '' && $id != ''){
    include_once(TPATH_LIBRARIES."/general/Imagecrop.class.php");
    $thumb = new thumbnail(); 
    
    $Photo_folder = $tconfig["tsite_upload_images_organization_path"].$id."/";
    $target_path = $tconfig['tsite_upload_images_tmpphotos_path'].$_SESSION['iTempUserId']."/";
    if(!is_dir($Photo_folder)){
    	mkdir($Photo_folder, 0777);
    }               
    $image = $generalobj->img_data_upload($target_path,$_REQUEST['uplogo'],$Photo_folder, $tconfig["tsite_upload_images_organization_size1"], $tconfig["tsite_upload_images_organization_size2"], $tconfig["tsite_upload_images_organization_size3"],$tconfig["tsite_upload_images_organization_size4"], "y");
    unlink($target_path.$_REQUEST['uplogo']);
    rmdir($tconfig['tsite_upload_images_tmpphotos_path'].$_SESSION['iTempUserId']);
    
    $sql = "UPDATE organization SET vImage = '".$image."' WHERE iOrganizationId = '".$id."'";
    $db_sql=$obj->sql_query($sql);	
  }
  
  if($id != '' && count($_REQUEST['iCategoryId']) > 0){
    for($i=0;$i<count($_REQUEST['iCategoryId']);$i++){
      $CatData['iOrganizationId'] = $id;
      $CatData['iCategoryId'] = $_REQUEST['iCategoryId'][$i];
      $catid = $obj->MySQLQueryPerform("organization_interests",$CatData,'insert');
    }     
  }
  
  if($id != '') {
    $generalobj->send_email_user("MEMBER_REGISTRATION_USER",$Data);
    $generalobj->send_email_user("MEMBER_REGISTRATION_ADMIN",$Data);
  }
  
  header("Location:".$tconfig["tsite_url"]."organization-registration-confirmation");
  exit;
?>