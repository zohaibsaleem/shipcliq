<?php
  ob_start();      
  session_start();
  define( '_TEXEC', 1 );
  define('TPATH_BASE', dirname(__FILE__) );
  define( 'DS', DIRECTORY_SEPARATOR );
  
  require_once ( TPATH_BASE .DS.'includes'.DS.'defines.php' ); 
  require_once ( TPATH_BASE .DS.'includes'.DS.'configuration.php' );
  
  #Include language lable file#
  include_once($tconfig["tsite_label_path"].$_SESSION['sess_lang']."/".$_SESSION['sess_lang'].".php");

  
  function ConvertTimezoneToAnotherTimezone($time, $currentTimezone, $timezoneRequired) {
    $dayLightFlag = false;
    $dayLgtSecCurrent = $dayLgtSecReq = 0;
    $system_timezone = date_default_timezone_get();
    $local_timezone = $currentTimezone;
    date_default_timezone_set($local_timezone);
    $local = date("Y-m-d H:i:s");
    /* Uncomment if daylight is required */
    //        $daylight_flag = date("I", strtotime($time));
    //        if ($daylight_flag == 1) {
    //            $dayLightFlag = true;
    //            $dayLgtSecCurrent = -3600;
    //        }
    date_default_timezone_set("GMT");
    $gmt = date("Y-m-d H:i:s ");
  
    $require_timezone = $timezoneRequired;
    date_default_timezone_set($require_timezone);
    $required = date("Y-m-d H:i:s ");
    /* Uncomment if daylight is required */
    //        $daylight_flag = date("I", strtotime($time));
    //        if ($daylight_flag == 1) {
    //            $dayLightFlag = true;
    //            $dayLgtSecReq = +3600;
    //        }
  
    date_default_timezone_set($system_timezone);
  
    $diff1 = (strtotime($gmt) - strtotime($local));
    $diff2 = (strtotime($required) - strtotime($gmt));
  
    $date = new DateTime($time);
  
    $date->modify("+$diff1 seconds");
    $date->modify("+$diff2 seconds");
  
    if ($dayLightFlag) {
        $final_diff = $dayLgtSecCurrent + $dayLgtSecReq;
        $date->modify("$final_diff seconds");
    }
  
    $timestamp = $date->format("Y-m-d H:i:s");
  
    return $timestamp;
  }
  
  function generateCode($numchars=5,$digits=1,$letters=1)
  {
    $dig = "012345678923456789";
    $abc = "ABCDEFGHJKLMNOPQRSTUVWXYZ";
    if($letters == 1)
    {
      $str .= $abc;
    }
    if($digits == 1)
    {
      $str .= $dig;
    }
    for($i=0; $i < $numchars; $i++)
    {
      $randomized .= $str{rand() % strlen($str)};
    }
    return $randomized;
  }
  
  $currnttz = date_default_timezone_get();     
  $cuurenttime = date('H:i', time());
  
  $sql = "SELECT * FROM booking_new WHERE eStatus = 'Active' AND vSmsCode = ''";
  $db_bookings = $obj->MySQLSelect($sql);
  
  for($i=0;$i<count($db_bookings);$i++){
    # Get Language preference of Booker #
    $sql = "SELECT vLanguageCode FROM member WHERE vEmail = '".$db_bookings[$i]['vBookerEmail']."' AND iMemberId = '".$db_bookings[$i]['iBookerId']."'";
    $db_mem_mail = $obj->MySQLSelect($sql);
    if(count($db_mem_mail) == 0){
       $db_mem_mail[0]['vLanguageCode'] = "EN";
    }
    $sql = "SELECT * from language_label WHERE vCode = '".$db_mem_mail[0]['vLanguageCode']."'";
    $db_label = $obj->MySQLSelect($sql);  
    $vLabel = array(); 
    for($j=0;$j<count($db_label);$j++){
  		$vLabel[$db_label[$j]['vLabel']]  = $db_label[$j]["vValue"];
  	}
    # Get Language preference of Booker #
    $usertz = $db_bookings[$i]['vTimeZone'];
    $userdt = ConvertTimezoneToAnotherTimezone($cuurenttime, $currnttz, $usertz);
    if($userdt != ''){
      $datetime = explode(' ', $userdt);
      $Date = $datetime[0];
      $Time = $datetime[1];
      
      if(strtotime($db_bookings[$i]['dBookingDate']) == strtotime($Date)){
        $Start = $db_bookings[$i]['dBookingTime'];
        $Minutes = 60;
        $To = date("H:i:s", strtotime($Start)+($Minutes*60));
       
        if(strtotime($userdt) >= strtotime($db_bookings[$i]['dBookingDate'].' '.$Start) && strtotime($userdt) <= strtotime($db_bookings[$i]['dBookingDate'].' '.$To)){
           $code = $db_bookings[$i]['vBookingNo'].generateCode('3','1','1');
           
           $sql = "UPDATE booking_new SET vSmsCode = '".$code."' WHERE iBookingId = '".$db_bookings[$i]['iBookingId']."'";
           $db_sql=$obj->sql_query($sql);	
           
           if($db_sql){
            $mailcont = '';
            $mailcont .= '<table width="600" style="border:1px solid #cccccc;border-collapse: collapse;background-color: #FFFFFF;">
                <tr>
                  <td width="100%" colspan="1" style="background:#44AA00;color:#FFF;padding:10px;text-align:left;font-size:17px;font-family:Arial, Helvetica, sans-serif;">'.$vLabel['LBL_TRIP_COMPLETE'].'</td>
                </tr> 
                <tr>                   
                  <td width="100%" valign="top" style="border-bottom:0px solid #cccccc;">
                    <table width="100%">     
                      <tr>
                        <td valign="top" style="color:#44AA00;padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
                          <span style="color:#000;">'.$vLabel['LBL_TRIP_OF'].':</span> '.$db_bookings[$i]['vFromPlace'].' &rarr; '.$db_bookings[$i]['vToPlace'].'
                        </td>
                      </tr>                       
                      <tr>
                        <td valign="top" style="color:#44AA00;padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
                          <span style="color:#000;">'.$vLabel['LBL_DATE'].':</span> '.$generalobj->DateTime($db_bookings[$i]['dBookingDate'],10).' 
                        </td>
                      </tr>
                      <tr>
                        <td valign="top" style="color:#44AA00;padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
                          <span style="color:#000;">'.$vLabel['LBL_TIME'].':</span> '.$generalobj->DateTime($db_bookings[$i]['dBookingTime'],12).' 
                        </td>
                      </tr>                       
                      <tr>
                        <td valign="top" style="color:#44AA00;padding:0 0 10px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
                          <span style="color:#000;">'.$vLabel['LBL_BOOKER_NAME'].' :</span> '.$db_bookings[$i]['vBookerFirstName'].' '.$db_bookings[$i]['vBookerLastName'].' 
                        </td>
                      </tr>
                      <tr>
                        <td valign="top" style="background:#cccccc;color:#44AA00;padding:10px 10px 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
                          <span style="color:#000;font-size:17px;">'.$vLabel['LBL_TRIP_COMPLETE_MSG'].'</span>
                        </td>
                      </tr>  
                      <tr>
                        <td valign="top" style="background:#cccccc;color:#44AA00;padding:10px 10px 10px 5px;text-align:left;font-size:15px;font-family:Arial, Helvetica, sans-serif;">
                          <span style="color:#000;font-size:17px;">'.$vLabel['LBL_MSG_CODE'].' :</span> <span style="color:#41A317;font-size:20px;">'.$code.'</span>
                        </td>
                      </tr>                        
                    </table>
                  </td>
                </tr>                                                
              </table>';
              
              $maildata['vBookerEmail'] = $db_bookings[$i]['vBookerEmail'];
              $maildata['details'] = $mailcont;
              $generalobj->send_email_user("TRIP_COMPLETION_MESSAGE",$maildata); 
			  
			  $sql = "SELECT vPhoneCode FROM country WHERE vCountryCode='". $db_bookings[$i]['vBookerCountry']."'";
			  $db_countrysmscode =  $obj->MySQLSelect($sql);
			  
			  if($db_countrysmscode[0]['vPhoneCode'] != ""){
			  	if($db_countrysmscode[0]['vPhoneCode'] != ""){
				  $bookerphonenumber = $db_countrysmscode[0]['vPhoneCode'].$db_bookings[$i]['vBookerPhone'];                                                                                                               
				  $generalobj->send_sms($bookerphonenumber,"Please reply to this SMS by sending code ".$code." to confirm your todays trip booked on projectName. In case if you can't reply to this SMS send the  code to +13238928019.");
				}	
			  }
           }
        }
      } 
    }
  }  
  echo 'rb';
  exit;
?>

