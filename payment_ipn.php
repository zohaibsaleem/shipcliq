<?php 
  ob_start();
  define( '_TEXEC', 1 );
  define('TPATH_BASE', dirname(__FILE__) );
  define( 'DS', DIRECTORY_SEPARATOR );
  //header('Content-Type: text/html; charset=iso-8859-1');
  
  require_once ( TPATH_BASE .DS.'includes'.DS.'defines.php' ); 
  require_once ( TPATH_BASE .DS.'includes'.DS.'configuration.php' );
  
  // Send an empty HTTP 200 OK response to acknowledge receipt of the notification 
  header('HTTP/1.1 200 OK'); 
  
  // Assign payment notification values to local variables
  $txn_type = $_POST['txn_type'];  
  $subscr_id = $_POST['subscr_id'];   
  $custom = $_POST['custom'];
  $subscr_date = $_POST['subscr_date'];
  $payment_status = $_POST['payment_status'];
  $txn_id = $_POST['txn_id'];
  // Build the required acknowledgement message out of the notification just received
  $req = 'cmd=_notify-validate';               // Add 'cmd=_notify-validate' to beginning of the acknowledgement

  foreach ($_POST as $key => $value) {         // Loop through the notification NV pairs
    $value = urlencode(stripslashes($value));  // Encode these values
    $req  .= "&$key=$value";                   // Add the NV pairs to the acknowledgement
  }
  
  // Set up the acknowledgement request headers
  $header  = "POST /cgi-bin/webscr HTTP/1.1\r\n";                    // HTTP POST request
  $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
  $header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
 
  // Open a socket for the acknowledgement request
  $fp = fsockopen('ssl://www.sandbox.paypal.com', 443, $errno, $errstr, 30);

  // Send the HTTP POST request back to PayPal for validation
  fputs($fp, $header . $req);
  
  if($payment_status == 'Pending' || $payment_status == 'Completed'){      
    $sql = "SELECT * FROM employer_payment WHERE iEmployerPaymentId = '".$custom."'";
    $db_emp_payment = $obj->MySQLSelect($sql);
    
    if($db_emp_payment[0]['ePlanPurchaseType'] == 'New'){
      if($db_emp_payment[0]['vTransactionId'] == ''){
        $sql = "UPDATE employer_payment SET vTransactionId = '".$txn_id."', eStatus = 'Approved' WHERE iEmployerPaymentId = '".$custom."'";
        $obj->sql_query($sql);
        
        $sql = "SELECT * FROM employer WHERE iEmployerId = '".$db_emp_payment[0]['iEmployerId']."'";
        $db_employer = $obj->MySQLSelect($sql);
        
        $sql = "UPDATE employer SET eStatus = 'Active' WHERE iEmployerId = '".$db_emp_payment[0]['iEmployerId']."'";
        $obj->sql_query($sql);
        
        $Data_email['NAME'] = $db_employer[0]['vFirstName'].' '.$db_employer[0]['vLastName'];
        $Data_email['PLAN'] = $db_emp_payment[0]['vPlanName'];
        $Data_email['DUARATION'] = $db_emp_payment[0]['iDuration'].' '.$db_emp_payment[0]['eDurationType'];
        $Data_email['SATRTDATE'] = $generalobj->DateTime($db_emp_payment[0]['dActivationDate'],11);
        $Data_email['ENDDATE'] = $generalobj->DateTime($db_emp_payment[0]['dExpireDate'],11);
        $Data_email['PRICE'] = $generalobj->Make_Currency($db_emp_payment[0]['fPrice']);
        $Data_email['PAYBY'] = $db_emp_payment[0]['ePaymentOption'];
        $Data_email['EMAIL'] = $db_employer[0]['vEmail'];
        $Data_email['PASSWORD'] = $generalobj->decrypt($db_employer[0]['vPassword']);
        $Data_email['LOGINLINK'] = '<a href="'.$tconfig['tsite_url'].'index.php?file=e-login">Login Now</a>';
        $Data_email['BILLINGNAME'] = $db_emp_payment[0]['vBFirstName'].' '.$db_emp_payment[0]['vBLastName'];
        
        $mail = $generalobj->send_email_user('EMPLOYER_REGISTRATION', $Data_email);
        $mail = $generalobj->send_email_user('EMPLOYER_REGISTRATION_ADMIN', $Data_email);        
      }
    }else{
      if($db_emp_payment[0]['vTransactionId'] == ''){
        $sql = "UPDATE employer_payment SET vTransactionId = '".$txn_id."', eStatus = 'Approved' WHERE iEmployerPaymentId = '".$custom."'";
        $obj->sql_query($sql);
        
        $sql = "SELECT * FROM employer WHERE iEmployerId = '".$db_emp_payment[0]['iEmployerId']."'";
        $db_employer = $obj->MySQLSelect($sql);
       
        $Data_email['NAME'] = $db_employer[0]['vFirstName'].' '.$db_employer[0]['vLastName'];
        $Data_email['PLAN'] = $db_emp_payment[0]['vPlanName'];
        $Data_email['DUARATION'] = $db_emp_payment[0]['iDuration'].' '.$db_emp_payment[0]['eDurationType'];
        $Data_email['SATRTDATE'] = $generalobj->DateTime($db_emp_payment[0]['dActivationDate'],11);
        $Data_email['ENDDATE'] = $generalobj->DateTime($db_emp_payment[0]['dExpireDate'],11);
        $Data_email['PRICE'] = $generalobj->Make_Currency($db_emp_payment[0]['fPrice']);
        $Data_email['PAYBY'] = $db_emp_payment[0]['ePaymentOption'];
        $Data_email['EMAIL'] = $db_employer[0]['vEmail'];
        $Data_email['PASSWORD'] = $generalobj->decrypt($db_employer[0]['vPassword']);
        $Data_email['LOGINLINK'] = '<a href="'.$tconfig['tsite_url'].'index.php?file=e-login">Login Now</a>';
        $Data_email['BILLINGNAME'] = $db_emp_payment[0]['vBFirstName'].' '.$db_emp_payment[0]['vBLastName'];
        
        $mail = $generalobj->send_email_user('EMPLOYER_PLAN_UPDATE', $Data_email);
        $mail = $generalobj->send_email_user('EMPLOYER_PLAN_UPDATE_ADMIN', $Data_email);         
      }
    }
  }
  
  exit;  
  
  while (!feof($fp)) {                     // While not EOF
    $res = fgets($fp, 1024);               // Get the acknowledgement response
    if (strcmp ($res, "VERIFIED") == 0) {  // Response contains VERIFIED - process notification

      // Send an email announcing the IPN message is VERIFIED
      $EMAIL_FROM_NAME = 'Roy Miller';
      $NOREPLY_EMAIL = 'harshilmehta1982@gmail.com';
      $headers = '';
      $headers = "MIME-Version: 1.0\n";
      $headers .= "Content-type: text/html; charset=iso-8859-2\nContent-Transfer-Encoding: 8bit\nX-Priority: 1\nX-MSMail-Priority: High\n";
      $headers .= "From: ".$EMAIL_FROM_NAME." < $NOREPLY_EMAIL >"."\n"."X-Mailer: PHP/".phpversion()."\nX-originating-IP: ".$_SERVER['REMOTE_ADDR']."\n";
      
      $emailsend = mail('rahulb.esw@gmail.com', 'IPN MESSAGE', $req, $headers); 

      // Authentication protocol is complete - OK to process notification contents

      // Possible processing steps for a payment include the following:

      // Check that the payment_status is Completed
      // Check that txn_id has not been previously processed
      // Check that receiver_email is your Primary PayPal email
      // Check that payment_amount/payment_currency are correct
      // Process payment
    } 
    else if (strcmp ($res, "INVALID") == 0) { //Response contains INVALID - reject notification

      // Authentication protocol is complete - begin error handling

      // Send an email announcing the IPN message is INVALID
      $mail_From    = "IPN@example.com";
      $mail_To      = "rahulb.esw@gmail.com";
      $mail_Subject = "INVALID IPN";
      $mail_Body    = $req;

      mail($mail_To, $mail_Subject, $mail_Body, $mail_From);
    }
  }
  
  fclose($fp);  // Close the file
  
  echo 'rb'; exit;
?>