function checkvalidate(frm)
{
   var fname = document.getElementById("vFirstName").value;
   var lname = document.getElementById("vLastName").value;
   var contactno = document.getElementById("vContactNo").value;
   var phoneno = document.getElementById("vPhone").value;
   var zipno = document.getElementById("vZip").value;
   var email = document.getElementById("vEmail").value;
   var username = document.getElementById("vUserName").value;
   var password = document.getElementById("vPassword").value; 
   var newpassword = document.getElementById("vNewPassword").value;
   
   if(fname == ''){
    alert("Please Enter your First Name");
		frm.vFirstName.focus();
		return false;
   }
   if(lname == ''){
    alert("Please Enter your Last Name");
		frm.vLastName.focus();
		return false;
   }
   var stripped = contactno.replace(/[\.\@\!\#\$\%\^\&\*\=\_\|\ ]/g, '');
   var stripped = phoneno.replace(/[\.\@\!\#\$\%\^\&\*\=\_\|\ ]/g, '');
   var stripped = zipno.replace(/[\.\@\!\#\$\%\^\&\*\=\_\|\ ]/g, '');
   
   if (contactno == "") {
          alert("Please Enter your Contact No");
          frm.vContactNo.focus();   
		      return false;
    }
    if (isNaN(parseInt(stripped))) {
          alert("The Contact No contains illegal characters.");
          frm.vContactNo.focus();   
		      return false;
    } 
    if (isNaN(parseInt(stripped))) {
          alert("The Phone No contains illegal characters.");
          frm.vPhone.focus();   
		      return false;
    } 
     if (isNaN(parseInt(stripped))) {
          alert("The Zip No contains illegal characters.");
          frm.vZip.focus();   
		      return false;
    } 
    if(email == ''){
        alert("Please enter your E-mail Id");
        frm.vEmail.focus();
  		  return false;
    }      
    if(username == ''){
    alert("Please Enter your User Name");
		frm.vUserName.focus();
		return false;
   }
    if(password == ''){
    alert("Please Enter your Password");
		frm.vPassword.focus();
		return false;
   } 
   if(newpassword == ''){
    alert("Please Enter your Confirm Password");
		frm.vNewPassword.focus();
		return false;
   } 
   if(password != newpassword){
    alert("Passwords do not match!");
		frm.vNewPassword.focus();
		return false;
   }
   if(email != ''){
      var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;  
        if(email.match(mailformat))  
        {  
        return true;  
        }  
        else  
        {  
        alert("You have entered an invalid email address!");  
        frm.vEmail.focus();  
        return false;  
        }  
    }     

   	frm.submit();
}