function getHTTPObject()
{
	// code for Mozilla, etc.
	if (window.XMLHttpRequest)
  	{
  		xmlhttp=new XMLHttpRequest()
  	}
// code for IE
	else if (window.ActiveXObject)
  	{
  		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP")
  	}
	return xmlhttp;
}
var http = getHTTPObject();
var http1 = getHTTPObject();
var http2 = getHTTPObject();
var http3 = getHTTPObject();
var http4 = getHTTPObject();

function get_state_ajax_registration(val)
{
    
	document.getElementById("state_combo_billing").style.display='none';
	document.getElementById("loading_state_billing").style.display='';
	var selected_state_registration = document.getElementById("selected_state_registration").value;
	country_id = val;
	var url = site_url+"ajax_file/get_state_ajax_registration.php";
	url = url + "?country_id="+country_id+"&selected_state_registration="+selected_state_registration;
    
	http4.open("GET", url, true);
	http4.onreadystatechange = get_state_combo_registration;
    http4.send(null);
}

function get_state_combo_registration()
{
	if (http4.readyState == 4)
	{
   		var responce_text = http4.responseText; 
		document.getElementById("loading_state_billing").style.display='none';
		document.getElementById("state_combo_billing").style.display='';
		document.getElementById("state_combo_billing").innerHTML = responce_text;
	}
}


function get_state_ajax_billing(val)
{ 
	document.getElementById("state_combo_billing").style.display='none';
	document.getElementById("loading_state_billing").style.display='';
	var selected_state_billing = document.getElementById("selected_state_billing").value;
	country_id = val;

	var url = site_url+"/index.php?file=ax-get_state_ajax_billing";
	url = url + "&country_id="+country_id+"&selected_state_billing="+selected_state_billing;
	//alert(url);
	http1.open("GET", url, true);
	http1.onreadystatechange = get_state_combo_billing;
    http1.send(null);
}

function get_state_ajax(val)
{ 
 //alert(val);
	document.getElementById("state_combo_billing").style.display='none';
	document.getElementById("loading_state_billing").style.display='';
	var selected_state_billing = document.getElementById("selected_state_billing").value;
	 //alert(selected_state_billing);
	country_id = val;
 // alert(site_url);
	var url = site_url+"/index.php?file=ax-get_state_ajax";
	 
	url = url + "&country_id="+country_id+"&selected_state_billing="+selected_state_billing;
	 
	http1.open("GET", url, true);
	http1.onreadystatechange = get_state_combo_billing;
    http1.send(null);
}

function get_state_ajax_billing_front(val)
{
	document.getElementById("state_combo_billing").style.display='none';
	document.getElementById("loading_state_billing").style.display='';
	var selected_state_billing = document.getElementById("selected_state_billing").value;
	country_id = val;
	var url = site_url+"/index.php?file=ax-get_state_ajax_billing_front";
	url = url + "&country_id="+country_id+"&selected_state_billing="+selected_state_billing;
	http1.open("GET", url, true);
	http1.onreadystatechange = get_state_combo_billing;
    http1.send(null);
}

function get_state_combo_billing()
{
	if (http1.readyState == 4)
	{
   		var responce_text = http1.responseText; 
		document.getElementById("loading_state_billing").style.display='none';
		document.getElementById("state_combo_billing").style.display='';
		document.getElementById("state_combo_billing").innerHTML = responce_text;
	}
}

function get_state_ajax_shipping(val)
{ 
	document.getElementById("state_combo_shipping").style.display='none';
	document.getElementById("loading_state_shipping").style.display='';
    var selected_state_shipping = document.getElementById("selected_state_shipping").value;
	//document.getElementById("selected_state_shipping").value = val;
	country_id = val;
	var url = site_url+"/index.php?file=ax-get_state_ajax_shipping";
	url = url + "&country_id="+country_id+"&selected_state_shipping="+selected_state_shipping;
	http.open("GET", url, true);
	http.onreadystatechange = get_state_combo_shipping;
    http.send(null);
}

function get_state_combo_shipping()
{
	if (http.readyState == 4)
	{
   	var responce_text = http.responseText;
		document.getElementById("loading_state_shipping").style.display='none';
		document.getElementById("state_combo_shipping").style.display='';
		document.getElementById("state_combo_shipping").innerHTML = responce_text;
	}
}




function get_city_ajax_billing(val)
{
	document.getElementById("city_combo_billing").style.display='none';
	document.getElementById("loading_city_billing").style.display='';
	var selected_city_billing = document.getElementById("selected_city_billing").value;
	state_id = val;
	document.getElementById("state_billing").value = val;
	var url = site_url+"/index.php?file=ax-get_city_ajax_billing";
	url = url + "&city_id="+state_id+"&selected_city_billing="+selected_city_billing;
	http2.open("GET", url, true);
	http2.onreadystatechange = get_city_combo_billing;
    http2.send(null);
}  

function get_city_ajax_billing_front(val)
{
	document.getElementById("city_combo_billing").style.display='none';
	document.getElementById("loading_city_billing").style.display='';
	var selected_city_billing = document.getElementById("selected_city_billing").value;
	state_id = val;
	document.getElementById("state_billing").value = val;
	var url = site_url+"/index.php?file=ax-get_city_ajax_billing_front";
	url = url + "&city_id="+state_id+"&selected_city_billing="+selected_city_billing;
	http2.open("GET", url, true);
	http2.onreadystatechange = get_city_combo_billing;
    http2.send(null);
}

function get_city_combo_billing()
{
	if (http2.readyState == 4)
	{
   		var responce_text = http2.responseText; 
		document.getElementById("loading_city_billing").style.display='none';
		document.getElementById("city_combo_billing").style.display='';
		document.getElementById("city_combo_billing").innerHTML = responce_text;
	}
}

function get_city_value(val)
{
	document.getElementById("city_billing").value = val;
} 

function get_products(val)
{
  //alert(val);return false;	
  document.getElementById("loading_combo_products").style.display='none';
	document.getElementById("loading_products").style.display='';
	var selected_product = document.getElementById("selected_product").value;
	cat_id = val;
	var url = site_url+"/index.php?file=ax-get_product";
	url = url + "&cat_id="+cat_id+"&selected_product="+selected_product;
	http2.open("GET", url, true);
	http2.onreadystatechange = get_combo_product;
    http2.send(null);
}
function get_combo_product()
{
	if (http2.readyState == 4)
	{
   		var responce_text = http2.responseText; 
		document.getElementById("loading_products").style.display='none';
		document.getElementById("loading_combo_products").style.display='';
		document.getElementById("loading_combo_products").innerHTML = responce_text;
	}
} 
function get_products_combo(val)
{
  //alert(val);return false;	
  document.getElementById("loading_combo_products1").style.display='none';
	document.getElementById("loading_products1").style.display='';
	var selected_product1 = document.getElementById("selected_product1").value;
	cat_id = val;
	var url = site_url+"/index.php?file=ax-get_comboproduct";
	url = url + "&cat_id="+cat_id+"&selected_product="+selected_product1;
	http2.open("GET", url, true);
	http2.onreadystatechange = get_combo_product1;
    http2.send(null);
}
function get_combo_product1()
{
	if (http2.readyState == 4)
	{
   		var responce_text = http2.responseText; 
		document.getElementById("loading_products1").style.display='none';
		document.getElementById("loading_combo_products1").style.display='';
		document.getElementById("loading_combo_products1").innerHTML = responce_text;
	}
} 
/*********************************************************************************/
function get_state_ajax_billing_art(val)
{
	document.getElementById("state_combo_billing_art").style.display='none';
	document.getElementById("loading_state_billing").style.display='';
	var selected_state_billing = document.getElementById("selected_state_billing").value;
	country_id = val;
	var url = site_url+"/index.php?file=ax-get_state_ajax_billing_art";
	url = url + "&country_id="+country_id+"&selected_state_billing="+selected_state_billing;
	http3.open("GET", url, true);
	http3.onreadystatechange = get_state_combo_billing_art;
    http3.send(null);
}

function get_state_combo_billing_art()
{
	if (http3.readyState == 4)
	{
   		var responce_text = http3.responseText; 
		document.getElementById("loading_state_billing").style.display='none';
		document.getElementById("state_combo_billing_art").style.display='';
		document.getElementById("state_combo_billing_art").innerHTML = responce_text;
	}
}


function get_city_ajax_billing_art(val)
{
	document.getElementById("city_combo_billing_art").style.display='none';
	document.getElementById("loading_city_billing").style.display='';
	var selected_city_billing = document.getElementById("selected_city_billing").value;
	state_id = val;
	document.getElementById("state_billing").value = val;
	var url = site_url+"/index.php?file=ax-get_city_ajax_billing_art";
	url = url + "&city_id="+state_id+"&selected_city_billing="+selected_city_billing;
	http4.open("GET", url, true);
	http4.onreadystatechange = get_city_combo_billing_art;
    http4.send(null);
}

function get_city_combo_billing_art()
{
	if (http4.readyState == 4)
	{
   		var responce_text = http4.responseText; 
		document.getElementById("loading_city_billing").style.display='none';
		document.getElementById("city_combo_billing_art").style.display='';
		document.getElementById("city_combo_billing_art").innerHTML = responce_text;
	}
}
