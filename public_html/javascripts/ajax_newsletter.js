// JavaScript Document
function Trim(s) 
{
	return s.replace(/^\s+/g, '').replace(/\s+$/g, '');
}
function checknewsletter(){
	//resp = jQuery("#frmnewsletter").validationEngine('validate');
	jQuery("#frmnewsletter").validationEngine('attach',{promptPosition : "topRight", scroll: false});
	resp = jQuery("#frmnewsletter").validationEngine('validate');
	if(resp == true){
		$("#newletter_subscribe").hide(); 
    $("#newletter_loading").show();
		var request = $.ajax({
			type: "POST",		
			url: site_url+'index.php?file=ax-newsletter_subscription', 
			data: "vEmail="+$("#vNewsletterEmail").val(),		
			success: function(data) { //alert(Trim(data));return false;  
			$("#newletter_loading").hide();
      $("#newletter_subscribe").show();		
				if(Trim(data) == "done"){      
        $("#vNewsletterEmail").val("");
        //shownotificationMessage('You have successfully subscribed for our newsletters.', 'success');
        showNotification({duration : 3, autoClose : true, type : 'success', message: 'You have successfully subscribed for our newsletters.'});
        
					/*$.msgGrowl ({ 
						type: 'success'
						, title: 'Newsletter Subscription'
						, text: 'You have successfully subscribed for our newsletters.'
					});*/
				}else{     
				$("#vNewsletterEmail").val("");
				//shownotificationMessage('An email address provided is already subscribed for our newsletters.', 'error');
        showNotification({duration : 3, autoClose : true, type : 'error', message: 'An email address provided is already subscribed for our newsletters.'});
        /*	$.msgGrowl ({ 
						type: 'warning'
						, title: 'Newsletter Subscription'
						, text: 'An email address provided is already subscribed for our newsletters.'
					}); */
				} 
			}
		});
			
		request.fail(function(jqXHR, textStatus) {
		  alert( "Request failed: " + textStatus );
		});		
	}else{
		return false;
	}		
}