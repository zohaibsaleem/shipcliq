function removespace()
{
	val = event.keyCode;
	if(val == "32")
	{
		event.keyCode=0;
	}
	else
	{
		return true;
	}
}
function checkspace(events)
{
    var unicodes=events.charCode? events.charCode :events.keyCode	
    //alert(unicodes);

	if (unicodes!=8){ //if the key isn't the backspace key (which we should allow)
        if(unicodes == 32)
            return false;
		else
			return true;	 //disable key press
	}
}
function setSelectList(list_arr,selval)
{
	for(i=0;i<list_arr.length;i++)
	{
		if(list_arr[i].value == selval)
		{
			list_arr[i].selected=true;
			break;
		}
	}
}	
function inputvalidation_red(val)
{
    document.getElementById(val).className = 'input-red';
}	
function checkSearchBox()
{
	if(document.frmsearch.keyword.value.length < 1)
	{
		alert("Enter Any Keyword For Search");
		document.frmsearch.keyword.focus();
		return false;
	}
	if(document.frmsearch.vValue.value.length < 1)
	{
		alert("Enter Any Value For Search");
		document.frmsearch.vValue.focus();
		return false;
	}		
}
function newsletter_onsubmit(f){
	var result = isValidEmail(f.vEmail.value);
	if(!result)
	//alert("Please enter a valid e-mail address to subscribe to the newsletter");
	return result;
}
function Productsearch(f){
	if(f.txtsearch.value == "" || f.txtsearch.value == "Product OR Keyword")
	{
		alert("Please Enter Keyword");
		f.txtsearch.focus();
		return false;
	}
}

function checkValidQuantityNew(events)
{ 	
	var unicodes=events.charCode? events.charCode :events.keyCode;
	if(unicodes > 47 && unicodes < 58 || unicodes == '8' || unicodes == '37' || unicodes == '39')
	   return true;
	else
		return false;
}

function sortPrice()
{
	if(document.frmsortPrice.convcur.value != "")
		document.frmsortPrice.submit();
	else alert("Please Select a currency");
}
function number(value,length)
{
	chk1="1234567890.";
	for(i=0;i<length;i++)
	{
		ch1=value.charAt(i);
		rtn1=chk1.indexOf(ch1);
		if(rtn1==-1)
			return false;
	}
	return true;
}
function checkValidNumber()
{
	val = event.keyCode;
	if(val<48)
	  event.keyCode=0;
	if(val>57)
	  event.keyCode=0;
	return true;
}

function checkValidDigit()
{  
	val = event.keyCode;
	if(val<48 && val!=46)
	{
	  event.keyCode=0;
	}
	if(val>57)
	{
	  event.keyCode=0;
	}
	return true;
}
function phoneformate(value,length)
{
	chk1="1234567890()- ";
	for(i=0;i<length;i++)
	{
		ch1=value.charAt(i);
		rtn1=chk1.indexOf(ch1);
		if(rtn1==-1)
			return false;
	}
	return true;
}
function validZip(value,length)
{
	chk1="1234567890";
	for(i=0;i<length;i++)
	{
		ch1=value.charAt(i);
		rtn1=chk1.indexOf(ch1);
		if(rtn1==-1)
			return false;
	}
	return true;
}
function Trim(s)
{
  // Remove leading spaces and carriage returns
  
  while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r'))
  {
    s = s.substring(1,s.length);
  }

  // Remove trailing spaces and carriage returns

  while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length) == '\n') || (s.substring(s.length-1,s.length) == '\r'))
  {
    s = s.substring(0,s.length-1);
  }
  return s;
}

//******************************************************************************************//
//********************* functions  for email-id validation ****************************//
//******************************************************************************************//
function isValidEmail(emailStr) {
	var checkTLD=1;
	var knownDomsPat=/^(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum)$/;
	var emailPat=/^(.+)@(.+)$/;
	var specialChars="\\(\\)><@,;:\\\\\\\"\\.\\[\\]";
	var validChars="\[^\\s" + specialChars + "\]";
	var quotedUser="(\"[^\"]*\")";
	var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
	var atom=validChars + '+';
	var word="(" + atom + "|" + quotedUser + ")";
	var userPat=new RegExp("^" + word + "(\\." + word + ")*$");
	var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$");
	var matchArray=emailStr.match(emailPat);
	if (matchArray==null) {
		alert("Email address seems incorrect (check @ and .'s)");
		return false;
	}
	var user=matchArray[1];
	var domain=matchArray[2];
	// Start by checking that only basic ASCII characters are in the strings (0-127).
	for (i=0; i<user.length; i++) {
		if (user.charCodeAt(i)>127) {
			alert("Ths username contains invalid characters.");
			return false;
		}
	}
	for (i=0; i<domain.length; i++) {
		if (domain.charCodeAt(i)>127) {
			alert("Ths domain name contains invalid characters.");
			return false;
		}
	}
	if (user.match(userPat)==null) {
		alert("The username doesn't seem to be valid.");
		return false;
	}
	var IPArray=domain.match(ipDomainPat);
	if (IPArray!=null) {
		for (var i=1;i<=4;i++) {
			if (IPArray[i]>255) {
				alert("Destination IP address is invalid!");
				return false;
	   		}
		}
		return true;
	}
	var atomPat=new RegExp("^" + atom + "$");
	var domArr=domain.split(".");
	var len=domArr.length;
	for (i=0;i<len;i++) {
		if (domArr[i].search(atomPat)==-1) {
			alert("The domain name does not seem to be valid.");
			return false;
	   }	
	}
	if (checkTLD && domArr[domArr.length-1].length!=2 && 
		domArr[domArr.length-1].search(knownDomsPat)==-1) {
		alert("The address must end in a well-known domain or two letter " + "country.");
		return false;
	}

// Make sure there's a host name preceding the domain.

	if (len<2) {
		alert("This address is missing a hostname!");
		return false;
	}	
	return true;
}

function checkValidQuantity()
{
	val = event.keyCode;
	if(val<48)
	  event.keyCode=0;
	if(val>57)
	  event.keyCode=0;
	return true;
}


function go_to_page(val)
{
	window.location=val;
}


function resetstyle(frm)
{    
    var n = frm.elements.length;
	for(var i=0;i<n;i++)
	{
		var id = frm.elements[i].id;
        if(id != ""){
            inputvalidation(id);
        }
	}
}

function reset_errmessages(divid_arr){
    for(j=0;j<divid_arr.length;j++){
        document.getElementById(divid_arr[j]).innerHTML = '';
		document.getElementById(divid_arr[j]).style.height = '0px';
    }
}

function errorvalidation_red(val)
{
    document.getElementById(val).className = 'error-input';
}

function validation_message(divid,message,top_position,left_position,left_text_position)
{
    if(top_position=='' || top_position==undefined){
        top_position='1';
    }if(left_position=='' || left_position==undefined){
        left_position='2';
    }if(left_text_position=='' || left_text_position==undefined){
        left_text_position='23';
    }
	
    document.getElementById(divid).innerHTML = '<div style="position:absolute; top:'+top_position+'px; left:'+left_position+'px;"><img src="images/error-icon.gif"></div><div style="position:absolute; top:0px;left:'+left_text_position+'px;">'+message+'</div>'
}

function inputvalidation(val)
{
	if(val!="login" && val!="eTitle" && val!="vCountry")
	document.getElementById(val).className = 'login-input';
}

function validation_height(val,height,width)
{
	if(height == '' || height == undefined)
		height = '16';
	if(width == '' || width == undefined)
		width = '180';	
	document.getElementById(val).style.height = height+"px";
	document.getElementById(val).style.width = width+"px";
}

//Valid Email Using Regular Expression
function isValidEmailRex(emailAddress) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[(2([0-4]\d|5[0-5])|1?\d{1,2})(\.(2([0-4]\d|5[0-5])|1?\d{1,2})){3} \])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(emailAddress);
}
function strtolower( str ) {   
    return (str+'').toLowerCase();   
}
// This code was written by Tyler Akins and has been placed in the
// public domain.  It would be nice if you left this header intact.
// Base64 code from Tyler Akins -- http://rumkin.com

var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

function encode64(input) {
   var output = "";
   var chr1, chr2, chr3;
   var enc1, enc2, enc3, enc4;
   var i = 0;

   do {
      chr1 = input.charCodeAt(i++);
      chr2 = input.charCodeAt(i++);
      chr3 = input.charCodeAt(i++);

      enc1 = chr1 >> 2;
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
      enc4 = chr3 & 63;

      if (isNaN(chr2)) {
         enc3 = enc4 = 64;
      } else if (isNaN(chr3)) {
         enc4 = 64;
      }

      output = output + keyStr.charAt(enc1) + keyStr.charAt(enc2) + 
         keyStr.charAt(enc3) + keyStr.charAt(enc4);
   } while (i < input.length);
   
   return output;
}

function decode64(input) {
   var output = "";
   var chr1, chr2, chr3;
   var enc1, enc2, enc3, enc4;
   var i = 0;
alert(input);
   // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
   input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

   do {
      enc1 = keyStr.indexOf(input.charAt(i++));
      enc2 = keyStr.indexOf(input.charAt(i++));
      enc3 = keyStr.indexOf(input.charAt(i++));
      enc4 = keyStr.indexOf(input.charAt(i++));

      chr1 = (enc1 << 2) | (enc2 >> 4);
      chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
      chr3 = ((enc3 & 3) << 6) | enc4;

      output = output + String.fromCharCode(chr1);

      if (enc3 != 64) {
         output = output + String.fromCharCode(chr2);
      }
      if (enc4 != 64) {
         output = output + String.fromCharCode(chr3);
      }
   } while (i < input.length);

   return output;
}
function Redirect(url)
{
	window.location =  tpanel_url+"/"+url;
	
}

/* To get relative Combo */
function getRelativeCombo(id,selectedCat,combId,comboText,genArr,ext){
//alert(genArr);
	var val = id;
	if(ext && ext !='')
		var extval = $(ext).value;
	//alert(extval);

	var control = document.getElementById(""+combId+"");
	///alert(id);
	control.options.length = 0;
	control.options[0] = new Option(comboText);
	control.options[0].value = "";
//	alert(genArr.length);
//	alert(genArr[0][2]+"===>"+val);
	for(i=0,j=0; i< genArr.length; i++)
	{
		if(ext && ext !=''){
			if(genArr[i][2] == val && extval == genArr[i][3])
			{
				if(selectedCat == genArr[i][0])
				{
					control.options[j+1] = new Option(genArr[i][1]);
					control.options[j+1].value = genArr[i][0];
					control.options[j+1].selected = true;
				}else
				{
					control.options[j+1] = new Option(genArr[i][1]);
					control.options[j+1].value = genArr[i][0];
					//alert(control.options[j+1].value);
				}
				j++;
			}
		}else{

			if(genArr[i][2] == val)
			{
				if(selectedCat == genArr[i][0])
				{
					control.options[j+1] = new Option(genArr[i][1]);
					control.options[j+1].value = genArr[i][0];
					control.options[j+1].selected = true;
				}else
				{
					control.options[j+1] = new Option(genArr[i][1]);
					control.options[j+1].value = genArr[i][0];
					//alert(control.options[j+1].value);
				}
				j++;
			}
		}
	}
}

