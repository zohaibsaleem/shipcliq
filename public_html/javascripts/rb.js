  function onrdy(){
    var slider = $('#slider').leanSlider({
        });
    $(".group1").colorbox({rel:'group1',opacity:"0.2"});
  }
  
  function hidemessage(){
      jQuery("#errormsgdiv").slideUp();
  }
  
  function chk_serch_valid(){
    resp = jQuery("#frmsearch").validationEngine('validate');    
	  if(resp == true){
      srch_by_cat(document.getElementById("bykeyword").value, 'bykeyword');
    }else{
      return false;
    }  
  }
  
  function srch_by_cat(val, type){
    if(type == 'category'){
      document.getElementById("iCategoryId").value = val;
    }
    
    if(type == 'suitable'){
      document.getElementById("iSuitableTypeId").value = val;
    }
    
    if(type == 'orderby'){
      document.getElementById("ordby").value = val;
    }
    
    if(type == 'bykeyword'){
      document.getElementById("srchkeyword").value = val;
    }
    
    document.getElementById("eventsfrm").action = site_url+'events';
    document.eventsfrm.submit();
  }
  
  function gmap_location(address){
		// Define addresses, define varible for the markers, define marker counter
		//var addrs = ['The Park Navi Mumbai Hotel, Mumbai, India'];
    var addrs = [address];
    var markers = [];
		var marker_num = 0;
		// Process each address and get it's lat long
		var geocoder = new google.maps.Geocoder();
		var center = new google.maps.LatLngBounds();
		for(k=0;k<addrs.length;k++){
			var addr = addrs[k];
			geocoder.geocode({'address':addr},function(res,stat){
				if(stat==google.maps.GeocoderStatus.OK){
					// add the point to the LatLngBounds to get center point, add point to markers
					center.extend(res[0].geometry.location);
					markers[marker_num]=res[0].geometry.location;
					marker_num++;
					// actually display the map and markers, this is only done the last time
					if(k==addrs.length){
						// actually display the map
						var map = new google.maps.Map(document.getElementById("map_canvas"),{
							'center': center.getCenter(),
							'zoom': 14,
							'streetViewControl': true,
						    'mapTypeId': google.maps.MapTypeId.ROADMAP, // Also try TERRAIN!    
						    'noClear':true
						}); 
						// go through the markers and display them
						for(p=0;p<markers.length;p++){
							var mark = markers[p];
							var marker = new google.maps.Marker({							
								'animation':google.maps.Animation.DROP, // This lags my computer somethin feirce
								'title':addrs[p],
								'map': map,
								'position': mark
								}) 
								
							// Create info window. In content you can pass simple text or html code.
              var infowindow = new google.maps.InfoWindow({
              content: addrs[p]
              });	
              // Add listner for marker. You can add listner for any object. It is just an example in which I am specifying that infowindow will be open on marker mouseover
                google.maps.event.addListener(marker, "click", (function(marker, p) {
                return function() {
                infowindow.setContent('<div class="newadr">'+addrs[p]+'</div>');
                infowindow.open(map, marker);
                }
              })(marker, p));
              
						}
						
						// zoom map so all points can be seen
						//map.fitBounds(center)
					
					}
				}else{
					//console.log('can\'t find address');
				}
			});  
		}
	}
	
	function view_event_related(type, iEventId){
	  $("#ajaxloader").show();
	  if(type == 'Students'){
      var url = site_url+'index.php?file=c-event_students';
    }else{
      var url = site_url+'index.php?file=c-event_reviews';
    }
   
    var request = $.ajax({  
  	  type: "POST",
  	  url: url,  
  	  data: "iEventId="+iEventId, 	  
  	  
  	  success: function(data) {  //alert(data); return false;
  	    $("#ajaxloader").hide();
  		  $("#shw_det").html(data); 
        $("#home").removeClass("active");
        if(type == 'Students'){
          $("#reviews").removeClass("active");
          $("#students").addClass("active");
        }else{
          $("#students").removeClass("active");
          $("#reviews").addClass("active");
        }
               			
  		}
  	});
  	
  	request.fail(function(jqXHR, textStatus) {
  	  alert( "Request failed: " + textStatus ); 
  	});
  }
  
  function show_vol_log(type, by){
    document.getElementById("lgfor").value = type;
    document.getElementById("psfor").value = type;
    if(type == 'Organizations'){
      document.getElementById("fb_loginlbl").style.display = 'none';
    }else{
      document.getElementById("fb_loginlbl").style.display = '';
    }      
           
    $("#"+by).colorbox({
  		transition:"elastic",opacity:"0.6", inline:true, href:"#vol_log",width:"665px", height:"325px"		
    });
  }
  
  function check_login(){
    jQuery("#login_box").validationEngine('attach',{scroll: false});  
    resp = jQuery("#login_box").validationEngine('validate');    
	  if(resp == true){
      $("#loginbtn").hide(); 
      $("#loginloader").show();    
      var request = $.ajax({  
    	  type: "POST",
    	  url: site_url+'index.php?file=c-login',  
    	  data: $("#login_box").serialize(), 	  
    	  
    	  success: function(data) {  //alert(data); return false; 
          $("#loginloader").hide();
          $("#loginbtn").show();                	     	
          if(data == 0){
            jQuery('#vEmail').validationEngine('showPrompt', 'Your login failed. This happens due to -Incorrect Username or Password.', 'error', true);
          }
          else if(data == 1){
            jQuery('#vEmail').validationEngine('showPrompt', 'Your account is not active. Please contact administrator.', 'error', true);
          }
          else{
            if(document.getElementById("from").value == 'subscription'){
               $.fn.colorbox.close();
               subscribe_event();
            }else{               
              if(document.getElementById("lgfor").value == 'Organizations'){
                window.location = site_url+'organization-account';
              }else{
                window.location = site_url+'volunteer-profile';
              } 
            }                            
          }  			
    		}
  	 });
  	
     request.fail(function(jqXHR, textStatus) {
      alert( "Request failed: " + textStatus ); 
     });
      
    }else{
      return false;
    }  
  }
  
  function show_frgpass(){
    $("#loginboxpop").hide();
    $("#passwordboxpop").show();
  }
  
  function show_login(){
    $("#passwordboxpop").hide();
    $("#loginboxpop").show();
  }
  
  function check_password(){
    jQuery("#password_box").validationEngine('attach',{scroll: false});  
    resp = jQuery("#password_box").validationEngine('validate');    
	  if(resp == true){ 
      $("#passwordbtn").hide(); 
      $("#passwordloader").show();  
	    var request = $.ajax({  
    	  type: "POST",
    	  url: site_url+'index.php?file=c-fpassword',  
    	  data: $("#password_box").serialize(), 	  
    	  
    	  success: function(data) {  //alert(data); return false;      	     	
          $("#passwordloader").hide();
          $("#passwordbtn").show();  
          if(data == 0){
            jQuery('#vFEmail').validationEngine('showPrompt', 'Sorry ! The Email address you have entered is not found.', 'error', true);
          }
          else if(data == 1){
            jQuery('#vFEmail').validationEngine('showPrompt', 'Error in sending password.', 'error', true);
          }
          else{
            show_login();
            $.fn.colorbox.close();
            $("#password_box")[0].reset();
            $.msgGrowl ({   
						type: 'success'
						, title: 'Forgot Password Request'
						, text: 'Your Password has been sent Successfully.'
						, position: 'top-center'
						, sticky: true
					});        
          }  			
    		}
  	 });
  	
     request.fail(function(jqXHR, textStatus) {
      alert( "Request failed: " + textStatus ); 
     });
    }else{
      return false;
    }
  }
  
  function check_orgreg(){
    resp = jQuery("#frmregister").validationEngine('validate');        
	  if(resp == true){
      document.frmregister.submit();
    }else{
      return false;
    }  
  }
  
  function get_county_list(code,selected)
  {   
    $("#county_list").html("Please wait..."); 
    var request = $.ajax({  
      type: "POST",
      url: site_url+'index.php?file=o-county_list',  
      data: "code="+code+"&selected="+selected, 	  
      
      success: function(data) {
        $("#county_list").html(data);       			
      }
    });
    
    request.fail(function(jqXHR, textStatus) {
      alert( "Request failed: " + textStatus ); 
    });   		
  } 
  
  function upload_reg_logo(id){ 
    $('#vImage').uploadify({              
      'height'      : '40',
      'width'       : '120',        
      'swf'         : site_url+'uploadify/uploadify.swf',
      'uploader'    : site_url+'upload_images.php',
      'folder'      : site_url+'public_html/uploads/tmp_photos/',
    	'formData'    : {'id':id},
    	'fileTypeExts'     : '*.jpg;*.gif;*.png;*.jpeg',
    	'fileTypeDesc'    : 'Image Files',
    	'fileSizeLimit'   : '2MB',
    	'uploadLimit' : 5,
    	'queueSizeLimit' : 1,
      'auto'       : true,
    	'multi'      : false,
    	'fileObjName': 'Filedata',
      'onUploadSuccess' : function(file, data, response) {  //alert(data); return false; 
    		if(response == true){      			     			
    			var img_url = data.split("|");
    			if($.trim(img_url[0]) == 'yes'){         			
    			  document.getElementById("uplogo").value = img_url[1];
    			  $('#logoimg').css({ "height": img_url[3], "width": img_url[2]});
            $("#logoimg").attr("src", img_url[4]);
            $("#logoimgdiv").show();                           
            return false;           
          }else{ alert('above');
            alert("Failed to upload your image. Please try again.");
            return false;
          }                  		
    		}else {
    			alert("Failed to upload your image. Please try again.");
    		}
        },
    	'onUploadStart' : function(file) {
    		if(file.size < 200){ //2097152
    			alert("Your image size is less than 2MB. Please upload image with size greater than 2MB.")
    			$('#vImage').uploadify('cancel');
    		}
    		//$(this).uploadify('stop');  
    	}
      });  
  }
  
  function shownotificationMessage(msg, type){
      showNotification({
          type : type,
          message: msg
      });    
  }
  
  function show_regimage(img, url, width, height){  
      $('#logoimg').css({ "height": height, "width": width});
      $("#logoimg").attr("src", url);          
      $("#logoimgdiv").show();  
  }
  
  function redirectcancel(url)
  {  
    window.location=url;
    return false;
  }
  
  function get_county_list_profile(code,selected)
  {   
    $("#county_list").html("Please wait..."); 
    var request = $.ajax({  
      type: "POST",
      url: site_url+'index.php?file=o-county_list_profile',  
      data: "code="+code+"&selected="+selected, 	  
      
      success: function(data) {
        $("#county_list").html(data);       			
      }
    });
    
    request.fail(function(jqXHR, textStatus) {
      alert( "Request failed: " + textStatus ); 
    });   		
  }  
  
  function upload_profile_logo(id){ 
    $('#vImage').uploadify({              
      'height'      : '40',
      'width'       : '120',        
      'swf'         : site_url+'uploadify/uploadify.swf',
      'uploader'    : site_url+'upload_images_profile.php',
      'folder'      : site_url+'public_html/uploads/tmp_photos/',
    	'formData'    : {'id':id},
    	'fileTypeExts'     : '*.jpg;*.gif;*.png;*.jpeg',
    	'fileTypeDesc'    : 'Image Files',
    	'fileSizeLimit'   : '2MB',
    	'uploadLimit' : 5,
    	'queueSizeLimit' : 1,
      'auto'       : true,
    	'multi'      : false,
    	'fileObjName': 'Filedata',
      'onUploadSuccess' : function(file, data, response) {  //alert(data); return false; 
    		if(response == true){      			     			
    			var img_url = data.split("|");
    			if($.trim(img_url[0]) == 'yes'){  
            $("#logoimg").attr("src", img_url[1]);
            $("#logoimgdiv").show();
            $("#logoimgdeletediv").show();
            shownotificationMessage('Profile Image uploaded successfully.', 'success');                           
            return false;           
          }else{
            alert("Failed to upload your image. Please try again.");
            return false;
          }                  		
    		}else {
    			alert("Failed to upload your image. Please try again.");
    		}
        },
    	'onUploadStart' : function(file) {
    		if(file.size < 200){ //2097152
    			alert("Your image size is less than 2MB. Please upload image with size greater than 2MB.")
    			$('#vImage').uploadify('cancel');
    		}
    		//$(this).uploadify('stop');  
    	}
      });  
  }
  
  function delete_profile_image(id){
    var r=confirm("Are you sure you want to delete profile Image(Logo)?");
    if (r==true)
    {
      var request = $.ajax({  
    	  type: "POST",
    	  url: site_url+'index.php?file=o-delete_profile_image',   
    	  data: "iOrganizationId="+id, 	  
    	  
    	  success: function(data) {  //alert(data); return false;
    	    if(data == 1){
    	      $("#logoimg").attr("src", '');
            $("#logoimgdiv").hide();
            $("#logoimgdeletediv").hide();
            shownotificationMessage('Profile Image deleted successfully.', 'success');
          }
    		}
    	});
    	
    	request.fail(function(jqXHR, textStatus) {
    	  alert( "Request failed: " + textStatus ); 
    	});
    }
    else
    {
      return false;
    }
  } 
  
  function check_edit_profile(){
    resp = jQuery("#frmeditprofile").validationEngine('validate');        
	  if(resp == true){
      document.frmeditprofile.submit();
    }else{
      return false;
    }     
  } 
  
  function checkvalidpass(){        
    resp = jQuery("#frmchangepass").validationEngine('validate');
		if(resp == true){
			document.frmchangepass.submit();
		}else{
			return false;
		}		
	}	
	
	function cancelpassword()
  {
    window.location=+site_url+"organization-account";
    return false;
  }
  
  function accept_sub_req(iEventId, iMemberId, iEventVolunteerId){
    var r=confirm("Are you sure want to accept this subscription request?");
    if(r==true){     
      $("#acc_"+iMemberId).hide();  
      $("#wt_"+iMemberId).show();    
      var request = $.ajax({  
    	  type: "POST",
    	  url: site_url+'index.php?file=o-subscription_requests_a',   
    	  data: "iEventId="+iEventId+"&iMemberId="+iMemberId+"&iEventVolunteerId="+iEventVolunteerId+"&action=accept", 	  
    	  
    	  success: function(data) {  //alert(data); return false;
    	    $("#acc_"+iMemberId).show();  
          $("#wt_"+iMemberId).hide(); 
          if(data == 1){
    	      window.location=site_url+"subscription-requests/"+iEventId+"&var_msg=Subscription request accepted successfully.";
            return false;  
          }else{
            shownotificationMessage('Error in accept request. Please try again.', 'error');
          }
    		}
    	});
    	
    	request.fail(function(jqXHR, textStatus) {
    	  alert( "Request failed: " + textStatus ); 
    	});
    }else{
      return false;
    } 
  }
  
  function decline_sub_req(iEventId, iMemberId, iEventVolunteerId){
    var r=confirm("Are you sure want to decline this subscription request?");
    if(r==true){     
      $("#decl_"+iMemberId).hide();  
      $("#wtdec_"+iMemberId).show();    
      var request = $.ajax({  
    	  type: "POST",
    	  url: site_url+'index.php?file=o-subscription_requests_a',   
    	  data: "iEventId="+iEventId+"&iMemberId="+iMemberId+"&iEventVolunteerId="+iEventVolunteerId+"&action=decline", 	  
    	  
    	  success: function(data) {  //alert(data); return false;
    	    $("#decl_"+iMemberId).show();  
          $("#wtdec_"+iMemberId).hide(); 
          if(data == 1){
    	      window.location=site_url+"subscription-requests/"+iEventId+"&var_msg=Subscription request declined successfully.";
            return false;  
          }else{
            shownotificationMessage('Error in decline request. Please try again.', 'error');
          }
    		}
    	});
    	
    	request.fail(function(jqXHR, textStatus) {
    	  alert( "Request failed: " + textStatus ); 
    	});
    }else{
      return false;
    }
  } 
  
  function subscribe_event(){
    var request = $.ajax({  
    	  type: "POST",
    	  url: site_url+'index.php?file=m-subscribe_event',   
    	  data: "iEventId="+document.getElementById("iEventId").value, 	  
    	  
    	  success: function(data) { //alert(data); 
          data = Trim_mw(data);
          if(data == 'login'){ 
    	      document.getElementById("lgfor").value = 'Volunteers';
            document.getElementById("psfor").value = 'Volunteers'; 
            document.getElementById("from").value = 'subscription';                         
            $.colorbox({
          		transition:"elastic",opacity:"0.6", inline:true, href:"#vol_log",width:"665px", height:"325px"		
            });  
          }else if(data == 'succ'){
            shownotificationMessage("Your subscription request for this event has been submitted successfully. You will be notified once your request has been approved.", 'success');
          }else if(data == 'org'){
            shownotificationMessage('You are registerd as organization. You can can not subscribe for this event.', 'error');
          }else if(data == 'exist'){
            shownotificationMessage('Your subscription request for this event has been already sent.', 'error');
          }else{
            shownotificationMessage('Error in subscription request. Please try again.', 'error');
          }
    		}
    	});
    	
    	request.fail(function(jqXHR, textStatus) {
    	  alert( "Request failed: " + textStatus ); 
    	});
  }
  
  function Trim_mw(s)
  {
    // Remove leading spaces and carriage returns
    
    while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r'))
    {
      s = s.substring(1,s.length);
    }
  
    // Remove trailing spaces and carriage returns
  
    while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length) == '\n') || (s.substring(s.length-1,s.length) == '\r'))
    {
      s = s.substring(0,s.length-1);
    }
    return s;
  }
  
  function event_actions(action, id){        
    document.getElementById("action").value = action;
    document.getElementById("iEventId").value = id;
    document.getElementById("frmevent").action = site_url+'index.php?file=o-subscription_requests_a';
    document.frmevent.submit();
    return false;
  }
  
  function add_event(){
    window.location=site_url+"add-event";
    return false; 
  }
  
  function show_hide_ebent_time(val){
    if(val == 'Fixed'){
      $("#event_date").show(); 
      $("#event_time").show(); 
      $('#dStartDate').Zebra_DatePicker({direction: true, pair: $('#dEndDate')});  
      $('#dEndDate').Zebra_DatePicker({direction: true});
    }else{
      $("#event_date").hide(); 
      $("#event_time").hide(); 
    }  
  }
  
  function check_event(){
    resp = jQuery("#frmevent").validationEngine('validate');
		if(resp == true){
      document.frmevent.submit();
      return false;
    }else{
      return false;
    }       
  }
  
    function upload_event_logo(id){ 
    $('#vImage').uploadify({              
      'height'      : '40',
      'width'       : '120',        
      'swf'         : site_url+'uploadify/uploadify.swf',
      'uploader'    : site_url+'upload_images.php',
      'folder'      : site_url+'public_html/uploads/tmp_photos/',
    	'formData'    : {'id':id},
    	'fileTypeExts'     : '*.jpg;*.gif;*.png;*.jpeg',
    	'fileTypeDesc'    : 'Image Files',
    	'fileSizeLimit'   : '2MB',
    	'uploadLimit' : 5,
    	'queueSizeLimit' : 1,
      'auto'       : true,
    	'multi'      : false,
    	'fileObjName': 'Filedata',
      'onUploadSuccess' : function(file, data, response) {  //alert(data); return false; 
    		if(response == true){      			     			
    			var img_url = data.split("|");
    			if($.trim(img_url[0]) == 'yes'){         			
    			  document.getElementById("uplogo").value = img_url[1];
    			  $('#logoimg').css({ "height": img_url[3], "width": img_url[2]});
            $("#logoimg").attr("src", img_url[4]);
            $("#logoimgdiv").show();                           
            return false;           
          }else{
            alert("Failed to upload your image. Please try again.");
            return false;
          }                  		
    		}else {
    			alert("Failed to upload your image. Please try again.");
    		}
        },
    	'onUploadStart' : function(file) {
    		if(file.size < 200){ //2097152
    			alert("Your image size is less than 2MB. Please upload image with size greater than 2MB.")
    			$('#vImage').uploadify('cancel');
    		}
    		//$(this).uploadify('stop');  
    	}
      });  
  }
  
  function upload_event_edit_logo(id){
    $('#vImage').uploadify({              
      'height'      : '40',
      'width'       : '120',        
      'swf'         : site_url+'uploadify/uploadify.swf',
      'uploader'    : site_url+'upload_images_event_edit.php',
      'folder'      : site_url+'public_html/uploads/tmp_photos/',
    	'formData'    : {'id':id},
    	'fileTypeExts'     : '*.jpg;*.gif;*.png;*.jpeg',
    	'fileTypeDesc'    : 'Image Files',
    	'fileSizeLimit'   : '2MB',
    	'uploadLimit' : 5,
    	'queueSizeLimit' : 1,
      'auto'       : true,
    	'multi'      : false,
    	'fileObjName': 'Filedata',
      'onUploadSuccess' : function(file, data, response) {  //alert(data); return false; 
    		if(response == true){      			     			
    			var img_url = data.split("|");
    			if($.trim(img_url[0]) == 'yes'){  
            $("#logoimg").attr("src", img_url[1]);
            $("#logoimgdiv").show();
            $("#logoimgdeletediv").show(); 
            shownotificationMessage('Event Image updated successfully.', 'success');                          
            return false;           
          }else{
            alert("Failed to upload your image. Please try again.");
            return false;
          }                  		
    		}else {
    			alert("Failed to upload your image. Please try again.");
    		}
        },
    	'onUploadStart' : function(file) {
    		if(file.size < 200){ //2097152
    			alert("Your image size is less than 2MB. Please upload image with size greater than 2MB.")
    			$('#vImage').uploadify('cancel');
    		}
    		//$(this).uploadify('stop');  
    	}
      });  
  }
  
  function delete_event_image(id){
    var r=confirm("Are you sure you want to delete event Image?");
    if (r==true)
    {
      var request = $.ajax({  
    	  type: "POST",
    	  url: site_url+'index.php?file=o-delete_event_image',   
    	  data: "iEventId="+id, 	  
    	  
    	  success: function(data) {  //alert(data); return false;
    	    if(data == 1){
    	      $("#logoimg").attr("src", '');
            $("#logoimgdiv").hide();
            $("#logoimgdeletediv").hide();
            shownotificationMessage('Event Image deleted successfully.', 'success');
          }
    		}
    	});
    	
    	request.fail(function(jqXHR, textStatus) {
    	  alert( "Request failed: " + textStatus ); 
    	});
    }
    else
    {
      return false;
    }
  } 
  
  function sort_organizations(val){
    if(val != ''){
      document.getElementById("ordby").value = val;
      document.orgfrm.submit();  
      return false; 
    }else{
      return false;
    }       
  }
  
  function search_by_key(){
   document.getElementById("srchkeyword1").value = document.getElementById("vCity").value;
   document.getElementById("srchkeyword2").value = document.getElementById("keyword").value;
   document.orgfrm.submit();
   return false;
  }
  
  function checkattending(id){
    resp = jQuery("#frmattending").validationEngine('validate');   
		if(resp == true){        
      $("#subbtnfrm").hide();
      $("#waitbtnfrm").show();  
      var request = $.ajax({  
    	  type: "POST",
    	  url: site_url+'index.php?file=o-subscription_requests_a',   
    	  data: $("#frmattending").serialize(), 	  
    	  
    	  success: function(data) {  //alert(data); return false;
    	    $("#waitbtnfrm").hide();
          $("#subbtnfrm").show();
    	    if(data == 1){
            $.fn.colorbox.close();   
            $("#attbtn_"+id).html('Update Hours');  	      
            shownotificationMessage('Volunteer attended hours served successfully.', 'success');  return false;
            
          }else{
            $.fn.colorbox.close();
            shownotificationMessage('Eror in volunteer attended hours served. Try again', 'error'); return false;
          }
    		}
    	});       	
    	request.fail(function(jqXHR, textStatus) {
    	  alert( "Request failed: " + textStatus ); 
    	});
      
      return false;
    }else{
      return false;
    }
  }
  
  function show_vol_reg(){
    $("#regvol").colorbox({
  		transition:"elastic",opacity:"0.6", inline:true, href:"#vol_register",width:"780px", height:"530px"		
    });
  }
  
  function check_vol_reg(){
    resp = jQuery("#frmvolreg").validationEngine('validate');   
		if(resp == true){        
      $("#volregbut").hide();
      $("#volregloader").show();
      var request = $.ajax({  
        type: "POST",
      	  url: site_url+'index.php?file=c-volunteer_registration',   
      	  data: $("#frmvolreg").serialize(), 	  
      	  
      	  success: function(data) {  //alert(data); return false;
      	   $("#volregbut").show();
           $("#volregloader").hide();
      	   if(data == 0){
            shownotificationMessage('Please enter values into required fields.', 'error');  return false;
           }else if(data == 1){
             shownotificationMessage('Email address you have entered is already exists. Please try with another E-mail.', 'error'); return false;              
           }else if(data == 2){
              window.location=site_url+"index.php?file=c-volunteer_registration_confirm";
              return false;
           }else{
             shownotificationMessage('Something goes wrong. Please try again.', 'error'); return false;    
           }
            
      		}
    	});       	
    	request.fail(function(jqXHR, textStatus) {
    	  alert( "Request failed: " + textStatus ); 
    	});
    }else{
      return false;
    }
  }
  
  function create_account()
  { 
    if(document.getElementById("lgfor").value == 'Volunteers'){
      $("#creatacc").colorbox({
  		  transition:"elastic",opacity:"0.6", inline:true, href:"#vol_register",width:"780px", height:"530px"		
      });  
    }else{
      window.location=site_url+"organization-registration";
      return false;
    }
  }
  
  function findPos(obj) {
  	var curtop = 0;
  	if (obj.offsetParent) {
  		do {
  			curtop += obj.offsetTop;
  		} while (obj = obj.offsetParent);
  	return [curtop];
  	}
  }
  
  function scrtodiv(val){    
    var SupportDiv = document.getElementById(val);
    Ypos = findPos(SupportDiv);   
    Ypos = parseInt(Ypos) - 260;     
    window.scroll(0,Ypos);       
  }
  
  function give_reviews(id){  
      var request = $.ajax({  
        type: "POST",
      	  url: site_url+'index.php?file=c-volunteer_reviews',   
      	  data: 'action=checking&id='+id, 	  
      	  
      	  success: function(data) {  //alert(data); return false;
      	   $("#volregbut").show();
           $("#volregloader").hide();
      	   if(data == 0){
            shownotificationMessage('Please login to give your ratings and reviews.', 'error');  return false;
           }else if(data == 1){
             document.getElementById("iOrganizationId").value = id;
             $.colorbox({
            	 transition:"elastic",opacity:"0.6", inline:true, href:"#rate_reviews",width:"547px", height:"325px"		
            });    
            return false;          
           }
      		}
    	});       	
    	request.fail(function(jqXHR, textStatus) {
    	  alert( "Request failed: " + textStatus ); 
    	});    
  }
  
    function check_reviews(id){
      resp = jQuery("#frmattending").validationEngine('validate');   
  		if(resp == true){        
        $("#subbtnfrm").hide();
        $("#waitbtnfrm").show();  
        var request = $.ajax({  
      	  type: "POST",
      	  url: site_url+'index.php?file=c-volunteer_reviews',   
      	  data: $("#frmattending").serialize(), 	  
      	  
      	  success: function(data) {  //alert(data); return false;
      	    $("#waitbtnfrm").hide();
            $("#subbtnfrm").show();
      	    if(data == 1){
              $.fn.colorbox.close();                  	      
              shownotificationMessage('Your review sent successfully successfully.', 'success');  return false;
              
            }else{
              $.fn.colorbox.close();
              shownotificationMessage('Eror in giving reviews. Please try again', 'error'); return false;
            }
      		}
      	});       	
      	request.fail(function(jqXHR, textStatus) {
      	  alert( "Request failed: " + textStatus ); 
      	});
        
        return false;
      }else{
        return false;
      }
  }
  
  function fb_log(){
    window.location=site_url+"fbconnect.php";
    return false;
  }
  
  function show_msg_skills(text){
          $.msgGrowl ({   
						type: 'info'
						, title: 'Notification'
						, text: text
						, position: 'top-center'
						, sticky: true
					});
  }
  
  function show_msg_int(text){
          $.msgGrowl ({   
						type: 'info'
						, title: 'Notification'
						, text: text
						, position: 'top-center'
						, sticky: true
					});
  }
  
  function close_popup(){
    $('#frmvolreg').validationEngine('hide');
    $.fn.colorbox.close();
    
  }
  
  function supports_placeholder() {
  	test = document.createElement('input');
  	return ('placeholder' in test);
  }
  
  function add_event_category(){
    resp = jQuery("#frmattending").validationEngine('validate');   
		if(resp == true){        
      $("#subbtnfrm").hide();
      $("#waitbtnfrm").show();  
      var request = $.ajax({  
    	  type: "POST",
    	  url: site_url+'index.php?file=o-add_new_category',   
    	  data: $("#frmattending").serialize(), 	  
    	  
    	  success: function(data) {  //alert(data); return false;
    	    var n=data.split("|");
    	    $("#waitbtnfrm").hide();
          $("#subbtnfrm").show();
    	    if(n[0] == 1){
            $.fn.colorbox.close();   
            //$("#catdivadd").appendChild('<span><input type="checkbox" value="'+n[1]+'" name="iCategoryId[]" id="iCategoryId" checked >&nbsp;'+n[2]+'</span>');
            
            $('<span><input type="checkbox" value="'+n[1]+'" name="iCategoryId[]" id="iCategoryId" checked >&nbsp;'+n[2]+'</span>').appendTo('#catdivadd'); 
            //var box = '<span><input type="checkbox" value="'+n[1]+'" name="iCategoryId[]" id="iCategoryId" checked >&nbsp;'+n[2]+'</span>';

            //document.getElementById("catdivadd").appendChild(box);

            /*var checkbox = document.createElement('input');
            checkbox.type = "checkbox";
            checkbox.name = 'iCategoryId[]';
            checkbox.value = n[1];
            checkbox.id = "iCategoryId"; 
            
            var em = document.createElement('em')
            em.htmlFor = n[1];
            em.appendChild(document.createTextNode('text for label after checkbox'));
               
            document.getElementById("catdivadd").appendChild(checkbox); 
            document.getElementById("catdivadd").appendChild(em); 	*/                      
            shownotificationMessage('New event category addedd successfully into Category list.', 'success');  return false;
            
          }else{
            $.fn.colorbox.close();
            shownotificationMessage('Eror in add new category. Try again', 'error'); return false;
          }
    		}
    	});       	
    	request.fail(function(jqXHR, textStatus) {
    	  alert( "Request failed: " + textStatus ); 
    	});
      
      return false;
    }else{
      return false;
    }
  }
  