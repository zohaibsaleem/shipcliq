  function get_county_list_volunteer(code,selected)
  {   
    $("#county_list").html("Please wait..."); 
    var request = $.ajax({  
      type: "POST",
      url: site_url+'index.php?file=m-county_list_volunteer',  
      data: "code="+code+"&selected="+selected, 	  
      
      success: function(data) {
        $("#county_list").html(data);       			
      }
    });
    
    request.fail(function(jqXHR, textStatus) {
      alert( "Request failed: " + textStatus ); 
    });   		
  } 
  
 function upload_volunteer_image(id){ 
   //alert(id);
    $('#vImage').uploadify({              
      'height'      : '40',
      'width'       : '120',        
      'swf'         : site_url+'uploadify/uploadify.swf',
      'uploader'    : site_url+'upload_images_volunteer.php',
      'folder'      : site_url+'public_html/uploads/tmp_photos/',
    	'formData'    : {'id':id},
    	'fileTypeExts'     : '*.jpg;*.gif;*.png;*.jpeg',
    	'fileTypeDesc'    : 'Image Files',
    	'fileSizeLimit'   : '2MB',
    	'uploadLimit' : 5,
    	'queueSizeLimit' : 1,
      'auto'       : true,
    	'multi'      : false,
    	'fileObjName': 'Filedata',
      'onUploadSuccess' : function(file, data, response) {  //alert(data); return false; 
    		if(response == true){      			     			
    			var img_url = data.split("|");
    			if($.trim(img_url[0]) == 'yes'){  
            $("#logoimg").attr("src", img_url[1]);
            $("#logoimgdiv").show();
            $("#logoimgdeletediv").show();
            shownotificationMessage('Profile Image uploaded successfully.', 'success');                           
            return false;           
          }else{
            alert("Failed to upload your image. Please try again.");
            return false;
          }                  		
    		}else {
    			alert("Failed to upload your image. Please try again.");
    		}
        },
    	'onUploadStart' : function(file) {
    		if(file.size < 200){ //2097152
    			alert("Your image size is less than 2MB. Please upload image with size greater than 2MB.")
    			$('#vImage').uploadify('cancel');
    		}
    		//$(this).uploadify('stop');  
    	}
      });  
  }
  
  function delete_volunteer_image(id){
  //alert(id); return false;
     var r=confirm("Are you sure you want to delete profile Image?");
    if (r==true)
    {
      var request = $.ajax({  
    	  type: "POST",
    	  url: site_url+'index.php?file=m-delete_profile_image',   
        data: "iMemberId="+id, 	  
    	  
    	  success: function(data) {  //alert(data); return false;
    	    if(data == 1){
    	      $("#logoimg").attr("src", '');
            $("#logoimgdiv").hide();
            $("#logoimgdeletediv").hide();
            shownotificationMessage('Profile Image deleted successfully.', 'success');
          }
    		}
    	});
    	
    	request.fail(function(jqXHR, textStatus) {
    	  alert( "Request failed: " + textStatus ); 
    	});
    }
    else
    {
      return false;
    }
  } 
  
  function check_edit_profile_volunteer(){
    resp = jQuery("#frmeditprofile").validationEngine('validate');        
	  if(resp == true){
      document.frmeditprofile.submit();
    }else{
      return false;
    }     
  } 
  
  function tab_volunteer_profile(type, id)
{
  document.getElementById("main_loader").style.display = '';     
  var request = $.ajax({  
    type: "POST",
    url: site_url+'index.php?file=m-points_earned',  
   
    data: "iMemberId="+id, 	  
   
    success: function(data) { //alert(data); return false;          
      document.getElementById("main_loader").style.display = 'none';
      if(type == 'Points'){
        document.getElementById("points_earned").className = 'active';
        document.getElementById("attended").className = '';
          
      }
      else{
        document.getElementById("points_earned").className = '';
        document.getElementById("attended").className = 'active';
         }  
      $("#main_tab").html(data);              			
    }  
      
  });
  
  request.fail(function(jqXHR, textStatus) {
    alert( "Request failed: " + textStatus ); 
  });    
}

  
  
  
 
