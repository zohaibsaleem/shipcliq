<?php
  session_start();
  define( '_TEXEC', 1 );
  define('TPATH_BASE', dirname(__FILE__) );
  define( 'DS', DIRECTORY_SEPARATOR );
  
  require_once ( TPATH_BASE .DS.'includes'.DS.'defines.php' ); 
  require_once ( TPATH_BASE .DS.'includes'.DS.'configuration.php' );
  $sql = "SELECT rides_new.iRideId, rides_new.iMemberId, rides_new.vMainDeparture, rides_new.vMainArrival, ride_dates.dDateOut, ride_dates.dDateRet  
          FROM rides_new 
          LEFT JOIN ride_dates ON ride_dates.iRideId = rides_new.iRideId 
          WHERE rides_new.eRidePlaceType = 'Airport' 
          AND (ride_dates.dDateOut >= '2014-01-03'  OR ride_dates.dDateRet >= '2014-01-03')
          ORDER BY RAND() 
          LIMIT 0,3";
  $db_ride_details = $obj->MySQLSelect($sql);
  
  $mainarr = array();
  for($i=0;$i<count($db_ride_details);$i++){
    $newarr = array();
    if($db_ride_details[$i]['dDateOut'] != '0000-00-00'){
      $newarr['iRideId'] = $db_ride_details[$i]['iRideId'];
      $newarr['vMainDeparture'] = $db_ride_details[$i]['vMainDeparture'];
      $newarr['vMainArrival'] = $db_ride_details[$i]['vMainArrival'];
      $newarr['dDateOut'] = $db_ride_details[$i]['dDateOut'];
      $newarr['strtotime'] = strtotime($db_ride_details[$i]['dDateOut']);
      array_push($mainarr,$newarr);  
    }
    if($db_ride_details[$i]['dDateRet'] != '0000-00-00'){
      $newarr['iRideId'] = $db_ride_details[$i]['iRideId'];
      $newarr['vMainDeparture'] = $db_ride_details[$i]['vMainDeparture'];
      $newarr['vMainArrival'] = $db_ride_details[$i]['vMainArrival'];
      $newarr['dDateOut'] = $db_ride_details[$i]['dDateRet'];
      $newarr['strtotime'] = strtotime($db_ride_details[$i]['dDateRet']);
      array_push($mainarr,$newarr);  
    }
  }
  
  function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
      $sort_col = array();
      foreach ($arr as $key=> $row) {
          $sort_col[$key] = $row[$col];
      }    
      array_multisort($sort_col, $dir, $arr); 
  }
  array_sort_by_column($mainarr, 'strtotime');
  
  
  echo "<pre>";
  print_r($mainarr); exit;
          
?>
