<?
 date_default_timezone_set('Europe/Riga');

 ##
 ## returning client. the payment process is finished by the client and we get him back here
 ##
 ob_start();      
session_start();
header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
define( '_TEXEC', 1 );
define('TPATH_BASE', dirname(__FILE__) );
define( 'DS', DIRECTORY_SEPARATOR );
//header('Content-Type: text/html; charset=iso-8859-1');  

require_once ( TPATH_BASE .DS.'includes'.DS.'defines.php' ); 
require_once ( TPATH_BASE .DS.'includes'.DS.'configuration.php' ); 
ini_set("track_errors", true); 



 /**
 * merchant constants
 */

 $merchant_id		= 260;			// merchant ID provided by AirPay
 $merchant_secret	= "secretkey";			// merchant secret code provided by merchant


 # include AirPay class
 include("/AirPay_v1.2/airpay.class.php");

 # create AirPay object
 $airpay = new airpay( $merchant_id, $merchant_secret );

 #echo "<pre>";print_r($_REQUEST);exit;
 # process the response to make sure we can trust the data
 if (!$ret = $airpay->response($_GET, 'return')) 
	print "error";
 else {
	## it is not recommended to update local database on this step
	## all the information here is just for the redirection of the client
	## status:
	## 1 = SUCCESS
	## 2 = CANCELLED
  #echo "<pre>";print_r($ret);exit;
	if ($ret['status'] == 1) {
		// where SUCCESS does not means that the payment is successfull
		// You just have to tell the client that the payment is processing and there are still no errors
    #echo "<pre>";print_r($ret);exit;
    #echo $temp_bookingid = $_SESSION['temp_booking_id'];echo "<hr />";
    ## the redirection to Your local page is supposed to be here
		// header ("Location: http://www.merchant-website.com/thank_you.html");
		header("Location:http://www.baltic-car.com/index.php?file=m-bookingprocess");
    exit;
	}

	if ($ret['status'] == 2) {
		// status CANCELLED means that the client clicked "cancel" button somewhere
    $sql = "DELETE FROM temp_booking WHERE iBookingId = '".$_SESSION['temp_booking_id']."'";
    $db_sql=$obj->sql_query($sql);
		## the redirection to Your local page is supposed to be here
		// header ("Location: http://www.merchant-website.com/cancelled.html");
		header("Location:http://www.baltic-car.com/index.php?file=m-payment_failed");
    exit;
	}
 }

?>