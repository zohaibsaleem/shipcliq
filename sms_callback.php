<?
  ob_start();      
  session_start();
  define( '_TEXEC', 1 );
  define('TPATH_BASE', dirname(__FILE__) );
  define( 'DS', DIRECTORY_SEPARATOR );
  
  require_once ( TPATH_BASE .DS.'includes'.DS.'defines.php' ); 
  require_once ( TPATH_BASE .DS.'includes'.DS.'configuration.php' );
  
  
  $vRemark = "";
  foreach($_REQUEST as $key => $value){
  	$vRemark .='<b>'.$key.' :</b> '.stripslashes($value).'<br>';
  }
  
  $SmsStatus = $_REQUEST['SmsStatus'];
  $Body = $_REQUEST['Body'];
  if($SmsStatus == 'received'){
    $Body = trim($Body);     
    if($Body != ''){
      $sql = "SELECT * FROM booking_new WHERE vSmsCode = '".$Body."' AND eBookerConfirmation = 'No'";
      $db_booking = $obj->MySQLSelect($sql);
            
      if(count($db_booking) > 0){
        $sql = "UPDATE booking_new SET eBookerConfirmation = 'Yes' WHERE iBookingId = '".$db_booking[0]['iBookingId']."'";
        $db_sql=$obj->sql_query($sql); 
                
        if($db_sql){
          $Data['vBookingNo'] = $db_booking[0]['vBookingNo'];
          $Data['iRideId'] = $db_booking[0]['iRideId'];
          $Data['iRidePointId'] = $db_booking[0]['iRidePointId'];
          $Data['vFromPlace'] = $db_booking[0]['vFromPlace'];
          $Data['vToPlace'] = $db_booking[0]['vToPlace'];
          $Data['vMainRidePlaceDetails'] = $db_booking[0]['vMainRidePlaceDetails'];
          $Data['iNoOfSeats'] = $db_booking[0]['iNoOfSeats'];
          $Data['fAmount'] = $db_booking[0]['fAmount'];
          $Data['dBookingDate'] = $db_booking[0]['dBookingDate'];
          $Data['dBookingTime'] = $db_booking[0]['dBookingTime'];
          $Data['iBookerId'] = $db_booking[0]['iBookerId'];
          $Data['iDriverId'] = $db_booking[0]['iDriverId'];
          $Data['vBookerFirstName'] = $db_booking[0]['vBookerFirstName'];
          $Data['vBookerLastName'] = $db_booking[0]['vBookerLastName'];
          $Data['vBookerAddress'] = $db_booking[0]['vBookerAddress'];
          $Data['vBookerCity'] = $db_booking[0]['vBookerCity'];
          $Data['vBookerState'] = $db_booking[0]['vBookerState'];
          $Data['vBookerCountry'] = $db_booking[0]['vBookerCountry'];
          $Data['vBookerZip'] = $db_booking[0]['vBookerZip'];
          $Data['vBookerPhone'] = $db_booking[0]['vBookerPhone'];
          $Data['vBookerEmail'] = $db_booking[0]['vBookerEmail'];
          $Data['vDriverFirstName'] = $db_booking[0]['vDriverFirstName'];
          $Data['vDriverLastName'] = $db_booking[0]['vDriverLastName'];
          $Data['vDriverPhone'] = $db_booking[0]['vDriverPhone'];
          $Data['vDriverEmail'] = $db_booking[0]['vDriverEmail'];
          $Data['vBookerCurrencyCode'] = $db_booking[0]['vBookerCurrencyCode'];
          $Data['vTransactionId'] = $txn_id;
          $Data['eBookerPaymentPaid'] = $db_booking[0]['eBookerPaymentPaid'];
          $Data['eDriverPaymentPaid'] = $db_booking[0]['eDriverPaymentPaid'];
          $Data['eBookerConfirmation'] = $db_booking[0]['eBookerConfirmation'];
          $Data['ePaymentType'] = $db_booking[0]['ePaymentType'];
          $Data['eTripReturn'] = $db_booking[0]['eTripReturn'];
          $Data['dPaymentDate'] = $db_booking[0]['dPaymentDate'];
          $Data['eStatus'] = $db_booking[0]['eStatus'];
          $Data['vTimeZone'] = $db_booking[0]['vTimeZone'];
          
          $Adminmailcont = $ridesobj->email_cont_booker_confirmation($Data, 'Admin');            
          $Adminmaildata['details'] = $Adminmailcont;            
          $generalobj->send_email_user("RIDE_COMPLETION_CONFIRMATION_ADMIN",$Adminmaildata); 
          
          
          $Drivermailcont = $ridesobj->email_cont_booker_confirmation($Data, 'Driver');            
          $Drivermaildata['vDriverEmail'] = $Data['vDriverEmail'];
          $Drivermaildata['details'] = $Drivermailcont;
          $generalobj->send_email_user("RIDE_COMPLETION_CONFIRMATION_DRIVER",$Drivermaildata);
          
          echo 'rb';
          exit;
        }	
      }else{
        #wrong code sms or already send confirmation
			if($_REQUEST['From'] != ""){
			  $bookerphonenumber = str_replace("+","",$_REQUEST['From']);
			  $generalobj->send_sms($bookerphonenumber,"You have entered the wrong wrong code. Please try again");
			}	

        echo 'wrong code sms or already send confirmation';
        exit;
      }
    }    
  }
  
  $to      = 'chirag.esw@gmail.com';
  $subject = 'SMS Respone';
  $message = $vRemark;
  $headers = 'From: no-reply@webprojectsdemo.com' . "\r\n" .
      'X-Mailer: PHP/' . phpversion();
  
 // mail($to, $subject, $message, $headers);
  exit;
?>