<?
 date_default_timezone_set('Europe/Riga');
 ob_start();      
 session_start();
 define( '_TEXEC', 1 );
 define('TPATH_BASE', dirname(__FILE__) );
 define( 'DS', DIRECTORY_SEPARATOR );
 //header('Content-Type: text/html; charset=iso-8859-1');  
  
 require_once ( TPATH_BASE .DS.'includes'.DS.'defines.php' ); 
 require_once ( TPATH_BASE .DS.'includes'.DS.'configuration.php' );    
  
 ini_set("track_errors", true);    

 ##
 ## status receiving. this is where AirPay pings You in background.
 ##
 
 /**
 * merchant constants
 */

 $merchant_id		= 260;			// merchant ID provided by AirPay
 $merchant_secret	= "secretkey";			// merchant secret code provided by merchant
    
 # include AirPay class
 #include("airpay.class.php");
 include("/AirPay_v1.2/airpay.class.php");
 
 # create AirPay object
 $airpay = new airpay( $merchant_id, $merchant_secret );
 
 /*$status = $airpay->response($_POST, 'status');
 $message = "Airpay response >> ".$status[transaction_id]." >> ".$status[amount]." >> ".$status[status_id]." >> ".$status[secret];
 $to = 'mrunal.esw@gmail.com';
 $subject = 'Airpay response';
 $headers = 'MIME-Version: 1.0' . "\r\n";
 $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
 $headers .= 'From: no-reply@baltic-car.com' . "\r\n" .'X-Mailer: PHP/' . phpversion();
 mail($to, $subject, $message, $headers);*/
 #echo "<pre>";print_r($status);exit;
 
 
 
 # process the response to make sure we can trust the data
 if (!$ret = $airpay->response($_POST, 'status')) {
  // error
	// update Your logs for the investigation 
	$sql = "UPDATE temp_booking SET vTransactionStatus = 'Fail' WHERE vAdaptivePaymentKey = '".$ret["transaction_id"]."'";
  $db_sql=$obj->sql_query($sql);
 } else {                                      
     #echo "<pre>";print_r($ret);exit;
  #echo "<pre>";print_r($airpay->response($_POST, 'status'));exit;
	# $ret array description
	#
	# Common data submitted by merchant:
	# [version] 			- version of the AirPay used
	# [merchant_id] 		- merchant ID
	# [mc_transaction_id] 		- merchant invoice value
	# [transaction_id]		- transaction ID in AirPay system
	# [amount]			- amount in minor units
	# [currency]			- currency code
	# [description] 		- description
	# [optional1]			- optional fields
	#
	# Payment data
	# [payment_system] 		- alias of the payment system that was used
	# [status_id]			- status of the transaction
	# [payment_system_status]	- original status of the transaction provided by the payment system above
	#
	# Sign
	# [hash] 			- hash sign
	#
	# Additional data
	# [is_reference_of] 		- presents if the transaction is a reference of any other transaction. used when refund happened
	#
	# Possible payment statuses
	# -1	- transaction expired with no try for the payment, means that the client just closed the AirPay window without proceeding the payment
	#  0	- transaction is created by payment() or payment_req() methods of the AirPay class, means that the AirPay window opened
	#  1	- transaction is successfull, means that the merchant can provide the service or product to the client
	#  2	- transaction is pending, means that the client have tried to pay and the payment system is still processing the payment
	#  3	- transaction is rejected, means that the client have no funds or cancelled the payment etc
	#  4	- transaction is expired, means that it didn't receive any status after the pending for a long time (36 hours). consider it as rejected.
	#  5	- transaction is refunded, means that the client received the funds back and Your account was charged for this amount

	// update Your local database according to the received data, send e-mail etc

	# Always check the following for successfull transactions:
	# the transaction was never successfull before (except it is refunded now)
	# the amount and currency is exactly what You expect to receive according to Your local database
	if($ret['status_id'] == "1")
  {
        $sql = "SELECT * FROM temp_booking WHERE vAdaptivePaymentKey = '".$ret["transaction_id"]."'";
        $db_booking = $obj->MySQLSelect($sql);
        
        $Data['vBookingNo'] = $db_booking[0]['vBookingNo'];
        $Data['iRideId'] = $db_booking[0]['iRideId'];
        $Data['iRidePointId'] = $db_booking[0]['iRidePointId'];
        $Data['vFromPlace'] = $db_booking[0]['vFromPlace'];
        $Data['vToPlace'] = $db_booking[0]['vToPlace']; 
        $Data['vMainRidePlaceDetails'] = $db_booking[0]['vMainRidePlaceDetails'];
        $Data['iNoOfSeats'] = $db_booking[0]['iNoOfSeats'];
        $Data['fCommission'] = $db_booking[0]['fCommission'];
        $Data['fVat'] = $db_booking[0]['fVat'];
        $Data['fRidePrice'] = $db_booking[0]['fRidePrice'];
        $Data['fAmount'] = $db_booking[0]['fAmount'];
        $Data['dBookingDate'] = $db_booking[0]['dBookingDate'];
        $Data['dBookingTime'] = $db_booking[0]['dBookingTime'];
        $Data['iBookerId'] = $db_booking[0]['iBookerId'];
        $Data['iDriverId'] = $db_booking[0]['iDriverId'];
        $Data['vBookerFirstName'] = $db_booking[0]['vBookerFirstName'];
        $Data['vBookerLastName'] = $db_booking[0]['vBookerLastName'];
        $Data['vBookerAddress'] = $db_booking[0]['vBookerAddress'];
        $Data['vBookerCity'] = $db_booking[0]['vBookerCity'];
        $Data['vBookerState'] = $db_booking[0]['vBookerState'];
        $Data['vBookerCountry'] = $db_booking[0]['vBookerCountry'];
        $Data['vBookerZip'] = $db_booking[0]['vBookerZip'];
        $Data['vBookerPhone'] = $db_booking[0]['vBookerPhone'];
        $Data['vBookerEmail'] = $db_booking[0]['vBookerEmail'];
        $Data['vDriverFirstName'] = $db_booking[0]['vDriverFirstName'];
        $Data['vDriverLastName'] = $db_booking[0]['vDriverLastName'];
        $Data['vDriverPhone'] = $db_booking[0]['vDriverPhone'];
        $Data['vDriverEmail'] = $db_booking[0]['vDriverEmail'];
        $Data['vBookerCurrencyCode'] = $db_booking[0]['vBookerCurrencyCode'];
        $Data['vTransactionId'] = $ret["transaction_id"];
        $Data['eBookerPaymentPaid'] = $db_booking[0]['eBookerPaymentPaid'];
        $Data['eDriverPaymentPaid'] = $db_booking[0]['eDriverPaymentPaid'];
        $Data['eBookerConfirmation'] = $db_booking[0]['eBookerConfirmation'];
        $Data['ePaymentType'] = $db_booking[0]['ePaymentType'];
        $Data['eTripReturn'] = $db_booking[0]['eTripReturn'];
        $Data['dPaymentDate'] = $db_booking[0]['dPaymentDate'];
        $Data['eStatus'] = $db_booking[0]['eStatus'];
        $Data['vTimeZone'] = $db_booking[0]['vTimeZone'];
        $Data['vAdaptivePaymentKey'] = $db_booking[0]['vAdaptivePaymentKey'];
        
        $id = $obj->MySQLQueryPerform("booking_new",$Data,'insert');
        
        if($id){
          $sql = "DELETE FROM temp_booking WHERE vAdaptivePaymentKey = '".$ret["transaction_id"]."'";
          $db_sql=$obj->sql_query($sql);	
          
          $mailcont = $ridesobj->email_cont($Data);
          $mailcont_driver = $ridesobj->email_cont_driver($Data);
          $mailcont_booker = $ridesobj->email_cont_booker($Data);
          $mailcont_admin = $ridesobj->email_cont_admin($Data);
          
          $maildata['vBookerEmail'] = $Data['vBookerEmail'];
          $maildata['vDriverEmail'] = $Data['vDriverEmail'];
          $maildata['details'] = $mailcont;
                    
          $maildata_booker['vBookerEmail'] = $Data['vBookerEmail'];
          $maildata_booker['vDriverEmail'] = $Data['vDriverEmail'];
          $maildata_booker['details'] = $mailcont_booker;
          $generalobj->send_email_user("BOOKING_PASSENGER",$maildata_booker);
          
          $maildata_driver['vBookerEmail'] = $Data['vBookerEmail'];
          $maildata_driver['vDriverEmail'] = $Data['vDriverEmail'];
          $maildata_driver['details'] = $mailcont_driver;
          $generalobj->send_email_user("BOOKING_DRIVER",$maildata_driver);
          
          $maildata_admin['vBookerEmail'] = $Data['vBookerEmail'];
          $maildata_admin['vDriverEmail'] = $Data['vDriverEmail'];
          $maildata_admin['details'] = $mailcont_admin;
          $generalobj->send_email_user("BOOKING_ADMIN",$maildata_admin);
          /*$mailcont = $ridesobj->email_cont($Data);
          
          $maildata['vBookerEmail'] = $Data['vBookerEmail'];
          $maildata['vDriverEmail'] = $Data['vDriverEmail'];
          $maildata['details'] = $mailcont;
                    
          $generalobj->send_email_user("BOOKING_PASSENGER",$maildata);
          $generalobj->send_email_user("BOOKING_DRIVER",$maildata);
          $generalobj->send_email_user("BOOKING_ADMIN",$maildata);*/
          
          $iMemberId = $_SESSION['sess_iMemberId'];
          $vFirstName = $_SESSION["sess_vFirstName"];
          $vLastName = $_SESSION["sess_vLastName"];
          $vEmail = $_SESSION["sess_vEmail"];
          $vImage = $_SESSION["sess_vImage"];
          $eGender = $_SESSION["sess_eGender"];
          
          session_unset();
          
          $_SESSION['sess_iMemberId'] = $iMemberId;
          $_SESSION["sess_vFirstName"] = $vFirstName;
          $_SESSION["sess_vLastName"] = $vLastName;
          $_SESSION["sess_vEmail"] = $vEmail;
          $_SESSION["sess_vImage"] = $vImage;
          $_SESSION["sess_eGender"] = $eGender;
          
          #header("Location:".$tconfig["tsite_url"]."index.php?file=m-booking_confirm&id=".$id."&cd=1");
          #exit;
        }else{
        
        } 
  }else{
      $sql = "UPDATE temp_booking SET vTransactionStatus = 'Fail' WHERE vAdaptivePaymentKey = '".$ret["transaction_id"]."'";
      $db_sql=$obj->sql_query($sql);
  }   
 }
?>