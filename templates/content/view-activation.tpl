<div class="about-page">         
  <h2>{$smarty.const.LBL_ACCOUNT_ACTIVATION}</h2>
  
  <div class="right-about-part">
    <h1>{$smarty.const.LBL_ACCOUNT_ACTIVATION}</h1>
    <p>{$msg}</p>
  </div>
  {include file="left.tpl"}
  <div style="clear:both;"></div>
</div>