{literal}
<script> 			
	function checkvalid(){
        //alert('hello..');
		resp = jQuery("#frmcontact").validationEngine('validate');
		//alert(resp);return false;
		if(resp == true){
			document.frmcontact.submit();
			}else{
			return false;
		}		
	}
	function redirectcancel()
	{  
		window.location="{/literal}{$tconfig.tsite_url}{literal}";
		return false;
	} 
</script>
{/literal} 
{if $smarty.get.var_msg neq ''}          
{if $smarty.get.msg_code eq '1'}
{literal}
<script>        
	showNotification({type : 'success', message: '{/literal}{$var_msg}{literal}'});  
</script>
{/literal}
{/if}
{/if} 

{if $smarty.get.var_msg neq ''}          
{if $smarty.get.msg_code eq '0'}
{literal}
<script>
	showNotification({type : 'error', message: '{/literal}{$var_msg}{literal}'});
</script>
{/literal}
{/if}
{/if}
{if $var_err_msg neq ''}          
{literal}
<script>
	showNotification({type : 'error', message: '{/literal}{$var_err_msg}{literal}'});
</script>
{/literal}
{/if}
<div class="body-inner-part">
	<div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_CONTACT_US}</span></div>
	<div class="main-inner-page">
		<h2>{$smarty.const.LBL_CONTACT_US}</h2>
		<div class="right-inner-part">
			<div class="contact-inner">
				<div class="contact-left">
					<h2>{$smarty.const.LBL_ADDRESS}</h2>
					<p>{$COMPANY_ADDRESS|nl2br}<br />
						{$smarty.const.LBL_PHONE} : &nbsp;{$SUPPORT_PHONE}<br />
					{$smarty.const.LBL_EMAIL} : &nbsp;<a href="mailto:{$SUPPORT_MAIL}">{$SUPPORT_MAIL}</a></p>
					<p class="map">{$GOOGLE_MAP}</p>
				</div>
				<form name="frmcontact" id="frmcontact" action="index.php?file=c-contactus_a" method="post">
					<div class="contact-right">
						<h2>{$smarty.const.LBL_CONTACT_FORM}</h2>
						<p>{$smarty.const.LBL_CONTACT_FORM_DESC}<span>* {$smarty.const.LBL_REQUERED_FIELD}</span></p>
						<div class="form">
							<span>
								<label>{$smarty.const.LBL_NAME} : * </label>
								<input class="validate[required] form-box" name="Data[vFirstName]" id="vFirstName" value="{$Data.vFirstName}" type="text" />
							</span>
							<span>
								<label>{$smarty.const.LBL_EMAIL} : * </label>
								<input class="validate[required,custom[email]] form-box" name="Data[vEmail]" id="vEmail" value="{$Data.vEmail}" type="text" />
							</span>
							<!--<span>
								<label>{$smarty.const.LBL_PHONE} / {$smarty.const.LBL_MOBILE} : * </label>
								<input class="validate[required,custom[phone]] form-box" name="Data[cellno]" id="cellno" value="{$Data.cellno}" type="text" />
							</span>
							<label>{$smarty.const.LBL_SUBJECT} : * </label><br />
								<select name="Data[eSubject]" id="eSubject" class="validate[required] form-box-cont">
								<option>{$smarty.const.LBL_GENERAL_INQUERY}</option>
								<option>{$smarty.const.LBL_CUST_SUPPORT}</option>
								<option>{$smarty.const.LBL_INVESTOR_RELATION}</option>
								<option>{$smarty.const.LBL_MARKETING}</option>	
							</select>-->
							<span>
								<label>{$smarty.const.LBL_MESSAGE} : * </label>
								<textarea class="validate[required] form-box1" name="Data[tSubject]" id="tSubject" cols="" rows=""></textarea>
							</span>
						<span><label class="blanck-co">&nbsp;</label><a href="javascript:void(0);" onClick="javascript:checkvalid();return false;">{$smarty.const.LBL_SUBMIT}</a></span> </div>
					</div>
				</form>
				<div style="clear:both;"></div>
			</div>
		</div>
	{include file="left.tpl"} </div>
	<div style="clear:both;"></div>
</div>
