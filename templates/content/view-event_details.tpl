<script type="text/javascript" src="{$tconfig.tsite_javascript}jquery.raty.js"></script>
{literal}
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/src/markerclusterer_compiled.js"></script>
{/literal}
<div class="about-page">
<input type="hidden" name="iEventId" id="iEventId" value="{$iEventId}">
  <div class="bradecrame"><span><a href="{$tconfig.tsite_url}">Home</a>&raquo;&nbsp;&nbsp;<a href="{$tconfig.tsite_url}events">Find Opportunities</a>&raquo;&nbsp;&nbsp;{$events[0].vTitle}</span></div>
  <h2>Event Details</h2>
  <div class="event-details">
    <div class="top-title">       
      <h2>{$events[0].vTitle}</h2>
      
      <div style="float:left; width:50%; margin:11px 0 15px 0; position:relative; left:280px;">
      <span><a href="{$tconfig.tsite_url}organization-details/{$events[0].iOrganizationId}" style="color:#92A20A;">{$events[0].vOrganizationName}</a></span>
      <span>
      <div id="star" style="margin:0 auto; float:left;"></div>
      {literal}
        <script>
          $.fn.raty.defaults.path = '{/literal}{$tconfig.tsite_images}{literal}img';
          $('#star').raty({ readOnly: true, score: {/literal}{$organization_rerings}{literal},width: 140 });
        </script>
      {/literal}
      <div style="text-align:center; color:#333333; font-size:15px; margin-top:6px; float:left;"><a href="{$tconfig.tsite_url}organization-details/{$events[0].iOrganizationId}">{$organization_reviews} Reviews</a></div>
      </span>
      </div>
      
      </div>
    <div class="details-main">
      <div class="left-deta">
        <div class="tabs2"><a id="home" href="{$tconfig.tsite_url}event-details/{$iEventId}" class="active">Home</a>{if $attending_students gt 0}<a id="students" href="javascript:void(0);" onClick="view_event_related('Students', {$iEventId})">Students Attending({$attending_students})</a>{/if}</div>
        <div id="shw_det">
        <img id="ajaxloader" style="margin: 20px 0 0 291px;display:none;" src="{$tconfig.tsite_images}252.gif">
        {if $events[0].img_url neq ''}<div class="video1"><img src="{$events[0].img_url}" alt="" /></div>{/if}
        <div class="share"> 
          <a href="javascript:void(0);" class="addthis_button_facebook" addthis:url="{$tconfig.tsite_url}event-details/{$iEventId}" addthis:title="{$events[0].vTitle}" addthis:description="{$events[0].tDetails|truncate:200}">
            <img onmouseout="this.src='{$tconfig.tsite_images}f-de.jpg'" onmouseover="this.src='{$tconfig.tsite_images}f-de-hover.jpg'" onclick="return submitsearch(document.frmsearch);" src="{$tconfig.tsite_images}f-de.jpg" alt=""/>
          </a>
          <a href="javascript:void(0);" class="addthis_button_twitter" addthis:url="{$tconfig.tsite_url}event-details/{$iEventId}" addthis:title="{$events[0].vTitle}" addthis:description="{$events[0].tDetails|truncate:200}">
          <img onmouseout="this.src='{$tconfig.tsite_images}t-de.jpg'" onmouseover="this.src='{$tconfig.tsite_images}t-de-hover.jpg'" onclick="return submitsearch(document.frmsearch);" src="{$tconfig.tsite_images}t-de.jpg" alt=""/>
          </a>
        </div>
        {if $events[0].tDetails neq ''}
        <div class="full-description">
         <h2>About</h2> 
          <p>{$events[0].tDetails|nl2br}</p>
        </div>
        {/if}
        {if $skills|@count gt 0}
        <div class="full-description">
          <h2>Skills</h2> 
          <ul>
          {section name="skills" loop=$skills}
          <li>{$skills[skills].vTitle}</li>
          {/section}            
          </ul>
        </div>
        {/if}
        {if $suitable_for|@count gt 0}
        <div class="full-description">
          <h2>Good Match For</h2>
          <ul>
          {section name="suitable" loop=$suitable_for}
          <li>{$suitable_for[suitable].vSuitableTitle}</li>
          {/section}          
          </ul>
        </div>
        {/if}
        {if $events[0].tRequirements neq ''}
        <div class="full-description">
        <h2>Requirement &amp; Commitment</h2>
        <p>{$events[0].tRequirements|nl2br}</p>
        </div>
        {/if}
      </div>
      </div>
      <div class="right-deta">
        <h2><a href="javascript:void(0);"><img src="{$tconfig.tsite_images}location.png" alt="" />{$events[0].vCity}, {$events[0].vState}</a></h2>
        <div class="donate">
          <div class="support_link"> <a href="javascript:void(0);" class="donateGreenBtn" id="eve_sub" onClick="subscribe_event();"><span class="support_text">Subscribe</span> <br>
            <span>FOR THIS Event</span></a> </div>
          {if $categories|@count gt 0}
          <div class="clint-category1">
          <h2>Categories</h2>
            <ul>
              {section name="category" loop=$categories}
              <li><a href="javascript:void(0);" onClick="srch_by_cat({$categories[category].iCategoryId}, 'category');">{$categories[category].vCategory}</a></li> 
              {/section}          
            </ul>
          </div>
          {/if}    
          <div class="clint-category">
          <ul>
            <li>
              <h2>When</h2>
              {if $events[0].eEventType eq 'Fixed'} 
              <p><img src="{$tconfig.tsite_images}calender.png" alt="" />
                <em>{$generalobj->DateTime($events[0].dStartDate,10)} {if $events[0].dEndDate neq '0000-00-00'} <b>To</b> {$generalobj->DateTime($events[0].dEndDate,10)}{/if}<br />
                {$events[0].vStartTime} - {$events[0].vEndTime}</em>
              </p>
              {else}
              <p><img src="{$tconfig.tsite_images}calender.png" alt="" />
                <em>It's flexible! We'll work with your schedule.</em>
              </p>
              {/if}
            </li>
            <li>
              <h2>Where</h2>
              <p><img src="{$tconfig.tsite_images}location.png" alt="" /><em>{$events[0].vAddress}, <br>{$events[0].vCity}, {$events[0].vState}, <br>{$events[0].vCountry}</em></p>
            </li>
            <li>
              <div id="map_canvas" style="width: 274px; height: 274px; border: 10px solid #F4F4F4;"></div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div style="clear:both;"></div>
</div>
<div style="clear:both;"></div>
</div>
<form name="eventsfrm" id="eventsfrm" method="post" action="">
  <input type="hidden" name="iCategoryId" id="iCategoryId" value="{$iCategoryId}">    
</form>
<script type="text/javascript">var addthis_config ={literal}{"data_track_addressbar":true}{/literal};</script><script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-50b5aa4b7afc65e8"></script>

{literal}<script type="text/javascript">$(document).ready(function(){gmap_location('{/literal}{$events[0].vAddress}, {$events[0].vCity}, {$events[0].vState}, {$events[0].vCountry}{literal}');});</script>{/literal} 