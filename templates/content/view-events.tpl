<div class="about-page">
  <h2>FIND OPPORTUNITIES</h2>
  <div class="left-about-part">
    <form name="frmsearch" id="frmsearch" method="post">
    <div class="keywords-left">
      <input name="bykeyword" id="bykeyword" type="text" class="validate[required] keywords-input" placeholder="Search Events" style="width:136px;" value="{$srchkeyword}"/>
      <a href="javascript:void(0);" onClick="chk_serch_valid();">Go</a> 
    </div>
    </form>
    <div class="informaction2">
      <h1>Filter By Category</h1>
      <ul>
        {section name="evecat" loop=$category}
        <li><a href="javascript:void(0);" onClick="srch_by_cat({$category[evecat].iCategoryId}, 'category');">{$category[evecat].vCategory}</a></li>
        {/section}        
      </ul>
    </div>
    <div class="informaction2" style="margin-top:15px;">
      <h1>Suitable For</h1>
      <ul>
        {section name="suitable" loop="$suitables"}
        <li><a href="javascript:void(0);" onClick="srch_by_cat({$suitables[suitable].iSuitableTypeId}, 'suitable');">{$suitables[suitable].vSuitableTitle}</a></li>      
        {/section}  
      </ul>
    </div> 
  </div>
  {if $events|@count gt 0}
  <div class="right-about-part">
    <div class="event-listing">
      <h2>{if $srchkeyword eq '' and $iCategoryId eq '' and $iSuitableTypeId eq '' and $ordby eq ''}All Events{else}Events{/if} 
        <div class="inner-bot-top">
          <p style="float:right; text-align:right; font-size:14px; text-transform:uppercase; padding-top:3px;">Sort By : &nbsp;
            <select name="ordbydrop" id="ordbydrop" onchange="srch_by_cat(this.value, 'orderby');" style="width:172px; padding:3px; font-size:12px; color:#666666;">
              <option value="">-- Select --</option>
              <option value="posted_date" {if $ordby eq 'posted_date'} selected {/if}>Date Posted</option>
              <option value="event_date" {if $ordby eq 'event_date'} selected {/if}>Event Date</option> 
            </select>
          </p>
        </div>
      </h2>
      {section name="events" loop=$events}
      <div class="event-listing-details">
        <div class="event-img"><a href="{$tconfig.tsite_url}event-details/{$events[events].iEventId}"><img src="{$events[events].img_url}" alt="" /></a></div>
        <div class="event-listing-right">
          <h2><a href="{$tconfig.tsite_url}event-details/{$events[events].iEventId}">{$events[events].vTitle}</a></h2>
          <h3>{$events[events].vOrganizationName}</h3>
          <p>{$events[events].tDetails|truncate:300}</p>
          <span><img src="{$tconfig.tsite_images}con-icon.jpg" alt="" />{$events[events].vCity}, {$events[events].vState}<a href="{$tconfig.tsite_url}event-details/{$events[events].iEventId}">View More</a></span> </div>
      </div>
      {/section}  
    </div>
    <div class="paging"> {$recmsg} <span>{$page_link}</span></div>
  </div>
  {else}
   <div class="right-about-part">
    <p style="font-size: 20px;text-align: center;font-size: 20px;margin-top:40px;">Events not found.</p>
   </div>
   {/if}
  <div style="clear:both;"></div>
</div>
<form name="eventsfrm" id="eventsfrm" method="post" action="">
  <input type="hidden" name="iCategoryId" id="iCategoryId" value="{$iCategoryId}">
  <input type="hidden" name="srchkeyword" id="srchkeyword" value="{$srchkeyword}">
  <input type="hidden" name="ordby" id="ordby" value="{$ordby}">
  <input type="hidden" name="iSuitableTypeId" id="iSuitableTypeId" value="{$iSuitableTypeId}">
</form>