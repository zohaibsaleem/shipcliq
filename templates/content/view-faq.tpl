<div class="about-page">         
  <h2>Faq's</h2>
  <div class="right-about-part">
    <h1>Faq's</h1>
    {section name="que" loop=$db_faq}
      <li><a href="{$tconfig.tsite_url}index.php?file=c-faq#{$db_faq[que].iFaqId}">{$db_faq[que].vTitle}</a></li>
      {/section}
      </ul>         
      {section name=i loop=$db_faq}
      <div class="faq-ans">
      <h1><a name="{$db_faq[i].iFaqId}">Q.&nbsp;&nbsp;{$db_faq[i].vTitle}</a></h1>
      <span><p>A.</p>{$db_faq[i].tAnswer}</span>
      </div>
      {/section}
  </div>
  {include file="left.tpl"}
  <div style="clear:both;"></div>
</div>
