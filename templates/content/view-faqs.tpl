<div class="body-inner-part">
  <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_FAQ}</span></div>
  <div class="main-inner-page">
    <h2>{$smarty.const.LBL_FAQ}</h2>   
    <div class="right-inner-part">
      <h2>{$smarty.const.LBL_FAQ} 
      <span class="faq-select">
        <select name="iFaqcategoryId" id="iFaqcategoryId" onChange="flt_me(this.value);" style="border: 1px solid #CCCCCC;margin: 0 3px;padding: 6px;">
          <option value="">{$smarty.const.LBL_ALL}</option>
          {section name="fcat" loop=$db_faq_cat}
            <option value="{$db_faq_cat[fcat].iFaqcategoryId}" {if $iFaqcategoryId eq $db_faq_cat[fcat].iFaqcategoryId} selected {/if}>{$db_faq_cat[fcat].vTitle}</option>
          {/section}
        </select>
      </span>
      </h2>
      <div class="faq">
        <ul>
          {if $iFaqcategoryId eq ''}
          {section name="que" loop=$db_faq_cat}   
          <li> <h2>{$db_faq_cat[que].vTitle}</h2>
            {section name=i loop=$db_faq}
            {if $db_faq[i].iFaqcategoryId eq $db_faq_cat[que].iFaqcategoryId}
            <div class="faq-ans" style="width:100%;">
              <h3><a name="{$db_faq[i].iFaqId}"><!--Q.&nbsp;&nbsp;-->{$db_faq[i].vTitle}</a></h3>
              <span><!--<strong>A.</strong>-->
              {$db_faq[i].tAnswer|nl2br}</span> </div>
            {/if}
            {/section} </li>
          {/section}
          {else}
          {section name="que" loop=$db_faq_cat}             
          {if $db_faq_cat[que].iFaqcategoryId eq $iFaqcategoryId}
          <li> <h2>{$db_faq_cat[que].vTitle}</h2>
            {section name=i loop=$db_faq}
            {if $db_faq[i].iFaqcategoryId eq $db_faq_cat[que].iFaqcategoryId}
            <div class="faq-ans" style="width:100%;">
              <h3><a name="{$db_faq[i].iFaqId}">Q.&nbsp;&nbsp;{$db_faq[i].vTitle}</a></h3>
              <span><strong>A.</strong>
              {$db_faq[i].tAnswer|nl2br}</span> </div>
            {/if}
            {/section} </li>
            {/if}            
          {/section}
          {/if}
        </ul>
      </div>
    </div>
	{include file="left.tpl"}
  </div>
  <div style="clear:both;"></div>
</div>
{literal}
  <script>
    function flt_me(id){
      window.location="{/literal}{$tconfig.tsite_url}{literal}index.php?file=c-faqs&id="+id;
      return false;
    }
  </script>
{/literal}
