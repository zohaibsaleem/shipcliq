<script type="text/javascript" src="{$tconfig.tsite_url}rrb/zebra_datepicker.src.js"></script>
<link rel="stylesheet" href="{$tconfig.tsite_url}rrb/default.css" type="text/css">
<link rel="stylesheet" type="text/css" href="{$tconfig.tsite_stylesheets}front/simptip-mini.css" media="screen,projection" />
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script type="text/javascript" src="{$tconfig.tsite_javascript}gmap3.js"></script> 
<div class="body-inner-part">
<div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_SEARCH} {$smarty.const.LBL_RIDE}</span></div>
<div class="main-inner-page">
  <h2>{$smarty.const.LBL_SEARCH}<span>{$smarty.const.LBL_RIDE}</span></h2>
  <div class="search-car">
    <div class="search-car-top-part"> <span>{$smarty.const.LBL_SEARCH_RIDE}</span>          
      <form name="home_search" id="home_search" method="post" action="{$tconfig.tsite_url}index.php?file=c-ride_list">
        <p>
          <input name="search" id="search" type="hidden" value="place">
          <input name="From_lat_long" id="From_lat_long" type="hidden" value="">
          <input name="To_lat_long" id="To_lat_long" type="hidden" value="">      
          <input name="From" id="From" type="text" placeholder="{$smarty.const.LBL_FROM}" class="validate[required] search-car-find-cla" value=""/>
          <input placeholder="{$smarty.const.LBL_TO}" name="To" id="To" type="text" class="validate[required] search-car-find-cla" value=""/>
          <input name="searchdate" id="searchdate" type="text" class="search-car-find-date" value="" placeholder="{$smarty.const.LBL_DATE}"/>
          <a href="javascript:void(0);" onClick="search_now();">{$smarty.const.LBL_SEARCH}</a>
        </p>
      </form>
    </div>
    <div class="body-mid-part-search-car">
     <!--{if $ridesarroundyou|count gt 0}
      <div class="body-mid-part-inner">
        <div class="rides-around">
          <p>{$smarty.const.LBL_RIDE}<br />
            {$smarty.const.LBL_AROUND}<br />
            <strong>{$smarty.const.LBL_YOU}</strong></p>
          <a href="{$tconfig.tsite_url}index.php?file=c-ride_list&search=arround_you">{$smarty.const.LBL_VIEW_MORE}</a> </div>
        {section name="around" loop=$ridesarroundyou max=2} <a href="{$tconfig.tsite_url}index.php?file=c-ride_details&from=home&id={$ridesarroundyou[around].iRideId}&dt={$ridesarroundyou[around].strtotime}&ret={$ridesarroundyou[around].return}">
        <div class="trip-main {if $smarty.section.around.last} trip-main-last {/if}">
          <div class="trip roundcol {if $smarty.section.around.last} last {/if} simptip-position-top simptip-multiline simptip-info simptip-smooth" data-tooltip="{$ridesarroundyou[around].vMainDeparture} - {$ridesarroundyou[around].vMainArrival}">
            <div style="float:left;width: 114px;">
              <div style="float:left;width: 114px;"> <img src="{$ridesarroundyou[around].img}" alt="" style="margin: 0 0 10px;"/> </div>
              {$ridesarroundyou[around].pref} </div>
            <div class="trip-deails">
              <div class="trip-deails-left">
                <h2>{$generalobj->DateTime($ridesarroundyou[around].dDate,10)} - {$generalobj->DateTime($ridesarroundyou[around].time,12)}</h2>
                <p>{$ridesarroundyou[around].vMainDeparture|truncate:10} - {$ridesarroundyou[around].vMainArrival|truncate:10}</p>
                <p><span style="display: block; width: 65px; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 0;"> <span style=" position:relative; top:-10px; margin-right: 10px;display: block; width: {$ridesarroundyou[around].rates}%; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 -13px;"></span> </span></p>
                <span>{$smarty.const.LBL_CAR}: {$ridesarroundyou[around].membercar}</span> </div>
              <div class="trip-deails-right">{$ridesarroundyou[around].price}</div>
            </div>
          </div>
          <div style=" clear:both;"></div>
        </div>
        </a> 
        {/section} 
        </div>
      {/if}-->
     
      <!--Rides arroundyou Rides ----->
      {if $ridesarroundyou|count gt 0}
      <div class="body-mid-part-inner">
        <div class="rides-around">    
          <p>{$smarty.const.LBL_RIDE}<br />
          {$smarty.const.LBL_AROUND}<br />
          <strong>{$smarty.const.LBL_YOU}</strong>
          </p>
          <a href="{$tconfig.tsite_url}index.php?file=c-ride_list&search=arround_you">{$smarty.const.LBL_VIEW_MORE}</a> </div>
        {section name=i loop=$ridesarroundyou max=2} <a href="{$tconfig.tsite_url}index.php?file=c-ride_details&from=home&id={$ridesarroundyou[i].iRideId}&dt={$ridesarroundyou[i].strtotime}&ret={$ridesarroundyou[i].return}">
        <div {if $smarty.section.i.last and $ridesarroundyou|count gt 1}class="trip-main trip-main-last"{else}class="trip-main"{/if}>
          <div class="trip roundcol {if $smarty.section.i.last} last {/if} simptip-position-top simptip-multiline simptip-info simptip-smooth" data-tooltip="{$ridesarroundyou[i].vMainDeparture} - {$ridesarroundyou[i].vMainArrival}">
            <div class="ho-box-left2">
           <div class="ho-box-left-img"> <img src="{$ridesarroundyou[i].img}" alt="" style="margin: 0 0 10px;"/> </div>
              {$ridesarroundyou[i].pref} </div>
            <div class="trip-deails">
              <div class="trip-deails-left">
                <h2>{$generalobj->DateTimeFormat($ridesarroundyou[i].dDate)} - {$generalobj->DateTime($ridesarroundyou[i].time,12)}</h2>
                <p>{$ridesarroundyou[i].vMainDeparture|truncate:10} - {$ridesarroundyou[i].vMainArrival|truncate:10}</p>
                <p> <span style="display: block; width: 65px; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 0;"> <span style="  position:relative; top:-10px; margin-right: 10px;display: block; width: {$ridesarroundyou[i].rating}%; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 -13px;"></span> </span> </p>
                <span>{$smarty.const.LBL_CAR}: {$ridesarroundyou[i].membercar|truncate:10}</span></div>
              <div class="trip-deails-right" style="font-size:21px;">{$ridesarroundyou[i].price}</div>
              <div class="trip-deails-right-bot"></div>
            </div>
          </div>
          </a>
          <div style=" clear:both;"></div>
        </div>
        {/section} </div>
      {/if}
      <!--Rides arroundyou Rides ends----->
      <!--Airport Rides ----->    
      {if $AIRPORTS_RIDES_SHOW eq 'Yes'}
      {if $airportmainarr|count gt 0}
      <div class="body-mid-part-inner">
        <div class="rides-around2">
          <p>{$smarty.const.LBL_AIRPORT}<br />
            <strong>{$smarty.const.LBL_RIDE}</strong>
          </p>
          <a href="{$tconfig.tsite_url}index.php?file=c-ride_list&search=simple&type=airport">{$smarty.const.LBL_VIEW_MORE}</a> </div>
        {section name=i loop=$airportmainarr max=2} <a href="{$tconfig.tsite_url}index.php?file=c-ride_details&from=home&id={$airportmainarr[i].iRideId}&dt={$airportmainarr[i].strtotime}&ret={$airportmainarr[i].return}">
        <div {if $smarty.section.i.last and $airportmainarr|count gt 1}class="trip-main trip-main-last"{else}class="trip-main"{/if}>
          <div class="trip airportcol {if $smarty.section.i.last} last {/if} simptip-position-top simptip-multiline simptip-info simptip-smooth" data-tooltip="{$airportmainarr[i].vMainDeparture} - {$airportmainarr[i].vMainArrival}">
            <div class="ho-box-left2">
             <div class="ho-box-left-img"><img src="{$airportmainarr[i].memberimage}" alt="" style="margin: 0 0 10px;"/> </div>
              {$airportmainarr[i].pref} </div>
            <div class="trip-deails">
              <div class="trip-deails-left">
                <h2>{$generalobj->DateTimeFormat($airportmainarr[i].dDateOut)} - {$generalobj->DateTime($airportmainarr[i].deptime,12)}</h2>
                <p>{$airportmainarr[i].vMainDeparture|truncate:10} - {$airportmainarr[i].vMainArrival|truncate:10}</p>
                <p> <span style="display: block; width: 65px; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 0;"> <span style=" position:relative; top:-10px; margin-right: 10px;display: block; width: {$airportmainarr[i].rating}%; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 -13px;"></span> </span></p>
                <span>{$smarty.const.LBL_CAR}: {$airportmainarr[i].membercar|truncate:15}</span> </div>
              <div class="trip-deails-right" style="font-size:21px;">{$airportmainarr[i].totprice}</div>
              <div class="trip-deails-right-bot"></div>
            </div>
          </div>
          <div style=" clear:both;"></div>
        </div>
        </a> {/section}
      </div>
      {/if}
      {/if}
      <!--Airport Rides ends----->
                               
      <!--Ladies Only Rides ----->
      {if $LADIES_ONLY_SHOW eq 'Yes'}
      {if $ladiesmainarr|count gt 0}
      <div class="body-mid-part-inner">
        <div class="rides-around3">
          <p>{$smarty.const.LBL_LADIES}<br />
          <strong>{$smarty.const.LBL_ONLY}</strong>
          </p>
          <a href="{$tconfig.tsite_url}index.php?file=c-ride_list&search=simple&type=ladiesonly">{$smarty.const.LBL_VIEW_MORE}</a> </div>
        {section name=i loop=$ladiesmainarr max=2} <a href="{$tconfig.tsite_url}index.php?file=c-ride_details&from=home&id={$ladiesmainarr[i].iRideId}&dt={$ladiesmainarr[i].strtotime}&ret={$ladiesmainarr[i].return}">
        <div {if $smarty.section.i.last and $ladiesmainarr|count gt 1}class="trip-main trip-main-last"{else}class="trip-main"{/if}>
          <div class="trip ladiescol {if $smarty.section.i.last} last {/if} simptip-position-top simptip-multiline simptip-info simptip-smooth" data-tooltip="{$ladiesmainarr[i].vMainDeparture} - {$ladiesmainarr[i].vMainArrival}">
                        <div class="ho-box-left2">
             <div class="ho-box-left-img"> <img src="{$ladiesmainarr[i].memberimage}" alt="" style="margin: 0 0 10px;"/> </div>
              {$ladiesmainarr[i].pref} </div>
            <div class="trip-deails">
              <div class="trip-deails-left">
                <h2>{$generalobj->DateTimeFormat($ladiesmainarr[i].dDateOut)} - {$generalobj->DateTime($ladiesmainarr[i].deptime,12)}</h2>
                <p>{$ladiesmainarr[i].vMainDeparture|truncate:10} - {$ladiesmainarr[i].vMainArrival|truncate:10}</p>
                <p> <span style="display: block; width: 65px; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 0;"> <span style=" position:relative; top:-10px; margin-right: 10px;display: block; width: {$ladiesmainarr[i].rating}%; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 -13px;"></span> </span> </p>
                <span>{$smarty.const.LBL_CAR}: {$ladiesmainarr[i].membercar|truncate:15}</span> </div>
              <div class="trip-deails-right" style="font-size:21px;">{$ladiesmainarr[i].totprice}</div>
              <div class="trip-deails-right-bot"></div>
            </div>
          </div>
          <div style=" clear:both;"></div>
        </div>
        </a> {/section} 
        </div>
      {/if}
      {/if}
      <!--Ladies Only Rides Ends----->
      
      <!--Shopping Rides----->
      {if $SHOPPING_RIDES_SHOW eq 'Yes'}
      {if $shoppingmainarr|count gt 0}
      <div class="body-mid-part-inner">
        <div class="rides-around4">
          <p>{$smarty.const.LBL_SHOPPING}<br />
            <strong>{$smarty.const.LBL_RIDE}</strong>
          </p>
          <a href="{$tconfig.tsite_url}index.php?file=c-ride_list&search=simple&type=shopping">{$smarty.const.LBL_VIEW_MORE}</a> </div>
        {section name=i loop=$shoppingmainarr max=2} <a href="{$tconfig.tsite_url}index.php?file=c-ride_details&from=home&id={$shoppingmainarr[i].iRideId}&dt={$shoppingmainarr[i].strtotime}&ret={$shoppingmainarr[i].return}">
        <div {if $smarty.section.i.last and $shoppingmainarr|count gt 1}class="trip-main trip-main-last"{else}class="trip-main"{/if}>
          <div class="trip shoppingcol {if $smarty.section.i.last} last {/if} simptip-position-top simptip-multiline simptip-info simptip-smooth" data-tooltip="{$shoppingmainarr[i].vMainDeparture} - {$shoppingmainarr[i].vMainArrival}">
                        <div class="ho-box-left2">
             <div class="ho-box-left-img"> <img src="{$shoppingmainarr[i].memberimage}" alt="" style="margin: 0 0 10px;"/> </div>
              {$shoppingmainarr[i].pref} </div>
            <div class="trip-deails">
              <div class="trip-deails-left">
                <h2>{$generalobj->DateTimeFormat($shoppingmainarr[i].dDateOut)} - {$generalobj->DateTime($shoppingmainarr[i].deptime,12)}</h2>
                <p>{$shoppingmainarr[i].vMainDeparture|truncate:10} - {$shoppingmainarr[i].vMainArrival|truncate:10}</p>
                <p> <span style="display: block; width: 65px; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 0;"> <span style=" position:relative; top:-10px; margin-right: 10px;display: block; width: {$shoppingmainarr[i].rating}%; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 -13px;"></span> </span> </p>
                <span>{$smarty.const.LBL_CAR}: {$shoppingmainarr[i].membercar|truncate:15}</span> </div>
              <div class="trip-deails-right" style="font-size:21px;">{$shoppingmainarr[i].totprice}</div>
              <div class="trip-deails-right-bot"></div>
            </div>
          </div>
          <div style=" clear:both;"></div>
        </div>
        </a> {/section} 
        </div>
      {/if}
      {/if}
      <!--Shopping Rides Ends----->
      
      
      <div style="clear:both;"></div>
    </div>
  </div>
</div>
<div style="clear:both;"></div>
</div>
<div id="map-canvas" class="gmap3"></div>
{literal}
<script type="text/javascript">
$('#searchdate').Zebra_DatePicker({
      direction: 0
});

function initialize() {
    //var input = document.getElementById("From);     
    //var options = {componentRestrictions: {country: 'us'}};
                 
    var from = new google.maps.places.Autocomplete(document.getElementById('From'));
    var to = new google.maps.places.Autocomplete(document.getElementById('To'));
}  
             
google.maps.event.addDomListener(window, 'load', initialize);

function search_now(){ 
  resp = jQuery("#home_search").validationEngine('validate');    
	if(resp == true){
    if(document.getElementById('From').value !='' && document.getElementById('To').value != ''){
      var From = $("#From").val();
      $("#map-canvas").gmap3({
              getlatlng:{
                address:  From,
                callback: function(results){
                 $("#From_lat_long").val(results[0].geometry.location);
                }
              }
      });
      
      var To = $("#To").val();
      $("#map-canvas").gmap3({
              getlatlng:{
                address:  To,
                callback: function(results){
                 $("#To_lat_long").val(results[0].geometry.location);
                 document.home_search.submit();
                }
              }
      }); 
      return false;
    }else{
      alert('{/literal}{$smarty.const.LBL_SELECT_FROM_TO_ADD}{literal}');
      return false;
    }
  }else{
    return false;
  }
}

/*function go_get_latlong(from){
   if(from == 'From'){
     var From = $("#From").val();
     $("#map-canvas").gmap3({
            getlatlng:{
              address:  From,
              callback: function(results){
               $("#From_lat_long").val(results[0].geometry.location);
              }
            }
      });      
   }else{
     var To = $("#To").val();
     $("#map-canvas").gmap3({
            getlatlng:{
              address:  To,
              callback: function(results){
               $("#To_lat_long").val(results[0].geometry.location);
              }
            }
      });       
   }
} */
</script>
  <script type="text/javascript">        
    $(function() {
        var from = document.getElementById('From');        
        var autocomplete_from = new google.maps.places.Autocomplete(from);
    		google.maps.event.addListener(autocomplete_from, 'place_changed', function() {
    			var place = autocomplete_from.getPlace();     		
    			go_get_latlong('From');
    		});
    		
    		var to = document.getElementById('To');
    		var autocomplete_to = new google.maps.places.Autocomplete(to);
    		google.maps.event.addListener(autocomplete_to, 'place_changed', function() {
    			var place = autocomplete_to.getPlace();     			
    			go_get_latlong('to');
    		});
    });
      
  </script>
{/literal}