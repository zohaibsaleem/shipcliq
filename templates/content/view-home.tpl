<link href='http://fonts.googleapis.com/css?family=Raleway:400,700,300,500,900,800,600,200,100' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="{$tconfig.tsite_stylesheets}front/simptip-mini.css" media="screen,projection" />
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script type="text/javascript" src="{$tconfig.tsite_javascript}gmap3.js"></script>
<div id="main-div">
  <div id="main-div-top-bg">
  <div id="main-div-top-second-bg">
  <div id="main-top-part">
  <div class="main-top-part-inner"><span>
  <a href="{$FACEBOOK_LINK}" target="_blank"><img onmouseout="this.src='{$tconfig.tsite_images}home/{$THEME}/fb.png'" onmouseover="this.src='{$tconfig.tsite_images}home/{$THEME}/fb-hover.png'" src="{$tconfig.tsite_images}home/{$THEME}/fb.png" alt=""/></a> <a href="{$TWITTER_LINK}" target="_blank"><img onmouseout="this.src='{$tconfig.tsite_images}home/{$THEME}/twi.png'" onmouseover="this.src='{$tconfig.tsite_images}home/{$THEME}/twi-hover.png'" src="{$tconfig.tsite_images}home/{$THEME}/twi.png" alt=""/></a>
      <select name="sess_price_ratio" id="sess_price_ratio" onChange="change_currency(this.value);" class="lag">
        {section name="currrb" loop=$db_curr_mst}
        <option value="{$db_curr_mst[currrb].vName}" {if $sess_price_ratio eq $db_curr_mst[currrb].vName} selected {/if}>{$db_curr_mst[currrb].vName}</option>
        {/section}
        <!--<option value="USD" {if $sess_price_ratio eq 'USD'} selected {/if}>USD</option>
        <option value="GBP" {if $sess_price_ratio eq 'GBP'} selected {/if}>GBP</option>
        <option value="NOK" {if $sess_price_ratio eq 'NOK'} selected {/if}>NOK</option>
        <option value="SEK" {if $sess_price_ratio eq 'SEK'} selected {/if}>SEK</option>
        <option value="DKK" {if $sess_price_ratio eq 'DKK'} selected {/if}>DKK</option>
        <option value="PLN" {if $sess_price_ratio eq 'PLN'} selected {/if}>PLN</option>
        <option value="RUB" {if $sess_price_ratio eq 'RUB'} selected {/if}>RUB</option> 
        <option value="EUR" {if $sess_price_ratio eq 'EUR'} selected {/if}>EUR</option>-->
      </select>
      <select name="sess_language" id="sess_language" onChange="change_lang(this.value);" class="lag">
        {section name="lngrb" loop=$db_lng_mst}
        <option value="{$db_lng_mst[lngrb].vCode}" {if $sess_lang eq $db_lng_mst[lngrb].vCode} selected {/if}>{$db_lng_mst[lngrb].vTitle}</option> 
        {/section}
        <!--<option value="EN" {if $sess_lang eq 'EN'} selected {/if}>{$smarty.const.LBL_ENGLISH}</option>
        <option value="DN" {if $sess_lang eq 'DN'} selected {/if}>{$smarty.const.LBL_DANISH}</option> 
        <option value="FI" {if $sess_lang eq 'FI'} selected {/if}>{$smarty.const.LBL_FINISH}</option>
        <option value="FN" {if $sess_lang eq 'FN'} selected {/if}>{$smarty.const.LBL_FRENCH}</option>    
        <option value="LV" {if $sess_lang eq 'LV'} selected {/if}>{$smarty.const.LBL_LATVIN}</option>
        <option value="EE" {if $sess_lang eq 'EE'} selected {/if}>{$smarty.const.LBL_ESTONIAN}</option>
        <option value="LT" {if $sess_lang eq 'LT'} selected {/if}>{$smarty.const.LBL_LITHUNIAN}</option>
        <option value="DE" {if $sess_lang eq 'DE'} selected {/if}>{$smarty.const.LBL_GERMAN}</option>
        <option value="NO" {if $sess_lang eq 'NO'} selected {/if}>{$smarty.const.LBL_NORWAY}</option>
        <option value="PO" {if $sess_lang eq 'PO'} selected {/if}>{$smarty.const.LBL_POLISH}</option> 
        <option value="RS" {if $sess_lang eq 'RS'} selected {/if}>{$smarty.const.LBL_RUSSIAN}</option>
        <option value="ES" {if $sess_lang eq 'ES'} selected {/if}>{$smarty.const.LBL_SPANISH}</option>
        <option value="SW" {if $sess_lang eq 'SW'} selected {/if}>{$smarty.const.LBL_SWEDISH}</option>-->  
      </select>
      <!--<div class="in-ride-new"><span>{if $smarty.cookies.ride_locations_type eq 'international'}<a href="javascript:void(0);" onClick="chnage_ride_types('local');">{$smarty.const.LBL_VIEW_LOCAL_RIDES}</a> {else}<a href="javascript:void(0);" onClick="chnage_ride_types('international');" style="color:#0094C8;">{$smarty.const.LBL_VIEW_INTERNATIONAL_RIDES}</a>{/if}</span></div>-->
      <select name="" class="lag1" onchange="chnage_ride_types(this.value);">
      <option value="international" {if $smarty.cookies.ride_locations_type eq 'international'}selected{/if}>{$smarty.const.LBL_VIEW_INTERNATIONAL_RIDES}</option>
      <option value="local" {if $smarty.cookies.ride_locations_type eq 'local'}selected{/if} >{$smarty.const.LBL_VIEW_LOCAL_RIDES}</option>
      </select>
      </span></div>
  </div>
  
  <div class="site-banner-main">
  <!--{if $ismobile eq 'No'}
  <div class="sitebanner-left-img">
      {$db_left_banner[0].source}
  </div>
  {/if}-->
    <div class="main-in">
    <div class="main-in-new">
      {include file="homeheader.tpl"}
      <!--// body part //-->
      <div id="body-part-main">
        <div class="home-body-part-top">
           <!--//-->
          {if $ridesarroundyou|@count gt 0}
          <div class="rides-aroud-you">
            <div class="rides-aroud-you-box">
              <h2>heyyy{$smarty.const.LBL_RIDE}<br />
                {$smarty.const.LBL_AROUND}<br />
                <strong>{$smarty.const.LBL_YOU}</strong></h2>
              <span><a href="{$tconfig.tsite_url}index.php?file=c-ride_list&search=arround_you">{$smarty.const.LBL_VIEW_MORE}</a></span></div>
            {section name=i loop=$ridesarroundyou max=2}
            <a href="{$tconfig.tsite_url}index.php?file=c-ride_details&from=home&id={$ridesarroundyou[i].iRideId}&dt={$ridesarroundyou[i].strtotime}&ret={$ridesarroundyou[i].return}">
            <div {if $smarty.section.i.first}class="trip-main"{else}class="trip-main-last"{/if}>
              <div class="trip roundcol simptip-position-top simptip-multiline simptip-info simptip-smooth" data-tooltip="{$ridesarroundyou[i].vMainDeparture} - {$ridesarroundyou[i].vMainArrival}">
                <img src="{$ridesarroundyou[i].memberimage}" alt="" class="mem-img" />
                <div class="trip-deails">
                  <h2 style="text-transform:none;">
                   {$generalobj->DateTime($ridesarroundyou[i].dDate,19)} - {$generalobj->DateTime($ridesarroundyou[i].time,12)}
                   <!--<img src="images/star.png" alt="" />-->
                   <p class="star-ride"> <span style="display: block; width: 65px; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 0;"> <span style="position:relative; display: block; width: {$ridesarroundyou[i].rating}%; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 -13px;"></span> </span> </p>
                   </h2>
                  <div class="trip-deails-left">
                    <p>{$ridesarroundyou[i].vMainDeparture|truncate:15} - {$ridesarroundyou[i].vMainArrival|truncate:15}</p>
                    <span>{$smarty.const.LBL_CAR}: {$ridesarroundyou[i].membercar|truncate:15}</span>
                    <div class="ho-icon">{$ridesarroundyou[i].pref}</div>
                  </div>
                  <div class="trip-deails-right">{$ridesarroundyou[i].totprice}</div>
                </div>
              </div>
              <div style=" clear:both;"></div>
            </div></a>
            {/section}
          </div>
          {/if}
          <!--//-->
          <!--//-->
          <!--Airport Rides ----->
          {if $AIRPORTS_RIDES_SHOW eq 'Yes'}
          <div class="airport-rides">
            <div class="airport-rides-box">
              <h2>{$smarty.const.LBL_AIRPORT}<strong>{$smarty.const.LBL_RIDE}</strong></h2>
              <span><a href="{$tconfig.tsite_url}index.php?file=c-ride_list&search=simple&type=airport">{$smarty.const.LBL_VIEW_MORE}</a></span></div>
           
            {section name=i loop=$airportmainarr max=2}
            <a href="{$tconfig.tsite_url}index.php?file=c-ride_details&from=home&id={$airportmainarr[i].iRideId}&dt={$airportmainarr[i].strtotime}&ret={$airportmainarr[i].return}">
            <div {if $smarty.section.i.first}class="trip-main"{else}class="trip-main-last"{/if}>
              <div class="trip airportcol simptip-position-top simptip-multiline simptip-info simptip-smooth" data-tooltip="{$airportmainarr[i].vMainDeparture} - {$airportmainarr[i].vMainArrival}">
                <img src="{$airportmainarr[i].memberimage}" alt="" class="mem-img" />
                <div class="trip-deails">
                  <h2 style="text-transform:none;">{$generalobj->DateTime($airportmainarr[i].dDateOut,19)} - {$generalobj->DateTime($airportmainarr[i].deptime,12)}
                     <!--<img src="images/star.png" alt="" />-->
                     <p class="star-ride"><span style="display: block; width: 65px; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 0;"> <span style=" position:relative; display: block; width: {$airportmainarr[i].rating}%; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 -13px;"></span> </span></p>
                  </h2>
                  <div class="trip-deails-left">
                    <p>{$airportmainarr[i].vMainDeparture|truncate:15} - {$airportmainarr[i].vMainArrival|truncate:15}</p>
                    <span>{$smarty.const.LBL_CAR}: {$airportmainarr[i].membercar|truncate:15}</span>
                    <div class="ho-icon">{$airportmainarr[i].pref}</div>
                  </div>
                  <div class="trip-deails-right">{$airportmainarr[i].totprice}</div>
                </div>
              </div>
              <div style=" clear:both;"></div>
            </div>
            </a>
            {/section}
          </div>
          {/if}
          <!--Airport Rides ends----->
          <!--//-->
          <!--//-->
          <!--Ladies Only Rides ----->
          {if $LADIES_ONLY_SHOW eq 'Yes'}
          <div class="ladies-only">
            <div class="ladies-only-box">
              <h2>{$smarty.const.LBL_LADIES}<strong>{$smarty.const.LBL_ONLY}</strong></h2>
              <span><a href="{$tconfig.tsite_url}index.php?file=c-ride_list&search=simple&type=ladiesonly">{$smarty.const.LBL_VIEW_MORE}</a></span></div>
            {section name=i loop=$ladiesmainarr max=2}
            <a href="{$tconfig.tsite_url}index.php?file=c-ride_details&from=home&id={$ladiesmainarr[i].iRideId}&dt={$ladiesmainarr[i].strtotime}&ret={$ladiesmainarr[i].return}">
            <div {if $smarty.section.i.first}class="trip-main"{else}class="trip-main-last"{/if}>
              <div class="trip ladiescol simptip-position-top simptip-multiline simptip-info simptip-smooth" data-tooltip="{$ladiesmainarr[i].vMainDeparture} - {$ladiesmainarr[i].vMainArrival}" alt="" />
                <img src="{$ladiesmainarr[i].memberimage}" alt="" class="mem-img" />
                <div class="trip-deails">
                  <h2 style="text-transform:none;">{$generalobj->DateTime($ladiesmainarr[i].dDateOut,19)} - {$generalobj->DateTime($ladiesmainarr[i].deptime,12)}
                     <!--<img src="images/star.png" alt="" />-->
                     <p class="star-ride"><span style="display: block; width: 65px; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 0;"> <span style=" position:relative; display: block; width: {$ladiesmainarr[i].rating}%; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 -13px;"></span> </span></p>
                  </h2>
                  <div class="trip-deails-left">
                    <p>{$ladiesmainarr[i].vMainDeparture|truncate:15} - {$ladiesmainarr[i].vMainArrival|truncate:15}</p>
                    <span>{$smarty.const.LBL_CAR}: {$ladiesmainarr[i].membercar|truncate:15}</span>
                    <div class="ho-icon">{$ladiesmainarr[i].pref}</div>
                  </div>
                  <div class="trip-deails-right">{$ladiesmainarr[i].totprice}</div>
                </div>
              </div>
              <div style=" clear:both;"></div>
            </div>
            </a>
            {/section}
          </div>
          {/if}
          <!--Ladies Only Rides Ends----->
          <!--//-->
          <!--//-->
          <!--Shopping Rides----->
          {if $SHOPPING_RIDES_SHOW eq 'Yes'}
          <div class="shopping-rides">
            <div class="shopping-rides-box">
              <h2>{$smarty.const.LBL_SHOPPING}<strong>{$smarty.const.LBL_RIDE}</strong></h2>
              <span>{if $shoppingmainarr|@count gt 0} <a href="{$tconfig.tsite_url}index.php?file=c-ride_list&search=simple&type=shopping">{$smarty.const.LBL_VIEW_MORE}</a>{/if}</span></div>
            {section name=i loop=$shoppingmainarr max=2} 
           <a href="{$tconfig.tsite_url}index.php?file=c-ride_details&from=home&id={$shoppingmainarr[i].iRideId}&dt={$shoppingmainarr[i].strtotime}&ret={$shoppingmainarr[i].return}">
            <div {if $smarty.section.i.first}class="trip-main"{else}class="trip-main-last"{/if}>
              <div class="trip shoppingcol simptip-position-top simptip-multiline simptip-info simptip-smooth" data-tooltip="{$shoppingmainarr[i].vMainDeparture} - {$shoppingmainarr[i].vMainArrival}"> 
                <img src="{$shoppingmainarr[i].memberimage}" alt="" class="mem-img" />
                <div class="trip-deails">
                  <h2 style="text-transform:none;">{$generalobj->DateTime($shoppingmainarr[i].dDateOut,19)} - {$generalobj->DateTime($shoppingmainarr[i].deptime,12)} 
                   <!--<img src="{$tconfig.tsite_images}home/star.png" alt="" />-->
                   <p class="star-ride"><span style="display: block; width: 65px; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 0;"> <span style=" position:relative; display: block; width: {$shoppingmainarr[i].rating}%; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 -13px;"></span> </span> </p>
                   </h2>
                  <div class="trip-deails-left">
                    <p>{$shoppingmainarr[i].vMainDeparture|truncate:15} - {$shoppingmainarr[i].vMainArrival|truncate:15}</p>
                    <span>{$smarty.const.LBL_CAR}: {$shoppingmainarr[i].membercar|truncate:15}</span>
                    <div class="ho-icon">{$shoppingmainarr[i].pref}</div>
                  </div>
                  <div class="trip-deails-right">{$shoppingmainarr[i].totprice}</div>
                </div>
              </div>
              <div style=" clear:both;"></div>
            </div></a>
            {/section}
          </div>
          {/if}
          <!--Shopping Rides Ends----->
          <!--//-->
            
                <div style="clear:both;"></div>    
        </div>
        <div style="clear:both;"></div>
      </div>
         <div style="clear:both;"></div>
      </div>
      <!--{if $ismobile eq 'No'} 
       <div class="sitebanner-right-img">
         {$db_right_banner[0].source}
       </div>
      {/if}--> 
    </div>
      <!--// body part end //-->
      <div style="clear:both;"></div>
    </div>
      <div style="clear:both;"></div>
    </div>
   
    <!--// home bottom part //-->
     <div id="home-bootm-part">
      <div class="home-bootm-part-inner">
        <div class="home-bootm-part-inner-1">
          <div class="latest-blog">
            <h2>{$smarty.const.LBL_LATEST} <span>{$smarty.const.LBL_BLOGS}</span><a href="http://blog.blablacarscript.com/" target="_blank">{$smarty.const.LBL_VIEW_MORE}</a></h2>
            <ul>
              {section name=rec loop=$db_post}
              <li>
        			<strong><a href="{$db_post[rec].guid}" target="_blank">{$db_post[rec].post_title}</a></strong>
        			<p>{$db_post[rec].post_content|strip_tags|truncate:150}</p><div class="date">{$generalobj->DateTime($db_post[rec].post_date,9)}</div>
        		  </li>
        		  {/section}
            </ul>
          </div>
          {if $ismobile eq 'No'}
          <div class="watch-how-it-works">
            <h2>{$box1title} <a href="{$tconfig.tsite_url}aboutus">{$smarty.const.LBL_VIEW_MORE}</a></h2>
            <p>
            <!--<img src="images/video.jpg" alt="" />-->
            <!--<iframe width="100%" height="100%" src="//www.youtube.com/embed/Ry8Uw1bkCuo" frameborder="0" allowfullscreen></iframe>-->
            {$box1video}
            </p>
          </div>
          {/if}
          <div class="map-ho"><img src="{$tconfig.tsite_images}{$THEME}/map.png" alt="" /></div>
        </div>
      </div>
      <div style="clear:both;"></div>
    </div>
    <!--// home bottom part end //-->
    <!--// footer part //-->
    {include file="footer.tpl"} 
    <!--// footer part end //-->
    <div style="clear:both;"></div>
  </div>
</div>
<div id="map-canvas" class="gmap3"></div>
{literal}
<script type="text/javascript">
function initialize() {
    //var input = document.getElementById("From);     
    //var options = {componentRestrictions: {country: 'us'}};
                 
    var from = new google.maps.places.Autocomplete(document.getElementById('From'));
    var to = new google.maps.places.Autocomplete(document.getElementById('To'));     
}  

$(document).ready(function(){
  google.maps.event.addDomListener(window, 'load', initialize);
});  

function search_now(){    
  jQuery("#home_search").validationEngine('init',{scroll: false});
	jQuery("#home_search").validationEngine('attach',{scroll: false});
  resp = jQuery("#home_search").validationEngine('validate');    
	if(resp == true){
    if(document.getElementById('From').value !='' && document.getElementById('To').value != ''){
      var From = $("#From").val();  
      $("#map-canvas").gmap3({
              getlatlng:{
                address:  From,
                callback: function(results){
                 $("#From_lat_long").val(results[0].geometry.location);
                }
              }
      });
      
      var To = $("#To").val();
      $("#map-canvas").gmap3({
              getlatlng:{
                address:  To,
                callback: function(results){
                 $("#To_lat_long").val(results[0].geometry.location);
                 document.home_search.submit();
                }
              }
      }); 
      return false;
    }else{
      alert('{/literal}{$smarty.const.LBL_SELECT_FROM_TO_ADD}{literal}');
      return false;
    }
  }else{
    return false;
  }
}
/*function go_get_latlong(from){
   if(from == 'From'){
     var From = $("#From").val();
     $("#map-canvas").gmap3({
            getlatlng:{
              address:  From,
              callback: function(results){
               $("#From_lat_long").val(results[0].geometry.location);
              }
            }
      });      
   }else{
     var To = $("#To").val();
     $("#map-canvas").gmap3({
            getlatlng:{
              address:  To,
              callback: function(results){
               $("#To_lat_long").val(results[0].geometry.location);
              }
            }
      });       
   }
} */
</script>
<script type="text/javascript">        
    var	site_url = '{/literal}{$tconfig.tsite_url}{literal}';
    $(function() {
        var from = document.getElementById('From');        
        var autocomplete_from = new google.maps.places.Autocomplete(from);
    		google.maps.event.addListener(autocomplete_from, 'place_changed', function() {
    			var place = autocomplete_from.getPlace();     		
    			go_get_latlong('From');
    		});
    		
    		var to = document.getElementById('To');
    		var autocomplete_to = new google.maps.places.Autocomplete(to);
    		google.maps.event.addListener(autocomplete_to, 'place_changed', function() {
    			var place = autocomplete_to.getPlace();     			
    			go_get_latlong('to');
    		});
    });
    checkCookie(); 
  </script>
{/literal}











