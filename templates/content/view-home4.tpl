<link href="{$tconfig.tsite_stylesheets}{$THEME}/home/application.css" media="all" rel="stylesheet" type="text/css">
<link href="{$tconfig.tsite_stylesheets}{$THEME}/home/homemedia.css" media="all" rel="stylesheet" type="text/css">
<link href="{$tconfig.tsite_stylesheets}{$THEME}/{$sess_lang}/homemedia.css" rel="stylesheet" type="text/css" />
<script src="{$tconfig.tsite_javascript}conversion_async.js" async="" type="text/javascript"></script>
<!--<script src="{$tconfig.tsite_javascript}application-ba4cfef7752778f6f6e3850a819dedc0.js" type="text/javascript"></script>-->
<script src="{$tconfig.tsite_javascript}backstretch-29d4fdd11161d58cef6a48166cde0ac8.js" type="text/javascript"></script>
<!--<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.js"></script>-->
<link rel="stylesheet" type="text/css" href="{$tconfig.tsite_stylesheets}front/simptip-mini.css" media="screen,projection" />
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script type="text/javascript" src="{$tconfig.tsite_javascript}gmap3.js"></script>
<script src="{$tconfig.tsite_javascript}modernizr.custom.67841.js" type="text/javascript"></script>
{literal}
<script type="text/javascript">
    var	site_url = '{/literal}{$tconfig.tsite_url}{literal}';
</script>
{/literal}
{literal}
<script type="text/javascript">
    $(function () {
		$("#finder").backstretch(
        /*["{/literal}{$tconfig.tsite_images}{$THEME}{literal}/banner-img1.jpg","{/literal}{$tconfig.tsite_images}{$THEME}{literal}/banner-img2.jpg","{/literal}{$tconfig.tsite_images}{$THEME}{literal}/banner-img3.jpg","{/literal}{$tconfig.tsite_images}{$THEME}{literal}/banner-img4.jpg"]*/
        {/literal}{$bannerArr}{literal},
        { duration: '{/literal}{$ROTATE_TIME*1000}{literal}', fade: 750 }
		);
		// $("#p2p-intro").backstretch("https://dgaqgnnkkz5ef.cloudfront.net/assets/bg_header_parking_blur-cb907df25db9819655bbdd57ac2e6126.jpg");
	});


</script>
<!-- -->

<!-- -->
{/literal}
<script type="text/javascript" src="{$tconfig.tsite_url}rrb/zebra_datepicker.src.js"></script>
<link rel="stylesheet" href="{$tconfig.tsite_url}rrb/default.css" type="text/css">
<div id="main-content">
	<!-- top part -->
	{include file="homeheader4.tpl"}
	<!-- top part end -->
	<!-- header part -->
	<div id="header-part">
		<!-- <img src="{$tconfig.tsite_images}home/{$THEME}/header-img.jpg" alt="" height="666" /> -->
		<div id="sldier" class="index">
			{if $videocount eq 1}
			<div style="position: relative; z-index: 10; background: none repeat scroll 0% 0% transparent;">
				{$videocode}
				<div class="container carpool">
					<div class="row">
						<div class="span12">
							<div class="intro hidden-phone">
								<h5>{$smarty.const.LBL_LOOKING_FOR_RIDE}</h5>
								<!--<p class="lead">{$smarty.const.LBL_MORE_FUN}</p>-->
							</div>
							<!-- <form name="home_search" id="home_search" method="post" action="{$tconfig.tsite_url}index.php?file=c-ride_list"> -->
							<div style="margin:0;padding:0;display:inline"> </div>
							<input name="search" id="search" type="hidden" value="place">
							<input name="From_lat_long" id="From_lat_long" type="hidden" value="">
							<input name="To_lat_long" id="To_lat_long" type="hidden" value="">
							<!--<span class="Search-ride-from">{$smarty.const.LBL_FIND_RIDE}</span>-->
							<div class="header-up-bg">
								<span class="js-autocompleter">
									<input name="From" id="From" type="text" placeholder="{$smarty.const.LBL_FROM_PLACE}" class="validate[required] span3 from autocompleted" value="" tabindex="1"/>
								</span>
								<!--<span class="reverse"> <span class="splash-version">to</span> </span>-->
								<span class="js-autocompleter">
									<input placeholder="{$smarty.const.LBL_TO_PLACE}" name="To" id="To" tabindex="2" type="text" class="validate[required] span3 to autocompleted" value=""/>
								</span>
								<input name="searchdate" id="searchdate" type="text" tabindex="3" class="datepicker span2 hasDatepicker" value="" placeholder="{$smarty.const.LBL_DATE}" />
								<div class="btn-group" id="top-search-button"> <a href="javascript:void(0);" onClick="search_now();" class="btn btn-warning submitter" tabindex="4">{$smarty.const.LBL_SEARCH}</a> </div>
							</div>
							<!-- </form> -->
						</div>
					</div>
				</div>
			</div>
			{else if $videocount eq 0}
			<div style="position: relative; z-index: 10; background: none repeat scroll 0% 0% transparent;" id="finder" class="collapse">
				<div class="container carpool">
					<div class="row">
						<div class="span12">
							<div class="intro hidden-phone">
								<h5>{$smarty.const.LBL_LOOKING_FOR_RIDE}</h5>
								<!--<p class="lead">{$smarty.const.LBL_MORE_FUN}</p>-->
							</div>
							<form name="home_search" id="home_search" method="post" action="{$tconfig.tsite_url}index.php?file=c-ride_list">
								<div style="margin:0;padding:0;display:inline"> </div>
								<input name="search" id="search" type="hidden" value="place">
								<input name="From_lat_long" id="From_lat_long" type="hidden" value="">
								<input name="To_lat_long" id="To_lat_long" type="hidden" value="">
								<!--<span class="Search-ride-from">{$smarty.const.LBL_FIND_RIDE}</span>-->
								<div class="header-up-bg">
									<span class="js-autocompleter">
										<input name="From" id="From" type="text" placeholder="{$smarty.const.LBL_FROM_PLACE}" class="validate[required] span3 from autocompleted" value="" tabindex="1"/>
									</span>
									<span id="swapper" class="swap-img">
										<a href="javascript:void(0)" onclick="swapaddress();"><img src="{$tconfig.tsite_images}swap.png" style="width:30px;height:30px;"/></a>
									</span>
									<!--<span class="reverse"> <span class="splash-version">to</span> </span>-->
									<span class="js-autocompleter">
										<input placeholder="{$smarty.const.LBL_TO_PLACE}" name="To" id="To" tabindex="2" type="text" class="validate[required] span3 to autocompleted" value=""/>
									</span>
									<select name="category" id="category" tabindex="3" class="search-car-find-rad" placeholder="{$smarty.const.LBL_CATEGORY}">
										<option value="all">{$smarty.const.LBL_CATEGORY}</option>
										<option value="document">{$smarty.const.LBL_DOCUMENT}</option>
										<option value="box">{$smarty.const.LBL_BOX}</option>
										<option value="luggage">{$smarty.const.LBL_LUGGAGE}</option>
									</select>
									<input name="searchdate" id="searchdate" type="text" tabindex="4" class="datepicker span2 hasDatepicker" value="" placeholder="{$smarty.const.LBL_DATE}" />
									<div class="btn-group" id="top-search-button">
										<a href="javascript:void(0);" onClick="search_now();" class="btn btn-warning submitter" tabindex="4">{$smarty.const.LBL_SEARCH}</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		{/if}
	</div>
</div>
<!-- header part ends-->
<!-- body part -->
<div id="body-content">
    <div class="how-does-work">
		<h2>{$smarty.const.LBL_HOW_DOES_IT_WORKS}</h2>
		<ul>
			<li>
				<div class="how-does-work-img"><img src="{$find_a_match[0].img_url}" alt="" width="250" height="276"/></div>
				<h2>{$find_a_match[0].vPageTitle|truncate:40}</h2>
				<p>{$find_a_match[0].tPageDesc|truncate:250}</p>
			</li>
			<li>
				<div class="how-does-work-img"><img src="{$start_your_trip[0].img_url}" alt="" width="250" height="276"/></div>
				<h2>{$start_your_trip[0].vPageTitle|truncate:40}</h2>
				<p>{$start_your_trip[0].tPageDesc|truncate:250}</p>
			</li>
			<li>
				<div class="how-does-work-img"><img src="{$all_done[0].img_url}" alt="" width="250" height="276"/></div>
				<h2>{$all_done[0].vPageTitle}</h2>
				<p>{$all_done[0].tPageDesc|truncate:250}</p>
			</li>
		</ul>
		<div style="clear:both;"></div>
	</div>
    <!-- -->
    <div id="rides-content">
		<div class="rides-content2">&nbsp;</div>
		<!-- -->
		<div class="rides-content-inner">
			<div class="upcoming-rides-area" style="Width:100%">
				<h2>{$smarty.const.LBL_UPCOMING_RIDES}
					<a href="{$tconfig.tsite_url}index.php?file=c-ride_list&search=simple&type=latest">{$smarty.const.LBL_VIEW_MORE}</a>
				</h2>
				{if $latestmainarr|@count gt 0}
				<ul>
					{section name=i loop=$latestmainarr max=6}
					{if $latestmainarr[i].iRideId neq ''}
					<li><a href="{$tconfig.tsite_url}index.php?file=c-ride_details&from=home&id={$latestmainarr[i].iRideId}&dt={$latestmainarr[i].strtotime}&ret={$latestmainarr[i].return}">
						<div class="trip roundcol simptip-position-top simptip-multiline simptip-info simptip-smooth" data-tooltip="{$latestmainarr[i].vMainDeparture} - {$latestmainarr[i].vMainArrival}">
							<div class="rides-img"><span><img src="{$latestmainarr[i].memberimage}" class="mem-img"/></span></div>
							<div class="rides-text">
								<p>{$generalobj->DateTime($latestmainarr[i].dDateOut,10)}<!-- - {$generalobj->DateTime($latestmainarr[i].deptime,12)}--></p>
								<h3>{$latestmainarr[i].vMainDeparture|truncate:20}&nbsp;&nbsp;&nbsp;&#8594;&nbsp;&nbsp;{$latestmainarr[i].vMainArrival|truncate:20}</h3>
									<!-- <br />{$smarty.const.LBL_CAR}: {$latestmainarr[i].membercar}</h3>
									<span>{$latestmainarr[i].pref}</span> -->
									<!--hide luggage load and price
									<p>Small load: {$latestmainarr[i].vDocRemain} - {$latestmainarr[i].fDocumentPrice}</p>
									<p>Medium load: {$latestmainarr[i].vBoxRemain} - {$latestmainarr[i].fBoxPrice}</p>
									<p>Large load: {$latestmainarr[i].vLuggageRemain} - {$latestmainarr[i].fLuggagePrice}</p>-->
							</div>
								<div class="rides-price">
									<p> <span style="display: block; width: 65px; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 0;"> <span style="  position:relative; top:-10px; margin-right: 10px;display: block; width: {$latestmainarr[i].rating}%; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 -13px;"></span> </span> </p>
									<!--  <h2>{$latestmainarr[i].totprice}</h2> -->
								</div>
							</div>
						</a>
					</li>
					{/if}
					{/section}
					</ul>
					{/if}
				</div>
				{if $ladiesmainarr|count gt 0}
				<div class="ladies-rides">
					<h2>{$smarty.const.LBL_LADIESONLY_RIDES}
						<!-- <a href="{$tconfig.tsite_url}index.php?file=c-ride_list&search=simple&type=ladiesonly">{$smarty.const.LBL_VIEW_MORE}</a> -->
					</h2>
					<ul>
						{section name=i loop=$ladiesmainarr max=1}
						<!--  <a href="{$tconfig.tsite_url}index.php?file=c-ride_details&from=home&id={$ladiesmainarr[i].iRideId}&dt={$ladiesmainarr[i].strtotime}&ret={$ladiesmainarr[i].return}"> -->
						<div class="trip ladiescol {if $smarty.section.i.last} last {/if} simptip-position-top simptip-multiline simptip-info simptip-smooth" data-tooltip="{$ladiesmainarr[i].vMainDeparture} - {$ladiesmainarr[i].vMainArrival}">
							<li>
								<div class="rides-img"><span><img src="{$ladiesmainarr[i].memberimage}" alt=""/> </span></div>
								<div class="rides-text">
									<p>{$generalobj->DateTime($ladiesmainarr[i].dDateOut,10)} - {$generalobj->DateTime($ladiesmainarr[i].deptime,12)}</p>
									<h3>{$ladiesmainarr[i].vMainDeparture|truncate:15} - {$ladiesmainarr[i].vMainArrival|truncate:15}<br />
									{$smarty.const.LBL_CAR}: {$ladiesmainarr[i].membercar|truncate:15}</h3>
								<!-- <span>{$ladiesmainarr[i].pref}</span> --> </div>
								<div class="rides-price">
									<p><span style="display: block; width: 65px; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 0;"> <span style=" position:relative; top:-10px; margin-right: 10px;display: block; width: {$ladiesmainarr[i].rating}%; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 -13px;"></span> </span></p>
									<h2>{$ladiesmainarr[i].totprice}</h2>
								</div>
							</li>
						</div>
						<!-- </a> -->
						{/section}
					</ul>
				</div>
				{/if}
				<div style="clear:both;"></div>
			</div>
			<!-- -->

		</div>
		<!-- -->
		{if $featuredin|@count gt 0}
		<div id="featured-in">
			<div class="featured-in-inner">
				<h2>{$smarty.const.LBL_FEATURED} <span>{$smarty.const.LBL_IN}</span></h2>
				<ul>
					{section name="featuredin" loop=$featuredin}
					{if $featuredin[featuredin].img_url neq ''}
					<li>{if $featuredin[featuredin].vURL neq ''}<a href="{$featuredin[featuredin].vURL}" target="_blank">{/if}<img src="{$featuredin[featuredin].img_url}" alt="" />{if $featuredin[featuredin].vURL neq ''}</a>{/if}</li>
					{/if}
					{/section}
					<!--<li><a href="#"><img src="{$tconfig.tsite_images}home/{$THEME}/bottom-logo1.jpg" alt="" /></a></li>
						<li><a href="#"><img src="{$tconfig.tsite_images}home/{$THEME}/bottom-logo2.jpg" alt="" /></a></li>
						<li><a href="#"><img src="{$tconfig.tsite_images}home/{$THEME}/bottom-logo3.jpg" alt="" /></a></li>
						<li><a href="#"><img src="{$tconfig.tsite_images}home/{$THEME}/bottom-logo4.jpg" alt="" /></a></li>
						<li><a href="#"><img src="{$tconfig.tsite_images}home/{$THEME}/bottom-logo5.jpg" alt="" /></a></li>
						<li><a href="#"><img src="{$tconfig.tsite_images}home/{$THEME}/bottom-logo6.jpg" alt="" /></a></li>
					<li><a href="#"><img src="{$tconfig.tsite_images}home/{$THEME}/bottom-logo7.jpg" alt="" /></a></li>-->
				</ul>
			</div>
		</div>
		{/if}
	</div>
	<!-- footer-part -->
	<div id="footer">
	<div class="footer-inner">
		<div class="footer-top-part">
			<div class="footer-top-logo-part">
				<div class="footer-logo"><a href="{$tconfig.tsite_url}"><img src="{$tconfig['tsite_upload_footer_logo']}1_{$FOOTER_LOGO}"></a></div>
				<span>
					<a href="{$FACEBOOK_LINK}" target="_blank">
						<img onmouseout="this.src='{$tconfig.tsite_images}home/{$THEME}/fb.png'" onmouseover="this.src='{$tconfig.tsite_images}home/{$THEME}/fb-hover.png'" onclick="return submitsearch(document.frmsearch);" src="{$tconfig.tsite_images}home/{$THEME}/fb.png" alt=""/>
					</a>
					<a href="{$TWITTER_LINK}" target="_blank">
						<img onmouseout="this.src='{$tconfig.tsite_images}home/{$THEME}/twi.png'" onmouseover="this.src='{$tconfig.tsite_images}home/{$THEME}/twi-hover.png'" onclick="return submitsearch(document.frmsearch);" src="{$tconfig.tsite_images}home/{$THEME}/twi.png" alt=""/>
					</a>
					<a href="{$PINTEREST_LINK}" target="_blank">
						<img onmouseout="this.src='{$tconfig.tsite_images}home/{$THEME}/p-icon.png'" onmouseover="this.src='{$tconfig.tsite_images}home/{$THEME}/p-icon-hover.png'" onclick="return submitsearch(document.frmsearch);" src="{$tconfig.tsite_images}home/{$THEME}/p-icon.png" alt=""/>
					</a>

				</span>
			</div>
			<div class="footer-mid-part">
				<div class="footer-box1">
					<h2>{$smarty.const.LBL_USING} {$smarty.const.LBL_CAR_SHARING_WEBSITE}</h2>
					<ul>
						{section name=i loop=$db_pages_1}
						<li><a href="{$tconfig.tsite_url}page/{$db_pages_1[$smarty.section.i.index].iPageId}/{$generalobj->getPageUrlName({$db_pages_1[$smarty.section.i.index].iPageId})}" >{$generalobj->getPageTitle({$db_pages_1[$smarty.section.i.index].iPageId})}</a></li>
						{/section}
						<!--hide old faq<li><a href="{$tconfig.tsite_url}faqs" {if $script eq 'faqs'}class="active"{/if}>{$smarty.const.LBL_FAQ}</a></li>-->
						<li><a href="{$tconfig.tsite_url}Member-Stories" {if $script eq 'member_stories'}class="active"{/if}>{$smarty.const.LBL_MEMBER_STORY}</a></li>
					</ul>
				</div>
				<div class="footer-box2">
					<h2>{$smarty.const.LBL_OUR_COMPANY}</h2>
					<li><a href="/page/5/how-it-works">How It Works</a></li>
					<li><a href="/page/7/trust-safety">Trust & Safety</a></li>
					<li><a href="{$tconfig.tsite_url}contactus" {if $script eq 'contactus'}class="active"{/if}>{$smarty.const.LBL_CONTACT_US1}</a></li>
					<li><a href="{$tconfig.tsite_url}blog" target="_blank">{$smarty.const.LBL_BLOGS}</a></li>
				</div>

				<!--old footer box2
				<div class="footer-box2">
					<h2>{$smarty.const.LBL_OUR_COMPANY}</h2>
					{section name=i loop=$db_pages_2}
					<li><a href="{$tconfig.tsite_url}page/{$db_pages_2[$smarty.section.i.index].iPageId}/{$generalobj->getPageUrlName({$db_pages_2[$smarty.section.i.index].iPageId})}" >{$generalobj->getPageTitle({$db_pages_2[$smarty.section.i.index].iPageId})}</a></li>
					{/section}
					<li><a href="{$tconfig.tsite_url}contactus" {if $script eq 'contactus'}class="active"{/if}>{$smarty.const.LBL_CONTACT_US1}</a></li>
				</div>
				-->

				<div class="footer-box3">
					<form name="frmnewsletter" id="frmnewsletter" method="post" action="">
						<div class="footer-search-box">
							<h2>{$smarty.const.LBL_SUBSRIBE} {$smarty.const.LBL_NEWSLETTERS}</h2>
							<span>
								<input name="vNewsletterEmail" id="vNewsletterEmail" type="text" class="validate[required,custom[email]] footer-search" placeholder="Enter E-mail Address" />
								<input value="{$smarty.const.LBL_SUBMIT}" id="newletter_subscribe" name="" type="button" class="search-button" onClick="checknewsletter();" style="display:;"/>
								<input value="{$smarty.const.LBL_WAIT}"   id="newletter_loading" name="" type="button" class="search-button" onClick="checknewsletter();" style="display:none;"/>
							</span> </div>
							<p>{$smarty.const.LBL_FOOTER_TERMS} </p>
					</form>
				</div>
			</div>
		</div>
		<div class="footer-bottom-part">
			<p>
				{$COPY_RIGHT_TEXT}
				 <br />
					<!--{$smarty.const.LBL_SITE_DESIGN_DEVELOP} : <a href="http://blablacarscript.com/" target="_blank" class="site-name">blablacarscript.com</a>-->

			</p>
		</div>
	</div>
</div>
	<!-- -->
</div>
<div id="map-canvas" class="gmap3"></div>
{literal}
<script type="text/javascript">
	function initialize() {
		//var input = document.getElementById("From);
		//var options = {componentRestrictions: {country: 'us'}};

		var from = new google.maps.places.Autocomplete(document.getElementById('From'));
		var to = new google.maps.places.Autocomplete(document.getElementById('To'));
	}

	$(document).ready(function(){
		google.maps.event.addDomListener(window, 'load', initialize);
	});

	function search_now(){
		$("#home_search").validationEngine('init',{scroll: false});
		$("#home_search").validationEngine('attach',{scroll: false});
		resp = $("#home_search").validationEngine('validate');
		if(resp == true){
			if(document.getElementById('From').value !='' && document.getElementById('To').value != ''){
				var From = $("#From").val();
				$("#map-canvas").gmap3({
					getlatlng:{
						address:  From,
						callback: function(results){
							$("#From_lat_long").val(results[0].geometry.location);
						}
					}
				});

				var To = $("#To").val();
				$("#map-canvas").gmap3({
					getlatlng:{
						address:  To,
						callback: function(results){
							$("#To_lat_long").val(results[0].geometry.location);
							document.home_search.submit();
						}
					}
				});
				return false;
				}else{
				alert('{/literal}{$smarty.const.LBL_SELECT_FROM_TO_ADD}{literal}');
				return false;
			}
			}else{
			return false;
		}
	}
	/*function go_get_latlong(from){
		if(from == 'From'){
		var From = $("#From").val();
		$("#map-canvas").gmap3({
		getlatlng:{
		address:  From,
		callback: function(results){
		$("#From_lat_long").val(results[0].geometry.location);
		}
		}
		});
		}else{
		var To = $("#To").val();
		$("#map-canvas").gmap3({
		getlatlng:{
		address:  To,
		callback: function(results){
		$("#To_lat_long").val(results[0].geometry.location);
		}
		}
		});
		}
	} */
	function swapaddress(){
		$('#From').val([$('#To').val(), $('#To').val($('#From').val())][0]);
	}
</script>
<script type="text/javascript">
    $(function() {
        var from = document.getElementById('From');
        var autocomplete_from = new google.maps.places.Autocomplete(from);
		google.maps.event.addListener(autocomplete_from, 'place_changed', function() {
			var place = autocomplete_from.getPlace();
			go_get_latlong('From');
		});

		var to = document.getElementById('To');
		var autocomplete_to = new google.maps.places.Autocomplete(to);
		google.maps.event.addListener(autocomplete_to, 'place_changed', function() {
			var place = autocomplete_to.getPlace();
			go_get_latlong('to');
		});
	});
    checkCookie();
</script>
{/literal}
