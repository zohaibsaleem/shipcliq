{if $var_msg_forget neq ''}
{if $msg_code eq '1'} 
{literal}
 <script>
  showNotification({type : 'error', message: '{/literal}{$var_msg_forget}{literal}'});
 </script> 
 {/literal}
{/if}     	
{if $msg_code eq '0'}
{literal}
 <script>
  showNotification({type : 'success', message: '{/literal}{$var_msg_forget}{literal}'});
 </script> 
 {/literal}    
{/if} 
{/if} 
<div class="body-inner-part">
  <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_LOGIN}</span></div>
  <div class="main-inner-page">
    <h2>{$smarty.const.LBL_LOGIN}</h2>
    <div class="right-inner-part">
      <div class="login-page">
        <div class="form-login">
          <h4>{$smarty.const.LBL_LOGIN}</h4>
          <form name="login_box" id="login_box" method="post">
          <input type="hidden" name="lgfor" id="lgfor" value="">
          <input type="hidden" name="from" id="from" value="">
          <input type="hidden" name="action" id="action" value="loginchk">
          <div class="inner-form1 login-page-form">
          <span>
            <label class="email-label">{$smarty.const.LBL_EMAIL} : <strong>*</strong></label>
            <div class="singlerow-login">
              <input type="text" name="vEmail" id="vEmail" class="validate[required,custom[email]] form-input-login" value="{$vEmail}">
            </div>
            </span>
            <span>
            <label class="email-label">{$smarty.const.LBL_PASSWORD} : <strong>*</strong></label>
            <div class="singlerow-login">
              <input type="password"  name="vPassword" id="vPassword" class="validate[required,minSize[6]] form-input-login" value="{$vPassword}">
            </div>
            </span>
            <span>
            <label class="email-label blanck-c">&nbsp;</label>
            <div class="singlerow-login-log"><a onClick="check_login();" href="javascript:void(0);" {if $sess_lang eq 'RS'}style="padding:7px 0px;font-size:14px;"{/if}>{$smarty.const.LBL_SUBMIT}</a> <a onclick="document.login_box.reset();return false;" href="javascript:void(0);" {if $sess_lang eq 'RS'}style="padding:7px 0px;font-size:14px;"{/if}>{$smarty.const.LBL_RESET}</a></div>
            </span>
            <div style="clear:both;"></div>
          </div>
          </form>
          <div class="bot-line-login">{$smarty.const.LBL_OR}</div>
          <table width="100%" cellspacing="0" cellpadding="7" border="0">
            <tbody>
              <tr>
                <td align="center">
                  <button class="btn btn-primary" onclick="fbconnect();return false;">
                  <a style="color:#fff;" onclick="fbconnect();"><img src="{$tconfig.tsite_images}fb-but-icon.png" alt="" />{$smarty.const.LBL_CONNECT_FACEBOOK}</a>
                  </button>
                </td>
              </tr>
			  <!--
              <tr>
                <td align="center">
					{$draugiem_login_button}
                </td>
              </tr>
			  -->
            </tbody>
          </table>
        </div>
        <div class="login-fr">
          <h2 class="inner-hd">{$smarty.const.LBL_NEW_CUST}?</h2>
          <p>{$smarty.const.LBL_CUST_DESCRIPTION}</p>
          <div class="singlerow-login"><a href="{$tconfig.tsite_url}sign-up">{$smarty.const.LBL_REGISTER_NOW}</a></div>
        </div> 
        <form name="forgetpassword" id="forgetpassword" action="{$tconfig.tsite_url}index.php?file=c-login" method="post">
        <input type="Hidden" name="action" value="forgetpsw">
        <div class="fordot">
          <h2 class="inner-hd">{$smarty.const.LBL_FORGOT_PASS}?</h2>
          <p>{$smarty.const.LBL_FORGOT_DESCRIPTON}</p>
          <span><em>{$smarty.const.LBL_EMAIL} :</em>
          <input type="text" id="vPassEmail" name="vPassEmail" class="validate[required,custom[email]] form-input-login-for">
          <div class="singlerow-forgot"><a onclick="javascript:checkforgotpassword(); return false;" href="javascript:void(0);">{$smarty.const.LBL_SEND}</a></div>
          </span>
          <div style="clear:both;"></div>
        </div>
        </form>
        <div style="clear:both;"></div>
      </div>
    </div>
	{include file="left.tpl"}
  </div><div style="clear:both;"></div>
</div>
{literal}
<script>
  $('#login_box').bind('keydown',function(e){ 
    if(e.which == 13){
      check_login(); return false;
    }
  });
  
   //login with fb
  function fbconnect()
  {
	 javscript:window.location='{/literal}{$tconfig.tsite_url}fbconnect.php{literal}';
  }
  
  //login with dr
  /*function drconnect()
  {
	 javscript:window.location='{/literal}{$tconfig.tsite_url}drconnect.php{literal}';
  } */
  
  function check_login(){
	var redirect_url = '{/literal}{$redirect_link}{literal}';
    jQuery("#login_box").validationEngine('attach',{scroll: false});  
    resp = jQuery("#login_box").validationEngine('validate');    
	  if(resp == true){
      //$("#loginbtn").hide(); 
      //$("#loginloader").show();    
      var request = $.ajax({  
    	  type: "POST",
    	  url: site_url+'index.php?file=c-login',  
    	  data: $("#login_box").serialize(), 	  
    	  
    	  success: function(data) {  //alert(data); return false; 
          $("#loginloader").hide();
          $("#loginbtn").show();                	     	
          if(data == 0){             
            showNotification({type : 'error', message: '{/literal}{$smarty.const.LBL_LOGIN_FAILED_USER_PASS}.{literal}'});
          }
          else if(data == 1){              
            showNotification({type : 'error', message: '{/literal}{$smarty.const.LBL_ACC_NOT_ACTIVE}.{literal}'});
          }
          else{
				if(redirect_url != '')
				{
					window.location = redirect_url; 
				}
				else
				{
					window.location = site_url+'my-account'; 
				}                          
			}  			
    	}
  	 });
  	
     request.fail(function(jqXHR, textStatus) {
      alert( "Request failed: " + textStatus ); 
     });
      
    }else{
      return false;
    }  
  }
  
  function checkforgotpassword(){    
    resp = jQuery("#forgetpassword").validationEngine('validate');
		if(resp == true){
			document.forgetpassword.submit();
		}else{
			return false;
		}		
	}
	
 
</script>
{/literal}
