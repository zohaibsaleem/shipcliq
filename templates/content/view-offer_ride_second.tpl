<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&language=en"></script>
<script type="text/javascript" src="{$tconfig.tsite_javascript}gmap3.js"></script>
{if $sess_browser eq 'Internet Explorer'}
{literal}
<script>
	alert('{/literal}{$smarty.const.LBL_RIDE_POST_NOT_SUPPORT}{literal}');
	window.location = "{/literal}{$tconfig.tsite_url}{literal}";
</script>
{/literal}
{/if}
{literal}
<style>
	#map-canvas {
    height: 383px;
    margin-top: 0px;
    padding: 0px;
    width: 370px;
	}
</style>
{/literal}
{literal}
<script>
</script>
{/literal}
<div class="body-inner-part">
	<div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_OFFER_RIDE}</span></div>
	<div class="main-inner-page">
		<h2>{$smarty.const.LBL_OFFER}<span>{$smarty.const.LBL_A_RIDE}</span></h2>
		<form name="frmstep2" id="frmstep2" method="post" action="{$tcondig.tsite_url}index.php?file=m-offer_rede_action">
			<input type="hidden" name="vCountryCodeTo" id="vCountryCodeTo" value="{$destination_country}">
			<div class="offer-seats">
				<div class="tabing"> <a  href="{$tconfig.tsite_url}offer-ride">{$smarty.const.LBL_MY_ITINERARY}</a><a href="javascript:void(0);" class="active">{$smarty.const.LBL_PRICE}</a> </div>
				<div class="offer-seats-left">
					<div id="pricing" class="offer-a-ride">
						<h2>{$smarty.const.LBL_OFFER} {$smarty.const.LBL_A_RIDE}</h2>
						{section name="points" loop=$main_points_arr}
						<span class="spa-off">
							<input type="hidden" name="points_{$smarty.section.points.index}" id="points_{$smarty.section.points.index}" value="{$main_points_arr[points].addr_post}">
							<input type="hidden" name="start_lat_{$smarty.section.points.index}" id="start_lat_{$smarty.section.points.index}" value="{$main_points_arr[points].start_lat}">
							<input type="hidden" name="end_lat_{$smarty.section.points.index}" id="end_lat_{$smarty.section.points.index}" value="{$main_points_arr[points].end_lat}">
							<input type="hidden" name="distance_{$smarty.section.points.index}" id="distance_{$smarty.section.points.index}" value="">
							<input type="hidden" name="duration_{$smarty.section.points.index}" id="duration_{$smarty.section.points.index}" value="">
							<!-- Hemali..
							{literal}<script> give_price_details('{/literal}{$main_points_arr[points].addr}{literal}', {/literal}{$smarty.section.points.index}{literal});</script>{/literal} -->
							<em>{$main_points_arr[points].addr}</em>
							<!-- Hemali.. <p class="ord2">
								<b>{$sess_price_ratio}</b>
								<input name="price_{$smarty.section.points.index}" id="price_{$smarty.section.points.index}" type="text" class="pound-input" value="" onKeyUp="change_price_amount({$smarty.section.points.index});" style="color:#000;"/>
								<input name="price_type_{$smarty.section.points.index}" id="price_type_{$smarty.section.points.index}" type="hidden" value="Average"/>
								<input name="price_orig_{$smarty.section.points.index}" id="price_orig_{$smarty.section.points.index}" type="hidden" value=""/>
							</p> -->
						</span>
						<!-- Hemali.. <div class="plus-minus-container">
							<span><a href="javascript:void(0);" onClick="increase_price({$smarty.section.points.index});">+</a></span>
							<span><a href="javascript:void(0);" onClick="descrese_price({$smarty.section.points.index});">-</a></span>
						</div> -->
						{/section}
						<!-- Hemali..  <div {if $no_other_points eq 'Yes'}  style="display:none;"  {/if}>
							<span class="spa-off" style="color:#0094C8;"><em class="ord1"> {$from} &rarr; {$to}</em>
							<p class="ord2">
							<b>{$sess_price_ratio}</b>
							<input name="main_price" id="main_price" type="text" class="pound-input" value="{$Price_per_passenger}" readonly style="color:#000;"/>
							<input name="main_price_type_{$smarty.section.points.index}" id="main_price_type_{$smarty.section.points.index}" type="hidden" value="Average"/>
							<input name="main_price_orig_{$smarty.section.points.index}" id="main_price_orig_{$smarty.section.points.index}" type="hidden" value=""/>
							</p>
							</span>
						</div> -->
					</div>
					<!--<div class="offer-a-ride2"> <span> <em>Number of seats offered:</em>
						<p>
						<input name="seats" id="seats" type="text" class="pound-input2" value="3" />
						<img src="{$tconfig.tsite_images}plush-min.jpg" alt="" /></p>
						</span>
						</div>
						<div id="pricing" class="offer-a-ride">
						<h2>{$smarty.const.LBL_CAR_DETAILS}</h2>
						<span class="spa-off"> <em class="ord1" style="width: 62%;">{$smarty.const.LBL_SELECT_CAR}:</em>
						<p class="ord2">
						{if $db_car|@count gt 0}
						<select name="iMemberCarId" id="iMemberCarId" class="pound-input" style="border-radius: 3px;width:200px;text-align: left;">
						{section name="car" loop=$db_car}
						<option value="{$db_car[car].iMemberCarId}">{$db_car[car].vMake} {$db_car[car].vTitle}</option>
						{/section}
						</select>
						{else}
						<a href="{$tconfig.tsite_url}car-add-form" class="cont-bot-but1">{$smarty.const.LBL_ADD_YOUR_CAR_NOW}</a>
						<input type="hidden" name="iMemberCarId" id="iMemberCarId" value="">
						{/if}

						</p>
						</span>
						<span class="spa-off"> <em class="ord1">{$smarty.const.LBL_NUMBER_SEATS_OFFERED}:</em>
						<p class="ord2">
						<input name="seats" id="seats" type="text" class="pound-input" value="3" style="border-radius: 3px;width:73px;" readonly/>
						</p>
						</span>
						<div class="plus-minus-container">
						<span><a href="javascript:void(0);" onClick="increase_seats();">+</a></span>
						<span><a href="javascript:void(0);" onClick="descrese_seats();">-</a></span>
						</div>
						</div>
					-->
					<!-- AS per cargosharing1 clients requirement Start - Hemali..-->
					<div class="offer-a-ride3">
						<h2>{$smarty.const.LBL_FREIGHT_TYPE}</h2>
						<span>
							<table border="0" cellpadding="10" width="100%" style="text-align:left">
								<tr>
									<td style="text-align:left"></td>
									<td style="text-align:left" id="price_per_unit">{$smarty.const.LBL_PRICE}/Unit</td>
									<td style="text-align:left">{$smarty.const.LBL_WEIGHT}</td>
									<td style="text-align:left">{$smarty.const.LBL_UNIT}</td>
								</tr>
								<tr>
									<th style="text-align:left">{$smarty.const.LBL_DOCUMENT}</th>
									<td><input type="text" class="form-box" name="fDocumentPrice" id="fDocumentPrice" value="" onKeyUp="numericFilter(this)" style="width:50px;" placeholder="{$sess_price_ratio}"></td>
									<td><input type="text" class="form-box" name="vDocumentWeight" id="vDocumentWeight" value="" placeholder="{$smarty.const.LBL_DOCUMENT_EXAMPLE}"></td>
									<td><select name="vDocumentUnit" id="vDocumentUnit" class="form-box">
									{section name="unit" loop=$DocUnit}
										<option value="{$DocUnit[unit]}">{$DocUnit[unit]}</option>
									{/section}
									</select></td>
								</tr>
								<tr>
									<th>{$smarty.const.LBL_BOX}</th>
									<td><input type="text" class="form-box" name="fBoxPrice" id="fBoxPrice" value="" onKeyUp="numericFilter(this)" style="width:50px;" placeholder="{$sess_price_ratio}"></td>
									<td><input type="text" class="form-box" name="vBoxWeight" id="vBoxWeight" value="" placeholder="{$smarty.const.LBL_BOX_EXAMPLE}"></td>
									<td><select name="vBoxUnit" id="vBoxUnit" class="form-box">
									{section name="unit" loop=$BoxUnit}
										<option value="{$BoxUnit[unit]}">{$BoxUnit[unit]}</option>
									{/section}
									</select></td>
								</tr>
								<tr>
									<th>{$smarty.const.LBL_LUGGAGE}</th>
									<td><input type="text" class="form-box" name="fLuggagePrice" id="fLuggagePrice" value="" onKeyUp="numericFilter(this)" style="width:50px;" placeholder="{$sess_price_ratio}"></td>
									<td><input type="text" class="form-box" name="vLuggageWeight" id="vLuggageWeight" value="" placeholder="{$smarty.const.LBL_LUGGAGE_EXAMPLE}"></td>
									<td><select name="vLuggageUnit" id="vLuggageUnit" class="form-box">
									{section name="unit" loop=$LuggageUnit}
										<option value="{$LuggageUnit[unit]}">{$LuggageUnit[unit]}</option>
									{/section}
									</select></td>
								</tr>
							</table>
						</span>
					</div>

					<!--

						<div class="offer-a-ride3">
						<h2>{$smarty.const.LBL_DOCUMENT}</h2>
						<span>
						<p class="offer-ride1">
						<em>{$smarty.const.LBL_DOCUMENT} {$smarty.const.LBL_PRICE}: </em>
						<input type="text" class="form-box" name="fDocumentPrice" id="fDocumentPrice" value="" onKeyUp="numericFilter(this)">{$sess_price_ratio}
						</p>
						<p class="offer-ride1"><em>{$smarty.const.LBL_DOCUMENT} {$smarty.const.LBL_WEIGHT}: </em><input type="text" class="form-box" name="vDocumentWeight" id="vDocumentWeight" value=""></p>
						</span>
						</div>

						<div class="offer-a-ride3">
						<h2>{$smarty.const.LBL_BOX}</h2>
						<span>
						<p class="offer-ride1">
						<em>{$smarty.const.LBL_BOX} {$smarty.const.LBL_PRICE}: </em>
						<input type="text" class="form-box" name="fBoxPrice" id="fBoxPrice" value="" onKeyUp="numericFilter(this)">{$sess_price_ratio}
						</p>
						<p class="offer-ride1"><em>{$smarty.const.LBL_BOX} {$smarty.const.LBL_WEIGHT}: </em><input type="text" class="form-box" name="vBoxWeight" id="vBoxWeight" value=""></p>
						</span>
						</div>

						<div class="offer-a-ride3">
						<h2>{$smarty.const.LBL_LUGGAGE}</h2>
						<span>
						<p class="offer-ride1">
						<em>{$smarty.const.LBL_LUGGAGE} {$smarty.const.LBL_PRICE}: </em>
						<input type="text" class="form-box" name="fLuggagePrice" id="fLuggagePrice" value="" onKeyUp="numericFilter(this)">{$sess_price_ratio}
						</p>
						<p class="offer-ride1"><em>{$smarty.const.LBL_LUGGAGE} {$smarty.const.LBL_WEIGHT}: </em><input type="text" class="form-box" name="vLuggageWeight" id="vLuggageWeight" value=""></p>
						</span>
						</div>
					<!-- AS per cargosharing1 clients requirement End-->

					<div class="offer-a-ride3">
						<h2>{$smarty.const.LBL_FURTHER_DETAILS}</h2>
						<span>
							<p class="offer-ride1"><em>{$smarty.const.LBL_I_WILL_LEAVE}:</em>
								<select name="leave_time" id="leave_time" class="ride-input">
									<option value="ON_TIME">{$smarty.const.LBL_RIGHT_ON_TIME}</option>
									<option value="FIFTEEN_MINUTES">{$smarty.const.LBL_15_MNT_WINDOW}</option>
									<option value="THIRTY_MINUTES">{$smarty.const.LBL_30_MNT_WINDOW}</option>
									<option value="ONE_HOUR">{$smarty.const.LBL_1_HOUR_WINDOW}</option>
									<option value="TWO_HOURS">{$smarty.const.LBL_2_HOUR_WINDOW}</option>
								</select>
							</p>
							<p>
								<textarea name="tDetails" id="tDetails" cols="" rows="" class="textarea-input"></textarea>
								<br />
							<img src="{$tconfig.tsite_images}information-inset.png" alt="" />&nbsp;{$smarty.const.LBL_OFFER_RIDE_NOTE}</p>
							<!-- Hemali.. <p>&nbsp;</p>
								<p class="offer-ride1"><em>{$smarty.const.LBL_MAX_LUGGAGE_SIZE}:</em>
								<select name="luggage_size" id="luggage_size" class="ride-input">
								<option value="Small">{$smarty.const.LBL_SMALL}</option>
								<option value="Medium">{$smarty.const.LBL_MEDIUM}</option>
								<option value="Big">{$smarty.const.LBL_BIG}</option>
								</select>
							</p> -->

							<!-- Hemali.. <p class="offer-ride1"><em>{$smarty.const.LBL_I_MAKE_DETOUR}:</em>
								<select name="wait_time" id="wait_time" class="ride-input">
								<option value="NONE">{$smarty.const.LBL_NONE}</option>
								<option value="FIFTEEN_MINUTES">{$smarty.const.LBL_15_MNT_DETOUR_MAX}</option>
								<option value="THIRTY_MINUTES">{$smarty.const.LBL_30_MNT_DETOUR_MAX}</option>
								<option value="WHATEVER_IT_TAKES">{$smarty.const.LBL_ANYTHING_IS_FINE}</option>
								</select>
								</p>

								{if $LADIES_ONLY_SHOW eq 'Yes'}
								{if $smarty.session.sess_eGender eq 'Female'}
								<p class="offer-ride1"><em>{$smarty.const.LBL_LADIES_ONLY}:</em>
								<select name="eLadiesOnly" id="eLadiesOnly" class="ride-input">
								<option value="No">No</option>
								<option value="Yes">Yes</option>
								</select>
								</p>
								{else}
								<input type="hidden" name="eLadiesOnly" id="eLadiesOnly" value="No">
								{/if}
								{/if}
								{if $AIRPORTS_RIDES_SHOW eq 'Yes' OR $SHOPPING_RIDES_SHOW eq 'Yes'}
								<p class="offer-ride1"><em>{$smarty.const.LBL_RIDE_PLACE}:</em>
								<select name="eRidePlaceType" id="eRidePlaceType" class="ride-input">
								<option value="">{$smarty.const.LBL_SELECT}</option>
								{if $AIRPORTS_RIDES_SHOW eq 'Yes'}
								<option value="Airport">{$smarty.const.LBL_AIRPORT}</option>
								{/if}
								{if $SHOPPING_RIDES_SHOW eq 'Yes'}
								<option value="Shopping">{$smarty.const.LBL_SHOPPING}</option>
								{/if}
								</select>
								</p>
								{/if}
							-->
						</span> </div>
						<div class="offer-a-ride4"> <span>
							<p>
								<input name="tos" id="tos" type="checkbox" value="Yes" />
							&nbsp;{$smarty.const.LBL_OFFER_RIDE_NOTE1}&nbsp;(<a href="{$tcofig.tsite_url}terms-conditions" target="_blank">{$smarty.const.LBL_SEE_OUR} ToS</a>) </p>
						</span> </div>
				<a class="cont-bot-but2" href="javascript:void(0);" onCLick=go_for_publish();>{$smarty.const.LBL_PUBLISH_OFFER}</a><a class="cont-bot-but1" href="{$tconfig.tsite_url}offer-ride">{$smarty.const.LBL_BACK}</a> </div>
				<div class="offer-seats-right"><div id="map-canvas" class="gmap3"></div></div>
			</div>
		</form>
	</div>
	<div style="clear:both;"></div>
</div>
{literal}
<script>
	var tot_points = '{/literal}{$main_points_arr|@count}{literal}';
	var min_price_any_ride = {/literal}{$min_price_any_ride}{literal};

	function clearThat(){
		var opts = {};
		opts.name = ["marker", "directionsrenderer"];
		opts.first = true;
		$('#map-canvas').gmap3({clear:opts});
	}

	function from_to(from, to, loc1, loc2, loc3, loc4, loc5, loc6){
		clearThat();
		var waypts = [];

		if(loc1 != ''){
			waypts.push({location:loc1, stopover:true});
		}
		if(loc2 != ''){
			waypts.push({location:loc2, stopover:true});
		}
		if(loc3 != ''){
			waypts.push({location:loc3, stopover:true});
		}
		if(loc4 != ''){
			waypts.push({location:loc4, stopover:true});
		}
		if(loc5 != ''){
			waypts.push({location:loc5, stopover:true});
		}
		if(loc6 != ''){
			waypts.push({location:loc6, stopover:true});
		}

		$("#map-canvas").gmap3({
			getroute:{
				options:{
					origin:from,
					destination:to,
					waypoints:waypts,
					travelMode: google.maps.DirectionsTravelMode.DRIVING
				},
				callback: function(results){
					if (!results) return;
					$(this).gmap3({
						map:{
							options:{
								zoom: 13,
								//     center: [-33.879, 151.235]
								center: [50.0000, 20.0000]
							}
						},
						directionsrenderer:{
							options:{
								directions:results
							}
						}
					});
				}
			}
		});
	}
	from_to('{/literal}{$from}{literal}', '{/literal}{$to}{literal}', '{/literal}{$loc1}{literal}', '{/literal}{$loc2}{literal}', '{/literal}{$loc3}{literal}', '{/literal}{$loc4}{literal}', '{/literal}{$loc5}{literal}', '{/literal}{$loc6}{literal}');

	function give_all_details(details, index){
		var modes = details.split(" = ");

		$("#map-canvas").gmap3({
			getdistance:{
				options:{
					origins:modes[0],
					destinations:modes[1],
					travelMode: google.maps.TravelMode.DRIVING
				},
				callback: function(results, status){
					var html = "";
					if (results){
						for (var i = 0; i < results.rows.length; i++){
							var elements = results.rows[i].elements;
							for(var j=0; j<elements.length; j++){
								switch(elements[j].status){
									case "OK":
									var request = $.ajax({
										type: "POST",
										url: site_url+'index.php?file=c-count_price',
										data: "distance="+elements[j].distance.text+"&duration="+elements[j].duration.text+"&from="+modes[0]+"&to="+modes[1]+"&pos="+pos+"&index="+index,

										success: function(data) {
											$("#pricing").append(data);
										}
									});

									request.fail(function(jqXHR, textStatus) {
										//alert( "Request failed: " + textStatus );
									});
									break;

								}
							}
						}
						} else {
						html = "error";
					}
					$("#results").html( html );
				}
			}
		});
	}

	function increase_seats(){
		var main_price = Number(document.getElementById('seats').value);
		document.getElementById('seats').value = main_price + 1;
	}

	function descrese_seats(){
		if(document.getElementById('seats').value != 1){
			var main_price = Number(document.getElementById('seats').value);
			document.getElementById('seats').value = main_price - 1;
		}
	}


	function go_for_publish(){
		/* validation for at least one category selected from (Document, Box, Weight) */

		if(document.getElementById('fDocumentPrice').value == '' && document.getElementById('vDocumentWeight').value == '' && document.getElementById('fBoxPrice').value == '' && document.getElementById('fLuggagePrice').value == '' && document.getElementById('fLuggagePrice').value == '' && document.getElementById('vLuggageWeight').value == ''){ // Does price insert?
			showNotification({type : 'error', message: '{/literal}{$smarty.const.LBL_ERR_MSG_ATLEAST_ONE}{literal}'});
			return false;
		}
		else
		{
			<!-- ### For Document ### -->
			if(document.getElementById('fDocumentPrice').value != '' && document.getElementById('vDocumentWeight').value == '') { // It should not be zero
				showNotification({type : 'error', message: "{/literal}{$smarty.const.LBL_ERR_MSG_DOCWEIGHT_NOT_EMPTY}{literal}"});
				return false;
			}
			if(document.getElementById('vDocumentWeight').value != '' && document.getElementById('fDocumentPrice').value == '' ) { // When Document price is written Weight Cannot be blank

				showNotification({type : 'error', message: '{/literal}{$smarty.const.LBL_ERR_MSG_DOCPRICE_NOT_ZERO}{literal}'});  // Document Price and Weight both are written
				return false;
			}

			<!-- ### For Box ### -->
			if(document.getElementById('fBoxPrice').value != '' && document.getElementById('vBoxWeight').value == '') { // It should not be zero
				showNotification({type : 'error', message: "{/literal}{$smarty.const.LBL_ERR_MSG_BOXWEIGHT_NOT_EMPTY}{literal}"});
				return false;
			}
			if(document.getElementById('vBoxWeight').value != '' && document.getElementById('fBoxPrice').value == '' ) { // When Document price is written Weight Cannot be blank

				showNotification({type : 'error', message: '{/literal}{$smarty.const.LBL_ERR_MSG_BOXPRICE_NOT_ZERO}{literal}'});  // Document Price and Weight both are written
				return false;
			}

			<!-- ### For Luggage ### -->
			if(document.getElementById('fLuggagePrice').value != '' && document.getElementById('vLuggageWeight').value == '') { // It should not be zero
				showNotification({type : 'error', message: "{/literal}{$smarty.const.LBL_ERR_MSG_LUGWEIGHT_NOT_EMPTY}{literal}"});
				return false;
			}
			if(document.getElementById('vLuggageWeight').value != '' && document.getElementById('fLuggagePrice').value == '' ) { // When Document price is written Weight Cannot be blank

				showNotification({type : 'error', message: '{/literal}{$smarty.const.LBL_ERR_MSG_LUGPRICE_NOT_ZERO}{literal}'});  // Document Price and Weight both are written
				return false;
			}
			var valid = 0;
			if(document.getElementById('vDocumentWeight').value != '' && document.getElementById('fDocumentPrice').value != '') {
				valid =1;
			}
			if(document.getElementById('vBoxWeight').value != '' && document.getElementById('fBoxPrice').value != '') {
				valid =1;
			}
			if(document.getElementById('vLuggageWeight').value != '' && document.getElementById('fLuggagePrice').value != '') {
				valid =1;
			}
			if(valid == 0){
				showNotification({type : 'error', message: '{/literal}{$smarty.const.LBL_ERR_MSG_ATLEAST_ONE}{literal}'});
				return false;
			}
		}


		/* Hemali.. if(document.getElementById('iMemberCarId').value == ''){
			showNotification({type : 'error', message: '{/literal}{$smarty.const.LBL_PLEASE_SELECT_CAR}{literal}'});
			return false;
		} */
		if($("#tos").is(':checked')){
			}else{
			showNotification({type : 'error', message: '{/literal}{$smarty.const.LBL_SELECT_TERMS_CONDITIONS}{literal}'});
			return false;
		}

		for (var i=0;i<tot_points;i++)
		{
			if($("#price_"+i).val() < min_price_any_ride){
				//showNotification({type : 'error', message: "You can't enter any ride point price smallar than "+min_price_any_ride});
				//return false;
				}else if($("#price_"+i).val() == 0 || $("#price_"+i).val() == ''){
				showNotification({type : 'error', message: "You can't enter zero or blank price for any ride point"});
				return false;
			}
		}
		/*Hemali..
			if($("#main_price").val() < min_price_any_ride){
			showNotification({type : 'error', message: "Minimum price for any trip is "+min_price_any_ride+". Please check price for your trip."});
			return false;
		}*/

		document.frmstep2.submit();
		return false;

	}

	function increase_price(id){
		var old_price = $("#price_"+id).val();
		var new_price = Number(old_price) + Number(1);
		$("#price_"+id).val(new_price);

		/*var distance = $("#distance_"+id).val().split(" ");
			var miles = Math.round(distance[0] * 0.6214);
			var price_per_100_miles = {/literal}{$Hundradrmilespice}{literal};
			var Price_per_passenger = Math.round((price_per_100_miles * miles)/100);

			if(Price_per_passenger < 3){
			Price_per_passenger = 3;
			}

			if(Price_per_passenger < new_price){
			$("#price_"+id).css( "color", "#FF8000" );
			$("#price_type_"+id).val("High");
			}else{
			$("#price_"+id).css( "color", "#000" );
			$("#price_type_"+id).val("Average");
			}
		cacl_all();*/
		change_price_amount(id);
	}

	function descrese_price(id){
		var old_price = $("#price_"+id).val();
		if(old_price > 1){
			var new_price = Number(old_price) - Number(1);
			$("#price_"+id).val(new_price);

			/*var distance = $("#distance_"+id).val().split(" ");
				var miles = Math.round(distance[0] * 0.6214);
				var price_per_100_miles = {/literal}{$Hundradrmilespice}{literal};
				var Price_per_passenger = Math.round((price_per_100_miles * miles)/100);

				if(Price_per_passenger < 3){
				Price_per_passenger = 3;
				}

				if(Price_per_passenger == new_price){
				$("#price_"+id).css( "color", "#000" );
				$("#price_type_"+id).val("Average");
				}else if(Price_per_passenger > new_price){
				$("#price_"+id).css( "color", "#149414" );
				$("#price_type_"+id).val("Low");
				}
			cacl_all();*/
			change_price_amount(id);
			}else{
			$("#price_"+id).val(1);
			//cacl_all();
			change_price_amount(id);
		}
	}

	function cacl_all(){
		var price = 0;
		for (var i=0;i<tot_points;i++)
		{
			price = Number(price) + Number($("#price_"+i).val());
		}
		$("#main_price").val(price);
	}

	function change_price_amount(id){
		var price = $("#price_"+id).val();
		var origional_price = $("#price_orig_"+id).val();

		$("#price_"+id).val(price.replace(/[^/(0-9)/]/ig, ""));

		var main_price = $("#main_price").val();
		var origional_main_price = $("#main_price_orig").val();

		var price_diff = price - origional_price;
		price_diff = Math.abs(price_diff);

		if(Number(price_diff) > Number(2) && Number(price) > Number(origional_price)){
			$("#price_"+id).css( "color", "#FF8000" );
			$("#price_type_"+id).val("High");
			cacl_all();
			return false;
			}else if(Number(price_diff) > Number(2) && Number(price) < Number(origional_price)){
			$("#price_"+id).css( "color", "#149414" );
			$("#price_type_"+id).val("Low");
			cacl_all();
			return false;
			}else{
			$("#price_"+id).css( "color", "#000" );
			$("#price_type_"+id).val("Average");
			cacl_all();
			return false;
		}

		/*if(price > origional_price){
			$("#price_"+id).css( "color", "#FF8000" );
			$("#price_type_"+id).val("High");
			cacl_all();
			}else if(price < origional_price){
			$("#price_"+id).css( "color", "#149414" );
			$("#price_type_"+id).val("Low");
			cacl_all();
			}else{
			$("#price_"+id).css( "color", "#000" );
			$("#price_type_"+id).val("Average");
			cacl_all();
		}*/
	}

	function give_price_details(addr, id){
		var modes = addr.split(" &rarr; ");
        $("#map-canvas").gmap3({
			getdistance:{
				options:{
					origins:modes[0],
					destinations:modes[1],
					travelMode: google.maps.TravelMode.DRIVING
				},
				callback: function(results, status){
					if (results){
						for (var i = 0; i < results.rows.length; i++){
							var elements = results.rows[i].elements;
							for(var j=0; j<elements.length; j++){

								switch(elements[j].status){
									case "OK":
									//data: "distance="+elements[j].distance.text+"&duration="+elements[j].duration.text+"&from="+modes[0]+"&to="+modes[1]+"&pos="+pos+"&index="+index,

									$("#distance_"+id).val(elements[j].distance.text);
									$("#duration_"+id).val(elements[j].duration.text);

									//alert('Distance >> '+elements[j].distance.text);
									//alert('Duration >> '+elements[j].duration.text);

									var dist = elements[j].distance.text.split(" ");
									dist[0] = dist[0].replace(/,/g,'');
									var miles = Math.round(dist[0] * 0.6214);
									//var price_per_100_miles = {/literal}{$Hundradrmilespice}{literal};

									var Price_per_passenger = Math.round((price_per_100_miles * miles)/100);

									//if(Price_per_passenger < min_price_any_ride){
									//Price_per_passenger = min_price_any_ride;
									//}

									$("#price_"+id).val(Price_per_passenger);
									$("#price_orig_"+id).val(Price_per_passenger);

									var trrip_main_price = $("#main_price").val();
									$("#main_price").val(Number(Price_per_passenger) + Number(trrip_main_price));
									$("#main_price_orig_"+id).val(Number(Price_per_passenger) + Number(trrip_main_price));
									break;
								}
							}
						}
						} else {
						html = "error";
					}
				}
			}
		});
	}
	function numericFilter(txb) {
		//txb.value = txb.value.replace(/[^\0-9]/ig, "");
		txb.value = txb.value.replace(/[^0-9.]/g,"");
	}
</script>
{/literal}

{section name="points" loop=$main_points_arr}
{literal}<script> give_price_details('{/literal}{$main_points_arr[points].addr}{literal}', {/literal}{$smarty.section.points.index}{literal});</script>{/literal}
	{/section}
