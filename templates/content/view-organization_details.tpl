<script type="text/javascript" src="{$tconfig.tsite_javascript}jquery.raty.js"></script>
<div class="about-page">
  <div class="bradecrame"><span><a href="{$tconfig.tsite_url}">Home</a>&raquo;&nbsp;&nbsp;<a href="{$tconfig.tsite_url}organizations">Organization Listing</a>&raquo;&nbsp;&nbsp;{$db_organization[0].vOrganizationName}</span></div>
  <h2>organization details</h2>
  <div class="member-top-part">
    <div class="member-left"><img src="{$db_organization[0].small_url}" alt="" /></div>
    <div class="member-right">
      <h2>{$db_organization[0].vOrganizationName}</h2>
      <div id="star" class="star-img"></div>
      {literal}
        <script>
          $.fn.raty.defaults.path = '{/literal}{$tconfig.tsite_images}{literal}img';
          $('#star').raty({ readOnly: true, score: {/literal}{$db_organization[0].iRate}{literal} });
        </script>
      {/literal}
      <div class="row1">
        {if $db_organization[0].vAddress neq ''}
        <label><strong> Address :</strong>{$db_organization[0].vAddress} </label>  {/if} 
        <label><strong>Contact :</strong> {$db_organization[0].vFirstName} {$db_organization[0].vLastName}</label>  
        {if $db_organization[0].vPhone neq ''}
        <label><strong>Phone :</strong> {$db_organization[0].vPhone} </label>{/if}
        {if $db_organization[0].vFax neq ''}
        <label><strong>Fax :</strong> {$db_organization[0].vFax}</label> {/if}
        {if $db_organization[0].vURL neq ''}
        <label><strong>Website :</strong> <a href="{$db_organization[0].vURL}" target="_blank">{$db_organization[0].vURL} {/if}</a></label>
        {if $db_organization_cat_str neq ''}
        <label><strong>Interest Area :</strong> {$db_organization_cat_str} {/if}</label>
      </div>
      <div class="clear"></div>
    </div>
  </div>
  {if $db_organization[0].tDescription neq ''}
  <div class="row2">
    <h3>About this Organization</h3>
    <p>{$db_organization[0].tDescription|nl2br} </p>
  </div>
  {/if}
  <div class="tabs">
    <div class="profile-tab">
      <ul>
        <li><a href="{$tconfig.tsite_url}organization-details/{$iOrganizationId}" class="active" id="Upcoming"> Upcoming Events </a></li>
        <li><a href="javascript:void(0);" onClick="dispaly_tab('past_events');" class="" id="Events">Past Events</a></li>
        <li> <a href="javascript:void(0);" onClick="dispaly_tab('reviews');" class="" id="Reviews">Reviews ({$reviews_total})</a></li>
      </ul>
      <div id="main_loader" style="display:none;margin-left: 305px;margin-top: 169px;position: absolute;"><img src="{$tconfig.tsite_images}252 (1).gif"></div>
      <div class="clear"></div>
    </div>
    <div id="main1" class="tabs-produ"> {if $db_events|@count gt 0}
      {section name="i" loop=$db_events}
      {if $db_events[i].eEventType eq 'Flexible' or $db_events[i].start_time gte $current}
      <div class="organization-event-listing-details" {if $smarty.section.i.iteration eq $smarty.section.i.last}style="border-bottom:none;"{/if}>
        <div class="organization-event-img"><a href="{$tconfig.tsite_url}event-details/{$db_events[i].iEventId}"><img src="{$db_events[i].thumb_url}" alt="" /></a></div>
          <div class="organization-event-listing-right">
            <h2><a href="{$tconfig.tsite_url}event-details/{$db_events[i].iEventId}">{$db_events[i].vTitle}</a></h2>
            <h3>{$db_organization[0].vOrganizationName}</h3>
            <p>{$db_events[i].tDetails|truncate:250}</p>
            <span><img src="{$tconfig.tsite_images}con-icon.jpg" alt="" /> {$db_events[i].vCity}, {$db_events[i].vState}<a href="{$tconfig.tsite_url}event-details/{$db_events[i].iEventId}">View More</a></span> <br>
        </div>
        </div>
      {/if}
      {/section}
        
        {else}
        <span style="color:#000000;font-size:18px;margin:10px;">
        <center>           There are no Upcoming Events.
        </center>
        </span> 
         {/if}
        </div>
     
      
      
      <div style="clear:both;"></div>
    </div>
    <div style="clear:both;"></div>
  </div>
  <div style="clear:both;"></div>
</div> </div>
<div style='display:none'>  
<div id="rate_reviews"> 
<form name="frmattending" id="frmattending" method="post">       
<input type="hidden" name="iOrganizationId" id="iOrganizationId" value="">  
<input type="hidden" name="action" id="action" value="givereviews">
<a href="javascript:void(0);" onclick="javascript:$.fn.colorbox.close();" style="left: 490px;position: relative;top: 17px;z-index: 999;"><img src="{$tconfig.tsite_images}fancyclose.png"></a>  
<div class="popup-from" style="width:460px;">     
  <label><strong>Rates :</strong>
    <select class="validate[required] form-input-login" name="iEventRate" id="iEventRate" style="width:100px;">
      <option value="">-- Select --</option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
    </select>
  </label>
  <label><strong>Your Review :</strong>
    <textarea class="validate[required] form-input-login" name="tReview" id="tReview" style="width:325px;height:100px;"></textarea>
  </label>
  <label>
  <div class="popup-login" style=" margin: 0 0 0 120px;">
  <a id="subbtnfrm" href="javascript:void(0);" onclick="check_reviews(); return false;" style="display:;">Submit</a>
  <a id="waitbtnfrm" href="javascript:void(0);" style="display:none;">Wait...</a>
  <a href="javascript:void(0);" onclick="javascript:$.fn.colorbox.close();">Cancel</a>
  </div>
  </label>
  <div style="clear:both;"></div>
</div>
</form>
</div>
</div>
{literal} 
<script> 
function dispaly_tab(type, msg)
{
  document.getElementById("main_loader").style.display = '';
  if(type == 'past_events'){
     var url = site_url+'index.php?file=c-organization_pastevents';
     //alert (url);
  }else{
     var url = site_url+'index.php?file=c-organization_reviews';
  }
  var request = $.ajax({  
    type: "POST",
    url: url,  
    data: "iOrganizationId={/literal}{$iOrganizationId}{literal}&msg="+msg, 	  
    
    success: function(data) { //alert(data); return false;          
      document.getElementById("main_loader").style.display = 'none';
      if(type == 'past_events'){
        document.getElementById("Events").className = 'active';
        document.getElementById("Reviews").className = '';
        document.getElementById("Upcoming").className = '';
      }else{
        document.getElementById("Events").className = '';
        document.getElementById("Reviews").className = 'active';
        document.getElementById("Upcoming").className = '';
      }
     $("#main1").html(data);  
                         			
    }
  });
  
  request.fail(function(jqXHR, textStatus) {
    alert( "Request failed: " + textStatus ); 
  });    
}
</script> 
{/literal}