<script type='text/javascript' src='{$tconfig.tsite_url}autocomplete/jquery.autocomplete.js'></script>  
<link rel="stylesheet" type="text/css" href="{$tconfig.tsite_url}autocomplete/jquery.autocomplete.css" />
<script type="text/javascript" src="{$tconfig.tsite_javascript}jquery.raty.js"></script>
{literal}
<script type="text/javascript">
$().ready(function() {$("#vCity").focus().autocomplete(city,{autoFill: false,scrollHeight: 220});});

</script>
{/literal}
<div class="about-page">
  <div class="keywords">
    <h2>Find an Organization</h2>
    <input name="vCity" id="vCity" type="text" class="keywords-input" value="{$srchkeyword1}" style="width:200px;" placeholder="Search By City"/>
    <input name="keyword" id="keyword" type="text" class="keywords-input" value="{$srchkeyword2}"  style="width:200px;" placeholder="Search By Keyword"/>
    <a href="javascript:void(0);" onClick="search_by_key();">Search</a> </div>
  <h2 style="float:left; width:100%;">Organization listing
    <div class="inner-bot-top">
      <p style="float:right; text-align:right; font-size:14px; text-transform:uppercase; padding-top:3px; color:rgb(57, 57, 57);">Sort By : &nbsp;
        <select name="ordbyselect" id="ordbyselect" onchange="sort_organizations(this.value);" style="width:172px; padding:3px; font-size:12px; color:#666666;">
          <option value="">-- Please select  --</option>
          <option value="nameasc" {if $ordby eq 'nameasc'} selected {/if}>By Name(A-Z)</option>
          <option value="namedesc" {if $ordby eq 'namedesc'} selected {/if}>By Name(Z-A)</option>
          <option value="rateasc" {if $ordby eq 'rateasc'} selected {/if}>By Ratings(0-5)</option>
          <option value="ratedesc" {if $ordby eq 'ratedesc'} selected {/if}>By Ratings(5-0)</option>
        </select>
      </p>
    </div>
  </h2>
  <div class="organization-listing">
    <div class="listing-page">
      <ul>
        {section name="orgs" loop=$organizations}
        <li {if $smarty.section.orgs.index%4 eq 3}class="last"{/if}>
          <div class="slid-in-top"><a href="{$tconfig.tsite_url}organization-details/{$organizations[orgs].iOrganizationId}"> <img src="{$organizations[orgs].img_url}" alt="" /> </a> </div>
          <strong><a href="{$tconfig.tsite_url}organization-details/{$organizations[orgs].iOrganizationId}">{$organizations[orgs].vOrganizationName|truncate:30}</a></strong>
          <p><em>{$organizations[orgs].vFirstName} {$organizations[orgs].vLastName}</em></p>
          <p id="star_{$organizations[orgs].iOrganizationId}"></p>
          <p style="min-height: 30px;">{$organizations[orgs].tDescription|truncate:60}</p>
          <p class="lon"><img src="{$tconfig.tsite_images}con-icon.jpg" alt="" />&nbsp;{$organizations[orgs].vCity}, {$organizations[orgs].vState},</p>
        </li>
         {literal}
          <script>
            $.fn.raty.defaults.path = '{/literal}{$tconfig.tsite_images}{literal}img';
            $('#star_{/literal}{$organizations[orgs].iOrganizationId}{literal}').raty({ readOnly: true, score: {/literal}{$organizations[orgs].iRate}{literal} });
          </script>
        {/literal}
        {/section}
      </ul>
    </div>
    <div class="paging"> {$recmsg} <span>{$page_link}</span> </div>
  </div>
  <div style="clear:both;"></div>
</div>
<form name="orgfrm" id="orgfrm" method="post" action="">                                                          
  <input type="hidden" name="srchkeyword1" id="srchkeyword1" value="{$srchkeyword1}">
  <input type="hidden" name="srchkeyword2" id="srchkeyword2" value="{$srchkeyword2}">
  <input type="hidden" name="ordby" id="ordby" value="{$ordby}">      
</form>
{literal}<script> var city = [{/literal}{$cities}{literal}];</script>{/literal}
{literal}
<script>
if (!supports_placeholder()) { 
  if($("#vCity").val() == ''){
    $("#vCity").focus(function(){
			if ($(this).val() === "Search By City") {
				$(this).val("");
			}
		}).blur(function(){
			if ($(this).val() === "") {
				$(this).val("Search By City");
			}
		}).val("Search By City");
  }
	
	if($("#keyword").val() == ''){
	 $("#keyword").focus(function(){
			if ($(this).val() === "Search By Keyword") {
				$(this).val("");
			}
		}).blur(function(){
			if ($(this).val() === "") {
				$(this).val("Search By Keyword");
			}
		}).val("Search By Keyword");
	}
}
</script>
{/literal}