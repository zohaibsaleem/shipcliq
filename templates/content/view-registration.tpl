 {if $var_msg neq ''}
 {literal}
 <script>
  showNotification({type : 'error', message: '{/literal}{$var_msg}{literal}'});
 </script>
 {/literal}
{/if}
<div class="body-inner-part">
  <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_REGISTRATION}</span></div>
  <div class="main-inner-page">
    <h2>{$smarty.const.LBL_REGISTRATION}</h2>

    <form class="drop form_new" name="frmregister" id="frmregister" action="{$tconfig.tsite_url}index.php?file=c-registration_a" method="post">
    <input type="Hidden" name="mode" value="register">
    <div class="right-inner-part">
      <div class="registration">
        <table width="100%" cellspacing="0" cellpadding="7" border="0">
          <tr>
            <td align="center"><button class="btn btn-primary" onclick="fbconnect();return false;">
              <a style="color:#fff;" href="javascript:void(0);" onclick="fbconnect();"><img src="{$tconfig.tsite_images}fb-but-icon.png" alt="" />{$smarty.const.LBL_CONNECT_FACEBOOK}</a>
              </button></td>
          </tr>
          <tr>
            <td class="registration-or" align="center">{$smarty.const.LBL_OR}</td>
          </tr>
          <tr>
            <td style="padding:0px;">&nbsp;</td>
          </tr>
        </table>
        <div class="reg-input1" id="registration">
          <label><em>{$smarty.const.LBL_NICK_NAME} : <strong>*</strong></em>
            <input type="text" id="vNickName" name="vNickName" class="validate[required] form-input-re" value="{$db_customer[0].vNickName}">
          </label>
          <label><em>{$smarty.const.LBL_FIRST_NAME} : <strong>*</strong></em>
            <input type="text" id="vFirstName" name="vFirstName" class="validate[required] form-input-re" value="{$db_customer[0].vFirstName}">
          </label>
          <label><em>{$smarty.const.LBL_LAST_NAME} : <strong>*</strong></em>
            <input type="text" id="vLastName" name="vLastName" class="validate[required] form-input-re" value="{$db_customer[0].vLastName}">
          </label>
          <div style="clear:both;"></div>
        </div>
        <div class="reg-input1">
          <label><em>{$smarty.const.LBL_EMAIL} : <strong>*</strong></em>
            <input type="text" name="vEmail" id="vEmail" class="validate[required,custom[email]] form-input-re" value="{$db_customer[0].vEmail}"></label>
          <label><em>{$smarty.const.LBL_PASSWORD} : <strong>*</strong></em>
            <input type="password" title="Password" name="vPassword" id="vPassword" class="validate[required,minSize[6]] form-input-re"></label>
          <label><em>{$smarty.const.LBL_RE_ENTER} {$smarty.const.LBL_PASSWORD} : <strong>*</strong></em>
            <input type="password" title="Re-Enter Password" name="vRePassword" id="vRePassword" class="validate[required,equals[vPassword]] form-input-re">
          </label>
           </span>
            <label><em> {$smarty.const.LBL_MOBILE} {$smarty.const.LBL_PHONE} *</em>
           <select name="vCountry" id="vCountry" class="validate[required] profile-select1" onChange="get_sate(this.value);">
              <option value="">{$smarty.const.LBL_SELECT_COUNTRY}</option>
             {section name=i loop=$db_country}
                <option value="{$db_country[i].vCountryCode}" {if $db_country[i].vCountryCode eq $vCountry} selected {/if}>{$db_country[i].vCountry}</option>
             {/section}
            </select>

            &nbsp;&nbsp;
            <input name="vPhone" id="vPhone" type="text" class="validate[required,custom[phone]] profile-input1" value="{$db_member[0].vPhone}" />
            <!--<p>
              <input name="Data[eShowPhoneOnline]" id="eShowPhoneOnline" type="checkbox" {if $db_member[0].eShowPhoneOnline eq 'No'}checked="true"{/if} />
              &nbsp;&nbsp;{$smarty.const.LBL_NEVER_SHOW_PHONE} </p>-->
            </label>
            <label><em> {$smarty.const.LBL_STATE} </em>
            <div id="state">
            <select name="vState" id="vState" class="validate[required] profile-select form-re-selec">
              <option value="">{$smarty.const.LBL_SELECT} {$smarty.const.LBL_STATE}</option>
            </select>
            </div>
            </label>
          <label><em>{$smarty.const.LBL_PREFERRED_LANG} : <strong></strong></em>
            <select name="vLanguageCode" id="vLanguageCode" class="form-input-select form-re-selec">
              <option value="EN" {if $db_customer[0].vLanguageCode eq 'EN'} selected {/if}>{$smarty.const.LBL_ENGLISH}</option>
              <option value="FN" {if $db_customer[0].vLanguageCode eq 'FN'} selected {/if}>{$smarty.const.LBL_FRENCH}</option>

            </select>
          </label>
		  {if $PAYMENT_OPTION eq 'PayPal'}
		  <label><em>{$smarty.const.LBL_PAYMENT_EMAIL} : </em>
			<input name="vPaymentEmail" id="vPaymentEmail" type="text" class="validate[custom[email]] form-input-re" value="{$db_customer[0].vPaymentEmail}" />&nbsp;&nbsp;&nbsp;&nbsp;<a class="create-paypal" href="https://www.paypal.com/" target="_blank">{$smarty.const.LBL_CREATE_PAYPAL_ACCOUNT}</a>
			</label>
		{/if}
          <div style="clear:both;"></div>
        </div>
        <div class="reg-input1" style="border:none;">
          <label><em>{$smarty.const.LBL_ENTER_CODE} : <strong>*</strong></em>
            <input type="text" title="Captcha Code" name="capthca" id="capthca" class="validate[required] form-input-re1">
            &nbsp;<img src="{$tconfig.tsite_url}plugins/captcha/CaptchaSecurityImages.php" align="top" height="31"/>
            </label>
        </div>
        <div class="reg-input1" style="border:none;">
        <label><em class="reg-last1">&nbsp;</em>
        <div class="registration-button"><a onclick="javascript:checkregister();" href="javascript:void(0);">{$smarty.const.LBL_SUBMIT}</a> <a onclick="document.frmregister.reset();return false;" href="javascript:void(0);">{$smarty.const.LBL_RESET}</a></div>
        </div></label>
        <div style="clear:both;"></div>
      </div>
    </div>
    </form>
	{include file="left.tpl"}
  </div><div style="clear:both;"></div>
</div>
{literal}
<script>
  $('#frmregister').bind('keydown',function(e){
    if(e.which == 13){
      checkregister(); return false;
    }
  });
function checkregister(){
  jQuery("#frmregister").validationEngine('init',{scroll: false});
	jQuery("#frmregister").validationEngine('attach',{scroll: false});
	resp = jQuery("#frmregister").validationEngine('validate');
	if(resp == true){
		document.frmregister.submit();
	}else{
		return false;
	}
}

function fbconnect()
  {
	 javscript:window.location='{/literal}{$tconfig.tsite_url}fbconnect.php{literal}';
  }

 var	site_url = '{/literal}{$tconfig.tsite_url}{literal}';
    function get_sate(country){

      $("#state").html('Wait...');
	  //alert("country="+country+"&vStaten="+'{/literal}{$vState}{literal}');
      var request = $.ajax({

    	  type: "POST",
    	  url: site_url+'index.php?file=m-get_state',
    	  data: "country="+country+"&vStaten="+'{/literal}{$vState}{literal}',
    	  success: function(data) {
		  //alert(data);
    		  $("#state").html(data);
         // $('#vState').val('{/literal}{$db_member[0].vState}{literal}');
    		}
    	});

    	request.fail(function(jqXHR, textStatus) {
    	  alert( "Request failed: " + textStatus );
    	});
    }

</script>
{/literal}

{if $vCountry neq ''}
{literal}
  <script>
     get_sate('{/literal}{$vCountry}{literal}');
  </script>
{/literal}
{/if}
