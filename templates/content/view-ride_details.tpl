<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script type="text/javascript" src="{$tconfig.tsite_javascript}gmap3.js"></script>
<script type="text/javascript" src="{$tconfig.tsite_javascript}/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="{$tconfig.tsite_javascript}/jquery.fancybox.css?v=2.1.5" media="screen" />
{literal}
<style>
	#map-canvas {
    height: 250px;
    margin-top: 10px;
    padding: 0px;
    width: 97.5%;
    border:1px solid #cccccc;
	}
</style>
{/literal}
<script type="text/javascript" src="{$tconfig.tsite_javascript}jquery.raty.js"></script>
<div class="body-inner-part">
	<div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_TRIP_DETAIL}</span></div>
	<div class="main-inner-page">
		<form name="frmrent" id="frmrent" method="post" action="{$tconfig.tsite_url}index.php?file=m-bookingform&id={$id}">
			<input type="hidden" name="imemberId" id="imemberId" value="{$imemberId}">
			<input type="hidden" name="id" id="id" value="{$id}">
		</form>
		{if $from neq 'home'}
		<div class="beck-prv"><span><a href="{$tconfig.tsite_url}index.php?file=c-ride_list&action=old">&larr; {$smarty.const.LBL_BACK_TO_SEARCH}</a></span>
			<p>
				{if $previous gte '0'}
				<a href="{$tconfig.tsite_url}index.php?file=c-ride_details&id={$previous}">&larr; {$smarty.const.LBL_PREVIOUS_RIDE}</a>
				{/if}
				{if $id gt '0' and $id lt $tot_res}
				&nbsp;&nbsp;|&nbsp;&nbsp;
				{/if}
				{if $next neq ''}
				<a href="{$tconfig.tsite_url}index.php?file=c-ride_details&id={$next}">{$smarty.const.LBL_NEXT_RIDE} &rarr;</a>
				{/if}
			</p>
		</div>
		{/if}
		<div class="event-details">
			<div class="top-title">
				<!--<div class="bredcrumbs"><span><a href="#">Home</a>&nbsp;&raquo;&nbsp;<a href="#">Find Your Scholar</a>&nbsp;&raquo;&nbsp;Scholar Details</span></div> -->
				<!--<h2>{$places_str}</h2>-->
			<span>{$places_str}</span></div>
			<div class="details-main">
				<div class="left-deta">
					{if $db_ratings|@count gt 0}
					<div class="tabs2"><a id="detia" href="javascript:void(0);" onClick="show_hide('details');" class="active">{$smarty.const.LBL_TRIP_DETAIL}</a> <a id="reva" href="javascript:void(0);" onClick="show_hide('reviews');">{$smarty.const.LBL_DRIVER_REVIEWS}</a>
						<span style="float:right;">{if $db_ride[0].eLadiesOnly eq 'Yes'}<img src="{$tconfig.tsite_images}ladies.png">{/if}{if $db_ride[0].eRidePlaceType eq 'Airport'}<img src="{$tconfig.tsite_images}airport.png">{/if}{if $db_ride[0].eRidePlaceType eq 'Shopping'}<img src="{$tconfig.tsite_images}shopping.png">{/if}</span>
					</div>
					{else}
					<div class="tabs2"><a id="detia" href="javascript:void(0);" class="active">{$smarty.const.LBL_TRIP_DETAIL}</a>
						<span style="float:right;">{if $db_ride[0].eLadiesOnly eq 'Yes'}<img src="{$tconfig.tsite_images}ladies.png">{/if}{if $db_ride[0].eRidePlaceType eq 'Airport'}<img src="{$tconfig.tsite_images}airport.png">{/if}{if $db_ride[0].eRidePlaceType eq 'Shopping'}<img src="{$tconfig.tsite_images}shopping.png">{/if}</span>
					</div>
					{/if}
					<div id="geninfo" class="trip-detail-container" style="dispaly:;">
						<div class="trip-detail-data">
							<ul>
								<li><strong>{$smarty.const.LBL_PICK_UP_POINT}</strong><img src="{$tconfig.tsite_images}search-from-plot.png" alt="" />{$startpoint}</li>
								<li><strong>{$smarty.const.LBL_DROP_OFF_POINT}</strong><img src="{$tconfig.tsite_images}search-to-plot.png" alt="" />{$endpoint}</li>
								<li><strong>{$smarty.const.LBL_DATE}</strong>{$date}</li>
							<li><strong>{$smarty.const.LBL_DEPARTURE_TIME_APP}</strong>{$time}</strong></li>
						</ul>
					</div>
					<div class="trip-details">
						<h2>{$smarty.const.LBL_TRIP_DETAIL}</h2>
						<!--<div class="details-trip-photo"><img src="{$stack[0].image}" alt=""/></div>  -->
						{if $db_ride[0].tDetails neq ''}
						<div class="details-trip-right" style="float:left;">
							<p>{$db_ride[0].tDetails|nl2br}</p>
						</div>
						{/if}
						<div class="user-vehicle">
							<ul>
								{if $db_ride[0].eLeaveTime neq ''}
								<li><strong>{$smarty.const.LBL_I_WILL_LEAVE} : </strong>{$db_ride[0].eLeaveTime}</li>
								{/if}
								{if $doc eq "Yes"}
								<li><strong>{$smarty.const.LBL_DOCUMENT} : </strong>{$docprice} ({$docweight}) {$docunit}</li>
								{/if}
								{if $box eq "Yes"}
								<li><strong>{$smarty.const.LBL_BOX} : </strong>{$boxprice} ({$boxweight}) {$boxunit}</li>
								{/if}
								{if $lug eq "Yes"}
								<li><strong>{$smarty.const.LBL_LUGGAGE} : </strong>{$lugprice} ({$lugweight}) {$lugunit}</li>
								{/if}

							</ul>
						</div>
						<!-- Bhumi <div class="user-vehicle">
							<ul>
							<li><strong>{$smarty.const.LBL_DETOUR}</strong>{$db_ride[0].eWaitTime}</li>
							<li><strong>{$smarty.const.LBL_SCHEDULE_FLEXIBLITY}</strong>{$db_ride[0].eLeaveTime}</li>
							<li><strong>{$smarty.const.LBL_LAGGAGE_SIZE}</strong><!--<img src="{$tconfig.tsite_images}{$db_ride[0].eLuggageSize}.png" alt="" />--888{$db_ride[0].eLuggageSize}</li>
							<li><strong>{$smarty.const.LBL_CAR}</strong>{$stack[0].carname}&nbsp;&nbsp;<b>{$stack[0].carcomfort}</b></li>
							<li><strong>{$smarty.const.LBL_CAR_SHARE} {$smarty.const.LBL_PREFERENCES}</strong>{$stack[0].pref1}</li>
							</ul>
							<img src="{$stack[0].car_image}" alt="" />
						</div> -->
					</div>
				</div>
				<div class="user-comment-list"  id="ratinfo" style="display:none;width:100%;">
					{section name="rat" loop=$db_ratings}
					<div style="width:100%;background: none repeat scroll 0 0 #F6F7F7;border: 1px solid #DEDFDF;{if $smarty.section.rat.first}{else}margin-top:10px;{/if}">
						<table width="100%">
							<tr>
								<td valign="top" width="10%" style="text-align:center;border-right:1px solid #cccccc;">
									<img src="{$db_ratings[rat].image}"><br><br>
									<span style="color: #05A2DB;font-size: 15px;">{$smarty.const.LBL_RATE}: {$db_ratings[rat].iRate}
									</td>
									<td width="90%" valign="top">
										<table width="100%">
											<tr>
												<td width="20%" style="text-align:left;color: #000000;font-size: 15px;font-weight: normal;">{$db_ratings[rat].vFirstName}&nbsp;{$db_ratings[rat].vLastName}</td>
												<td width="80%" style="text-align:right;">{$db_ratings[rat].rating}</td>
											</tr>
											<tr>
												<td width="100%" style="text-align:left;" colspan="2">{$db_ratings[rat].tFeedback|nl2br}</td>
											</tr>
											<tr>
												<td width="100%" style="text-align:right;" colspan="2"><strong>{$generalobj->DateTimeFormat($db_ratings[rat].dAddedDate)}</strong></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</div>
						{/section}
					</div>
				</div>
				<div class="right-deta">
					<h2 id="show_link"><a href="javascript:void(0);" onClick="from_to();"><img src="{$tconfig.tsite_images}location.png" alt="" />{$smarty.const.LBL_SHOW_MAP}</a></h2>
					<h2 id="hide_link" style="display:none;"><a href="javascript:void(0);" onClick="from_to_hide();"><img src="{$tconfig.tsite_images}location.png" alt="" />{$smarty.const.LBL_HIDE_MAP}</a></h2>
					<div class="donate">
						<div id="map" style="display:none;">
							<div id="map-canvas" class="gmap3"></div>
						</div>
						<div class="trip-price-container">
							<!--Bhumi  <div class="big-price-container"> <span class="price-green" style="color:{$price_color};">{$price}</span><span>{$smarty.const.LBL_PER_PESSENGER1}</span> </div>
								<div class="big-price-container"> <span class="price-black">{$seats}</span><span>{$smarty.const.LBL_SEATS_LEFT} </span>
								<p><img src="{$tconfig.tsite_images}{$THEME}/user-man-driver-36-transparent.png" alt="" /> <img src="{$tconfig.tsite_images}empty-seat.png" /> <img src="{$tconfig.tsite_images}empty-seat.png" /></p>
							</div>  -->
							{if $button_show eq 'Yes'}
							<div class="support_link">
								<em>{$smarty.const.LBL_CLICK_FOR_BOOK}:</em>
								{if $db_ride[0].iMemberId neq $sess_iMemberId}
									{if $Remaindoc eq 0 && $Remainbox eq 0 && $Remainluggage eq 0}
										<a href="javascript:void()" class="donateGreenBtn" id="support_link">
											<span class="support_text">{$smarty.const.LBL_BOOK_SEAT_FULL}</span>
										</a>
									{elseif $smarty.const.PAYMENT_OPTION eq 'Contact'}
										<a id="support_link" class="donateGreenBtn" href="{$tconfig.tsite_url}index.php?file=c-user_profile&iMemberId={$stack[0].iMemberId}&megop=1"><span class="support_text">{$smarty.const.LBL_CONTACT_DRIVER}</span></a>
									{else}
										<a href="javascript:void()" class="donateGreenBtn" id="support_link" onClick = "chk_booking_frm();">
											<span class="support_text">{$smarty.const.LBL_BOOK_SEAT}</span>
										</a>
									{/if}
								{else}
								<a href="#" class="donateGreenBtn" id="support_link" >
									<span class="support_text">{$smarty.const.LBL_CANT_BOOK}</span>
								</a>
								{/if}
							</div>
							{else}
							Ride is Expired
							{/if}
							</div>
							<div class="clint-category">
								<ul>
									<li>
										<h2>{$smarty.const.LBL_DRIVER1}</h2>
										<a href="{$tconfig.tsite_url}index.php?file=c-user_profile&iMemberId={$stack[0].iMemberId}">
											<div class="trip-box-right"> <img src="{$stack[0].image}" alt="" />
												<p><strong>{$stack[0].drivername}</strong> {*$stack[0].age} {$smarty.const.LBL_YEARS_OLD*} <em>{$stack[0].pref}</em></p>
												{$stack[0].rating}<!--<span>{$smarty.const.LBL_GOOD_DRIVE_SKILL} � 3 / 3 </span>-->
											</div>
										</a>
										{if $smarty.session.sess_iMemberId neq ''}
										<div class="ask-d">
											{if $db_ride[0].iMemberId neq $sess_iMemberId}
											<a id="support_link" class="donateGreenBtn" href="{$tconfig.tsite_url}index.php?file=c-user_profile&iMemberId={$stack[0].iMemberId}&megop=1"><span class="support_text">{$smarty.const.LBL_ASK_DRIVER}</span></a>
											{/if}
										</div>
										{/if}
									</li>
								</ul>
							</div>
							<div class="clint-category1">
								<h2>{$smarty.const.LBL_MEMBER_VERIFICATION}</h2>
								<ul>
									{if $PHONE_VERIFICATION_REQUIRED eq 'Yes'}
									<li style="background:#F4F4F4;">{if $stack[0].ePhoneVerified eq 'No'}<img src="{$tconfig.tsite_images}verification-no-check.png" alt="" />{else}<img src="{$tconfig.tsite_images}/verification-checked.png" alt="" />{/if}{$smarty.const.LBL_PHONE_VARIFIED}</li>
									{/if}
									<!--
									<li style="background:#F4F4F4;">{if $stack[0].eEmailVarified eq 'No'}<img src="{$tconfig.tsite_images}verification-no-check.png" alt="" />{else}<img src="{$tconfig.tsite_images}/verification-checked.png" alt="" />{/if}{$smarty.const.LBL_EMAIL_VERIFIED} </li>
									<li style="background:#F4F4F4;">{if $stack[0].ePaymentEmailVerified eq 'No'}<img src="{$tconfig.tsite_images}verification-no-check.png" alt="" />{else}<img src="{$tconfig.tsite_images}/verification-checked.png" alt="" />{/if}{$smarty.const.LBL_PAYPALEMAIL_VARIFICATION} </li>-->
									{if $LICENSE_VERIFICATION_REQUIRED eq 'Yes'}
									<li style="background:#F4F4F4;">{if $stack[0].eLicenseStatus neq 'Approved'}<img src="{$tconfig.tsite_images}verification-no-check.png" alt="" />{else}<img src="{$tconfig.tsite_images}/verification-checked.png" alt="" />{/if}{$smarty.const.LBL_LICENSE_VERIFIED} </li>
									{/if}
									{if $CARPAPER_VERIFICATION_REQUIRED eq 'Yes'}
									<li style="background:#F4F4F4;">{if $stack[0].eCarPaperStatus neq 'Approved'}<img src="{$tconfig.tsite_images}verification-no-check.png" alt="" />{else}<img src="{$tconfig.tsite_images}/verification-checked.png" alt="" />{/if}{$smarty.const.LBL_CAR_PAPER_VERIFIED} </li>
									{/if}
									<!-- <li style="background:#F4F4F4;"><img src="{$tconfig.tsite_images}fb-right-i.png" alt="" />{$stack[0].vFbFriendCount} {$smarty.const.LBL_FRIENDS}</li> -->
								</ul>
							</div>
							<div class="clint-category2">
								<h2>{$smarty.const.LBL_MEMBER_ACTIVITY}</h2>
								<ul>
									<li><img src="{$tconfig.tsite_images}rides-right-i.png" alt="" />{$stack[0].tot_ride} {$smarty.const.LBL_RIDES_OFFERED}</li>
								<!--<li><img src="{$tconfig.tsite_images}fb-right-i.png" alt="" />{$stack[0].vFbFriendCount} {$smarty.const.LBL_FACEBOOK_FRIEND}</li>-->
									<!--<li><img src="{$tconfig.tsite_images}rate-right-i.png" alt="" />100 % response rate </li>-->
									<li><img src="{$tconfig.tsite_images}clock-right-i.png" alt="" />Last online: {$generalobj->DateTime($stack[0].tLastLogin)} </li>
									<li><img src="{$tconfig.tsite_images}member-right-i.png" alt="" />{$smarty.const.LBL_MEMBER_SINCE}: {$stack[0].JoinDate} </li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div style="clear:both;"></div>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
	<div style="display:none">
		<div id="loginmodel">
			<div class="form-login">

				<h4>{$smarty.const.LBL_NEW_CUST}?</h4>
				<p>{$smarty.const.LBL_CUST_DESCRIPTION}</p>
				<div class="singlerow-login-log">
					<a href="{$tconfig.tsite_url}sign-up">{$smarty.const.LBL_REGISTER_NOW}</a>
				</div>
				<div class="bot-line-login">{$smarty.const.LBL_OR}</div>
				<Br/>
				<h4>{$smarty.const.LBL_LOGIN}</h4>
				<form name="login_box" id="login_box" method="post">
					<input type="hidden" name="lgfor" id="lgfor" value="">
					<input type="hidden" name="from" id="from" value="">

					<input type="hidden" name="action" id="action" value="loginchk">
					<div class="inner-form1">
						<label class="email-label">{$smarty.const.LBL_EMAIL} : <strong>*</strong></label>
						<div class="singlerow-login">
							<input type="text" name="vEmail" id="vEmail" class="validate[required,custom[email]] form-input-login" value="{$vEmail}">
						</div>
						<label class="email-label">{$smarty.const.LBL_PASSWORD} : <strong>*</strong></label>
						<div class="singlerow-login">
							<input type="password"  name="vPassword" id="vPassword" class="validate[required,minSize[6]] form-input-login" value="{$vPassword}">
						</div>
						<label class="email-label"></label>
						<div class="singlerow-login-log">
							<a onClick="check_login_small();" href="javascript:void(0);">{$smarty.const.LBL_SUBMIT}</a>
							<a onclick="document.login_box.reset();return false;" href="javascript:void(0);">{$smarty.const.LBL_RESET}</a>
						</div>
						<div style="clear:both;"></div>
					</div>
				</form>
				<div class="bot-line-login">{$smarty.const.LBL_OR}</div>
				<table width="100%" cellspacing="0" cellpadding="7" border="0">
					<tbody>
						<tr>
							<td align="center"><button class="btn btn-primary" onclick="fbconnect();return false;">
								<a style="color:#fff;" onclick="fbconnect();"><img src="{$tconfig.tsite_images}fb-but-icon.png" alt="" />{$smarty.const.LBL_CONNECT_FACEBOOK}</a>
							</button></td>
						</tr>
					</tbody>
				</table>
		</div>
	</div>
</div>
	{literal}
	<script>
		function show_hide(type){
			if(type == 'details'){
				$("#detia").addClass( "active" );
				$("#reva").removeClass( "active" );
				$('#ratinfo').hide();
				$('#geninfo').show();
				}else{
				$("#reva").addClass( "active" );
				$("#detia").removeClass( "active" );
				$('#geninfo').hide();
				$('#ratinfo').show();
			}
		}

		function clearThat(){
			var opts = {};
			opts.name = ["marker", "directionsrenderer"];
			opts.first = true;
			$('#map-canvas').gmap3({clear:opts});
		}
		function from_to(){
			clearThat();

			var waypts = [];
			var loc1 = '{/literal}{$loc1}{literal}';
			var loc2 = '{/literal}{$loc2}{literal}';
			var loc3 = '{/literal}{$loc3}{literal}';
			var loc4 = '{/literal}{$loc4}{literal}';
			var loc5 = '{/literal}{$loc5}{literal}';
			var loc6 = '{/literal}{$loc6}{literal}';

			if(loc1 != ''){
				waypts.push({location:loc1, stopover:true});
			}
			if(loc2 != ''){
				waypts.push({location:loc2, stopover:true});
			}
			if(loc3 != ''){
				waypts.push({location:loc3, stopover:true});
			}
			if(loc4 != ''){
				waypts.push({location:loc4, stopover:true});
			}
			if(loc5 != ''){
				waypts.push({location:loc5, stopover:true});
			}
			if(loc6 != ''){
				waypts.push({location:loc6, stopover:true});
			}

			$("#map-canvas").gmap3({
				getroute:{
					options:{
						origin:'{/literal}{$db_ride[0].vMainDeparture}{literal}',
						destination:'{/literal}{$db_ride[0].vMainArrival}{literal}',
						waypoints:waypts,
						travelMode: google.maps.DirectionsTravelMode.DRIVING
					},
					callback: function(results){
						if (!results) return;
						$(this).gmap3({
							map:{
								options:{
									zoom: 13,
									center: [-33.879, 151.235]
								}
							},
							directionsrenderer:{
								options:{
									directions:results
								}
							}
						});
					}
				}
			});
			$('#map').show();
			$('#show_link').hide();
			$('#hide_link').show();
		}

		function from_to_hide(){
			//clearThat();
			$('#map').hide();
			$('#hide_link').hide();
			$('#show_link').show();
		}
		function chk_booking_frm(){
			if('{/literal}{$smarty.session.sess_iMemberId}{literal}' == ''){
				$.fancybox("#loginmodel");return false;
				}else{
				document.frmrent.submit();
			}
		}
		function check_login_small(){
			jQuery("#login_box").validationEngine('attach',{scroll: false});
			resp = jQuery("#login_box").validationEngine('validate');
			if(resp == true){
				var request = $.ajax({
					type: "POST",
					url: site_url+'index.php?file=c-login',
					data: $("#login_box").serialize(),

					success: function(data) {

						$("#loginloader").hide();
						$("#loginbtn").show();
						if(data == 0){
							showNotification({type : 'error', message: '{/literal}{$smarty.const.LBL_LOGIN_FAILED_USER_PASS}.{literal}'});
						}
						else if(data == 1){
							showNotification({type : 'error', message: '{/literal}{$smarty.const.LBL_ACC_NOT_ACTIVE}.{literal}'});
						}
						else if(data == 2){    // Male user logged in

							if('{/literal}{$db_ride[0].eLadiesOnly}{literal}' == 'Yes')
							{
								window.location = '{/literal}{$tconfig.tsite_url}{literal}index.php?file=c-ride_details&from={/literal}{$smarty.request.from}{literal}&id={/literal}{$smarty.request.id}{literal}&dt={/literal}{$smarty.request.dt}{literal}&ret={/literal}{$smarty.request.ret}{literal}';
							}
							else {
								document.frmrent.submit();
							}
						}
						else if(data == 3) { // Female user logged in

							document.frmrent.submit();
						}
						else{
							//alert('{/literal}{$smarty.session.sess_iMemberId}{literal}')'
							document.frmrent.submit();
						}
					}
				});

				request.fail(function(jqXHR, textStatus) {
					alert( "Request failed: " + textStatus );
				});
				}else{
				return false;
			}
		}

		function fbconnect()
		{
			javscript:window.location='{/literal}{$tconfig.tsite_url}fbconnect.php{literal}';
		}
	</script>
{/literal}
