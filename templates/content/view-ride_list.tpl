<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script type="text/javascript" src="{$tconfig.tsite_javascript}gmap3.js"></script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=182852035257135";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script type="text/javascript" src="{$tconfig.tsite_url}rrb/zebra_datepicker.src.js"></script>
<link rel="stylesheet" href="{$tconfig.tsite_url}rrb/default.css" type="text/css">
<link href="{$tconfig.tsite_url}rbslider/css/redmond/jquery-ui-1.10.3.custom.css" rel="stylesheet">
<script src="{$tconfig.tsite_url}rbslider/js/jquery-ui-1.10.3.custom.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script type="text/javascript" src="{$tconfig.tsite_javascript}gmap3.js"></script>
<script type="text/javascript" src="{$tconfig.tsite_javascript}jquery.autocomplete.js"></script>
<script type="text/javascript" src="{$tconfig.tsite_javascript}jquery.atooltip.js"></script>
<link rel="stylesheet" type="text/css" href="{$tconfig.tsite_stylesheets}front/jquery.autocomplete.css" media="screen,projection" />
<div class="body-inner-part">
	<div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_SR_RIDE} {$smarty.const.LBL_SEARCH_RESULTS}</span></div>
	<div class="main-inner-page">
		<h2>{$smarty.const.LBL_SR_RIDE}<span>{$smarty.const.LBL_SEARCH_RESULTS}</span></h2>
		<div class="search-car-top-part search-car-top-part1"> <span>{$smarty.const.LBL_SEARCH_RIDE}</span>
			<form name="home_search" id="home_search" method="post" action="{$tconfig.tsite_url}index.php?file=c-ride_list&search=place">
				<p>
					<input name="search" id="search" type="hidden" value="place">
					<input name="From_lat_long" id="From_lat_long" type="hidden" value="">
					<input name="To_lat_long" id="To_lat_long" type="hidden" value="">
					<!--<input name="From" id="From" type="text" placeholder="{$smarty.const.LBL_FROM}" class="validate[required] search-car-find-cla" value=""/>-->
					<input name="From" id="From"  type="text" placeholder="{$smarty.const.LBL_FROM}" class="validate[required] search-car-find-cla" value="{$From}"/>
					<input placeholder="{$smarty.const.LBL_TO}" name="To" id="To" type="text" class="validate[required] search-car-find-cla" value="{$To}"/>
				</p>
				<p> <a href="javascript:void(0);" onClick="search_now();">{$smarty.const.LBL_SEARCH}</a> </p>
			</form>
		</div>
		<div class="sharing-result"><div class="sharing-result-left">
			<form name="searchfrm" id="searchfrm" method="post" action="{$tconfig.tsite_url}index.php?file=c-ride_list">
				<input type="hidden" name="action" id="action" value="old">
				<input type="hidden" name="fromhr" id="fromhr" value="{$fromassign}">
				<input type="hidden" name="tohr" id="tohr" value="{$toassign}">
				<input type="hidden" name="schedule" id="schedule" value="{$ordscheduleby}">
				<input type="hidden" name="priceord" id="priceord" value="{$ordpriceby}">
				<input type="hidden" name="srtby" id="srtby" value="{$srtby}">
				<input type="hidden" name="comfort" id="comfort" value="{$comfort}">
				<input type="hidden" name="eDocument" id="eDocument" value="{$eDocument}">
				<input type="hidden" name="eLuggage" id="eLuggage" value="{$eLuggage}">
				<input type="hidden" name="eBox" id="eBox" value="{$eBox}">
				<input type="hidden" name="fltphoto" id="fltphoto" value="{$fltphoto}">
				<input type="hidden" name="pricetype" id="pricetype" value="{$pricetype}">
				<input type="hidden" name="fltmemrating" id="fltmemrating" value="{$fltmemrating}">
				
					<!--hide sort by time
						<div class="trip-search-facets">
						<h2><b>{$smarty.const.LBL_DATE}</b></h2>
						<span>
							<input name="searchdate" id="searchdate" type="text" class="calander" value="{$searchdate}"/>
							</span> <span>{$smarty.const.LBL_DEPARTURE_DATE}: <em id="from">{$fromassign}:00</em> - <em id="to">{$toassign}:00</em></span> <span>
							<div id="slider-range"></div>
						</span> </div>-->
						<!--Bhumi  <div class="trip-comfort">
							<h2>{$smarty.const.LBL_PRICE}</h2>
							<span><input name="ePriceType" id="All" type="radio" value="All" class="radio1" onClick="fltbyprice('All');"/><strong>All</strong></span>
							<span><input name="ePriceType" id="Average" type="radio" value="Average" class="radio1" onClick="fltbyprice('Average');"/><strong>Average</strong></span>
							<span><input name="ePriceType" id="High" type="radio" value="High" class="radio1" onClick="fltbyprice('High');"/><strong>High</strong></span> 
							<span><input name="ePriceType" id="Low" type="radio" value="Low" class="radio1" onClick="fltbyprice('Low');"/><strong>Low</strong></span> 
						</div>  
						<div class="trip-photo">
							<h2><b>{$smarty.const.LBL_PHOTO}</b></h2>
							<span>
								<input name="fltimg" id="Allphoto" type="radio" value="Allphoto" class="radio1" onClick="fltbyphoto('Allphoto');"/>
								<strong>{$smarty.const.LBL_ALL}</strong></span> <span>
								<input name="fltimg" id="Photo" type="radio" value="Photo" class="radio1" onClick="fltbyphoto('Photo');"/>
							<strong>{$smarty.const.LBL_PHOTO_ONLY}</strong></span> </div> --->
							<!--- Document,Luggage,Box Start ---->
							<div class="trip-comfort">
								<h2><b>{$smarty.const.LBL_SHIPMENT_TYPE}</b></h2>
								<span>
									<input name="shipmenttypedocu" id="shipmenttypedocu" type="checkbox" onClick="fltbyshipment('document',this.checked);" {if $eDocument eq 'Yes'}checked{/if} class="form-checkbox"/>
									<strong>{$smarty.const.LBL_DOCUMENT}</strong></span> <span>
									<input name="shipmenttypebox" id="shipmenttypebox" type="checkbox" onClick="fltbyshipment('box',this.checked);" {if $eBox eq 'Yes'}checked{/if} class="form-checkbox"/>
								<strong>{$smarty.const.LBL_BOX}</strong></span> <span>
									<input name="shipmenttypeluggage" id="shipmenttypeluggage" type="checkbox" onClick="fltbyshipment('luggage',this.checked);" {if $eLuggage eq 'Yes'}checked{/if} class="form-checkbox"/>
									<strong>{$smarty.const.LBL_LUGGAGE}</strong></span> </div>
								<!--- Document,Luggage,Box End --->
								<!-- Bhumi <div class="trip-comfort">
									<h2>{$smarty.const.LBL_CAR_CONFORT}</h2>
									<span><input name="carcomfort" id="All" type="radio" value="All" class="radio1" onClick="fltbycarcomf('All');"/><strong>{$smarty.const.LBL_ALL_TYPE}</strong></span>
									<span><input name="carcomfort" id="Basic" type="radio" value="Basic" class="radio1" onClick="fltbycarcomf('Basic');"/><strong>{$smarty.const.LBL_BASIC}</strong></span> 
									<span><input name="carcomfort" id="Normal" type="radio" value="Normal" class="radio1" onClick="fltbycarcomf('Normal');"/><strong>{$smarty.const.LBL_NORMAL}</strong></span> 
									<span><input name="carcomfort" id="Comfortable" type="radio" value="Comfortable" class="radio1" onClick="fltbycarcomf('Comfortable');"/><strong>{$smarty.const.LBL_COMFORTABLE}</strong></span>
									<span><input name="carcomfort" id="Luxury" type="radio" value="Luxury" class="radio1" onClick="fltbycarcomf('Luxury');"/><strong>{$smarty.const.LBL_LUXURY}</strong></span> 
								</div>  --->
								<div class="trip-comfort">
									<h2><b>{$smarty.const.LBL_RATING}</b></h2>
									<span>
										<input name="memrating" id="memrating" type="radio" value="All" class="radio1" {if $fltmemrating eq 'All'}checked{/if} onClick="fltbymemrating('All');"/>
										<strong>{$smarty.const.LBL_ALL}</strong></span> <span>
										<input name="memrating" id="memrating1" type="radio" value="1" class="radio1"  {if $fltmemrating eq '1'}checked{/if} onClick="fltbymemrating('1');"/>
										<img src="{$tconfig.tsite_images}{$THEME}/1star.png"></span> <span>
										<input name="memrating" id="memrating2" type="radio" value="2" class="radio1"  {if $fltmemrating eq '2'}checked{/if} onClick="fltbymemrating('2');"/>
										<img src="{$tconfig.tsite_images}{$THEME}/2star.png"></span> <span>
										<input name="memrating" id="memrating3" type="radio" value="3" class="radio1"  {if $fltmemrating eq '3'}checked{/if} onClick="fltbymemrating('3');"/>
										<img src="{$tconfig.tsite_images}{$THEME}/3star.png"></span> <span>
										<input name="memrating" id="memrating4" type="radio" value="4" class="radio1"  {if $fltmemrating eq '4'}checked{/if} onClick="fltbymemrating('4');"/>
										<img src="{$tconfig.tsite_images}{$THEME}/4star.png"></span> <span>
										<input name="memrating" id="memrating5" type="radio" value="5" class="radio1"  {if $fltmemrating eq '5'}checked{/if} onClick="fltbymemrating('5') ;"/>
									<img src="{$tconfig.tsite_images}{$THEME}/5star.png"></span> </div>
				
			</form></div>
			<div class="sharing-result-right"> {if $search eq 'place'}
				<div class="trip-alert-form-container">
					<h2><b>{$smarty.const.LBL_LIST_RIDE_MESSAGE1}</b></h2>
					<img src="{$tconfig.tsite_images}email-envelope2.png" alt="" />
					<form name="frmalert" id="frmalert" method="post">
						<span> <em>{$smarty.const.LBL_LIST_RIDE_MESSAGE2} {$From} - {$To} {$smarty.const.LBL_LIST_RIDE_MESSAGE3}</em>
							<input type="hidden" name="iMemberId" id="iMemberId" value="{$sess_iMemberId}">
							<input type="hidden" name="vStartPoint" id="vStartPoint" value="{$From}">
							<input type="hidden" name="vEndPoint" id="vEndPoint" value="{$To}">
							<input type="hidden" name="vStartLatitude" id="vStartLatitude" value="{$from_lat}">
							<input type="hidden" name="vStartLongitude" id="vStartLongitude" value="{$from_long}">
							<input type="hidden" name="vEndLatitude" id="vEndLatitude" value="{$to_lat}">
							<input type="hidden" name="vEndLongitude" id="vEndLongitude" value="{$to_long}">
							<input name="dDate" id="dDate" type="text" class="validate[required] trip-input" value="" placeholder="{$smarty.const.LBL_SELECT_TRAVEL_DATE}"/>
							<input name="vAlertEmail" id="vAlertEmail" type="text" class="validate[required,custom[email]] trip-input" value="{$sess_vEmail}"  placeholder="{$smarty.const.LBL_ENTER_YOUR_EMAIL1}"/>
						<a href="javascript:void(0);" onClick="create_mail_alert();"> {$smarty.const.LBL_CREATE_EMAIL_ALERT}</a> </span>
					</form>
				</div>
				{/if}
				<div class="trip-search-sorts"> <span>{$mainarr|@count} {$smarty.const.LBL_RIDES_AVILABLE}</span>
					<div class="trip-search-sort-right"><em>{$smarty.const.LBL_REARRANGE_BY}</em> <a id="schanc" href="javascript:void(0);" onClick="arrbyschedule(0);">{$smarty.const.LBL_SCHEDULE}&nbsp;&nbsp;<img id="schedimg" src="{$tconfig.tsite_images}white-point-up.png" alt="" /></a>
						<!-- Bhumi <a id="priceanc" href="javascript:void(0);" onClick="arrbyschedule(1);">{$smarty.const.LBL_PRICE}&nbsp;&nbsp;<img id="priceimg" src="{$tconfig.tsite_images}white-point.png" alt="" /></a>  -->
					</div>
				</div>
				<input type="hidden" name="pagecount" id="pagecount" value="11">
				<span style="display:none;">{counter start=1}</span> {section name="rides" loop=$mainarr}
				<div id="div_{$smarty.section.rides.index}" class="ridelist-ho" {if $smarty.section.rides.index gt '10'} style="display:none;" {else} style="display:;" {/if}>
				<a href="{$tconfig.tsite_url}index.php?file=c-ride_details&id={$smarty.section.rides.index}">{$mainarr[rides].details}</a> </div>
				<!--<div style="float: right;position: relative;right: 208px;top: -54px;">
					<table width="100%">
					<tr>
					<td>
					Share on: 
					</td>
					<td>
					<div class="fb-share-button" data-href="http://www.balticcar.co/index.php?file=c-ride_details&from=home&id='.$mainsessionarr[$i]['iRideId'].'&dt='.$mainsessionarr[$i]['strtotime'].'&ret='.$mainsessionarr[$i]['return'].'" data-type="button"></div>
					</td>
					<td>
					<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://balticcar" data-lang="en-gb" data-count="none">Tweet</a>
					</td>
					</tr>
					</table>
				</div>-->
				{/section}
				{if $totrec gt '0' and $totrec gt '10'} <a id="load_butt" class="load_butt1" href="javascript:void(0);" onClick="lod_more_data();">{$smarty.const.LBL_LOAD_MORE}</a> {/if}
				<!--<div class="paging">
					<p>1 to 10 of 30 results</p>
					<span><a href="#">?</a> <a href="#" class="active">1</a> <a href="#">2</a> <a href="#">3</a> <a href="#">4</a> <a href="#">5</a> <a href="#">?</a></span>
				</div>-->
			</div>
			<div style=" clear:both;"></div>
		</div>
		<div style=" clear:both;"></div>
	</div>
	<div style="clear:both;"></div>
	<div id="map-canvas" class="gmap3"></div>
</div>
{if $comfort neq ''}
{literal}
<script>
	$('#{/literal}{$comfort}{literal}').attr("checked", "checked");
</script>
{/literal} 
{/if}
{if $pricetype neq ''}
{literal}
<script>
	$('#{/literal}{$pricetype}{literal}').attr("checked", "checked");
</script>
{/literal} 
{/if}

{if $fltphoto neq ''}
{literal}
<script>
	$('#{/literal}{$fltphoto}{literal}').attr("checked", "checked");
</script>
{/literal} 
{/if}  
{if $srtby eq 0}
{literal}
<script>
	//$("#schanc").css("background-color","#006197");
</script>
{/literal}
{else}
{literal}
<script>
	$("#priceanc").css("background-color","#006197");
</script>
{/literal}
{/if}
{if $ordscheduleby eq 'ASC'}
{literal}
<script>
	$("#schedimg").attr("src","{/literal}{$tconfig.tsite_images}{literal}white-point-up.png");
</script>
{/literal}
{else}
{literal}
<script>
	$("#schedimg").attr("src","{/literal}{$tconfig.tsite_images}{literal}white-point.png");
</script>
{/literal}
{/if}
{if $ordpriceby eq 'ASC'}
{literal}
<script>
	$("#priceimg").attr("src","{/literal}{$tconfig.tsite_images}{literal}white-point-up.png");
</script>
{/literal}
{else}
{literal}
<script>
	$("#priceimg").attr("src","{/literal}{$tconfig.tsite_images}{literal}white-point.png");
</script>
{/literal}
{/if}
{literal}
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
<script>
    var totrec = {/literal}{$totrec}{literal};
    function lod_more_data(){
		var pagecount = $("#pagecount").val(); 
		$("#pagecount").val(Number(pagecount)+ Number(10));
		var incre = $("#pagecount").val();
		if(totrec <= incre){
			$("#load_butt").hide();
		} 
		for (var i=pagecount;i<incre;i++)
		{ 
			$("#div_"+i).show(); 
		}       
	}
    
    function arrbyschedule(val){
		$("#srtby").val(val);
		if(val == 0){
			var schedule = $("#schedule").val();
			if(schedule == 'ASC'){
				$("#schedule").val('DESC');          
				$("#schedimg").attr("src","{/literal}{$tconfig.tsite_images}{literal}white-point-up.png");
				document.searchfrm.submit();
				}else{
				$("#schedule").val('ASC');
				$("#schedimg").attr("src","{/literal}{$tconfig.tsite_images}{literal}white-point.png");
				document.searchfrm.submit();
			}
			}else{
			var priceord = $("#priceord").val();
			if(priceord == 'ASC'){
				$("#priceord").val('DESC');          
				$("#priceimg").attr("src","{/literal}{$tconfig.tsite_images}{literal}white-point-up.png");
				document.searchfrm.submit();
				}else{
				$("#priceord").val('ASC');
				$("#priceimg").attr("src","{/literal}{$tconfig.tsite_images}{literal}white-point.png");
				document.searchfrm.submit();
			}
		}       
	}
    
    function fltbycarcomf(type){
		$("#comfort").val(type);
		document.searchfrm.submit();
	}
    
    function fltbyshipment(type, val)
    {
		if(type == "document")
		{
			if(val == true)
			{
				$("#eDocument").val('Yes');
				document.searchfrm.submit();
			} 
			else
			{
				$("#eDocument").val('No');
				document.searchfrm.submit();
			}       
		}
		else if(type == "luggage")
		{
			if(val == true)
			{
				$("#eLuggage").val('Yes');
				document.searchfrm.submit();
			} 
			else
			{
				$("#eLuggage").val('No');
				document.searchfrm.submit();
			}
		}
		else if(type == "box")
		{
			if(val == true)
			{
				$("#eBox").val('Yes');
				document.searchfrm.submit();
			} 
			else
			{
				$("#eBox").val('No');
				document.searchfrm.submit();
			}
		}
		
	}
    
    function fltbyprice(type){
		$("#pricetype").val(type);
		document.searchfrm.submit();
	}
    
    function fltbyphoto(type){
		$("#fltphoto").val(type);
		document.searchfrm.submit();
	}
	
    function fltbymemrating(type){
		$("#fltmemrating").val(type);
		document.searchfrm.submit();
	}
    
    $('#searchdate').Zebra_DatePicker({
		direction: 1,
		onSelect: function() {
			document.searchfrm.submit();
		},
		onClear: function() {
			document.searchfrm.submit();
		}
		
	});
    
    $('#dDate').Zebra_DatePicker({
		direction: 1
	});
    
    function ass_hrs(from, to){ 
		$("#fromhr").val(from);
		$("#tohr").val(to); 
		
		document.searchfrm.submit();       
	}
    
    $( "#slider-range" ).slider({
		range: true,
		min: 1,
		max: 24,
		values: [ {/literal}{$fromassign}{literal}, {/literal}{$toassign}{literal} ], 
		animate: "slow",
		step: 1,
		change: function(event, ui) { 
			if(ui.values[1] - ui.values[0] < 1 ){
				return false;  
			}
			$("#from").html(ui.values[0]+'h');
			$("#to").html(ui.values[1]+'h');
			$("#fromhr").val(ui.values[0]);
			$("#tohr").val(ui.values[1]);
			
			ass_hrs(ui.values[0], ui.values[1]); 
		}  
	});
    
    var	site_url = '{/literal}{$tconfig.tsite_url}{literal}';
    
    function create_mail_alert(){
		resp = jQuery("#frmalert").validationEngine('validate');        
  		if(resp == true){
  			var request = $.ajax({  
				type: "POST",
				url: site_url+'index.php?file=c-emailalert',  
				data: $("#frmalert").serialize(), 	  
				
				success: function(data) {
					if(data == 1){
						showNotification({type : 'success', message: '{/literal}{$smarty.const.LBL_YOUR_EMAIL_ALERT_SAVED}{literal}'});  
						}else if(data == 2){
						showNotification({type : 'warning', message: '{/literal}{$smarty.const.LBL_EMAIL_ALERT_ALREADY}{literal}'}); 
						}else{
						showNotification({type : 'error', message: '{/literal}{$smarty.const.LBL_SOME_ERRO_TRY_AGAIN}{literal}'});  
					}        
					$('#frmalert')[0].reset();
					return false;   			
				}
			});
			
			request.fail(function(jqXHR, textStatus) {
				alert( "Request failed: " + textStatus ); 
			});   
			}else{
  			return false;
		}
	}
</script>
{/literal}


{literal}
<script type="text/javascript">
	function initialize() {
		//var input = document.getElementById("From);     
		//var options = {componentRestrictions: {country: 'us'}};
		
		var from = new google.maps.places.Autocomplete(document.getElementById('From'));
		var to = new google.maps.places.Autocomplete(document.getElementById('To'));
	}  
	
	google.maps.event.addDomListener(window, 'load', initialize);
	
	function search_now(){ 
		resp = jQuery("#home_search").validationEngine('validate');    
		if(resp == true){
			if(document.getElementById('From').value !='' && document.getElementById('To').value != ''){
				var From = $("#From").val();
				$("#map-canvas").gmap3({
					getlatlng:{
						address:  From,
						callback: function(results){
							if(results[0] == null)
							{
									$("#From_lat_long").val("");
							}
							$("#From_lat_long").val(results[0].geometry.location);
						}
					}
				});
				
				var To = $("#To").val();
				$("#map-canvas").gmap3({
					getlatlng:{
						address:  To,
						callback: function(results){
							if(results[0] == null)
							{
									$("#To_lat_long").val("");
							}
							$("#To_lat_long").val(results[0].geometry.location);
							document.home_search.submit();
						}
					}
				}); 
				return false;
				}else{
				alert('{/literal}{$smarty.const.LBL_SELECT_FROM_TO_ADD}{literal}');
				return false;
				}
			}else{
			return false;
		}
	}
</script>
<script type="text/javascript">        
	$(function() {
		var from = document.getElementById('From');        
		var autocomplete_from = new google.maps.places.Autocomplete(from);
		google.maps.event.addListener(autocomplete_from, 'place_changed', function() {
			var place = autocomplete_from.getPlace();     		
			go_get_latlong('From');
		});
		
		var to = document.getElementById('To');
		var autocomplete_to = new google.maps.places.Autocomplete(to);
		google.maps.event.addListener(autocomplete_to, 'place_changed', function() {
			var place = autocomplete_to.getPlace();     			
			go_get_latlong('to');
		});
	});
	
</script>
{/literal}