<script type="text/javascript" src="{$tconfig.tsite_javascript}jquery.raty.js"></script>
<div class="body-inner-part">
      <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_USR_PROFILE}</span></div>
<div class="main-inner-page">
        <h2>{$smarty.const.LBL_USR_PROFILE}</h2>
        <div class="dashbord">
          <div class="dashbord-left-part">
            <div class="dashbord-left-in">
              <h1><em>{$smarty.const.LBL_MY_VERIFICATION}</em></h1>
              <ul>
                {if $PHONE_VERIFICATION_REQUIRED eq 'Yes'}
                <li><img src="{$tconfig.tsite_images}phone-right-i.png" alt="" />&nbsp;&nbsp;{$smarty.const.LBL_PHONE_VERIFICATION} <em>{if $db_email_verification[0].ePhoneVerified eq 'No'}<img src="{$tconfig.tsite_images}{$THEME}/verification-no-check.png" alt="" />{else}<img src="{$tconfig.tsite_images}/verification-checked.png" alt="" />{/if}</em></li>
                {/if}
                <li><img src="{$tconfig.tsite_images}email-right-i.png" alt="" />&nbsp;&nbsp;{$smarty.const.LBL_EMAIL_VARIFICATION} <em>{if $db_email_verification[0].eEmailVarified eq 'No'}<img src="{$tconfig.tsite_images}/verification-no-check.png" alt="" />{else}<img src="{$tconfig.tsite_images}/verification-checked.png" alt="" />{/if}</em></li>
                <li><img src="{$tconfig.tsite_images}email-right-i.png" alt="" />&nbsp;&nbsp;{$smarty.const.LBL_PAYPALEMAIL_VARIFICATION} <em>{if $db_email_verification[0].ePaymentEmailVerified eq 'No'}<img src="{$tconfig.tsite_images}/verification-no-check.png" alt="" />{else}<img src="{$tconfig.tsite_images}/verification-checked.png" alt="" />{/if}</em></li>
                <!-- <li><img src="{$tconfig.tsite_images}fb-right-i.png" alt="" />{$db_member[0].vFbFriendCount} {$smarty.const.LBL_FRIENDS}<em>{if $db_member[0].vFbFriendCount gt 0}<img src="{$tconfig.tsite_images}{$THEME}/verification-checked.png" alt="" />{else}<img src="{$tconfig.tsite_images}{$THEME}/verification-no-check.png" alt="" />{/if}</em> </li> -->
              </ul>
            </div>
            <div class="dashbord-left-in">
              <h1><em>{$smarty.const.LBL_MEMBER_ACTIVITY}</em></h1>
              <ul>
                <li><img src="{$tconfig.tsite_images}rides-right-i.png" alt="" />&nbsp;&nbsp;{$tot_rides} {$smarty.const.LBL_RIDES_OFFERED}</li>
				<li><img src="{$tconfig.tsite_images}fb-right-i.png" alt="" />&nbsp;&nbsp;{$db_member[0].vFbFriendCount} {$smarty.const.LBL_FACEBOOK_FRIEND}</li>
                <li><img src="{$tconfig.tsite_images}member-right-i.png" alt="" />&nbsp;&nbsp;{$smarty.const.LBL_MEMBER_SINCE} : {$generalobj->DateTimeFormat($db_email_verification[0].dAddedDate)} </li>
                {*if $same_member eq 1*}
                  <!--<li><a href="{$tconfig.tsite_url}Member-Varification" class="active1">{$smarty.const.LBL_COMPLETE_MEM_VERIFICATION}</a></li>-->
                {*/if*}
              </ul>
            </div>
           <!-- Hemali.. <div class="dashbord-left-in">
              <h1>{$smarty.const.LBL_MY_CAR}</h1>
              {if $db_car|@count gt 0}
              <ul>
                <li>
                  {if $db_car[0].img neq ''}<img src="{$db_car[0].img}" alt="" />{/if}
                  <h3>{$db_car[0].vMake},&nbsp;&nbsp;{$db_car[0].vTitle}</h3>
                  <p>{$smarty.const.LBL_COLOR}: {$db_car[0].vColour}</p>
                  <p>{$smarty.const.LBL_COMFORT}: {$db_car[0].eComfort}</p>
                </li>
                {if $same_member eq 1}
                  <li><a href="{$tconfig.tsite_url}car-details" class="active1">{$smarty.const.LBL_EDIT_YOUR_CAR}</a></li>
                {/if}
              </ul>
              {else}
              <ul><li>{$smarty.const.LBL_NO_CAR}</li></ul>
              {/if}
            </div> -->    
            <div style="clear:both;"></div>
          </div>
          <div class="dashbord-right-part">
            <div class="user-profile-page">
              <div class="main-infos">
                <div class="member-picture"> {if $db_member[0].img neq ''}<img src="{$db_member[0].img}" alt="" />{/if} </div>
                <div class="main-infos-list">
                  <ul>
                    <li>
                      <h1>{$db_member[0].vFirstName} {$db_member[0].vLastName} 
                       <!--hide age
                       {if $db_member[0].iBirthYear neq '0'}<span class="user-age">({$member_age} {$smarty.const.LBL_YEARS_OLD})</span>{/if}-->
                       {if $same_member eq 1}
                        <div style="float:right"><a class="link-dashbord" href="{$tconfig.tsite_url}personal-information">{$smarty.const.LBL_EDIT_PROF}</a></div>
                       {/if}
                      </h1>
                    </li>
					<!-- Hemali..
                    {if $db_pref|@count gt 0}
                    <li>{$smarty.const.LBL_MY_PREFERENCE1}</li>
                    <li><em class="i-am">{$smarty.const.LBL_I_AM} &nbsp;&nbsp;</em><span class="big-star-rating">{$pref}
                        <img src="{$eChattiness}" alt="title1" {if $db_pref[0].eChattiness eq 'YES'}title="{$smarty.const.LBL_I_LOVE_CHAT}"{else if $db_pref[0].eChattiness eq 'MAYBE'}title="{$smarty.const.LBL_TALK_MOOD}"{else}title="{$smarty.const.LBL_QUIET_TYPE} :)"{/if}/>&nbsp;&nbsp;
                        <img src="{$eMusic}" alt="" {if $db_pref[0].eMusic eq 'YES'}title="{$smarty.const.LBL_ABOUT_PLAYLIST}" {else if $db_pref[0].eMusic eq 'MAYBE'}title="{$smarty.const.LBL_DEPEND_MOOD}" {else}title="{$smarty.const.LBL_SILENCE_GOLDEN}" {/if} />&nbsp;&nbsp;
                        <img src="{$eSmoking}" alt="" {if $db_pref[0].eSmoking eq 'YES'}title="{$smarty.const.LBL_CIGARETTE_SMOKE}" {else if $db_pref[0].eSmoking eq 'MAYBE'}title="{$smarty.const.LBL_DEPEND_MOOD}" {else}title="{$smarty.const.LBL_NO_SMOKE}" {/if} />&nbsp;&nbsp;
                        <img src="{$eEcig}" alt="" {if $db_pref[0].eEcig eq 'YES'}title="{$smarty.const.LBL_CIGARETTE_SMOKE}" {else if $db_pref[0].eEcig eq 'MAYBE'}title="{$smarty.const.LBL_DEPEND_MOOD}" {else}title="{$smarty.const.LBL_NO_SMOKE}" {/if} />&nbsp;&nbsp;
                        <img src="{$ePets}" alt="" {if $db_pref[0].ePets eq 'YES'}title="{$smarty.const.LBL_PETS1}" {else if $db_pref[0].ePets eq 'MAYBE'}title="{$smarty.const.LBL_DEPEND_MOOD}" {else}title="{$smarty.const.LBL_PETS2}" {/if} />
                        </span>
                        </li>
                    {/if}
					-->
                    {if $sess_iMemberId neq '' and $sess_iMemberId neq $iMemberId}
                    <li><a id="showmsg" class="link-dashbord" href="javascript:void(0);">{$smarty.const.LBL_SEND_MSG_TO} {$db_member[0].vFirstName}</a></li>
                    {/if}
                  </ul>
                </div>
              </div>
              <div id="messagefrmdiv" class="big-prefs" style="display:none;">
                <form name="frmmessage" id="frmmessage" method="post">
                <input type="Hidden" name="mode" id="mode" value="message">
                <input type="Hidden" name="iFromMemberId" id="iFromMemberId" value="{$sess_iMemberId}">
                <input type="Hidden" name="iToMemberId" id="iToMemberId" value="{$iMemberId}">
                <input type="Hidden" name="type" id="type" value="simple">  
                <p>
                <textarea name="tMessage" id="tMessage" class="validate[required] profile-textarea" style="width:98%;"></textarea>
                <div style="float:left;margin: 10px 0 0 10px;"><a id="sendbutton" class="link-dashbord" href="javascript:void(0);" onClick="sendmessage_valid();">{$smarty.const.LBL_SEND}</a></div>
                <div style="float:left;margin: 10px 0 0 10px;"><a id="hidemsg" class="link-dashbord" href="javascript:void(0);">{$smarty.const.LBL_CANCEL}</a></div>
                </p>
                </form>
              </div>
              <div class="big-prefs">
                <p>{if $db_member[0].tDescription neq ''}{$db_member[0].tDescription|nl2br}{else}{$smarty.const.LBL_HAVENT_DESC}{/if} </p>
              </div>
              <div class="rating-received">
                <div class="rating-received-left-part">
                  <span>{$rating} {$smarty.const.LBL_RATING}<!-- rating - 3 / 5 --> 
                    <!--<img src="{$tconfig.tsite_images}blue-star.png" alt="" />-->
                    <!--<div id="star" class="star-img"></div>-->
                    <span style="float:right;margin-left: 10px;margin-top: 3px;display: block; width: 65px; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 0;">
                        <span style="display: block; width: {$rating_width}%; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 -13px;"></span>
                    </span>
                   
                 </span> 
                </div>
                <div class="rating-received-right-part">
                   <div class="rating-received-right-part-inner"> 
                    <span>5 {$smarty.const.LBL_STARS}</span>
                    <div class="progress"><img style="width:{$totalwidth[5]}%; padding: 0px 0 7px;max-width: 100%;" src="{$tconfig.tsite_images}{$THEME}/progress.jpg"></div>
                    <span class="last-star">{$totrating[5]}</span> 
                  </div>
                  <div class="rating-received-right-part-inner"> 
                    <span>4 {$smarty.const.LBL_STARS}</span>
                    <div class="progress"><img style="width:{$totalwidth[4]}%; padding: 0px 0 7px;max-width: 100%;" src="{$tconfig.tsite_images}{$THEME}/progress.jpg"></div>
                    <span class="last-star">{$totrating[4]}</span> 
                  </div>
                  <div class="rating-received-right-part-inner"> 
                    <span>3 {$smarty.const.LBL_STARS}</span>
                    <div class="progress"><img style="width:{$totalwidth[3]}%; padding: 0px 0 7px;max-width: 100%;" src="{$tconfig.tsite_images}{$THEME}/progress.jpg"></div>
                    <span class="last-star">{$totrating[3]}</span>
                  </div>
                  <div class="rating-received-right-part-inner">
                    <span>2 {$smarty.const.LBL_STARS}</span>
                    <div class="progress"><img style="width:{$totalwidth[2]}%; padding: 0px 0 7px;max-width: 100%;" src="{$tconfig.tsite_images}{$THEME}/progress.jpg"></div>
                    <span class="last-star">{$totrating[2]}</span>
                  </div>
                  <div class="rating-received-right-part-inner">
                    <span>1 {$smarty.const.LBL_STARS}</span>
                    <div class="progress"><img style="width:{$totalwidth[1]}%; padding: 0px 0 7px;max-width: 100%;" src="{$tconfig.tsite_images}{$THEME}/progress.jpg"></div>
                    <span class="last-star">{$totrating[1]}</span>
                  </div>
                </div>
              </div>
              <div class="user-comment-list">
                {if $db_rating_from|@count gt 0}
                <ul>
                  {section name=i loop=$db_rating_from}
                  <li>
                    <div class="user-img2"><img src="{$db_rating_from[i].img}" alt="" /></div>
                    <div class="user-profile-de">
                   
                      <h2><div style="float:left;" class="user-profile-de-head1">Rating : {$db_rating_from[i].iRate}</div>
                      <div style="margin:0px; float:left;" class="user-profile-de-head2">  <span style="float:left; margin-left: 10px;margin-top:5px;display: block; width: 65px; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 0;">
                        <span style="display: block; float:none; width: {$db_rating_from[i].rating_width}%; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 -13px;"></span>
                    </span></div>
                    
                    <div style="color:#414141; float:right; font-size:13px;" class="user-profile-de-head3">{$generalobj->DateTimeFormat($db_rating_from[i].dAddedDate)} </div>
                      
                      </h2>
                      <p><strong>From {$db_rating_from[i].vFirstName} {$db_rating_from[i].vLastName} :</strong> {$db_rating_from[i].tFeedback}. </p>
                    </div>
                  </li>
                  {/section}
                </ul>
                {/if}  
              </div>
            </div>
          </div>
          <div style="clear:both;"></div>
        </div>
        <div style="clear:both;"></div>
      </div>
      <div style="clear:both;"></div>
    </div>
{literal}
  <script>
    var	site_url = '{/literal}{$tconfig.tsite_url}{literal}';
    $("#showmsg").click(function() {
      $("#messagefrmdiv").toggle('slow');
    });
    
    $("#hidemsg").click(function() {
      $("#messagefrmdiv").toggle('slow');
    });
    
    
    
    function sendmessage_valid(){     
      resp = jQuery("#frmmessage").validationEngine('validate');
        
    		if(resp == true){ 
    		  $("#sendbutton").html('Wait...');      
    		  var tMessage = document.getElementById("tMessage").value;      
          var vSubject = '';
          var iToMemberId = document.getElementById("iToMemberId").value;
          var iFromMemberId = document.getElementById("iFromMemberId").value;
          var action = document.getElementById("mode").value;
          var type = document.getElementById("type").value;
              
         
    			var request = $.ajax({
      	  type: "POST",
      	  url: site_url+'index.php?file=c-messages_a',   	  
      	  data: "tMessage="+tMessage+"&vSubject="+vSubject+"&iToMemberId="+iToMemberId+"&iFromMemberId="+iFromMemberId+"&action="+action+"&type="+type, 	  
      	  
      	  success: function(data) { 
            $("#sendbutton").html('Send');
            $("#tMessage").val('');
      			if(data == 1)
      			{
              showNotification({type : 'success', message: '{/literal}{$smarty.const.LBL_YOUR_MSG_SENT_SUCC}{literal}'});
              $("#messagefrmdiv").toggle('slow');          
            }else{
              showNotification({type : 'error', message: '{/literal}{$smarty.const.LBL_SOME_WRONG_TRY_AGAIN}{literal}'});
              $("#messagefrmdiv").toggle('slow');  
            }
                            
      		}
      	});
      	
      	request.fail(function(jqXHR, textStatus) {
      	  $("#sendbutton").html('Send');
      	  $("#tMessage").val('');
          alert( "Request failed: " + textStatus ); 
      	});
    		
    		}else{
    			return false;
    		}	   
      	
    }
  </script>
{/literal}
{if $megop eq 1}
{literal}
  <script>
        $("#messagefrmdiv").toggle('slow');
  </script>
{/literal}
{/if}