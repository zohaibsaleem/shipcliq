<div class="about-page">    
  <h2>Registration<span> Confirmation</span></h2>
  <div class="right-about-part">
    <h1>Registration<span> Confirmation</span></h1>     
      <p>You have successfully registered with us. We have sent an email to your registered email account. Please click on the link in the email to activate your account. If you have not received the email please check your spam folder.</p>   
  </div>
  {include file="left.tpl"}
  <div style="clear:both;"></div>
</div>