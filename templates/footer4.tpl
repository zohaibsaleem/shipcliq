<div id="footer">
	<div class="footer-inner">
		<div class="footer-inner-cont">
			<div class="footer-top-part">
				<div class="subscribe">
					<form name="frmnewsletter" id="frmnewsletter" method="post" action="">
						<h2>{$smarty.const.LBL_SUBSRIBE} <span>{$smarty.const.LBL_NEWSLETTERS}</span></h2>
						<b>
							<input name="vNewsletterEmail" id="vNewsletterEmail" type="text" class="validate[required,custom[email]] newsletter-ho" placeholder="{$smarty.const.LBL_ENTER_EMAIL}" />
							<input value="{$smarty.const.LBL_SUBMIT}" id="newletter_subscribe" name="" type="button" class="newsletter-but" onClick="checknewsletter();" placeholder="Enter E-mail Address"/>
							<input value="{$smarty.const.LBL_WAIT}" id="newletter_loading" style="display:none;" type="button" class="newsletter-but"/>
						</b>
					</form>
					<div class="fb-page">
						<!--<img src="{$tconfig.tsite_images}home/{$THEME}/fb-page.png" alt="" />-->
						<!--<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FeSiteWorld&amp;width=338&amp;height=116&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=true&amp;appId=1404686089767190" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:338px; height:116px;" allowTransparency="true"></iframe>-->
						<!--<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FeSiteWorld&amp;width=338&amp;height=116&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=true&amp;appId=645233418900619" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:338px; height:116px;" allowTransparency="true"></iframe>-->
						<!--<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FeSiteWorld&amp;width=338&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=true&amp;appId=645233418900619" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100%; height:100%;" allowTransparency="true"></iframe>-->
						<!--<img src="{$tconfig.tsite_images}home/{$THEME}/fb-page.png" alt="" />-->
						<iframe src="//www.facebook.com/plugins/likebox.php?href={$FACEBOOK_LINK}&amp;width=338&amp;height=300&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=true&amp;appId=1404686089767190" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100%; height:100%;" allowTransparency="true"></iframe>
					</div>
				</div>

				<div class="fo-meniu">
					<!--<h2>{$smarty.const.LBL_USING} {$smarty.const.LBL_BALTIC_CAR}</h2>-->
					<h2>{$smarty.const.LBL_USING} {$smarty.const.LBL_CAR_SHARING_WEBSITE}</h2>
					<ul>
						<!--  <li><a href="{$tconfig.tsite_url}page/{$db_page[0].iPageId}/{$generalobj->getPageUrlName($db_page[0].iPageId)}" {if $iPageId eq $db_page[0].iPageId}class="active"{/if}>{$generalobj->getPageTitle($db_page[0].iPageId)}</a></li>
							<li><a href="{$tconfig.tsite_url}page/{$db_page[1].iPageId}/{$generalobj->getPageUrlName($db_page[1].iPageId)}" {if $iPageId eq $db_page[1].iPageId}class="active"{/if}>{$generalobj->getPageTitle($db_page[1].iPageId)}</a></li>
						<!--<li><a href="#">Rating</a></li> -->

						{section name=i loop=$db_pages_1}
						<li><a href="{$tconfig.tsite_url}page/{$db_pages_1[$smarty.section.i.index].iPageId}/{$generalobj->getPageUrlName({$db_pages_1[$smarty.section.i.index].iPageId})}" >{$generalobj->getPageTitle({$db_pages_1[$smarty.section.i.index].iPageId})}</a></li>
						{/section}
						<!--hide old faq<li><a href="{$tconfig.tsite_url}faqs" {if $script eq 'faqs'}class="active"{/if}>{$smarty.const.LBL_FAQ}</a></li>-->
						<li><a href="{$tconfig.tsite_url}Member-Stories" {if $script eq 'member_stories'}class="active"{/if}>{$smarty.const.LBL_MEMBER_STORY}</a></li>
						<!--<li><a href="{$tconfig.tsite_url}page/3/{$generalobj->getPageUrlName(3)}" {if $iPageId eq 3}class="active"{/if}>{$generalobj->getPageTitle(3)}</a></li>-->
					</ul>
				</div>
				<div class="fo-meniu2">
					<h2>{$smarty.const.LBL_OUR_COMPANY}</h2>
					<ul>
						<!-- <li><a href="{$tconfig.tsite_url}page/4/{$generalobj->getPageUrlName(4)}" {if $iPageId eq 4}class="active"{/if}>{$generalobj->getPageTitle(4)}</a></li>-->
						<!--hide old part of footer
						{section name=i loop=$db_pages_2}
						<li><a href="{$tconfig.tsite_url}page/{$db_pages_2[$smarty.section.i.index].iPageId}/{$generalobj->getPageUrlName({$db_pages_2[$smarty.section.i.index].iPageId})}" >{$generalobj->getPageTitle({$db_pages_2[$smarty.section.i.index].iPageId})}</a></li>
						{/section}
						-->
						<li><a href="/page/5/how-it-works">How It Works</a></li>
						<li><a href="/page/7/trust-safety">Trust & Safety</a></li>
						<li><a href="{$tconfig.tsite_url}contactus" {if $script eq 'contactus'}class="active"{/if}>{$smarty.const.LBL_CONTACT_US1}</a></li>
						<li><a href="{$tconfig.tsite_url}blog" target="_blank">{$smarty.const.LBL_BLOGS}</a></li>
						<!--<li><a href="{$tconfig.tsite_url}page/5/{$generalobj->getPageUrlName(5)}" {if $iPageId eq 5}class="active"{/if}>{$generalobj->getPageTitle(5)}</a></li>
							<li><a href="{$tconfig.tsite_url}page/6/{$generalobj->getPageUrlName(6)}" {if $iPageId eq 6}class="active"{/if}>{$generalobj->getPageTitle(6)}</a></li>
						<li><a href="{$tconfig.tsite_url}page/7/{$generalobj->getPageUrlName(7)}" {if $iPageId eq 7}class="active"{/if}>{$generalobj->getPageTitle(7)}</a></li>-->
					</ul>
				</div>
			</div>
			<div class="footer-bot-part">
				<p>
					{$COPY_RIGHT_TEXT}<br />
				<!-- {$smarty.const.LBL_SITE_DESIGN_DEVELOP} : <a href="http://www.esiteworld.com/" target="_blank" class="site-n">eSiteWorld.com</a> --></p>
				<span>
					<h3>{$smarty.const.LBL_PAYMENT_ACCEPT}</h3>
					<em>
						<!--<a href="javascript:void(0);">--><img src="{$tconfig.tsite_images}pay-pal.jpg" alt="" /></a>
						<!--<a href="javascript:void(0);">--><img src="{$tconfig.tsite_images}visa.jpg" alt="" /></a>
						<!--<a href="javascript:void(0);">--><img src="{$tconfig.tsite_images}master-card.jpg" alt="" /></a>
					</em>
				</span>
			</div>
			<div style="clear:both;"></div>
		</div>
	</div>
</div>
{literal}
<script>
	$('#frmnewsletter').bind('keydown',function(e){
		if(e.which == 13){
			checknewsletter(); return false;
		}
	});
</script>
{/literal}
