<script type="text/javascript" src="{$tconfig.tsite_url}rrb/zebra_datepicker.src.js"></script>
<link rel="stylesheet" href="{$tconfig.tsite_url}rrb/default.css" type="text/css">
<!--// top part //-->
<div class="top-part">
  <div class="logo"><a href="{$tconfig.tsite_url}"><img src="{$logodis}" alt="" title="logo" /></a></div>
  <div  class="top-right-part"> {if $sess_iMemberId eq ''}
    <div class="ho-right-top">
      <div class="ride-f">
        <ul>
          <li class="home-ride-find"><img src="{$tconfig.tsite_images}home/{$THEME}/ride-left-img.jpg" alt="" class="ride-img-left" /><a href="{$tconfig.tsite_url}find-ride" {if $sess_lang eq 'LV' or $sess_lang eq 'LT'} style="font-size:11px;"{/if}>{$smarty.const.LBL_FIND_A_RIDE} </a>
          <img src="{$tconfig.tsite_images}home/{$THEME}/ride-right-img.jpg" alt="" class="ride-img-right" /></li>
          <li class="home-ride-ofer"><img src="{$tconfig.tsite_images}home/{$THEME}/ride-left-img.jpg" alt="" class="ride-img-left" /><a href="{$tconfig.tsite_url}offer-ride" {if $sess_lang eq 'LV' or $sess_lang eq 'LT'} style="font-size:11px;"{/if}>{$smarty.const.LBL_OFFER_RIDE}</a>
          <img src="{$tconfig.tsite_images}home/{$THEME}/ride-right-img.jpg" class="ride-img-right" alt="" />
          </li> <!--  Hemali
		  <li class="home-ride-find"><img src="{$tconfig.tsite_images}home/{$THEME}/ride-left-img.jpg" alt="" class="ride-img-left" /><a href="#">{$smarty.const.LBL_FIND_A_RIDE} </a>
          <img src="{$tconfig.tsite_images}home/{$THEME}/ride-right-img.jpg" alt="" class="ride-img-right" /></li>
          <li class="home-ride-ofer"><img src="{$tconfig.tsite_images}home/{$THEME}/ride-left-img.jpg" alt="" class="ride-img-left" /><a href="#" {if $sess_lang eq 'LV' or $sess_lang eq 'LT'} style="font-size:11px;"{/if}>{$smarty.const.LBL_OFFER_RIDE}</a>
          <img src="{$tconfig.tsite_images}home/{$THEME}/ride-right-img.jpg" class="ride-img-right" alt="" />
          </li>  -->
        </ul>
      </div>
      <!--<div class="in-ride"><a href="#">International Rides</a></div>-->
      <!--<div class="in-ride-new"><span>{if $smarty.cookies.ride_locations_type eq 'international'}<a href="javascript:void(0);" onClick="chnage_ride_types('local');">{$smarty.const.LBL_VIEW_LOCAL_RIDES}</a> {else}<a href="javascript:void(0);" onClick="chnage_ride_types('international');" style="color:#0094C8;">{$smarty.const.LBL_VIEW_INTERNATIONAL_RIDES}</a>{/if}</span></div>-->
    </div>
    {else}
    <div class="ho-right-top2">
      <!--<div class="ride-f-in">
        <ul>
          <li><a href="{$tconfig.tsite_url}find-ride" {if $sess_lang eq 'LV' or $sess_lang eq 'LT'} style="font-size:11px;"{/if}>{$smarty.const.LBL_FIND_A_RIDE}</a></li>
          <li><a href="{$tconfig.tsite_url}offer-ride" {if $sess_lang eq 'LV' or $sess_lang eq 'LT'} style="font-size:11px;"{/if}>{$smarty.const.LBL_OFFER_RIDE}</a></li>
        </ul>
      </div>-->
      <div class="top-div-iocn">
        <div class="top-icons">
          <ul>
            {if $smarty.session.tot_unread gte 1}
            <li><a href="{$tconfig.tsite_url}received-messages"><strong>{$smarty.session.tot_unread}</strong><img src="{$tconfig.tsite_images}{$THEME}/message_notification_new.png"></a></li>
            {else}
            <li><a href="{$tconfig.tsite_url}received-messages"><strong>&nbsp;</strong><img src="{$tconfig.tsite_images}{$THEME}/message_notification.png"></a></li>
            {/if}        
            {if $smarty.session.tot_feat_booking gte 1}
            <li><a href="{$tconfig.tsite_url}my-bookings"><strong>{$smarty.session.tot_feat_booking}</strong><img src="{$tconfig.tsite_images}{$THEME}/booking_notification_new.png"></a></li>
            {else}
            <li><a href="{$tconfig.tsite_url}my-bookings"><strong>&nbsp;</strong><img src="{$tconfig.tsite_images}{$THEME}/booking_notification.png"></a></li>
            {/if}
          </ul>
        </div>
        <div class="user-part-main">
          <div class="user-part"> <img src="{$member_image}" alt="" title="logo"/>
            <h2>{$sess_vFirstName}</h2>
            <div class="drop-menu">
              <p><a href="{$tconfig.tsite_url}my-account">{$smarty.const.LBL_MY_ACCOUNT} <img src="{$tconfig.tsite_images}{$THEME}/white-point.png" alt="" /></a></p>
              <ul class="sub-menu">
                <li><a href="{$tconfig.tsite_url}my-account">{$smarty.const.LBL_DASHBOARD}</a></li>
                <!-- <li><a href="{$tconfig.tsite_url}List-Rides-Offer">{$smarty.const.LBL_RIDES_OFFERED1}</a></li>
                <li><a href="{$tconfig.tsite_url}Email-Alerts">{$smarty.const.LBL_EMAIL_ALERTS}</a></li>
                <li><a href="{$tconfig.tsite_url}received-messages">{$smarty.const.LBL_MESSAGES}</a></li>
                <li><a href="{$tconfig.tsite_url}leave-Rating">{$smarty.const.LBL_RATINGS}</a></li>
                <li><a href="{$tconfig.tsite_url}personal-information">{$smarty.const.LBL_PROFILE}</a></li>
                <li><a href="{$tconfig.tsite_url}logout">{$smarty.const.LBL_LOGOUT}</a></li>
				Hemali -->
              </ul>
            </div>
            <p><a href="{$tconfig.tsite_url}logout">{$smarty.const.LBL_LOGOUT}</a></p>
            <div style="clear:both;"></div>
          </div>
        </div>
      </div>
      <!--<div class="in-ride"><a href="#">International Rides</a></div>-->
      <!-- <div class="in-ride-new"><span>{if $smarty.cookies.ride_locations_type eq 'international'}<a href="javascript:void(0);" onClick="chnage_ride_types('local');">{$smarty.const.LBL_VIEW_LOCAL_RIDES}</a> {else}<a href="javascript:void(0);" onClick="chnage_ride_types('international');" style="color:#0094C8;">{$smarty.const.LBL_VIEW_INTERNATIONAL_RIDES}</a>{/if}</span></div> -->
     
    </div>
    {/if}
    <!--navigation-res-->
    <div class="navigation"><span><a class="toggleMenu" href="#"><img src="{$tconfig.tsite_images}home/{$THEME}/menu-bg1.png"></a></span>
      <ul class="nav">
        <li class="test"><a href="{$tconfig.tsite_url}" {if $script eq 'home'}class="active"{/if} {if $sess_lang eq 'LV' or $sess_lang eq 'LT'} style="font-size:13px;"{/if}>{$smarty.const.LBL_HOME}</a></li>
        <!-- <li><a href="{$tconfig.tsite_url}find-ride" {if $script eq 'find_ride'}class="active"{/if} {if $sess_lang eq 'LV' or $sess_lang eq 'LT'} style="font-size:13px;"{/if}>{$smarty.const.LBL_FIND_RIDE}</a></li>
        <li><a href="{$tconfig.tsite_url}offer-ride" {if $script eq 'offer_ride'}class="active"{/if} {if $sess_lang eq 'LV' or $sess_lang eq 'LT'} style="font-size:13px;"{/if}>{$smarty.const.LBL_OFFER_RIDE}</a></li>
        <!--<li><a href="{$tconfig.tsite_url}blog">{$smarty.const.LBL_BLOGS}</a></li>
        {if $sess_iMemberId eq ''}
        <li><a href="{$tconfig.tsite_url}sign-in" {if $script eq 'login'}class="active"{/if} {if $sess_lang eq 'LV' or $sess_lang eq 'LT'} style="font-size:13px;"{/if}>{$smarty.const.LBL_LOG_IN}</a></li>
        {else}
        <li><a href="http://blog.blablacarscript.com/" {if $sess_lang eq 'LV' or $sess_lang eq 'LT'} style="font-size:13px;"{/if}>{$smarty.const.LBL_BLOGS}</a></li>
        {/if} Hemali -->
		<li><a href="#">{$smarty.const.LBL_FIND_RIDE}</a></li>
        <li><a href="#">{$smarty.const.LBL_OFFER_RIDE}</a></li>
        <li><a href="{$tconfig.tsite_url}aboutus" {if $script eq 'page' and $iPageId eq '1'}class="active"{/if} {if $sess_lang eq 'LV' or $sess_lang eq 'LT'} style="font-size:13px;"{/if}>{$smarty.const.LBL_ABOUT_US} </a> </li>
        <li class="last"><a href="{$tconfig.tsite_url}contactus" class="last {if $script eq 'contactus'}active{/if}" {if $sess_lang eq 'LV' or $sess_lang eq 'LT'} style="font-size:13px;"{/if}>{$smarty.const.LBL_CONTACT_US}</a></li>
      </ul>
    </div>
    <!--navigation-res end-->
  </div>
</div>
<!--// top part end //-->
<!--// top header //-->
<div class="header-part">
  <div class="header-in-top">
    <h2><strong><b><img src="{$tconfig.tsite_images}home/{$THEME}/head-bg1.png"></b><span>{$smarty.const.LBL_BANNER_WELCOME}</span></strong></h2>
    <h3><strong><span>{$smarty.const.LBL_BANNER_BALTIC_SYSTEM}</span><b><img src="{$tconfig.tsite_images}home/{$THEME}/head-bg4.png"></b></strong></h3>
    <!--<h4><strong><b><img src="{$tconfig.tsite_images}home/{$THEME}/head-bg5.png"></b> <span>
      <p>{$smarty.const.LBL_BANNER_DESC}</p>
      </span> <b><img src="{$tconfig.tsite_images}home/{$THEME}/head-bg7.png"></b></strong></h4> -->
  </div>
  <div class="header-in-bot">
    <form name="home_search" id="home_search" method="post" action="{$tconfig.tsite_url}index.php?file=c-ride_list">
      <h2><b>{$smarty.const.LBL_FIND_RIDE_TO_JOIN_LIFT}</b></h2>
      <span>
      <input name="search" id="search" type="hidden" value="place">
      <input name="From_lat_long" id="From_lat_long" type="hidden" value="">
      <input name="To_lat_long" id="To_lat_long" type="hidden" value="">
      <input name="From" id="From" type="text" placeholder="{$smarty.const.LBL_FROM}" class="validate[required] he-input" value=""/>
      <input placeholder="{$smarty.const.LBL_TO}" name="To" id="To" type="text" class="validate[required] he-input" value=""/>
      <input name="searchdate" id="searchdate" type="text" class="he-input-cal" value="" placeholder="{$smarty.const.LBL_DATE}" />
      <input name="searchrides" type="button" class="search-but" value="{$smarty.const.LBL_SEARCH}" onClick="search_now();"/>
      </span>
    </form>
  </div>
</div>
<!--// top part end //-->
{literal}
<script>
    $('#searchdate').Zebra_DatePicker({
      direction: 0
    });
    
    function change_lang(lang){
      window.location = '{/literal}{$tconfig.tsite_url}{literal}index.php?lang='+lang; 
    }
    
    function change_currency(currency){
      window.location = '{/literal}{$tconfig.tsite_url}{literal}index.php?currency='+currency; 
    }
    
    function setCookie(cname,cvalue,exdays)
    {
      var d = new Date();
      d.setTime(d.getTime()+(exdays*24*60*60*1000));
      var expires = "expires="+d.toGMTString();
      document.cookie = cname + "=" + cvalue + "; " + expires;
    } 
    
    function getCookie(cname)
    {
      var name = cname + "=";
      var ca = document.cookie.split(';');
      for(var i=0; i<ca.length; i++) 
        {
        var c = ca[i].trim();
        if (c.indexOf(name)==0) return c.substring(name.length,c.length);
        }
      return "";
    }
    
    function showPosition(position)
    { 
      setCookie('ride_locations_type', 'local', 1);
      setCookie('userlatitude', position.coords.latitude, 1);
      setCookie('userlongitude', position.coords.longitude, 1); 
      if(getCookie('country_code') == ''){
        var request = $.ajax({  
      	  type: "POST",
      	  url: site_url+"createcookie.php",  
      	  data: "", 	  
      	  
      	  success: function(data) {    		  
            document.location.reload(true);      			
      		}
      	});
      } 
    }
    
    function getLocation()
    {        
      if (navigator.geolocation)
      {
        navigator.geolocation.getCurrentPosition(showPosition);
        //window.location = site_url;
      }
      else{x.innerHTML="Geolocation is not supported by this browser.";}
    }
    
    function checkCookie()
    {
      var userlatitude = getCookie("userlatitude");
      var userlongitude = getCookie("userlongitude");
      if (userlatitude != "" && userlongitude != '')
      {
      }else{  
        getLocation();
      }
    } 
    
    function chnage_ride_types(type){
      if(type == 'international'){
        document.cookie = "country_code=; expires=Thu, 01 Jan 1970 00:00:00 GMT";
        setCookie('ride_locations_type', 'international', 1);
        window.location = site_url;
      }else{
        setCookie('ride_locations_type', 'local', 1);
        getLocation();
      }
    }      
  </script>
{/literal}
{if $smarty.cookies.ride_locations_type eq ''}
  {literal}
<script>      
      /*var ride_locations_type = getCookie("ride_locations_type");
      if(ride_locations_type == ''){ 
        chnage_ride_types('international');
      } */
    </script>
{/literal}
{/if} 