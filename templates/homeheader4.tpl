<div id="top-part" class="{if $sess_iMemberId neq ''}login-part{/if}">
	<div class="top-part-inner">
		<div class="top-select-part">
			<div class="top-select-part-inner">
				<div class="login-b-login">
					<div class="top-div-iocn"> {if $sess_iMemberId neq ''}
						<div class="top-icons">
							<ul>
								{if $smarty.session.tot_unread gte 1}
								<li><a href="{$tconfig.tsite_url}received-messages"><strong>{$smarty.session.tot_unread}</strong><img src="{$tconfig.tsite_images}{$THEME}/message_notification_new.png"></a></li>
								{else}
								<li><a href="{$tconfig.tsite_url}received-messages"><strong>&nbsp;</strong><img src="{$tconfig.tsite_images}{$THEME}/message_notification.png"></a></li>
								{/if}
								{if $smarty.session.tot_feat_booking gte 1}
								<li><a href="{$tconfig.tsite_url}my-bookings"><strong>{$smarty.session.tot_feat_booking}</strong><img src="{$tconfig.tsite_images}{$THEME}/booking_notification_new.png"></a></li>
								{else}
								<li><a href="{$tconfig.tsite_url}my-bookings"><strong>&nbsp;</strong><img src="{$tconfig.tsite_images}{$THEME}/booking_notification.png"></a></li>
								{/if}
							</ul>
						</div>
						<div class="user-part-main">
							<div class="user-part"> <img src="{$member_image}" alt="" title="logo"/>
								<h2>{$sess_vFirstName}</h2>
								<div class="drop-menu">
									<p><a href="{$tconfig.tsite_url}my-account">{$smarty.const.LBL_MY_ACCOUNT} <img src="{$tconfig.tsite_images}{$THEME}/white-point.png" alt="" /></a></p>
									<ul class="sub-menu">
										<li><a href="{$tconfig.tsite_url}my-account">{$smarty.const.LBL_DASHBOARD}</a></li>
										{if $smarty.const.PAYMENT_OPTION neq 'Contact'}
										<li><a href="{$tconfig.tsite_url}my-bookings">{$smarty.const.LBL_MY_BOOKINGS}</a></li>
										{/if}
										<li><a href="{$tconfig.tsite_url}List-Rides-Offer">{$smarty.const.LBL_RIDES_OFFERED1}</a></li>
										<li><a href="{$tconfig.tsite_url}received-messages">{$smarty.const.LBL_MESSAGES}</a></li>
										<li><a href="{$tconfig.tsite_url}Email-Alerts">{$smarty.const.LBL_EMAIL_ALERTS}</a></li>
										<li><a href="{$tconfig.tsite_url}leave-Rating">{$smarty.const.LBL_RATINGS}</a></li>
										<li><a href="{$tconfig.tsite_url}personal-information">{$smarty.const.LBL_PROFILE}</a></li>
										<li><a href="{$tconfig.tsite_url}index.php?file=m-money_detail&pagetype=money&type=available">{$smarty.const.LBL_PAYMENT}</a></li>
										<li><a href="{$tconfig.tsite_url}logout">{$smarty.const.LBL_LOGOUT}</a></li>
									</ul>
								</div>
								<!-- <p><a href="{$tconfig.tsite_url}logout">{$smarty.const.LBL_LOGOUT}</a></p> -->
								<div style="clear:both;"></div>
							</div>
						</div>
					{/if} </div>
				</div>
				<div class="select-box">
					<select name="" class="custom-select custom-select01" onchange="chnage_ride_types(this.value);">
						<option value="international" {if $smarty.cookies.ride_locations_type eq 'international'}selected{/if}>{$smarty.const.LBL_VIEW_INTERNATIONAL_RIDES}</option>
						<option value="local" {if $smarty.cookies.ride_locations_type eq 'local'}selected{/if} >{$smarty.const.LBL_VIEW_LOCAL_RIDES}</option>
					</select>
					{if $db_lng_mst|@count gt 1}
					<select name="sess_language" id="sess_language" onChange="change_lang(this.value);" class="custom-select se-in">

						{section name="lngrb" loop=$db_lng_mst}

						<option value="{$db_lng_mst[lngrb].vCode}" {if $sess_lang eq $db_lng_mst[lngrb].vCode} selected {/if}>{$db_lng_mst[lngrb].vTitle}</option>

						{/section}

					</select>
					{else}
					{section name="lngrb" loop=$db_lng_mst}
						<input type="hidden" name="sess_language" id="sess_language" value="{$db_lng_mst[lngrb].vCode}">
					{/section}

					{/if}
					{if $db_curr_mst|@count gt 1}
					<select name="sess_price_ratio" id="sess_price_ratio" onChange="change_currency(this.value);" class="custom-select se-in">

						{section name="currrb" loop=$db_curr_mst}

						<option value="{$db_curr_mst[currrb].vName}" {if $sess_price_ratio eq $db_curr_mst[currrb].vName} selected {/if}>{$db_curr_mst[currrb].vName}</option>

						{/section}
					</select>
					{else}
					{section name="lngrb" loop=$db_curr_mst}
						<input type="hidden" name="sess_price_ratio" id="sess_price_ratio" value="{$db_curr_mst[lngrb].vName}">
					{/section}
					{/if}
				{if $sess_iMemberId eq ''} <span><a href="{$tconfig.tsite_url}sign-in">{$smarty.const.LBL_LOG_IN}</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="{$tconfig.tsite_url}sign-up">{$smarty.const.LBL_SIGN_UP}</a></span> {else} </div>
			{/if} </div>
		</div>
		<div class="top-logo-menu-part">
			<div class="logo"> <a href="{$tconfig.tsite_url}"> <img src="{$tconfig['tsite_upload_site_logo']}1_{$SITE_LOGO}" alt="" title="logo" />
				<!--<img src="{$tconfig.tsite_images}home/{$THEME}/logo.png" alt="" />-->
			</a> </div>
			<div class="menu-part">
				<!--navigation-res-->
				<div class="navigation"><span><a class="toggleMenu" href="#"><img src="{$tconfig.tsite_images}home/{$THEME}/menu-bg1.png" alt=""></a></span>
					<ul class="nav">
						<!--hide home <li class="test"><a href="{$tconfig.tsite_url}" {if $script eq 'home4'}class="active"{/if}>{$smarty.const.LBL_HOME}</a></li>-->
						<li><a href="/page/5/how-it-works">How It Works</a></li>
						<li><a href="/page/6/faq">FAQ</a></li>
						<li><a href="{$tconfig.tsite_url}index.php?file=c-ride_list&search=simple" {if $script eq 'ride_list'}class="active"{/if}>{$smarty.const.LBL_FIND_RIDE}</a></li>
						<!--<li><a href="{$tconfig.tsite_url}find-ride" {if $script eq 'find_ride'}class="active"{/if}>{$smarty.const.LBL_FIND_RIDE}</a></li>    -->
						<!--hide blog<li><a href="{$tconfig.tsite_url}blog">{$smarty.const.LBL_BLOGS}</a></li>-->
						{section name=i loop=$db_pages_1}
						{if $db_pages_1[$smarty.section.i.index].iPageId eq '1'}
						<li> <a href="{$tconfig.tsite_url}page/{$db_pages_1[$smarty.section.i.index].iPageId}/{$generalobj->getPageUrlName({$db_pages_1[$smarty.section.i.index].iPageId})}" {if $script eq 'page' and $iPageId eq '1'} class="active"{/if}>{$generalobj->getPageTitle({$db_pages_1[$smarty.section.i.index].iPageId})} </a> </li>
						{/if}
						{/section}
						<!-- <li><a href="{$tconfig.tsite_url}aboutus" {if $script eq 'page' and $iPageId eq '1'}class="active"{/if} >{$smarty.const.LBL_ABOUT_US} </a> </li> -->
						<!--hide contact<li><a href="{$tconfig.tsite_url}contactus" {if $script eq 'contactus'}class="active"{/if}>{$smarty.const.LBL_CONTACT_US}</a></li>-->
						<li class="last"><a href="{$tconfig.tsite_url}offer-ride" {if $script eq 'offer_ride'}class="active"{/if}>{$smarty.const.LBL_OFFER_RIDE}</a></li>
					</ul>
				</div>
				<!--<script type="text/javascript" src="js/script.js"></script>-->
				<!--navigation-res end-->
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
	<div style="clear:both;"></div>
</div>
</div>
{literal}
<script>
	$(document).ready(function(){
		$('#searchdate').Zebra_DatePicker({
			direction: 1
		});
	});

    function change_lang(lang){
		window.location = '{/literal}{$tconfig.tsite_url}{literal}index.php?lang='+lang;
	}

    function change_currency(currency){
		window.location = '{/literal}{$tconfig.tsite_url}{literal}index.php?currency='+currency;
	}

    function setCookie(cname,cvalue,exdays)
    {
		var d = new Date();
		d.setTime(d.getTime()+(exdays*24*60*60*1000));
		var expires = "expires="+d.toGMTString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	}

    function getCookie(cname)
    {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++)
        {
			var c = ca[i].trim();
			if (c.indexOf(name)==0) return c.substring(name.length,c.length);
		}
		return "";
	}

    function showPosition(position)
    {
		//setCookie('ride_locations_type', 'local', 1);
		setCookie('userlatitude', position.coords.latitude, 1);
		setCookie('userlongitude', position.coords.longitude, 1);
		if(getCookie('country_code') == ''){
			var request = $.ajax({
				type: "POST",
				url: site_url+"createcookie.php",
				data: "",

				success: function(data) {
					document.location.reload(true);
				}
			});
		}
	}

    function getLocation()
    {
		if (navigator.geolocation)
		{
			navigator.geolocation.getCurrentPosition(showPosition);
			//window.location = site_url;
		}
		else{x.innerHTML="Geolocation is not supported by this browser.";}
	}

    function checkCookie()
    {
		var userlatitude = getCookie("userlatitude");
		var userlongitude = getCookie("userlongitude");
		if (userlatitude != "" && userlongitude != '')
		{
			}else{
			getLocation();
		}
	}

    function chnage_ride_types(type){
		if(type == 'international'){
			document.cookie = "country_code=; expires=Thu, 01 Jan 1970 00:00:00 GMT";
			setCookie('ride_locations_type', 'international', 1);
			window.location = site_url;
			}else{
			setCookie('ride_locations_type', 'local', 1);
			getLocation();
		}
	}
</script>
{/literal}
{if $smarty.cookies.ride_locations_type eq ''}
{literal}
<script>
	/*var ride_locations_type = getCookie("ride_locations_type");
		if(ride_locations_type == ''){
        chnage_ride_types('international');
	} */
</script>
{/literal}
{/if}
