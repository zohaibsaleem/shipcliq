<div id="main-top-part">
  <div class="main-top-part-inner"> <span> <b><a href="{$FACEBOOK_LINK}" target="_blank"> <img onmouseout="this.src='{$tconfig.tsite_images}home/{$THEME}/fb.png'" onmouseover="this.src='{$tconfig.tsite_images}home/{$THEME}/fb-hover.png'" onclick="return submitsearch(document.frmsearch);" src="{$tconfig.tsite_images}home/{$THEME}/fb.png" alt=""/> </a> <a href="{$TWITTER_LINK}" target="_blank"> <img onmouseout="this.src='{$tconfig.tsite_images}home/{$THEME}/twi.png'" onmouseover="this.src='{$tconfig.tsite_images}home/{$THEME}/twi-hover.png'" onclick="return submitsearch(document.frmsearch);" src="{$tconfig.tsite_images}home/{$THEME}/twi.png" alt=""/> </a> <a href="{$PINTEREST_LINK}" target="_blank"> <img onmouseout="this.src='{$tconfig.tsite_images}home/{$THEME}/p-icon.png'" onmouseover="this.src='{$tconfig.tsite_images}home/{$THEME}/p-icon-hover.png'" onclick="return submitsearch(document.frmsearch);" src="{$tconfig.tsite_images}home/{$THEME}/p-icon.png" alt=""/> </a> </b> {if $sess_iMemberId neq ''}
    <div class="log-out-div">
      <div class="top-icons">
        <ul>
          {if $smarty.session.tot_unread gte 1}
          <li><a href="{$tconfig.tsite_url}received-messages"><strong>{$smarty.session.tot_unread}</strong><img src="{$tconfig.tsite_images}{$THEME}/message_notification_new.png" title="{$smarty.const.LBL_MESSAGES}"></a></li>
          {else}
          <li><a href="{$tconfig.tsite_url}received-messages"><strong>&nbsp;</strong><img src="{$tconfig.tsite_images}{$THEME}/message_notification.png" title="{$smarty.const.LBL_MESSAGES}"></a></li>
          {/if}
          {if $smarty.const.PAYMENT_OPTION neq 'Contact'}
          {if $smarty.session.tot_feat_booking gte 1}
          <li><a href="{$tconfig.tsite_url}my-bookings"><strong>{$smarty.session.tot_feat_booking}</strong><img src="{$tconfig.tsite_images}{$THEME}/booking_notification_new.png" title="{$smarty.const.LBL_MY_BOOKINGS}"></a></li>
          {else}
          <li><a href="{$tconfig.tsite_url}my-bookings"><strong>&nbsp;</strong><img src="{$tconfig.tsite_images}{$THEME}/booking_notification.png" title="{$smarty.const.LBL_MY_BOOKINGS}"></a></li>
          {/if}
          {/if}
          <!-- HEMALI
			{if $smarty.session.tot_unread gte 1}
				<li><a href="#"><strong>{$smarty.session.tot_unread}</strong><img src="{$tconfig.tsite_images}{$THEME}/message_notification_new.png" title="{$smarty.const.LBL_MESSAGES}"></a></li>
            {else}
				<li><a href="#"><strong>&nbsp;</strong><img src="{$tconfig.tsite_images}{$THEME}/message_notification.png" title="{$smarty.const.LBL_MESSAGES}"></a></li>
            {/if}
            {if $smarty.const.PAYMENT_OPTION neq 'Contact'}
				{if $smarty.session.tot_feat_booking gte 1}
					<li><a href="#"><strong>{$smarty.session.tot_feat_booking}</strong><img src="{$tconfig.tsite_images}{$THEME}/booking_notification_new.png" title="{$smarty.const.LBL_MY_BOOKINGS}"></a></li>
				{else}
					<li><a href="#"><strong>&nbsp;</strong><img src="{$tconfig.tsite_images}{$THEME}/booking_notification.png" title="{$smarty.const.LBL_MY_BOOKINGS}"></a></li>
				{/if}
            {/if}-->
        </ul>
      </div>
      <div class="user-part-main">
        <div class="user-part"> <img src="{$member_image}" alt="" title="logo"/>
          <h2>{$sess_vFirstName}</h2>
          <div class="drop-menu">
            <p><a href="{$tconfig.tsite_url}my-account">{$smarty.const.LBL_MY_ACCOUNT} <img src="{$tconfig.tsite_images}{$THEME}/white-point.png" alt="" /></a></p>
            <ul class="sub-menu">
              <li><a href="{$tconfig.tsite_url}my-account">{$smarty.const.LBL_DASHBOARD}</a></li>
              {if $smarty.const.PAYMENT_OPTION neq 'Contact'}
              <li><a href="{$tconfig.tsite_url}my-bookings">{$smarty.const.LBL_MY_BOOKINGS}</a></li>
              {/if}
              <li><a href="{$tconfig.tsite_url}List-Rides-Offer">{$smarty.const.LBL_RIDES_OFFERED1}</a></li>
              <li><a href="{$tconfig.tsite_url}received-messages">{$smarty.const.LBL_MESSAGES}</a></li>
              <li><a href="{$tconfig.tsite_url}Email-Alerts">{$smarty.const.LBL_EMAIL_ALERTS}</a></li>
              <li><a href="{$tconfig.tsite_url}leave-Rating">{$smarty.const.LBL_RATINGS}</a></li>
              <li><a href="{$tconfig.tsite_url}personal-information">{$smarty.const.LBL_PROFILE}</a></li>
              <li><a href="{$tconfig.tsite_url}index.php?file=m-money_detail&pagetype=money&type=available">{$smarty.const.LBL_PAYMENT}</a></li>
              <li><a href="{$tconfig.tsite_url}logout">{$smarty.const.LBL_LOGOUT}</a></li>
            </ul>
          </div>
          <!--<p><a href="{$tconfig.tsite_url}logout">{$smarty.const.LBL_LOGOUT}</a></p>-->
          <div style="clear:both;"></div>
        </div>
      </div>
    </div>
    {else} <span class="log-sing"><a style="color:#FFF;" href="{$tconfig.tsite_url}sign-in">{$smarty.const.LBL_LOG_IN}</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="{$tconfig.tsite_url}sign-up" style="color:#FFF;" >{$smarty.const.LBL_SIGN_UP}</a></span> {/if} <strong> {if $db_curr_mst|@count gt 1}
    <select name="sess_price_ratio" id="sess_price_ratio" onChange="change_currency(this.value);" class="lag">

        {section name="currrb" loop=$db_curr_mst}

      <option value="{$db_curr_mst[currrb].vName}" {if $sess_price_ratio eq $db_curr_mst[currrb].vName} selected {/if}>{$db_curr_mst[currrb].vName}</option>

        {/section}

      <!--<option value="USD" {if $sess_price_ratio eq 'USD'} selected {/if}>USD</option>
        <option value="GBP" {if $sess_price_ratio eq 'GBP'} selected {/if}>GBP</option>
        <option value="NOK" {if $sess_price_ratio eq 'NOK'} selected {/if}>NOK</option>
        <option value="SEK" {if $sess_price_ratio eq 'SEK'} selected {/if}>SEK</option>
        <option value="DKK" {if $sess_price_ratio eq 'DKK'} selected {/if}>DKK</option>
        <option value="PLN" {if $sess_price_ratio eq 'PLN'} selected {/if}>PLN</option>
        <option value="RUB" {if $sess_price_ratio eq 'RUB'} selected {/if}>RUB</option>
        <option value="EUR" {if $sess_price_ratio eq 'EUR'} selected {/if}>EUR</option>-->
    </select>
    {else}
    {section name="lngrb" loop=$db_curr_mst}
    <input type="hidden" name="sess_price_ratio" id="sess_price_ratio" value="{$db_curr_mst[lngrb].vName}">
    {/section}
    {/if}
    {if $db_lng_mst|@count gt 1}
    <select name="sess_language" id="sess_language" onChange="change_lang(this.value);" class="lag">

        {section name="lngrb" loop=$db_lng_mst}

      <option value="{$db_lng_mst[lngrb].vCode}" {if $sess_lang eq $db_lng_mst[lngrb].vCode} selected {/if}>{$db_lng_mst[lngrb].vTitle}</option>

        {/section}

      <!--<option value="EN" {if $sess_lang eq 'EN'} selected {/if}>{$smarty.const.LBL_ENGLISH}</option>
        <option value="DN" {if $sess_lang eq 'DN'} selected {/if}>{$smarty.const.LBL_DANISH}</option>
        <option value="FI" {if $sess_lang eq 'FI'} selected {/if}>{$smarty.const.LBL_FINISH}</option>
        <option value="FN" {if $sess_lang eq 'FN'} selected {/if}>{$smarty.const.LBL_FRENCH}</option>
        <option value="LV" {if $sess_lang eq 'LV'} selected {/if}>{$smarty.const.LBL_LATVIN}</option>
        <option value="EE" {if $sess_lang eq 'EE'} selected {/if}>{$smarty.const.LBL_ESTONIAN}</option>
        <option value="LT" {if $sess_lang eq 'LT'} selected {/if}>{$smarty.const.LBL_LITHUNIAN}</option>
        <option value="DE" {if $sess_lang eq 'DE'} selected {/if}>{$smarty.const.LBL_GERMAN}</option>
        <option value="NO" {if $sess_lang eq 'NO'} selected {/if}>{$smarty.const.LBL_NORWAY}</option>
        <option value="PO" {if $sess_lang eq 'PO'} selected {/if}>{$smarty.const.LBL_POLISH}</option>
        <option value="RS" {if $sess_lang eq 'RS'} selected {/if}>{$smarty.const.LBL_RUSSIAN}</option>
        <option value="ES" {if $sess_lang eq 'ES'} selected {/if}>{$smarty.const.LBL_SPANISH}</option>
        <option value="SW" {if $sess_lang eq 'SW'} selected {/if}>{$smarty.const.LBL_SWEDISH}</option>-->
    </select>
    {else}
    {section name="lngrb" loop=$db_lng_mst}
    <input type="hidden" name="sess_language" id="sess_language" value="{$db_lng_mst[lngrb].vCode}">
    {/section}

    {/if}
    <!--<div class="in-ride-new"><span>{if $smarty.cookies.ride_locations_type eq 'international'}<a href="javascript:void(0);" onClick="chnage_ride_types('local');">{$smarty.const.LBL_VIEW_LOCAL_RIDES}</a> {else}<a href="javascript:void(0);" onClick="chnage_ride_types('international');" style="color:#0094C8;">{$smarty.const.LBL_VIEW_INTERNATIONAL_RIDES}</a>{/if}</span></div>-->
    <select name="" class="lag1" onchange="chnage_ride_types(this.value);">
      <option value="international" {if $smarty.cookies.ride_locations_type eq 'international'}selected{/if}>{$smarty.const.LBL_VIEW_INTERNATIONAL_RIDES}</option>
      <option value="local" {if $smarty.cookies.ride_locations_type eq 'local'}selected{/if} >{$smarty.const.LBL_VIEW_LOCAL_RIDES}</option>
    </select>
    </strong> </span> </div>
</div>
