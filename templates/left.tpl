<div class="left-about-part">
  <div class="informaction">
    <h1>{$smarty.const.LBL_INFORMATION}</h1>
    <ul>
         {section name=i loop=$db_pages_1}
		     <li><a href="{$tconfig.tsite_url}page/{$db_pages_1[$smarty.section.i.index].iPageId}/{$generalobj->getPageUrlName({$db_pages_1[$smarty.section.i.index].iPageId})}" >{$generalobj->getPageTitle({$db_pages_1[$smarty.section.i.index].iPageId})}</a></li>
		  {/section} 
	</ul>
  </div>
  <div class="informaction">
    <h1>{$smarty.const.LBL_HELP}</h1>
    <ul>
      <li><a href="{$tconfig.tsite_url}contactus" {if $script eq 'contactus'}class="active"{/if}>{$smarty.const.LBL_CONTACT_US}</a></li>
      <li><a href="{$tconfig.tsite_url}faqs" {if $script eq 'faqs'}class="active"{/if}>{$smarty.const.LBL_FAQ}</a></li>
       
    </ul>
  </div>
</div>
