<div class="left-about-part">
  <div class="informaction">
    <h1>{$smarty.const.LBL_PROFILE_INFO}</h1>
    <ul>
      <li><a href="#" {if $script eq 'edit_profile'} class="active" {/if}>{$smarty.const.LBL_PERSONAL_INFO}</a></li>
      <li><a href="#" {if $script eq 'profile_photo'} class="active" {/if}>{$smarty.const.LBL_PROFILE_PHOTO}</a></li>
      <li><a href="#">{$smarty.const.LBL_SHARE_PREFERENCE}</a></li>
      <li><a href="#">{$smarty.const.LBL_MEMBER_VERIFICATION}</a></li>
      <li><a href="{$tconfig.tsite_url}" {if $script eq 'car_form' or $script eq 'car_details'} class="active" {/if}>{$smarty.const.LBL_CAR_DETAILS}</a></li>
      <li><a href="#">{$smarty.const.LBL_POST_ADDRESS}</a></li>
    </ul>
  </div>
  <div class="informaction">
    <h1>{$smarty.const.LBL_ACCOUNT}</h1>
    <ul>
      <li><a href="#">{$smarty.const.LBL_NOTIFICATION1}</a></li>
      <li><a href="#">{$smarty.const.LBL_SOCIAL_SHARING}</a></li>
      <li><a href="#">{$smarty.const.LBL_CHANGE_PASSWORD}</a></li>
      <li><a href="#">{$smarty.const.LBL_DELET_MY_ACCOUNT}</a></li>
    </ul>
  </div>
</div>
