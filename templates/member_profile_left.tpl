<div class="left-about-part">
  <div class="informaction">
    <h1>{$smarty.const.LBL_PROFILE_INFO}</h1>
    <ul>
      <li><a {if $script eq 'edit_profile'}class="active"{/if} href="{$tconfig.tsite_url}personal-information">{$smarty.const.LBL_PERSONAL_INFO}</a></li>
      <li><a {if $script eq 'profile_photo'}class="active"{/if} href="{$tconfig.tsite_url}profile-photo">{$smarty.const.LBL_PROFILE_PHOTO}</a></li>
      <!-- Hemali.. <li><a {if $script eq 'preferences'}class="active"{/if} href="{$tconfig.tsite_url}car-preferences">{$smarty.const.LBL_SHARE_PREFERENCE}</a></li> -->
      <li><a {if $script eq 'verification'}class="active"{/if} href="{$tconfig.tsite_url}Member-Verification">{$smarty.const.LBL_MEMBER_VERIFICATION}</a></li>
      <!-- Hemali .. <li><a {if $script eq 'car_details'}class="active"{/if} href="{$tconfig.tsite_url}car-details">{$smarty.const.LBL_CAR_DETAILS}</a></li> -->
    </ul>
  </div>
  <div class="informaction">
    <h1>{$smarty.const.LBL_ACCOUNT}</h1>
    <ul>
      <li><a {if $script eq 'notification'}class="active"{/if} href="{$tconfig.tsite_url}Notification">{$smarty.const.LBL_NOTIFICATION1}</a></li>
      <!--<li><a {if $script eq 'socialsharing'}class="active"{/if} href="{$tconfig.tsite_url}social-sharrings">{$smarty.const.LBL_SOCIAL_SHARING}</a></li>-->
      <li><a {if $script eq 'changepassword'}class="active"{/if} href="{$tconfig.tsite_url}Change-Password" >{$smarty.const.LBL_CHANGE_PASSWORD}</a></li>
      <li><a href="{$tconfig.tsite_url}index.php?file=m-delete_account" {if $script eq 'delete_account'} class="active" {/if}>{$smarty.const.LBL_DELET_MY_ACCOUNT}</a></li>
    </ul>
  </div>
</div>
