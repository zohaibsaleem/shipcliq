<div class="left-about-part">
  <div class="informaction">
    <h1>{$smarty.const.LBL_RATINGS}</h1>
    <ul>
      <li><a href="{$tconfig.tsite_url}leave-Rating" {if $script eq 'leaverating' or $script eq 'memberrating'} class="active" {/if}>{$smarty.const.LBL_LEAVE_RATING}</a></li>
      <li><a href="{$tconfig.tsite_url}Receive-Rating" {if $script eq 'receiverating'} class="active" {/if}>{$smarty.const.LBL_RATINGS} {$smarty.const.LBL_RECEIVED}</a></li>
      <li><a href="{$tconfig.tsite_url}Rating-Given" {if $script eq 'ratinggiven'} class="active" {/if}>{$smarty.const.LBL_RAGINGS_GIVEN}</a></li>
    </ul>
  </div>
</div>
