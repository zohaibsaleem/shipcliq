<script>
	$(document).ready(function(){
		$('.tabs-main ul').hide();
		$(".open-s").click(function(){
			
			$(".tabs-main-list").toggle('fast');
		});
	});
</script>

<div class="inner-submenu">
	<div class="tabs-main">
		<a href="javascript:void(0);" class="open-s"><img src="{$tconfig.tsite_images}inner-menu-icon.png" alt="" /></a>
		<ul class="tabs-main-list">
			<li><a {if $script eq 'dashboard'}class="active"{/if} href="{$tconfig.tsite_url}my-account">{$smarty.const.LBL_DASHBOARD}</a></li>
			{if $smarty.const.PAYMENT_OPTION neq 'Contact'}
			<li><a {if $script eq 'mybooking'}class="active"{/if} href="{$tconfig.tsite_url}my-bookings">{$smarty.const.LBL_MY_BOOKINGS}</a></li>
			{/if}
			<li><a {if $script eq 'list_rides_offer'}class="active"{/if} href="{$tconfig.tsite_url}List-Rides-Offer">{$smarty.const.LBL_RIDES_OFFERED1}</a></li>
			<li><a {if $script eq 'received_messages' or $script eq 'sent_messages' or $script eq 'messagedtl' or $script eq 'archived_messages'}class="active"{/if} href="{$tconfig.tsite_url}received-messages">{$smarty.const.LBL_MESSAGES}</a></li>
			<li><a {if $script eq 'member_alert'}class="active"{/if} href="{$tconfig.tsite_url}Email-Alerts">{$smarty.const.LBL_EMAIL_ALERTS}</a></li>
			<li><a {if $script eq 'leaverating' or $script eq 'memberrating' or $script eq 'receiverating' or $script eq 'ratinggiven'}class="active"{/if} href="{$tconfig.tsite_url}leave-Rating">{$smarty.const.LBL_RATINGS}</a></li>
			<li><a {if $script eq 'edit_profile' or $script eq 'verification' or $script eq 'changepassword' or $script eq 'notification'}class="active"{/if} href="{$tconfig.tsite_url}personal-information">{$smarty.const.LBL_PROFILE}</a></li>
			<!--
				<li><a {if $script eq 'money_detail'} class="active"{/if} href="{$tconfig.tsite_url}index.php?file=m-money_detail&type=available">{$smarty.const.LBL_DRIVER}</a></li>
			<li><a {if $script eq 'payment_detail'} class="active"{/if} class="active" href="{$tconfig.tsite_url}index.php?file=m-payment_detail&type=avaliable">{$smarty.const.LBL_PASSENGER}</a></li>-->
			<li><a {if $script eq 'money_detail' or $script eq 'payment_detail'} class="active"{/if} href="{$tconfig.tsite_url}index.php?file=m-money_detail&pagetype=money&type=available">{$smarty.const.LBL_PAYMENT}</a></li>	
		</ul>
		<div style="clear:both;"></div>
	</div>
</div>