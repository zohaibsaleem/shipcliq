<div class="body-inner-part">
  <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;Freeze Account</span></div>
  <div class="main-inner-page">
    <h2>Freeze account</h2>
    <div class="right-inner-part">
      <h2>Freeze account</h2>
      <p>Your Account is freeze for 24 hours due to cancel of booking within last 24 hours. You will post trip after 24 hour of your last booking cancel. For further information please contact administrator. </p>
    </div>
	{include file="left.tpl"}
   </div>
  <div style="clear:both;"></div>
</div>