{if $err_msg neq ''}
  {literal}
    <script>
      showNotification({type : 'error', message: '{/literal}{$err_msg}{literal}'});
    </script>
  {/literal}
{/if}
{if $var_msg neq ''}
  {literal}
    <script>
      showNotification({type : 'success', message: '{/literal}{$var_msg}{literal}'});
    </script>
  {/literal}
{/if}
<div class="body-inner-part">
    <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;Booking Cancellation</span></div>
    <div class="main-inner-page">
      <h2>{$smarty.const.LBL_BOOKING_CANCELLATION}</h2>
      {include file="member_top.tpl"}
      <div class="ratings">
        <div class="profile">
          <h2>{$smarty.const.LBL_BOOKING_DETAILS}</h2>
          <span style="color:#0094C8;font-size:15px;"><em style="color:#000000;font-size: 13px;">{$smarty.const.LBL_BOOKING_NO}</em>
            #{$db_booking_details[0].vBookingNo}
          </span>
          <span style="color:#0094C8;font-size:15px;"><em style="color:#000000;font-size: 13px;">{$smarty.const.LBL_BOOKING_DATE}</em>
            {$generalobj->DateTime($db_booking_details[0].dBookingDate,14)}
            <!--comment out time
             @ {$generalobj->DateTime($db_booking_details[0].dBookingTime,18)}
             -->
          </span>
          <span style="color:#0094C8;font-size:15px;"><em style="color:#000000;font-size: 13px;">{$smarty.const.LBL_BOOKING_FROM}</em>
            {$db_booking_details[0].vFromPlace}
          </span>
          <span style="color:#0094C8;font-size:15px;"><em style="color:#000000;font-size: 13px;">{$smarty.const.LBL_BOOKING_TO}</em>
            {$db_booking_details[0].vToPlace}
          </span>
          <span style="color:#0094C8;font-size:15px;"><em style="color:#000000;font-size: 13px;">{$smarty.const.LBL_TRAVELLER_NAME}</em>
            {$db_booking_details[0].vDriverFirstName} {$db_booking_details[0].vDriverLastName}
          </span>
          <span style="color:#0094C8;font-size:15px;"><em style="color:#000000;font-size: 13px;">{$smarty.const.LBL_BOOKER_NAME}</em>
            {$db_booking_details[0].vBookerFirstName} {$db_booking_details[0].vBookerLastName}
          </span>
          <span style="border-bottom:1px solid #CCCCCC;"></span>
          <!--
          {*if $db_booking_details[0].enttype eq 'Passenger'*}
          <span style="border-radius: 4px;background:#05A2DB;width:99%;padding:5px;font-size:15px;color:#FFFFFF;">
          <em style="width: 45px;"><img src="{$tconfig.tsite_images}msgGrowl_warning.png"></em>
          As a Sender after cancelation you get 50% amount as a refund from your booking payment amount. Once you cancel booking you can not active it.
          </span>
          {*else*}
          <span style="border-radius: 4px;background:#05A2DB;width:99%;padding:5px;font-size:15px;color:#FFFFFF;">
          <em style="width: 45px;"><img src="{$tconfig.tsite_images}msgGrowl_warning.png"></em>
          As a driver after cancelation your account is "frozen" during 24 hours, you can not post a trip for next 12/24 hours. Once you cancel booking you can not active it.
          </span>
          {*/if*}
          -->
          <form name="frmcancelation" id="frmcancelation" action="{$tconfig.tsite_url}index.php?file=m-booking_cancelation_form" method="post">
          {if $smarty.const.PAYMENT_OPTION eq 'PayPal'}
          {if $db_booking_details[0].enttype eq 'Passenger'}
          <span style="border-radius: 4px;background:#05A2DB;width:99%;padding:5px;font-size:15px;color:#FFFFFF;">
          <em style="width: 45px;"><img src="{$tconfig.tsite_images}msgGrowl_info.png"></em>
          Provide your Payment Email (Paypal) details or Bank details for refund process.
          </span>
          {/if}
          <h2>Booking Cancellation Details </h2>
          {if $db_booking_details[0].enttype eq 'Passenger'}
          <span><em> {$smarty.const.LBL_PAYMENT_EMAIL} *</em>
          <input name="Data_Payment[vPaymentEmail]" id="vPaymentEmail" type="text" class="validate[custom[email]] profile-input" value="{$db_member[0].vPaymentEmail}" />
          <p><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_registration-run" target="_blank">{$smarty.const.LBL_CREATE_PAYPAL_ACCOUNT}</a></p>
          </span>
          <div id="bankinfo" style="">
          <span><em> &nbsp; </em>
          {$smarty.const.LBL_OR}
          </span>
          <span><em>{$smarty.const.LBL_ACC_HODLER} *</em>
          <input name="Data_Payment[vBankAccountHolderName]" id="vBankAccountHolderName" type="text" class="profile-input" value="{$db_member[0].vBankAccountHolderName}" />
          </span>
          <span><em>{$smarty.const.LBL_ACC_NUMBER} (IBAN) *</em>
          <input name="Data_Payment[vAccountNumber]" id="vAccountNumber" type="text" class="profile-input" value="{$db_member[0].vAccountNumber}" />
          </span>
          <span><em>{$smarty.const.LBL_NAME_BANK} *</em>
          <input name="Data_Payment[vBankLocation]" id="vBankLocation" type="text" class="profile-input" value="{$db_member[0].vBankLocation}" />
          </span>
          <span><em>{$smarty.const.LBL_BANK_LOCATION} *</em>
          <input name="Data_Payment[vBankName]" id="vBankName" type="text" class="profile-input" value="{$db_member[0].vBankName}" />
          </span>
          <span><em>{$smarty.const.LBL_BIC_SWIFT_CODE} *</em>
          <input name="Data_Payment[vBIC_SWIFT_Code]" id="vBIC_SWIFT_Code" type="text" class="profile-input" value="{$db_member[0].vBIC_SWIFT_Code}" />
          </span>
          </div>
          {/if}
          {/if}
          <input type="hidden" name="id" id="id" value="{$iBookingId}">
          <input type="hidden" name="action" id="action" value="cancel_booking">
          <span><em> {$smarty.const.LBL_CANCELLATION_REASON} *</em>
          <textarea name="Data[tCancelReason]" id="tCancelReason" class="validate[required] profile-textarea"></textarea>
          </span>
          <span><em> &nbsp;</em>
          <input type="checkbox" name="RefundPolicy" id="RefundPolicy" class="validate[required]" value="Yes"> I agree to the <a href="{$tcinfig.tsite_url}refund-cancellation-policy" style="color:#05A2DB;" target="_blank">Refund & Cancellation Policy</a>
          </span>
          {if $db_booking_details[0].enttype eq 'Passenger'}
          <span class="sav-but-pro"><a href="javascript:void(0);" onclick="javascript:checkcancel_passenger(); return false;">Cancel Booking</a></span>
          {else}
          <span class="sav-but-pro"><a href="javascript:void(0);" onclick="javascript:checkcancel(); return false;">Cancel Booking</a></span>
          {/if}
          </form>
         </div>
      </div>
      {include file="member_profile_left.tpl"}
	</div>
    <div style="clear:both;"></div>
  </div>
{literal}
<script>
function checkcancel(){
 resp = jQuery("#frmcancelation").validationEngine('validate');// alert(resp);return false;
	if(resp == true){
		document.frmcancelation.submit();
	}else{
		return false;
	}
}





function Trim_me(s)
{
	return s.replace(/^\s+/g, '').replace(/\s+$/g, '');
}

var	site_url = '{/literal}{$tconfig.tsite_url}{literal}';
    function get_sate(country){
      $("#state").html('Wait...');
      var request = $.ajax({
    	  type: "POST",
    	  url: site_url+'index.php?file=m-get_state',
    	  data: "country="+country,

    	  success: function(data) {
    		  $("#state").html(data);
          $('#vState').val('{/literal}{$db_member[0].vState}{literal}');
    		}
    	});

    	request.fail(function(jqXHR, textStatus) {
    	  alert( "Request failed: " + textStatus );
    	});
    }


</script>
{/literal}
{if $db_member[0].vCountry neq ''}
{literal}
  <script>
     get_sate('{/literal}{$db_member[0].vCountry}{literal}');
  </script>
{/literal}
{/if}
{if $smarty.const.PAYMENT_OPTION eq 'PayPal'}
{literal}
<script>
function checkcancel_passenger(){

  if(Trim_me($("#vPaymentEmail").val()) == ''){
    if(Trim_me($("#vBankAccountHolderName").val()) == '' || Trim_me($("#vAccountNumber").val()) == '' || Trim_me($("#vBankLocation").val()) == '' || Trim_me($("#vBankName").val()) == '' || Trim_me($("#vBIC_SWIFT_Code").val()) == ''){
      alert('Please Provid Payapl payment email detail or All Bank details.'); return false;
    }
  }


  resp = jQuery("#frmcancelation").validationEngine('validate');// alert(resp);return false;
	if(resp == true){
		document.frmcancelation.submit();
	}else{
		return false;
	}
}
</script>
{/literal}
{else}
{literal}
<script>
function checkcancel_passenger(){

  resp = jQuery("#frmcancelation").validationEngine('validate');// alert(resp);return false;
	if(resp == true){
		document.frmcancelation.submit();
	}else{
		return false;
	}
}
</script>
{/literal}
{/if}
