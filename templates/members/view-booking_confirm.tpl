<div class="body-inner-part">
  <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_RIDE_BOOK_CONFIRM}</span></div>
  <div class="main-inner-page">
    <h2>{$smarty.const.LBL_RIDE_BOOK_CONFIRM}</h2>
    {if $cd eq 1}
    <div class="right-inner-part">
      <h2>{$smarty.const.LBL_RIDE_BOOK_SUCC_CONF}</h2>
      <p>{$smarty.const.LBL_BOOKING_CONF_SUCC}</p>
      <p>{$mailcont}</p>
    </div>
    {else}
    <div class="right-inner-part">
      <h2>{$smarty.const.LBL_RIDE_BOOK_FAILED}</h2>
      <p>{$smarty.const.LBL_PAYMENT_BOOK_FAILED}</p>
      <p>{$smarty.const.LBL_TRY_AGAIN}</p>
    </div>
    {/if}
	{include file="left.tpl"}
  </div>
  <div style="clear:both;"></div>
</div>