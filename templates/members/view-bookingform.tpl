
<div class="body-inner-part">
	<div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_BOOKING}</span></div>
	<div class="main-inner-page">
		<h2>{$smarty.const.LBL_BOOKING}</h2>
		<div class="ratings">
			<form name="frmpayment" id="frmpayment" action="{$tconfig.tsite_url}index.php?file=m-payment_a" method="post">
				<input type="hidden" name="ride_id" id="ride_id" value="{$id}">
				<input type="hidden" name="iRidePointId" id="iRidePointId" value="{$pointsid}">
				<input type="hidden" name="startpoint" id="startpoint" value="{$startpoint}">
				<input type="hidden" name="endpoint" id="endpoint" value="{$endpoint}">
				<input type="hidden" name="iNoOfSeats" id="iNoOfSeats" value="1">
				<input type="hidden" name="price" id="price" value="{$price}">
				<input type="hidden" name="fDocumentPrice" id="fDocumentPrice" value="{$fDocumentPrice}">
				<input type="hidden" name="vDocumentWeight" id="vDocumentWeight" value="{$vDocumentWeight}">
				<input type="hidden" name="vDocumentUnit" id="vDocumentUnit" value="{$vDocumentUnit}">
				<input type="hidden" name="fBoxPrice" id="fBoxPrice" value="{$fBoxPrice}">
				<input type="hidden" name="vBoxWeight" id="vBoxWeight" value="{$vBoxWeight}">
				<input type="hidden" name="vBoxUnit" id="vBoxUnit" value="{$vBoxUnit}">
				<input type="hidden" name="fLuggagePrice" id="fLuggagePrice" value="{$fLuggagePrice}">
				<input type="hidden" name="vLuggageWeight" id="vLuggageWeight" value="{$vLuggageWeight}">
				<input type="hidden" name="vLuggageUnit" id="vLuggageUnit" value="{$vLuggageUnit}">
				<input type="hidden" name="commission" id="commission" value="{$fCommission}">
				<input type="hidden" name="vat" id="vat" value="{$vat}">
				<input type="hidden" name="totamount" id="totamount" value="{$totamount}">
				<input type="hidden" name="mainprice" id="mainprice" value="{$mainprice}">
				<input type="hidden" name="commisionprice" id="commisionprice" value="{$commisionprice}">
				<input type="hidden" name="fSiteFixFee" id="fSiteFixFee" value="{$fSiteFixFee}">
				<input type="hidden" name="vatprice" id="vatprice" value="{$vatprice}">
				<input type="hidden" name="totprice" id="totprice" value="{$totprice}">
				<div class="sharing-result-box-main">{$ride_details}</div>
				<div class="booking1">
					<h2>{if $smarty.const.PAYMENT_OPTION eq 'PayPal'}{$smarty.const.LBL_BILING_INFO}{else}Booker Information{/if}</h2>
					<span><em>{$smarty.const.LBL_FIRST_NAME}</em>
						<input name="vFirstName" id="vFirstName" type="text" class="validate[required] profile-input" value="{$db_booker_details[0].vFirstName}" />
					</span>
					<span><em>{$smarty.const.LBL_LAST_NAME}</em>
						<input name="vLastName" id="vLastName" type="text" class="validate[required] profile-input" value="{$db_booker_details[0].vLastName}"/>
					</span>
					<span><em>{$smarty.const.LBL_ADDRESS} :</em>
						<textarea name="vAddress" id="vAddress" cols="" rows="" class="validate[required] profile-textarea">{$db_booker_details[0].vAddress}</textarea>
					</span>
					<span><em>{$smarty.const.LBL_ZIP_CODE} :</em>
						<input name="vZip" id="vZip" type="text" class="validate[required] profile-input" value="{$db_booker_details[0].vZip}"/>
					</span>
					<span><em>{$smarty.const.LBL_COUNTRY} :</em>
						<select name="vCountry" id="vCountry" class="validate[required] profile-select" onChange="get_sate(this.value);">
							<option value=""> -- {$smarty.const.LBL_SELECT_COUNTRY} -- </option>
							{section name="country" loop=$db_country}
							<option value="{$db_country[country].vCountryCode}" {if $db_booker_details[0].vCountry eq $db_country[country].vCountryCode} selected {/if}>{$db_country[country].vCountry}</option>
							{/section}
						</select>
					</span>
					<span><em>{$smarty.const.LBL_STATE} :</em>
						<div id="state">
							<select name="vState" id="vState" class="validate[required] profile-select">
								<option value=""> -- {$smarty.const.LBL_SELECT} {$smarty.const.LBL_STATE} -- </option>
							</select>
						</div>
					</span>
					<span><em>{$smarty.const.LBL_CITY} :</em>
						<input name="vCity" id="vCity" type="text" class="validate[required] profile-input" value="{$db_booker_details[0].vCity}"/>
					</span>
					<span><em>{$smarty.const.LBL_PHONE}</em>
						<input name="vPhone" id="vPhone" type="text" class="validate[required,custom[phone]] profile-input" value="{$db_booker_details[0].vPhone}"/>
					</span>
					<!--<h2 class="pay-n1">Pay by Credit Card</h2>
						<span><em>Card Type :</em>
						<select name="CardType" id="CardType" class="validate[required] profile-select">
						<option value=""> -- Select Card -- </option>
						<option value="VISA">Visa</option>
						<option value="MC">Master Card</option>
						<option value="MAESTRO">Maestro</option>
						<option value="AMEX">American Express</option>
						<option value="DISC">Discover</option>
						</select>
						</span> <span><em>Credit Card Number :</em>
						<input name="CardNumber"  id="CardNumber" type="text" class="validate[required] profile-input" value="" />
						</span> <span><em>CVV :</em>
						<input name="CV2"  id="CV2" type="text" class="validate[required] profile-input" value=""/>
						</span> <span><em>Card Type :</em>
						<select name="vExpMonth" id="vExpMonth" class="validate[required] profile-select-n">
						<option value=""> -- Select -- </option>
						<option value="01">01</option>
						<option value="02">02</option>
						<option value="03">03</option>
						<option value="04">04</option>
						<option value="05">05</option>
						<option value="06">06</option>
						<option value="07">07</option>
						<option value="08">08</option>
						<option value="09">09</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						</select>
						<select name="vExpYear" id="vExpYear" class="validate[required] profile-select-n1">
						<option value=""> -- Select -- </option>
						<option value="2014">2014</option>
						<option value="2015">2015</option>
						<option value="2016">2016</option>
						<option value="2017">2017</option>
						<option value="2018">2018</option>
						<option value="2019">2019</option>
						<option value="2020">2020</option>
						<option value="2021">2021</option>
						<option value="2022">2022</option>
						<option value="2023">2023</option>
						<option value="2024">2024</option>
						<option value="2025">2025</option>
						</select>
					</span>-->
					<span class="sav-but-pro1"><em class="blanck-c">&nbsp;</em><a href="javascript:void(0);" onClick="checkpayment();">{if $smarty.const.PAYMENT_OPTION eq 'PayPal'}{$smarty.const.LBL_PAY_NOW1}{else}Book Now{/if}</a></span>
				</div>
			</form>
		</div>
		{include file="left.tpl"}
	</div>
	<div style="clear:both;"></div>
</div>
{literal}
<script>
	$(document).ready(function(){
		showtag();
	});
    var	site_url = '{/literal}{$tconfig.tsite_url}{literal}';
    function get_sate(country){
		$("#state").html('Wait...');
		var request = $.ajax({
			type: "POST",
			url: site_url+'index.php?file=m-get_state',
			data: "country="+country,

			success: function(data) {
				$("#state").html(data);
				$('#vState').val('{/literal}{$db_booker_details[0].vState}{literal}');
			}
		});

    	request.fail(function(jqXHR, textStatus) {
			alert( "Request failed: " + textStatus );
		});
	}

    get_sate('{/literal}{$db_booker_details[0].vCountry}{literal}');

    function checkpayment(){
		resp = jQuery("#frmpayment").validationEngine('validate');
    	if(resp == true){
			var checked = 0;
			if($("#DocumentPrice").attr("checked") == 'checked')
			checked = 1;
			if($("#BoxPrice").attr("checked") == 'checked')
			checked = 1;
			if($("#LuggagePrice").attr("checked") == 'checked')
			checked = 1;

			var docid = document.getElementById('vDoc_val');
			if(docid != null)
			{
				var EnteredBooking = document.getElementById("vDoc_val").value;
			}

			var boxid = document.getElementById('vBox_val');
			if(boxid != null)
			{
				var EnteredBox = document.getElementById("vBox_val").value;
			}

			var lugageid = document.getElementById('vLuggage_val');
			if(lugageid != null)
			{
				var Enteredlugg = document.getElementById("vLuggage_val").value;
			}


			if(EnteredBooking == undefined)
			EnteredBooking = 0;
			if(EnteredBox == undefined)
			EnteredBox = 0;
			if(Enteredlugg == undefined)
			Enteredlugg = 0;

			/* 	if(checked == 1)
			{ */
			var err = "";
			if($("#DocumentPrice").attr("checked") == 'checked')
			{
				if(EnteredBooking == '' && EnteredBooking == 0)
				{
					err += 'Please Enter doc value.\r\n';

				}
			}
			/* else
				{
				document.frmpayment.submit();
			} */

			if($("#BoxPrice").attr("checked") == 'checked')
			{
				if(EnteredBox == '' && EnteredBox == 0)
				{
					err += 'Please Enter Box value.\r\n';
				}
			}


			if($("#LuggagePrice").attr("checked") == 'checked')
			{
				if(Enteredlugg == '' && Enteredlugg == 0)
				{
					err += 'Please Enter Luggage value.\r\n';
				}
			}

			if(err != "") {
				alert(err);
				return false;
			}




			//}
			if(checked == 0)
			{
				alert('Please select at least one item');
				return false;
			}
			else
			{
				document.frmpayment.submit();
			}

			}else{
    		return false;
		}
	}

    function change_seats(val){
		$('#iNoOfSeats').val(val);
		var price = $('#price').val();
		var commision = $('#commission').val();
		var vat = $('#vat').val();
		var seats = val;

		var ride_price = price * seats;
		ride_price = parseFloat(ride_price).toFixed(2);
		var tot_commision = commision * seats;
		tot_commision = parseFloat(tot_commision).toFixed(2);
		var tot_vat = vat * seats;
		tot_vat = parseFloat(tot_vat).toFixed(2);


		var tot_price = parseFloat(ride_price) + parseFloat(tot_commision) + parseFloat(tot_vat);
		tot_price = parseFloat(tot_price).toFixed(2);

		var mainprice = $('#mainprice').val();
		rideprice = mainprice.split(' ');
		$('#ride_price').html(rideprice[0]+' '+ride_price);

		var commisionprice = $('#commisionprice').val();
		commisionprice = commisionprice.split(' ');
		$('#commision_price').html(commisionprice[0]+' '+tot_commision);

		var vatprice = $('#vatprice').val();
		vatprice = vatprice.split(' ');
		$('#vat_price').html(vatprice[0]+' '+tot_vat);

		var totprice = $('#totprice').val();
		totprice = totprice.split(' ');
		$('#tot_price').html(totprice[0]+' '+tot_price);
	}
    /*function change_seats(val){
		$('#iNoOfSeats').val(val);
		var price = $('#price').val();
		var seats = val;

		var tot_price = price * seats;

		var mainprice = $('#mainprice').val();

		mainprice = mainprice.split(' ');

		$('#tot_price').html(mainprice[0]+' '+tot_price);
	}     */
	function price_update(id, new_price){
		var vDocPrice = $('#vDocPrice').val(); // price per unit
		var vBoxPrice = $('#vBoxPrice').val();
		var vLuggagePrice = $('#vLuggagePrice').val();

		if(vDocPrice == undefined)
		vDocPrice = 0;
		if(vBoxPrice == undefined)
		vBoxPrice = 0;
		if(vLuggagePrice == undefined)
		vLuggagePrice = 0;

		var vDocEntered = $('#vDoc_val').val(); // Enterd load
		var vBoxEntered = $('#vBox_val').val();
		var vLuggageEntered = $('#vLuggage_val').val();

		if(vDocEntered == undefined)
		vDocEntered = 0;
		if(vBoxEntered == undefined)
		vBoxEntered = 0;
		if(vLuggageEntered == undefined)
		vLuggageEntered = 0;

		price = (vDocPrice * vDocEntered) +(vBoxPrice * vBoxEntered) +(vLuggagePrice * vLuggageEntered);

		$('#price').val(price);

		/*var price = $('#price').val();
			if(price == '')
			price = 0;
			var id_name = $(id).attr("name");
			alert(id_name)
			if($(id).attr("checked") == 'checked') {
			price =  parseFloat(price) + parseFloat(new_price);
			$('#price').val(price);
			$('#f'+id_name).val(new_price);
			}
			else {
			price =  parseFloat(price) - parseFloat(new_price);
			$('#price').val(price);
			$('#f'+id_name).val(0);
		}*/

		var commision1 = parseFloat($('#commission').val());
		var fSiteFixFee=parseFloat($('#fSiteFixFee').val());
		var vat = parseFloat($('#vat').val());
		var seats = 1;

		var ride_price = price * seats;
		ride_price = parseFloat(ride_price).toFixed(2);

		var tot_commision = fSiteFixFee+((price*commision1)/100);
		//var tot_commision = commision * seats;
		tot_commision = parseFloat(tot_commision).toFixed(2);
		if(vat > 0){
		var tot_vat = Number(price) + Number(tot_commision);
		tot_vat=(tot_vat*vat)/100;

		//var tot_vat = vat * seats;
		tot_vat = parseFloat(tot_vat).toFixed(2);
		}
		else{
			tot_vat=0;
		}
		var tot_price = parseFloat(ride_price) + parseFloat(tot_commision) + parseFloat(tot_vat);
		tot_price = parseFloat(tot_price).toFixed(2);

		var mainprice = $('#mainprice').val();
		rideprice = mainprice.split(' ');
		$('#ride_price').html(rideprice[0]+' '+ride_price);

		var commisionprice = $('#commisionprice').val();
		commisionprice = commisionprice.split(' ');
		$('#commision_price').html(commisionprice[0]+' '+tot_commision);

		var vatprice = $('#vatprice').val();
		vatprice = vatprice.split(' ');
		$('#vat_price').html(vatprice[0]+' '+tot_vat);

		var totprice = $('#totprice').val();
		totprice = totprice.split(' ');
		$('#tot_price').html(totprice[0]+' '+tot_price);

		$('#totamount').val(tot_price);
	}

	function booking_alert(id_name)
	{
		var RemainBooking = document.getElementById(id_name).value;
		var EnteredBooking = document.getElementById(id_name+"_val").value;
		var price = document.getElementById(id_name+'Price').value;

		var TotalDocumentPrice = EnteredBooking * price;
		//alert(TotalDocumentPrice);
		var a = parseInt(RemainBooking);
		var b = parseInt(EnteredBooking);
		//alert (b);
		if(a < b)
		{
			alert('booking full');
			var EnteredBooking = document.getElementById(id_name+"_val").value = RemainBooking;
		}
		price_update(id_name, TotalDocumentPrice);

	}
</script>
<script>
	function showtag(){
		/*if(document.getElementById('DocumentPrice').checked) {
			$("#bookhideshow").show();
			var EnteredBooking = document.getElementById("vDoc_val").value = '';
			price_update(vDoc_val, EnteredBooking);
			} else {
			$("#bookhideshow").hide();
			var EnteredBooking = document.getElementById("vDoc_val").value = '';
			price_update(vDoc_val, EnteredBooking);
		}*/
		$('#DocumentPrice').click(function () {
			$("#bookhideshow").toggle(this.checked);
			var EnteredBooking = document.getElementById("vDoc_val").value = '';
			price_update(vBox_val, EnteredBooking);

		});

		$('#BoxPrice').click(function () {
			$("#boxhideshow").toggle(this.checked);
			var EnteredBooking = document.getElementById("vBox_val").value = '';
			price_update(vBox_val, EnteredBooking);

		});
		$('#LuggagePrice').click(function () {
			$("#luggagehideshow").toggle(this.checked);
			var EnteredBooking = document.getElementById("vLuggage_val").value = '';
			price_update(vLuggage_val, EnteredBooking);

		});

	}
</script>
{/literal}
