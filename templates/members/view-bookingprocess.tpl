<div class="body-inner-part">
  <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_BOOKING_PYMT_PROCESS}</span></div>
  <div class="main-inner-page">
    <h2>Booking Process</h2>
    <div class="right-inner-part">
      <h2>{$smarty.const.LBL_BOOKING_PYMT_PROCESS}</h2>
      <p><b>{$smarty.const.LBL_PYMT_INFO_UPDATE}</b><p></p><p style="font-size:18px;"> <b>{$smarty.const.LBL_WAIT_PYMT_PAGE}</b></p></p>
    </div>
	{include file="left.tpl"}
  </div>
  <div style="clear:both;"></div>
</div>
{literal}
<script>
    setTimeout(function(){
       window.location='{/literal}{$tconfig.tsite_url}{literal}checkpayment.php?id={/literal}{$numberid}{literal}';
    }, 10000);
</script>
{/literal}
