{if $smarty.get.var_msg neq ''}          
{if $smarty.get.msg_code eq '1'}
  {literal}
    <script>        
      showNotification({type : 'success', message: '{/literal}{$var_msg}{literal}'});  
    </script>
  {/literal}
{/if}
{/if} 

{if $smarty.get.var_msg neq ''}          
{if $smarty.get.msg_code eq '0'}
  {literal}
    <script>
      showNotification({type : 'error', message: '{/literal}{$var_msg}{literal}'});
    </script>
  {/literal}
{/if}
{/if}
 <div class="body-inner-part">
      <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_MY_CAR}</span></div>
      <div class="main-inner-page">
        <h2>{$smarty.const.LBL_MY}<span>{$smarty.const.LBL_CAR}</span></h2>
        {include file="member_top.tpl"}
        <div class="ratings">
          <div class="my-car">
            <h3>{$smarty.const.LBL_MY_CAR} <div style="float:right"><a class="link-dashbord" href="{$tconfig.tsite_url}car-add-form">{$smarty.const.LBL_ADD_NEW_CAR}</a></div></h3>
            <form name="frmaddcar" id="frmaddcar" action="{$tconfig.tsite_url}index.php?file=m-car_form_a" method="post" enctype="multipart/form-data">
              <input type="hidden" name="iMemberCarId" id="iMemberCarId" value="">
              <input type="hidden" name="action" id="action" value="Deletes">
            </form>
            {if $db_membercar|@count gt 0}
            <ul> 
              {section name=i loop=$db_membercar}
              <li>
                <div class="my-car-photo"> {if $db_membercar[i].vUrl neq ''}<img src="{$db_membercar[i].vUrl}" alt="" /> <a href="{$tconfig.tsite_url}car-edit-form/{$db_membercar[i].iMemberCarId}">{$smarty.const.LBL_UPDATE_PHOTO}</a>{/if} </div>
                <div class="my-car-photo-right">
                  <h2>{$db_membercar[i].vMake}</h2>
                  <strong>{$db_membercar[i].vModel}</strong>
                  <p>{$db_membercar[i].iSeats} {$smarty.const.LBL_SEATS}</p>
                  <p>{$smarty.const.LBL_CONFORT_LEVEL} : {$db_membercar[i].eComfort}</p>
                  <p><a href="{$tconfig.tsite_url}car-edit-form/{$db_membercar[i].iMemberCarId}"><img src="{$tconfig.tsite_images}pen.png" alt="" /></a><a href="javascript:void(0);" onclick="confirm_delete({$db_membercar[i].iMemberCarId});"><img src="{$tconfig.tsite_images}delete.png" alt="" /></a></p>
                </div>
              </li>
              {/section} 
            </ul>
            {else}
              <p style="text-align:center;font-size:15px;">{$smarty.const.LBL_NO_CAR_DETAILS} </p>
            {/if}
          </div>
        </div>
        {include file="member_profile_left.tpl"}
	  </div>
      <div style="clear:both;"></div>
    </div>
    
{literal}
<script>
function checkmembercar(){
   	resp = jQuery("#frmaddcar").validationEngine('validate');
    //alert(resp);return false;
		if(resp == true){
			document.frmaddcar.submit();
		}else{
			return false;
		}	
}
function confirm_delete(id)
{
   ans = confirm('{/literal}{$smarty.const.LBL_SURE_DEL_CAR_DETAIL}{literal}');
   if(ans == true){
    document.frmaddcar.iMemberCarId.value = id;
    document.frmaddcar.submit();
   }else{
    return false;
   }
}

function get_model(code,selected)
{   
    $("#model").html("{/literal}{$smarty.const.LBL_PLESE_WAIT}{literal}...");
    var request = $.ajax({
        type: "POST",
        url: '{/literal}{$tconfig.tsite_url}{literal}'+'/index.php?file=m-getmodel',
        data: "code="+code+"&stcode="+selected,
    
        success: function(data) { 
       
        $("#model").html(data);
        }
    });

        request.fail(function(jqXHR, textStatus) {
        alert( "Request failed: " + textStatus );
    });
}

if('{/literal}{$db_membercar[0].iModelId}{literal}' != '' || '{/literal}{$db_membercar[0].iMakeId}{literal}' != ''){
get_model('{/literal}{$db_membercar[0].iMakeId}{literal}', '{/literal}{$db_membercar[0].iModelId}{literal}');
}

function redirect(url){
    window.location=url;
  return false;
}
</script>
{/literal}    