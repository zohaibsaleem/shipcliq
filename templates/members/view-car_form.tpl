{if $smarty.get.var_msg neq ''}          
{if $smarty.get.msg_code eq '1'}
  {literal}
<script>        
      showNotification({type : 'success', message: '{/literal}{$var_msg}{literal}'});  
    </script>
{/literal}
{/if}
{/if} 

{if $smarty.get.var_msg neq ''}          
{if $smarty.get.msg_code eq '0'}
  {literal}
<script>
      showNotification({type : 'error', message: '{/literal}{$smarty.get.var_msg}{literal}'});
    </script>
{/literal}
{/if}
{/if}
<div class="body-inner-part">
  <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_ADD} {$smarty.const.LBL_CAR}</span></div>
  <div class="main-inner-page">
    <h2>{$smarty.const.LBL_ADD}<span>{$smarty.const.LBL_CAR}</span></h2>
    {include file="member_top.tpl"}
    <div class="ratings">
      <div class="add-car">
        <h3>{$smarty.const.LBL_MY_CAR}
          <div style="float:right;"><a class="link-dashbord" href="{$tconfig.tsite_url}car-details">{$smarty.const.LBL_BACK}</a></div>
        </h3>
        <form name="frmaddcar" id="frmaddcar" action="{$tconfig.tsite_url}index.php?file=m-car_form_a" method="post" enctype="multipart/form-data">
          <input type="hidden" name="iMemberCarId" id="iMemberCarId" value="{$db_membercar[0].iMemberCarId}">
          <input type="hidden" name="imagemode" id="imagemode" />
          {if $iMemberCarId eq ''}
          <input type="hidden" name="action" id="action" value="Add">
          {else}
          <input type="hidden" name="action" id="action" value="Edit">
          {/if} <span><em>{$smarty.const.LBL_CAR_PLATENO} </em>
          <input type="text" name="Data[vPlateno]" id="vPlateno" value="{$db_membercar[0].vPlateno}" class="add-car-select3" />
          </span> <span><em>{$smarty.const.LBL_MAKE} </em>
          <select name="Data[iMakeId]" id="iMakeId" class="validate[required] add-car-select" onchange="get_model(this.value,'');">
            <option value="">-----{$smarty.const.LBL_SELECT_TYPE}-----</option>
            
              {section name=i loop=$db_make}
                
            <option value="{$db_make[i].iMakeId}" {if $db_membercar[0].iMakeId eq $db_make[i].iMakeId} selected {/if}>{$db_make[i].vMake}</option>
            
              {/section}
            
          </select>
          </span> <span><em>{$smarty.const.LBL_MODEL} </em>
          <div id="model">
            <select name="" class="add-car-select">
              <option value="">-----{$smarty.const.LBL_SELECT} {$smarty.const.LBL_MODEL}-----</option>
            </select>
          </div>
          </span> <span><em>{$smarty.const.LBL_COMFORT} </em>
          <select name="Data[eComfort]" id="eComfort" class="validate[required] add-car-select">
            <option value="">-----{$smarty.const.LBL_SELECT} {$smarty.const.LBL_COMFORT}-----</option>
            <option value="Basic" {if $db_membercar[0].eComfort eq 'Basic'} selected {/if}>{$smarty.const.LBL_BASIC}</option>
            <option value="Normal" {if $db_membercar[0].eComfort eq 'Normal'} selected {/if}>{$smarty.const.LBL_NORMAL}</option>
            <option value="Comfortable" {if $db_membercar[0].eComfort eq 'Comfortable'} selected {/if}>{$smarty.const.LBL_COMFORTABLE}</option>
            <option value="Luxury" {if $db_membercar[0].eComfort eq 'Luxury'} selected {/if}>{$smarty.const.LBL_LUXURY}</option>
          </select>
          </span> <span><em>{$smarty.const.LBL_NO_OF_SEATS} </em>
          <select name="Data[iSeats]" id="iSeats" class="validate[required] add-car-select1">
            <option value="">-----{$smarty.const.LBL_SELECT} {$smarty.const.LBL_SEATS}-----</option>
            
              {section name=i loop=10}
                
            <option value="{$smarty.section.i.index+1}" {if $db_membercar[0].iSeats eq $smarty.section.i.index+1} selected {/if}>{$smarty.section.i.index+1}</option>
            
              {/section}
            
          </select>
          <p>({$smarty.const.LBL_INCLUDING_DRIVER})</p>
          </span> <span><em>{$smarty.const.LBL_COLOR}</em>
          <select name="Data[iColourId]" id="iColourId" class="validate[required] add-car-select">
            <option value="">-----{$smarty.const.LBL_SELECT} {$smarty.const.LBL_COLOR}-----</option>
            
              {section name=i loop=$db_color}
                
            <option value="{$db_color[i].iColourId}" {if $db_membercar[0].iColourId eq $db_color[i].iColourId} selected {/if}>{$db_color[i].vColour}</option>
            
              {/section}
            
          </select>
          </span> <span><em>{$smarty.const.LBL_TYPE} </em>
          <select name="Data[iCarTypeId]" id="iCarTypeId" class="validate[required] add-car-select">
            <option value="">-----{$smarty.const.LBL_SELECT_TYPE}-----</option>
            
              {section name=i loop=$db_type}
                
            <option value="{$db_type[i].iCarTypeId}" {if $db_membercar[0].iCarTypeId eq $db_type[i].iCarTypeId} selected {/if}>{$db_type[i].vTitle}</option>
            
              {/section}
            
          </select>
          </span> <span><em>{$smarty.const.LBL_PHOTO} </em>
          <input type="file" id="vImage" name="vImage">
          </span>
          <span style="margin-bottom:5px;"><em>&nbsp;</em>[Note: Please Select Image of Extension jpg, png, gif]
          <span style="margin-bottom:5px;"><em>&nbsp;</em>[Image Size Should be less then 1 MB]<span style="margin-bottom:5px;"><em>&nbsp;</em>[Recommended dimension width 255px and height 160px.]</span></span></span>
          <span><em class="last1em">&nbsp;</em> {if $img eq 'yes'} <img src="{$tconfig.tsite_upload_images_member}/{$iMemberId}/1_{$db_membercar[0].vImage}" title="Image" />
          <a href="javascript:void(0);" onclick="return confirm_delete();" style="margin-left:0px;" class="detele-im">{$smarty.const.LBL_DELETE} {$smarty.const.LBL_IMAGE}</a> {/if} </span>
          <span class="car-but">
          <em class="blanck-c">&nbsp;</em><a href="javascript:void(0);" onclick="checkmembercar();">{$smarty.const.LBL_SAVE}</a><a href="{$tconfig.tsite_url}car-details">{$smarty.const.LBL_CANCEL}</a></span>
        </form>
      </div>
    </div>
    {include file="member_profile_left.tpl"}
	<!-------------------------inner-page end----------------->
  </div>
  <div style="clear:both;"></div>
</div>
{literal}
<script>
function checkmembercar(){
    
   	resp = jQuery("#frmaddcar").validationEngine('validate');
    //alert(resp);return false;
		if(resp == true){
			document.frmaddcar.submit();
		}else{
			return false;
		}	
}
function confirm_delete()
{
   ans = confirm('{/literal}{$smarty.const.LBL_SURE_DELETE_PHOTO}{literal}?');
   if(ans == true){
    document.frmaddcar.imagemode.value = 'delete_image';
    document.frmaddcar.submit();
   }else{
    return false;
   }
}

function get_model(code,selected)
{   
    $("#model").html("{/literal}{$smarty.const.LBL_PLESE_WAIT}{literal}...");
    var request = $.ajax({
        type: "POST",
        url: '{/literal}{$tconfig.tsite_url}{literal}'+'/index.php?file=m-getmodel',
        data: "code="+code+"&stcode="+selected,
    
        success: function(data) { 
       
        $("#model").html(data);
        }
    });

        request.fail(function(jqXHR, textStatus) {
        alert( "Request failed: " + textStatus );
    });
}

if('{/literal}{$db_membercar[0].iModelId}{literal}' != '' || '{/literal}{$db_membercar[0].iMakeId}{literal}' != ''){
get_model('{/literal}{$db_membercar[0].iMakeId}{literal}', '{/literal}{$db_membercar[0].iModelId}{literal}');
}
</script>
{/literal} 