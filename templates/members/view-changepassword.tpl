{if $smarty.get.var_msg neq ''}          
{if $smarty.get.msg_code eq '1'}
  {literal}
    <script>        
      showNotification({type : 'success', message: '{/literal}{$var_msg}{literal}'});  
    </script>
  {/literal}
{/if}
{/if} 

{if $smarty.get.var_msg neq ''}          
{if $smarty.get.msg_code eq '0'}
  {literal}
    <script>
      showNotification({type : 'error', message: '{/literal}{$var_msg}{literal}'});
    </script>
  {/literal}
{/if}
{/if} 
<div class="body-inner-part">
  <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_CHANGE_PASSWORD}</span></div>
  <!-------------------------inner-page----------------->
  <div class="main-inner-page">
    <h2>{$smarty.const.LBL_CHANGE_PASSWORD}</h2>
    {include file="member_top.tpl"}
    <div class="ratings">
      <div class="profile">
        <h2>{$smarty.const.LBL_CHANGE_PASSWORD} </h2>
        <form name="frmchangepassword" id="frmchangepassword" method="post" action="{$tconfig.tsite_url}index.php?file=m-changepassword">
        <span><em> {$smarty.const.LBL_OLD} {$smarty.const.LBL_PASSWORD}</em>
        <input class="validate[required,minSize[6]] profile-input" name="vOldPassword" id="vOldPassword" type="password" value=""/>
        </span>
        <span><em> {$smarty.const.LBL_NEW} {$smarty.const.LBL_PASSWORD}</em>
        <input class="validate[required,minSize[6]] profile-input" name="vPassword" id="vPassword" type="password" value=""/>
        </span> 
        <span><em> {$smarty.const.LBL_RETYPE} {$smarty.const.LBL_PASSWORD}</em>
        <input class="validate[required,equals[vPassword]] profile-input" name="vPassword2" id="vPassword2" type="password" />
        </span>
         <span class="sav-but-pro"><a href="javascript:void(0);" onClick="javascript:checkvalid(); return false;">{$smarty.const.LBL_SUBMIT}</a></span>
       </form> 
      </div>
    </div>
	{include file="member_profile_left.tpl"}    
  </div>
  <!-------------------------inner-page end----------------->
  <div style="clear:both;"></div>
</div>
{literal} 
<script>
function checkvalid(){
    //alert('hello..');
    jQuery("#frmchangepassword").validationEngine('init',{scroll: false});
	  jQuery("#frmchangepassword").validationEngine('attach',{scroll: false});
		resp = jQuery("#frmchangepassword").validationEngine('validate');
	//	alert(resp); return false;
		if(resp == true){
			document.frmchangepassword.submit();
		}else{
			return false;
		}		
}
$('#frmchangepassword').bind('keydown',function(e){
    if(e.which == 13){
      checkvalid(); return false;
    }
});
</script> 
{/literal}