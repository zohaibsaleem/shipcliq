{if $var_msg neq ''}
{if $msg_code eq '0'}
{literal}
 <script>
  showNotification({type : 'error', message: '{/literal}{$var_msg}{literal}'});
 </script>
 {/literal}
{/if}
{/if}
<div class="body-inner-part">
	<div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_DASHBOARD}</span></div>
	<div class="main-inner-page">
		<div class="dashboard-new-page">
			<h2 class="dashboard-new-page-heading">{$smarty.const.LBL_DASHBOARD}</h2>
			<div class="dashboard-new-page-main-left-part">
				<div class="dashboard-client">
					<div class="dashboard-client-img"> <a href="{$tconfig.tsite_url}profile-photo"><img src="{$dashboard_user_img}" alt="" /></a> </div>
					<h3>
						{$smarty.session.sess_vFirstName} {$smarty.session.sess_vLastName}<br />
						{if $img eq 'No'} <a href="{$tconfig.tsite_url}profile-photo">{$smarty.const.LBL_ADD_PHOTO|nl2br}</a><br/>
					{/if} <a href="{$tconfig.tsite_url}personal-information">[{$smarty.const.LBL_EDIT_YOUR_PROFILE}]</a> </h3></div>

					<div class="dashboard-new-page-bottom-part-left">
						<div class="dashboard-new-your-box">
							<div class="dashboard-new-your-box-top"><img src="{$tconfig.tsite_images}dashboard-new-your-box-top.jpg" alt="" /></div>
							<div class="dashboard-new-your-box-mid">
								<h2>{$smarty.const.LBL_PROFILE}</h2>
								<ul>
									<!--<li>{$smarty.const.LBL_PROFILE_VIEW} :<b>{$db_member[0].iMemberViewCnt}</b></li>-->
									{*<li>{$smarty.const.LBL_RIDE_VIEW} :<b>{$iRideCount}</b></li>*}
									<li>{$smarty.const.LBL_RATINGS} :<b>{$rating}</b></li>
								</ul>
							</div>
							<div class="dashboard-new-your-box-bottom"><img src="{$tconfig.tsite_images}/dashboard-new-your-box-bottom.jpg" alt="" /></div>
						</div>
						<div class="dashboard-new-profile-status">
							<h2>{$smarty.const.LBL_PROFILE_STATUS}</h2>
							<ul>
								<div class="progress-striped"><span style="width:{$prof_percent}%;">{$prof_percent}%</span></div>
								<b class="din-tetx">{$smarty.const.LBL_FIND_MORE_MACHES} </b>
								<li>{if $first_last_name_img eq 'green'}<img src="{$tconfig.tsite_images}green-arrow.jpg" alt="" />{else}<img src="{$tconfig.tsite_images}gray-arrow.jpg" alt="" />{/if}<a href="{$tconfig.tsite_url}personal-information">{$smarty.const.LBL_FIRST_NAME_AND_LAST_NAME}</a></li>
								<li>{if $nickname_img eq 'green'}<img src="{$tconfig.tsite_images}green-arrow.jpg" alt="" />{else}<img src="{$tconfig.tsite_images}gray-arrow.jpg" alt="" />{/if}<a href="{$tconfig.tsite_url}personal-information">{$smarty.const.LBL_NICK_NAME}</a></li>
								<li>{if $birthdate_img eq 'green'}<img src="{$tconfig.tsite_images}green-arrow.jpg" alt="" />{else}<img src="{$tconfig.tsite_images}gray-arrow.jpg" alt="" />{/if}<a href="{$tconfig.tsite_url}personal-information">{$smarty.const.LBL_AGE}</a></li>
								<li>{if $email_varified_img eq 'green'}<img src="{$tconfig.tsite_images}green-arrow.jpg" alt="" />{else}<img src="{$tconfig.tsite_images}gray-arrow.jpg" alt="" />{/if}<a href="{$tconfig.tsite_url}Member-Verification">{$smarty.const.LBL_EMAIL_VERIFIED}</a></li>
								<li>{if $profile_photo_img eq 'green'}<img src="{$tconfig.tsite_images}green-arrow.jpg" alt="" />{else}<img src="{$tconfig.tsite_images}gray-arrow.jpg" alt="" />{/if}<a href="{$tconfig.tsite_url}profile-photo">{$smarty.const.LBL_PROFILE_PHOTO}</a></li>
								{*<li>{if $car_pref_status_img eq 'green'}<img src="{$tconfig.tsite_images}green-arrow.jpg" alt="" />{else}<img src="{$tconfig.tsite_images}gray-arrow.jpg" alt="" />{/if}<a href="{$tconfig.tsite_url}car-preferences">{$smarty.const.LBL_SHARE_PREFERENCE}</a></li>*}
								{if $PHONE_VERIFICATION_REQUIRED eq 'Yes'}
								<li>{if $phone_verified_img eq 'green'}<img src="{$tconfig.tsite_images}green-arrow.jpg" alt="" />{else}<img src="{$tconfig.tsite_images}gray-arrow.jpg" alt="" />{/if}<a href="{$tconfig.tsite_url}Member-Verification">{$smarty.const.LBL_PHONE} {$smarty.const.LBL_VERIFIED}</a></li>{/if}
								<li>{if $address_img eq 'green'}<img src="{$tconfig.tsite_images}green-arrow.jpg" alt="" />{else}<img src="{$tconfig.tsite_images}gray-arrow.jpg" alt="" />{/if}<a href="{$tconfig.tsite_url}personal-information">{$smarty.const.LBL_ADDRESS}</a></li>
								<li>{if $descritption_img eq 'green'}<img src="{$tconfig.tsite_images}green-arrow.jpg" alt="" />{else}<img src="{$tconfig.tsite_images}gray-arrow.jpg" alt="" />{/if}<a href="{$tconfig.tsite_url}personal-information">{$smarty.const.LBL_QUIK_DESCRIPTION}</a></li>
								{if $PAYMENT_OPTION eq 'PayPal'}
								<li>{if $payment_email_img eq 'green'}<img src="{$tconfig.tsite_images}green-arrow.jpg" alt="" />{else}<img src="{$tconfig.tsite_images}gray-arrow.jpg" alt="" />{/if}<a href="{$tconfig.tsite_url}personal-information">{$smarty.const.LBL_PAYMENT_DETAILS}</a>
								</li>
								{/if}
                {if $CARPAPER_VERIFICATION_REQUIRED eq 'Yes'}
								<li>{if $carpaper_img eq 'green'}<img src="{$tconfig.tsite_images}green-arrow.jpg" alt="" />{else}<img src="{$tconfig.tsite_images}gray-arrow.jpg" alt="" />{/if}<a href="{$tconfig.tsite_url}personal-information">{$smarty.const.LBL_CAR_PAPER_VERIFIED}</a>
								</li>
								{/if}
                {if $LICENSE_VERIFICATION_REQUIRED eq 'Yes'}
								<li>{if $license_img eq 'green'}<img src="{$tconfig.tsite_images}green-arrow.jpg" alt="" />{else}<img src="{$tconfig.tsite_images}gray-arrow.jpg" alt="" />{/if}<a href="{$tconfig.tsite_url}personal-information">{$smarty.const.LBL_LICENSE_VERIFIED}</a>
								</li>
								{/if}
								{*<li>{if $car_detail_img eq 'green'}<img src="{$tconfig.tsite_images}green-arrow.jpg" alt="" />{else}<img src="{$tconfig.tsite_images}gray-arrow.jpg" alt="" />{/if}<a href="{$tconfig.tsite_url}car-details">{$smarty.const.LBL_CAR_DETAILS}</a></li>*}
								<div class="update-profile"><a href="{$tconfig.tsite_url}personal-information">{$smarty.const.LBL_UPDATE_PROFILE} &raquo;</a></div>
							</ul>
						</div>
						<!-- <div class="dashboard-new-profile-status">
							<h2>Your profile status</h2>
							<b><img src="{$tconfig.tsite_images}/profile-status-img.jpg" alt="" /></b>
							<p>Find more matches by complete your profile:</p>
							<ul>
							<li><img src="{$tconfig.tsite_images}/arrow-gr.jpg" alt="" /><a href="#">First Name & Last Name</a></li>
							<li><img src="{$tconfig.tsite_images}/arrow-gra.jpg" alt="" /><a href="#">Nick Name</a></li>
							<li><img src="{$tconfig.tsite_images}/arrow-gr.jpg" alt="" /><a href="#">Age</a></li>
							<li><img src="{$tconfig.tsite_images}/arrow-gra.jpg" alt="" /><a href="#">Email Verified</a></li>
							<li><img src="{$tconfig.tsite_images}/arrow-gr.jpg" alt="" /><a href="#">Profile Picture</a></li>
							<li><img src="{$tconfig.tsite_images}/arrow-gra.jpg" alt="" /><a href="#">Car Share Preferences</a></li>
							<li><img src="{$tconfig.tsite_images}/arrow-gra.jpg" alt="" /><a href="#">Phone Number Verified</a></li>
							<li><img src="{$tconfig.tsite_images}/arrow-gra.jpg" alt="" /><a href="#">Address</a></li>
							<li><img src="{$tconfig.tsite_images}/arrow-gra.jpg" alt="" /><a href="#">Description</a></li>
							<li><img src="{$tconfig.tsite_images}/arrow-gra.jpg" alt="" /><a href="#">Payment Details</a></li>
							<li><img src="{$tconfig.tsite_images}/arrow-gra.jpg" alt="" /><a href="#">Car Details</a></li>
							</ul>
							<div class="update-profile"><a href="{$tconfig.tsite_url}personal-information">update profile</a></div>
						</div>  -->
					</div>
			</div>
			<div class="dashboard-new-page-main-right-part"> {if $tot_notofications gt '0'}
				<div class="dashboard-notifications">
					<div class="dashboard-notifications-inner">
						<h2><img src="{$tconfig.tsite_images}/notifications-icon.png" alt="" />{$smarty.const.LBL_NOTIFICATION} ({$tot_notofications})</h2>
						<ul>
							{if $db_email_verification[0].eEmailVarified eq 'No'}
							<li> <span>
								<h3>{$smarty.const.LBL_VERIFY_EMAIL}</h3>
								<form name="frmemailverify" enctype="multipart/form-data" id="frmemailverify" action="{$tconfig.tsite_url}index.php?file=m-verification_a" method="post">
									<input type="hidden" name="action" id="action" value="emailverify">
									<input type="hidden" name="vEmail" id="vEmail" value="{$smarty.session.sess_vEmail}">
									<input type="hidden" name="vName" id="vName" value="{$smarty.session.sess_vFirstName} {$smarty.session.sess_vLastName}">
								</form>
								<p>{$smarty.const.LBL_EMAIL_VERI_NOTE1}
								{$smarty.const.LBL_EMAIL_VERI_NOTE2} </p>
							</span> <a href="javascript:void(0);" class="link-dashbord" onClick="checkmail();">{$smarty.const.LBL_VERIFY_EMAIL}</a> </li>
							{/if}
							{if $PHONE_VERIFICATION_REQUIRED eq 'Yes'}
							{if $db_email_verification[0].ePhoneVerified eq 'No'}
							<li> <span>
								<h3>{$smarty.const.LBL_VERIFY_PHONE}</h3>
								<p>{$smarty.const.LBL_EMAIL_VERI_NOTE1}
								{$smarty.const.LBL_EMAIL_VERI_NOTE2}</p>
							</span> <a href="{$tconfig.tsite_url}Member-Verification" class="link-dashbord">{$smarty.const.LBL_VERIFY_PHONE}</a> </li>
							{/if}
							{/if}
						</ul>
					</div>
				</div>
				{/if}
				<div class="dashboard-new-page-bottom-part-right">
					<div class="dashboard-new-page-bottom-part-list">
						<ul>
							<li>
								<div class="dashboard-bottom-box1">
									<a href="{$tconfig.tsite_url}my-bookings">
										<h2>{$smarty.const.LBL_MY_BOOKINGS}</h2>
										<h3>{$tot_bookings}</h3>
									<span>{$smarty.const.LBL_BOOKING}</span>
									</a>
									</div>
							</li>
							<li>
								<div class="dashboard-bottom-box2">
									<a href="{$tconfig.tsite_url}List-Rides-Offer">
										<h2>{$smarty.const.LBL_RIDES_OFFERED1}</h2>
										<h3>{$tot_rides}</h3>
									<span>{$smarty.const.LBL_RIDES_OFFERED1}</span>
									</a>
									</div>
							</li>
							<li>
								<div class="dashboard-bottom-box3">
									<a href="{$tconfig.tsite_url}received-messages">
										<h2>{$smarty.const.LBL_MESSAGES}</h2>
										<h3>{$tot_messages}</h3>
									<span>{$smarty.const.LBL_MESSAGES}</span>
									</a>
									</div>
							</li>
							<li>
								<div class="dashboard-bottom-box4">
									<a href="{$tconfig.tsite_url}Email-Alerts">
										<h2>{$smarty.const.LBL_MY_ALERTS}</h2>
										<h3>{$tot_ride_alerts}</h3>
										<span>{$smarty.const.LBL_EMAIL_ALERTS}</span>
									</a>
								</div>
							</li>
							<li>
								<div class="dashboard-bottom-box5">
									<a href="{$tconfig.tsite_url}index.php?file=m-payment_detail&pagetype=payment&type=available">
										<h2>{$smarty.const.LBL_PAID_BY_YOU}</h2>
										<h3>{$tot_paid}</h3>
									<span>{$smarty.session.sess_price_ratio}</span>
									</a>
									</div>
							</li>
							<li>
								<div class="dashboard-bottom-box6">
									<a href="{$tconfig.tsite_url}index.php?file=m-money_detail&pagetype=money&type=available">
										<h2>{$smarty.const.LBL_YOU_EARNED}</h2>
										<h3>{$tot_earned}</h3>
									<span>{$smarty.session.sess_price_ratio}</span>
									</a>
									</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div style="clear:both;"></div>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>
{literal}
<script>
	function checkmail(){
		document.frmemailverify.submit();
	}
</script>
{/literal}
