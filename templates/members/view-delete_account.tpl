  <div class="body-inner-part">
      <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_DELETE_ACCOUNT}</span></div>
      <div class="main-inner-page">
        <h2>{$smarty.const.LBL_DELETE_ACCOUNT}</h2>
        {include file="member_top.tpl"}
        <div class="ratings">
          <div class="profile">
            <h2>{$smarty.const.LBL_DELETE_YOUR_ACC} </h2>
            <form name="frmdelete" enctype="multipart/form-data" id="frmdelete" action="{$tconfig.tsite_url}index.php?file=m-delete_account_a" method="post">
            {if $msg neq 1}
                <span>{$smarty.const.LBL_CLICK_CONFIRM}</span>
                <span class="sav-but-pro"><a href="javascript:void(0);" onclick="javascript:checkprofile(); return false;">Confirm</a></span>
            {/if}
            {if $msg eq 1}<span style="font-size:16px; color:#21610B;">{$smarty.const.LBL_DELETE_ACC_MSG}</span>{/if}
            </form>
           </div>
        </div>
		{include file="member_profile_left.tpl"}
      </div>
      <!-------------------------inner-page end----------------->
      
      <div style="clear:both;"></div>
    </div>
{literal} 
<script>
function checkprofile(){
 resp = confirm("{/literal}{$smarty.const.LBL_SURE_DELET_ACCOUNT}{literal}");
	if(resp == true){
		document.frmdelete.submit();
	}else{
		return false;
	}		
}
</script> 
{/literal} 