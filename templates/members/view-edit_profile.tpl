{if $smarty.get.var_msg neq ''}
{if $smarty.get.msg_code eq '1'}
{literal}
<script>
	showNotification({type : 'success', message: '{/literal}{$var_msg}{literal}'});
</script>
{/literal}
{/if}
{/if}

{if $smarty.get.var_msg neq ''}
{if $smarty.get.msg_code eq '0'}
{literal}
<script>
	showNotification({type : 'error', message: '{/literal}{$var_msg}{literal}'});
</script>
{/literal}
{/if}
{/if}

{if $duplicate eq '1'}
{if $var_msg neq ''}
{literal}
<script>
	showNotification({type : 'error', message: '{/literal}{$var_msg}{literal}'});
</script>
{/literal}
{/if}
{/if}

<div class="body-inner-part">
	<div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_PROFILE}</span></div>
	<div class="main-inner-page">
        <h2>{$smarty.const.LBL_PROFILE}</h2>
        {include file="member_top.tpl"}
        <div class="ratings">
			<div class="profile profile-edit">
				<h2>{$smarty.const.LBL_MY_PERSONAL_INFO} </h2>
				<form name="frmprofile" enctype="multipart/form-data" id="frmprofile" action="{$tconfig.tsite_url}index.php?file=m-edit_profile_a" method="post">
					<span class="edit-p"><em> {$smarty.const.LBL_GENDER}</em>
						<input type="radio" name="Data[eGender]" id="eGender" value="Male" {if $db_member[0].eGender eq 'Male'}checked="true"{/if} />{$smarty.const.LBL_MALE}
						<input type="radio" name="Data[eGender]" id="eGender" value="Female" {if $db_member[0].eGender eq 'Female'}checked="true"{/if} />{$smarty.const.LBL_FEMALE}
					</span>
					<span><em> {$smarty.const.LBL_FIRST_NAME} *</em>
						<input name="Data[vFirstName]" id="vFirstName" type="text" class="validate[required] profile-input" value="{$db_member[0].vFirstName}" />
					</span>
					<span><em> {$smarty.const.LBL_LAST_NAME} *</em>
						<input name="Data[vLastName]" id="vLastName" type="text" class="validate[required] profile-input" value="{$db_member[0].vLastName}"/>
					</span>
					<span><em> {$smarty.const.LBL_NICK_NAME}</em>
						<input name="Data[vNickName]" id="vNickName" type="text" class="profile-input" value="{$db_member[0].vNickName}"/>
					</span>
					<span><em> {$smarty.const.LBL_EMAIL} *</em>
						<input name="Data[vEmail]" id="vEmail" type="text" class="validate[required,custom[email]] profile-input" value="{$db_member[0].vEmail}" />
					</span>
					<span><em> {$smarty.const.LBL_MOBILE} {$smarty.const.LBL_PHONE} *</em>
						<select name="Data[vCountry]" id="vCountry" class="validate[required] profile-select1" onChange="get_sate(this.value);">
							<option value="">{$smarty.const.LBL_SELECT_COUNTRY}</option>
							{section name=i loop=$db_country}
							<option value="{$db_country[i].vCountryCode}" {if $db_country[i].vCountryCode eq $db_member[0].vCountry} selected {/if}>{$db_country[i].vCountry}</option>
							{/section}
						</select>

						&nbsp;&nbsp;
						<input name="Data[vPhone]" id="vPhone" type="text" class="validate[required,custom[phone]] profile-input1" value="{$db_member[0].vPhone}" />
						<!--<p>
							<input name="Data[eShowPhoneOnline]" id="eShowPhoneOnline" type="checkbox" {if $db_member[0].eShowPhoneOnline eq 'No'}checked="true"{/if} />
						&nbsp;&nbsp;{$smarty.const.LBL_NEVER_SHOW_PHONE} </p>-->
					</span>
					<span><em> {$smarty.const.LBL_STATE} </em>
						<div id="state">
							<select name="vState" id="vState" class="validate[required] profile-select">
								<option value="">{$smarty.const.LBL_SELECT} {$smarty.const.LBL_STATE}</option>
							</select>
						</div>
					</span>
					<span><em> {$smarty.const.LBL_CITY} </em>
						<div id="state">
							<input name="Data[vCity]" id="vCity" type="text" class="validate[required] profile-input" value="{$db_member[0].vCity}" />
						</div>
					</span>
					<span><em>{$smarty.const.LBL_ADDRESS} </em>
						<textarea name="Data[vAddress]" id="vAddress" cols="" rows="" class="profile-textarea">{$db_member[0].vAddress}</textarea>
					</span>
					<span><em>{$smarty.const.LBL_ZIP_CODE} </em>
						<input name="Data[vZip]" id="vZip" type="text" class="profile-input" value="{$db_member[0].vZip}" />
					</span>
					<span><em> {$smarty.const.LBL_BIRTH_YEAR} *</em>
						{$com_year_combo}
					</span>
					<span><em> {$smarty.const.LBL_PREFERRED_LANG} </em>
						<select name="Data[vLanguageCode]" id="vLanguageCode" class="validate[required] profile-select">
							<option value="EN" {if $db_member[0].vLanguageCode eq 'EN'} selected {/if}>{$smarty.const.LBL_ENGLISH}</option>
							<option value="DN" {if $db_member[0].vLanguageCode eq 'DN'} selected {/if}>{$smarty.const.LBL_DANISH}</option>
							<option value="FI" {if $db_member[0].vLanguageCode eq 'FI'} selected {/if}>{$smarty.const.LBL_FINISH}</option>
							<option value="FN" {if $db_member[0].vLanguageCode eq 'FN'} selected {/if}>{$smarty.const.LBL_FRENCH}</option>
							<option value="LV" {if $db_member[0].vLanguageCode eq 'LV'} selected {/if}>{$smarty.const.LBL_LATVIN}</option>
							<option value="EE" {if $db_member[0].vLanguageCode eq 'EE'} selected {/if}>{$smarty.const.LBL_ESTONIAN}</option>
							<option value="LT" {if $db_member[0].vLanguageCode eq 'LT'} selected {/if}>{$smarty.const.LBL_LITHUNIAN}</option>
							<option value="DE" {if $db_member[0].vLanguageCode eq 'DE'} selected {/if}>{$smarty.const.LBL_GERMAN}</option>
							<option value="NO" {if $db_member[0].vLanguageCode eq 'NO'} selected {/if}>{$smarty.const.LBL_NORWAY}</option>
							<option value="PO" {if $db_member[0].vLanguageCode eq 'PO'} selected {/if}>{$smarty.const.LBL_POLISH}</option>
							<option value="RS" {if $db_member[0].vLanguageCode eq 'RS'} selected {/if}>{$smarty.const.LBL_RUSSIAN}</option>
							<option value="ES" {if $db_member[0].vLanguageCode eq 'ES'} selected {/if}>{$smarty.const.LBL_SPANISH}</option>
							<option value="SW" {if $db_member[0].vLanguageCode eq 'SW'} selected {/if}>{$smarty.const.LBL_SWEDISH}</option>
						</select>
					</span>
					<span><em> {$smarty.const.LBL_QUIK_DESCRIPTION}</em>
						<textarea name="Data[tDescription]" id="tDescription" cols="" rows="" class="profile-textarea">{$db_member[0].tDescription}</textarea>
						<p>{$smarty.const.LBL_PROFILE_NOTE}</p>
					</span>
					<span><em>{$smarty.const.LBL_NEWS_LETTER_EDIT}</em>
						<input type="checkbox" id="newsletter" name="newsletter" {if $totalnewsletter neq 0} checked {/if}>
					</span>
					{if $smarty.const.PAYMENT_OPTION eq 'PayPal'}
					<h2>{$smarty.const.LBL_PAYMENT_DETAILS}</h2>
					<p class="memberpaydtls" style="background:#44AA00;color:#fff;padding:5px;width:auto;">{$smarty.const.LBL_PAYMENT_INFO}</p>
					<span class="edit-p"><em> {$smarty.const.LBL_MEMBER_TYPE} </em>
						<input type="radio" name="Data[eMemberType]" id="Passenger" value="Passenger" {if $db_member[0].eMemberType eq 'Passenger'}checked="true"{/if} onclick="checkMeberType(this.value);" />{$smarty.const.LBL_PASSENGER}
						<input type="radio" name="Data[eMemberType]" id="Driver" value="Driver" {if $db_member[0].eMemberType eq 'Driver'}checked="true"{/if} onclick="checkMeberType(this.value);" />{$smarty.const.LBL_DRIVER1}
						<input type="radio" name="Data[eMemberType]" id="Both" value="Both" {if $db_member[0].eMemberType eq 'Both'}checked="true"{/if} onclick="checkMeberType(this.value);" />{$smarty.const.LBL_BOTH}
					</span>
					<span id="paymail">
						<em> {$smarty.const.LBL_PAYMENT_EMAIL} </em>
						<input name="Data[vPaymentEmail]" id="vPaymentEmail" type="email" class="validate[required,custom[email]] profile-input" value="{$db_member[0].vPaymentEmail}" />
						<p><a href="https://www.paypal.com/" target="_blank">{$smarty.const.LBL_CREATE_PAYPAL_ACCOUNT}</a></p>
					</span>
					<span id="paymail1">
						<em> {$smarty.const.LBL_PAYMENT_EMAIL} </em>
						<input name="Data[vPaymentEmail]" id="vPaymentEmail1" type="email" class="validate[custom[email]] profile-input" value="{$db_member[0].vPaymentEmail}" />
						<p><a href="https://www.paypal.com/" target="_blank">{$smarty.const.LBL_CREATE_PAYPAL_ACCOUNT}</a></p>
					</span>


				{*	<span><em> &nbsp; </em>
						{$smarty.const.LBL_OR}
					</span>
					<span><em>{$smarty.const.LBL_ACC_HODLER} </em>
						<input name="Data[vBankAccountHolderName]" id="vBankAccountHolderName" type="text" class="profile-input" value="{$db_member[0].vBankAccountHolderName}" />
					</span>
					<span><em>{$smarty.const.LBL_ACC_NUMBER} (IBAN) </em>
						<input name="Data[vAccountNumber]" id="vAccountNumber" type="text" class="profile-input" value="{$db_member[0].vAccountNumber}" />
					</span>
					<span><em>{$smarty.const.LBL_NAME_BANK} </em>
						<input name="Data[vBankName]" id="vBankName" type="text" class="profile-input" value="{$db_member[0].vBankName}" />
					</span>
					<span><em>{$smarty.const.LBL_BANK_LOCATION} </em>
						<input name="Data[vBankLocation]" id="vBankLocation" type="text" class="profile-input" value="{$db_member[0].vBankLocation}" />
					</span>
					<span><em>{$smarty.const.LBL_BIC_SWIFT_CODE} </em>
						<input name="Data[vBIC_SWIFT_Code]" id="vBIC_SWIFT_Code" type="text" class="profile-input" value="{$db_member[0].vBIC_SWIFT_Code}" />
					</span>*}
					{/if}
					<span class="sav-but-pro"><em class="blanck-c">&nbsp;</em><a href="javascript:void(0);" onclick="javascript:checkprofile(); return false;">{$smarty.const.LBL_SUBMIT}</a></span>
				</form>
			</div>
		</div>
		{include file="member_profile_left.tpl"}
	</div>
	<!-------------------------inner-page end----------------->
	<div style="clear:both;"></div>
</div>
{literal}
<script>
	function checkprofile(){
		resp = jQuery("#frmprofile").validationEngine('validate');// alert(resp);return false;
		if(resp == true){
			document.frmprofile.submit();
			}else{
			return false;
		}
	}
	function checkMeberType(val)
	{
		//alert(val);
		if(val == "Passenger")
		{
			document.getElementById('paymail').style.display = "none";
			document.getElementById('paymail1').style.display = "block";
		}
		else if(val=="Driver" || val=="Both")
		{
			document.getElementById('paymail').style.display = "block";
			document.getElementById('paymail1').style.display = "none";
		}

		/*if(val=="Driver" || val=="Both")
			{
			$('#vPaymentEmail').addClass('validate[required, custom[email]] profile-input');
			//$('#vPaymentEmail').removeClass('validate[required]');
			}
			else if(val=="Passenger")
			{
			$('#vPaymentEmail').addClass('validate[required]');
		}*/
	}

	var	site_url = '{/literal}{$tconfig.tsite_url}{literal}';
    function get_sate(country){
		$("#state").html('Wait...');
		var request = $.ajax({
			type: "POST",
			url: site_url+'index.php?file=m-get_state',
			data: "country="+country,

			success: function(data) {
				$("#state").html(data);
				$('#vState').val('{/literal}{$db_member[0].vState}{literal}');
			}
		});

    	request.fail(function(jqXHR, textStatus) {
			alert( "Request failed: " + textStatus );
		});
	}


</script>
{/literal}
{if $db_member[0].vCountry neq ''}
{literal}
<script>
	get_sate('{/literal}{$db_member[0].vCountry}{literal}');
</script>
{/literal}
{/if}
{if $db_member[0].eMemberType neq ''}
{literal}
<script>
	checkMeberType('{/literal}{$db_member[0].eMemberType}{literal}');
</script>
{/literal}
{/if}
