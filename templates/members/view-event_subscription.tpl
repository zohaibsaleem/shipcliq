  <div class="about-page">
    <h2>My Account</h2>
  
    <div class="right-about-part">
      <h1>My Event Subscriptions</h1>
      <div class="history">
        <table width="100%" cellspacing="1" cellpadding="3" border="0" class="tabborbg">
          <tbody>
           {if $db_event_subscriptions|@count gt 0}
            <tr>
              <th width="20%" height="20">Event</th>
              <th width="20%" >Organization name</th>
              <th width="20%"  class="padding-right">Date</th>
              <th width="20%" >Status</th>
            </tr>
            {section name="i" loop=$db_event_subscriptions} 
            <tr class="oddbg">
              <td ><a href="{$tconfig.tsite_url}event-details/{$db_event_subscriptions[i].iEventId}">{$db_event_subscriptions[i].vTitle}</a></td>
              <td >{$db_event_subscriptions[i].vOrganizationName}</td>
              <td >{if $db_event_subscriptions[i].eEventType neq 'Fixed'}{$db_event_subscriptions[i].eEventType}{else}{$db_event_subscriptions[i].dStartDate}{/if}</td>
              <td >{$db_event_subscriptions[i].eStatus}</td>
            
            {/section} 
          {else}
        <span style="color:#000000;font-size:18px;margin:10px;">
        <center>           You have not subscribed for any Event yet.
        </center>
        </span> 
         {/if}        
         </tr>   
          </tbody>
        </table>
        <div style="clear:both;"></div>
      </div>
    </div>
    {include file="volunteer_left.tpl"}
	<div style="clear:both;"></div>
  </div>