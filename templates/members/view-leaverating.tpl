{if $smarty.get.var_msg neq ''}          
{if $smarty.get.msg_code eq '1'}
  {literal}
<script>        
      showNotification({type : 'success', message: '{/literal}{$var_msg}{literal}'});  
    </script>
{/literal}
{/if}
{/if} 

{if $smarty.get.var_msg neq ''}          
{if $smarty.get.msg_code eq '0'}
  {literal}
<script>
      showNotification({type : 'error', message: '{/literal}{$var_msg}{literal}'});
    </script>
{/literal}
{/if}
{/if}
<div class="body-inner-part">
  <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_RATINGS}</span></div>
  <!-------------------------inner-page----------------->
  <div class="main-inner-page">
    <h2>{$smarty.const.LBL_RATINGS}</h2>
    {include file="member_top.tpl"}
    <div class="ratings">
      <div class="ratings-inner">
        <h3 style="margin-bottom:10px;">{$smarty.const.LBL_LEAVE_RATING}</h3>
        <form name="frmbooking" id="frmbooking" method="post" action="">
          <input type="hidden" id="Member" name="Member" value="{$Member}">
          <input type="hidden" id="BookDate" name="BookDate" value="{$BookDate}">
          <div class="rides-offered">
            <div class="tab-pane">
              <ul>
                <li><a href="javascript:void(0);" onclick="getDetails(1);" {if $Member eq 'Driver'}class="active"{/if}>{$smarty.const.LBL_AS_DRIVER}</a></li>
                <li><a href="javascript:void(0);" onclick="getDetails(2);" {if $Member eq 'Booker'}class="last active"{else}class="last"{/if}>{$smarty.const.LBL_AS_BOOKER}</a></li>
              </ul>
            </div>
          </div>
        </form>
        {if $db_ride_list|@count gt 0}
        {section name= i loop=$db_ride_list}
        {if $db_ride_list[i].ratingid neq $db_ride_list[i].iBookingId}
        <div class="main-block-new">
          <div class="main-block-1">
            <h2>{$smarty.const.LBL_BOOKING_NO} #{$db_ride_list[i].vBookingNo}
              <p><img src="{$tconfig.tsite_images}member-right-i.png" alt="" /> {$generalobj->DateTimeFormat($db_ride_list[i].dBookingDate)} @ {$generalobj->DateTime($db_ride_list[i].dBookingTime,18)} </p>
            </h2>
            <span><img src="{$tconfig.tsite_images}input-iocn.png" alt="" />{$db_ride_list[i].vFromPlace} &rarr; <b>{$db_ride_list[i].vToPlace}</b><a href="javascript:void(0);" onclick="giveratingtomember('{$Member}','{$db_ride_list[i].iBookingId}','{$db_ride_list[i].iBookerId}','{$db_ride_list[i].iDriverId}');" class="give-rate" {if $sess_lang eq 'RS'}style="font-size:9px;"{/if} {if $sess_lang eq 'LV'}style="font-size:11px;"{/if}>{$smarty.const.LBL_GIVE_RATING}</a></span>
            <div class="leave-rating-list">
              <table width="100%">
                <tr>
                  <td width="30%" valign="top" style="border-right:1px solid #CCCCCC;"> {if $Member eq 'Driver'} <strong>{$smarty.const.LBL_BOOKER_DETAIL}</strong><br>
                    {$smarty.const.LBL_NAME} : <a href="{$tconfig.tsite_url}index.php?file=c-user_profile&iMemberId={$db_ride_list[i].iDriverId}">{$db_ride_list[i].vBookerFirstName} {$db_ride_list[i].vBookerLastName}</a><br>
                    {$smarty.const.LBL_PHONE} : {$db_ride_list[i].vBookerPhone}<br>
                    {$smarty.const.LBL_EMAIL} : {$db_ride_list[i].vBookerEmail}<br>
                    {else} <strong>{$smarty.const.LBL_DRIVER_DETAILS}</strong><br>
                    {$smarty.const.LBL_NAME} : <a href="{$tconfig.tsite_url}index.php?file=c-user_profile&iMemberId={$db_ride_list[i].iBookerId}">{$db_ride_list[i].vDriverFirstName} {$db_ride_list[i].vDriverLastName}</a><br>
                    {$smarty.const.LBL_PHONE} : {$db_ride_list[i].vDriverPhone}<br>
                    {$smarty.const.LBL_EMAIL} : {$db_ride_list[i].vDriverEmail}<br>
                    {/if} </td>
                  <td width="30%" valign="top" style="border-right:1px solid #CCCCCC;padding-left:5px;"><strong>{$smarty.const.LBL_PAYMENT_DETAILS}</strong><br>
                    {if $db_ride_list[i].fDocumentPrice neq 0}
                    {$smarty.const.LBL_DOCUMENT}: {$generalobj->booking_currency($db_ride_list[i].fDocumentPrice,$db_ride_list[i].vBookerCurrencyCode)}<br/>
                    {/if}
                    {if $db_ride_list[i].fBoxPrice neq 0}
                    {$smarty.const.LBL_BOX}: {$generalobj->booking_currency($db_ride_list[i].fBoxPrice,$db_ride_list[i].vBookerCurrencyCode)}<br/>
                    {/if}
                    {if $db_ride_list[i].fLuggagePrice neq 0}
                    {$smarty.const.LBL_LUGGAGE}: {$generalobj->booking_currency($db_ride_list[i].fLuggagePrice,$db_ride_list[i].vBookerCurrencyCode)}<br/>
                    {/if} <br/>
                  </td>
                  <td width="25%" valign="top" {if $db_ride_list[i].eStatus neq 'Cencelled'}style="border-right:1px solid #CCCCCC;padding-left:5px;"{else}style="padding-left:5px;"{/if}><strong>{$smarty.const.LBL_PAYMENT_BOOKING_STATUS}</strong><br>
                    {$smarty.const.LBL_PAYMENT}: {if $Member eq 'Driver'}{if $db_ride_list[i].eDriverPaymentPaid eq 'Yes'}Paid{else}Unpaid{/if}{else}{if $db_ride_list[i].eBookerPaymentPaid eq 'Yes'}Paid{else if $db_ride_list[i].eBookerPaymentPaid eq 'No'}Unpaid {else}Refund {/if}{/if} <br>
                    {$smarty.const.LBL_BOOKING}: {$db_ride_list[i].eStatus} </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
        {/if} 
        {/section}
        {else}
        <div class="main-block"> {if $Member eq 'Driver'}
          {$smarty.const.LBL_NO_BOOKING_AS_DRIVER}
          {else}
          {$smarty.const.LBL_NO_BOOKING_AS_BOOKER}
          {/if} </div>
        {/if}
        <div style="clear:both;"></div>
      </div>
    </div>
	{include file="member_rating_left.tpl"}
  </div>
  <!-------------------------inner-page end----------------->
  <div style="clear:both;"></div>
</div>
<form method="post" id="giverating" name="giverating" action="{$tconfig.tsite_url}Member-Rating">
  <input type="hidden" value="" id="iBookingId" name="iBookingId">
  <input type="hidden" value="giverating" id="action" name="action">
  <input type="hidden" value="{$iMemberId}" id="iMemberFromId" name="iMemberFromId">
  <input type="hidden" value="" id="iMemberToId" name="iMemberToId">
  <input type="hidden" value="" id="membertype" name="membertype">
</form>
{literal}
<script>
function giveratingtomember(memtype,bookingid,bookerid,driverid){
    //alert(driverid);return false;
    if(bookerid == driverid){
        alert("{/literal}{$smarty.const.LBL_RIDE_BOOKING_RATING}{literal}");
        return false;
    }
    $("#iBookingId").val(bookingid);
	  $("#membertype").val(memtype);
	  if(memtype == "Driver"){
	    $("#iMemberToId").val(bookerid);
    }else{
      $("#iMemberToId").val(driverid);
    }
	  document.giverating.submit();
    /*ans = confirm("{/literal}{$smarty.const.LBL_RIDEVALIDE_MESSAGE}{literal}");
    ans = confirm("Are You sure to give rating ?");
  	if(ans == true){
  	  $("#iBookingId").val(bookingid);
  	  $("#membertype").val(memtype);
  	  if(memtype == "Driver"){
  	    $("#iMemberToId").val(bookerid);
      }else{
        $("#iMemberToId").val(driverid);
      }
  	  document.giverating.submit();
    }else{
      return false;
    } */
} 
function findmember(){
    //alert('hello..');
    jQuery("#frmsearch").validationEngine('init',{scroll: false});
	  jQuery("#frmsearch").validationEngine('attach',{scroll: false});
		resp = jQuery("#frmsearch").validationEngine('validate');
	//	alert(resp); return false;
		if(resp == true){
			document.frmsearch.submit();
		}else{
			return false;
		}		
}
$('#frmsearch').bind('keydown',function(e){
    if(e.which == 13){
      findmember(); return false;
    }
});
function cancelsearch()
{
	window.location =  "{/literal}{$tconfig.tsite_url}{literal}leave-Rating";
	return false;
}
</script>
{/literal}


{literal}
<script>
  function getDetails(val){
    if(val==2){
       document.frmbooking.Member.value="Booker";
       document.frmbooking.submit();
    }else{
       document.frmbooking.Member.value="Driver";
       document.frmbooking.submit();
    }
  }
</script>
{/literal}