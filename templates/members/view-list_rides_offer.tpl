{if $var_msg neq ''}
  {literal}
<script>        
      showNotification({type : 'success', message: '{/literal}{$var_msg}{literal}'});  
    </script>
{/literal}
{/if}
{if $var_err_msg neq ''}
  {literal}
<script>        
      showNotification({type : 'error', message: "{/literal}{$var_err_msg}{literal}"});  
    </script>
{/literal}
{/if}
<div class="body-inner-part">
  <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_RIDES_OFFERED1}</span></div>
  <!-------------------------inner-page----------------->
  <div class="main-inner-page">
    <h2>{$smarty.const.LBL_RIDES_OFFERED1}</h2>
    <div class="dashbord"> {include file="member_top.tpl"}
      <div class="rides-offered">
        <div class="tab-pane">
          <ul>
            <li><a href="javascript:void(0);" id="Avail" onClick="show_hide('available');" class="active">{$smarty.const.LBL_AVAILABLE_RIDE}</a></li>
            <li><a href="javascript:void(0);" id="Past" onClick="show_hide('past');" class="last">{$smarty.const.LBL_PAST_RIDE}</a></li>
          </ul>
        </div>
        <div id="Avail_ride" style="display:;"> {if $avail_count gt 0}
          {section name="rideavail" loop=$db_ride_list}
          {if $db_ride_list[rideavail].type eq 'Available'}
          <div class="main-block">
          <div class="main-block-inner">
            <h2>{$db_ride_list[rideavail].vMainDeparture} &rarr; {$db_ride_list[rideavail].vMainArrival}&nbsp;({if $db_ride_list[rideavail].eRideType eq 'Reccuring'}{$smarty.const.LBL_RECURRING}{else}{$smarty.const.LBL_ONETIME}{/if})</h2>
            <div class="main-block-left">
              <!-- Hemali.. Code for category Start -->
              {if $db_ride_list[rideavail].eDocument eq 'Yes'}
              <label>
              <p> <strong style="color: #000;font-size:14px;"> {$smarty.const.LBL_DOCUMENT}: </strong>{$db_ride_list[rideavail].fDocumentPrice} ({$db_ride_list[rideavail].vDocumentWeight}) </p>
              </label>
              {/if}
              {if $db_ride_list[rideavail].eBox eq 'Yes'}
              <label>
              <p> <strong style="color: #000;font-size: 14px;"> {$smarty.const.LBL_BOX}: </strong>{$db_ride_list[rideavail].fBoxPrice} ({$db_ride_list[rideavail].vBoxWeight}) </p>
              </label>
              {/if}
              {if $db_ride_list[rideavail].eLuggage eq 'Yes'}
              <label>
              <p> <strong style="color: #000;font-size: 14px;"> {$smarty.const.LBL_LUGGAGE}: </strong>{$db_ride_list[rideavail].fLuggagePrice} ({$db_ride_list[rideavail].vLuggageWeight}) </p>
              </label>
              {/if}
              <!-- Hemali.. Code for category End -->
            </div>
            <div class="main-block-right"> {if $db_ride_list[rideavail].eRideType eq 'Reccuring'}
              <label>
              <p><strong style="color: #000;font-size: 14px;">{$smarty.const.LBL_START_FROM_DATE}:</strong>&nbsp;{$generalobj->DateTimeFormat($db_ride_list[rideavail].dStartDate)}&nbsp;&nbsp;&nbsp;<strong style="color: #000;font-size: 14px;">{$smarty.const.LBL_END_TO_DATE}:</strong>&nbsp;{$generalobj->DateTimeFormat($db_ride_list[rideavail].dEndDate)}</p>
              <!-- <span><em><strong>{$db_ride_list[rideavail].totprice}</strong> {$smarty.const.LBL_PER_PESSENGER}</em></span> -->
              </label>
              <label>
              <p><strong style="color: #000;font-size: 14px;">{$smarty.const.LBL_OUTBOUND_TIME}:</strong>&nbsp;{$generalobj->DateTime($db_ride_list[rideavail].vMainOutBoundTime,12)}&nbsp;&nbsp;&nbsp;<strong style="color: #000;font-size: 14px;">{$smarty.const.LBL_RETURN_TIME}:</strong>&nbsp;{$generalobj->DateTime($db_ride_list[rideavail].vMainReturnTime,12)}</p>
              </label>
              {else}
              <label>
              <p><strong style="color: #000;font-size: 14px;">{$smarty.const.LBL_DEPARTURE_DATE}:</strong>&nbsp;{$generalobj->DateTimeFormat($db_ride_list[rideavail].dDepartureDate)}
                {if $db_ride_list[rideavail].eRoundTrip == 'Yes'}
                &nbsp;&nbsp;&nbsp;<strong style="color: #000;font-size: 14px;">{$smarty.const.LBL_ARRIVAL_DATE}:</strong>&nbsp;{$generalobj->DateTimeFormat($db_ride_list[rideavail].dArrivalDate)}
                {/if} </p>
              <!--  <span><em><strong>{$db_ride_list[rideavail].totprice}</strong> {$smarty.const.LBL_PER_PESSENGER}</em></span> -->
              </label>
              <label>
              <p><strong style="color: #000;font-size: 14px;">{$smarty.const.LBL_DEPARTURE_TIME1}:</strong>&nbsp;{$generalobj->DateTime($db_ride_list[rideavail].vMainDepartureTime,12)} 
                {if $db_ride_list[rideavail].eRoundTrip == 'Yes'}
                &nbsp;&nbsp;&nbsp;<strong style="color: #000;font-size: 14px;">{$smarty.const.LBL_RETURN_TIME}:</strong>&nbsp;{$generalobj->DateTime($db_ride_list[rideavail].vMainArrivalTime,12)}
                {/if} </p>
              </label>
              {/if}
              {if $db_ride_list[rideavail].stop_over neq ''}
              <label>
              <p><strong style="color: #000;font-size: 14px;">{$smarty.const.LBL_STOPOVER_POINT}:</strong>{$db_ride_list[rideavail].stop_over}</p>
              </label>
              {/if} </div>
            <div class="links-main-block">
              <!--<div class="links-main-block-left"> <a href="#"><img src="{$tconfig.tsite_images}booking-blue-return.png" alt="" />Publish your return trip</a> <a href="#"><img src="{$tconfig.tsite_images}booking-blue-duplicate.png" alt="" />Duplicate</a> </div>-->
              <div class="links-main-block-right"> <a href="{$tconfig.tsite_url}index.php?file=m-create_edit&for=first&id={$db_ride_list[rideavail].iRideId}"><img src="{$tconfig.tsite_images}{$THEME}/edit.png" alt="" />{$smarty.const.LBL_VIEW_TRIP} </a> <a href="{$tconfig.tsite_url}index.php?file=c-create_duplicate&id={$db_ride_list[rideavail].iRideId}"><img src="{$tconfig.tsite_images}{$THEME}/duplicate.png" alt="" />Duplicate</a> <a href="javascript:void(0);" onClick="delete_booking({$db_ride_list[rideavail].iRideId});"><img src="{$tconfig.tsite_images}{$THEME}/delete.png" alt="" />{$smarty.const.LBL_DELETE_TRIP}</a></div>
            </div>
          </div></div>
          {/if}
          {/section}
          {else}
          <div class="main-block"> {$smarty.const.LBL_NO_RIDES}</div>
          {/if} </div>
        <div id="Past_ride" style="display:none;"> {if $past_count gt 0}
          {section name="rideavail" loop=$db_ride_list}
          {if $db_ride_list[rideavail].type eq 'Past'}
          <div class="main-block">
          <div class="main-block-inner">
            <h2>{$db_ride_list[rideavail].vMainDeparture} &rarr; {$db_ride_list[rideavail].vMainArrival}&nbsp;({if $db_ride_list[rideavail].eRideType eq 'Reccuring'}{$smarty.const.LBL_RECURRING}{else}{$smarty.const.LBL_ONETIME}{/if})</h2>
            {if $db_ride_list[rideavail].eRideType eq 'Reccuring'}
            <label>
            <p><strong style="color: #000;font-size: 14px;">{$smarty.const.LBL_START_FROM_DATE}:</strong>&nbsp;{$generalobj->DateTimeFormat($db_ride_list[rideavail].dStartDate)}&nbsp;&nbsp;&nbsp;<strong style="color: #000;font-size: 14px;">{$smarty.const.LBL_END_TO_DATE}:</strong>&nbsp;{$generalobj->DateTimeFormat($db_ride_list[rideavail].dEndDate)}</p>
            <!-- <span><em><strong>{$db_ride_list[rideavail].totprice}</strong> {$smarty.const.LBL_PER_PESSENGER}</em></span> -->
            </label>
            <label>
            <p><strong style="color: #000;font-size: 14px;">{$smarty.const.LBL_OUTBOUND_TIME}:</strong>&nbsp;{$generalobj->DateTime($db_ride_list[rideavail].vMainOutBoundTime,12)}&nbsp;&nbsp;&nbsp;<strong style="color: #000;font-size: 14px;">{$smarty.const.LBL_RETURN_TIME}:</strong>&nbsp;{$generalobj->DateTime($db_ride_list[rideavail].vMainReturnTime,12)}</p>
            </label>
            {else}
            <label>
            <p><strong style="color: #000;font-size: 14px;">{$smarty.const.LBL_DEPARTURE_DATE}:</strong>&nbsp;{$generalobj->DateTimeFormat($db_ride_list[rideavail].dDepartureDate)}
              {if $db_ride_list[rideavail].eRoundTrip == 'Yes'}
              &nbsp;&nbsp;&nbsp;<strong style="color: #000;font-size: 14px;">{$smarty.const.LBL_ARRIVAL_DATE}:</strong>&nbsp;{$generalobj->DateTimeFormat($db_ride_list[rideavail].dArrivalDate)}
              {/if} </p>
            <!-- <span><em><strong>{$db_ride_list[rideavail].totprice}</strong> {$smarty.const.LBL_PER_PESSENGER}</em></span> -->
            </label>
            <label>
            <p><strong style="color: #000;font-size: 14px;">{$smarty.const.LBL_DEPARTURE_TIME1}:</strong>&nbsp;{$generalobj->DateTime($db_ride_list[rideavail].vMainDepartureTime,12)}
              {if $db_ride_list[rideavail].eRoundTrip == 'Yes'}
              &nbsp;&nbsp;&nbsp;<strong style="color: #000;font-size: 14px;">{$smarty.const.LBL_RETURN_TIME}:</strong>&nbsp;{$generalobj->DateTime($db_ride_list[rideavail].vMainArrivalTime,12)}
              {/if} </p>
            </label>
            {/if}
            {if $db_ride_list[rideavail].stop_over neq ''}
            <label>
            <p><strong style="color: #000;font-size: 14px;">{$smarty.const.LBL_STOPOVER_POINT}:</strong>{$db_ride_list[rideavail].stop_over}</p>
            </label>
            {/if}
            <div class="links-main-block">
              <!--<div class="links-main-block-left"> <a href="#"><img src="{$tconfig.tsite_images}booking-blue-return.png" alt="" />Publish your return trip</a> <a href="#"><img src="{$tconfig.tsite_images}booking-blue-duplicate.png" alt="" />Duplicate</a> </div>-->
              <div class="links-main-block-right"> <a href="{$tconfig.tsite_url}Rides-Edit-Form/{$db_ride_list[rideavail].iRideId}"><img src="{$tconfig.tsite_images}{$THEME}/edit.png" alt="" />{$smarty.const.LBL_VIEW_TRIP} </a> <a href="{$tconfig.tsite_url}index.php?file=c-create_duplicate&id={$db_ride_list[rideavail].iRideId}"><img src="{$tconfig.tsite_images}{$THEME}/duplicate.png" alt="" />Duplicate</a></div>
            </div>
          </div></div>
          {/if}
          {/section}
          {else}
          <div class="main-block"> {$smarty.const.LBL_NO_RIDES} </div>
          {/if} </div>
        <div style="clear:both;"></div>
      </div>
      <div style="clear:both;"></div>
    </div>
    <!-------------------------inner-page end----------------->
    <div style="clear:both;"></div>
  </div>
</div>
<form name="frmoffered" method="post" action="{$tconfig.tsite_url}index.php?file=m-list_rides_offer">
  <input type="hidden" name="action" value="delete">
  <input type="hidden" name="id" id="id"  value="">
</form>
{literal}
<script>
    function show_hide(type){
      if(type == 'available'){
        $("#Avail").addClass( "active" );
        $("#Past").removeClass( "active" );
        $('#Past_ride').hide();
        $('#Avail_ride').show();
      }else{
        $("#Past").addClass( "active" );
        $("#Avail").removeClass( "active" );
        $('#Avail_ride').hide();
        $('#Past_ride').show();
      }
    }
    
    function delete_booking(id){
      var r=confirm("{/literal}{$smarty.const.LBL_SURE_DELTE_RIDE_TRIP}{literal}");
      if (r==true)
      {
        $('#id').val(id);
        document.frmoffered.submit();
        return false;
      }else{
        return false;
      }
    }
  </script>
{/literal}