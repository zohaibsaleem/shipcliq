{if $smarty.get.var_msg neq ''}          
{if $smarty.get.msg_code eq '1'}
  {literal}
    <script>        
      showNotification({type : 'success', message: '{/literal}{$var_msg}{literal}'});  
    </script>
  {/literal}
{/if}
{/if} 

{if $smarty.get.var_msg neq ''}          
{if $smarty.get.msg_code eq '0'}
  {literal}
    <script>
      showNotification({type : 'error', message: '{/literal}{$var_msg}{literal}'});
    </script>
  {/literal}
{/if}
{/if}
<div class="body-inner-part">
  <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<a href="{$tconfig.tsite_url}Member-Stories">{$smarty.const.LBL_MEMBER_STORY}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_MEMBER_STORY} {$smarty.const.LBL_FORM}</span></div>
  <!-------------------------inner-page----------------->
  <div class="main-inner-page">
    <h2>{$smarty.const.LBL_MEMBER_STORY}<span>{$smarty.const.LBL_FORM}</span></h2>
    <div class="right-inner-part">
      <h2 class="store-h">
        <em>{$smarty.const.LBL_MEMBER_STORY} {$smarty.const.LBL_FORM}</em>
        <!--<a href="#" class="tell-u">Tell us your stories</a>-->
      </h2>
      <div class="profile">
        <!-- <h2>{$smarty.const.LBL_MEMBER_STORY} {$smarty.const.LBL_FORM}</h2> -->
        <form name="frmaddstory" id="frmaddstory" action="{$tconfig.tsite_url}index.php?file=m-member_addstory_a" method="post" enctype="multipart/form-data">
        <input type="hidden" name="action" id="action" value="Add">
        <span><em>{$smarty.const.LBL_MEMBER_NAME}</em>
        {$smarty.session.sess_vFirstName} {$smarty.session.sess_vLastName}
        </span>
        <span><em>{$smarty.const.LBL_I_AM}</em>
          <select name="Data[eType]" id="eType" class="validate[required] profile-select">
              <option value="">-----{$smarty.const.LBL_SELECT_TYPE}-----</option>
                <option value="Passengers">{$smarty.const.LBL_PASSENGER}</option>
                <option value="Drivers">{$smarty.const.LBL_DRIVER1}</option>
          </select>   
        </span> 
        <span><em>{$smarty.const.LBL_SUBJECT}</em>
        <input name="Data[vTitle]" id="vTitle" type="text" class="validate[required] profile-input" value="" />
        </span> 
        <span><em>{$smarty.const.LBL_MESSAGE}</em>
        <textarea name="Data[tDescription]" id="tDescription" cols="" rows="" class="validate[required] profile-textarea"></textarea>
        </span>
        <!-- <span><em>Email</em>
        <input name="" type="text" class="profile-input" value=""/>
        </span> <span><em>Phone</em>
        <input name="" type="text" class="profile-input" value=""/>
        </span>--> 
        <span class="sav-but-pro">
        <em class="blanck-c">&nbsp;</em>
          <a href="javascript:void(0);" onclick="checkstory();">{$smarty.const.LBL_SEND}</a>
        </span> 
        </form>
      </div>
    </div>
	{include file="left.tpl"}
  </div>
  <!-------------------------inner-page end----------------->
  <div style="clear:both;"></div>
</div>
{literal}
<script>
function checkstory(){
    jQuery("#frmaddstory").validationEngine('init',{scroll: false});
	  jQuery("#frmaddstory").validationEngine('attach',{scroll: false});
	  resp = jQuery("#frmaddstory").validationEngine('validate');
    //alert(resp);return false;
		if(resp == true){
			document.frmaddstory.submit();
		}else{
			return false;
		}	
}
</script>
{/literal}    