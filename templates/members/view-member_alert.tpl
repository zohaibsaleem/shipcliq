{if $smarty.get.var_msg neq ''}
{if $smarty.get.msg_code eq '1'}
  {literal}
    <script>
      showNotification({type : 'success', message: '{/literal}{$var_msg}{literal}'});
    </script>
  {/literal}
{/if}
{/if}

{if $smarty.get.var_msg neq ''}
{if $smarty.get.msg_code eq '0'}
  {literal}
    <script>
      showNotification({type : 'error', message: '{/literal}{$var_msg}{literal}'});
    </script>
  {/literal}
{/if}
{/if}
<div class="body-inner-part">
  <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_EMAIL_ALERTS}</span></div>
  <div class="main-inner-page">
    <h2>{$smarty.const.LBL_EMAIL_ALERTS}</h2>
    <div class="dashbord">
      {include file="member_top.tpl"}
      <div class="bro-message email-alerts1">
      {if $db_alert|@count gt 0}
       <table border="0" cellpadding="5" cellspacing="0" style="width:100%;border:1px solid #CCCCCC; border-collapse: collapse;">
        <tr>
        <th style="border-bottom:1px solid #CCCCCC;border-right:1px solid #CCCCCC;">
            {$smarty.const.LBL_NO}
        </th>
        <th style="border-bottom:1px solid #CCCCCC;border-right:1px solid #CCCCCC;">
           {$smarty.const.LBL_FROM_LOCATION}
        </th>
        <th style="border-bottom:1px solid #CCCCCC;border-right:1px solid #CCCCCC;">
           {$smarty.const.LBL_TO_LOCATION}
        </th>
        <th style="border-bottom:1px solid #CCCCCC;border-right:1px solid #CCCCCC;">
           {$smarty.const.LBL_TRAVEL_DATE}
        </th>
        <th style="border-bottom:1px solid #CCCCCC;">
             {$smarty.const.LBL_ACTION}
        </th>
        </tr>

         {section name=i loop=$db_alert}
           <tr>
            <td style="text-align:center;border-bottom:1px solid #CCCCCC;border-right:1px solid #CCCCCC;">
              {$smarty.section.i.index+1}
            </td>
            <td style="border-bottom:1px solid #CCCCCC;border-right:1px solid #CCCCCC;padding-left:7px;">
              {$db_alert[i].vStartPoint}
            </td>
            <td style="border-bottom:1px solid #CCCCCC;border-right:1px solid #CCCCCC; padding-left:7px;">
              {$db_alert[i].vEndPoint}
            </td>
            <td style="border-bottom:1px solid #CCCCCC;border-right:1px solid #CCCCCC; padding-left:7px;">
              {$generalobj->DateTimeFormat($db_alert[i].dDate)}
            </td>
            <td style="text-align:center;border-bottom:1px solid #CCCCCC;">
              <a href="javascript:void(0);" onclick="deletAlert({$db_alert[i].iMemberAlertId});"><img src="{$tconfig.tpanel_img}icons/icon_delete.png" title="Delete" /></a>
            </td>
           </tr>
         {/section}
        {else}
        <div class="main-block">
          {$smarty.const.LBL_NO_ALERT_CREATED}
          </div>
        {/if}
       </table>

      </div>
      <div style="clear:both;"></div>
    </div>
    <div style="clear:both;"></div>
  </div>
  <div style="clear:both;"></div>
</div>
{literal}
<script>
function deletAlert(val){
 var res=confirm("{/literal}{$smarty.const.LBL_SURE_DELETE}{literal} ?");
 if(res==false) return false;
 window.location="{/literal}{$tconfig.tsite_url}{literal}index.php?file=m-member_alert_a&iMemberAlertId="+val;
}
</script>
{/literal}
