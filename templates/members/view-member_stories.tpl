{if $smarty.get.var_msg neq ''}
{if $smarty.get.msg_code eq '1'}
  {literal}
    <script>
      showNotification({type : 'success', message: '{/literal}{$var_msg}{literal}'});
    </script>
  {/literal}
{/if}
{/if}

{if $smarty.get.var_msg neq ''}
{if $smarty.get.msg_code eq '0'}
  {literal}
    <script>
      showNotification({type : 'error', message: '{/literal}{$var_msg}{literal}'});
    </script>
  {/literal}
{/if}
{/if}
<div class="body-inner-part">
  <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_MEMBER_STORY}</span></div>
  <!-------------------------inner-page----------------->
  <div class="main-inner-page">
    <h2>{$smarty.const.LBL_MEMBER1}<span>{$smarty.const.LBL_STORIES}</span></h2>
    <div class="right-inner-part">
      <h2 class="store-h">
        <em>{$smarty.const.LBL_MEMBER_STORY}</em>
        {if $smarty.session.sess_iMemberId neq ''}<a href="{$tconfig.tsite_url}Member-Stories-Form" class="tell-u">{$smarty.const.LBL_TELL_YOUR_STORIES}</a>{/if}
      </h2>
      <div class="member-stories">
        {if $db_passenger_stories|@count gt 0}
        <h2>{$smarty.const.LBL_BRAVOCAR_PASSENGER}</h2>
        {section name=i loop=$db_passenger_stories}
        <h3>{$db_passenger_stories[i].vFirstName},{$db_passenger_stories[i].vTitle}</h3>
        <p><img src="{$db_passenger_stories[i].img}" alt="" />
          {$db_passenger_stories[i].tDescription}
          </p>
        {/section}
        {/if}

        {if $db_driver_stories|@count gt 0}
        <h2>{$smarty.const.LBL_BRAVOCAR_DRIVERS}</h2>
        {section name=i loop=$db_driver_stories}
        <h3>{$db_driver_stories[i].vFirstName},{$db_driver_stories[i].vTitle}</h3>
        <p><img src="{$db_driver_stories[i].img}" alt="" />
          {$db_driver_stories[i].tDescription}
          </p>
        {/section}
        {/if}

      </div>
    </div>
	{include file="left.tpl"}
  </div>
  <!-------------------------inner-page end----------------->
  <div style="clear:both;"></div>
</div>
{literal}
<script>
function checkstory(){
    jQuery("#frmaddstory").validationEngine('init',{scroll: false});
	  jQuery("#frmaddstory").validationEngine('attach',{scroll: false});
	  resp = jQuery("#frmaddstory").validationEngine('validate');
    //alert(resp);return false;
		if(resp == true){
			document.frmaddstory.submit();
		}else{
			return false;
		}
}
</script>
{/literal}
