{if $smarty.get.var_msg neq ''}          
{if $smarty.get.msg_code eq '1'}
  {literal}
<script>        
      showNotification({duration: 3, autoClose: true, type : 'success', message: '{/literal}{$var_msg}{literal}'});  
    </script>
{/literal}
{/if}
{/if} 

{if $smarty.get.var_msg neq ''}          
{if $smarty.get.msg_code eq '0'}
  {literal}
<script>
      showNotification({duration: 3, autoClose: true, type : 'error', message: '{/literal}{$var_msg}{literal}'});
    </script>
{/literal}
{/if}
{/if}
<div class="body-inner-part">
  <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_RATIN_FORM}</span></div>
  <!-------------------------inner-page----------------->
  <div class="main-inner-page">
    <h2>{$smarty.const.LBL_RATING} <span>{$smarty.const.LBL_FORM}</span></h2>
    {include file="member_top.tpl"}
    <div class="ratings">
      <div class="ratings-from">
        <form name="frmmemrating" id="frmmemrating" action="{$tconfig.tsite_url}index.php?file=m-memberrating_a" method="post" enctype="multipart/form-data">
          <input type="hidden" name="action" id="action" value="giverating">
          <input type="hidden" name="Data[iMemberFromId]" id="iMemberFromId" value="{$iMemberFromId}">
          <input type="hidden" name="iMemberToId" id="iMemberToId" value="{$iMemberToId}">
          <input type="hidden" name="Data[iBookingId]" id="iBookingId" value="{$iBookingId}">
          <h3>{$smarty.const.LBL_SHARE_EXPERIENCE}</h3>
          <div class="list-choose-profil-left">
            <h2>{$db_tomember[0].vFirstName} {$db_tomember[0].vLastName} {$smarty.const.LBL_WAS} : </h2>
            <span>
            <input name="Data[eWas]" id="eWas1" type="radio" value="Passenger" class="validate[required,groupRequired[mem]]" onclick="showaddress(this.value);" {if $Member eq "Booker"}disabled {else} checked {/if} />
            &nbsp;&nbsp;{$smarty.const.LBL_APASSANGER} </span> <span>
            <input name="Data[eWas]" id="eWas2" type="radio" value="Driver" class="validate[required,groupRequired[mem]]" onclick="showaddress(this.value);" {if $Member eq "Driver"}disabled {else} checked  {/if}/>
            &nbsp;&nbsp;{$smarty.const.LBL_DRIVER} </span> <span>
            <!-- <input name="Data[eWas]" id="eWas3" type="radio" value="None" class="validate[required,groupRequired[mem]]" onclick="showaddress(this.value);" />
          &nbsp;&nbsp;{$smarty.const.LBL_I_DID_NOT_TRAVEL} {$db_tomember[0].vFirstName} {$db_tomember[0].vLastName} </span>-->
          </div>
          <div class="list-choose-profil-right"> <img src="{$db_tomember[0].img}" alt=""/> <a href="javascript:void(0);">{$db_tomember[0].vFirstName} {$db_tomember[0].vLastName} {if $db_tomember[0].iBirthYear neq 0}({$age} {$smarty.const.LBL_YEARS} {$smarty.const.LBL_OLD1}){/if} </a><br />
            <!--<img class="ra-img" src="{$tconfig.tsite_images}sprite-prefs1.png" alt="" />-->
          </div>
          <div id="experience" style="display:none;">
            <div class="main-elements-container">
              <!--<h3>Click on the stars to rate your experience</h3>
            <p><img src="{$tconfig.tsite_images}blue-star.png" alt="" />5 star (s) </p>-->
              <h3>{$smarty.const.LBL_SELECT_RATE}</h3>
              <select name="Data[iRate]" id="iRate" class="profile-select1">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
              </select>
              <label> {$smarty.const.LBL_SHARE_EXPR_NOTE} :
              <textarea name="Data[tFeedback]" id="tFeedback" cols="" rows="" class="validate[required] elements-input"></textarea>
              <!--200 characters left-->
              </label>
            </div>
          </div>
          <!-- 
        <div id="skill" style="display:none;">  
          <div class="list-choose-bottom-left">
            <h3>{$smarty.const.LBL_EVALUATE_SKILL}</h3>
            <span>                                                                                                               
            <input name="eEvaluate" id="eEvaluate" type="checkbox" value="Yes" class=""/>
            &nbsp;&nbsp;{$smarty.const.LBL_SHARE_EXP_NOTE1} </span>
            
            <div id="evaluate">     
            <span>
            <input name="eSkill" id="eSkill1" type="radio" value="Pleasant" class="validate[required,groupRequired[skll]]"/>
            &nbsp;&nbsp;{$smarty.const.LBL_PLEASANT}</span> 
            <span>
            <input name="eSkill" id="eSkill2" type="radio" value="ToImprove" class="validate[required,groupRequired[skll]]"/>
            &nbsp;&nbsp;{$smarty.const.LBL_COULD_IMPROVED}</span> 
            <span>
            <input name="eSkill" id="eSkill2" type="radio" value="Avoid" class="validate[required,groupRequired[skll]]"/>
            &nbsp;&nbsp;{$smarty.const.LBL_TO_BE_AVOIDED} </span>
            </div>
          </div>
          <div class="list-choose-bottom-right"> 
            <img src="{$db_tomember[0].img}" alt="" height="72px" width="72px"/>
             {$smarty.const.LBL_SHARE_EXP_NOTE2} 
          </div>
        </div>
-->
          <div id="publish" style="display:none;">
            <div class="pbulish-but"> <a href="javascript:void(0);" onclick="javascript:publishrating(); return false;"> {$smarty.const.LBL_PUBLISH}</a> </div>
          </div>
        </form>
      </div>
    </div>
      {include file="member_rating_left.tpl"}
  </div>
  <!-------------------------inner-page end----------------->

  <div style="clear:both;"></div>
</div>
{literal}
<script>
function publishrating(){
    //alert('hello..');
    jQuery("#frmmemrating").validationEngine('init',{scroll: false});
	  jQuery("#frmmemrating").validationEngine('attach',{scroll: false});
		resp = jQuery("#frmmemrating").validationEngine('validate');
	//	alert(resp); return false;
		if(resp == true){
			document.frmmemrating.submit();
		}else{
			return false;
		}		
}
function showaddress(val)
{
    if(val == "Passenger")
    {
         document.getElementById("experience").style.display = '';
         document.getElementById("publish").style.display = '';
         document.getElementById("skill").style.display = 'none';  
    }
    if(val == "Driver")
    {
         document.getElementById("experience").style.display = '';
         document.getElementById("publish").style.display = '';
         document.getElementById("skill").style.display = '';
    }
    if(val == "None")
    {
         document.getElementById("experience").style.display = '';
         document.getElementById("publish").style.display = '';
         document.getElementById("skill").style.display = 'none';  
    }
}  
/*function cancelsearch()
{
	window.location =  "{/literal}{$tconfig.tsite_url}{literal}leave-Rating";
	return false;
} */
$(document).ready(function(){
    //Hide div w/id extra
    $("#evaluate").css("display","");
    // Add onclick handler to checkbox w/id additional_contacts
    $("#eEvaluate").click(function(){
        // If checked
        if ($("#eEvaluate").is(":checked"))
        {
            //hide the div
            $("#evaluate").css("display","none");
        }
        else
        {
            //otherwise, show it
            $("#evaluate").css("display","");
        }
    });

});
</script>
{/literal}
{if $Member eq 'Driver'}
{literal}
<script>
showaddress("Passenger");
</script>
{/literal}
{else}
{literal}
<script>
showaddress("Driver");
</script>
{/literal}
{/if}