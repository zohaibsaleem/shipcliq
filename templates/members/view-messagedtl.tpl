<div class="body-inner-part">
  <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_MESSAGES}</span></div>
  <div class="main-inner-page">
    <h2>{$smarty.const.LBL_MESSAGES}</h2>
    <div class="dashbord">
      {include file="member_top.tpl"}
      <div class="bro-message">
        <div class="tab-pane">
          <ul>
            <!--<li> <a href="#"> Questions &amp; Answers </a> </li> -->
            <li> <a href="{$tconfig.tsute_url}received-messages"> {$smarty.const.LBL_RECEIVED_MESSAGES} <em>{$tot_unread}</em></a> </li>
            <li> <a href="{$tconfig.tsute_url}sent-messages"> {$smarty.const.LBL_SENT_MESSAGES} </a> </li>
            <li> <a class="last" href="{$tconfig.tsute_url}archived-messages"> Archived messages</a> </li>
          </ul>
        </div>
        <form id="frmmessagesend" name="frmmessagesend" action="{$tconfig.tsite_url}index.php?file=m-messagedtl_a" method="post" enctype="multipart/form-data">
        <input type="hidden" name="iThreadId" id="iThreadId" value="{$iThreadId}" />
        <input type="hidden" name="iFromMemberId" id="iFromMemberId" value="{$db_messages[0].iFromMemberId}" />
        <input type="hidden" name="iToMemberId" id="iToMemberId" value="{$db_messages[0].iToMemberId}" />
        <input type="hidden" name="from" id="from" value="{$from}" />
        <div class="aria">
          <textarea name="tMessage" id="tMessage" rows="2" class="validate[required] aria-text"></textarea>
          <a id="button_td" href="javascript:void(0);" onclick="validate_message_form();" style="display:;">{$smarty.const.LBL_POST_REPLY}</a>
          <a id="wait_td" href="javascript:void(0);" style="display:none;">{$smarty.const.LBL_WAIT}...</a>
          
          <a href="javascript:void(0);" onclick="redirectcancel();">{$smarty.const.LBL_CANCEL}</a> 
        </div>
        </form>
        <div class="back1"><a href="javascript:void(0);" onclick="redirectcancel();">{$smarty.const.LBL_BACK}</a></div>
        <div class="messages-details">
          <div class="message-top-file1"> <span>{$smarty.const.LBL_SENDER}</span><span>{$smarty.const.LBL_MESSAGE}</span> </div>
          <div id="message-file">
          {section name=rec loop=$db_messages}
          <label id="{$db_messages[rec].iMessageId}">
          <span><img src="{$db_messages[rec].img}" alt="" style="width:70px;height:70px;"/>{$db_messages[rec].vFirstName}&nbsp;{$db_messages[rec].vLastName|substr:0:1}</span>
          <p>{$db_messages[rec].tMessage|nl2br} <em>{$generalobj->DateTime($db_messages[rec].dAddedDate,7)}</em></p>
          </label>
          {/section}
          </div>           
        </div>
      </div>
      <div style="clear:both;"></div>
    </div>
    <div style="clear:both;"></div>
  </div>
  <div style="clear:both;"></div>
</div>
{literal}
<script>  
  function redirectcancel()
  {
    window.location="{/literal}{$tconfig.tsite_url}{literal}received-messages";
    return false;
  }
  
  function validate_message_form(){
    jQuery("#frmmessagesend").validationEngine('init', {promptPosition : "topLeft", scroll: false});
		jQuery("#frmmessagesend").validationEngine('attach',{promptPosition : "topLeft", scroll: false});   	
    resp = jQuery("#frmmessagesend").validationEngine('validate');         
		if(resp == true){
        document.getElementById("button_td").style.display = 'none';
        document.getElementById("wait_td").style.display = '';
			  var request = $.ajax({  
        type: "POST",
        url:  site_url+'index.php?file=m-send_message_reply',  
        data: $("#frmmessagesend").serialize(), 	  
        
        success: function(data) { //alert(data); return false;           
          var msgdata = data.split("|");
          if(msgdata[0] == 1)
          {            
            $('#message-file').prepend(msgdata[2]);
            document.frmmessagesend.reset();
            document.getElementById("wait_td").style.display = 'none';
            document.getElementById("button_td").style.display = '';              
            setTimeout( function(){ 
              document.getElementById(msgdata[1]).style.background = '#ffffff'; 
            }
            , 3000 );
          }else{
           
          }                       			
        }
      });
      
      request.fail(function(jqXHR, textStatus) {
        alert( "Request failed: " + textStatus ); 
        document.getElementById("button_td").style.display = '';
        document.getElementById("wait_td").style.display = 'none';
      });
		}else{
			return false;
		}	
  }
  
  function prependElement(parentID,child)
  {
    parent=document.getElementById(parentID);
    parent.insertBefore(child,parent.childNodes[0]);
  }
  </script>
{/literal} 