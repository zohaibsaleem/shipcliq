{literal}
<style>
.tab-pane ul {width:75%;}
</style>
{/literal}
{if $smarty.get.var_msg neq ''}
{literal}
<script>
      showNotification({type : 'success', message: '{/literal}{$smarty.get.var_msg}{literal}'});
</script>
{/literal}
{/if}

{if $smarty.get.var_msg_err neq ''}
{literal}
<script>
      showNotification({type : 'error', message: '{/literal}{$smarty.get.var_msg_err}{literal}'});
    </script>
{/literal}
{/if}
<div class="body-inner-part">
	<div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_REWARDS_SUMMARY}</span></div>
	<!-------------------------inner-page----------------->
	<div class="main-inner-page">
		<h2>Money Available</span></h2>
		{include file="member_top.tpl"}
		<div class="rides-offered">
			<div class="tab-pane">
				<ul>
			<!--  <li><a href="{$tconfig.tsite_url}index.php?file=m-money_detail&type=available"  {if $smarty.get.type eq 'available'}class="active"{/if}> Your Money Available</a></li>
				  <li><a href="{$tconfig.tsite_url}index.php?file=m-money_detail&type=history"  {if $smarty.get.type  eq 'history'}class="last active"{else}class="last"{/if}>History of Your Transfer</a></li>
				  <li><a href="{$tconfig.tsite_url}index.php?file=m-money_detail&type=refund"  {if $smarty.get.type  eq 'refund'}class="last active"{else}class="last"{/if}>Your Refund</a></li>-->
				   <li><a href="{$tconfig.tsite_url}index.php?file=m-money_detail&pagetype=money&type=available"  {if $smarty.get.pagetype eq 'money'}class="active"{/if}>{$smarty.const.LBL_AS_DRIVER}</a></li>
				   <li><a href="{$tconfig.tsite_url}index.php?file=m-payment_detail&pagetype=payment&type=available"  {if $smarty.get.pagetype eq 'payment'}class="active"{/if}>{$smarty.const.LBL_AS_BOOKER}</a></li>
				</ul>
				<select name="type[]" onChange="return travelerChange(this.value);" class="booking_detail_dropdown">
				   <option value="available" {if $smarty.get.type eq 'available'}selected{/if} >{$smarty.const.LBL_PENDING_PAYMENTS}</option>
				   <option value="history" {if $smarty.get.type eq 'history'}selected{/if}>{$smarty.const.LBL_PAYMENTS_HISTORY}</option>
				   <option value="refund" {if $smarty.get.type eq 'refund'}selected{/if}>{$smarty.const.LBL_REFUNDS}</option>
				</select>
            </div>
        </div>

		<div class="">
			<form name="frmbooking" id="frmbooking" method="post" action="">
				<input type="hidden" id="type" name="type" value="{$type}">
				<input type="hidden" id="action" name="action" value="send_equest">
				<input type="hidden"  name="eTransRequest" id="eTransRequest" value="">
				<input type="hidden"  name="iBookingId" id="iBookingId" value="">
				<div class="rating-received2">
					<div class="rating-given-right-part">
						<table width="100%" border="0" cellpadding="10" cellspacing="0" class="credit-p">
							{if $db_money|@count gt 0}
								<tr style="background:#efefef;">
									{if $smarty.get.type eq 'available'}
										<th width="1%" align="center" class="credits"></th>
									{/if}
									<th width="1%" align="center" class="credits">Booking No. </th>
									<th width="6%" align="center" class="credits">Booking Date</th>
									<th width="40%" align="center" class="credits">Details</th>
									<th width="3%" align="center" valign="top" class="credits">Amount</th>
									{if $smarty.get.type eq 'available'}
										<th width="10%" align="center" valign="top">Status</th>
									{else}
										<th width="10%" align="center" valign="top"> Status</th>
									{/if}
								</tr>
								{section name=rec1 loop=$db_money}
									<tr {$db_money[rec1].trstyle}>
										{if $smarty.get.type eq 'available'}
											<!--<td><input name="iBookingId[]" type="checkbox" id="iBookingId_{$db_money[rec1].iBookingId}" value="{$db_money[i].iBookingId}" /></td-->
											<td><input class="validate[required]" type="checkbox" value="{$db_money[rec1].iBookingId}" id="iBookingId_{$db_money[rec1].iBookingId}" name="iBookingId[]" {if $db_money[rec1].eTransRequest eq 'Yes'} checked  disabled="disabled" {/if}>
										{/if}
										<td align="center" class="credits">{$db_money[rec1].vBookingNo}</td>
										<td align="center" class="credits">{$generalobj->DateTime($db_money[rec1].dBookingDate,9)}</td>
										<td align="center" class="credits">{$db_money[rec1].vMainRidePlaceDetails}</td>
										<td align="center" class="credits">{$db_money[rec1].fAmount} {$db_money[rec1].vBookerCurrencyCode}</td>
										{if $smarty.get.type eq 'available'}
											<td align="center" class="credits">
												{if $db_money[rec1].eTransRequest eq 'No'}
													Transfer request not made
												{else}
													Requested for transfer
												{/if}
											</td>
										{else}
											<td align="center" class="credits">Transfered</td>
										{/if}
									</tr>
								{/section}
								<tr style="background:#efefef;">
									{if $db_money|@count gt 0}
										{if $type eq 'available'}
										  <td colspan="4" style="text-align:right;padding-right:5px;"><b>Total :</b> </td>
										{else}
										   <td colspan="3" style="text-align:right;padding-right:5px;"><b>Total earned Money :</b></td>
										{/if}
										<td align="center" colspan="2">{$total} {$db_money[0].vBookerCurrencyCode}</td>
									{/if}
								</tr>
							{else}
								<tr>
									{if $type eq 'available'}
										<td colspan="5">
											<div class="main-block">
												No Records Found.
											</div>
										</td>
									{else}
										<td colspan="5">
											<div class="main-block">
												No Records Found.
											</div>
										</td>
									{/if}
								</tr>
							{/if}
							{if $db_money|@count gt 0}
								{if $type eq 'available'}
									<tr  style="background:#efefef;">
										<td colspan="6">
											<span class="singlerow-login-log" style="float:right;padding:5px;background:#efefef;"><a href="javascript:void(0);" onclick="javascript:check_skills_edit(); return false;">Send transfer Request</a></span>
										</td>
									</tr>
								{/if}
							{/if}
						</table>
					</div>
				</div>
			</form>
		</div>
	</div>
	<!-------------------------inner-page end----------------->
	<div style="clear:both;"></div>
</div>
{literal}
<script>
function getCheckCount(frmbooking)
{
	var x=0;
	for(i=0;i < frmbooking.elements.length;i++)
	{	if ( frmbooking.elements[i].checked == true)
			{x++;}
	}
	return x;
}
function check_skills_edit(){
  y = getCheckCount(document.frmbooking);
  if(y>0)
	{
    jQuery("#frmbooking").validationEngine('attach',{scroll: false});
    resp = jQuery("#frmbooking").validationEngine('validate');
    if(resp == true){
     $("#eTransRequest").val('Yes');
        document.frmbooking.submit();
    }else{
       return false;
    }
  }
  else{
      alert("Select Ride for send transfer request")
      return false;
      }
  }
  function travelerChange(type)
  {

	  if(type=='available')
		  window.location="index.php?file=m-money_detail&type=available&pagetype=money";
	  if(type=='history')
		    window.location="index.php?file=m-money_detail&type=history&pagetype=money";
	  if(type=='refund')
		    window.location="index.php?file=m-money_detail&type=refund&pagetype=money";
  }
</script>
{/literal}
