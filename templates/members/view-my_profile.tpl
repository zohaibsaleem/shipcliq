{if $msgtext neq ''}{literal}<script>show_msg_skills('{/literal}{$msgtext}{literal}'); </script>{/literal}{/if}
{if $msgtext1 neq ''}{literal}<script>show_msg_int('{/literal}{$msgtext1}{literal}');</script>{/literal}{/if}
<div class="about-page">
    <h2>Student Profile</h2>
    <div class="student-profile">
      <div class="student-account-left">
        <div class="member-left"><img src="{$volunteer[0].img_url}" alt="" /></div>
        <span>Member Since {$generalobj->DateTime($volunteer[0].dAddedDate,20)}  (Points: {$total_points}) </span>
        {if $sess_iMemberId eq $iMemberId}
        <div class="infor-stud">
          <ul>
            <li><a href="{$tconfig.tsite_url}volunteer-account">Edit Profile</a></li>
            <li><a href="{$tconfig.tsite_url}volunteer-account#vImage">Change | Remove picture</a></li>
            <li><a href="{$tconfig.tsite_url}volunteer-change-password">Change Password</a></li>
            <li><a href="{$tconfig.tsite_url}volunteer-event-subscriptions">My Events Subscription</a></li>
            <li><a href="{$tconfig.tsite_url}volunteer-points">My Points</a></li>
            <li class="last"><a href="#">Manage Notifications</a></li>
          </ul>
        </div>
        {/if}
      </div>
      
      <div class="student-profile-right">
        <h2>{$volunteer[0].vFirstName}&nbsp;{$volunteer[0].vLastName} <em>Student ID#: {$volunteer[0].iMemberId}</em></h2>
        <div class="clint-top-link">{if $volunteer[0].vCity neq '' && $volunteer[0].vState neq ''}<img alt="" src="{$tconfig.tsite_images}location.png"> <a href="#">{$volunteer[0].vCity}, {$volunteer[0].vState}</a> {/if}</div>
        {if $sess_iMemberId eq $iMemberId} 
        <div class="skills1">           
            <h2>Skills</h2>
            {section name="skills" loop=$my_profile_skills}            
            <label>{$my_profile_skills[skills].vTitle}</label>        
           {/section}           
          <em> <a href="{$tconfig.tsite_url}volunteer-account#edit_skills">Edit</a></em>
        </div>
        {else}
        {if $my_profile_skills|@count gt 0}
        <div class="skills1">           
            <h2>Skills</h2>
            {section name="skills" loop=$my_profile_skills}            
            <label>{$my_profile_skills[skills].vTitle}</label>        
           {/section} 
        </div>
        {/if}
        {/if}
        {if $sess_iMemberId eq $iMemberId}
        <div class="Interests">
          <h2>Interests</h2>
            {section name="i" loop=$volunteer_interests}
          <span>{if $volunteer_interests[i].vCategoryImage neq ''}<img src="{$volunteer_interests[i].img_url}" alt="" />{/if}</span>{/section} 
          <em><a href="{$tconfig.tsite_url}volunteer-account#edit_interests">Edit</a></em>   
        </div>
        {else}
        {if $volunteer_interests|@count gt 0}
        <div class="Interests">
          <h2>Interests</h2>
          {section name="i" loop=$volunteer_interests}
          <span>{if $volunteer_interests[i].vCategoryImage neq ''}<img src="{$volunteer_interests[i].img_url}" alt="" />{/if}</span>
          {/section}               
        </div>
        {/if}
        {/if}
        <div class="clear"></div>
      </div>
    </div>
    <div class="tabs">
      <div class="profile-tab">
        <ul>
          <li><a href="{$tconfig.tsite_url}volunteer-profile/{$iMemberId}" class="active" id="attended"> Event Attended </a></li>
        <!--  <li><a href="javascript:void(0);" onClick="tab_volunteer_profile('Points',{$iMemberId});" class="" id="points_earned">Points Earned</a></li> -->
        </ul>
        <div id="main_loader" style="display:none;margin-left: 305px;margin-top: 169px;position: absolute;"><img src="{$tconfig.tsite_images}252 (1).gif"></div>
        <div class="clear"></div>
      </div>
      
       <div id="main_tab" class="tabs-produ">
        <div class="student-tabs">
          {if $events_attended|@count gt 0}
        {section name="i" loop=$events_attended}
        {if $events_attended[i].fPoints neq '0.00' || $events_attended[i].dHours neq '0.00'}
          <div class="organization-event-listing-details"{if $smarty.section.i.iteration eq $smarty.section.i.last}style="border-bottom:none;"{/if}>
            <div class="organization-event-img"><a href="{$tconfig.tsite_url}event-details/{$events_attended[i].iEventId}"><img src="{$events_attended[i].img_url}" alt="" /></a></div>
            <div class="organization-event-listing-right">
              <h2><a href="{$tconfig.tsite_url}event-details/{$events_attended[i].iEventId}">{$events_attended[i].vTitle}</a></h2>
              <h3>{$events_attended[i].vOrganizationName}</h3>
              {if $events_attended[i].fPoints neq '0.00'}
              <p><strong>Points Earned: </strong>{$events_attended[i].fPoints}</p> {/if}
              {if $events_attended[i].dHours neq '0.00'}
              <p><strong>Hours Served: </strong>{$events_attended[i].dHours}</p> {/if}
              <p>{$events_attended[i].tDetails|truncate:200}</p>
              <span><img src="{$tconfig.tsite_images}con-icon.jpg" alt="" /> {$events_attended[i].vCity}, {$events_attended[i].vState} <a href="{$tconfig.tsite_url}event-details/{$events_attended[i].iEventId}">View More</a></span> </div>
          </div>
             {/if}
             {/section}
         {else}
        <span style="color:#000000;font-size:18px;margin:10px;">
        <center>           Volunteer have not attended any Event yet.
        </center>
        </span> 
         {/if}
        </div>
      </div>
    </div>
    <div style="clear:both;"></div>
  
  </div>