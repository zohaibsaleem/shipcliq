<script type="text/javascript" src="{$tconfig.tsite_stylesheets}front/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="{$tconfig.tsite_stylesheets}{$THEME}/front/jquery.fancybox.css?v=2.1.5" media="screen" />
<div class="body-inner-part">
	<div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_MY_BOOKINGS}</span></div>
	<!-------------------------inner-page----------------->
	<div class="main-inner-page">
		<h2 class="page-heading-in">{$smarty.const.LBL_MY_BOOKINGS}</h2>
		<div class="dashbord">
			<form name="frmbooking" id="frmbooking" method="post" action="">
				<input type="hidden" id="Member" name="Member" value="{$Member}">
				<input type="hidden" id="BookDate" name="BookDate" value="{$BookDate}">
				{include file="member_top.tpl"}
				<div class="rides-offered">
					<div class="tab-pane">
						<ul>
							<li><a href="javascript:void(0);" onclick="getDetails(1);" {if $Member eq 'Driver'}class="active"{/if}>{$smarty.const.LBL_AS_DRIVER}</a></li>
							<li><a href="javascript:void(0);" onclick="getDetails(2);" {if $Member eq 'Booker'}class="last active"{else}class="last"{/if}>{$smarty.const.LBL_AS_BOOKER}</a></li>
						</ul>
						<span style="float:right;">
							<select id="bookDate" name="bookDate" onchange="getDateWise(this.value);" style="background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #CCCCCC;margin: 0;margin-right:4px; padding: 7px;width: 145px;">
								<option value="">-- {$smarty.const.LBL_SELECT} --</option>
								<option value="1" {if $BookDate eq 'Past'}selected {/if}>{$smarty.const.LBL_PAST_BOOKING}</option>
								<option value="2" {if $BookDate eq 'Today'}selected {/if}>{$smarty.const.LBL_TODAY_BOOKING}</option>
								<option value="3" {if $BookDate eq 'Futur'}selected {/if}>{$smarty.const.LBL_FUTURE_BOOKING}</option>
								<option value="5" {if $BookDate eq 'Canc'}selected {/if}>{$smarty.const.LBL_CANCEL_BOOKING}</option>
								<option value="4" {if $BookDate eq 'All'}selected {/if}>{$smarty.const.LBL_SHOW_ALL}</option>
							</select>
						</span>
					</div>
				</div>
			</form>
			{if $db_ride_list|@count gt 0}
			{section name=i loop=$db_ride_list}
			<div class="main-block-new">
				<div class="main-block-1" style="{if $BookDate eq 'Canc'}{else}{if $db_ride_list[i].bookingtype eq 'Avail'}{else}{/if}{/if}">
					<h2>{$smarty.const.LBL_BOOKING_NO} #{$db_ride_list[i].vBookingNo}
						<p><img src="{$tconfig.tsite_images}member-right-i.png" alt="" /> {$generalobj->DateTimeFormat($db_ride_list[i].dBookingDate)} @ {$generalobj->DateTime($db_ride_list[i].dBookingTime,18)}</p>
						<span>
							<img src="{$tconfig.tsite_images}input-iocn.png" alt="" />{$db_ride_list[i].vFromPlace} &rarr; <b>{$db_ride_list[i].vToPlace}</b> <strong>
								{if $Member eq 'Booker'}
								[ {$smarty.const.LBL_CONFIRM_BOOKING_CODE} :
								<b>{$db_ride_list[i].vVerificationCode}</b> ]
								{/if}
								<!--<img src="{$tconfig.tsite_images}empty-seat.png" alt="" /> : {$db_ride_list[i].iNoOfSeats}-->
							</strong>
						</span>
						</h2>
						<div class="leave-rating-list">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td width="20%" valign="top" style="border-right:1px solid #CCCCCC;">
										{if $Member eq 'Driver'}
										<strong>{$smarty.const.LBL_BOOKER_DETAIL}</strong><br>
										{$smarty.const.LBL_NAME} : <a href="{$tconfig.tsite_url}index.php?file=c-user_profile&iMemberId={$db_ride_list[i].iBookerId}">{$db_ride_list[i].vBookerFirstName} {$db_ride_list[i].vBookerLastName}</a><br>
										{$smarty.const.LBL_PHONE} : {$db_ride_list[i].vBookerPhone}<br>
										{$smarty.const.LBL_EMAIL} : {$db_ride_list[i].vBookerEmail}<br>
										{else}
										<strong>{$smarty.const.LBL_DRIVER_DETAILS}</strong><br>
										{$smarty.const.LBL_NAME} : <a href="{$tconfig.tsite_url}index.php?file=c-user_profile&iMemberId={$db_ride_list[i].iDriverId}">{$db_ride_list[i].vDriverFirstName} {$db_ride_list[i].vDriverLastName}</a><br>
										{$smarty.const.LBL_PHONE} : {$db_ride_list[i].vDriverPhone}<br>
										{$smarty.const.LBL_EMAIL} : {$db_ride_list[i].vDriverEmail}<br>
										{/if}
									</td>
									<td width="30%" valign="top" style="border-right:1px solid #CCCCCC;padding-left:5px;">
										<strong>{$smarty.const.LBL_PAYMENT_DETAILS}</strong><br>
										{if $db_ride_list[i].fDocumentPrice neq 0  && $db_ride_list[i].vDocumentWeight neq ''}
										<strong>{$smarty.const.LBL_DOCUMENT}:</strong> 	<!-- {$generalobj->booking_currency($db_ride_list[i].fDocumentPrice,$db_ride_list[i].vBookerCurrencyCode)} ->  --><br/>{$generalobj->perbooking($db_ride_list[i].fDocumentPrice, $db_ride_list[i].vDocumentWeight,$db_ride_list[i].vDocumentUnit,$db_ride_list[i].vBookerCurrencyCode)}
										<br/>
										{/if}
										{if $db_ride_list[i].fBoxPrice neq 0 && $db_ride_list[i].vBoxWeight neq ''}
										<strong>{$smarty.const.LBL_BOX}:</strong>
										<br/>{$generalobj->perbooking($db_ride_list[i].fBoxPrice, $db_ride_list[i].vBoxWeight,$db_ride_list[i].vBoxUnit,$db_ride_list[i].vBookerCurrencyCode)}<br/>
										{/if}
										{if $db_ride_list[i].fLuggagePrice neq '0' && $db_ride_list[i].vLuggageWeight neq ''}
										<strong>{$smarty.const.LBL_LUGGAGE}:</strong>
										<br/>{$generalobj->perbooking($db_ride_list[i].fLuggagePrice, $db_ride_list[i].vLuggageWeight,$db_ride_list[i].vLuggageUnit,$db_ride_list[i].vBookerCurrencyCode)}<br/>
										{/if}
										<br/>
									</td>
									{if $smarty.const.PAYMENT_OPTION eq 'PayPal'}
									<td width="20%" valign="top" {if $db_ride_list[i].eStatus neq 'Cancelled'}style="border-right:1px solid #CCCCCC;padding-left:5px;"{else}style="padding-left:5px;"{/if}>
										<strong>{$smarty.const.LBL_PAYMENT_BOOKING_STATUS}</strong><br>
										{$smarty.const.LBL_PAYMENT} : {if $Member eq 'Driver'}{if $db_ride_list[i].eDriverPaymentPaid eq 'Yes'}Paid{else}Unpaid{/if}{else}{if $db_ride_list[i].eBookerPaymentPaid eq 'Yes'}Paid{else if $db_ride_list[i].eBookerPaymentPaid eq 'No'}Unpaid {else}Refund {/if}{/if} <br>
										{$smarty.const.LBL_BOOKING} :  {$db_ride_list[i].eStatus}<br>
										{$smarty.const.LBL_PRICE} : {$generalobj->booking_currency($db_ride_list[i].fAmount,$db_ride_list[i].vBookerCurrencyCode)}
									</td>
									{else}
									<td width="20%" valign="top" {if $db_ride_list[i].eStatus neq 'Cancelled'}style="border-right:1px solid #CCCCCC; padding-left:5px;"{else}style="padding-left:5px;"{/if}>
										<strong>{$smarty.const.LBL_BOOKING_HEADING}</strong><br>
										{$smarty.const.LBL_BOOKING} :  {$db_ride_list[i].eStatus}<br>
										{$smarty.const.LBL_PRICE} : {$generalobj->booking_currency($db_ride_list[i].fAmount,$db_ride_list[i].vBookerCurrencyCode)}
									</td>
									{/if}
									{if $db_ride_list[i].eStatus neq 'Cancelled' && $db_ride_list[i].bookingtype eq 'Avail'}
									<!-- Changed by Hemali for booking code -->
									{if $db_ride_list[i].eBookerConfirmation eq 'No' && $db_ride_list[i].eStatus neq 'Cancelled' && $db_ride_list[i].cancelbutton eq 'Yes'}
									<td width="30%" valign="bottom" style="padding-left:5px;">
										<a href="javascript:void(0);" onClick="cancel_booking_conf({$db_ride_list[i].iBookingId});" class="canbooking" style="fount-size:12px;margin-top:2px;float:right;">{$smarty.const.LBL_CANCEL_BOOKING}</a>
									</td>
									{/if}
									{if $Member eq 'Booker' && $db_ride_list[i].eBookerConfirmation eq 'No' && $db_ride_list[i].eStatus neq 'Cancelled'}
									<!-- <td width="15%" valign="bottom" style="padding-left:5px;"><a href="javascript:void(0);" onclick="validateride('{$db_ride_list[i].iBookingId}');" class="validride">{$smarty.const.LBL_VALIDATE_RIDE}</a> </td> -->
									{/if}
									{else if $db_ride_list[i].eStatus neq 'Cancelled'}
									{if $smarty.session.sess_iMemberId EQ $db_ride_list[i].iDriverId && $Member eq 'Driver' && $db_ride_list[i].eBookerConfirmation eq 'No' && $db_ride_list[i].eStatus neq 'Cancelled'}
									<td width="30%" valign="bottom" style="padding-left:5px;">
										<table width="100%" border="0">
											<tr>
												<td width="50%" valign="middle">
													<input type="text" name="vCodeDriver_{$db_ride_list[i].iBookingId}" id="vCodeDriver_{$db_ride_list[i].iBookingId}" class="validation[required] profile-input1" required onkeypress="checkEnter()">
												</td>
												<td width="50%">
													<a href="javascript:void(0);" onClick="validateride('{$db_ride_list[i].iBookingId}');"  class="canbooking" style="fount-size:12px;margin-top:2px;float: left;">{$smarty.const.LBL_CONFIRM_BOOKING_BY_DRIVER}</a>
												</td>
											</tr>
											<tr>
												<td width="50%">
													{$smarty.const.LBL_CONFIRM_BOOKING_CODE_MSG}
												</td>
												<!-- <td width="50%" style="padding-left:5px;">
													<a href="javascript:void(0);" onClick="cancel_booking_conf({$db_ride_list[i].iBookingId});" class="canbooking" style="fount-size:12px;margin-top:2px;float: left;">{$smarty.const.LBL_CANCEL_BOOKING}</a>
												</td> -->
											</tr>
										</table>
									</td>
									{/if}
									{/if}
								</tr>
							</table>
						</div>
					</div>
				</div>
				{/section}
				{else}
				<div class="main-block"> {if $Member eq 'Driver'}
					{$smarty.const.LBL_NO_BOOKING_AS_DRIVER}
					{else}
					{$smarty.const.LBL_NO_BOOKING_AS_BOOKER}
				{/if} </div>
				{/if}
				{if $db_ride_list|@count gt 0}
				<div class="paging">{$page_link}</div>
			{/if} </div>
			<div style="clear:both;"></div>
		</div>
		<div style="clear:both;"></div>
		<!-------------------------inner-page end----------------->
		<div style="clear:both;"></div>
	</div>
	<form method="post" id="validbookerride" name="validbookerride" action="{$tconfig.tsite_url}my-bookings">
		<input type="hidden" value="" id="bookingid" name="bookingid">
		<input type="hidden" value="validbookerride" id="action" name="action">
		<input type="hidden" value="" id="vCode" name="vCode">
	</form>
	<div style="display:none">
		<div id="bookingcancel" class="form-login">
			<h4>{$smarty.const.LBL_BOOK_CANCEL}</h4>
			<form method="post" id="book_cancel" name="book_cancel" action="{$tconfig.tsite_url}my-bookings">
				<input type="hidden" value="" id="eCancelBy" name="eCancelBy">
				<input type="hidden" value="" id="ibookid" name="ibookid">
				<input type="hidden" value="cancelbooking" id="action" name="action">
				<div class="inner-form1"> <span><strong>{$smarty.const.LBL_REASON} :</strong>
					<textarea value="" id="tCancelreason" name="tCancelreason" class="validate[required] login-ageinput-po"></textarea>
				</span>
				<div class="singlerow-login-log"><a href="javascript:void(0);" onclick="check_cancel();">{$smarty.const.LBL_CONFIRM}</a></div>
				<div style="clear:both;"></div>
				</div>
			</form>
		</div>
	</div>
	{literal}
	<script>
		function getDetails(val){
			if(val==2){
				document.frmbooking.Member.value="Booker";
				document.frmbooking.submit();
				}else{
				document.frmbooking.Member.value="Driver";
				document.frmbooking.submit();
			}
		}
		function getDateWise(val){
			if(val==1){
				document.frmbooking.BookDate.value="Past";
				document.frmbooking.submit();
				}else if(val==2){
				document.frmbooking.BookDate.value="Today";
				document.frmbooking.submit();
				}else if(val==3){
				document.frmbooking.BookDate.value="Futur";
				document.frmbooking.submit();
				}else if(val==4){
				document.frmbooking.BookDate.value="All";
				document.frmbooking.submit();
				}else if(val==5){
				document.frmbooking.BookDate.value="Canc";
				document.frmbooking.submit();
			}
		}

		function cancelbooking(membertype,bookingid){
			//ans = confirm("Confirm to cancel selected booking?");
			ans = confirm("{/literal}{$smarty.const.LBL_CONFIRM_CANCEL|escape}{literal}");
			if(ans == true){
				//alert(bookingid);return false;
				$("#eCancelBy").val(membertype);
				$("#ibookid").val(bookingid);
				$.fancybox("#bookingcancel");return false;
				}else{
				return false;
			}
		}

		function validateride(bookingid)
		{
			//alert(bookingid);return false;
			var id_name = "#vCodeDriver_"+bookingid;
			var vCodeDriver = $(id_name).val();
			//alert(id_name+'=='+vCodeDriver);
			if(vCodeDriver == null || vCodeDriver == '')
			{
				alert('{/literal}{$smarty.const.LBL_CONFIRM_BOOKING_CODE_MSG|escape}{literal}');
				return false;
			}
			else
			{
				ans = confirm("{/literal}{$smarty.const.LBL_RIDEVALIDE_MESSAGE|escape}{literal}");

				if(ans == true)
				{
					$("#bookingid").val(bookingid);

					$("#vCode").val(vCodeDriver);
					document.validbookerride.submit();
				}
				else
				{
					return false;
				}
			}

		}
		/*prevent form to submit on keypress */
		function checkEnter(e){
			e = e || event;
			var txtArea = /textarea/i.test((e.target || e.srcElement).tagName);
			return txtArea || (e.keyCode || e.which || e.charCode || 0) !== 13;
		}

		function check_cancel(){
			jQuery("#book_cancel").validationEngine('init',{scroll: false});
			jQuery("#book_cancel").validationEngine('attach',{scroll: false});
			resp = jQuery("#book_cancel").validationEngine('validate');

			if(resp == true){
				$.fancybox.close();
				document.book_cancel.submit();
				}else{
				return false;
			}
		}

		function cancel_booking_conf(id){
			var r = confirm("{/literal}{$smarty.const.LBL_CONFIRM_CANCEL|escape}{literal}");
			if (r == true) {
				window.location = "{/literal}{$tconfig.tsite_url}{literal}index.php?file=m-booking_cancelation_form&id="+id;
				} else {
				return false;
			}
		}
	</script>
	{/literal}

	{if $smarty.get.var_msg neq ''}
	{if $smarty.get.msg_code eq '1'}
	{literal}
	<script>
		$( document ).ready(function(){
			showNotification({type : 'success', message: '{/literal}{$var_msg}{literal}'});
		});
	</script>
	{/literal}
	{/if}
	{/if}

	{if $smarty.get.var_msg neq ''}
	{if $smarty.get.msg_code eq '0'}
	{literal}
	<script>
		$( document ).ready(function(){
			showNotification({type : 'error', message: '{/literal}{$var_msg}{literal}'});
		});
	</script>
	{/literal}
	{/if}
{/if}
