{if $smarty.get.var_msg neq ''}          
{if $smarty.get.msg_code eq '1'}
  {literal}
    <script>        
      showNotification({type : 'success', message: '{/literal}{$var_msg}{literal}'});  
    </script>
  {/literal}
{/if}
{/if} 

{if $smarty.get.var_msg neq ''}          
{if $smarty.get.msg_code eq '0'}
  {literal}
    <script>
      showNotification({type : 'error', message: '{/literal}{$var_msg}{literal}'});
    </script>
  {/literal}
{/if}
{/if} 
<div class="body-inner-part">
  <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_NOTIFICATION1}</span></div>
  <!-------------------------inner-page----------------->
  <div class="main-inner-page">
    <h2>{$smarty.const.LBL_NOTIFICATION1} </h2>
    {include file="member_top.tpl"}
    <div class="ratings">
    <!--
      <div class="notifications">
        <div class="notifications-top-part">
          <h2>SMS {$smarty.const.LBL_NOTIFICATION1} <span>/ {$smarty.const.LBL_SEND_TO} <strong>07400 215478</strong> </span></h2>
          <p><strong>{$smarty.const.LBL_RECEIVE_SMS} :</strong> <em>
            <input name="" type="checkbox" value="" />
            &nbsp;&nbsp;{$smarty.const.LBL_NOTIFICATION_NOTE} </em> <a href="#">{$smarty.const.LBL_SAVE}</a> </p>
        </div>
      </div>
      -->
      <div class="notifications">
        <div class="notifications-bottom-part">
        <form name="frmnotification" enctype="multipart/form-data" id="frmnotification" action="{$tconfig.tsite_url}index.php?file=m-notification_a" method="post">
        {if $db_member_notification|count gt 0}
        <input type="hidden" name="action" id="action" value="Update">
        {else}
        <input type="hidden" name="action" id="action" value="Add">
        {/if}
          <h2>{$smarty.const.LBL_EMAIL_NOTIFICATION} <span>/ {$smarty.const.LBL_EMAIL_NOTI_SENT} <strong>{$db_member[0].vEmail}</strong> </span></h2>
          <ul>
            <h2>{$smarty.const.LBL_RECEIVE_EMAIL_NOTI} :</h2>
            <li>
              <input name="eSuccessPublish" id="eSuccessPublish" type="checkbox" value="Yes" class="radio-input" {if $db_member_notification[0].eSuccessPublish eq 'Yes'}checked {/if}/>
              {$smarty.const.LBL_EMAIL_NOTI_NOTE1} </li>
            <li>
              <input name="eSuccessUpdate" id="eSuccessUpdate" type="checkbox" value="Yes" class="radio-input" {if $db_member_notification[0].eSuccessUpdate eq 'Yes'}checked {/if}/>
              {$smarty.const.LBL_EMAIL_NOTI_NOTE2} </li>
            <li>
              <input name="ePrivateMessage" id="ePrivateMessage" type="checkbox" value="Yes" class="radio-input" {if $db_member_notification[0].ePrivateMessage eq 'Yes'}checked {/if}/>
              {$smarty.const.LBL_EMAIL_NOTI_NOTE3} </li>
            <li>
              <input name="eRatePassenger" id="eRatePassenger" type="checkbox" value="Yes" class="radio-input" {if $db_member_notification[0].eRatePassenger eq 'Yes'}checked {/if}/>
              {$smarty.const.LBL_EMAIL_NOTI_NOTE4} </li>
            <li>
              <input name="eNewRating" id="eNewRating" type="checkbox" value="Yes" class="radio-input" {if $db_member_notification[0].eNewRating eq 'Yes'}checked {/if}/>
              {$smarty.const.LBL_EMAIL_NOTI_NOTE5} </li>
          </ul>
          <ul>
            <h2>{$smarty.const.LBL_RECEIVE_EMAIL} :</h2>
            <li>
              <input name="eOtherInformation" id="eOtherInformation" type="checkbox" value="Yes" class="radio-input" {if $db_member_notification[0].eOtherInformation eq 'Yes'}checked {/if}/>
              {$smarty.const.LBL_RECEIVE_EMAIL_NOTE} </li>
          </ul>
          <ul>
            <h2>{$smarty.const.LBL_MY_ALERTS}</h2>
            <li class="my-alerts">{$smarty.const.LBL_MY_ALERTS_NOTE} <br />
              <a href="{$tconfig.tsite_url}Email-Alerts">{$smarty.const.LBL_SEE_RIDE_ALERT}</a></li>
          </ul>
          <div class="notification-save"><a href="javascript:void(0);" onclick="javascript:checknotification(); return false;">{$smarty.const.LBL_SAVE}</a></div>
          </form>
        </div>
      </div>
    </div>
	{include file="member_profile_left.tpl"}
    <!-------------------------inner-page end-----------------> 
  </div>
  <div style="clear:both;"></div>
</div>
{literal} 
<script>
function checknotification(){
    //alert('hello..');
    jQuery("#frmnotification").validationEngine('init',{scroll: false});
	  jQuery("#frmnotification").validationEngine('attach',{scroll: false});
		resp = jQuery("#frmnotification").validationEngine('validate');
	//	alert(resp); return false;
		if(resp == true){
			document.frmnotification.submit();
		}else{
			return false;
		}		
}
</script> 
{/literal}