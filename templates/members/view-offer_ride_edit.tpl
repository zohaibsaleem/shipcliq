<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&language=en"></script>
<script type="text/javascript" src="{$tconfig.tsite_javascript}gmap3.js"></script>
<script type="text/javascript" src="{$tconfig.tsite_url}rrb/zebra_datepicker.src.js"></script>
<link rel="stylesheet" href="{$tconfig.tsite_url}rrb/default.css" type="text/css">
{if $sess_browser eq 'Internet Explorer'}
  {literal}
    <script>
      alert('{/literal}{$smarty.const.LBL_RIDE_POST_NOT_SUPPORT}{literal}');
      window.location = "{/literal}{$tconfig.tsite_url}{literal}";
    </script>
  {/literal}
{/if}
{if $var_err_msg neq ''}
{literal}
<script>
      showNotification({type : 'error', message: '{/literal}{$var_err_msg}{literal}'});
</script>
{/literal}
{/if}
{literal}
<style>
  #map-canvas {
    height: 383px;
    margin-top: 0px;
    padding: 0px;
    width: 370px;
  }
</style>
{/literal}
{literal}
  <script type="text/javascript">
    $(function() {
        var from = document.getElementById('from');
        var autocomplete_from = new google.maps.places.Autocomplete(from);
    		google.maps.event.addListener(autocomplete_from, 'place_changed', function() {
    			var place = autocomplete_from.getPlace();
    			go_for_action();
    		});

    		var to = document.getElementById('to');
    		var autocomplete_to = new google.maps.places.Autocomplete(to);
    		google.maps.event.addListener(autocomplete_to, 'place_changed', function() {
    			var place = autocomplete_to.getPlace();
    			go_for_action();
    		});

        var loc1 = document.getElementById('loc1');
        var autocomplete_loc1 = new google.maps.places.Autocomplete(loc1);
    		google.maps.event.addListener(autocomplete_loc1, 'place_changed', function() {
    			var place = autocomplete_loc1.getPlace();
    			go_for_action();
    		});

    		var loc2 = document.getElementById('loc2');
        var autocomplete_loc2 = new google.maps.places.Autocomplete(loc2);
    		google.maps.event.addListener(autocomplete_loc2, 'place_changed', function() {
    			var place = autocomplete_loc2.getPlace();
    			go_for_action();
    		});

    		var loc3 = document.getElementById('loc3');
        var autocomplete_loc3 = new google.maps.places.Autocomplete(loc3);
    		google.maps.event.addListener(autocomplete_loc3, 'place_changed', function() {
    			var place = autocomplete_loc3.getPlace();
    			go_for_action();
    		});

    		var loc4 = document.getElementById('loc4');
        var autocomplete_loc4 = new google.maps.places.Autocomplete(loc4);
    		google.maps.event.addListener(autocomplete_loc4, 'place_changed', function() {
    			var place = autocomplete_loc4.getPlace();
    			go_for_action();
    		});

    		var loc5 = document.getElementById('loc5');
        var autocomplete_loc5 = new google.maps.places.Autocomplete(loc5);
    		google.maps.event.addListener(autocomplete_loc5, 'place_changed', function() {
    			var place = autocomplete_loc5.getPlace();
    			go_for_action();
    		});

    		var loc6 = document.getElementById('loc6');
        var autocomplete_loc6 = new google.maps.places.Autocomplete(loc6);
    		google.maps.event.addListener(autocomplete_loc6, 'place_changed', function() {
    			var place = autocomplete_loc6.getPlace();
    			go_for_action();
    		});

    		function go_for_action(){
           if($("#from").val() != '' &&  $("#to").val() == ''){
            show_location($("#from").val());
           }

           if($("#to").val() != '' &&  $("#from").val() == ''){
            show_location($("#to").val());
           }

           if($("#from").val() != '' &&  $("#to").val() != ''){
            from_to($("#from").val(), $("#to").val());
           }
        }
    });
  </script>
{/literal}
<div class="body-inner-part">
  <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_OFFER} {$smarty.const.LBL_A_RIDE}</span></div>
  <div class="main-inner-page">
    <h2>{$smarty.const.LBL_OFFER}<span>{$smarty.const.LBL_A_RIDE}</span></h2>
    <div class="offer-seats">
      <div class="tabing"> <a href="javascript:void(0);" class="active">{$smarty.const.LBL_MY_ITINERARY}</a><a href="javascript:void(0);">{$smarty.const.LBL_PRICE}</a> </div>
      <form name="frmride" id="frmride" method="post" action="{$tconfig.tsite_url}index.php?file=m-offer_ride_second_edit">
      <input type="hidden" name="iRideId" id="iRideId" value="{$iRideId}">
      <input type="hidden" name="distance" id="distance" value="">
      <input type="hidden" name="duration" id="duration" value="">
      <input type="hidden" name="from_lat_long" id="from_lat_long" value="{$from_lat_long}">
      <input type="hidden" name="to_lat_long" id="to_lat_long" value="{$to_lat_long}">
      <input type="hidden" name="loc1_lat_long" id="loc1_lat_long" value="{$loc1_lat_long}">
      <input type="hidden" name="loc2_lat_long" id="loc2_lat_long" value="{$loc2_lat_long}">
      <input type="hidden" name="loc3_lat_long" id="loc3_lat_long" value="{$loc3_lat_long}">
      <input type="hidden" name="loc4_lat_long" id="loc4_lat_long" value="{$loc4_lat_long}">
      <input type="hidden" name="loc5_lat_long" id="loc5_lat_long" value="{$loc5_lat_long}">
      <input type="hidden" name="loc6_lat_long" id="loc6_lat_long" value="{$loc6_lat_long}">
      <div class="offer-seats-left">
        <div class="type-of-trip">
          <h2>{$smarty.const.LBL_TYPE_TRIP}</h2>
          <span>
          <em><input name="triptype" id="onetime" type="radio" value="onetime" onClick="show_trip_type(this.value);"/> {$smarty.const.LBL_ONE_TIME_TRIP}</em>
          <em><input name="triptype" id="recurring" type="radio" value="recurring" onClick="show_trip_type(this.value);"/> {$smarty.const.LBL_RECURRING_TRIP} </em>
          </span>
        </div>
        <div class="type-of-trip2">
          <h2>{$smarty.const.LBL_ROUTE}</h2>
          <span> <em>
          <input name="from" id="from" type="text" class="location-input" value="{$from}"  placeholder="{$smarty.const.LBL_YOUR_DEPARTURE_POINT}"/>
          <br />
          <p>{$smarty.const.LBL_EXAMPLE}: Canary Wharf, London</p>
          </em> <em>
          <input name="to" id="to" type="text" class="location-input1" value="{$to}" placeholder="{$smarty.const.LBL_YOUR_DEPARTURE_POINT}"/>
          </em>
          <em> {$smarty.const.LBL_RIDE_OFFER_NOTE1}
          </em>
          <em>
          <input name="loc1" id="loc1" type="text" class="location-input1" value="{$loc1}" placeholder="{$smarty.const.LBL_POSSIBLE_STOP_POINT}"/>
          <input name="loc2" id="loc2" type="text" class="location-input1" value="{$loc2}" placeholder="{$smarty.const.LBL_POSSIBLE_STOP_POINT}"/>
          <input name="loc3" id="loc3" type="text" class="location-input1" value="{$loc3}" placeholder="{$smarty.const.LBL_POSSIBLE_STOP_POINT}"/>
          <input name="loc4" id="loc4" type="text" class="location-input1" value="{$loc4}" placeholder="{$smarty.const.LBL_POSSIBLE_STOP_POINT}"/>
          <input name="loc5" id="loc5" type="text" class="location-input1" value="{$loc5}" placeholder="{$smarty.const.LBL_POSSIBLE_STOP_POINT}"/>
          <input name="loc6" id="loc6" type="text" class="location-input1" value="{$loc6}" placeholder="{$smarty.const.LBL_POSSIBLE_STOP_POINT}"/>
          </em>
          <!--<em class="last"><a href="#">Add another stopover point</a></em>-->
          </span>
          </div>
        <div style="display:none;" id="frmric" class="type-of-trip3">
          <h2>{$smarty.const.LBL_DATE_TIME} <div style="float:right;margin-right:5px;font-size:15px;"><input type="checkbox" name="roundtripric" id="roundtripric" value="Yes" checked>{$smarty.const.LBL_ROUND_TRIP}</div></h2>
          <span>
          <em>
          {$smarty.const.LBL_OUTBOUND_DAY}:
          <table width="70%">
            <tr>
              <td style="background-color: #ADDE71;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="outbound[]" id="outbound" value="Sunday" {if $out_Sunday eq 'Yes'} checked {/if}>Sun</td>
              <td style="background-color: #ADDE71;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="outbound[]" id="outbound" value="Monday" {if $out_Monday eq 'Yes'} checked {/if}>Mon</td>
              <td style="background-color: #ADDE71;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="outbound[]" id="outbound" value="Tuesday" {if $out_Tuesday eq 'Yes'} checked {/if}>Tue</td>
              <td style="background-color: #ADDE71;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="outbound[]" id="outbound" value="Wednesday" {if $out_Wednesday eq 'Yes'} checked {/if}>Wed</td>
              <td style="background-color: #ADDE71;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="outbound[]" id="outbound" value="Thursday" {if $out_Thursday eq 'Yes'} checked {/if}>Thu</td>
              <td style="background-color: #ADDE71;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="outbound[]" id="outbound" value="Friday" {if $out_Friday eq 'Yes'} checked {/if}>Fri</td>
              <td style="background-color: #ADDE71;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="outbound[]" id="outbound" value="Saturday" {if $out_Saturday eq 'Yes'} checked {/if}>Sat</td>
            </tr>
          </table>
          </em>
          <em id="returndays">
          {$smarty.const.LBL_RETURN_DAY}:
          <table width="70%">
            <tr>
              <td style="background-color: #C5F0FF;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="retturn[]" id="retturn" value="Sunday" {if $ret_Sunday eq 'Yes'} checked {/if}>Sun</td>
              <td style="background-color: #C5F0FF;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="retturn[]" id="retturn" value="Monday" {if $ret_Monday eq 'Yes'} checked {/if}>Mon</td>
              <td style="background-color: #C5F0FF;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="retturn[]" id="retturn" value="Tuesday" {if $ret_Tuesday eq 'Yes'} checked {/if}>Tue</td>
              <td style="background-color: #C5F0FF;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="retturn[]" id="retturn" value="Wednesday" {if $ret_Wednesday eq 'Yes'} checked {/if}>Wed</td>
              <td style="background-color: #C5F0FF;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="retturn[]" id="retturn" value="Thursday" {if $ret_Thursday eq 'Yes'} checked {/if}>Thu</td>
              <td style="background-color: #C5F0FF;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="retturn[]" id="retturn" value="Friday" {if $ret_Friday eq 'Yes'} checked {/if}>Fri</td>
              <td style="background-color: #C5F0FF;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="retturn[]" id="retturn" value="Saturday" {if $ret_Saturday eq 'Yes'} checked {/if}>Sat</td>
            </tr>
          </table>
          </em>
          <em> {$smarty.const.LBL_OUTBOUND_TIME}:<br />
          <select name="richourstart" id="richourstart" class="location-select">
            {section name="ots" loop=24 start=1}
              <option value="{$smarty.section.ots.index}" {if $richourstart eq $smarty.section.ots.index} selected {/if}>{$smarty.section.ots.index}</option>
            {/section}
          </select>
          <select class="location-select" name="ricminstart" id="ricminstart">
            <option value="00" {if $ricminstart eq '00'} selected {/if}>00</option>
            <option value="10" {if $ricminstart eq '10'} selected {/if}>10</option>
            <option value="20" {if $ricminstart eq '20'} selected {/if}>20</option>
            <option value="30" {if $ricminstart eq '30'} selected {/if}>30</option>
            <option value="40" {if $ricminstart eq '40'} selected {/if}>40</option>
            <option value="50" {if $ricminstart eq '50'} selected {/if}>50</option>
          </select>
          </em>
          <em id="returntime"> {$smarty.const.LBL_RETURN_TIME}:<br />
         <select name="richourend" id="richourend" class="location-select">
            {section name="ote" loop=24 start=1}
              <option value="{$smarty.section.ote.index}" {if $richourend eq $smarty.section.ots.index} selected {/if}>{$smarty.section.ote.index}</option>
            {/section}
          </select>
          <select class="location-select" name="ricminend" id="ricminend">
            <option value="00" {if $ricminend eq '00'} selected {/if}>00</option>
            <option value="10" {if $ricminend eq '10'} selected {/if}>10</option>
            <option value="20" {if $ricminend eq '20'} selected {/if}>20</option>
            <option value="30" {if $ricminend eq '30'} selected {/if}>30</option>
            <option value="40" {if $ricminend eq '40'} selected {/if}>40</option>
            <option value="50" {if $ricminend eq '50'} selected {/if}>50</option>
          </select>
          </em>
          <em>{$smarty.const.LBL_START_FROM_DATE}:<br />
          <input name="sdate" id="sdate" type="text" class="location-input3" value="{$sdate}"/>
          </em>
          <em>{$smarty.const.LBL_END_TO_DATE}:<br />
          <input name="edate" id="edate" type="text" class="location-input3" value="{$edate}"/>
          </em>
          <em>
          <a class="cont-bot-but" href="javascript:void(0);" onClick="show_me_dates();" style="float: left;">{$smarty.const.LBL_VIEW_ALL_DATE}</a>
          </em>
          <em id="rb" class="last"></em>
          </span>
        </div>
        <div style="display:none;" id="frmonetime" class="type-of-trip3">
          <h2>{$smarty.const.LBL_DATE_TIME} <div style="float:right;margin-right:5px;font-size:15px;"><input type="checkbox" name="roundtriponetime" id="roundtriponetime" value="Yes">{$smarty.const.LBL_ROUND_TRIP}</div></h2>
          <span> <em> {$smarty.const.LBL_DEPARTURE_DATE}:<br />
          <input name="sdateone" id="sdateone" type="text" class="location-input3" value="{$sdateone}"/>
          <select name="onetihourstart" id="onetihourstart" class="location-select">
            <option value="">{$smarty.const.LBL_HR}</option>
            {section name="ots" loop=24 start=1}
              <option value="{$smarty.section.ots.index}" {if $onetihourstart eq $smarty.section.ots.index} selected {/if}>{$smarty.section.ots.index}</option>
            {/section}
          </select>
          <select class="location-select" name="onetimeminstart" id="onetimeminstart">
            <option value="">{$smarty.const.LBL_MIN}</option>
            <option value="00" {if $onetimeminstart eq '00'} selected {/if}>00</option>
            <option value="10" {if $onetimeminstart eq '10'} selected {/if}>10</option>
            <option value="20" {if $onetimeminstart eq '20'} selected {/if}>20</option>
            <option value="30" {if $onetimeminstart eq '30'} selected {/if}>30</option>
            <option value="40" {if $onetimeminstart eq '40'} selected {/if}>40</option>
            <option value="50" {if $onetimeminstart eq '50'} selected {/if}>50</option>
          </select>
          </em>
          <em id="returndaystime" class="last"> {$smarty.const.LBL_RETURN_DATE}:<br />
          <input name="edateone" id="edateone" type="text" class="location-input3" value="{$edateone}"/>
          <select name="onetihourend" id="onetihourend" class="location-select">
            <option value="">{$smarty.const.LBL_HR}</option>
            {section name="ots" loop=24 start=1}
              <option value="{$smarty.section.ots.index}" {if $onetihourend eq $smarty.section.ots.index} selected {/if}>{$smarty.section.ots.index}</option>
            {/section}
          </select>
          <select class="location-select" name="onetimeminend" id="onetimeminend">
            <option value="">{$smarty.const.LBL_MIN}</option>
            <option value="00" {if $onetimeminend eq '00'} selected {/if}>00</option>
            <option value="10" {if $onetimeminend eq '10'} selected {/if}>10</option>
            <option value="20" {if $onetimeminend eq '20'} selected {/if}>20</option>
            <option value="30" {if $onetimeminend eq '30'} selected {/if}>30</option>
            <option value="40" {if $onetimeminend eq '40'} selected {/if}>40</option>
            <option value="50" {if $onetimeminend eq '50'} selected {/if}>50</option>
          </select>
          </em>
          </span>
          </div>
        <a class="cont-bot-but" href="javascript:void(0);" onClick="go_next_page();">{$smarty.const.LBL_CONTINUE}</a>
      </div>
      </form>
      <div class="offer-seats-right"><div id="map-canvas" class="gmap3"></div></div>
    </div>
  </div>
  <div style="clear:both;"></div>
</div>

<div style="display:none">
  <div id="searchmodel" class="form-login">
    <h4>{$smarty.const.LBL_ADD_RIDE}</h4>
    <form method="post" id="add_box" name="add_box">
    <input type="hidden" value="" id="addtype" name="addtype">
    <input type="hidden" value="" id="adddate" name="adddate">
    <input type="hidden" value="add_date" id="add_action" name="add_action">
    <div class="inner-form1">
      <div class="singlerow-login" style="background-color:#ADDE71;width:130px;padding: 10px;border: 1px solid #D2D2D2;font-size: 16px;">
        <input type="checkbox" value="addout" id="addout" name="addout">&nbsp;{$smarty.const.LBL_OURBOUND}
      </div>
      <div class="singlerow-login" style="background-color:#C5F0FF;width:130px;padding: 10px;border: 1px solid #D2D2D2;font-size: 16px;">
        <input type="checkbox" value="addret" id="addret" name="addret">&nbsp;{$smarty.const.LBL_RETURN}
      </div>
      <div class="singlerow-login-log"><a href="javascript:void(0);" onclick="check_add();">{$smarty.const.LBL_ADD}</a></div>
      <div style="clear:both;"></div>
    </div>
    </form>
  </div>
</div>
{literal}
  <script>
   /* var map;
    function initialize() {
      var mapOptions = {
        zoom: 5,
        center: new google.maps.LatLng('{/literal}{$MAP_LATITUDE}{literal}', '{/literal}{$MAP_LONGITUDE}{literal}')
      };
      map = new google.maps.Map(document.getElementById('map-canvas'),
          mapOptions);
    }

    google.maps.event.addDomListener(window, 'load', initialize);*/
  </script>
  <script type="text/javascript">
      var chk_route;
      function show_location(address){
          clearThat()
          $('#map-canvas').gmap3({
            marker:{
              address: address
            },
            map:{
              options:{
                zoom: 8
              }
            }
          });
      }

      function clearThat(){
          var opts = {};
          opts.name = ["marker", "directionsrenderer"];
          opts.first = true;
          $('#map-canvas').gmap3({clear:opts});
      }

      function from_to(from, to){
        clearThat();

        var chks = document.getElementsByName('loc');
        var waypts = [];

        $("#map-canvas").gmap3({
            getlatlng:{
              address:  from,
              callback: function(results){
               $("#from_lat_long").val(results[0].geometry.location);
              }
            }
        });

        $("#map-canvas").gmap3({
            getlatlng:{
              address:  to,
              callback: function(results){
               $("#to_lat_long").val(results[0].geometry.location);
              }
            }
        });

		if($("#loc1").val() != ''){
          waypts.push({location:$("#loc1").val(), stopover:true});
          $("#map-canvas").gmap3({
          getlatlng:{
            address:  $("#loc1").val(),
            callback: function(results){
             $("#loc1_lat_long").val(results[0].geometry.location);
            }
          }
          });
        }
        if($("#loc2").val() != ''){
          waypts.push({location:$("#loc2").val(), stopover:true});
          $("#map-canvas").gmap3({
          getlatlng:{
            address:  $("#loc2").val(),
            callback: function(results){
            $("#loc2_lat_long").val(results[0].geometry.location);
            }
          }
          });
        }
        if($("#loc3").val() != ''){
          waypts.push({location:$("#loc3").val(), stopover:true});
          $("#map-canvas").gmap3({
          getlatlng:{
            address:  $("#loc3").val(),
            callback: function(results){
            $("#loc3_lat_long").val(results[0].geometry.location);
            }
          }
          });
        }
        if($("#loc4").val() != ''){
          waypts.push({location:$("#loc4").val(), stopover:true});
          $("#map-canvas").gmap3({
          getlatlng:{
            address:  $("#loc4").val(),
            callback: function(results){
            $("#loc4_lat_long").val(results[0].geometry.location);
            }
          }
          });
        }
        if($("#loc5").val() != ''){
          waypts.push({location:$("#loc5").val(), stopover:true});
          $("#map-canvas").gmap3({
          getlatlng:{
            address:  $("#loc5").val(),
            callback: function(results){
            $("#loc5_lat_long").val(results[0].geometry.location);
            }
          }
          });
        }
        if($("#loc6").val() != ''){
          waypts.push({location:$("#loc6").val(), stopover:true});
          $("#map-canvas").gmap3({
          getlatlng:{
            address:  $("#loc6").val(),
            callback: function(results){
            $("#loc6_lat_long").val(results[0].geometry.location);
            }
          }
          });
        }


        $("#map-canvas").gmap3({
          getroute:{
            options:{
                origin:from,
                destination:to,
                waypoints:waypts,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            },
            callback: function(results,status){
              chk_route=status;
              if (!results) return;
              $(this).gmap3({
                map:{
                  options:{
                    zoom: 8,
             //       center: [51.511214, -0.119824]
                    center: [58.0000, 20.0000]
                  }
                },
                directionsrenderer:{
                  options:{
                    directions:results
                  }
                }
              });
            }
          }
        });

        $("#map-canvas").gmap3({
          getdistance:{
            options:{
              origins:from,
              destinations:to,
              travelMode: google.maps.TravelMode.DRIVING
            },
            callback: function(results, status){
              var html = "";
              if (results){
                for (var i = 0; i < results.rows.length; i++){
                  var elements = results.rows[i].elements;
                  for(var j=0; j<elements.length; j++){
                    switch(elements[j].status){
                      case "OK":
                        html += elements[j].distance.text + " (" + elements[j].duration.text + ")<br />";
                        document.getElementById("distance").value = elements[j].distance.text;
                        document.getElementById("duration").value = elements[j].duration.text;
                        break;

                    }
                  }
                }
              } else {
                html = "error";
              }
              $("#results").html( html );
            }
          }
        });
      }
  </script>
  <script>
      function show_me_dates(){
        var request = $.ajax({
      	  type: "POST",
      	  url: site_url+'index.php?file=c-ride_dates',
      	  data: $("#frmride").serialize(),

      	  success: function(data) {//alert(data);
      	   $("#rb").html(data);
      		}
    	 });

       request.fail(function(jqXHR, textStatus) {
        //alert( "Request failed: " + textStatus );
       });
      }

      function select_date(type, date){
        document.getElementById("addtype").value = type;
        document.getElementById("adddate").value = date;
        if(type == 'out'){
         document.getElementById("addout").checked=true;
         document.getElementById("addret").checked=false;
        }else if(type == 'ret'){
         document.getElementById("addout").checked=false;
         document.getElementById("addret").checked=true;
        }else if(type == 'both'){
         document.getElementById("addout").checked=true;
         document.getElementById("addret").checked=true;
        }else{
         document.getElementById("addout").checked=false;
         document.getElementById("addret").checked=false;
        }
        $.fancybox("#searchmodel");return false;
      }

      function check_add(){
        var request = $.ajax({
      	  type: "POST",
      	  url: site_url+'index.php?file=c-add_date',
      	  data: $("#add_box").serialize(),

      	  success: function(data) { //alert(data);
            $.fancybox.close();
            show_me_dates();
      		}
    	  });

        request.fail(function(jqXHR, textStatus) {
    	   loading_hide() ;
         //alert( "Request failed: " + textStatus );
    	  });
      }

      $('#sdate').Zebra_DatePicker({onSelect: function(view, elements){if(document.getElementById("edate").value != ''){show_me_dates();}}});
      $('#edate').Zebra_DatePicker({onSelect: function(view, elements){if(document.getElementById("sdate").value != ''){show_me_dates();}}});

      $('#sdateone').Zebra_DatePicker();
      $('#edateone').Zebra_DatePicker();

      function show_trip_type(type){
        if(type == 'onetime'){
         $("#frmric").hide();
         $("#frmonetime").show();
        }else{
         $("#frmonetime").hide();
         $("#frmric").show();
        }
      }

      $("#roundtripric").on( "click", function() {
        if($("#roundtripric").is(':checked')){
          $("#returndays").show();
          $("#returntime").show();
        }else{
          $("#returndays").hide();
          $("#returntime").hide();
        }
      });

      $("#roundtriponetime").on( "click", function() {
        if($("#roundtriponetime").is(':checked')){
          $("#returndaystime").show();
        }else{
          $("#returndaystime").hide();
          document.getElementById("edateone").value = '';
          document.getElementById("onetihourend").value = '';
          document.getElementById("onetimeminend").value = '';
        }
      });

      function magage_trip_type(){
        $("#onetime").attr('checked', 'checked');
        show_trip_type('onetime');
      }

      function go_next_page(){
        if(chk_route=="OK"){
			       document.frmride.submit();
		    }
    		else{
    			showNotification({type : 'error', message: '{/literal}{$smarty.const.LBL_SOMETHIG_WRONG_IN_LOC}{literal}'});
    		}
      }

      magage_trip_type();
  </script>
{/literal}
{if $from neq '' and $to neq ''}
{literal}
<script>
from_to('{/literal}{$from}{literal}', '{/literal}{$to}{literal}');</script>
{/literal}
{/if}

{if $roundtriponetime eq 'Yes'}
  {literal}
    <script>
      $("#returndaystime").show();
      $('#roundtriponetime').attr("checked", "checked");
    </script>
  {/literal}
{else}
  {literal}
    <script>
      $("#returndaystime").hide();
      $('#roundtriponetime').attr("checked", false);
    </script>
  {/literal}
{/if}

{if $triptype neq ''}
{literal}
  <script>
     $('#{/literal}{$triptype}{literal}').attr("checked", "checked");
     show_trip_type('{/literal}{$triptype}{literal}');
  </script>
{/literal}
{/if}
{if $dateshow eq 'Yes'}
{literal}
  <script>
    show_me_dates();
  </script>
{/literal}
{/if}
{if $roundtripric eq 'Yes'}
  {literal}
    <script>
      $("#returndays").show();
      $("#returntime").show();
      $('#roundtripric').attr("checked", "checked");
    </script>
  {/literal}
{else}
  {literal}
    <script>
      $("#returndays").hide();
      $("#returntime").hide();
      $('#roundtripric').attr("checked", false);
    </script>
  {/literal}
{/if}
{if $go_to_second eq 'Yes'}
  {literal}
    <script>
       go_next_page();
    </script>
  {/literal}
{/if}
