<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&language=en"></script>
<script type="text/javascript" src="{$tconfig.tsite_javascript}gmap3.js"></script>
{if $sess_browser eq 'Internet Explorer'}
  {literal}
    <script>
      alert('{/literal}{$smarty.const.LBL_RIDE_POST_NOT_SUPPORT}{literal}');
      window.location = "{/literal}{$tconfig.tsite_url}{literal}";
    </script>
  {/literal}
{/if}
{literal}
<style>
  #map-canvas {
    height: 383px;
    margin-top: 0px;
    padding: 0px;
    width: 370px;
  }
</style>
{/literal}
{literal}
  <script>
  </script>
{/literal}
<div class="body-inner-part">
  <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_OFFER_RIDE}</span></div>
  <div class="main-inner-page">
    <h2>{$smarty.const.LBL_OFFER}<span>{$smarty.const.LBL_A_RIDE}</span></h2>
    <form name="frmstep2" id="frmstep2" method="post" action="{$tcondig.tsite_url}index.php?file=m-offer_rede_action_edit">
    <input type="hidden" name="vCountryCodeTo" id="vCountryCodeTo" value="{$destination_country}">
    <input type="hidden" name="iRideId" id="iRideId" value="{$iRideId}">
    <div class="offer-seats">
      <div class="tabing"> <a  href="{$tconfig.tsite_url}offer-ride">{$smarty.const.LBL_MY_ITINERARY}</a><a href="javascript:void(0);" class="active">{$smarty.const.LBL_PRICE}</a> </div>
      <div class="offer-seats-left">
        <div id="pricing" class="offer-a-ride">
          <h2>{$smarty.const.LBL_OFFER} {$smarty.const.LBL_A_RIDE}</h2>
          {section name="points" loop=$main_points_arr}
            <span class="spa-off">
              <input type="hidden" name="points_{$smarty.section.points.index}" id="points_{$smarty.section.points.index}" value="{$main_points_arr[points].addr_post}">
              <input type="hidden" name="start_lat_{$smarty.section.points.index}" id="start_lat_{$smarty.section.points.index}" value="{$main_points_arr[points].start_lat}">
              <input type="hidden" name="end_lat_{$smarty.section.points.index}" id="end_lat_{$smarty.section.points.index}" value="{$main_points_arr[points].end_lat}">
              <input type="hidden" name="distance_{$smarty.section.points.index}" id="distance_{$smarty.section.points.index}" value="">
              <input type="hidden" name="duration_{$smarty.section.points.index}" id="duration_{$smarty.section.points.index}" value="">
              <!-- Hemali..
			  {literal}<script> give_price_details('{/literal}{$main_points_arr[points].addr}{literal}', {/literal}{$smarty.section.points.index}{literal}, {/literal}{$main_points_arr[points].fPrice}{literal}, {/literal}{$main_points_arr[points].fOriginalPrice}{literal});</script>{/literal} -->
              <em class="ord1">{$main_points_arr[points].addr}</em>
              <!-- Hemali.. <p class="ord2">
              <b>{$sess_price_ratio}</b>
                <input name="price_{$smarty.section.points.index}" id="price_{$smarty.section.points.index}" type="text" class="pound-input" value="" onKeyUp="change_price_amount({$smarty.section.points.index});" style="color:#000;"/>
                <input name="price_type_{$smarty.section.points.index}" id="price_type_{$smarty.section.points.index}" type="hidden" value="Average"/>
                <input name="price_orig_{$smarty.section.points.index}" id="price_orig_{$smarty.section.points.index}" type="hidden" value=""/>
              </p> -->
            </span>
            <!-- Hemali.. <div class="plus-minus-container">
              <span><a href="javascript:void(0);" onClick="increase_price({$smarty.section.points.index});">+</a></span>
              <span><a href="javascript:void(0);" onClick="descrese_price({$smarty.section.points.index});">-</a></span>
            </div>  -->
          {/section}
          <!-- Hemali..<div {if $no_other_points eq 'Yes'}  style="display:none;"  {/if}>
          <span class="spa-off" style="color:#0094C8;"><em class="ord1"> {$from} &rarr; {$to}</em>
          <p class="ord2">
          <b>{$sess_price_ratio}</b>
            <input name="main_price" id="main_price" type="text" class="pound-input" value="{$Price_per_passenger}" readonly style="color:#000;"/>
            <input name="main_price_type_{$smarty.section.points.index}" id="main_price_type_{$smarty.section.points.index}" type="hidden" value="Average"/>
            <input name="main_price_orig_{$smarty.section.points.index}" id="main_price_orig_{$smarty.section.points.index}" type="hidden" value=""/>
          </p>
          </span>
          </div>   -->
        </div>
        <!--<div class="offer-a-ride2"> <span> <em>Number of seats offered:</em>
          <p>
            <input name="seats" id="seats" type="text" class="pound-input2" value="3" />
            <img src="{$tconfig.tsite_images}plush-min.jpg" alt="" /></p>
          </span>
        </div>

        <div id="pricing" class="offer-a-ride">
        <h2>{$smarty.const.LBL_CAR_DETAILS}</h2>
          <span class="spa-off"> <em class="ord1" style="width: 62%;">{$smarty.const.LBL_SELECT_CAR}:</em>
          <p class="ord2">
            <select name="iMemberCarId" id="iMemberCarId" class="pound-input" style="border-radius: 3px;width:200px;text-align: left;">
              {if $db_car|@count gt 0}
              {section name="car" loop=$db_car}
                <option value="{$db_car[car].iMemberCarId}" {if $db_ride[0].iMemberCarId eq $db_car[car].iMemberCarId} selected {/if}>{$db_car[car].vMake} {$db_car[car].vTitle}</option>
              {/section}
              {else}
              <option value="">{$smarty.const.LBL_ADD_YOUR_CAR_NOW}</option>
              {/if}
            </select>
          </p>
          </span>
          <span class="spa-off"> <em class="ord1">{$smarty.const.LBL_NUMBER_SEATS_OFFERED}:</em>
          <p class="ord2">
            <input name="seats" id="seats" type="text" class="pound-input" value="{$db_ride[0].iSeats}" style="border-radius: 3px;width:73px;" readonly/>
          </p>
          </span>
          <div class="plus-minus-container">
          <span><a href="javascript:void(0);" onClick="increase_seats();">+</a></span>
          <span><a href="javascript:void(0);" onClick="descrese_seats();">-</a></span>
          </div>
        </div>
         -->
		<!-- AS per xFetch clients requirement Start - Hemali.. -->
		<div class="offer-a-ride3">
          <h2>{$smarty.const.LBL_FREIGHT_TYPE}</h2>
          <span>
			<table border="0" cellpadding="10" width="100%" style="text-align:left">
				<tr>
					<td></td>
					<td>{$smarty.const.LBL_PRICE}</td>
					<td>{$smarty.const.LBL_WEIGHT}</td>
				</tr>
				<tr>
					<th>{$smarty.const.LBL_DOCUMENT}</th>
					<td><input type="text" class="form-box" name="fDocumentPrice" id="fDocumentPrice" value="{if $db_ride[0].fDocumentPrice gt 0}{$db_ride[0].fDocumentPrice}{/if}" onKeyUp="numericFilter(this)" style="width:50px;" placeholder="{$sess_price_ratio}"></td>
					<td><input type="text" class="form-box" name="vDocumentWeight" id="vDocumentWeight" value="{$db_ride[0].vDocumentWeight}" placeholder="{$smarty.const.LBL_DOCUMENT_EXAMPLE}"></td>
				</tr>
				<tr>
					<th>{$smarty.const.LBL_BOX}</th>
					<td><input type="text" class="form-box" name="fBoxPrice" id="fBoxPrice" value="{if $db_ride[0].fBoxPrice gt 0}{$db_ride[0].fBoxPrice}{/if}" onKeyUp="numericFilter(this)" style="width:50px;" placeholder="{$sess_price_ratio}"></td>
					<td><input type="text" class="form-box" name="vBoxWeight" id="vBoxWeight" value="{$db_ride[0].vBoxWeight}" placeholder="{$smarty.const.LBL_BOX_EXAMPLE}"></td>
				</tr>
				<tr>
					<th>{$smarty.const.LBL_LUGGAGE}</th>
					<td><input type="text" class="form-box" name="fLuggagePrice" id="fLuggagePrice" value="{if $db_ride[0].fLuggagePrice gt 0}{$db_ride[0].fLuggagePrice}{/if}" onKeyUp="numericFilter(this)" style="width:50px;" placeholder="{$sess_price_ratio}"></td>
					<td><input type="text" class="form-box" name="vLuggageWeight" id="vLuggageWeight" value="{$db_ride[0].vLuggageWeight}" placeholder="{$smarty.const.LBL_LUGGAGE_EXAMPLE}"></td>
				</tr>
			</table>
		  </span>
		</div>
		<!-- AS per xFetch clients requirement Start - Hemali..
		<div class="offer-a-ride3">
          <h2>{$smarty.const.LBL_DOCUMENT}</h2>
          <span>
			<p class="offer-ride1">
				<em>{$smarty.const.LBL_DOCUMENT} {$smarty.const.LBL_PRICE}: </em>
				<input type="text" class="form-box" name="fDocumentPrice" id="fDocumentPrice" value="{if $db_ride[0].fDocumentPrice gt 0}{$db_ride[0].fDocumentPrice}{/if}" onKeyUp="numericFilter(this)">{$sess_price_ratio}
			</p>
			<p class="offer-ride1"><em>{$smarty.const.LBL_DOCUMENT} {$smarty.const.LBL_WEIGHT}: </em><input type="text" class="form-box" name="vDocumentWeight" id="vDocumentWeight" value="{$db_ride[0].vDocumentWeight}"></p>
		  </span>
		</div>

		<div class="offer-a-ride3">
          <h2>{$smarty.const.LBL_BOX}</h2>
          <span>
			<p class="offer-ride1">
				<em>{$smarty.const.LBL_BOX} {$smarty.const.LBL_PRICE}: </em>
				<input type="text" class="form-box" name="fBoxPrice" id="fBoxPrice" value="{if $db_ride[0].fBoxPrice gt 0}{$db_ride[0].fBoxPrice}{/if}" onKeyUp="numericFilter(this)">{$sess_price_ratio}
			</p>
			<p class="offer-ride1"><em>{$smarty.const.LBL_BOX} {$smarty.const.LBL_WEIGHT}: </em><input type="text" class="form-box" name="vBoxWeight" id="vBoxWeight" value="{$db_ride[0].vBoxWeight}"></p>
		  </span>
		</div>

		<div class="offer-a-ride3">
          <h2>{$smarty.const.LBL_LUGGAGE}</h2>
          <span>
			<p class="offer-ride1">
				<em>{$smarty.const.LBL_LUGGAGE} {$smarty.const.LBL_PRICE}: </em>
				<input type="text" class="form-box" name="fLuggagePrice" id="fLuggagePrice" value="{if $db_ride[0].fLuggagePrice gt 0}{$db_ride[0].fLuggagePrice}{/if}" onKeyUp="numericFilter(this)">{$sess_price_ratio}
			</p>
			<p class="offer-ride1"><em>{$smarty.const.LBL_LUGGAGE} {$smarty.const.LBL_WEIGHT}: </em><input type="text" class="form-box" name="vLuggageWeight" id="vLuggageWeight" value="{$db_ride[0].vLuggageWeight}"></p>
		  </span>
		</div>
		<!-- AS per xFetch clients requirement End-->
        <div class="offer-a-ride3">
          <h2>{$smarty.const.LBL_FURTHER_DETAILS}</h2>
          <span>
           <p class="offer-ride1"><em>{$smarty.const.LBL_I_WILL_LEAVE}:</em>
            <select name="leave_time" id="leave_time" class="ride-input">
              <option value="ON_TIME" {if $db_ride[0].eLeaveTime eq 'ON_TIME'} selected {/if}>Right on time</option>
              <option value="FIFTEEN_MINUTES" {if $db_ride[0].eLeaveTime eq 'FIFTEEN_MINUTES'} selected {/if}>In a 15 minute window</option>
              <option value="THIRTY_MINUTES" {if $db_ride[0].eLeaveTime eq 'THIRTY_MINUTES'} selected {/if}>In a 30 minute window</option>
              <option value="ONE_HOUR" {if $db_ride[0].eLeaveTime eq 'ONE_HOUR'} selected {/if}>In a 1 hour window</option>
              <option value="TWO_HOURS" {if $db_ride[0].eLeaveTime eq 'TWO_HOURS'} selected {/if}>In a 2 hour window</option>
            </select>
          </p>
          <p>
            <textarea name="tDetails" id="tDetails" cols="" rows="" class="textarea-input">{$db_ride[0].tDetails}</textarea>
            <br />
            <img src="{$tconfig.tsite_images}information-inset.png" alt="" />&nbsp;{$smarty.const.LBL_OFFER_RIDE_NOTE}</p>
          <!-- Hemali.. <p>&nbsp;</p>
          <p class="offer-ride1"><em>{$smarty.const.LBL_MAX_LUGGAGE_SIZE}:</em>
            <select name="luggage_size" id="luggage_size" class="ride-input">
              <option value="Small" {if $db_ride[0].eLuggageSize eq 'Small'} selected {/if}>Small</option>
              <option value="Medium" {if $db_ride[0].eLuggageSize eq 'Medium'} selected {/if}>Medium</option>
              <option value="Big" {if $db_ride[0].eLuggageSize eq 'Big'} selected {/if}>Big</option>
            </select>
          </p>
          <p class="offer-ride1"><em>{$smarty.const.LBL_I_WILL_LEAVE}:</em>
            <select name="leave_time" id="leave_time" class="ride-input">
              <option value="ON_TIME" {if $db_ride[0].eLeaveTime eq 'ON_TIME'} selected {/if}>Right on time</option>
              <option value="FIFTEEN_MINUTES" {if $db_ride[0].eLeaveTime eq 'FIFTEEN_MINUTES'} selected {/if}>In a 15 minute window</option>
              <option value="THIRTY_MINUTES" {if $db_ride[0].eLeaveTime eq 'THIRTY_MINUTES'} selected {/if}>In a 30 minute window</option>
              <option value="ONE_HOUR" {if $db_ride[0].eLeaveTime eq 'ONE_HOUR'} selected {/if}>In a 1 hour window</option>
              <option value="TWO_HOURS" {if $db_ride[0].eLeaveTime eq 'TWO_HOURS'} selected {/if}>In a 2 hour window</option>
            </select>
          </p>
          <p class="offer-ride1"><em>{$smarty.const.LBL_I_MAKE_DETOUR}:</em>
            <select name="wait_time" id="wait_time" class="ride-input">
              <option value="NONE" {if $db_ride[0].eWaitTime eq 'NONE'} selected {/if}>None</option>
              <option value="FIFTEEN_MINUTES" {if $db_ride[0].eWaitTime eq 'FIFTEEN_MINUTES'} selected {/if}>15 minute detour, max.</option>
              <option value="THIRTY_MINUTES" {if $db_ride[0].eWaitTime eq 'THIRTY_MINUTES'} selected {/if}>30 minute detour, max.</option>
              <option value="WHATEVER_IT_TAKES" {if $db_ride[0].eWaitTime eq 'WHATEVER_IT_TAKES'} selected {/if}>Anything is fine</option>
            </select>
          </p>
          {if $LADIES_ONLY_SHOW eq 'Yes'}
          {if $smarty.session.sess_eGender eq 'Female'}
          <p class="offer-ride1"><em>{$smarty.const.LBL_LADIES_ONLY}:</em>
            <select name="eLadiesOnly" id="eLadiesOnly" class="ride-input">
              <option value="No" {if $db_ride[0].eLadiesOnly eq 'No'} selected {/if}>No</option>
              <option value="Yes" {if $db_ride[0].eLadiesOnly eq 'Yes'} selected {/if}>Yes</option>
            </select>
          </p>
          {else}
          <input type="hidden" name="eLadiesOnly" id="eLadiesOnly" value="No">
          {/if}
          {/if}
          {if $LADIES_ONLY_SHOW eq 'Yes'}
          <p class="offer-ride1"><em>{$smarty.const.LBL_RIDE_PLACE}:</em>
            <select name="eRidePlaceType" id="eRidePlaceType" class="ride-input">
              <option value="">Select</option>
              {if $AIRPORTS_RIDES_SHOW eq 'Yes'}
              <option value="Airport" {if $db_ride[0].eRidePlaceType eq 'Airport'} selected {/if}>Airport</option>
              {/if}
              {if $SHOPPING_RIDES_SHOW eq 'Yes'}
              <option value="Shopping" {if $db_ride[0].eRidePlaceType eq 'Shopping'} selected {/if}>Shopping</option>
              {/if}
            </select>
          </p>
          {/if}
		  -->
          </span> </div>
        <div class="offer-a-ride4"> <span>
          <p>
            <input name="tos" id="tos" type="checkbox" value="Yes" checked/>
            &nbsp;{$smarty.const.LBL_OFFER_RIDE_NOTE1}&nbsp;(<a href="{$tcofig.tsite_url}terms-conditions" target="_blank">{$smarty.const.LBL_SEE_OUR} ToS</a>) </p>
          </span> </div>
        <a class="cont-bot-but2" href="javascript:void(0);" onCLick=go_for_publish();>{$smarty.const.LBL_PUBLISH_OFFER}</a><a class="cont-bot-but1" href="{$tconfig.tsite_url}index.php?file=m-offer_ride_edit">{$smarty.const.LBL_BACK}</a> </div>
      <div class="offer-seats-right"><div id="map-canvas" class="gmap3"></div></div>
    </div>
    </form>
  </div>
  <div style="clear:both;"></div>
</div>
{literal}
<script>
  var tot_points = '{/literal}{$main_points_arr|@count}{literal}';
  var min_price_any_ride = {/literal}{$min_price_any_ride}{literal};

  function clearThat(){
    var opts = {};
    opts.name = ["marker", "directionsrenderer"];
    opts.first = true;
    $('#map-canvas').gmap3({clear:opts});
  }

  function from_to(from, to, loc1, loc2, loc3, loc4, loc5, loc6){
    clearThat();
    var waypts = [];

    if(loc1 != ''){
      waypts.push({location:loc1, stopover:true});
    }
    if(loc2 != ''){
      waypts.push({location:loc2, stopover:true});
    }
    if(loc3 != ''){
      waypts.push({location:loc3, stopover:true});
    }
    if(loc4 != ''){
      waypts.push({location:loc4, stopover:true});
    }
    if(loc5 != ''){
      waypts.push({location:loc5, stopover:true});
    }
    if(loc6 != ''){
      waypts.push({location:loc6, stopover:true});
    }

    $("#map-canvas").gmap3({
      getroute:{
        options:{
            origin:from,
            destination:to,
            waypoints:waypts,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        },
        callback: function(results){
          if (!results) return;
          $(this).gmap3({
            map:{
              options:{
                zoom: 13,
      //          center: [-33.879, 151.235]
                center: [58.0000, 20.0000]
              }
            },
            directionsrenderer:{
              options:{
                directions:results
              }
            }
          });
        }
      }
    });
  }
  from_to('{/literal}{$from}{literal}', '{/literal}{$to}{literal}', '{/literal}{$loc1}{literal}', '{/literal}{$loc2}{literal}', '{/literal}{$loc3}{literal}', '{/literal}{$loc4}{literal}', '{/literal}{$loc5}{literal}', '{/literal}{$loc6}{literal}');

  function give_all_details(details, index){
    var modes = details.split(" = ");

    $("#map-canvas").gmap3({
      getdistance:{
        options:{
          origins:modes[0],
          destinations:modes[1],
          travelMode: google.maps.TravelMode.DRIVING
        },
        callback: function(results, status){
          var html = "";
          if (results){
            for (var i = 0; i < results.rows.length; i++){
              var elements = results.rows[i].elements;
              for(var j=0; j<elements.length; j++){
                switch(elements[j].status){
                  case "OK":
                    var request = $.ajax({
                  	  type: "POST",
                  	  url: site_url+'index.php?file=c-count_price',
                  	  data: "distance="+elements[j].distance.text+"&duration="+elements[j].duration.text+"&from="+modes[0]+"&to="+modes[1]+"&pos="+pos+"&index="+index,

                  	  success: function(data) {
                  		  $("#pricing").append(data);
                  		}
                    });

                  	request.fail(function(jqXHR, textStatus) {
                  	  //alert( "Request failed: " + textStatus );
                  	});
                    break;

                }
              }
            }
          } else {
            html = "error";
          }
          $("#results").html( html );
        }
      }
    });
  }

  function increase_seats(){
    var main_price = Number(document.getElementById('seats').value);
    document.getElementById('seats').value = main_price + 1;
  }

  function descrese_seats(){
    if(document.getElementById('seats').value != 1){
      var main_price = Number(document.getElementById('seats').value);
      document.getElementById('seats').value = main_price - 1;
    }
  }

  function go_for_publish(){
    /* validation for at least one category selected from (Document, Box, Weight) */
	var valid = 0;
	if(document.getElementById('fDocumentPrice').value != ''){ // Does price insert?
		if(document.getElementById('fDocumentPrice').value == 0) { // It should not be zero
			showNotification({type : 'error', message: "{/literal}{$smarty.const.LBL_ERR_MSG_DOCPRICE_NOT_ZERO}{literal}"});
			return false;
		}
		if(document.getElementById('vDocumentWeight').value != '') { // When Document price is written Weight Cannot be blank
			valid =1; // Document Price and Weight both are written
		} else {
			showNotification({type : 'error', message: '{/literal}{$smarty.const.LBL_ERR_MSG_DOCWEIGHT_NOT_EMPTY}{literal}'});
			return false;
		}
	}
	if(document.getElementById('fBoxPrice').value != ''){ // Does price insert?
		if(document.getElementById('fBoxPrice').value == 0) { // It should not be zero
			showNotification({type : 'error', message: "{/literal}{$smarty.const.LBL_ERR_MSG_BOXPRICE_NOT_ZERO}{literal}"});
			return false;
		}
		if(document.getElementById('vBoxWeight').value != '') { // When Box price is written Weight Cannot be blank
			valid =1; // Box Price and Weight both are written
		} else {
			showNotification({type : 'error', message: '{/literal}{$smarty.const.LBL_ERR_MSG_BOXWEIGHT_NOT_EMPTY}{literal}'});
			return false;
		}
	}
	if(document.getElementById('fLuggagePrice').value != ''){ // Does price insert?
		if(document.getElementById('fLuggagePrice').value == 0) { // It should not be zero
			showNotification({type : 'error', message: "{/literal}{$smarty.const.LBL_ERR_MSG_LUGPRICE_NOT_ZERO}{literal}"});
			return false;
		}
		if(document.getElementById('vLuggageWeight').value != '') { // When Luggage price is written Weight Cannot be blank
			valid =1; // Luggage Price and Weight both are written
		} else {
			showNotification({type : 'error', message: '{/literal}{$smarty.const.LBL_ERR_MSG_LUGWEIGHT_NOT_EMPTY}{literal}'});
			return false;
		}
	}
	if(valid == 0){
		showNotification({type : 'error', message: '{/literal}{$smarty.const.LBL_ERR_MSG_ATLEAST_ONE}{literal}'});
		return false;
	}
   /* Hemali.. if(document.getElementById('iMemberCarId').value == ''){
      showNotification({type : 'error', message: '{/literal}{$smarty.const.LBL_PLEASE_SELECT_CAR}{literal}'});
      return false;
    } */
    if($("#tos").is(':checked')){
    }else{
      showNotification({type : 'error', message: '{/literal}{$smarty.const.LBL_SELECT_TERMS_CONDITIONS}{literal}'});
      return false;
    }

    for (var i=0;i<tot_points;i++)
    {
      if($("#price_"+i).val() < min_price_any_ride){
        //showNotification({type : 'error', message: "You can't enter any ride point price smallar than "+min_price_any_ride});
        //return false;
      }else if($("#price_"+i).val() == 0 || $("#price_"+i).val() == ''){
        showNotification({type : 'error', message: "You can't enter zero or blank price for any ride point"});
        return false;
      }
    }
    /*Hemali..
     if($("#main_price").val() < min_price_any_ride){
      showNotification({type : 'error', message: "You can't enter price smallar than "+min_price_any_ride+" for whole ride"});
      return false;
    }*/

    document.frmstep2.submit();
    return false;

  }

  function increase_price(id){
    var old_price = $("#price_"+id).val();
    var new_price = Number(old_price) + Number(1);
    $("#price_"+id).val(new_price);

    /*var distance = $("#distance_"+id).val().split(" ");
    var miles = Math.round(distance[0] * 0.6214);
    var price_per_100_miles = {/literal}{$Hundradrmilespice}{literal};
    var Price_per_passenger = Math.round((price_per_100_miles * miles)/100);

    if(Price_per_passenger < 3){
     Price_per_passenger = 3;
    }

    if(Price_per_passenger < new_price){
      $("#price_"+id).css( "color", "#FF8000" );
      $("#price_type_"+id).val("High");
    }else{
      $("#price_"+id).css( "color", "#000" );
      $("#price_type_"+id).val("Average");
    }
    cacl_all();*/
    change_price_amount(id);
  }

  function descrese_price(id){
    var old_price = $("#price_"+id).val();
    if(old_price > 1){
       var new_price = Number(old_price) - Number(1);
       $("#price_"+id).val(new_price);

        /*var distance = $("#distance_"+id).val().split(" ");
        var miles = Math.round(distance[0] * 0.6214);
        var price_per_100_miles = {/literal}{$Hundradrmilespice}{literal};
        var Price_per_passenger = Math.round((price_per_100_miles * miles)/100);

        if(Price_per_passenger < 3){
         Price_per_passenger = 3;
        }

        if(Price_per_passenger == new_price){
          $("#price_"+id).css( "color", "#000" );
          $("#price_type_"+id).val("Average");
        }else if(Price_per_passenger > new_price){
          $("#price_"+id).css( "color", "#149414" );
          $("#price_type_"+id).val("Low");
        }
       cacl_all();*/
       change_price_amount(id);
    }else{
       $("#price_"+id).val(1);
       //cacl_all();
       change_price_amount(id);
    }
  }

  function cacl_all(){
    var price = 0;
    for (var i=0;i<tot_points;i++)
    {
      price = Number(price) + Number($("#price_"+i).val());
    }
    $("#main_price").val(price);
  }

  function change_price_amount(id){
     var price = $("#price_"+id).val();
     var origional_price = $("#price_orig_"+id).val();

     $("#price_"+id).val(price.replace(/[^/(0-9)/]/ig, ""));

     var main_price = $("#main_price").val();
     var origional_main_price = $("#main_price_orig").val();

     var price_diff = price - origional_price;
     price_diff = Math.abs(price_diff);

     if(Number(price_diff) > Number(2) && Number(price) > Number(origional_price)){
        $("#price_"+id).css( "color", "#FF8000" );
        $("#price_type_"+id).val("High");
        cacl_all();
        return false;
     }else if(Number(price_diff) > Number(2) && Number(price) < Number(origional_price)){
        $("#price_"+id).css( "color", "#149414" );
        $("#price_type_"+id).val("Low");
        cacl_all();
        return false;
     }else{
        $("#price_"+id).css( "color", "#000" );
        $("#price_type_"+id).val("Average");
        cacl_all();
        return false;
     }

     /*if(price > origional_price){
      $("#price_"+id).css( "color", "#FF8000" );
      $("#price_type_"+id).val("High");
      cacl_all();
     }else if(price < origional_price){
      $("#price_"+id).css( "color", "#149414" );
      $("#price_type_"+id).val("Low");
      cacl_all();
     }else{
      $("#price_"+id).css( "color", "#000" );
      $("#price_type_"+id).val("Average");
      cacl_all();
     }*/
  }

  function give_price_details(addr, id){
       var modes = addr.split(" &rarr; ");
        $("#map-canvas").gmap3({
         getdistance:{
          options:{
            origins:modes[0],
            destinations:modes[1],
            travelMode: google.maps.TravelMode.DRIVING
          },
          callback: function(results, status){
            if (results){
              for (var i = 0; i < results.rows.length; i++){
                var elements = results.rows[i].elements;
                for(var j=0; j<elements.length; j++){

                  switch(elements[j].status){
                    case "OK":
                      //data: "distance="+elements[j].distance.text+"&duration="+elements[j].duration.text+"&from="+modes[0]+"&to="+modes[1]+"&pos="+pos+"&index="+index,

                      $("#distance_"+id).val(elements[j].distance.text);
                      $("#duration_"+id).val(elements[j].duration.text);

                      //alert('Distance >> '+elements[j].distance.text);
                      //alert('Duration >> '+elements[j].duration.text);

                      var dist = elements[j].distance.text.split(" ");
                      dist[0] = dist[0].replace(/,/g,'');
                      var miles = Math.round(dist[0] * 0.6214);
                      //var price_per_100_miles = {/literal}{$Hundradrmilespice}{literal}; 

                      var Price_per_passenger = Math.round((price_per_100_miles * miles)/100);

                      //if(Price_per_passenger < min_price_any_ride){
                       //Price_per_passenger = min_price_any_ride;
                      //}

                      var trrip_main_price = $("#main_price").val();
                      $("#main_price").val(Number(Price_per_passenger) + Number(trrip_main_price));
                      $("#main_price_orig_"+id).val(Number(Price_per_passenger) + Number(trrip_main_price));

                     /* if(old_fPrice != 0 && old_fOriginalPrice != 0){
                        $("#price_"+id).val(old_fPrice);
                        $("#price_orig_"+id).val(old_fOriginalPrice);
                      }else{
                        $("#price_"+id).val(Price_per_passenger);
                        $("#price_orig_"+id).val(Price_per_passenger);
                      }

                      change_price_amount(id);   */


                      //var trrip_main_price = $("#main_price").val();
                      //$("#main_price").val(Number(Price_per_passenger) + Number(trrip_main_price));
                      //$("#main_price_orig_"+id).val(Number(Price_per_passenger) + Number(trrip_main_price));
                      break;
                  }
                }
              }
            } else {
              html = "error";
            }
          }
        }
      });
    }
	function numericFilter(txb) {
	   //txb.value = txb.value.replace(/[^\0-9]/ig, "");
	   txb.value = txb.value.replace(/[^0-9.]/g,"");
	}
</script>
{/literal}

{section name="points" loop=$main_points_arr}
{literal}<script> give_price_details('{/literal}{$main_points_arr[points].addr}{literal}', {/literal}{$smarty.section.points.index}{literal}, {/literal}{$main_points_arr[points].fPrice}{literal}, {/literal}{$main_points_arr[points].fOriginalPrice}{literal});</script>{/literal}
{/section}
