{literal}
<style>
.tab-pane ul {width:75%;}
</style>
{/literal}
{if $smarty.get.var_msg neq ''}          
{literal}
<script>        
      showNotification({type : 'success', message: '{/literal}{$smarty.get.var_msg}{literal}'});  
</script>
{/literal}
{/if} 

{if $smarty.get.var_msg_err neq ''}          
{literal}
<script>
      showNotification({type : 'error', message: '{/literal}{$smarty.get.var_msg_err}{literal}'});
    </script>
{/literal}
{/if}
<div class="body-inner-part">
	<div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_MY_PAYMENT}</span></div>
	<!-------------------------inner-page----------------->
	<div class="main-inner-page">
		<h2>{$smarty.const.LBL_MY_PAYMENT}</span></h2>
		{include file="member_top.tpl"}
		<div class="rides-offered">
          <div class="tab-pane">
            <ul>
			<!--<li><a href="{$tconfig.tsite_url}index.php?file=m-payment_detail&type=available"  {if $smarty.get.type eq 'available'}class="active"{/if}>History of Your Transfer</a></li>
			  <li><a href="{$tconfig.tsite_url}index.php?file=m-payment_detail&type=refund"  {if $smarty.get.type  eq 'refund'}class="last active"{else}class="last"{/if}>Your Refund</a></li>-->
			     <li><a href="{$tconfig.tsite_url}index.php?file=m-money_detail&pagetype=money&type=available"  {if $smarty.get.pagetype eq 'money'}class="active"{/if}>{$smarty.const.LBL_AS_DRIVER}</a></li>
				 <li><a href="{$tconfig.tsite_url}index.php?file=m-payment_detail&pagetype=payment&type=available"  {if $smarty.get.pagetype eq 'payment'}class="active"{/if}>{$smarty.const.LBL_AS_BOOKER}</a></li>
            </ul>
			<select name="type[]" onChange="return travelerChange(this.value);" style="float: right; border: 1px solid #CCCCCC; margin:0 4px; padding: 7px;">
			   <option value="available" {if $smarty.get.type eq 'available'}selected{/if} > {$smarty.const.LBL_PAYMENTS_HISTORY}</option>
			   <option value="refund" {if $smarty.get.type eq 'refund'}selected{/if}>{$smarty.const.LBL_REFUNDS}</option>
			</select>
             </div>
        </div>
		{*include file="member_left.tpl"*}
		<div class="">
			<form name="frmbooking" id="frmbooking" method="post" action="">
				<input type="hidden" id="type" name="type" value="{$type}">
				<input type="hidden" id="action" name="action" value="send_equest">
				<input type="hidden"  name="eTransRequest" id="eTransRequest" value="">
				<input type="hidden"  name="iBookingId" id="iBookingId" value="">
				<div class="rating-received2">
					<div class="rating-given-right-part"style="min-height:135px">
						<table width="100%" border="0" cellpadding="10" cellspacing="0" class="credit-p">
							{if $db_money|@count gt 0}
								<tr style="background:#efefef;">
									<th width="1%" align="center" class="credits">{$smarty.const.LBL_BOOKING_NO} #</th>
									<th width="15%" align="center" class="credits">{$smarty.const.LBL_BOOKING_DATE}</th>
									<th width="25%" align="center" class="credits">{$smarty.const.LBL_DETAILS}</th>
									<th width="10%" align="center"  class="credits">{$smarty.const.LBL_AMOUNT}</th>
									<th width="30%" align="center" >{$smarty.const.LBL_STATUS1}</th>
								</tr>
								{section name=rec1 loop=$db_money}
									<tr {$db_money[rec1].trstyle}>
									  <td align="center" class="credits">{$db_money[rec1].vBookingNo}</td>
									  <td align="center" class="credits">{$generalobj->DateTime($db_money[rec1].dBookingDate,9)}</td>
									  <td align="center" class="credits">{$db_money[rec1].vMainRidePlaceDetails}</td>
									  <td align="center" class="credits">{$db_money[rec1].fAmount}</td>
									  <td align="center" class="credits">{$smarty.const.LBL_PAID}</td>
									</tr>
								{/section}
								<tr style="background:#efefef;">
									<td colspan="4" style="text-align:right;padding-right:5px;"><b>{$smarty.const.LBL_TOT_PAID} :</b></td>
									<td align="center">{$total}</td>
								</tr>
							{else} 
								<tr>
									<td colspan="5">
										<div class="main-block">No Records Found.</div>
									</td>
								</tr>
							{/if}
						</table>
					</div>
				</div>
			</form> 
		</div>
	</div>
	<!-------------------------inner-page end----------------->
	<div style="clear:both;"></div>
</div>
{literal}
<script>
function getCheckCount(frmbooking)
{
	var x=0;
	for(i=0;i < frmbooking.elements.length;i++)
	{	if ( frmbooking.elements[i].checked == true) 
			{x++;}
	}
	return x;
}
function check_skills_edit(){
  y = getCheckCount(document.frmbooking);
  if(y>0)
	{
    jQuery("#frmbooking").validationEngine('attach',{scroll: false});  
    resp = jQuery("#frmbooking").validationEngine('validate');    
    if(resp == true){
     $("#eTransRequest").val('Yes');
        document.frmbooking.submit();
    }else{
       return false;
    }
  }
  else{
      alert("Select Ride for send transfer request")
      return false;
  }
  }
   function travelerChange(type)
  {
	  
	  if(type=='available')
		  window.location="index.php?file=m-payment_detail&type=available&pagetype=payment";
		  if(type=='refund')
		    window.location="index.php?file=m-payment_detail&type=refund&pagetype=payment";
  }
</script>
{/literal}


