<div class="body-inner-part">
  <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_PYMT_FAILED}</span></div>
  <div class="main-inner-page">
    <h2>Booking Process</h2>
   <div class="right-inner-part">
      <h2>{$smarty.const.LBL_PYMT_FAILED}</h2>
      <p>{$smarty.const.LBL_PYMT_FAILED_TRYAGAIN}</p>
    </div>
	 {include file="left.tpl"}
    
  </div>
  <div style="clear:both;"></div>
</div>
{literal}
<script>
    setTimeout(function(){
       window.location='checkpayment.php?id={/literal}{$numberid}{literal}';
    }, 10000);
</script>
{/literal}
