{if $smarty.get.var_msg neq ''}          
{if $smarty.get.msg_code eq '1'}
  {literal} 
<script>        
      showNotification({type : 'success', message: '{/literal}{$smarty.get.var_msg}{literal}'});  
    </script> 
{/literal}
{/if}
{/if} 

{if $smarty.get.var_msg neq ''}          
{if $smarty.get.msg_code eq '0'}
  {literal} 
<script>    
  showNotification({type : 'error', message: '{/literal}{$smarty.get.var_msg}{literal}'});
</script> 
{/literal}
{/if}
{/if}
<div class="body-inner-part">
  <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_PHONE_NUMBER_VERIFICATION}</span></div>
  <div class="main-inner-page">
    <h2>{$smarty.const.LBL_PHONE_NUMBER_VERIFICATION}</h2>
    {include file="member_top.tpl"}
    <div class="ratings">
      <div class="email-verification">
        <h3>{$smarty.const.LBL_PHONE_NUMBER_VERIFICATION}</h3>
      </div>
      <div class="verifications-zone" style="width:100%;">
      <div class="mpv">
      <span>
      <p>{$smarty.const.LBL_PHONE_NOTE1}:</p>
        <form name="frmemailverifyphone" id="frmemailverifyphone" class="co-fr" action="{$tconfig.tsite_url}index.php?file=m-verified_phonenumber" method="post">
          <input type="text" id="code" name="code" class="v-input">
          <button class="btn-validation apply-btn-loader" type="submit"> {$smarty.const.LBL_CONFIRM} </button>
        </form></span>
        <span>{$smarty.const.LBL_PHONE_NOTE2} <a href="#">{$smarty.const.LBL_PHONE_NOTE3}</a></span> 
        </div>
         <div class="mpv1">
        <span><img src="{$tconfig.tsite_images}{$THEME}/verify.png" alt="" /><strong>{$smarty.const.LBL_PHONE_NOTE4} ?
          <p>{$smarty.const.LBL_PHONE_NOTE5}</p></strong></span> 
      </div>
      
        </div>
    </div>
	{include file="member_profile_left.tpl"}
  </div>
</div>
