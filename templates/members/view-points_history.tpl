 <div class="about-page">
    <h2>{$smarty.const.LBL_MY_ACCOUNT}</h2>
    <div class="right-about-part">
      <h1>{$smarty.const.LBL_MY_POINTS}</h1>
      <div class="history">
        <table width="100%" cellspacing="1" cellpadding="3" border="0" class="tabborbg">
          <tbody>
            {if $db_points_history|@count gt 0}
            <tr>
              <th width="20%" height="20">{$smarty.const.LBL_EVENTS}</th>
              <th width="20%" >{$smarty.const.LBL_ORGANIZTION_NAME}</th>
              <th width="20%" class="padding-right">{$smarty.const.LBL_DATE}</th>
               <th width="20%">{$smarty.const.LBL_SERVED_HOURS}</th>
              <th width="20%" >{$smarty.const.LBL_POINTS}</th>
            </tr>
            {section name="i" loop=$db_points_history}
             {if $db_points_history[i].dHours neq '0.00'}
            <tr class="oddbg">
            <td ><a href="{$tconfig.tsite_url}event-details/{$db_points_history[i].iEventId}">{$db_points_history[i].vTitle}</a></td>
              <td >{$db_points_history[i].vOrganizationName}</td>
              <td >{if $db_points_history[i].eEventType neq 'Fixed'}{$db_points_history[i].eEventType}{else}{$db_points_history[i].dStartDate}{/if}</td>
              <td >{$db_points_history[i].dHours}</td>
               <td >{$db_points_history[i].fPoints}</td>
            </tr>
              {/if}
              {/section}
            <tr class="evenbg">
             <td>&nbsp;</td>
              <td colspan="3" style="color:#000000; font-size:15px;" align="right">{$smarty.const.LBL_TOTAL} :&nbsp;&nbsp;&nbsp;</td>
              <td style="color:#000000; font-size:15px;">{$total_points}</td>
            </tr>
            {else}
            <span style="color:#000000;font-size:18px;margin:10px;">
        <center>           {$smarty.const.LBL_NOT_EARN_POINTS}
        </center>
        </span> 
            {/if}  
            
          </tbody>
        </table>
        
        <div style="clear:both;"></div>
      </div>
    </div>
	{include file="volunteer_left.tpl"} 
	<div style="clear:both;"></div>
  </div>