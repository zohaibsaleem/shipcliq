<link rel="stylesheet" type="text/css" href="{$tconfig.tsite_stylesheets}front/simptip-mini.css" media="screen,projection" />
{if $var_msg neq ''} 
  {literal}
    <script> 
      showNotification({type : 'success', message: '{/literal}{$var_msg}{literal}'});  
    </script>
  {/literal}
{/if}

{if $var_err_msg neq ''} 
  {literal}
    <script>
      showNotification({type : 'error', message: '{/literal}{$var_err_msg}{literal}'});
    </script>
  {/literal}
{/if} 

<div class="body-inner-part">
<div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_SHARE_PREFERENCE}</span></div>
<div class="main-inner-page">
  <h2>{$smarty.const.LBL_CAR_SHARE} <span>{$smarty.const.LBL_PREFERENCES}</span></h2>
  {include file="member_top.tpl"}
  <div class="ratings">
    <form name="prefrm" id="prefrm" method="post" action="{$tconfig.tsite_url}car-preferences">
    <input type="hidden" name="action" id="action" value="edit">
    <div class="car-share-preferences">
      <h3>{$smarty.const.LBL_MY_CAR_REFERENCE}</h3>
      
      {section name="pref" loop=$db_preferences}
        <label><em>{$db_preferences[pref].vTitle}:</em>
        <span class="simptip-position-top" ><img id="img_vYes_{$db_preferences[pref].iPreferencesId}" src="{$db_preferences[pref].vYes_img}" alt="" onClick="checked_me('vYes', {$db_preferences[pref].iPreferencesId});"/></span> 
        <span class="simptip-position-top" ><img id="img_vMAYBE_{$db_preferences[pref].iPreferencesId}" src="{$db_preferences[pref].vMAYBE_img}" alt="" onClick="checked_me('vMAYBE', {$db_preferences[pref].iPreferencesId});"/></span> 
        <span class="simptip-position-top"><img id="img_vNO_{$db_preferences[pref].iPreferencesId}" src="{$db_preferences[pref].vNO_img}" alt="" onClick="checked_me('vNO', {$db_preferences[pref].iPreferencesId});"/></span> 
        </label>
         
        <span style="display:none;">
        <input type="radio" name="vYes_{$db_preferences[pref].iPreferencesId}" id="vYes_{$db_preferences[pref].iPreferencesId}" value="vYes">
        <input type="radio" name="vMAYBE_{$db_preferences[pref].iPreferencesId}" id="vMAYBE_{$db_preferences[pref].iPreferencesId}" value="vMAYBE">
        <input type="radio" name="vNO_{$db_preferences[pref].iPreferencesId}" id="vNO_{$db_preferences[pref].iPreferencesId}" value="vNO">
        </span>  
      {/section}
      
      <!--<label><em>{$smarty.const.LBL_CHATTINESS}:</em>
      <span class="simptip-position-top" data-tooltip="{$smarty.const.LBL_I_LOVE_CHAT}"><img {if $db_pref[0].eChattiness eq 'YES'}class="borderClass"{/if} id="eChattiness_YES_img" src="{$tconfig.tsite_images}chat1.png" alt="" onClick="checked_me('eChattiness', 'YES');"/></span> 
      <span class="simptip-position-top" data-tooltip="{$smarty.const.LBL_TALK_MOOD}"><img {if $db_pref[0].eChattiness eq 'MAYBE'}class="borderClass"{/if} class="" id="eChattiness_MAYBE_img" src="{$tconfig.tsite_images}chat2.png" alt="" onClick="checked_me('eChattiness', 'MAYBE');"/></span> 
      <span class="simptip-position-top simptip-danger" data-tooltip="{$smarty.const.LBL_QUIET_TYPE} :)"><img {if $db_pref[0].eChattiness eq 'NO'}class="borderClass"{/if} class="" id="eChattiness_NO_img" src="{$tconfig.tsite_images}chat3.png" alt="" onClick="checked_me('eChattiness', 'NO');"/></span> 
      </label>      
      <label><em>{$smarty.const.LBL_MUSIC}:</em> 
      <span class="simptip-position-top" data-tooltip="{$smarty.const.LBL_ABOUT_PLAYLIST}"><img {if $db_pref[0].eMusic eq 'YES'}class="borderClass"{/if} id="eMusic_YES_img" src="{$tconfig.tsite_images}music1.png" alt="" onClick="checked_me('eMusic', 'YES');"/></span> 
      <span class="simptip-position-top" data-tooltip="{$smarty.const.LBL_DEPEND_MOOD}"><img {if $db_pref[0].eMusic eq 'MAYBE'}class="borderClass"{/if} id="eMusic_MAYBE_img" src="{$tconfig.tsite_images}music2.png" alt="" onClick="checked_me('eMusic', 'MAYBE');"/></span> 
      <span class="simptip-position-top simptip-danger" data-tooltip="{$smarty.const.LBL_SILENCE_GOLDEN}"><img {if $db_pref[0].eMusic eq 'NO'}class="borderClass"{/if} id="eMusic_NO_img" src="{$tconfig.tsite_images}music3.png" alt="" onClick="checked_me('eMusic', 'NO');"/></span>
      </label>
      <label><em>{$smarty.const.LBL_SMOKING}:</em> 
      <span class="simptip-position-top" data-tooltip="{$smarty.const.LBL_CIGARETTE_SMOKE}"><img {if $db_pref[0].eSmoking eq 'YES'}class="borderClass"{/if} id="eSmoking_YES_img" src="{$tconfig.tsite_images}smoke1.png" alt="" onClick="checked_me('eSmoking', 'YES');"/></span> 
      <span class="simptip-position-top" data-tooltip="{$smarty.const.LBL_DEPEND_MOOD}"><img {if $db_pref[0].eSmoking eq 'MAYBE'}class="borderClass"{/if} id="eSmoking_MAYBE_img" src="{$tconfig.tsite_images}smoke2.png" alt="" onClick="checked_me('eSmoking', 'MAYBE');"/></span> 
      <span class="simptip-position-top simptip-danger" data-tooltip="{$smarty.const.LBL_NO_SMOKE}"><img {if $db_pref[0].eSmoking eq 'NO'}class="borderClass"{/if} id="eSmoking_NO_img" src="{$tconfig.tsite_images}smoke3.png" alt="" onClick="checked_me('eSmoking', 'NO');"/></span> 
      </label>
      <label><em>E. {$smarty.const.LBL_SMOKING}:</em> 
      <span class="simptip-position-top" data-tooltip="E. {$smarty.const.LBL_CIGARETTE_SMOKE}"><img {if $db_pref[0].eEcig eq 'YES'}class="borderClass"{/if} id="eEcig_YES_img" src="{$tconfig.tsite_images}ecig1.png" alt="" onClick="checked_me('eEcig', 'YES');"/></span> 
      <span class="simptip-position-top" data-tooltip="{$smarty.const.LBL_DEPEND_MOOD}"><img {if $db_pref[0].eEcig eq 'MAYBE'}class="borderClass"{/if} id="eEcig_MAYBE_img" src="{$tconfig.tsite_images}ecig2.png" alt="" onClick="checked_me('eEcig', 'MAYBE');"/></span> 
      <span class="simptip-position-top simptip-danger" data-tooltip="{$smarty.const.LBL_NO_SMOKE}"><img {if $db_pref[0].eEcig eq 'NO'}class="borderClass"{/if} id="eEcig_NO_img" src="{$tconfig.tsite_images}ecig3.png" alt="" onClick="checked_me('eEcig', 'NO');"/></span> 
      </label>
      <label><em>{$smarty.const.LBL_PETS}:</em> 
      <span class="simptip-position-top" data-tooltip="{$smarty.const.LBL_PETS1}"><img {if $db_pref[0].ePets eq 'YES'}class="borderClass"{/if} id="ePets_YES_img" src="{$tconfig.tsite_images}pets1.png" alt="" onClick="checked_me('ePets', 'YES');"/></span> 
      <span class="simptip-position-top" data-tooltip="{$smarty.const.LBL_DEPEND_MOOD}"><img {if $db_pref[0].ePets eq 'MAYBE'}class="borderClass"{/if} id="ePets_MAYBE_img" src="{$tconfig.tsite_images}pets2.png" alt="" onClick="checked_me('ePets', 'MAYBE');"/></span> 
      <span class="simptip-position-top simptip-danger" data-tooltip="{$smarty.const.LBL_PETS2}"><img {if $db_pref[0].ePets eq 'NO'}class="borderClass"{/if} id="ePets_NO_img" src="{$tconfig.tsite_images}pets3.png" alt="" onClick="checked_me('ePets', 'NO');"/></span>
      </label>-->
      <span style="display:none;">
      <input type="radio" name="Data[eChattiness]" id="eChattiness_YES" value="YES" {if $db_pref[0].eChattiness eq 'YES'} checked {/if}>
      <input type="radio" name="Data[eChattiness]" id="eChattiness_MAYBE" value="MAYBE" {if $db_pref[0].eChattiness eq 'MAYBE'} checked {/if}>
      <input type="radio" name="Data[eChattiness]" id="eChattiness_NO" value="NO" {if $db_pref[0].eChattiness eq 'NO'} checked {/if}>
      
      <input type="radio" name="Data[eMusic]" id="eMusic_YES" value="YES" {if $db_pref[0].eMusic eq 'YES'} checked {/if}>
      <input type="radio" name="Data[eMusic]" id="eMusic_MAYBE" value="MAYBE" {if $db_pref[0].eMusic eq 'MAYBE'} checked {/if}>
      <input type="radio" name="Data[eMusic]" id="eMusic_NO" value="NO" {if $db_pref[0].eMusic eq 'NO'} checked {/if}>
      
      <input type="radio" name="Data[eSmoking]" id="eSmoking_YES" value="YES" {if $db_pref[0].eSmoking eq 'YES'} checked {/if}>
      <input type="radio" name="Data[eSmoking]" id="eSmoking_MAYBE" value="MAYBE" {if $db_pref[0].eSmoking eq 'MAYBE'} checked {/if}>
      <input type="radio" name="Data[eSmoking]" id="eSmoking_NO" value="NO" {if $db_pref[0].eSmoking eq 'NO'} checked {/if}>
      
      <input type="radio" name="Data[eEcig]" id="eEcig_YES" value="YES" {if $db_pref[0].eEcig eq 'YES'} checked {/if}>
      <input type="radio" name="Data[eEcig]" id="eEcig_MAYBE" value="MAYBE" {if $db_pref[0].eEcig eq 'MAYBE'} checked {/if}>
      <input type="radio" name="Data[eEcig]" id="eEcig_NO" value="NO" {if $db_pref[0].eEcig eq 'NO'} checked {/if}>
      
      <input type="radio" name="Data[ePets]" id="ePets_YES" value="YES" {if $db_pref[0].ePets eq 'YES'} checked {/if}>
      <input type="radio" name="Data[ePets]" id="ePets_MAYBE" value="MAYBE" {if $db_pref[0].ePets eq 'MAYBE'} checked {/if}>
      <input type="radio" name="Data[ePets]" id="ePets_NO" value="NO" {if $db_pref[0].ePets eq 'NO'} checked {/if}>
      </span>
      <label><em></em><a href="javascript:void(0);" onClick="submit_frm();">{$smarty.const.LBL_SAVE}</a></label>
    </div>
    </form>
  </div>
  {include file="member_profile_left.tpl"}
  <div style="clear:both;"></div>
</div>
</div>
{literal}
  <script>
    function submit_frm(){
       document.prefrm.submit();
    }
    
    function checked_me(type, val){         
      $("#img_vYes_"+val).removeClass('borderClass');
      $("#img_vMAYBE_"+val).removeClass('borderClass');
      $("#img_vNO_"+val).removeClass('borderClass');
      $("#img_"+type+"_"+val).addClass('borderClass');
      
      $("#vYes_"+val).prop("checked", false);
      $("#vMAYBE_"+val).prop("checked", false);
      $("#vNO_"+val).prop("checked", false);
      $("#"+type+"_"+val).prop("checked", true);          
      return false;
    }
  </script>
{/literal}

{section name="pref" loop=$db_member_preferences}
{literal}
  <script>
   checked_me('{/literal}{$db_member_preferences[pref].vType}{literal}', '{/literal}{$db_member_preferences[pref].iPreferencesId}{literal}'); 
  </script>
{/literal}
{/section}