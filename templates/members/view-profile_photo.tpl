    {if $smarty.get.var_msg neq ''}          
{if $smarty.get.msg_code eq '1'}
  {literal}
    <script>        
      showNotification({type : 'success', message: '{/literal}{$var_msg}{literal}'});  
    </script>
  {/literal}
{/if}
{/if} 

{if $smarty.get.var_msg neq ''}          
{if $smarty.get.msg_code eq '0'}
  {literal}
    <script>
      showNotification({type : 'error', message: '{/literal}{$var_msg}{literal}'});
    </script>
  {/literal}
{/if}
{/if}
    <div class="body-inner-part">
      <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_PROFILE_PHOTO}</span></div>
      <!-------------------------inner-page----------------->
      <div class="main-inner-page">
        <h2>{$smarty.const.LBL_PROFILE} <span>{$smarty.const.LBL_PHOTO}</span></h2>
        {include file="member_top.tpl"}
        <div class="ratings">
          <div class="profile-photo">
            <h3>{$smarty.const.LBL_MY_PHOTO}</h3>
            <p>{$smarty.const.LBL_PHOTO_NOTE}</p>
            <div class="profile-photo-left">{if $db_photo[0]['url'] neq ''} <img src="{$db_photo[0]['url']}" alt="" id="photo" /> {if $db_photo[0].cont eq 'Yes'}<span><a href="javascript:void(0);" onclick="confirm_delete();">{$smarty.const.LBL_REMOVE_PHOTO}</a></span>{/if} {/if}</div>
           	<form method="post" name="frmphoto" id="frmphoto" enctype="multipart/form-data"  action="{$tconfig.tsite_url}index.php?file=m-profile_photo_a">
            <input type="hidden" name="action" id="action" value="">
            <div class="profile-photo-right">
            
              <div class="choose-photo">
                 <input type="file" id="vImage" name="vImage"> 
              </div>
              <span><a href="#" onclick="getfbimage();return false;"><img src="{$tconfig.tsite_images}/profil-photo-fb.png" alt="" />{$smarty.const.LBL_UPLOAD_FB_PHOTO}</a></span>
              <!--<span><a href="{$draugiem_login_button}"><img src="{$tconfig.tsite_images}/draugiem.png" alt="" />{$smarty.const.LBL_DRAUGIEM_PHOTO}</a></span>-->
              <div class="pnl-info">
                <p>{$smarty.const.LBL_PHOTO_NOTE1} <br />
                  {$smarty.const.LBL_PHOTO_NOTE2} </p>
                <a href="javascript:void(0);" onclick="upload_photo();">{$smarty.const.LBL_SAVE}</a></span>
              </div> 
             
              <div style="clear:both;"></div>
            </div>
             </form>
          </div>
        </div>
        <!-------------------------inner-page end-----------------> 
		{include file="member_profile_left.tpl"}
      </div>
      <div style="clear:both;"></div>
    </div>
{literal}
<script>

function upload_photo(){  

  document.frmphoto.action.value='edit';
  document.frmphoto.submit();
 
}  
function getfbimage()
{
 javscript:window.location='{/literal}{$tconfig.tsite_url}fbconnect.php?ctype=fbphoto{literal}';
}


function confirm_delete()
{
   ans = confirm('{/literal}{$smarty.const.LBL_SURE_DELETE_PHOTO}{literal} ?');
   if(ans == true){
    document.frmphoto.action.value = 'delete_image';
    document.frmphoto.submit();
   }else{
    return false;
   }
}   
</script>    
{/literal}
