<script type="text/javascript" src="{$tconfig.tsite_javascript}jquery.raty.js"></script>
{if $smarty.get.var_msg neq ''}          
{if $smarty.get.msg_code eq '1'}
  {literal}
    <script>        
      showNotification({type : 'success', message: '{/literal}{$var_msg}{literal}'});  
    </script>
  {/literal}
{/if}
{/if} 

{if $smarty.get.var_msg neq ''}          
{if $smarty.get.msg_code eq '0'}
  {literal}
    <script>
      showNotification({type : 'error', message: '{/literal}{$var_msg}{literal}'});
    </script>
  {/literal}
{/if}
{/if} 
<div class="body-inner-part">
  <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_RAGINGS_GIVEN}</span></div>
  <!-------------------------inner-page----------------->
  <div class="main-inner-page">
    <h2>{$smarty.const.LBL_RATINGS} <span>{$smarty.const.LBL_GIVEN}</span></h2>
    {include file="member_top.tpl"}
    <div class="ratings">
      <div class="rating-received2">
        <div class="rating-given-right-part">
           <table width="100%" border="0" cellpadding="0" cellspacing="0" class="note-from-re3">
             <tr>
                <th width="25%" align="left">{$smarty.const.LBL_MEMBER_NAME}</th>
                <th width="25%" align="left">{$smarty.const.LBL_PHOTO}</th>
                <th width="15%" align="left">{$smarty.const.LBL_RATE}</th>
                <th width="35%" align="left">{$smarty.const.LBL_GIVEN_DATE}</th>
             </tr>
             {if $db_ratings_given|@count gt 0}
              {section name=i loop=$db_ratings_given}
              {if $smarty.section.i.index % 2}
              {assign var="class" value=""}
              {else}
              {assign var="class" value="alt"}
              {/if}
              <tr class="{$class}">
                <td>{$db_ratings_given[i].vFirstName} {$db_ratings_given[i].vLastName}</td>
                <td><img src="{$db_ratings_given[i].img}" alt=""/></td>
                <td> 
				<span style="float:left; margin-left: 10px;margin-top:5px;display: block; width: 65px; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 0;">
					<span style="display: block; float:none; width: {$db_ratings_given[i].rating_width}%; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 -13px;"></span>
				</span>
				</td>
                <td>{$generalobj->DateTime($db_ratings_given[i].dAddedDate,13)}</td>
              </tr>
              {/section}
              {else}
              <tr>
               <td colspan="4"><div class="NoRating" style="text-align:center;color:red;width:100%;">{$smarty.const.LBL_NO_RECORDS_FOUND}</div></td>
              </tr>
              {/if}
           </table> 
        </div>
      </div>
    </div>
  {include file="member_rating_left.tpl"}
    </div>
  <!-------------------------inner-page end----------------->
  <div style="clear:both;"></div>
</div>
{literal} 
<script>
function findmember(){
    //alert('hello..');
    jQuery("#frmsearch").validationEngine('init',{scroll: false});
	  jQuery("#frmsearch").validationEngine('attach',{scroll: false});
		resp = jQuery("#frmsearch").validationEngine('validate');
	//	alert(resp); return false;
		if(resp == true){
			document.frmsearch.submit();
		}else{
			return false;
		}		
}
function cancelsearch()
{
	window.location =  "{/literal}{$tconfig.tsite_url}{literal}leave-Rating";
	return false;
}
</script> 
{/literal}