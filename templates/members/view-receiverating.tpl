<script type="text/javascript" src="{$tconfig.tsite_javascript}jquery.raty.js"></script>
{if $smarty.get.var_msg neq ''}          
{if $smarty.get.msg_code eq '1'}
  {literal}
    <script>        
      showNotification({type : 'success', message: '{/literal}{$var_msg}{literal}'});  
    </script>
  {/literal}
{/if}
{/if} 

{if $smarty.get.var_msg neq ''}          
{if $smarty.get.msg_code eq '0'}
  {literal}
    <script>
      showNotification({type : 'error', message: '{/literal}{$var_msg}{literal}'});
    </script>
  {/literal}
{/if}
{/if} 
<div class="body-inner-part">
  <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_RATING} {$smarty.const.LBL_RECEIVED}</span></div>
  <!-------------------------inner-page----------------->
  <div class="main-inner-page">
    <h2>{$smarty.const.LBL_RATING} <span>{$smarty.const.LBL_RECEIVED}</span></h2>
    {include file="member_top.tpl"}
    <div class="ratings">
      <div class="rating-received">
        <div class="rating-received-left-part" style="width:21%;">
          <span>{$rating} {$smarty.const.LBL_RATING}<!-- rating - 3 / 5 --> 
            <!--<img src="{$tconfig.tsite_images}blue-star.png" alt="" />-->
            <span style="float: right;margin-left: 15px;margin-top: 1px;display: block; width: 65px; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 0;">
                  <span style="display: block; width: {$rating_width}%; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 -13px;"></span>
            </span>
         </span> 
        </div>
        <div class="rating-received-right-part">
          <div class="rating-received-right-part-inner"> 
            <span>5 {$smarty.const.LBL_STARS}</span>
            <div class="progress"><img style="width:{$totalwidth[5]}%; padding: 0px 0 7px;max-width: 100%;" src="{$tconfig.tsite_images}{$THEME}/progress.jpg"></div>
            <span class="last-star">{$totrating[5]}</span> 
          </div>
          <div class="rating-received-right-part-inner"> 
            <span>4 {$smarty.const.LBL_STARS}</span>
            <div class="progress"><img style="width:{$totalwidth[4]}%; padding: 0px 0 7px;max-width: 100%;" src="{$tconfig.tsite_images}{$THEME}/progress.jpg"></div>
            <span class="last-star">{$totrating[4]}</span> 
          </div>
          <div class="rating-received-right-part-inner"> 
            <span>3 {$smarty.const.LBL_STARS}</span>
            <div class="progress"><img style="width:{$totalwidth[3]}%; padding: 0px 0 7px;max-width: 100%;" src="{$tconfig.tsite_images}{$THEME}/progress.jpg"></div>
            <span class="last-star">{$totrating[3]}</span>
          </div>
          <div class="rating-received-right-part-inner">
            <span>2 {$smarty.const.LBL_STARS}</span>
            <div class="progress"><img style="width:{$totalwidth[2]}%; padding: 0px 0 7px;max-width: 100%;" src="{$tconfig.tsite_images}{$THEME}/progress.jpg"></div>
            <span class="last-star">{$totrating[2]}</span>
          </div>
          <div class="rating-received-right-part-inner">
            <span>1 {$smarty.const.LBL_STARS}</span>
            <div class="progress"><img style="width:{$totalwidth[1]}%; padding: 0px 0 7px;max-width: 100%;" src="{$tconfig.tsite_images}{$THEME}/progress.jpg"></div>
            <span class="last-star">{$totrating[1]}</span>
          </div>
        </div>
      </div>
      {if $db_rating_from|@count gt 0}
      <div class="user-comment-list">
        <ul>
          {section name=i loop=$db_rating_from}
          <li>
            <div class="user-img2"><img src="{$db_rating_from[i].img}" alt="" /></div>
            <div class="user-profile-de">
           
              <h2 style="font-size:20px;border-bottom:none;"><div style="float:left;">{$smarty.const.LBL_RATING} : {$db_rating_from[i].iRate}</div>
              <div style="margin:0px; float:left;"> <span style="float:left; margin-left: 10px;margin-top:5px;display: block; width: 65px; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 0;">
                <span style="display: block; float:none; width: {$db_rating_from[i].rating_width}%; height: 13px; background: url({$tconfig.tsite_images}star-rating-sprite.png) 0 -13px;"></span>
            </span></div>
            <div style="color:#414141; float:right; font-family:'droid_sansregular'; font-size:13px;">{$generalobj->DateTime($db_rating_from[i].dAddedDate,7)} </div>
              </h2>
              <p><strong>{$smarty.const.LBL_FROM} {$db_rating_from[i].vFirstName} {$db_rating_from[i].vLastName} :</strong> {$db_rating_from[i].tFeedback}. </p>
            </div>
          </li>
          {/section}
        </ul>
       </div>
      {/if} 
    </div>
	{include file="member_rating_left.tpl"}
  </div>
  <!-------------------------inner-page end----------------->
  
  <div style="clear:both;"></div>
</div>
{literal} 
<script>
function findmember(){
    //alert('hello..');
    jQuery("#frmsearch").validationEngine('init',{scroll: false});
	  jQuery("#frmsearch").validationEngine('attach',{scroll: false});
		resp = jQuery("#frmsearch").validationEngine('validate');
	//	alert(resp); return false;
		if(resp == true){
			document.frmsearch.submit();
		}else{
			return false;
		}		
}
function cancelsearch()
{
	window.location =  "{/literal}{$tconfig.tsite_url}{literal}leave-Rating";
	return false;
}
</script> 
{/literal}