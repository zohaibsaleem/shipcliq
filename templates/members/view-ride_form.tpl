<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script type="text/javascript" src="{$tconfig.tsite_javascript}gmap3.js"></script>
<script type="text/javascript" src="{$tconfig.tsite_url}rrb/zebra_datepicker.src.js"></script>
<link rel="stylesheet" href="{$tconfig.tsite_url}rrb/default.css" type="text/css"> 
{if $var_err_msg neq ''}
{literal} 
<script>
      showNotification({type : 'error', message: '{/literal}{$var_err_msg}{literal}'});
</script> 
{/literal}
{/if}  
{literal}
<style>
  #map-canvas {
    height: 383px;
    margin-top: 0px;
    padding: 0px;
    width: 370px;
  }  
</style>
{/literal}
{nocache}
<div class="body-inner-part">
  <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<a href="{$tconfig.tsite_url}List-Rides-Offer">{$smarty.const.LBL_RIDES_OFFERED1}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_EDIT_RIDE} {$smarty.const.LBL_DETAILS}</span></div>
  <div class="main-inner-page">
    <h2>{$smarty.const.LBL_EDIT_RIDE}<span>{$smarty.const.LBL_DETAILS}</span></h2>
    <div class="offer-seats">      
      <form name="frmride" id="frmride" method="post" action="{$tconfig.tsite_url}index.php?file=c-offer_ride_second">
      <input type="hidden" name="distance" id="distance" value=""> 
      <input type="hidden" name="duration" id="duration" value=""> 
      <input type="hidden" name="from_lat_long" id="from_lat_long" value="">
      <input type="hidden" name="to_lat_long" id="to_lat_long" value="">
      <input type="hidden" name="loc1_lat_long" id="loc1_lat_long" value="">
      <input type="hidden" name="loc2_lat_long" id="loc2_lat_long" value="">
      <input type="hidden" name="loc3_lat_long" id="loc3_lat_long" value="">
      <input type="hidden" name="loc4_lat_long" id="loc4_lat_long" value="">
      <input type="hidden" name="loc5_lat_long" id="loc5_lat_long" value="">
      <input type="hidden" name="loc6_lat_long" id="loc6_lat_long" value="">       
      <div class="offer-seats-left">
        <div class="type-of-trip">
          <h2>{$smarty.const.LBL_TYPE_TRIP}</h2>
          <span>
          <em>{if $db_memberride[0].eRideType eq 'Reccuring'}Reccuring trip{else}One-time trip{/if}</em>
          </span> 
        </div>
        <div class="type-of-trip2">
          <h2>{$smarty.const.LBL_ROUTE}</h2>
          <span> <em>{$smarty.const.LBL_FROM} :  {$db_memberride[0].vMainDeparture}
           </em>
           <em> {$smarty.const.LBL_TO} :  {$db_memberride[0].vMainArrival} 
          </em>
          <h1>Hello World {$routearr}</h1>
          {if $routearr|@count gt 0}
          <em> {$smarty.const.LBL_RIDE_OFFER_NOTE1}
          {section name=i loop=$routearr}
          <input name="loc{$smarty.section.i.index}" id="loc{$smarty.section.i.index}" type="text" readonly class="location-input2" value="{$routearr[i]}"/>
          {/section}
          </em>
          {/if}

          <!--<em class="last"><a href="#">Add another stopover point Zohaib</a></em>-->
         </span> </div> 
        <!--------------------Div For Recurring Trip-------------------------------------->         
        {if $db_memberride[0].eRideType eq 'Reccuring'}
        <div style="display:" id="frmric" class="type-of-trip3">
          <h2>Date and time <div style="float:right;margin-right:5px;font-size:15px;">
           {if $db_memberride[0].eRoundTrip eq 'Yes'}Round Trip{/if}
           <!--<input type="checkbox" name="roundtripric" id="roundtripric" value="Yes">Round Trip-->
           </div>
          </h2>
          <span> 
          <em>
          {$smarty.const.LBL_OUTBOUND_DAY}:
          <table width="70%">
            <tr>
              <td style="background-color: #ADDE71;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;">{$db_memberride[0].vOutBoundDays}</td>
            </tr>
          </table> 
          </em>
          {if $db_memberride[0].eRoundTrip eq 'Yes'}
          <em id="returndays">
          {$smarty.const.LBL_RETURN_DAY}:
          <table width="70%">
            <tr>
              <td style="background-color: #C5F0FF;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;">{$db_memberride[0].vReturnDays}</td>
            </tr>
          </table>  
          </em>
          <!-- Zohaib Only TPL -->
          {/if}
          <em> {$smarty.const.LBL_OUTBOUND_TIME}:&nbsp;&nbsp;{$db_memberride[0].vMainOutBoundTime}
          </em> 
          {if $db_memberride[0].eRoundTrip eq 'Yes'}
          <em id="returntime"> {$smarty.const.LBL_RETURN_TIME}:&nbsp;&nbsp;{$db_memberride[0].vMainReturnTime}         
          </em> 
          {/if}
          <em>{$smarty.const.LBL_START_FROM_DATE}:&nbsp;&nbsp;{$generalobj->DateTimeFormat($db_memberride[0].dStartDate)}
          </em>             
          <em>{$smarty.const.LBL_END_TO_DATE}:&nbsp;&nbsp;{$generalobj->DateTimeFormat($db_memberride[0].dEndDate)}
          </em> 
          <em>
          <a class="cont-bot-but" href="javascript:void(0);" onClick="show_me_dates();" style="float: left;">View All Dates</a>
          </em>
          <em id="rb" class="last"></em>
          </span> 
        </div>  
        {/if}              
        <!--------------------Div For Recurring Trip-------------------------------------->
        <!--------------------Div For One Time Trip-------------------------------------->
        {if $db_memberride[0].eRideType eq 'One Time'}
        <div style="display:;" id="frmonetime" class="type-of-trip3">
          <h2>{$smarty.const.LBL_DATE_TIME}
           <div style="float:right;margin-right:5px;font-size:15px;">
            {if $db_memberride[0].eRoundTrip eq 'Yes'}Round Trip{/if}
            <!--<input type="checkbox" name="roundtriponetime" id="roundtriponetime" value="Yes">Round Trip-->
           </div></h2>
          <span> <em> {$smarty.const.LBL_DEPARTURE_DATE}:&nbsp;&nbsp;
          {$generalobj->DateTimeFormat($db_memberride[0].dDepartureDate)} - 
          {$db_memberride[0].vMainDepartureTime}
          </em> 
          {if $db_memberride[0].eRoundTrip eq 'Yes'}
          <em id="returndaystime" class="last"> {$smarty.const.LBL_RETURN_DATE}:
          {$generalobj->DateTimeFormat($db_memberride[0].dArrivalDate)} - 
          {$db_memberride[0].vMainArrivalTime}
          </em>
          {/if} 
          </span> 
          </div> 
         {/if} 
         <!--------------------Div For One Time Trip-------------------------------------->         
        <!--<a class="cont-bot-but" href="javascript:void(0);" onClick="go_next_page();">Continue</a>-->
        <div id="pricing" class="offer-a-ride">
          <h2>{$smarty.const.LBL_PRICE_PER_PESSANGER}</h2>
          {if $db_ride_price|@count gt 1}
          {section name=i loop=$db_ride_price}
          <span class="spa-off">
          <em class="ord1">{$db_ride_price[i].vStartPoint} &rarr; {$db_ride_price[i].vEndPoint}</em>
          <p class="ord2">           
            {$db_ride_price[i].fPrice}
          </p>
          </span>
          {/section}
          {/if}
          <div style="display:">   
          <span class="spa-off" style="color:#0094C8;"><em class="ord1"> {$db_memberride[0].vMainDeparture} &rarr; {$db_memberride[0].vMainArrival}</em>
          <p class="ord2">
          {$db_total_price}
          </p>
          </span>
          </div>  
        </div> 
        
        <!------------------------------Car Details----------------------------------->
         <div id="pricing" class="offer-a-ride"> 
        <h2>{$smarty.const.LBL_CAR_DETAILS}</h2> 
          <span class="spa-off"> <em class="ord1" style="width: 62%;">{$smarty.const.LBL_SELECT_CAR}:</em>
          <p class="ord2">  
          {$db_car[0].vMake} {$db_car[0].vTitle}        
          </p>
          </span>       
          <span class="spa-off"> <em class="ord1">{$smarty.const.LBL_NUMBER_SEATS_OFFERED}:</em>
          <p class="ord2">          
            {$db_memberride[0].iSeats}
          </p>
          </span>  
        </div>
        <!------------------------------Car Details----------------------------------->
        <!------------------------------Further Details----------------------------------->
        <div class="offer-a-ride3">
          <h2>{$smarty.const.LBL_FURTHER_DETAILS}</h2>
          <span>
          <!--<p>
            <input name="" type="checkbox" value="" />
            &nbsp;Publish the same comment for the departure and the return </p>-->
          <p>
            {$db_memberride[0].tDetails}  </p>
          <p>&nbsp;</p>
          <p class="offer-ride1">{$smarty.const.LBL_MAX_LUGGAGE_SIZE}:&nbsp;&nbsp;&nbsp;&nbsp;
            {$db_memberride[0].eLuggageSize}  
          </p>
          <p class="offer-ride1">{$smarty.const.LBL_I_WILL_LEAVE}:&nbsp;&nbsp;&nbsp;&nbsp;
            {if $db_memberride[0].eLeaveTime eq 'ON_TIME'}Right on time
            {elseif $db_memberride[0].eLeaveTime eq 'FIFTEEN_MINUTES'}In a 15 minute window
            {elseif $db_memberride[0].eLeaveTime eq 'THIRTY_MINUTES'}In a 30 minute window
            {elseif $db_memberride[0].eLeaveTime eq 'ONE_HOUR'}In a 1 hour window
            {elseif $db_memberride[0].eLeaveTime eq 'TWO_HOURS'}In a 2 hour window
            {/if}
          </p>
          <p class="offer-ride1">{$smarty.const.LBL_I_MAKE_DETOUR}:&nbsp;&nbsp;&nbsp;&nbsp;
            {if $db_memberride[0].eWaitTime eq 'NONE'}None
            {elseif $db_memberride[0].eWaitTime eq 'FIFTEEN_MINUTES'}15 minute detour, max.
            {elseif $db_memberride[0].eWaitTime eq 'THIRTY_MINUTES'}30 minute detour, max.
            {elseif $db_memberride[0].eWaitTime eq 'WHATEVER_IT_TAKES'}Anything is fine
            {/if}
          </p>
     <!-- <p class="offer-ride1">{$smarty.const.LBL_LADIES_ONLY}:&nbsp;&nbsp;&nbsp;&nbsp;
            {$db_memberride[0].eLadiesOnly}
          </p>
          {if $db_memberride[0].eRidePlaceType neq ''}
          <p class="offer-ride1">{$smarty.const.LBL_RIDE_PLACE}:&nbsp;&nbsp;&nbsp;&nbsp;
            {$db_memberride[0].eRidePlaceType}
          </p>
          {/if}-->
          </span> </div>
        <!------------------------------Further Details----------------------------------->
        {/nocache}
      </div> 
      </form>
      <div class="offer-seats-right"><div id="map-canvas" class="gmap3"></div></div>
    </div>
  </div>
  <div style="clear:both;"></div>
</div>

<div style="display:none">
  <div id="searchmodel" class="form-login">
    <h4>Add a trip</h4>
    <form method="post" id="add_box" name="add_box">
    <input type="hidden" value="" id="addtype" name="addtype">
    <input type="hidden" value="" id="adddate" name="adddate">  
    <input type="hidden" value="add_date" id="add_action" name="add_action">
    <div class="inner-form1">        
      <div class="singlerow-login" style="background-color:#ADDE71;width:130px;padding: 10px;border: 1px solid #D2D2D2;font-size: 16px;">
        <input type="checkbox" value="addout" id="addout" name="addout">&nbsp;{$smarty.const.LBL_OURBOUND}
      </div> 
      <div class="singlerow-login" style="background-color:#C5F0FF;width:130px;padding: 10px;border: 1px solid #D2D2D2;font-size: 16px;">
        <input type="checkbox" value="addret" id="addret" name="addret">&nbsp;{$smarty.const.LBL_RETURN}
      </div>  
      <div class="singlerow-login-log"><a href="javascript:void(0);" onclick="check_add();">Add</a></div>
      <div style="clear:both;"></div>
    </div>
    </form>           
  </div>
</div>
{literal}
  <script type="text/javascript">
      function clearThat(){ 
          var opts = {};
          opts.name = ["marker", "directionsrenderer"];
          opts.first = true;        
          $('#map-canvas').gmap3({clear:opts});         
      }
      
      function from_to(){
            clearThat();
            
            var waypts = [];
            var loc1 = '{/literal}{$loc1}{literal}';
            var loc2 = '{/literal}{$loc2}{literal}';
            var loc3 = '{/literal}{$loc3}{literal}';
            var loc4 = '{/literal}{$loc4}{literal}';
            var loc5 = '{/literal}{$loc5}{literal}';
            var loc6 = '{/literal}{$loc6}{literal}';
            
            if(loc1 != ''){
            waypts.push({location:loc1, stopover:true});
            }
            if(loc2 != ''){
            waypts.push({location:loc2, stopover:true});
            }
            if(loc3 != ''){
            waypts.push({location:loc3, stopover:true});
            }
            if(loc4 != ''){
            waypts.push({location:loc4, stopover:true});
            }
            if(loc5 != ''){
            waypts.push({location:loc5, stopover:true});
            }
            if(loc6 != ''){
            waypts.push({location:loc6, stopover:true});
            }
            
            $("#map-canvas").gmap3({
            getroute:{
            options:{
            origin:'{/literal}{$db_memberride[0].vMainDeparture}{literal}',
            destination:'{/literal}{$db_memberride[0].vMainArrival}{literal}',
            waypoints:waypts,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
            },
            callback: function(results){
            if (!results) return;
            $(this).gmap3({
            map:{
            options:{
            zoom: 13,
            center: ['{/literal}{$MAP_LATITUDE}{literal}', '{/literal}{$MAP_LONGITUDE}{literal}']             
            }
            },
            directionsrenderer:{
            options:{
            directions:results
            }
            }
            });
            }
            }
            });
            //$('#map').show();
        }
  </script>
  <script>
      function show_me_dates(){
        var request = $.ajax({  
      	  type: "POST",
      	  url: site_url+'index.php?file=c-ride_dates',  
      	  data: $("#frmride").serialize(), 	  
      	  
      	  success: function(data) {//alert(data);  
      	   $("#rb").html(data);
      		}
    	 });                                        
    	
       request.fail(function(jqXHR, textStatus) {
        alert( "Request failed: " + textStatus ); 
       });
      }
      
      function select_date(type, date){
        document.getElementById("addtype").value = type;
        document.getElementById("adddate").value = date;
        if(type == 'out'){
         document.getElementById("addout").checked=true;
         document.getElementById("addret").checked=false;
        }else if(type == 'ret'){
         document.getElementById("addout").checked=false;
         document.getElementById("addret").checked=true;
        }else if(type == 'both'){
         document.getElementById("addout").checked=true;
         document.getElementById("addret").checked=true;
        }else{
         document.getElementById("addout").checked=false;
         document.getElementById("addret").checked=false;  
        }
        $.fancybox("#searchmodel");return false;
      }
      
      function check_add(){
        var request = $.ajax({  
      	  type: "POST",
      	  url: site_url+'index.php?file=c-add_date', 
      	  data: $("#add_box").serialize(), 	  
      	  
      	  success: function(data) { //alert(data);     		 
            $.fancybox.close();
            show_me_dates();			
      		}
    	  });
        
        request.fail(function(jqXHR, textStatus) {
    	   loading_hide() ; 
         alert( "Request failed: " + textStatus ); 
    	  });
      }
      
      $('#sdate').Zebra_DatePicker({direction:  [1, 180], pair: $('#edate'),onSelect: function(view, elements){if(document.getElementById("edate").value != ''){show_me_dates();}}});  
      $('#edate').Zebra_DatePicker({direction:  [1, 180],onSelect: function(view, elements){if(document.getElementById("sdate").value != ''){show_me_dates();}}});
      
      $('#sdateone').Zebra_DatePicker({direction:  [1, 180], pair: $('#edateone')});  
      $('#edateone').Zebra_DatePicker({direction:  [1, 180]});
      
      function show_trip_type(type){
        if(type == 'onetime'){
         $("#frmric").hide();
         $("#frmonetime").show();
        }else{
         $("#frmonetime").hide();
         $("#frmric").show();
        }
      }
      
     /* $("#roundtripric").on( "click", function() {
        if($("#roundtripric").is(':checked')){
          $("#returndays").show();
          $("#returntime").show();
        }else{
          $("#returndays").hide();
          $("#returntime").hide();              
        }
      });
      
      $("#roundtriponetime").on( "click", function() {
        if($("#roundtriponetime").is(':checked')){
          $("#returndaystime").show();
        }else{
          $("#returndaystime").hide();
          document.getElementById("edateone").value = '';
          document.getElementById("onetihourend").value = '';
          document.getElementById("onetimeminend").value = '';
        }
      });
      
      function magage_trip_type(){
        $("#onetime").attr('checked', 'checked');
        show_trip_type('onetime');
      }
      
      function go_next_page(){
        document.frmride.submit();
      }
      
      magage_trip_type();       */
  </script>
{/literal}
{literal}
<script>
from_to('{/literal}{$db_memberride[0].vMainDeparture}{literal}', '{/literal}{$db_memberride[0].vMainArrival}{literal}');</script>
{/literal}
