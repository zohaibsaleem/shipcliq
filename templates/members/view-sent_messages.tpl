<div class="body-inner-part">
  <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_MESSAGES}</span></div>
  <div class="main-inner-page">
    <h2>{$smarty.const.LBL_MESSAGES}</h2>
    <div class="dashbord">
      {include file="member_top.tpl"}
      <div class="bro-message">
        <div class="tab-pane">
          <ul>
            <!--<li> <a href="#"> Questions &amp; Answers </a> </li> -->
            <li> <a href="{$tconfig.tsute_url}received-messages"> {$smarty.const.LBL_RECEIVED_MESSAGES} <em>{$tot_unread}</em></a> </li>
            <li> <a href="javascript:void(0);" class="active"> {$smarty.const.LBL_SENT_MESSAGES} </a> </li>
            <li> <a class="last" href="{$tconfig.tsute_url}archived-messages" > {$smarty.const.LBL_ARCHIVED_MESSAGES}</a> </li>
          </ul>
        </div>
        <div class="messages-container">
          {if $db_messages|@count gt 0}
          {section name="messages" loop=$db_messages}
          <label>
            <span>
            <img src="{$db_messages[messages].img}" alt="" style="height:70px;width:70px;"/><a href="{$tconfig.tsite_url}message-details/sent/{$db_messages[messages].iMessageId}">{$db_messages[messages].vFirstName} {$db_messages[messages].vLastName}</a></span>
            <span class="stap2"><a href="{$tconfig.tsite_url}message-details/sent/{$db_messages[messages].iMessageId}">{$db_messages[messages].tMessage|strip_tags|truncate:50}</a></span> 
            <span class="stap3">{$generalobj->DateTime($db_messages[messages].dAddedDate,7)}
              <!--<a href="#"><img src="{$tconfig.tsite_images}mail-delete-archive.png" alt="" /></a>-->
            </span> 
          </label>
          {/section}
          {else}
          <div class="main-block">
          {$smarty.const.LBL_NO} {$smarty.const.LBL_SENT_MESSAGES}</div>
          {/if}
        </div>
        {if $db_messages|@count gt 0}
        <div class="paging">{$page_link}</div>
        {/if}
      </div>
      <div style="clear:both;"></div>
    </div>
    <div style="clear:both;"></div>
  </div>
  <div style="clear:both;"></div>
</div>