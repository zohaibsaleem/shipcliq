{if $smarty.get.var_msg neq ''}          
{if $smarty.get.msg_code eq '1'}
  {literal}
    <script>        
      showNotification({type : 'success', message: '{/literal}{$var_msg}{literal}'});  
    </script>
  {/literal}
{/if}
{/if} 

{if $smarty.get.var_msg neq ''}          
{if $smarty.get.msg_code eq '0'}
  {literal}
    <script>
      showNotification({type : 'error', message: '{/literal}{$var_msg}{literal}'});
    </script>
  {/literal}
{/if}
{/if} 
<div class="body-inner-part">
  <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_SOCIAL_SHARING}</span></div>
  <!-------------------------inner-page----------------->
  <div class="main-inner-page">
    <h2>{$smarty.const.LBL_SOCIAL_SHARING} </h2>
    {include file="member_top.tpl"}
   <div class="ratings">
          <div class="social-sharing">
            <div class="social-sharing-inner">
            <form name="frmsocial" enctype="multipart/form-data" id="frmsocial" action="{$tconfig.tsite_url}index.php?file=m-socialsharing_a" method="post">
              <h2>{$smarty.const.LBL_SOCIAL_SHARING}</h2>
            <!--  <p><img src="{$tconfig.tsite_images}/profil-photo-fb.png" alt="" />Connected - ( <a href="#">disconnect</a> ) </p> -->
              <p><img src="{$tconfig.tsite_images}/profil-photo-fb.png" alt="" />{if $db_member[0].iFBId neq '0'}{$smarty.const.LBL_CONNECTED} &nbsp;<a href="{$tconfig.tsite_url}disconnect_social.php">({$smarty.const.LBL_DISCONNECT})</a>{else} {$smarty.const.LBL_DISCONNECT} &nbsp;<a href="{$tconfig.tsite_url}fbconnect.php?ctype=fbsocial">({$smarty.const.LBL_CONNECT_NOW})</a>{/if} </p>
              <p>{$smarty.const.LBL_POST_TO_FACEBOOK} :</p>
              <ul>
                <li>
                  <input name="eOffrer_RidePost" id="eOffrer_RidePost" type="checkbox" value="Yes" class="radio-input" {if $db_member[0].eOffrer_RidePost eq 'Yes'}checked {/if}/>
                <!--  <input name="" type="radio" value="" class="radio-input" /> -->
                  {$smarty.const.LBL_YOUR_OFFER_RIDE} </li>
                <!--<li>
                  <input name="eReceiveRatingsPost" id="eReceiveRatingsPost" type="checkbox" value="Yes" class="radio-input" {if $db_member[0].eReceiveRatingsPost eq 'Yes'}checked {/if}/>
                  you receive a rating (5 stars only) </li>
                <li>
                  <input name="eGiveRatingsPost" id="eGiveRatingsPost" type="checkbox" value="Yes" class="radio-input" {if $db_member[0].eGiveRatingsPost eq 'Yes'}checked {/if}/>
                  you rate another member (5 stars only) </li> -->
              </ul>
              <div class="notification-save"><a href="javascript:void(0);" onclick="javascript:checknotification(); return false;">{$smarty.const.LBL_SAVE}</a></div>
             </form>
            </div>
          </div>
      </div>
    <!-------------------------inner-page end-----------------> 
   {include file="member_profile_left.tpl"}
    </div>
  <div style="clear:both;"></div>
</div>
{literal} 
<script>
function checknotification(){
    //alert('hello..');
    jQuery("#frmsocial").validationEngine('init',{scroll: false});
	  jQuery("#frmsocial").validationEngine('attach',{scroll: false});
		resp = jQuery("#frmsocial").validationEngine('validate');
	//	alert(resp); return false;
		if(resp == true){
			document.frmsocial.submit();
		}else{
			return false;
		}		
}
</script> 
{/literal}
