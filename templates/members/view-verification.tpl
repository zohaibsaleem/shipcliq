{if $var_msg neq ''}
{if $msg_code eq '1'}
  {literal}
<script>
      showNotification({type : 'success', message: '{/literal}{$var_msg}{literal}'});
    </script>
{/literal}
{/if}
{/if}

{if $var_msg neq ''}
{if $msg_code eq '0'}
  {literal}
<script>
  showNotification({type : 'error', message: '{/literal}{$var_msg}{literal}'});
</script>
{/literal}
{/if}
{/if}
<div class="body-inner-part">
  <div class="bradcram"><span><a href="{$tconfig.tsite_url}">{$smarty.const.LBL_HOME}</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;{$smarty.const.LBL_EMAIL_VARIFICATION}</span></div>
  <div class="main-inner-page">
    <h2>{$smarty.const.LBL_MEMBER1} <span>{$smarty.const.LBL_VERIFICATION}</span></h2>
    {include file="member_top.tpl"}
    <div class="ratings">
      <div class="email-verification">
        <h3>{$smarty.const.LBL_MY_VERIFICATION}</h3>
        <div class="verifications-zone">
          <form name="frmemailverify" enctype="multipart/form-data" id="frmemailverify" action="{$tconfig.tsite_url}index.php?file=m-verification_a" method="post">
            <input type="hidden" name="action" id="action" value="emailverify">
            <input type="hidden" name="vEmail" id="vEmail" value="{$db_member[0].vEmail}">
            <input type="hidden" name="vName" id="vName" value="{$db_member[0].vFirstName} {$db_member[0].vLastName}">
            {if $db_member[0].eEmailVarified eq 'No'} <img src="{$tconfig.tsite_images}/verification-no-check.png" alt="">
            <h2>{$smarty.const.LBL_EMAIL}: {$smarty.const.LBL_NOT} {$smarty.const.LBL_VERIFIED} </h2>
            {else} <img src="{$tconfig.tsite_images}/verification-checked.png" alt="">
            <h2>{$smarty.const.LBL_EMAIL}: {$smarty.const.LBL_VERIFIED} </h2>
            {/if}
            <p>{$smarty.const.LBL_YOUR_EMAIL_ADD}: <strong>{$db_member[0].vEmail}</strong>&nbsp;&nbsp;<!--(&nbsp;<a href="#">edit</a>&nbsp;)--> </p>
            {if $db_member[0].eEmailVarified eq 'No'}
              <p>{$smarty.const.LBL_SENT_MAIL} {$smarty.const.LBL_NOTGET_EMAIL} <a href="javascript:void(0);" onclick="javascript:checkmail(); return false;">{$smarty.const.LBL_CLICK_HERE}</a> {$smarty.const.LBL_NEW_EMAIL}</p>
              <!--<span><a href="javascript:void(0);" onclick="javascript:checkmail(); return false;">{$smarty.const.LBL_VERIFY_THIS}</a></span>-->
            {/if}
          </form>
        </div>
        {if $PHONE_VERIFICATION_REQUIRED eq 'Yes'}
        <div class="verifications-zone">
         {if $db_member[0].ePhoneVerified eq 'No'}
          <img src="{$tconfig.tsite_images}/verification-no-check.png" alt="">
          <h2>{$smarty.const.LBL_PHONE}: {$smarty.const.LBL_NOT} {$smarty.const.LBL_VERIFIED}</h2>
         {else}
         <img src="{$tconfig.tsite_images}/verification-checked.png" alt="">
         <h2>{$smarty.const.LBL_PHONE}: {$smarty.const.LBL_VERIFIED}</h2>
         {/if}

          <p>{$smarty.const.LBL_YOUR_PHONE}: <strong>{$db_country[0].vPhoneCode}{$db_member[0].vPhone}</strong>&nbsp;&nbsp;(&nbsp;<a href="{$tconfig.tsite_url}personal-information">edit</a>&nbsp;) </p>
         {if $db_member[0].ePhoneVerified eq 'No'}  <span><a href="{$tconfig.tsite_url}sendsms.php?to_number={$db_member[0].vPhone}&country_code={$db_country[0].vPhoneCode}&email={$db_member[0].vEmail}">{$smarty.const.LBL_VERFY_NUMBER}</a></span> {/if} </div>
         {/if}

		 {if $LICENSE_VERIFICATION_REQUIRED eq 'Yes'}

			<!-- LICENSE User varification -->
				<div class="verifications-zone ">
					<form name="frmlicenseverify" enctype="multipart/form-data" id="frmlicenseverify" action="{$tconfig.tsite_url}index.php?file=m-verification_a" method="post">
						<input type="hidden" name="action" id="action" value="licenseverify">
						<input type="hidden" name="vEmail" id="vEmail" value="{$db_member[0].vEmail}">
						<input type="hidden" name="vName" id="vName" value="{$db_member[0].vFirstName} {$db_member[0].vLastName}">
						{if $db_member[0].vLicense eq '' && $db_member[0].eLicenseStatus eq 'Pending'}
						<img src="{$tconfig.tsite_images}/verification-no-check.png" alt="" >
						<h2>{$smarty.const.LBL_LICENSE_USER}: {$smarty.const.LBL_NOT} {$smarty.const.LBL_VERIFIED} </h2>
						<input type="file" name="vLicense"  id="vLicense" required onchange="filename();">
						<input type="submit" name="submit"  id="submit" value="Add License">
						{else if $db_member[0].vLicense neq '' && $db_member[0].eLicenseStatus eq 'Pending'}
						<img src="{$tconfig.tsite_images}/verification-no-check.png" alt="" >
						<h2>{$smarty.const.LBL_LICENSE_USER}: {$smarty.const.LBL_NOT} {$smarty.const.LBL_VERIFIED} </h2>
						{$db_member[0].vLicense} - Awaiting from Admin side to approve
						{else if $db_member[0].eLicenseStatus eq 'Unapproved'}
						<img src="{$tconfig.tsite_images}/verification-no-check.png" alt="" >
						<h2>{$smarty.const.LBL_LICENSE_USER}:  {$smarty.const.LBL_UNAPPROVED} </h2>
						<input type="file" name="vLicense"  id="vLicense" required onchange="filename();">
						<input type="submit" name="submit"  id="submit" value="Add License">
						{else}
						<img src="{$tconfig.tsite_images}/verification-checked.png" alt="" >
						<h2>{$smarty.const.LBL_LICENSE_USER}: {$smarty.const.LBL_VERIFIED} </h2>
						<a href="{$path}/{$db_member[0].vLicense}" target="_blank">view</a>
						{/if}
					</form>
				</div>

		 {/if}
		 {if $CARPAPER_VERIFICATION_REQUIRED eq 'Yes'}

			<!-- LICENSE User varification -->
				<div class="verifications-zone ">
					<form name="frmcarverify" enctype="multipart/form-data" id="frmcarverify" action="{$tconfig.tsite_url}index.php?file=m-verification_a" method="post">
						<input type="hidden" name="action" id="action" value="carpaperverify">
						<input type="hidden" name="vEmail" id="vEmail" value="{$db_member[0].vEmail}">
						<input type="hidden" name="vName" id="vName" value="{$db_member[0].vFirstName} {$db_member[0].vLastName}">
						{if $db_member[0].vCarPaper eq '' && $db_member[0].eCarPaperStatus eq 'Pending'}
						<img src="{$tconfig.tsite_images}/verification-no-check.png" alt="" >
						<h2>{$smarty.const.LBL_CAR_PAPER}: {$smarty.const.LBL_NOT} {$smarty.const.LBL_VERIFIED} </h2>
						<input type="file" name="vCarPaper"  id="vCarPaper" required onchange="filename();">
						<input type="submit" name="submit"  id="submit" value="Add Proof">
						{else if $db_member[0].vCarPaper neq '' && $db_member[0].eCarPaperStatus eq 'Pending'}
						<img src="{$tconfig.tsite_images}/verification-no-check.png" alt="" >
						<h2>{$smarty.const.LBL_CAR_PAPER}: {$smarty.const.LBL_NOT} {$smarty.const.LBL_VERIFIED} </h2>
						{$db_member[0].vCarPaper} - Awaiting from Admin side to approve
						{else if $db_member[0].eCarPaperStatus eq 'Unapproved'}
						<img src="{$tconfig.tsite_images}/verification-no-check.png" alt="" >
						<h2>{$smarty.const.LBL_CAR_PAPER}:  {$smarty.const.LBL_UNAPPROVED} </h2>
						<input type="file" name="vCarPaper"  id="vCarPaper" required onchange="filename();">
						<input type="submit" name="submit"  id="submit" value="Add Proof">
						{else}
						<img src="{$tconfig.tsite_images}/verification-checked.png" alt="" >
						<h2>{$smarty.const.LBL_CAR_PAPER}: {$smarty.const.LBL_VERIFIED} </h2>
						<a href="{$path}/{$db_member[0].vCarPaper}" target="_blank">view</a>
						{/if}
					</form>
				</div>

		 {/if}

		 {*Paypal email varification code *}
        {if $PAYMENTEMAIL_VERIFICATION_REQUIRED eq 'Yes'}
			<div class="verifications-zone">
			{if $db_member[0].ePaymentEmailVerified eq 'No'}
				<img src="{$tconfig.tsite_images}/verification-no-check.png" alt="">
				<h2>{$smarty.const.LBL_PAYMENT_EMAIL}: {$smarty.const.LBL_NOT} {$smarty.const.LBL_VERIFIED}</h2>
				<p>{$smarty.const.LBL_PAYMENT_EMAIL}: <strong>{$db_member[0].vPaymentEmail}</strong>&nbsp;&nbsp;(&nbsp;<a href="{$tconfig.tsite_url}personal-information">edit</a>&nbsp;) </p>
			{else}
				<img src="{$tconfig.tsite_images}/verification-checked.png" alt="">
				<h2>{$smarty.const.LBL_PAYMENT_EMAIL}: {$smarty.const.LBL_VERIFIED}</h2>
				<p>{$smarty.const.LBL_PAYMENT_EMAIL}: <strong>{$db_member[0].vPaymentEmail}</strong></p>
			{/if}

			  <form name="frmpaypalemailverify" enctype="multipart/form-data" id="frmpaypalemailverify" action="{$tconfig.tsite_url}index.php?file=m-verification_a" method="post">
				<input type="hidden" name="action" id="action" value="paymentemailverify">
				<input type="hidden" name="vEmail" id="vEmail" value="{$db_member[0].vPaymentEmail}">
				<input type="hidden" name="vName" id="vName" value="{$db_member[0].vFirstName} {$db_member[0].vLastName}">
				{if $db_member[0].ePaymentEmailVerified eq 'No'}
				  <p>{$smarty.const.LBL_SENT_MAIL} {$smarty.const.LBL_NOTGET_EMAIL} <a href="javascript:void(0);" onclick="javascript:checkpaymail(); return false;">{$smarty.const.LBL_CLICK_HERE}</a> {$smarty.const.LBL_NEW_EMAIL}</p>
				{/if}
			  </form>
			</div>
         {/if}
      </div>
    </div>
    <!-------------------------inner-page end----------------->
	{include file="member_profile_left.tpl"}
  </div>
  <div style="clear:both;"></div>
</div>
{literal}
<script>
function filename(){

	var vLicense = document.getElementById("vLicense").value;
    if(vLicense != '')
    {

        var vLicense=vLicense.split(".");
        if(vLicense[1] != 'pdf' && vLicense[1] != 'doc' && vLicense[1] != 'jpg' && vLicense[1] != 'jpeg' && vLicense[1] != 'gif' && vLicense[1] != 'png')
        {
          alert("Only .jpeg .png, .gif, .doc, .pdf Files Are Supported..");
          document.getElementById("vLicense").value = "";
          return false;
        }
    }
	if(vCarPaper != '')
    {

        var vCarPaper=vCarPaper.split(".");
        if(vCarPaper[1] != 'pdf' && vCarPaper[1] != 'doc' && vCarPaper[1] != 'jpg' && vCarPaper[1] != 'jpeg' && vCarPaper[1] != 'gif' && vCarPaper[1] != 'png')
        {
          alert("Only .jpeg .png, .gif, .doc, .pdf Files Are Supported..");
          document.getElementById("vCarPaper").value = "";
          return false;
        }
    }
}
function checkmail(){
 resp = jQuery("#frmemailverify").validationEngine('validate');// alert(resp);return false;
	if(resp == true){
		document.frmemailverify.submit();
	}else{
		return false;
	}
}
function checkpaymail(){
 resp = jQuery("#frmpaypalemailverify").validationEngine('validate');// alert(resp);return false;
	if(resp == true){
		document.frmpaypalemailverify.submit();
	}else{
		return false;
	}
}

</script>
{/literal}
