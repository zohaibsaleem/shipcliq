{literal}<script>$(document).ready(function(){$(".editbooking").colorbox({width:"400px", height:"240px", opacity:"0.62"});});</script>{/literal}
{if $iEventId eq ''}
{literal}<script>$(document).ready(function() { upload_event_logo({/literal}{$id}{literal});});</script>{/literal}
{else}
{literal}<script>$(document).ready(function() { upload_event_edit_logo({/literal}{$iEventId}{literal});});</script>{/literal}
{/if}
{if $var_msg neq ''}
  {literal}
    <script>
      shownotificationMessage('{/literal}{$var_msg}{literal}', 'error')
    </script>
  {/literal}
{/if} 
<div class="about-page">
  <h2>Add Event</h2>
  <form name="frmevent" id="frmevent" method="post" action="{$tconfig.tsite_url}index.php?file=o-add_event_a">
  <input type="hidden" name="iEventId" id="iEventId" value="{$iEventId}">
  <input type="hidden" name="uplogo" id="uplogo" value="{$event[0].uplogo}">
  <div class="right-about-part">
    <h1>Add Event</h1>
    <div class="page-r">
      <div class="add-event">
        <label>
        <em>Event Title :</em>   
        <p><input type="text"  name="vTitle" id="vTitle" class="validate[required] form-input" style="width:309px;" value="{$event[0].vTitle}"></p>
        </label>
      </div>
      <div class="add-event">
        <label>
        <em>Event Image :</em>   
        <p><input type="file" name="vImage" id="vImage"/></p>
        </label>
      </div>
      <div id="logoimgdiv" {if $event[0].img_url eq ''}style="display:none"{/if} class="add-event">
        <label>
        <em>&nbsp;</em>   
        <p><img id="logoimg" src="{$event[0].img_url}" style=""></p>
        </label>
      </div> 
      <div id="logoimgdeletediv" class="add-event" {if $event[0].img_url eq ''}style="display:none"{/if}>
        <label><em>&nbsp;</em>
         <p><div class="singlerow-login10"><a href="javascript:void(0);" onClick="delete_event_image({$iEventId});">Delete Image</a></div></p>       
        </label>
      </div>         
      <div class="add-event">
        <label style="width:17%;">
        <em>Select Category :</em>
        </label>
        <div id="catdivadd" class="chak-box3" style="float:right;margin-right: 79px;">
        {section name="evecat" loop=$event_category}
        <span><input type="checkbox" value="{$event_category[evecat].iCategoryId}" name="iCategoryId[]" id="iCategoryId" {if in_array($event_category[evecat].iCategoryId, $event[0].iCategoryId)} checked {/if}>&nbsp;{$event_category[evecat].vCategory}</span> 
        {/section}              
        </div>                  
      </div> 
      <div class="add-event">
        <label style="width:20%;">
        <em></em>
        </label>          
        <span style="flot:left;width: 100%;"><strong>Note: </strong>If event categoty is not available which you want to add please <a href="{$tconfig.tsite_url}index.php?file=o-add_new_category" style="font-size:17px;color: #92A20A;" class="editbooking">click here</a> to add new event category.</span>          
      </div> 
      <div class="add-event">
        <label style="width:20%;">
        <em>Date Type :</em>
        </label>
        <p style="width:auto;">
          <input name="eEventType" id="Flexible" type="radio" value="Flexible" {if $event[0].eEventType neq 'Fixed'}checked="checked"{/if} onClick="show_hide_ebent_time(this.value);"/>
          &nbsp;Flexible&nbsp;&nbsp;&nbsp;
          <input name="eEventType" id="Fixed" type="radio" value="Fixed" {if $event[0].eEventType eq 'Fixed'}checked="checked"{/if} onClick="show_hide_ebent_time(this.value);"/>
          &nbsp; Fixed</p>        
      </div>
      <div id="event_date" class="add-event" style="display:none;">
        <label style="width:20%;">
        <em>Date :</em>
        </label>
        <p style="width:auto;"> From <input name="dStartDate" id="dStartDate" type="text" class="form-input" style="width:100px;" value="{$event[0].dStartDate}"/> To <input name="dEndDate" id="dEndDate" type="text" class="form-input" style="width:100px;" value="{$event[0].dEndDate}"/></p>         
      </div>
      <div id="event_time" class="add-event" style="display:none;">
        <label style="width:20%;">
        <em>Time :</em>
        </label>
        <p style="width:auto;"> From <input name="vStartTime"  id="vStartTime" type="text" class="form-input" style="width:125px;" value="{$event[0].vStartTime}"/> To <input name="vEndTime" id="vEndTime"  type="text" class="form-input" style="width:125px;" value="{$event[0].vEndTime}"/></p>
        
      </div>
      <div class="add-event">
        <label>
        <em>Where :</em>   
        <p><textarea  name="vAddress" id="vAddress" class="validate[required] form-input" style="width:309px; height:70px;">{$event[0].vAddress}</textarea></p>
        </label>
      </div>
      <div class="add-event">
        <label>
        <em>City :</em>   
        <p><input type="text"  name="vCity" id="vCity" class="validate[required] form-input" style="width:309px;" value="{$event[0].vCity}"></p>
        </label>
      </div>
      
      <div class="add-event">
        <label>
        <em>Country :</em>   
        <p>
          <select class="validate[required]" id="vCountry" name="vCountry" style="width:322px; float:left;" onChange="get_county_list(this.value);">
            <option value="">-- Please Select --</option>
            {section name=i loop=$db_country}
              <option value="{$db_country[i].vCountryCode}" {if $event[0].vCountry eq ''}{if $db_country[i].vCountryCode eq 'US'} selected {/if}{else}{if $db_country[i].vCountryCode eq $event[0].vCountry} selected {/if}{/if}>{$db_country[i].vCountry}</option>
            {/section}
          </select>
        </p>
        </label>
      </div>
      
      <div class="add-event">
        <label>
        <em>State :</em>   
        <p id="county_list">
          <select style="width:322px; float:left;" id="vState" name="vState" class="validate[required]">
            <option value="">-- Please Select --</option>
          </select>
        </p>
        </label>
      </div>
      
      <div class="add-event">
        <label style="width:17%;">
        <em>Skills Required :</em>
        </label>
        <div class="chak-box-new1">
          {section name="skills" loop=$event_skills}
         <em> <input name="iSkillId[]" name="iSkillId" type="checkbox" value="{$event_skills[skills].iSkillId}" {if in_array($event_skills[skills].iSkillId, $event[0].iSkillId)} checked {/if}/>{$event_skills[skills].vTitle}&nbsp;&nbsp;</em>
          {/section}
        </div>         
      </div>
      <div class="add-event">
        <label>
        <em>About :</em>
        <p>
          <textarea  name="tDetails" id="tDetails" class="validate[required] form-input" style="width:309px; height:70px;">{$event[0].tDetails}</textarea>
        </p>
        </label>
      </div>
      <div class="add-event">
        <label style="width:17%;">
        <em>Good Match For :</em>
        </label>
         <div class="chak-box-new1">
          {section name="suitables" loop=$event_suitables}
          <em><input name="iSuitableTypeId[]" name="iSuitableTypeId" type="checkbox" value="{$event_suitables[suitables].iSuitableTypeId}" {if in_array($event_suitables[suitables].iSuitableTypeId, $event[0].iSuitableTypeId)} checked {/if}/>{$event_suitables[suitables].vSuitableTitle}&nbsp;&nbsp;</em>
          {/section}
        </div> 
        
      </div>
      <div class="add-event">
        <label>
        <em>Requirement <br />
        & Commitment :</em>
        <p>
          <textarea  name="tRequirements" id="tRequirements" class="form-input" style="width:309px; height:70px;">{$event[0].tRequirements}</textarea>
        </p>
        </label>
      </div>
      <div class="add-event">
        <label style="width:20%;"><em>&nbsp;</em>
         </label>
        <input type="button" value="Submit" name="Submit" class="button" onClick="check_event();">
        &nbsp;&nbsp;
        <input type="button" value="Cancel" name="Submit" onClick="redirectcancel('{$tconfig.tsite_url}/my-events')" class="button">
       
      </div>
    </div>
  </div>
  </form>
  {include file="member_left.tpl"} 
  <div style="clear:both;"></div>
</div>
{if $event[0].eEventType eq 'Fixed'}
{literal}
  <script>
    show_hide_ebent_time('Fixed');
  </script>
{/literal}
{/if}
{if $event[0].uplogo neq ''}{literal}<script>show_regimage('{/literal}{$event[0].uplogo}{literal}', '{/literal}{$event[0].imageurl}{literal}', '{/literal}{$event[0].width}{literal}', '{/literal}{$event[0].height}{literal}');</script>{/literal}{/if}
{literal}<script>if('{/literal}{$event[0].vState}{literal}' != '' || '{/literal}{$event[0].vCountry}{literal}' != ''){get_county_list('{/literal}{$event[0].vCountry}{literal}', '{/literal}{$event[0].vState}{literal}');}</script>{/literal}
{literal}<script>if('{/literal}{$event[0].vCountry}{literal}' == '' ){get_county_list('US');}</script>{/literal}