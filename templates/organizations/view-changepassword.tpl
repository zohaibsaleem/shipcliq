{if $smarty.get.var_msg neq ''}
{if $smarty.get.msg_code eq 1}
{literal}
  <script>
    shownotificationMessage('{/literal}{$smarty.get.var_msg}{literal}', 'error');
  </script>
{/literal}
{/if}{/if} 

{if $smarty.get.var_msg neq ''}
{if $smarty.get.msg_code eq 0}
{literal}
  <script>
    shownotificationMessage('{/literal}{$smarty.get.var_msg}{literal}', 'success');
  </script>
{/literal}
{/if}{/if}
<div class="about-page">
  <h2>Change Password</h2>
  <div class="right-about-part">
    <h1>Change Password</h1>
    <form name="frmchangepass" id="frmchangepass" action="{$tconfig.tsite_url}index.php?file=o-changepassword" method="post">
    <input type="Hidden" name="mode" value="changepassword">
    <div class="form-login-edit">
      <h4>Password Information</h4>
      <div class="inner-editprofile">
        <label style="width:180px;">Old Password : <strong>*</strong></label>
        <div class="singlerow-editprofile">              
          <input style="width:300px;" name="vOldPassword" id="vOldPassword" type="password" class="validate[required,minSize[6]] cont-input">
        </div>
        <label style="width:180px;">New Password : <strong>*</strong></label>
        <div class="singlerow-editprofile">                
          <input style="width:300px;" name="vPassword" id="vPassword2" type="password" class="validate[required,minSize[6]] cont-input">
        </div>
        <label style="width:180px;">Re-Type New Password : <strong>*</strong></label>
        <div class="singlerow-editprofile">                 
          <input style="width:300px;" name="vPasswordretype" id="vPasswordretype" type="password" class="validate[required,equals[vPassword2]] cont-input"><!--equals[vPassword]-->
        </div>
        <label style="width:180px;"></label>
        <div class="singlerow-edit"><a onclick="javascript:checkvalidpass();return false;" href="javascript:void(0);">Update</a> <a onclick="document.frmchangepass.reset();return false;" href="javascript:void(0);">Reset</a></div>
        <div style="clear:both;"></div>
      </div>
    </div>
    </form>
  </div>
  {include file="member_left.tpl"} 
  <div style="clear:both;"></div>
</div>









