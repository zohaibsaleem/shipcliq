{literal}<script>$(document).ready(function() {upload_profile_logo({/literal}{$iOrganizationId}{literal});});</script>{/literal}
{if $var_msg neq ''}
  {literal}
    <script>
      shownotificationMessage('{/literal}{$var_msg}{literal}', 'success')
    </script>
  {/literal}
{/if}  
<div class="about-page">
  <h2>Edit Profile</h2>
  <form name="frmeditprofile" id="frmeditprofile" action="{$tconfig.tsite_url}index.php?file=o-edit_profile_a" method="post">    
  <input type="hidden" name="editact" id="editact" value="edit">
  <div class="right-about-part">
    <h1>Edit Profile</h1>
    <div class="page-r">
      <!--for fail-->
      <!--for success-->
      <p>Edit your profile information below. Lines marked with <span style="color:#ff0000;">*</span> are mandatory</p>
      <h3>Login Information</h3>
      <div class="row3">
        <label>E-mail: <span></span></label>
        <span style="line-height:30px;">{$organization[0].vEmail}</span> </div>
      <hr>
      <br>
      <h3>Organization Details</h3>
      <div class="row3">
        <label>Organization Name:</label>
        <input type="text"  class="validate[required] form-input" value="{$organization[0].vOrganizationName}"  name="Data[vOrganizationName]" id="vOrganizationName" style="width:280px;">
      </div>
      
      <div class="row3">
        <label>First Name:</label>
        <input type="text"  class="validate[required] form-input"  value="{$organization[0].vFirstName}" name="Data[vFirstName]" id="vFirstName" style="width:280px;">
      </div>
      
      <div class="row3">
        <label>Last Name:</label>
        <input type="text"  class="validate[required] form-input"  value="{$organization[0].vLastName}" name="Data[vLastName]" id="vLastName" style="width:280px;">
      </div>
      
      <div class="row3">
        <label>Profile Image(Logo):</label>
        <div style="float:left;margin-bottom: 10px;"><input type="file" name="vImage" id="vImage"/></div>
      </div> 
            
      <div id="logoimgdiv" class="row3" {if $organization[0].img_url eq ''}style="display:none"{/if}>
        <label>&nbsp;</label>
         <div style="float:left;margin-bottom: 10px;"><img id="logoimg" src="{$organization[0].img_url}"></div>       
      </div>
      <div id="logoimgdeletediv" class="row3" {if $organization[0].img_url eq ''}style="display:none"{/if}>
        <label>&nbsp;</label>
         <div style="float:left;margin-bottom: 10px;"><div class="singlerow-login10"><a href="javascript:void(0);" onClick="delete_profile_image({$iOrganizationId});">Delete Image</a></div></div>       
      </div>
      
      <div class="row3">
        <label>Address :</label>
         <textarea  name="Data[vAddress]" id="vAddress" class="validate[required] form-input" style="width:280px; height:70px;">{$organization[0].vAddress}</textarea>       
      </div>
      
      <div class="row3">
        <label>City :</label>
        <input type="text" class="validate[required] form-input" value="{$organization[0].vCity}"  name="Data[vCity]" id="vCity" style="width:280px;">
      </div>
      
      <div class="row3">
        <label>Country :</label>         
        <select id="vCountry" name="Data[vCountry]" style="width:292px;" onChange="get_county_list_profile(this.value);">
          <option value="">-- Please Select --</option>
        {section name=i loop=$db_country}
          <option value="{$db_country[i].vCountryCode}" {if $organization[0].vCountry eq ''}{if $db_country[i].vCountryCode eq 'US'} selected {/if}{else}{if $db_country[i].vCountryCode eq $organization[0].vCountry} selected {/if}{/if}>{$db_country[i].vCountry}</option>
        {/section}
        </select>
      </div>
      
      <div class="row3">
        <label>State :</label>
        <span id="county_list">
          <select style="width:292px;" id="vState" name="Data[vState]" class="validate[required]">
            <option value="">-- Please Select --</option>
          </select>
        </span>
      </div>
      
      <div class="row3">
        <label>Phone :</label>
        <input type="text" class="validate[custom[phone]] form-input" value="{$organization[0].vPhone}" name="Data[vPhone]" id="vPhone" style="width:280px;">
      </div>
      
      <div class="row3">
        <label>Fax :</label>
        <input type="text" class="form-input"  value="{$organization[0].vFax}" name="Data[vFax]" id="vFax" style="width:280px;">
      </div>
      
      <div class="row3">
        <label>Website :</label>
        <input type="text" class="validate[custom[url]]form-input"  value="{$organization[0].vURL}" name="Data[vURL]" id="vURL" style="width:280px;" placeholder="http://">
      </div>
      
      <div class="row3">
        <label> About:</label>
        <textarea  name="Data[tDescription]" id="tDescription" class="form-input" style="width:280px; height:70px;">{$organization[0].tDescription}</textarea>
      </div>
      
       <div class="row3">
     <label>Interest Areas :</label>
        <div class="chak-box-new">
          {section name="areas" loop=$interest_areas}
           <em> <input name="iCategoryId[]" id="iCategoryId" type="checkbox" value="{$interest_areas[areas].iCategoryId}" {if in_array($interest_areas[areas].iCategoryId, $organization_interest_areas)} checked {/if}/>&nbsp;{$interest_areas[areas].vCategory}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</em>
          {/section} 
      </div>
      </div>
      
      <div class="row3">
        <label>&nbsp;</label>
        <input type="button" value="Submit" name="Submit" class="button" onClick="check_edit_profile();">        
      </div>
    </div>
  </div>
  </form>
  {include file="member_left.tpl"} 
  <div style="clear:both;"></div>
</div>
{literal}<script>if('{/literal}{$organization[0].vState}{literal}' != '' || '{/literal}{$organization[0].vCountry}{literal}' != ''){get_county_list_profile('{/literal}{$organization[0].vCountry}{literal}', '{/literal}{$organization[0].vState}{literal}');}</script>{/literal}
{literal}<script>if('{/literal}{$organization[0].vCountry}{literal}' == '' ){get_county_list_profile('US');}</script>{/literal}