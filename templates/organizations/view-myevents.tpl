{if $var_msg neq ''} 
{literal}
  <script>
    shownotificationMessage('{/literal}{$var_msg}{literal}', 'success');
  </script>
{/literal}
{/if} 
<div class="about-page">
  <h2>My Account</h2>
  <div class="right-about-part">
    <h1>My Events <input type="button" value="Add New Event" name="Submit" class="button" onClick="add_event();" style="float:right;margin-bottom:10px;"></h1>
    <div class="history">
      {if $organization_events|@count gt 0}
      <table width="100%" cellspacing="1" cellpadding="3" border="0" class="tabborbg1">
        <tbody>
          <tr>
            <th width="30%" height="20">Event</th>
            <th width="15%">Date</th>
            <th width="20%">Student Attending</th>
            <th width="25%">Subscription Requests</th>
            <th width="10%">Action</th>
          </tr>
          
          {section name="myevents" loop=$organization_events}
          <tr class="oddbg">
            <td>{$organization_events[myevents].vTitle}</td>
            <td>
            {if $organization_events[myevents].eEventType eq 'Fixed'}
            {$generalobj->DateTime($organization_events[myevents].dStartDate,10)}
            {else}
            {$organization_events[myevents].eEventType}
            {/if}
            </td>
            <td align="center"><a href="{$tconfig.tsite_url}event-attending/{$organization_events[myevents].iEventId}">View ({$organization_events[myevents].tot_attending})</a></td>
            <td align="center"><a href="{$tconfig.tsite_url}subscription-requests/{$organization_events[myevents].iEventId}">View ({$organization_events[myevents].tot_pendings})</a></td>
            <td align="center">
            <a href="{$tconfig.tsite_url}edit-event/{$organization_events[myevents].iEventId}" title="Edit Event"><img src="{$tconfig.tsite_images}edit.png"></a> 
            {if $organization_events[myevents].eStatus neq 'Active'}
            <a href="javascript:void(0);" title="Show this event in event list" onClick="event_actions('Active', {$organization_events[myevents].iEventId});"><img src="{$tconfig.tsite_images}active.png"></a> 
            {else}
            <a href="javascript:void(0);" title="Hide this event from event list" onClick="event_actions('Inactive', {$organization_events[myevents].iEventId});"><img src="{$tconfig.tsite_images}inactive.png"></a> 
            {/if}
            <!--<a href="javascript:void(0);" title="Delete Event" onClick="event_actions('Delete', {$organization_events[myevents].iEventId});"><img src="{$tconfig.tsite_images}delete.png"></a>-->
            </td>
          </tr>
          {/section}        
        </tbody>
      </table>
      {else}
      <p style="text-align:center;font-size: 19px;margin-top: 15px;">No events found in your event list.</p>
      {/if}
      <div style="clear:both;"></div>
    </div>
  </div>
  {include file="member_left.tpl"}
  <div style="clear:both;"></div>
</div>
<form name="frmevent" id="frmevent" method="post">
<input type="hidden" name="action" id="action" value="">
<input type="hidden" name="iEventId" id="iEventId" value="">
</form>