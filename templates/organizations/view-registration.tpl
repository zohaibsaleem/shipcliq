{literal}<script>$(document).ready(function() { upload_reg_logo({/literal}{$iTempUserId}{literal});});</script>{/literal}
{if $var_msg neq ''}
  {literal} 
<script>
      shownotificationMessage('{/literal}{$var_msg}{literal}', 'error')
    </script> 
{/literal}
{/if}
<div class="about-page">
  <h2>Organization Registration</h2>
  <div class="or-reg">
    <form name="frmregister" id="frmregister" action="{$tconfig.tsite_url}index.php?file=o-registration_a" method="post">
      <input type="hidden" name="uplogo" id="uplogo" value="{$Data['uplogo']}">
      <div class="formpanel">
        <h4>Log In Details</h4>
        <label><em>E-mail :</em>
          <input type="text" style="width:310px; float:left;" class="validate[required,custom[email]] form-input" name="vEmail" id="vEmail" value="{$Data['vEmail']}">
        </label>
        <label><em>Password :</em>
          <input type="password" style="width:310px; float:left;" class="validate[required,minSize[6]] form-input" name="vPassword" id="vPassword1">
        </label>
        <label><em>Confirm password :</em>
          <input type="password" style="width:310px;float:left;" class="validate[required,equals[vPassword1]] form-input" name="vRepassword" id="vRepassword">
          <!-- validate[required,equals[vPassword]] --> 
        </label>
        <h5>Organization Details</h5>
        <label><em>Organization Name :</em>
          <input type="text" style="width:310px; float:left;" class="validate[required] form-input" name="vOrganizationName" id="vOrganizationName" value="{$Data['vOrganizationName']}">
        </label>
        <label><em>Contact Person First Name :</em>
          <input type="text" style="width:310px; float:left;" class="validate[required] form-input" name="vFirstName" id="vFirstName" value="{$Data['vFirstName']}">
        </label>
        <label><em>Contact Person Last Name :</em>
          <input type="text" style="width:310px; float:left;" class="validate[required] form-input" name="vLastName" id="vLastName" value="{$Data['vLastName']}">
        </label>
        <label>
        <em>Upload Image(Organization Logo) :</em>
        <div style="float:left;">
          <input type="file" name="vImage" id="vImage"/>
        </div>
        </label>
        <label id="logoimgdiv" style="display:none;"><em></em> <img id="logoimg" src="" style=""> </label>
        <label><em>Address :</em>
          <textarea style="width:310px; height:80px; float:left;" class="validate[required] form-input" name="vAddress" id="vAddress">{$Data['vAddress']}</textarea>
        </label>
        <label><em>City :</em>
          <input type="text" style="width:310px; float:left;" class="validate[required] form-input" name="vCity" id="vCity" value="{$Data['vCity']}">
        </label>
        <label><em>Country :</em>
          <select id="vCountry" name="vCountry" style="width:322px; float:left;" onChange="get_county_list(this.value);">
            <option value="">-- Please Select --</option>             
          {section name=i loop=$db_country}                 
            <option value="{$db_country[i].vCountryCode}" {if $Data[i].vCountry eq ''}{if $db_country[i].vCountryCode eq 'US'} selected {/if}{else}{if $db_country[i].vCountryCode eq $Data[0].vCountry} selected {/if}{/if}>{$db_country[i].vCountry}</option>            
          {/section}        
          </select>
        </label>
        <label><em>State :</em> <span id="county_list">
          <select style="width:322px; float:left;" id="vState" name="vState" class="validate[required]">
            <option value="">-- Please Select --</option>
          </select>
          </span> </label>
        <label><em>Phone :</em>
          <input type="text" style="width:310px; float:left;" class="validate[custom[phone]] form-input" name="vPhone" id="vPhone" value="{$Data['vPhone']}">
        </label>
        <label><em>Fax :</em>
          <input type="text" style="width:310px; float:left;" class="validate[custom[onlyLetterNumber]] form-input" name="vFax" id="vFax" value="{$Data['vFax']}">
        </label>
        <label><em>Website :</em>
          <input type="text" style="width:310px; float:left;" class="validate[custom[url]] form-input" name="vURL" id="vURL" value="{$Data['vURL']}" placeholder="http://">
        </label>
        <label><em>About :</em>
          <textarea style="width:310px; height:80px; float:left;" class="form-input" name="tDescription" id="tDescription">{$Data['tDescription']}</textarea>
        </label>
        
        <div class="chak-box2">
        <h2>Interest Areas :</h2>
         {section name="areas" loop=$interest_areas}
         <em> <input name="iCategoryId[]" id="iCategoryId" type="checkbox" value="{$interest_areas[areas].iCategoryId}" {if in_array($interest_areas[areas].iCategoryId, $Data['iCategoryId'])} checked {/if}/>
          &nbsp;{$interest_areas[areas].vCategory}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</em>
          {/section} </div>
        <label></label>
        <label>
        <div class="singlerow-login10"><a href="javascript:void(0);" onClick="check_orgreg();">Submit</a> <a href="javascript:void(0);" onClick="redirectcancel('{$tconfig.tsite_url}')">Cancel</a></div>
        </label>
        <div style="clear:both;"></div>
      </div>
    </form>
  </div>
  <div style="clear:both;"></div>
</div>
{if $Data["uplogo"] neq ''}{literal}<script>show_regimage('{/literal}{$Data["uplogo"]}{literal}', '{/literal}{$Data["imageurl"]}{literal}', '{/literal}{$Data["width"]}{literal}', '{/literal}{$Data["height"]}{literal}');</script>{/literal}{/if}
{literal}<script>if('{/literal}{$Data["vState"]}{literal}' != '' || '{/literal}{$Data["vCountry"]}{literal}' != ''){get_county_list('{/literal}{$Data["vCountry"]}{literal}', '{/literal}{$Data["vState"]}{literal}');}</script>{/literal}     
{literal}<script>if('{/literal}{$Data["vCountry"]}{literal}' == '' ){get_county_list('US');}</script>{/literal}