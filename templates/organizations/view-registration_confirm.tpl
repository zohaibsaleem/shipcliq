<div class="about-page">    
  <h2>Registration<span> Confirmation</span></h2>
  <div class="left-about-part">
    <div class="informaction">
      <h1>Information</h1>
      <ul>
        <li><a href="about.html" class="active">About Us</a></li>
        <li><a href="#">Terms &amp; Conditions</a></li>
        <li><a href="#">Privacy Policy</a></li>
        <li><a href="#">Site Map</a></li>
      </ul>
    </div>
    <div class="informaction">
      <h1>Help</h1>
      <ul>
        <li><a href="contact.html">Contact Us</a></li>
        <li><a href="faq.html">FAQ's</a></li>
      </ul>
    </div>
  </div>
  <div class="right-about-part">
    <h1>Registration<span> Confirmation</span></h1>     
      <p>You have successfully registered with us.</p> 
      <p>We have sent an email to your registered email account. Your account is not activated yet. Once it's approved by our admin we inform via mail.</p>
      <p>If you have not received the email please check your spam folder.</p>
  </div>
  <div style="clear:both;"></div>
</div>