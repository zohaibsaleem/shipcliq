{if $smarty.get.var_msg neq ''} 
{literal}
  <script>
    shownotificationMessage('{/literal}{$smarty.get.var_msg}{literal}', 'success');
  </script>
{/literal}
{/if} 
<div class="about-page">
  <h2>Subscription Requests</h2>
  <div class="right-about-part">
    <h1>Subscription Requests For {$event_details_subscription[0].vTitle} Event</h1>
     <div class="student-atteuding">
      {if $subscriptions|@count gt 0}
      <ul id="rrb">
        {section name="subscription" loop=$subscriptions}
        <li id="li_{$subscriptions[subscription].iEventVolunteerId}" {if $smarty.section.subscription.index%3 eq 2}class="last"{/if}>
          <div class="slid-in-top"><a href="#"> <img src="{$subscriptions[subscription].img_url}" alt="" /> </a> </div>
          <strong><a href="#">{$subscriptions[subscription].vFirstName}&nbsp;{$subscriptions[subscription].vLastName}</a></strong>
          <p class="lon"><img src="{$tconfig.tsite_images}con-icon.jpg" alt="" />&nbsp{$subscriptions[subscription].vCity}, {$subscriptions[subscription].vState}</p>
          <span>
          <a id="acc_{$subscriptions[subscription].iMemberId}" style="display:;" href="javascript:void(0);" onClick="accept_sub_req({$iEventId}, {$subscriptions[subscription].iMemberId}, {$subscriptions[subscription].iEventVolunteerId});">Accept</a>
          <img id="wt_{$subscriptions[subscription].iMemberId}" style="display:none;" src="{$tconfig.tsite_images}299.gif">
          <a id="decl_{$subscriptions[subscription].iMemberId}" href="javascript:void(0);" onClick="decline_sub_req({$iEventId}, {$subscriptions[subscription].iMemberId}, {$subscriptions[subscription].iEventVolunteerId});" class="del">Decline</a></span>
          <img id="wtdec_{$subscriptions[subscription].iMemberId}" style="display:none;" src="{$tconfig.tsite_images}299.gif">
        </li>
        {/section}
      </ul>
      {else}
        <p style="text-align:center;font-size: 19px;margin-top: 15px;">Subscription request not found for this event.</p>
      {/if}   
      <div class="back-but"><a href="{$tconfig.tsite_url}my-events">Back</a></div>
      </div>
  </div>
  {include file="member_left.tpl"}
  <div style="clear:both;"></div>
</div>