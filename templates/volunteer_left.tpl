<div class="left-about-part">
  <div class="orange-block">
    <h2>Volunteer Area</h2>
    <div align="center" class="welcome-txt">Welcome, <br>
      {if $sess_iMemberId neq ''}
      <span>{$sess_vFirstName}&nbsp;{$sess_vLastName}</span>
      {/if}
    </div>
  </div>
  <div class="informaction2">
    <h1>My Account</h1>       
    {if $sess_iMemberId neq ''}
    <ul>
      <li><a href="{$tconfig.tsite_url}volunteer-account">Edit Profile</a></li>
      <li><a href="{$tconfig.tsite_url}volunteer-profile">Your Public Profile</a></li>
      <li><a href="{$tconfig.tsite_url}volunteer-change-password">Change Password</a></li>
      <li><a href="{$tconfig.tsite_url}index.php?file=m-logout">Logout</a></li>
    </ul>
    {/if}
  </div>
  {if $sess_iMemberId neq ''}
  <div class="informaction2">
    <h1>Events</h1>
    <ul>
      <li><a href="{$tconfig.tsite_url}volunteer-event-subscriptions">My Events Subscription</a></li>
      <li><a href="{$tconfig.tsite_url}volunteer-points">My Points</a></li>         
    </ul>
  </div>
  {/if}
</div>