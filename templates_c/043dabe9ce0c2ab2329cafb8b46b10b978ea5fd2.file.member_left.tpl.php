<?php /* Smarty version Smarty-3.0.7, created on 2015-06-19 17:50:21
         compiled from "/home/www/xfetch/templates/member_left.tpl" */ ?>
<?php /*%%SmartyHeaderCode:152113765455840905858dd4-72094307%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '043dabe9ce0c2ab2329cafb8b46b10b978ea5fd2' => 
    array (
      0 => '/home/www/xfetch/templates/member_left.tpl',
      1 => 1395812648,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '152113765455840905858dd4-72094307',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="left-about-part">
  <div class="informaction">
    <h1><?php echo @LBL_PROFILE_INFO;?>
</h1>
    <ul>
      <li><a href="#" <?php if ($_smarty_tpl->getVariable('script')->value=='edit_profile'){?> class="active" <?php }?>><?php echo @LBL_PERSONAL_INFO;?>
</a></li>
      <li><a href="#" <?php if ($_smarty_tpl->getVariable('script')->value=='profile_photo'){?> class="active" <?php }?>><?php echo @LBL_PROFILE_PHOTO;?>
</a></li>
      <li><a href="#"><?php echo @LBL_SHARE_PREFERENCE;?>
</a></li>
      <li><a href="#"><?php echo @LBL_MEMBER_VERIFICATION;?>
</a></li>
      <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
" <?php if ($_smarty_tpl->getVariable('script')->value=='car_form'||$_smarty_tpl->getVariable('script')->value=='car_details'){?> class="active" <?php }?>><?php echo @LBL_CAR_DETAILS;?>
</a></li>
      <li><a href="#"><?php echo @LBL_POST_ADDRESS;?>
</a></li>
    </ul>
  </div>
  <div class="informaction">
    <h1><?php echo @LBL_ACCOUNT;?>
</h1>
    <ul>
      <li><a href="#"><?php echo @LBL_NOTIFICATION1;?>
</a></li>
      <li><a href="#"><?php echo @LBL_SOCIAL_SHARING;?>
</a></li>
      <li><a href="#"><?php echo @LBL_CHANGE_PASSWORD;?>
</a></li>
      <li><a href="#"><?php echo @LBL_DELET_MY_ACCOUNT;?>
</a></li>
    </ul>
  </div>
</div>
