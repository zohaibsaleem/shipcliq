<?php /* Smarty version Smarty-3.0.7, created on 2016-02-08 08:04:33
         compiled from "/home4/shipcliq/public_html/templates/members/view-member_alert.tpl" */ ?>
<?php /*%%SmartyHeaderCode:55344169056b8a071bb36a3-86828084%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '069e1517953446c8b06d0fe2998b7bc2ab9da533' => 
    array (
      0 => '/home4/shipcliq/public_html/templates/members/view-member_alert.tpl',
      1 => 1454938249,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '55344169056b8a071bb36a3-86828084',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_GET['var_msg']!=''){?>
<?php if ($_GET['msg_code']=='1'){?>
  
    <script>
      showNotification({type : 'success', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});
    </script>
  
<?php }?>
<?php }?>

<?php if ($_GET['var_msg']!=''){?>
<?php if ($_GET['msg_code']=='0'){?>
  
    <script>
      showNotification({type : 'error', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});
    </script>
  
<?php }?>
<?php }?>
<div class="body-inner-part">
  <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_EMAIL_ALERTS;?>
</span></div>
  <div class="main-inner-page">
    <h2><?php echo @LBL_EMAIL_ALERTS;?>
</h2>
    <div class="dashbord">
      <?php $_template = new Smarty_Internal_Template("member_top.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
      <div class="bro-message email-alerts1">
      <?php if (count($_smarty_tpl->getVariable('db_alert')->value)>0){?>
       <table border="0" cellpadding="5" cellspacing="0" style="width:100%;border:1px solid #CCCCCC; border-collapse: collapse;">
        <tr>
        <th style="border-bottom:1px solid #CCCCCC;border-right:1px solid #CCCCCC;">
            <?php echo @LBL_NO;?>

        </th>
        <th style="border-bottom:1px solid #CCCCCC;border-right:1px solid #CCCCCC;">
           <?php echo @LBL_FROM_LOCATION;?>

        </th>
        <th style="border-bottom:1px solid #CCCCCC;border-right:1px solid #CCCCCC;">
           <?php echo @LBL_TO_LOCATION;?>

        </th>
        <th style="border-bottom:1px solid #CCCCCC;border-right:1px solid #CCCCCC;">
           <?php echo @LBL_TRAVEL_DATE;?>

        </th>
        <th style="border-bottom:1px solid #CCCCCC;">
             <?php echo @LBL_ACTION;?>

        </th>
        </tr>

         <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_alert')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
           <tr>
            <td style="text-align:center;border-bottom:1px solid #CCCCCC;border-right:1px solid #CCCCCC;">
              <?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index']+1;?>

            </td>
            <td style="border-bottom:1px solid #CCCCCC;border-right:1px solid #CCCCCC;padding-left:7px;">
              <?php echo $_smarty_tpl->getVariable('db_alert')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vStartPoint'];?>

            </td>
            <td style="border-bottom:1px solid #CCCCCC;border-right:1px solid #CCCCCC; padding-left:7px;">
              <?php echo $_smarty_tpl->getVariable('db_alert')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vEndPoint'];?>

            </td>
            <td style="border-bottom:1px solid #CCCCCC;border-right:1px solid #CCCCCC; padding-left:7px;">
              <?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTimeFormat($_smarty_tpl->getVariable('db_alert')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dDate']);?>

            </td>
            <td style="text-align:center;border-bottom:1px solid #CCCCCC;">
              <a href="javascript:void(0);" onclick="deletAlert(<?php echo $_smarty_tpl->getVariable('db_alert')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iMemberAlertId'];?>
);"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tpanel_img'];?>
icons/icon_delete.png" title="Delete" /></a>
            </td>
           </tr>
         <?php endfor; endif; ?>
        <?php }else{ ?>
        <div class="main-block">
          <?php echo @LBL_NO_ALERT_CREATED;?>

          </div>
        <?php }?>
       </table>

      </div>
      <div style="clear:both;"></div>
    </div>
    <div style="clear:both;"></div>
  </div>
  <div style="clear:both;"></div>
</div>

<script>
function deletAlert(val){
 var res=confirm("<?php echo @LBL_SURE_DELETE;?>
 ?");
 if(res==false) return false;
 window.location="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=m-member_alert_a&iMemberAlertId="+val;
}
</script>

