<?php /* Smarty version Smarty-3.0.7, created on 2016-02-13 04:17:02
         compiled from "/home4/shipcliq/public_html/templates/member_profile_left.tpl" */ ?>
<?php /*%%SmartyHeaderCode:166224296956bf029ec40747-86946831%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '08900b8c7c6f72a4a8aca1bab47dd50ba601d789' => 
    array (
      0 => '/home4/shipcliq/public_html/templates/member_profile_left.tpl',
      1 => 1455358448,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '166224296956bf029ec40747-86946831',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="left-about-part">
  <div class="informaction">
    <h1><?php echo @LBL_PROFILE_INFO;?>
</h1>
    <ul>
      <li><a <?php if ($_smarty_tpl->getVariable('script')->value=='edit_profile'){?>class="active"<?php }?> href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
personal-information"><?php echo @LBL_PERSONAL_INFO;?>
</a></li>
      <li><a <?php if ($_smarty_tpl->getVariable('script')->value=='profile_photo'){?>class="active"<?php }?> href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
profile-photo"><?php echo @LBL_PROFILE_PHOTO;?>
</a></li>
      <!-- Hemali.. <li><a <?php if ($_smarty_tpl->getVariable('script')->value=='preferences'){?>class="active"<?php }?> href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
car-preferences"><?php echo @LBL_SHARE_PREFERENCE;?>
</a></li> -->
      <li><a <?php if ($_smarty_tpl->getVariable('script')->value=='verification'){?>class="active"<?php }?> href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
Member-Verification"><?php echo @LBL_MEMBER_VERIFICATION;?>
</a></li>
      <!-- Hemali .. <li><a <?php if ($_smarty_tpl->getVariable('script')->value=='car_details'){?>class="active"<?php }?> href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
car-details"><?php echo @LBL_CAR_DETAILS;?>
</a></li> -->
    </ul>
  </div>
  <div class="informaction">
    <h1><?php echo @LBL_ACCOUNT;?>
</h1>
    <ul>
      <li><a <?php if ($_smarty_tpl->getVariable('script')->value=='notification'){?>class="active"<?php }?> href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
Notification"><?php echo @LBL_NOTIFICATION1;?>
</a></li>
      <!--<li><a <?php if ($_smarty_tpl->getVariable('script')->value=='socialsharing'){?>class="active"<?php }?> href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
social-sharrings"><?php echo @LBL_SOCIAL_SHARING;?>
</a></li>-->
      <li><a <?php if ($_smarty_tpl->getVariable('script')->value=='changepassword'){?>class="active"<?php }?> href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
Change-Password" ><?php echo @LBL_CHANGE_PASSWORD;?>
</a></li>
      <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=m-delete_account" <?php if ($_smarty_tpl->getVariable('script')->value=='delete_account'){?> class="active" <?php }?>><?php echo @LBL_DELET_MY_ACCOUNT;?>
</a></li>
    </ul>
  </div>
</div>
