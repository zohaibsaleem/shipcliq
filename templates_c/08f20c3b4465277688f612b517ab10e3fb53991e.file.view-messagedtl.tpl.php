<?php /* Smarty version Smarty-3.0.7, created on 2015-06-26 15:35:10
         compiled from "/home/www/xfetch/templates/members/view-messagedtl.tpl" */ ?>
<?php /*%%SmartyHeaderCode:158468770558d23d61d9516-21509519%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '08f20c3b4465277688f612b517ab10e3fb53991e' => 
    array (
      0 => '/home/www/xfetch/templates/members/view-messagedtl.tpl',
      1 => 1395812663,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '158468770558d23d61d9516-21509519',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="body-inner-part">
  <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_MESSAGES;?>
</span></div>
  <div class="main-inner-page">
    <h2><?php echo @LBL_MESSAGES;?>
</h2>
    <div class="dashbord">
      <?php $_template = new Smarty_Internal_Template("member_top.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
      <div class="bro-message">
        <div class="tab-pane">
          <ul>
            <!--<li> <a href="#"> Questions &amp; Answers </a> </li> -->
            <li> <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsute_url'];?>
received-messages"> <?php echo @LBL_RECEIVED_MESSAGES;?>
 <em><?php echo $_smarty_tpl->getVariable('tot_unread')->value;?>
</em></a> </li>
            <li> <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsute_url'];?>
sent-messages"> <?php echo @LBL_SENT_MESSAGES;?>
 </a> </li>
            <li> <a class="last" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsute_url'];?>
archived-messages"> Archived messages</a> </li>
          </ul>
        </div>
        <form id="frmmessagesend" name="frmmessagesend" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=m-messagedtl_a" method="post" enctype="multipart/form-data">
        <input type="hidden" name="iThreadId" id="iThreadId" value="<?php echo $_smarty_tpl->getVariable('iThreadId')->value;?>
" />
        <input type="hidden" name="iFromMemberId" id="iFromMemberId" value="<?php echo $_smarty_tpl->getVariable('db_messages')->value[0]['iFromMemberId'];?>
" />
        <input type="hidden" name="iToMemberId" id="iToMemberId" value="<?php echo $_smarty_tpl->getVariable('db_messages')->value[0]['iToMemberId'];?>
" />
        <input type="hidden" name="from" id="from" value="<?php echo $_smarty_tpl->getVariable('from')->value;?>
" />
        <div class="aria">
          <textarea name="tMessage" id="tMessage" rows="2" class="validate[required] aria-text"></textarea>
          <a id="button_td" href="javascript:void(0);" onclick="validate_message_form();" style="display:;"><?php echo @LBL_POST_REPLY;?>
</a>
          <a id="wait_td" href="javascript:void(0);" style="display:none;"><?php echo @LBL_WAIT;?>
...</a>
          
          <a href="javascript:void(0);" onclick="redirectcancel();"><?php echo @LBL_CANCEL;?>
</a> 
        </div>
        </form>
        <div class="back1"><a href="javascript:void(0);" onclick="redirectcancel();"><?php echo @LBL_BACK;?>
</a></div>
        <div class="messages-details">
          <div class="message-top-file1"> <span><?php echo @LBL_SENDER;?>
</span><span><?php echo @LBL_MESSAGE;?>
</span> </div>
          <div id="message-file">
          <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['name'] = 'rec';
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_messages')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['total']);
?>
          <label id="<?php echo $_smarty_tpl->getVariable('db_messages')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rec']['index']]['iMessageId'];?>
">
          <span><img src="<?php echo $_smarty_tpl->getVariable('db_messages')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rec']['index']]['img'];?>
" alt="" style="width:70px;height:70px;"/><?php echo $_smarty_tpl->getVariable('db_messages')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rec']['index']]['vFirstName'];?>
&nbsp;<?php echo substr($_smarty_tpl->getVariable('db_messages')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rec']['index']]['vLastName'],0,1);?>
</span>
          <p><?php echo nl2br($_smarty_tpl->getVariable('db_messages')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rec']['index']]['tMessage']);?>
 <em><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_messages')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rec']['index']]['dAddedDate'],7);?>
</em></p>
          </label>
          <?php endfor; endif; ?>
          </div>           
        </div>
      </div>
      <div style="clear:both;"></div>
    </div>
    <div style="clear:both;"></div>
  </div>
  <div style="clear:both;"></div>
</div>

<script>  
  function redirectcancel()
  {
    window.location="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
received-messages";
    return false;
  }
  
  function validate_message_form(){
    jQuery("#frmmessagesend").validationEngine('init', {promptPosition : "topLeft", scroll: false});
		jQuery("#frmmessagesend").validationEngine('attach',{promptPosition : "topLeft", scroll: false});   	
    resp = jQuery("#frmmessagesend").validationEngine('validate');         
		if(resp == true){
        document.getElementById("button_td").style.display = 'none';
        document.getElementById("wait_td").style.display = '';
			  var request = $.ajax({  
        type: "POST",
        url:  site_url+'index.php?file=m-send_message_reply',  
        data: $("#frmmessagesend").serialize(), 	  
        
        success: function(data) { //alert(data); return false;           
          var msgdata = data.split("|");
          if(msgdata[0] == 1)
          {            
            $('#message-file').prepend(msgdata[2]);
            document.frmmessagesend.reset();
            document.getElementById("wait_td").style.display = 'none';
            document.getElementById("button_td").style.display = '';              
            setTimeout( function(){ 
              document.getElementById(msgdata[1]).style.background = '#ffffff'; 
            }
            , 3000 );
          }else{
           
          }                       			
        }
      });
      
      request.fail(function(jqXHR, textStatus) {
        alert( "Request failed: " + textStatus ); 
        document.getElementById("button_td").style.display = '';
        document.getElementById("wait_td").style.display = 'none';
      });
		}else{
			return false;
		}	
  }
  
  function prependElement(parentID,child)
  {
    parent=document.getElementById(parentID);
    parent.insertBefore(child,parent.childNodes[0]);
  }
  </script>
 