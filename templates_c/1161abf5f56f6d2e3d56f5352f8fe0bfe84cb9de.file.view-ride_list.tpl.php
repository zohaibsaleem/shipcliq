<?php /* Smarty version Smarty-3.0.7, created on 2015-01-13 16:13:17
         compiled from "/home/www/blablaclone/templates/content/view-ride_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:60804732554b4f6c53f1983-28034748%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1161abf5f56f6d2e3d56f5352f8fe0bfe84cb9de' => 
    array (
      0 => '/home/www/blablaclone/templates/content/view-ride_list.tpl',
      1 => 1407489173,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '60804732554b4f6c53f1983-28034748',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_counter')) include '/home/www/blablaclone/libraries/general/smarty/plugins/function.counter.php';
?><div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=182852035257135";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
rrb/zebra_datepicker.src.js"></script>
<link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
rrb/default.css" type="text/css"> 
<link href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
rbslider/css/redmond/jquery-ui-1.10.3.custom.css" rel="stylesheet">  
<script src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
rbslider/js/jquery-ui-1.10.3.custom.js"></script>
<div class="body-inner-part">
  <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_SR_RIDE;?>
 <?php echo @LBL_SEARCH_RESULTS;?>
</span></div>
  <div class="main-inner-page">
    <h2><?php echo @LBL_SR_RIDE;?>
<span><?php echo @LBL_SEARCH_RESULTS;?>
</span></h2>
    <div class="sharing-result">
      <form name="searchfrm" id="searchfrm" method="post" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_list">
      <input type="hidden" name="action" id="action" value="old">
      <input type="hidden" name="fromhr" id="fromhr" value="<?php echo $_smarty_tpl->getVariable('fromassign')->value;?>
">
      <input type="hidden" name="tohr" id="tohr" value="<?php echo $_smarty_tpl->getVariable('toassign')->value;?>
">
      <input type="hidden" name="schedule" id="schedule" value="<?php echo $_smarty_tpl->getVariable('ordscheduleby')->value;?>
">
      <input type="hidden" name="priceord" id="priceord" value="<?php echo $_smarty_tpl->getVariable('ordpriceby')->value;?>
">
      <input type="hidden" name="srtby" id="srtby" value="<?php echo $_smarty_tpl->getVariable('srtby')->value;?>
">
      <input type="hidden" name="comfort" id="comfort" value="<?php echo $_smarty_tpl->getVariable('comfort')->value;?>
">
      <input type="hidden" name="fltphoto" id="fltphoto" value="<?php echo $_smarty_tpl->getVariable('fltphoto')->value;?>
">
      <input type="hidden" name="pricetype" id="pricetype" value="<?php echo $_smarty_tpl->getVariable('pricetype')->value;?>
">
      <input type="hidden" name="fltmemrating" id="fltmemrating" value="<?php echo $_smarty_tpl->getVariable('fltmemrating')->value;?>
">
      
      <div class="sharing-result-left">
        <div class="trip-search-facets">
          <h2><?php echo @LBL_DATE;?>
</h2>
          <span>
          <input name="searchdate" id="searchdate" type="text" class="calander" value="<?php echo $_smarty_tpl->getVariable('searchdate')->value;?>
"/>
          </span>
          <span><?php echo @LBL_TIME;?>
: <em id="from"><?php echo $_smarty_tpl->getVariable('fromassign')->value;?>
h</em> - <em id="to"><?php echo $_smarty_tpl->getVariable('toassign')->value;?>
h</em></span>
          <span><div id="slider-range"></div></span>
          </div>           
        <div class="trip-comfort">
          <h2><?php echo @LBL_PRICE;?>
</h2>
          <span><input name="ePriceType" id="All" type="radio" value="All" class="radio1" onClick="fltbyprice('All');"/><strong>All</strong></span>
          <span><input name="ePriceType" id="Average" type="radio" value="Average" class="radio1" onClick="fltbyprice('Average');"/><strong>Average</strong></span>
          <span><input name="ePriceType" id="High" type="radio" value="High" class="radio1" onClick="fltbyprice('High');"/><strong>High</strong></span> 
          <span><input name="ePriceType" id="Low" type="radio" value="Low" class="radio1" onClick="fltbyprice('Low');"/><strong>Low</strong></span> 
        </div>
        <div class="trip-photo">
          <h2><?php echo @LBL_PHOTO;?>
</h2>
          <span><input name="fltimg" id="Allphoto" type="radio" value="Allphoto" class="radio1" onClick="fltbyphoto('Allphoto');"/><strong><?php echo @LBL_ALL;?>
</strong></span> 
          <span><input name="fltimg" id="Photo" type="radio" value="Photo" class="radio1" onClick="fltbyphoto('Photo');"/><strong><?php echo @LBL_PHOTO_ONLY;?>
</strong></span>           
        </div>
        <div class="trip-comfort">
          <h2><?php echo @LBL_CAR_CONFORT;?>
</h2>
          <span><input name="carcomfort" id="All" type="radio" value="All" class="radio1" onClick="fltbycarcomf('All');"/><strong><?php echo @LBL_ALL_TYPE;?>
</strong></span>
          <span><input name="carcomfort" id="Basic" type="radio" value="Basic" class="radio1" onClick="fltbycarcomf('Basic');"/><strong><?php echo @LBL_BASIC;?>
</strong></span> 
          <span><input name="carcomfort" id="Normal" type="radio" value="Normal" class="radio1" onClick="fltbycarcomf('Normal');"/><strong><?php echo @LBL_NORMAL;?>
</strong></span> 
          <span><input name="carcomfort" id="Comfortable" type="radio" value="Comfortable" class="radio1" onClick="fltbycarcomf('Comfortable');"/><strong><?php echo @LBL_COMFORTABLE;?>
</strong></span>
          <span><input name="carcomfort" id="Luxury" type="radio" value="Luxury" class="radio1" onClick="fltbycarcomf('Luxury');"/><strong><?php echo @LBL_LUXURY;?>
</strong></span> 
        </div>
        <div class="trip-comfort">
          <h2><?php echo @LBL_RATING;?>
</h2>
          <span><input name="memrating" id="memrating" type="radio" value="All" class="radio1" <?php if ($_smarty_tpl->getVariable('fltmemrating')->value=='All'){?>checked<?php }?> onClick="fltbymemrating('All');"/><strong><?php echo @LBL_ALL;?>
</strong></span>
          <span><input name="memrating" id="memrating1" type="radio" value="1" class="radio1"  <?php if ($_smarty_tpl->getVariable('fltmemrating')->value=='1'){?>checked<?php }?> onClick="fltbymemrating('1');"/><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/1star.png"></span>
          <span><input name="memrating" id="memrating2" type="radio" value="2" class="radio1"  <?php if ($_smarty_tpl->getVariable('fltmemrating')->value=='2'){?>checked<?php }?> onClick="fltbymemrating('2');"/><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/2star.png"></span>
          <span><input name="memrating" id="memrating3" type="radio" value="3" class="radio1"  <?php if ($_smarty_tpl->getVariable('fltmemrating')->value=='3'){?>checked<?php }?> onClick="fltbymemrating('3');"/><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/3star.png"></span>
          <span><input name="memrating" id="memrating4" type="radio" value="4" class="radio1"  <?php if ($_smarty_tpl->getVariable('fltmemrating')->value=='4'){?>checked<?php }?> onClick="fltbymemrating('4');"/><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/4star.png"></span>
          <span><input name="memrating" id="memrating5" type="radio" value="5" class="radio1"  <?php if ($_smarty_tpl->getVariable('fltmemrating')->value=='5'){?>checked<?php }?> onClick="fltbymemrating('5') ;"/><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/5star.png"></span>
        </div>
      </div>
      </form>
      <div class="sharing-result-right">
      <?php if ($_smarty_tpl->getVariable('search')->value=='place'){?>
      <div class="trip-alert-form-container">
      <h2><?php echo @LBL_LIST_RIDE_MESSAGE1;?>
</h2>
      <img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
email-envelope2.png" alt="" />
      <form name="frmalert" id="frmalert" method="post">
      <span>
      <em><?php echo @LBL_LIST_RIDE_MESSAGE2;?>
 <?php echo $_smarty_tpl->getVariable('From')->value;?>
 - <?php echo $_smarty_tpl->getVariable('To')->value;?>
 <?php echo @LBL_LIST_RIDE_MESSAGE3;?>
</em>
      <input type="hidden" name="iMemberId" id="iMemberId" value="<?php echo $_smarty_tpl->getVariable('sess_iMemberId')->value;?>
">
      <input type="hidden" name="vStartPoint" id="vStartPoint" value="<?php echo $_smarty_tpl->getVariable('From')->value;?>
">
      <input type="hidden" name="vEndPoint" id="vEndPoint" value="<?php echo $_smarty_tpl->getVariable('To')->value;?>
">
      <input type="hidden" name="vStartLatitude" id="vStartLatitude" value="<?php echo $_smarty_tpl->getVariable('from_lat')->value;?>
">
      <input type="hidden" name="vStartLongitude" id="vStartLongitude" value="<?php echo $_smarty_tpl->getVariable('from_long')->value;?>
">
      <input type="hidden" name="vEndLatitude" id="vEndLatitude" value="<?php echo $_smarty_tpl->getVariable('to_lat')->value;?>
">
      <input type="hidden" name="vEndLongitude" id="vEndLongitude" value="<?php echo $_smarty_tpl->getVariable('to_long')->value;?>
">
      <input name="dDate" id="dDate" type="text" class="validate[required] trip-input" value="" placeholder="<?php echo @LBL_SELECT_TRAVEL_DATE;?>
"/>
      <input name="vAlertEmail" id="vAlertEmail" type="text" class="validate[required,custom[email]] trip-input" value="<?php echo $_smarty_tpl->getVariable('sess_vEmail')->value;?>
"  placeholder="<?php echo @LBL_ENTER_YOUR_EMAIL1;?>
"/> 
      <a href="javascript:void(0);" onClick="create_mail_alert();"> <?php echo @LBL_CREATE_EMAIL_ALERT;?>
</a>
      </span>           
      </form>
      </div>
      <?php }?>
        <div class="trip-search-sorts"> <span><?php echo count($_smarty_tpl->getVariable('mainarr')->value);?>
 <?php echo @LBL_RIDES_AVILABLE;?>
</span>
          <div class="trip-search-sort-right"><em><?php echo @LBL_REARRANGE_BY;?>
</em>
           <a id="schanc" href="javascript:void(0);" onClick="arrbyschedule(0);"><?php echo @LBL_SCHEDULE;?>
&nbsp;&nbsp;<img id="schedimg" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
white-point-up.png" alt="" /></a>
           <a id="priceanc" href="javascript:void(0);" onClick="arrbyschedule(1);"><?php echo @LBL_PRICE;?>
&nbsp;&nbsp;<img id="priceimg" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
white-point.png" alt="" /></a>
        </div>
        </div>
        <input type="hidden" name="pagecount" id="pagecount" value="11">
        <span style="display:none;"><?php echo smarty_function_counter(array('start'=>1),$_smarty_tpl);?>
</span>
        <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['name'] = "rides";
$_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('mainarr')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["rides"]['total']);
?>                
        <div id="div_<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['rides']['index'];?>
" class="ridelist-ho" <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['rides']['index']>'10'){?> style="display:none;" <?php }else{ ?> style="display:;" <?php }?>>
        <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_details&id=<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['rides']['index'];?>
"><?php echo $_smarty_tpl->getVariable('mainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rides']['index']]['details'];?>
</a>
        </div>
        <!--<div style="float: right;position: relative;right: 208px;top: -54px;">
        <table width="100%">
          <tr>
          <td>
          Share on: 
          </td>
          <td>
          <div class="fb-share-button" data-href="http://www.balticcar.co/index.php?file=c-ride_details&from=home&id='.$mainsessionarr[$i]['iRideId'].'&dt='.$mainsessionarr[$i]['strtotime'].'&ret='.$mainsessionarr[$i]['return'].'" data-type="button"></div>
          </td>
          <td>
          <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://balticcar" data-lang="en-gb" data-count="none">Tweet</a>
          </td>
          </tr>
        </table>
        </div>-->
        <?php endfor; endif; ?>
        <?php if ($_smarty_tpl->getVariable('totrec')->value>'0'&&$_smarty_tpl->getVariable('totrec')->value>'10'){?>
        <a id="load_butt" href="javascript:void(0);" onClick="lod_more_data();" style="background: none repeat scroll 0 0 #000000;
    color: #FFFFFF;
    float: left;
    font-size: 14px;
    margin: 0;
    padding: 5px 10px;"><?php echo @LBL_LOAD_MORE;?>
</a>
        <?php }?> 
        <!--<div class="paging">
          <p>1 to 10 of 30 results</p>
          <span><a href="#">?</a> <a href="#" class="active">1</a> <a href="#">2</a> <a href="#">3</a> <a href="#">4</a> <a href="#">5</a> <a href="#">?</a></span>
        </div>-->
      </div>
      <div style=" clear:both;"></div>
    </div>
     <div style=" clear:both;"></div>
  </div><div style="clear:both;"></div>
</div>
<?php if ($_smarty_tpl->getVariable('comfort')->value!=''){?>

<script>
$('#<?php echo $_smarty_tpl->getVariable('comfort')->value;?>
').attr("checked", "checked");
</script> 
 
<?php }?>
<?php if ($_smarty_tpl->getVariable('pricetype')->value!=''){?>

<script>
$('#<?php echo $_smarty_tpl->getVariable('pricetype')->value;?>
').attr("checked", "checked");
</script> 
 
<?php }?>

<?php if ($_smarty_tpl->getVariable('fltphoto')->value!=''){?>

<script>
$('#<?php echo $_smarty_tpl->getVariable('fltphoto')->value;?>
').attr("checked", "checked");
</script> 
 
<?php }?>  
<?php if ($_smarty_tpl->getVariable('srtby')->value==0){?>

<script>
//$("#schanc").css("background-color","#006197");
</script>

<?php }else{ ?>

<script>
$("#priceanc").css("background-color","#006197");
</script>

<?php }?>
<?php if ($_smarty_tpl->getVariable('ordscheduleby')->value=='ASC'){?>

<script>
$("#schedimg").attr("src","<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
white-point-up.png");
</script>

<?php }else{ ?>

<script>
$("#schedimg").attr("src","<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
white-point.png");
</script>

<?php }?>
<?php if ($_smarty_tpl->getVariable('ordpriceby')->value=='ASC'){?>

<script>
$("#priceimg").attr("src","<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
white-point-up.png");
</script>

<?php }else{ ?>

<script>
$("#priceimg").attr("src","<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
white-point.png");
</script>

<?php }?>

  <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
  <script>
    var totrec = <?php echo $_smarty_tpl->getVariable('totrec')->value;?>
;
    function lod_more_data(){
      var pagecount = $("#pagecount").val(); 
      $("#pagecount").val(Number(pagecount)+ Number(10));
      var incre = $("#pagecount").val();
      if(totrec <= incre){
        $("#load_butt").hide();
      } 
      for (var i=pagecount;i<incre;i++)
      { 
        $("#div_"+i).show(); 
      }       
    }
    
    function arrbyschedule(val){
      $("#srtby").val(val);
      if(val == 0){
        var schedule = $("#schedule").val();
        if(schedule == 'ASC'){
          $("#schedule").val('DESC');          
          $("#schedimg").attr("src","<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
white-point-up.png");
          document.searchfrm.submit();
        }else{
          $("#schedule").val('ASC');
          $("#schedimg").attr("src","<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
white-point.png");
          document.searchfrm.submit();
        }
      }else{
        var priceord = $("#priceord").val();
        if(priceord == 'ASC'){
          $("#priceord").val('DESC');          
          $("#priceimg").attr("src","<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
white-point-up.png");
          document.searchfrm.submit();
        }else{
          $("#priceord").val('ASC');
          $("#priceimg").attr("src","<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
white-point.png");
          document.searchfrm.submit();
        }
      }       
    }
    
    function fltbycarcomf(type){
      $("#comfort").val(type);
      document.searchfrm.submit();
    }
    
    function fltbyprice(type){
      $("#pricetype").val(type);
      document.searchfrm.submit();
    }
    
    function fltbyphoto(type){
      $("#fltphoto").val(type);
      document.searchfrm.submit();
    }
  
    function fltbymemrating(type){
      $("#fltmemrating").val(type);
      document.searchfrm.submit();
    }
    
    $('#searchdate').Zebra_DatePicker({
      direction: 1,
      onSelect: function() {
         document.searchfrm.submit();
      },
      onClear: function() {
         document.searchfrm.submit();
      }
      
    });
    
    $('#dDate').Zebra_DatePicker({
      direction: 1
    });
    
    function ass_hrs(from, to){ 
      $("#fromhr").val(from);
      $("#tohr").val(to); 
      
      document.searchfrm.submit();       
    }
    
    $( "#slider-range" ).slider({
      range: true,
      min: 1,
      max: 24,
      values: [ <?php echo $_smarty_tpl->getVariable('fromassign')->value;?>
, <?php echo $_smarty_tpl->getVariable('toassign')->value;?>
 ], 
      animate: "slow",
      step: 1,
      change: function(event, ui) { 
        if(ui.values[1] - ui.values[0] < 1 ){
          return false;  
        }
        $("#from").html(ui.values[0]+'h');
        $("#to").html(ui.values[1]+'h');
        $("#fromhr").val(ui.values[0]);
        $("#tohr").val(ui.values[1]);
        
        ass_hrs(ui.values[0], ui.values[1]); 
      }  
    });
    
    var	site_url = '<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
';
    
    function create_mail_alert(){
      resp = jQuery("#frmalert").validationEngine('validate');        
  		if(resp == true){
  			var request = $.ajax({  
      	  type: "POST",
      	  url: site_url+'index.php?file=c-emailalert',  
      	  data: $("#frmalert").serialize(), 	  
      	  
      	  success: function(data) {
      		  if(data == 1){
             showNotification({type : 'success', message: '<?php echo @LBL_YOUR_EMAIL_ALERT_SAVED;?>
'});  
            }else if(data == 2){
             showNotification({type : 'warning', message: '<?php echo @LBL_EMAIL_ALERT_ALREADY;?>
'}); 
            }else{
             showNotification({type : 'error', message: '<?php echo @LBL_SOME_ERRO_TRY_AGAIN;?>
'});  
            }        
            $('#frmalert')[0].reset();
            return false;   			
      		}
      	});
      	
      	request.fail(function(jqXHR, textStatus) {
      	  alert( "Request failed: " + textStatus ); 
      	});   
  		}else{
  			return false;
  		}
    }
  </script>
