<?php /* Smarty version Smarty-3.0.7, created on 2016-01-28 00:29:15
         compiled from "/home4/shipcliq/public_html/templates/member_top.tpl" */ ?>
<?php /*%%SmartyHeaderCode:185905565856a9b53bea39c2-41112759%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '11a132be3b1725e3ef548867620d8887488692b9' => 
    array (
      0 => '/home4/shipcliq/public_html/templates/member_top.tpl',
      1 => 1453962553,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '185905565856a9b53bea39c2-41112759',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<script>
	$(document).ready(function(){
		$('.tabs-main ul').hide();
		$(".open-s").click(function(){
			
			$(".tabs-main-list").toggle('fast');
		});
	});
</script>

<div class="inner-submenu">
	<div class="tabs-main">
		<a href="javascript:void(0);" class="open-s"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
inner-menu-icon.png" alt="" /></a>
		<ul class="tabs-main-list">
			<li><a <?php if ($_smarty_tpl->getVariable('script')->value=='dashboard'){?>class="active"<?php }?> href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
my-account"><?php echo @LBL_DASHBOARD;?>
</a></li>
			<?php if (@PAYMENT_OPTION!='Contact'){?>
			<li><a <?php if ($_smarty_tpl->getVariable('script')->value=='mybooking'){?>class="active"<?php }?> href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
my-bookings"><?php echo @LBL_MY_BOOKINGS;?>
</a></li>
			<?php }?>
			<li><a <?php if ($_smarty_tpl->getVariable('script')->value=='list_rides_offer'){?>class="active"<?php }?> href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
List-Rides-Offer"><?php echo @LBL_RIDES_OFFERED1;?>
</a></li>
			<li><a <?php if ($_smarty_tpl->getVariable('script')->value=='received_messages'||$_smarty_tpl->getVariable('script')->value=='sent_messages'||$_smarty_tpl->getVariable('script')->value=='messagedtl'||$_smarty_tpl->getVariable('script')->value=='archived_messages'){?>class="active"<?php }?> href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
received-messages"><?php echo @LBL_MESSAGES;?>
</a></li>
			<li><a <?php if ($_smarty_tpl->getVariable('script')->value=='member_alert'){?>class="active"<?php }?> href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
Email-Alerts"><?php echo @LBL_EMAIL_ALERTS;?>
</a></li>
			<li><a <?php if ($_smarty_tpl->getVariable('script')->value=='leaverating'||$_smarty_tpl->getVariable('script')->value=='memberrating'||$_smarty_tpl->getVariable('script')->value=='receiverating'||$_smarty_tpl->getVariable('script')->value=='ratinggiven'){?>class="active"<?php }?> href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
leave-Rating"><?php echo @LBL_RATINGS;?>
</a></li>
			<li><a <?php if ($_smarty_tpl->getVariable('script')->value=='edit_profile'||$_smarty_tpl->getVariable('script')->value=='verification'||$_smarty_tpl->getVariable('script')->value=='changepassword'||$_smarty_tpl->getVariable('script')->value=='notification'){?>class="active"<?php }?> href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
personal-information"><?php echo @LBL_PROFILE;?>
</a></li>
			<!--
				<li><a <?php if ($_smarty_tpl->getVariable('script')->value=='money_detail'){?> class="active"<?php }?> href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=m-money_detail&type=available"><?php echo @LBL_DRIVER;?>
</a></li>
			<li><a <?php if ($_smarty_tpl->getVariable('script')->value=='payment_detail'){?> class="active"<?php }?> class="active" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=m-payment_detail&type=avaliable"><?php echo @LBL_PASSENGER;?>
</a></li>-->
			<li><a <?php if ($_smarty_tpl->getVariable('script')->value=='money_detail'||$_smarty_tpl->getVariable('script')->value=='payment_detail'){?> class="active"<?php }?> href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=m-money_detail&pagetype=money&type=available"><?php echo @LBL_PAYMENT;?>
</a></li>	
		</ul>
		<div style="clear:both;"></div>
	</div>
</div>