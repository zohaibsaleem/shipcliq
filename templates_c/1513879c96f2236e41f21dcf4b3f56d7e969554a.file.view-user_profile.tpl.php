<?php /* Smarty version Smarty-3.0.7, created on 2015-01-21 11:09:09
         compiled from "/home/www/blablaclone/templates/content/view-user_profile.tpl" */ ?>
<?php /*%%SmartyHeaderCode:53793482854bf3b7d6db854-46489210%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1513879c96f2236e41f21dcf4b3f56d7e969554a' => 
    array (
      0 => '/home/www/blablaclone/templates/content/view-user_profile.tpl',
      1 => 1418820090,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '53793482854bf3b7d6db854-46489210',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
jquery.raty.js"></script>
<div class="body-inner-part">
      <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_USR_PROFILE;?>
</span></div>
<div class="main-inner-page">
        <h2><?php echo @LBL_USR_PROFILE;?>
</h2>
        <div class="dashbord">
          <div class="dashbord-left-part">
            <div class="dashbord-left-in">
              <h1><?php echo @LBL_MY_VERIFICATION;?>
</h1>
              <ul>
                <?php if ($_smarty_tpl->getVariable('PHONE_VERIFICATION_REQUIRED')->value=='Yes'){?>
                <li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
phone-right-i.png" alt="" />&nbsp;&nbsp;<?php echo @LBL_PHONE_VERIFICATION;?>
 <em><?php if ($_smarty_tpl->getVariable('db_email_verification')->value[0]['ePhoneVerified']=='No'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/verification-no-check.png" alt="" /><?php }else{ ?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/verification-checked.png" alt="" /><?php }?></em></li>
                <?php }?>
                <li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
email-right-i.png" alt="" />&nbsp;&nbsp;<?php echo @LBL_EMAIL_VARIFICATION;?>
 <em><?php if ($_smarty_tpl->getVariable('db_email_verification')->value[0]['eEmailVarified']=='No'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/verification-no-check.png" alt="" /><?php }else{ ?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/verification-checked.png" alt="" /><?php }?></em></li>
                <li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
fb-right-i.png" alt="" /><?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vFbFriendCount'];?>
 <?php echo @LBL_FRIENDS;?>
<em><?php if ($_smarty_tpl->getVariable('db_member')->value[0]['vFbFriendCount']>0){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/verification-checked.png" alt="" /><?php }else{ ?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/verification-no-check.png" alt="" /><?php }?></em> </li>
              </ul>
            </div>
            <div class="dashbord-left-in">
              <h1><?php echo @LBL_MEMBER_ACTIVITY;?>
</h1>
              <ul>
                <li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
rides-right-i.png" alt="" />&nbsp;&nbsp;<?php echo $_smarty_tpl->getVariable('tot_rides')->value;?>
 <?php echo @LBL_RIDES_OFFERED;?>
</li>
                <li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
member-right-i.png" alt="" />&nbsp;&nbsp;<?php echo @LBL_MEMBER_SINCE;?>
 : <?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTimeFormat($_smarty_tpl->getVariable('db_email_verification')->value[0]['dAddedDate']);?>
 </li>
                  <!--<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
Member-Varification" class="active1"><?php echo @LBL_COMPLETE_MEM_VERIFICATION;?>
</a></li>-->
              </ul>
            </div>
            <div class="dashbord-left-in">
              <h1><?php echo @LBL_MY_CAR;?>
</h1>
              <?php if (count($_smarty_tpl->getVariable('db_car')->value)>0){?>
              <ul>
                <li>
                  <?php if ($_smarty_tpl->getVariable('db_car')->value[0]['img']!=''){?><img src="<?php echo $_smarty_tpl->getVariable('db_car')->value[0]['img'];?>
" alt="" /><?php }?>
                  <h3><?php echo $_smarty_tpl->getVariable('db_car')->value[0]['vMake'];?>
,&nbsp;&nbsp;<?php echo $_smarty_tpl->getVariable('db_car')->value[0]['vTitle'];?>
</h3>
                  <p><?php echo @LBL_COLOR;?>
: <?php echo $_smarty_tpl->getVariable('db_car')->value[0]['vColour'];?>
</p>
                  <p><?php echo @LBL_COMFORT;?>
: <?php echo $_smarty_tpl->getVariable('db_car')->value[0]['eComfort'];?>
</p>
                </li>
                <?php if ($_smarty_tpl->getVariable('same_member')->value==1){?>
                  <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
car-details" class="active1"><?php echo @LBL_EDIT_YOUR_CAR;?>
</a></li>
                <?php }?>
              </ul>
              <?php }else{ ?>
              <ul><li><?php echo @LBL_NO_CAR;?>
</li></ul>
              <?php }?>
            </div>
            <div style="clear:both;"></div>
          </div>
          <div class="dashbord-right-part">
            <div class="user-profile-page">
              <div class="main-infos">
                <div class="member-picture"> <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['img']!=''){?><img src="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['img'];?>
" alt="" /><?php }?> </div>
                <div class="main-infos-list">
                  <ul>
                    <li>
                      <h1><?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vLastName'];?>
 
                       <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['iBirthYear']!='0'){?><span class="user-age">(<?php echo $_smarty_tpl->getVariable('member_age')->value;?>
 <?php echo @LBL_YEARS_OLD;?>
)</span><?php }?>
                       <?php if ($_smarty_tpl->getVariable('same_member')->value==1){?>
                        <div style="float:right"><a class="link-dashbord" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
personal-information"><?php echo @LBL_EDIT_PROF;?>
</a></div>
                       <?php }?>
                      </h1>
                    </li>
                 <!--   <li><em>Average rating:</em><span class="big-star-rating"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star.jpg" alt="" /></span></li> -->
                    <li><?php echo @LBL_EXPERIENCE1;?>
: Intermediate</li>
                    <?php if (count($_smarty_tpl->getVariable('db_pref')->value)>0){?>
                    <li><?php echo @LBL_MY_PREFERENCE1;?>
</li>
                    <li><em class="i-am"><?php echo @LBL_I_AM;?>
 &nbsp;&nbsp;</em><span class="big-star-rating"><?php echo $_smarty_tpl->getVariable('pref')->value;?>

                        <!--<img src="<?php echo $_smarty_tpl->getVariable('eChattiness')->value;?>
" alt="title1" <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eChattiness']=='YES'){?>title="<?php echo @LBL_I_LOVE_CHAT;?>
"<?php }elseif($_smarty_tpl->getVariable('db_pref')->value[0]['eChattiness']=='MAYBE'){?>title="<?php echo @LBL_TALK_MOOD;?>
"<?php }else{ ?>title="<?php echo @LBL_QUIET_TYPE;?>
 :)"<?php }?>/>&nbsp;&nbsp;
                        <img src="<?php echo $_smarty_tpl->getVariable('eMusic')->value;?>
" alt="" <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eMusic']=='YES'){?>title="<?php echo @LBL_ABOUT_PLAYLIST;?>
" <?php }elseif($_smarty_tpl->getVariable('db_pref')->value[0]['eMusic']=='MAYBE'){?>title="<?php echo @LBL_DEPEND_MOOD;?>
" <?php }else{ ?>title="<?php echo @LBL_SILENCE_GOLDEN;?>
" <?php }?> />&nbsp;&nbsp;
                        <img src="<?php echo $_smarty_tpl->getVariable('eSmoking')->value;?>
" alt="" <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eSmoking']=='YES'){?>title="<?php echo @LBL_CIGARETTE_SMOKE;?>
" <?php }elseif($_smarty_tpl->getVariable('db_pref')->value[0]['eSmoking']=='MAYBE'){?>title="<?php echo @LBL_DEPEND_MOOD;?>
" <?php }else{ ?>title="<?php echo @LBL_NO_SMOKE;?>
" <?php }?> />&nbsp;&nbsp;
                        <img src="<?php echo $_smarty_tpl->getVariable('eEcig')->value;?>
" alt="" <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eEcig']=='YES'){?>title="<?php echo @LBL_CIGARETTE_SMOKE;?>
" <?php }elseif($_smarty_tpl->getVariable('db_pref')->value[0]['eEcig']=='MAYBE'){?>title="<?php echo @LBL_DEPEND_MOOD;?>
" <?php }else{ ?>title="<?php echo @LBL_NO_SMOKE;?>
" <?php }?> />&nbsp;&nbsp;
                        <img src="<?php echo $_smarty_tpl->getVariable('ePets')->value;?>
" alt="" <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['ePets']=='YES'){?>title="<?php echo @LBL_PETS1;?>
" <?php }elseif($_smarty_tpl->getVariable('db_pref')->value[0]['ePets']=='MAYBE'){?>title="<?php echo @LBL_DEPEND_MOOD;?>
" <?php }else{ ?>title="<?php echo @LBL_PETS2;?>
" <?php }?> />-->
                        </span>
                        </li>
                    <?php }?>
                    <?php if ($_smarty_tpl->getVariable('sess_iMemberId')->value!=''&&$_smarty_tpl->getVariable('sess_iMemberId')->value!=$_smarty_tpl->getVariable('iMemberId')->value){?>
                    <li><a id="showmsg" class="link-dashbord" href="javascript:void(0);"><?php echo @LBL_SEND_MSG_TO;?>
 <?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vFirstName'];?>
</a></li>
                    <?php }?>
                  </ul>
                </div>
              </div>
              <div id="messagefrmdiv" class="big-prefs" style="display:none;">
                <form name="frmmessage" id="frmmessage" method="post">
                <input type="Hidden" name="mode" id="mode" value="message">
                <input type="Hidden" name="iFromMemberId" id="iFromMemberId" value="<?php echo $_smarty_tpl->getVariable('sess_iMemberId')->value;?>
">
                <input type="Hidden" name="iToMemberId" id="iToMemberId" value="<?php echo $_smarty_tpl->getVariable('iMemberId')->value;?>
">
                <input type="Hidden" name="type" id="type" value="simple">  
                <p>
                <textarea name="tMessage" id="tMessage" class="validate[required] profile-textarea" style="width:98%;"></textarea>
                <div style="float:left;margin: 0 0 0 10px;"><a id="sendbutton" class="link-dashbord" href="javascript:void(0);" onClick="sendmessage_valid();"><?php echo @LBL_SEND;?>
</a></div>
                <div style="float:left;margin: 0 0 0 10px;"><a id="hidemsg" class="link-dashbord" href="javascript:void(0);"><?php echo @LBL_CANCEL;?>
</a></div>
                </p>
                </form>
              </div>
              <div class="big-prefs">
                <p><?php if ($_smarty_tpl->getVariable('db_member')->value[0]['tDescription']!=''){?><?php echo nl2br($_smarty_tpl->getVariable('db_member')->value[0]['tDescription']);?>
<?php }else{ ?><?php echo @LBL_HAVENT_DESC;?>
<?php }?> </p>
              </div>
              <div class="rating-received">
                <div class="rating-received-left-part">
                  <span><?php echo $_smarty_tpl->getVariable('rating')->value;?>
 <?php echo @LBL_RATING;?>
<!-- rating - 3 / 5 --> 
                    <!--<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
blue-star.png" alt="" />-->
                    <!--<div id="star" class="star-img"></div>-->
                    <span style="float:right;margin-left: 10px;margin-top: 3px;display: block; width: 65px; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 0;">
                        <span style="display: block; width: <?php echo $_smarty_tpl->getVariable('rating_width')->value;?>
%; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 -13px;"></span>
                    </span>
                   
                 </span> 
                </div>
                <div class="rating-received-right-part">
                   <div class="rating-received-right-part-inner"> 
                    <span>5 <?php echo @LBL_STARS;?>
</span>
                    <div class="progress"><img style="width:<?php echo $_smarty_tpl->getVariable('totalwidth')->value[5];?>
%; padding: 0px 0 7px;max-width: 100%;" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/progress.jpg"></div>
                    <span class="last-star"><?php echo $_smarty_tpl->getVariable('totrating')->value[5];?>
</span> 
                  </div>
                  <div class="rating-received-right-part-inner"> 
                    <span>4 <?php echo @LBL_STARS;?>
</span>
                    <div class="progress"><img style="width:<?php echo $_smarty_tpl->getVariable('totalwidth')->value[4];?>
%; padding: 0px 0 7px;max-width: 100%;" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/progress.jpg"></div>
                    <span class="last-star"><?php echo $_smarty_tpl->getVariable('totrating')->value[4];?>
</span> 
                  </div>
                  <div class="rating-received-right-part-inner"> 
                    <span>3 <?php echo @LBL_STARS;?>
</span>
                    <div class="progress"><img style="width:<?php echo $_smarty_tpl->getVariable('totalwidth')->value[3];?>
%; padding: 0px 0 7px;max-width: 100%;" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/progress.jpg"></div>
                    <span class="last-star"><?php echo $_smarty_tpl->getVariable('totrating')->value[3];?>
</span>
                  </div>
                  <div class="rating-received-right-part-inner">
                    <span>2 <?php echo @LBL_STARS;?>
</span>
                    <div class="progress"><img style="width:<?php echo $_smarty_tpl->getVariable('totalwidth')->value[2];?>
%; padding: 0px 0 7px;max-width: 100%;" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/progress.jpg"></div>
                    <span class="last-star"><?php echo $_smarty_tpl->getVariable('totrating')->value[2];?>
</span>
                  </div>
                  <div class="rating-received-right-part-inner">
                    <span>1 <?php echo @LBL_STARS;?>
</span>
                    <div class="progress"><img style="width:<?php echo $_smarty_tpl->getVariable('totalwidth')->value[1];?>
%; padding: 0px 0 7px;max-width: 100%;" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/progress.jpg"></div>
                    <span class="last-star"><?php echo $_smarty_tpl->getVariable('totrating')->value[1];?>
</span>
                  </div>
                </div>
              </div>
              <div class="user-comment-list">
                <?php if (count($_smarty_tpl->getVariable('db_rating_from')->value)>0){?>
                <ul>
                  <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_rating_from')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
                  <li>
                    <div class="user-img2"><img src="<?php echo $_smarty_tpl->getVariable('db_rating_from')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['img'];?>
" alt="" /></div>
                    <div class="user-profile-de">
                   
                      <h2><div style="float:left;">Rating : <?php echo $_smarty_tpl->getVariable('db_rating_from')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iRate'];?>
</div>
                      <div style="margin:0px; float:left;">  <span style="float:left; margin-left: 10px;margin-top:5px;display: block; width: 65px; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 0;">
                        <span style="display: block; float:none; width: <?php echo $_smarty_tpl->getVariable('db_rating_from')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['rating_width'];?>
%; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 -13px;"></span>
                    </span></div>
                    
                    <div style="color:#414141; float:right; font-family:'droid_sansregular'; font-size:13px;"><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTimeFormat($_smarty_tpl->getVariable('db_rating_from')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dAddedDate']);?>
 </div>
                      
                      </h2>
                      <p><strong>From <?php echo $_smarty_tpl->getVariable('db_rating_from')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_rating_from')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vLastName'];?>
 :</strong> <?php echo $_smarty_tpl->getVariable('db_rating_from')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tFeedback'];?>
. </p>
                    </div>
                  </li>
                  <?php endfor; endif; ?>
                </ul>
                <?php }?>  
              </div>
            </div>
          </div>
          <div style="clear:both;"></div>
        </div>
        <div style="clear:both;"></div>
      </div>
      <div style="clear:both;"></div>
    </div>

  <script>
    var	site_url = '<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
';
    $("#showmsg").click(function() {
      $("#messagefrmdiv").toggle('slow');
    });
    
    $("#hidemsg").click(function() {
      $("#messagefrmdiv").toggle('slow');
    });
    
    
    
    function sendmessage_valid(){     
      resp = jQuery("#frmmessage").validationEngine('validate');
        
    		if(resp == true){ 
    		  $("#sendbutton").html('Wait...');      
    		  var tMessage = document.getElementById("tMessage").value;      
          var vSubject = '';
          var iToMemberId = document.getElementById("iToMemberId").value;
          var iFromMemberId = document.getElementById("iFromMemberId").value;
          var action = document.getElementById("mode").value;
          var type = document.getElementById("type").value;
              
         
    			var request = $.ajax({
      	  type: "POST",
      	  url: site_url+'index.php?file=c-messages_a',   	  
      	  data: "tMessage="+escape(tMessage)+"&vSubject="+escape(vSubject)+"&iToMemberId="+iToMemberId+"&iFromMemberId="+iFromMemberId+"&action="+action+"&type="+type, 	  
      	  
      	  success: function(data) { 
            $("#sendbutton").html('Send');
            $("#tMessage").val('');
      			if(data == 1)
      			{
              showNotification({type : 'success', message: '<?php echo @LBL_YOUR_MSG_SENT_SUCC;?>
'});
              $("#messagefrmdiv").toggle('slow');          
            }else{
              showNotification({type : 'error', message: '<?php echo @LBL_SOME_WRONG_TRY_AGAIN;?>
'});
              $("#messagefrmdiv").toggle('slow');  
            }
                            
      		}
      	});
      	
      	request.fail(function(jqXHR, textStatus) {
      	  $("#sendbutton").html('Send');
      	  $("#tMessage").val('');
          alert( "Request failed: " + textStatus ); 
      	});
    		
    		}else{
    			return false;
    		}	   
      	
    }
  </script>

<?php if ($_smarty_tpl->getVariable('megop')->value==1){?>

  <script>
        $("#messagefrmdiv").toggle('slow');
  </script>

<?php }?>