<?php /* Smarty version Smarty-3.0.7, created on 2015-02-09 13:49:37
         compiled from "/home/www/blablaclone/templates/members/view-booking_confirm.tpl" */ ?>
<?php /*%%SmartyHeaderCode:107787856154d86d9963d304-01035842%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1b257896efbceed41aea01d8e35182669a6f5d83' => 
    array (
      0 => '/home/www/blablaclone/templates/members/view-booking_confirm.tpl',
      1 => 1396605295,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '107787856154d86d9963d304-01035842',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="body-inner-part">
  <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_RIDE_BOOK_CONFIRM;?>
</span></div>
  <div class="main-inner-page">
    <h2><?php echo @LBL_RIDE_BOOK_CONFIRM;?>
</h2>
    <?php $_template = new Smarty_Internal_Template("left.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
    <?php if ($_smarty_tpl->getVariable('cd')->value==1){?>
    <div class="right-inner-part">
      <h2><?php echo @LBL_RIDE_BOOK_SUCC_CONF;?>
</h2>
      <p><?php echo @LBL_BOOKING_CONF_SUCC;?>
</p>
      <p><?php echo $_smarty_tpl->getVariable('mailcont')->value;?>
</p>
    </div>
    <?php }else{ ?>
    <div class="right-inner-part">
      <h2><?php echo @LBL_RIDE_BOOK_FAILED;?>
</h2>
      <p><?php echo @LBL_PAYMENT_BOOK_FAILED;?>
</p>
      <p><?php echo @LBL_TRY_AGAIN;?>
</p>
    </div>
    <?php }?>
  </div>
  <div style="clear:both;"></div>
</div>