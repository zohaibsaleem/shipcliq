<?php /* Smarty version Smarty-3.0.7, created on 2016-02-10 07:12:33
         compiled from "/home4/shipcliq/public_html/templates/members/view-booking_cancelation_form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:88753673356bb3741432471-85399264%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '22d0e9ebea064baa00192e28a450d7322ce98898' => 
    array (
      0 => '/home4/shipcliq/public_html/templates/members/view-booking_cancelation_form.tpl',
      1 => 1455109910,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '88753673356bb3741432471-85399264',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_smarty_tpl->getVariable('err_msg')->value!=''){?>
  
    <script>
      showNotification({type : 'error', message: '<?php echo $_smarty_tpl->getVariable('err_msg')->value;?>
'});
    </script>
  
<?php }?>
<?php if ($_smarty_tpl->getVariable('var_msg')->value!=''){?>
  
    <script>
      showNotification({type : 'success', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});
    </script>
  
<?php }?>
<div class="body-inner-part">
    <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;Booking Cancellation</span></div>
    <div class="main-inner-page">
      <h2><?php echo @LBL_BOOKING_CANCELLATION;?>
</h2>
      <?php $_template = new Smarty_Internal_Template("member_top.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
      <div class="ratings">
        <div class="profile">
          <h2><?php echo @LBL_BOOKING_DETAILS;?>
</h2>
          <span style="color:#0094C8;font-size:15px;"><em style="color:#000000;font-size: 13px;"><?php echo @LBL_BOOKING_NO;?>
</em>
            #<?php echo $_smarty_tpl->getVariable('db_booking_details')->value[0]['vBookingNo'];?>

          </span>
          <span style="color:#0094C8;font-size:15px;"><em style="color:#000000;font-size: 13px;"><?php echo @LBL_BOOKING_DATE;?>
</em>
            <?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_booking_details')->value[0]['dBookingDate'],14);?>

            <!--comment out time
             @ <?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_booking_details')->value[0]['dBookingTime'],18);?>

             -->
          </span>
          <span style="color:#0094C8;font-size:15px;"><em style="color:#000000;font-size: 13px;"><?php echo @LBL_BOOKING_FROM;?>
</em>
            <?php echo $_smarty_tpl->getVariable('db_booking_details')->value[0]['vFromPlace'];?>

          </span>
          <span style="color:#0094C8;font-size:15px;"><em style="color:#000000;font-size: 13px;"><?php echo @LBL_BOOKING_TO;?>
</em>
            <?php echo $_smarty_tpl->getVariable('db_booking_details')->value[0]['vToPlace'];?>

          </span>
          <span style="color:#0094C8;font-size:15px;"><em style="color:#000000;font-size: 13px;"><?php echo @LBL_TRAVELLER_NAME;?>
</em>
            <?php echo $_smarty_tpl->getVariable('db_booking_details')->value[0]['vDriverFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_booking_details')->value[0]['vDriverLastName'];?>

          </span>
          <span style="color:#0094C8;font-size:15px;"><em style="color:#000000;font-size: 13px;"><?php echo @LBL_BOOKER_NAME;?>
</em>
            <?php echo $_smarty_tpl->getVariable('db_booking_details')->value[0]['vBookerFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_booking_details')->value[0]['vBookerLastName'];?>

          </span>
          <span style="border-bottom:1px solid #CCCCCC;"></span>
          <!--
          <span style="border-radius: 4px;background:#05A2DB;width:99%;padding:5px;font-size:15px;color:#FFFFFF;">
          <em style="width: 45px;"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
msgGrowl_warning.png"></em>
          As a Sender after cancelation you get 50% amount as a refund from your booking payment amount. Once you cancel booking you can not active it.
          </span>
          <span style="border-radius: 4px;background:#05A2DB;width:99%;padding:5px;font-size:15px;color:#FFFFFF;">
          <em style="width: 45px;"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
msgGrowl_warning.png"></em>
          As a driver after cancelation your account is "frozen" during 24 hours, you can not post a trip for next 12/24 hours. Once you cancel booking you can not active it.
          </span>
          -->
          <form name="frmcancelation" id="frmcancelation" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=m-booking_cancelation_form" method="post">
          <?php if (@PAYMENT_OPTION=='PayPal'){?>
          <?php if ($_smarty_tpl->getVariable('db_booking_details')->value[0]['enttype']=='Passenger'){?>
          <span style="border-radius: 4px;background:#05A2DB;width:99%;padding:5px;font-size:15px;color:#FFFFFF;">
          <em style="width: 45px;"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
msgGrowl_info.png"></em>
          Provide your Payment Email (Paypal) details or Bank details for refund process.
          </span>
          <?php }?>
          <h2>Booking Cancellation Details </h2>
          <?php if ($_smarty_tpl->getVariable('db_booking_details')->value[0]['enttype']=='Passenger'){?>
          <span><em> <?php echo @LBL_PAYMENT_EMAIL;?>
 *</em>
          <input name="Data_Payment[vPaymentEmail]" id="vPaymentEmail" type="text" class="validate[custom[email]] profile-input" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vPaymentEmail'];?>
" />
          <p><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_registration-run" target="_blank"><?php echo @LBL_CREATE_PAYPAL_ACCOUNT;?>
</a></p>
          </span>
          <div id="bankinfo" style="">
          <span><em> &nbsp; </em>
          <?php echo @LBL_OR;?>

          </span>
          <span><em><?php echo @LBL_ACC_HODLER;?>
 *</em>
          <input name="Data_Payment[vBankAccountHolderName]" id="vBankAccountHolderName" type="text" class="profile-input" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vBankAccountHolderName'];?>
" />
          </span>
          <span><em><?php echo @LBL_ACC_NUMBER;?>
 (IBAN) *</em>
          <input name="Data_Payment[vAccountNumber]" id="vAccountNumber" type="text" class="profile-input" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vAccountNumber'];?>
" />
          </span>
          <span><em><?php echo @LBL_NAME_BANK;?>
 *</em>
          <input name="Data_Payment[vBankLocation]" id="vBankLocation" type="text" class="profile-input" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vBankLocation'];?>
" />
          </span>
          <span><em><?php echo @LBL_BANK_LOCATION;?>
 *</em>
          <input name="Data_Payment[vBankName]" id="vBankName" type="text" class="profile-input" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vBankName'];?>
" />
          </span>
          <span><em><?php echo @LBL_BIC_SWIFT_CODE;?>
 *</em>
          <input name="Data_Payment[vBIC_SWIFT_Code]" id="vBIC_SWIFT_Code" type="text" class="profile-input" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vBIC_SWIFT_Code'];?>
" />
          </span>
          </div>
          <?php }?>
          <?php }?>
          <input type="hidden" name="id" id="id" value="<?php echo $_smarty_tpl->getVariable('iBookingId')->value;?>
">
          <input type="hidden" name="action" id="action" value="cancel_booking">
          <span><em> <?php echo @LBL_CANCELLATION_REASON;?>
 *</em>
          <textarea name="Data[tCancelReason]" id="tCancelReason" class="validate[required] profile-textarea"></textarea>
          </span>
          <span><em> &nbsp;</em>
          <input type="checkbox" name="RefundPolicy" id="RefundPolicy" class="validate[required]" value="Yes"> I agree to the <a href="<?php echo $_smarty_tpl->getVariable('tcinfig')->value['tsite_url'];?>
refund-cancellation-policy" style="color:#05A2DB;" target="_blank">Refund & Cancellation Policy</a>
          </span>
          <?php if ($_smarty_tpl->getVariable('db_booking_details')->value[0]['enttype']=='Passenger'){?>
          <span class="sav-but-pro"><a href="javascript:void(0);" onclick="javascript:checkcancel_passenger(); return false;">Cancel Booking</a></span>
          <?php }else{ ?>
          <span class="sav-but-pro"><a href="javascript:void(0);" onclick="javascript:checkcancel(); return false;">Cancel Booking</a></span>
          <?php }?>
          </form>
         </div>
      </div>
      <?php $_template = new Smarty_Internal_Template("member_profile_left.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
	</div>
    <div style="clear:both;"></div>
  </div>

<script>
function checkcancel(){
 resp = jQuery("#frmcancelation").validationEngine('validate');// alert(resp);return false;
	if(resp == true){
		document.frmcancelation.submit();
	}else{
		return false;
	}
}





function Trim_me(s)
{
	return s.replace(/^\s+/g, '').replace(/\s+$/g, '');
}

var	site_url = '<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
';
    function get_sate(country){
      $("#state").html('Wait...');
      var request = $.ajax({
    	  type: "POST",
    	  url: site_url+'index.php?file=m-get_state',
    	  data: "country="+country,

    	  success: function(data) {
    		  $("#state").html(data);
          $('#vState').val('<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vState'];?>
');
    		}
    	});

    	request.fail(function(jqXHR, textStatus) {
    	  alert( "Request failed: " + textStatus );
    	});
    }


</script>

<?php if ($_smarty_tpl->getVariable('db_member')->value[0]['vCountry']!=''){?>

  <script>
     get_sate('<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vCountry'];?>
');
  </script>

<?php }?>
<?php if (@PAYMENT_OPTION=='PayPal'){?>

<script>
function checkcancel_passenger(){

  if(Trim_me($("#vPaymentEmail").val()) == ''){
    if(Trim_me($("#vBankAccountHolderName").val()) == '' || Trim_me($("#vAccountNumber").val()) == '' || Trim_me($("#vBankLocation").val()) == '' || Trim_me($("#vBankName").val()) == '' || Trim_me($("#vBIC_SWIFT_Code").val()) == ''){
      alert('Please Provid Payapl payment email detail or All Bank details.'); return false;
    }
  }


  resp = jQuery("#frmcancelation").validationEngine('validate');// alert(resp);return false;
	if(resp == true){
		document.frmcancelation.submit();
	}else{
		return false;
	}
}
</script>

<?php }else{ ?>

<script>
function checkcancel_passenger(){

  resp = jQuery("#frmcancelation").validationEngine('validate');// alert(resp);return false;
	if(resp == true){
		document.frmcancelation.submit();
	}else{
		return false;
	}
}
</script>

<?php }?>
