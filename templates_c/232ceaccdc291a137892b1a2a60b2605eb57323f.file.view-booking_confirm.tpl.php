<?php /* Smarty version Smarty-3.0.7, created on 2016-01-28 00:33:48
         compiled from "/home4/shipcliq/public_html/templates/members/view-booking_confirm.tpl" */ ?>
<?php /*%%SmartyHeaderCode:103232095356a9b64c1aa508-99180913%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '232ceaccdc291a137892b1a2a60b2605eb57323f' => 
    array (
      0 => '/home4/shipcliq/public_html/templates/members/view-booking_confirm.tpl',
      1 => 1453962697,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '103232095356a9b64c1aa508-99180913',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="body-inner-part">
  <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_RIDE_BOOK_CONFIRM;?>
</span></div>
  <div class="main-inner-page">
    <h2><?php echo @LBL_RIDE_BOOK_CONFIRM;?>
</h2>
    <?php if ($_smarty_tpl->getVariable('cd')->value==1){?>
    <div class="right-inner-part">
      <h2><?php echo @LBL_RIDE_BOOK_SUCC_CONF;?>
</h2>
      <p><?php echo @LBL_BOOKING_CONF_SUCC;?>
</p>
      <p><?php echo $_smarty_tpl->getVariable('mailcont')->value;?>
</p>
    </div>
    <?php }else{ ?>
    <div class="right-inner-part">
      <h2><?php echo @LBL_RIDE_BOOK_FAILED;?>
</h2>
      <p><?php echo @LBL_PAYMENT_BOOK_FAILED;?>
</p>
      <p><?php echo @LBL_TRY_AGAIN;?>
</p>
    </div>
    <?php }?>
	<?php $_template = new Smarty_Internal_Template("left.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
  </div>
  <div style="clear:both;"></div>
</div>