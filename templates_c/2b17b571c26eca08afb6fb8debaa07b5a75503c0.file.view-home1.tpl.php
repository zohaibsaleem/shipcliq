<?php /* Smarty version Smarty-3.0.7, created on 2015-03-07 16:51:37
         compiled from "/home/www/blablaclone/templates/content/view-home1.tpl" */ ?>
<?php /*%%SmartyHeaderCode:214585134254fadf41229d39-85544013%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2b17b571c26eca08afb6fb8debaa07b5a75503c0' => 
    array (
      0 => '/home/www/blablaclone/templates/content/view-home1.tpl',
      1 => 1425569989,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '214585134254fadf41229d39-85544013',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_truncate')) include '/home/www/blablaclone/libraries/general/smarty/plugins/modifier.truncate.php';
?><link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_stylesheets'];?>
front/simptip-mini.css" media="screen,projection" />
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
gmap3.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
rrb/zebra_datepicker.src.js"></script>
<link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
rrb/default.css" type="text/css"> 
<div id="main"> <?php $_template = new Smarty_Internal_Template("homeheader1.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
  <div id="body-content">
    <div class="body-top-search-part">
      <div class="body-top-search-part-inner"> <span><?php echo @LBL_FIND_RIDE;?>
</span>
        <form name="home_search" id="home_search" method="post" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_list">
          <p>
            <input name="search" id="search" type="hidden" value="place">
            <input name="From_lat_long" id="From_lat_long" type="hidden" value="">
            <input name="To_lat_long" id="To_lat_long" type="hidden" value="">
            <input name="From" id="From" type="text" placeholder="<?php echo @LBL_FROM;?>
" class="validate[required] find-cla" value=""/>
            <input placeholder="<?php echo @LBL_TO;?>
" name="To" id="To" type="text" class="validate[required] find-cla" value=""/>
            <input name="searchdate" id="searchdate" type="text" class="he-input-cal" value="" placeholder="<?php echo @LBL_DATE;?>
" />
            <a href="javascript:void(0);" onClick="search_now();"><?php echo @LBL_SEARCH;?>
</a> </p>
        </form>
      </div>
    </div>
    <div class="body-mid-part"> <?php if (count($_smarty_tpl->getVariable('ridesarroundyou')->value)>0){?>
      <div class="body-mid-part-inner">
        <div class="rides-around">
          <p><?php echo @LBL_RIDE;?>
<br />
            <?php echo @LBL_AROUND;?>
<br />
            <strong><?php echo @LBL_YOU;?>
</strong></p>
          <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_list&search=arround_you"><?php echo @LBL_VIEW_MORE;?>
</a> </div>
        <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["around"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['name'] = "around";
$_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('ridesarroundyou')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['max'] = (int)2;
$_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['show'] = true;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['max'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['total']);
?> <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_details&from=home&id=<?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['around']['index']]['iRideId'];?>
&dt=<?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['around']['index']]['strtotime'];?>
&ret=<?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['around']['index']]['return'];?>
">
        <div <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['around']['first']){?>class="trip-main"<?php }else{ ?>class="trip-main  trip-main-last"<?php }?>>
          <div class="trip roundcol <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['around']['last']){?> last <?php }?> simptip-position-top simptip-multiline simptip-info simptip-smooth" data-tooltip="<?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['around']['index']]['address'];?>
">
            <div class="ho-box-left"> <img src="<?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['around']['index']]['img'];?>
" alt=""/> </div>
            <div class="trip-deails">
              <div class="trip-deails-left">
                <h2><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['around']['index']]['dDate'],10);?>
 - <?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['around']['index']]['time'],12);?>
</h2>
                <b><?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['around']['index']]['address'];?>
</b>
                <p><span style="display: block; width: 65px; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 0;"> <span style=" position:relative; top:-10px; margin-right: 10px;display: block; width: <?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['around']['index']]['rates'];?>
%; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 -13px;"></span> </span></p>
                <em><?php echo @LBL_CAR;?>
: <?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['around']['index']]['membercar'];?>
</em>
                <?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['around']['index']]['pref'];?>

               </div>
              <div class="trip-deails-right"><?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['around']['index']]['price'];?>
</div>
              <div class="trip-deails-right-bot"></div>
            </div>
          </div></a>
          <div style=" clear:both;"></div>
        </div>
         <?php endfor; endif; ?> </div>
      <?php }?>  
      <?php if (count($_smarty_tpl->getVariable('airportmainarr')->value)>0){?>
      <div class="body-mid-part-inner">
        <div class="rides-around2">
          <p><?php echo @LBL_AIRPORT;?>
<br />
            <strong><?php echo @LBL_RIDE;?>
</strong></p>
          <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_list&search=simple&type=airport"><?php echo @LBL_VIEW_MORE;?>
</a> </div>
        <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('airportmainarr')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = (int)2;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?> <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_details&from=home&id=<?php echo $_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iRideId'];?>
&dt=<?php echo $_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['strtotime'];?>
&ret=<?php echo $_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['return'];?>
">
        <div <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['i']['first']){?>class="trip-main"<?php }else{ ?>class="trip-main  trip-main-last"<?php }?>>
          <div class="trip airportcol <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['i']['last']){?> last <?php }?> simptip-position-top simptip-multiline simptip-info simptip-smooth" data-tooltip="<?php echo $_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainDeparture'];?>
 - <?php echo $_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainArrival'];?>
">
           <div class="ho-box-left"> <img src="<?php echo $_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['memberimage'];?>
" alt=""/> </div>
            <div class="trip-deails">
              <div class="trip-deails-left">
                <h2><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dDateOut'],10);?>
 - <?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['deptime'],12);?>
</h2>
                <b><?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainDeparture'],15);?>
 - <?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainArrival'],15);?>
</b>
                <p> <span style="display: block; width: 65px; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 0;"> <span style="  position:relative; top:-10px; margin-right: 10px;display: block; width: <?php echo $_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['rating'];?>
%; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 -13px;"></span> </span> </p>
                <em><?php echo @LBL_CAR;?>
: <?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['membercar'],15);?>
</em>
                <?php echo $_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['pref'];?>

                </div>
              <div class="trip-deails-right"><?php echo $_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['totprice'];?>
</div>
              <div class="trip-deails-right-bot"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
airport.png"></div>
            </div>
          </div>
          </a>
          <div style=" clear:both;"></div>
        </div>
        <?php endfor; endif; ?> </div>
      <?php }?>
      <?php if (count($_smarty_tpl->getVariable('ladiesmainarr')->value)>0){?>
      <div class="body-mid-part-inner">
        <div class="rides-around3">
          <p><?php echo @LBL_LADIES;?>
<br />
            <strong><?php echo @LBL_ONLY;?>
</strong></p>
          <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_list&search=simple&type=ladiesonly"><?php echo @LBL_VIEW_MORE;?>
</a> </div>
        <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('ladiesmainarr')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = (int)2;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?> <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_details&from=home&id=<?php echo $_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iRideId'];?>
&dt=<?php echo $_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['strtotime'];?>
&ret=<?php echo $_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['return'];?>
">
        <div <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['i']['first']){?>class="trip-main"<?php }else{ ?>class="trip-main  trip-main-last"<?php }?>>
          <div class="trip ladiescol <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['i']['last']){?> last <?php }?> simptip-position-top simptip-multiline simptip-info simptip-smooth" data-tooltip="<?php echo $_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainDeparture'];?>
 - <?php echo $_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainArrival'];?>
">
            
              <div class="ho-box-left"> <img src="<?php echo $_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['memberimage'];?>
" alt=""/> </div>
            
            <div class="trip-deails">
              <div class="trip-deails-left">
                <h2><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dDateOut'],10);?>
 - <?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['deptime'],12);?>
</h2>
                <b><?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainDeparture'],15);?>
 - <?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainArrival'],15);?>
</b>
                <p> <span style="display: block; width: 65px; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 0;"> <span style=" position:relative; top:-10px; margin-right: 10px;display: block; width: <?php echo $_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['rating'];?>
%; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 -13px;"></span> </span></p>
                <em><?php echo @LBL_CAR;?>
: <?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['membercar'],15);?>
</em>
                <?php echo $_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['pref'];?>
  </div>
              <div class="trip-deails-right"><?php echo $_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['totprice'];?>
</div>
              <div class="trip-deails-right-bot"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
ladies.png"></div>
            </div>
          </div>
          <div style=" clear:both;"></div>
        </div>
        </a> <?php endfor; endif; ?> </div>
      <?php }?>
      <?php if (count($_smarty_tpl->getVariable('shoppingmainarr')->value)>0){?>
      <div class="body-mid-part-inner">
        <div class="rides-around4">
          <p><?php echo @LBL_SHOPPING;?>
<br />
            <strong><?php echo @LBL_RIDE;?>
</strong></p>
          <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_list&search=simple&type=shopping"><?php echo @LBL_VIEW_MORE;?>
</a> </div>
        <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('shoppingmainarr')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = (int)2;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?> <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_details&from=home&id=<?php echo $_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iRideId'];?>
&dt=<?php echo $_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['strtotime'];?>
&ret=<?php echo $_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['return'];?>
">
        <div <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['i']['first']){?>class="trip-main"<?php }else{ ?>class="trip-main  trip-main-last"<?php }?>>
          <div class="trip shoppingcol <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['i']['last']){?> last <?php }?> simptip-position-top simptip-multiline simptip-info simptip-smooth" data-tooltip="<?php echo $_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainDeparture'];?>
 - <?php echo $_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainArrival'];?>
">
            
              <div class="ho-box-left"><img src="<?php echo $_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['memberimage'];?>
" alt=""/> </div>
              
            <div class="trip-deails">
              <div class="trip-deails-left">
                <h2><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dDateOut'],10);?>
 - <?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['deptime'],12);?>
</h2>
                <b><?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainDeparture'],15);?>
 - <?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainArrival'],15);?>
</b>
                <p> <span style="display: block; width: 65px; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 0;"> <span style=" position:relative; top:-10px; margin-right: 10px;display: block; width: <?php echo $_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['rating'];?>
%; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 -13px;"></span> </span> </p>
                <em><?php echo @LBL_CAR;?>
: <?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['membercar'],15);?>
</em> 
                <?php echo $_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['pref'];?>
 </div>
              <div class="trip-deails-right"><?php echo $_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['totprice'];?>
</div>
              <div class="trip-deails-right-bot"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
shopping.png"></div>
            </div>
          </div>
          <div style=" clear:both;"></div>
        </div>
        </a> <?php endfor; endif; ?> </div>
      <?php }?>
      <div style="clear:both;"></div>
    </div>
    <div class="latest-infor"> <?php if (count($_smarty_tpl->getVariable('latestmainarr')->value)>0){?>
      <div class="latest-rides">
        <h2><?php echo @LBL_LATEST;?>
<span><?php echo @LBL_RIDE;?>
</span></h2>
        <ul>
          <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('latestmainarr')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = (int)3;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
          <li <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['i']['index']%2!=0){?>class="active"<?php }?>><img src="<?php echo $_smarty_tpl->getVariable('latestmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['memberimage'];?>
" alt="" style="max-width:55px;max-width:51px;"/> <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_details&from=home&id=<?php echo $_smarty_tpl->getVariable('latestmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iRideId'];?>
&dt=<?php echo $_smarty_tpl->getVariable('latestmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['strtotime'];?>
&ret=<?php echo $_smarty_tpl->getVariable('latestmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['return'];?>
" >
            <p class="simptip-position-top simptip-multiline simptip-info simptip-smooth" data-tooltip="<?php echo $_smarty_tpl->getVariable('latestmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainDeparture'];?>
 - <?php echo $_smarty_tpl->getVariable('latestmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainArrival'];?>
"><?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('latestmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainDeparture'],15);?>
-<?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('latestmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainArrival'],15);?>
<br />
              <?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('latestmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dDateOut'],10);?>
 - <?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('latestmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['deptime'],12);?>
</p>
            <span style="font-size: 18px;float:right;"><?php echo $_smarty_tpl->getVariable('latestmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['totprice'];?>
</span> </a> </li>
          <?php endfor; endif; ?>
        </ul>
        <em><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_list&search=simple&type=latest"><?php echo @LBL_VIEW_ALL;?>
<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
point-icon.png" alt="" /></a></em> </div>
      <?php }?>
      <!--<?php if (count($_smarty_tpl->getVariable('todaymainarr')->value)>0){?>
      <div class="last-chance">
        <h2><?php echo @LBL_LAST;?>
<span><?php echo @LBL_CHANCE;?>
</span> <em><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_list&search=simple&type=today">view all<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
point-icon.png" alt="" /></a></em></h2>
        <ul>
          <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('todaymainarr')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = (int)3;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
          <li <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['i']['last']){?>class="color-gray last"<?php }elseif($_smarty_tpl->getVariable('smarty')->value['section']['i']['index']%2==0){?>class="color-gray"<?php }else{ ?><?php }?>> <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_details&from=home&id=<?php echo $_smarty_tpl->getVariable('todaymainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iRideId'];?>
&dt=<?php echo $_smarty_tpl->getVariable('todaymainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['strtotime'];?>
&ret=<?php echo $_smarty_tpl->getVariable('todaymainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['return'];?>
" class="simptip-position-top simptip-multiline simptip-info simptip-smooth" data-tooltip="<?php echo $_smarty_tpl->getVariable('todaymainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainDeparture'];?>
 - <?php echo $_smarty_tpl->getVariable('todaymainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainArrival'];?>
">
            <div class="chance-deails">
              <div class="chance-deails-left">
                <p><?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('todaymainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainDeparture'],15);?>
-<?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('todaymainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainArrival'],15);?>
</p>
                <p> <span style="display: block; width: 65px; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 0;"> <span style="position:relative; top:-13px; margin-right: 10px;margin-right: 10px;display: block; width: <?php echo $_smarty_tpl->getVariable('todaymainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['rating'];?>
%; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 -13px;"></span> </span> </p>
                <span><?php echo @LBL_CAR;?>
: <?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('todaymainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['membercar'],15);?>
</span> </div>
              <div class="chance-deails-right"><?php echo $_smarty_tpl->getVariable('todaymainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['totprice'];?>
 </div>
            </div>
            </a> </li>
          <?php endfor; endif; ?>
        </ul>
      </div>
      <?php }?>-->
      <div class="latest-blogs">
        <h2><?php echo @LBL_LATEST;?>
<span><?php echo @LBL_BLOGS;?>
</span> <em><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
blog" target="_blank"><?php echo @LBL_VIEW_ALL;?>
<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
point-icon.png" alt="" /></a></em></h2>
        <ul>
          <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['name'] = 'rec';
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_post')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['rec']['total']);
?>
          <li>
    			<strong><a href="<?php echo $_smarty_tpl->getVariable('db_post')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rec']['index']]['guid'];?>
" target="_blank"><?php echo $_smarty_tpl->getVariable('db_post')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rec']['index']]['post_title'];?>
</a></strong>
    			<p><?php echo smarty_modifier_truncate(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->getVariable('db_post')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rec']['index']]['post_content']),150);?>
</p><div class="date"><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_post')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rec']['index']]['post_date'],9);?>
</div>
    		  </li>
        	<?php endfor; endif; ?>
        </ul>
      </div>
      <div class="subscribe-br">
        <form name="frmnewsletter" id="frmnewsletter" method="post" action="">
        <h2><?php echo @LBL_SUBSRIBE;?>
 <span><?php echo @LBL_NEWSLETTERS;?>
</span></h2>
        <b>
        <input name="vNewsletterEmail" id="vNewsletterEmail" type="text" class="validate[required,custom[email]] newsletter-ho-br" placeholder="Enter E-mail Address" />
        <input value="<?php echo @LBL_SUBMIT;?>
" id="newletter_subscribe" name="" type="button" class="newsletter-but-br" onClick="checknewsletter();" style="display:;"/>
        <input value="<?php echo @LBL_WAIT;?>
"   id="newletter_loading" name="" type="button" class="newsletter-but-br" onClick="checknewsletter();" style="display:none;"/>
        </b>
        <p><?php echo @LBL_SUBSCRIBE_MESSAGE1;?>
</p>
        </form>
      </div>
      <div style="clear:both;"></div>
    </div>
    <div class="body-bottom-part">
      <div class="body-bottom-part-t-part">
        <div class="body-bottom-part-t-part-inner">
          <div class="video1">
            <h2><?php echo $_smarty_tpl->getVariable('box1title')->value;?>
</h2>
            <div class="home-vid"><?php echo $_smarty_tpl->getVariable('box1video')->value;?>
</div>
           </div>
          <div class="trust-safety">
            <h2><?php echo $_smarty_tpl->getVariable('box2title')->value;?>
</h2>
            <img src="<?php echo $_smarty_tpl->getVariable('box2image')->value;?>
" alt="" />
            <p><?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('box2tPageDesc')->value,115);?>
</p>
            <em><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
how-we-works"><?php echo @LBL_VIEW_MORE;?>
<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
point-icon.png" alt="" /></a></em> </div>
          <div class="facebook-cov">
            <iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FFacebookDevelopers&amp;width=292&amp;height=303&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=true&amp;appId=1404686089767190" scrolling="no" frameborder="0" style="background:#fff;border:none; overflow:hidden; width:100%; height:100%;" allowTransparency="true"></iframe>
          </div>
        </div>
      </div>
      <?php if (count($_smarty_tpl->getVariable('featuredin')->value)>0){?>
      <div class="featured-in">
        <div class="featured-in-inner">
          <h2><?php echo @LBL_FEATURED;?>
<span><?php echo @LBL_IN;?>
</span></h2>
          <ul>
            <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['name'] = "featuredin";
$_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('featuredin')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["featuredin"]['total']);
?>
            <?php if ($_smarty_tpl->getVariable('featuredin')->value[$_smarty_tpl->getVariable('smarty')->value['section']['featuredin']['index']]['img_url']!=''){?>
            <li><?php if ($_smarty_tpl->getVariable('featuredin')->value[$_smarty_tpl->getVariable('smarty')->value['section']['featuredin']['index']]['vURL']!=''){?><a href="<?php echo $_smarty_tpl->getVariable('featuredin')->value[$_smarty_tpl->getVariable('smarty')->value['section']['featuredin']['index']]['vURL'];?>
" target="_blank"><?php }?><img src="<?php echo $_smarty_tpl->getVariable('featuredin')->value[$_smarty_tpl->getVariable('smarty')->value['section']['featuredin']['index']]['img_url'];?>
" alt="" /><?php if ($_smarty_tpl->getVariable('featuredin')->value[$_smarty_tpl->getVariable('smarty')->value['section']['featuredin']['index']]['vURL']!=''){?></a><?php }?></li>
            <?php }?>
            <?php endfor; endif; ?>
          </ul>
          <div style="clear:both;"></div>
        </div>
      </div>
      <?php }?>
      <div style="clear:both;"></div>
    </div>
    <?php $_template = new Smarty_Internal_Template("footer1.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?> </div>
  <div style="clear:both;"></div>
</div>
<div id="map-canvas" class="gmap3"></div>

<script type="text/javascript">
function initialize() {
    //var input = document.getElementById("From);     
    //var options = {componentRestrictions: {country: 'us'}};
                 
    var from = new google.maps.places.Autocomplete(document.getElementById('From'));
    var to = new google.maps.places.Autocomplete(document.getElementById('To'));     
}  
             
google.maps.event.addDomListener(window, 'load', initialize);

function search_now(){ 
  resp = jQuery("#home_search").validationEngine('validate');    
	if(resp == true){
    if(document.getElementById('From').value !='' && document.getElementById('To').value != ''){
      var From = $("#From").val();
      $("#map-canvas").gmap3({
              getlatlng:{
                address:  From,
                callback: function(results){
                 $("#From_lat_long").val(results[0].geometry.location);
                }
              }
      });
      
      var To = $("#To").val();
      $("#map-canvas").gmap3({
              getlatlng:{
                address:  To,
                callback: function(results){
                 $("#To_lat_long").val(results[0].geometry.location);
                 document.home_search.submit();
                }
              }
      }); 
      return false;
    }else{
      alert('Please select From and To address both');
      return false;
    }
  }else{
    return false;
  }
}

/*function go_get_latlong(from){
   if(from == 'From'){
     var From = $("#From").val();
     $("#map-canvas").gmap3({
            getlatlng:{
              address:  From,
              callback: function(results){
               $("#From_lat_long").val(results[0].geometry.location);
              }
            }
      });      
   }else{
     var To = $("#To").val();
     $("#map-canvas").gmap3({
            getlatlng:{
              address:  To,
              callback: function(results){
               $("#To_lat_long").val(results[0].geometry.location);
              }
            }
      });       
   }
} */
</script>
<script type="text/javascript">        
    var	site_url = '<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
';
    $('#searchdate').Zebra_DatePicker({
      direction: 0
    });
    
    $(function() {
        var from = document.getElementById('From');        
        var autocomplete_from = new google.maps.places.Autocomplete(from);
    		google.maps.event.addListener(autocomplete_from, 'place_changed', function() {
    			var place = autocomplete_from.getPlace();     		
    			go_get_latlong('From');
    		});
    		
    		var to = document.getElementById('To');
    		var autocomplete_to = new google.maps.places.Autocomplete(to);
    		google.maps.event.addListener(autocomplete_to, 'place_changed', function() {
    			var place = autocomplete_to.getPlace();     			
    			go_get_latlong('to');
    		});
    });  
    
    function setCookie(cname,cvalue,exdays)
    {
      var d = new Date();
      d.setTime(d.getTime()+(exdays*24*60*60*1000));
      var expires = "expires="+d.toGMTString();
      document.cookie = cname + "=" + cvalue + "; " + expires;
    }  
    
    function getCookie(cname)
    {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) 
      {
      var c = ca[i].trim();
      if (c.indexOf(name)==0) return c.substring(name.length,c.length);
      }
    return "";
    }
    
    function showPosition(position)
    { 
      /*$(function() {   
       var docHeight = $(document).height();    
       $("body").append("<div id='overlay'></div>");     
       $("#overlay")
          .height(docHeight)
          .css({
             'opacity' : 0.7,
             'position': 'absolute',
             'top': 0,
             'left': 0,
             'background-color': 'black',
             'width': '100%',
             'z-index': 5000
          });
    
      });*/
      setCookie('userlatitude', position.coords.latitude, 1);
      setCookie('userlongitude', position.coords.longitude, 1); 
      if(getCookie('country_code') == ''){
        var request = $.ajax({  
      	  type: "POST",
      	  url: site_url+"createcookie.php",  
      	  data: "", 	  
      	  
      	  success: function(data) { //alert(data);   		  
            document.location.reload(true);      			
      		}
      	});
      } 
    }  
    
    function getLocation()
    {        
      if (navigator.geolocation)
        {
        navigator.geolocation.getCurrentPosition(showPosition);
        }
      else{x.innerHTML="Geolocation is not supported by this browser.";}
    }
    
    function checkCookie()
    {
      var userlatitude = getCookie("userlatitude");
      var userlongitude = getCookie("userlongitude");
      if (userlatitude != "" && userlongitude != '')
      {
        //alert("Welcome again " + username);
      }else{  
        getLocation();
      }
    }
    
    function chnage_ride_types(type){
      if(type == 'international'){
        document.cookie = "country_code=; expires=Thu, 01 Jan 1970 00:00:00 GMT";
        setCookie('ride_locations_type', 'international', 1);
        window.location = site_url;
      }else{
        setCookie('ride_locations_type', 'local', 1);
        getLocation();
      }
    } 
  </script>
<script>
$('#frmnewsletter').bind('keydown',function(e){
    if(e.which == 13){
      checknewsletter(); return false;
    }
});
checkCookie();
</script>  

