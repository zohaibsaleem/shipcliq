<?php /* Smarty version Smarty-3.0.7, created on 2015-08-31 17:29:21
         compiled from "/home/www/cargosharing1/templates/members/view-booking_confirm.tpl" */ ?>
<?php /*%%SmartyHeaderCode:183995468255e44199078cf3-86080983%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '356a7fe9dc142fc5ef4fccd163374338b0f5190a' => 
    array (
      0 => '/home/www/cargosharing1/templates/members/view-booking_confirm.tpl',
      1 => 1436784180,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '183995468255e44199078cf3-86080983',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="body-inner-part">
  <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_RIDE_BOOK_CONFIRM;?>
</span></div>
  <div class="main-inner-page">
    <h2><?php echo @LBL_RIDE_BOOK_CONFIRM;?>
</h2>
    <?php if ($_smarty_tpl->getVariable('cd')->value==1){?>
    <div class="right-inner-part">
      <h2><?php echo @LBL_RIDE_BOOK_SUCC_CONF;?>
</h2>
      <p><?php echo @LBL_BOOKING_CONF_SUCC;?>
</p>
      <p><?php echo $_smarty_tpl->getVariable('mailcont')->value;?>
</p>
    </div>
    <?php }else{ ?>
    <div class="right-inner-part">
      <h2><?php echo @LBL_RIDE_BOOK_FAILED;?>
</h2>
      <p><?php echo @LBL_PAYMENT_BOOK_FAILED;?>
</p>
      <p><?php echo @LBL_TRY_AGAIN;?>
</p>
    </div>
    <?php }?>
	<?php $_template = new Smarty_Internal_Template("left.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
  </div>
  <div style="clear:both;"></div>
</div>