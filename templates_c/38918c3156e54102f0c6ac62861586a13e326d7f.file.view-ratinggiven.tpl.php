<?php /* Smarty version Smarty-3.0.7, created on 2015-05-29 12:40:02
         compiled from "/home/www/xfetch/templates/members/view-ratinggiven.tpl" */ ?>
<?php /*%%SmartyHeaderCode:788994214556810cae99141-01686362%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '38918c3156e54102f0c6ac62861586a13e326d7f' => 
    array (
      0 => '/home/www/xfetch/templates/members/view-ratinggiven.tpl',
      1 => 1432883045,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '788994214556810cae99141-01686362',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
jquery.raty.js"></script>
<?php if ($_GET['var_msg']!=''){?>          
<?php if ($_GET['msg_code']=='1'){?>
  
    <script>        
      showNotification({type : 'success', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});  
    </script>
  
<?php }?>
<?php }?> 

<?php if ($_GET['var_msg']!=''){?>          
<?php if ($_GET['msg_code']=='0'){?>
  
    <script>
      showNotification({type : 'error', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});
    </script>
  
<?php }?>
<?php }?> 
<div class="body-inner-part">
  <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_RAGINGS_GIVEN;?>
</span></div>
  <!-------------------------inner-page----------------->
  <div class="main-inner-page">
    <h2><?php echo @LBL_RATINGS;?>
 <span><?php echo @LBL_GIVEN;?>
</span></h2>
    <?php $_template = new Smarty_Internal_Template("member_top.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
    <?php $_template = new Smarty_Internal_Template("member_rating_left.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
    <div class="ratings">
      <div class="rating-received2">
        <div class="rating-given-right-part">
           <table width="100%" border="0" cellpadding="0" cellspacing="0" class="note-from-re3">
             <tr>
                <th width="25%" align="left"><?php echo @LBL_MEMBER_NAME;?>
</th>
                <th width="25%" align="left"><?php echo @LBL_PHOTO;?>
</th>
                <th width="15%" align="left"><?php echo @LBL_RATE;?>
</th>
                <th width="35%" align="left"><?php echo @LBL_GIVEN_DATE;?>
</th>
             </tr>
             <?php if (count($_smarty_tpl->getVariable('db_ratings_given')->value)>0){?>
              <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_ratings_given')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
              <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['i']['index']%2){?>
              <?php $_smarty_tpl->tpl_vars["class"] = new Smarty_variable('', null, null);?>
              <?php }else{ ?>
              <?php $_smarty_tpl->tpl_vars["class"] = new Smarty_variable("alt", null, null);?>
              <?php }?>
              <tr class="<?php echo $_smarty_tpl->getVariable('class')->value;?>
">
                <td><?php echo $_smarty_tpl->getVariable('db_ratings_given')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_ratings_given')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vLastName'];?>
</td>
                <td><img src="<?php echo $_smarty_tpl->getVariable('db_ratings_given')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['img'];?>
" alt=""/></td>
                <td> 
				<span style="float:left; margin-left: 10px;margin-top:5px;display: block; width: 65px; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 0;">
					<span style="display: block; float:none; width: <?php echo $_smarty_tpl->getVariable('db_ratings_given')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['rating_width'];?>
%; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 -13px;"></span>
				</span>
				</td>
                <td><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_ratings_given')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dAddedDate'],13);?>
</td>
              </tr>
              <?php endfor; endif; ?>
              <?php }else{ ?>
              <tr>
               <td colspan="4"><div class="NoRating" style="text-align:center;color:red;width:100%;"><?php echo @LBL_NO_RECORDS_FOUND;?>
</div></td>
              </tr>
              <?php }?>
           </table> 
        </div>
      </div>
    </div>
  </div>
  <!-------------------------inner-page end----------------->
  <div style="clear:both;"></div>
</div>
 
<script>
function findmember(){
    //alert('hello..');
    jQuery("#frmsearch").validationEngine('init',{scroll: false});
	  jQuery("#frmsearch").validationEngine('attach',{scroll: false});
		resp = jQuery("#frmsearch").validationEngine('validate');
	//	alert(resp); return false;
		if(resp == true){
			document.frmsearch.submit();
		}else{
			return false;
		}		
}
function cancelsearch()
{
	window.location =  "<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
leave-Rating";
	return false;
}
</script> 
