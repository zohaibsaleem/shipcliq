<?php /* Smarty version Smarty-3.0.7, created on 2015-07-20 17:15:04
         compiled from "/home/www/xfetch/templates/members/view-memberrating.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6186927655acdf4000f7d7-11068219%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '41696e0455ccbc70b4baedc4b4b45b71ac0d5884' => 
    array (
      0 => '/home/www/xfetch/templates/members/view-memberrating.tpl',
      1 => 1437392681,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6186927655acdf4000f7d7-11068219',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_GET['var_msg']!=''){?>          
<?php if ($_GET['msg_code']=='1'){?>
  
<script>        
      showNotification({duration: 3, autoClose: true, type : 'success', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});  
    </script>

<?php }?>
<?php }?> 

<?php if ($_GET['var_msg']!=''){?>          
<?php if ($_GET['msg_code']=='0'){?>
  
<script>
      showNotification({duration: 3, autoClose: true, type : 'error', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});
    </script>

<?php }?>
<?php }?>
<div class="body-inner-part">
  <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_RATIN_FORM;?>
</span></div>
  <!-------------------------inner-page----------------->
  <div class="main-inner-page">
    <h2><?php echo @LBL_RATING;?>
 <span><?php echo @LBL_FORM;?>
</span></h2>
    <?php $_template = new Smarty_Internal_Template("member_top.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
    <div class="ratings">
      <div class="ratings-from">
        <form name="frmmemrating" id="frmmemrating" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=m-memberrating_a" method="post" enctype="multipart/form-data">
          <input type="hidden" name="action" id="action" value="giverating">
          <input type="hidden" name="Data[iMemberFromId]" id="iMemberFromId" value="<?php echo $_smarty_tpl->getVariable('iMemberFromId')->value;?>
">
          <input type="hidden" name="iMemberToId" id="iMemberToId" value="<?php echo $_smarty_tpl->getVariable('iMemberToId')->value;?>
">
          <input type="hidden" name="Data[iBookingId]" id="iBookingId" value="<?php echo $_smarty_tpl->getVariable('iBookingId')->value;?>
">
          <h3><?php echo @LBL_SHARE_EXPERIENCE;?>
</h3>
          <div class="list-choose-profil-left">
            <h2><?php echo $_smarty_tpl->getVariable('db_tomember')->value[0]['vFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_tomember')->value[0]['vLastName'];?>
 <?php echo @LBL_WAS;?>
 : </h2>
            <span>
            <input name="Data[eWas]" id="eWas1" type="radio" value="Passenger" class="validate[required,groupRequired[mem]]" onclick="showaddress(this.value);" <?php if ($_smarty_tpl->getVariable('Member')->value=="Booker"){?>disabled <?php }else{ ?> checked <?php }?> />
            &nbsp;&nbsp;<?php echo @LBL_APASSANGER;?>
 </span> <span>
            <input name="Data[eWas]" id="eWas2" type="radio" value="Driver" class="validate[required,groupRequired[mem]]" onclick="showaddress(this.value);" <?php if ($_smarty_tpl->getVariable('Member')->value=="Driver"){?>disabled <?php }else{ ?> checked  <?php }?>/>
            &nbsp;&nbsp;<?php echo @LBL_DRIVER;?>
 </span> <span>
            <!-- <input name="Data[eWas]" id="eWas3" type="radio" value="None" class="validate[required,groupRequired[mem]]" onclick="showaddress(this.value);" />
          &nbsp;&nbsp;<?php echo @LBL_I_DID_NOT_TRAVEL;?>
 <?php echo $_smarty_tpl->getVariable('db_tomember')->value[0]['vFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_tomember')->value[0]['vLastName'];?>
 </span>-->
          </div>
          <div class="list-choose-profil-right"> <img src="<?php echo $_smarty_tpl->getVariable('db_tomember')->value[0]['img'];?>
" alt=""/> <a href="javascript:void(0);"><?php echo $_smarty_tpl->getVariable('db_tomember')->value[0]['vFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_tomember')->value[0]['vLastName'];?>
 <?php if ($_smarty_tpl->getVariable('db_tomember')->value[0]['iBirthYear']!=0){?>(<?php echo $_smarty_tpl->getVariable('age')->value;?>
 <?php echo @LBL_YEARS;?>
 <?php echo @LBL_OLD1;?>
)<?php }?> </a><br />
            <!--<img class="ra-img" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
sprite-prefs1.png" alt="" />-->
          </div>
          <div id="experience" style="display:none;">
            <div class="main-elements-container">
              <!--<h3>Click on the stars to rate your experience</h3>
            <p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
blue-star.png" alt="" />5 star (s) </p>-->
              <h3><?php echo @LBL_SELECT_RATE;?>
</h3>
              <select name="Data[iRate]" id="iRate" class="profile-select1">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
              </select>
              <label> <?php echo @LBL_SHARE_EXPR_NOTE;?>
 :
              <textarea name="Data[tFeedback]" id="tFeedback" cols="" rows="" class="validate[required] elements-input"></textarea>
              <!--200 characters left-->
              </label>
            </div>
          </div>
          <!-- 
        <div id="skill" style="display:none;">  
          <div class="list-choose-bottom-left">
            <h3><?php echo @LBL_EVALUATE_SKILL;?>
</h3>
            <span>                                                                                                               
            <input name="eEvaluate" id="eEvaluate" type="checkbox" value="Yes" class=""/>
            &nbsp;&nbsp;<?php echo @LBL_SHARE_EXP_NOTE1;?>
 </span>
            
            <div id="evaluate">     
            <span>
            <input name="eSkill" id="eSkill1" type="radio" value="Pleasant" class="validate[required,groupRequired[skll]]"/>
            &nbsp;&nbsp;<?php echo @LBL_PLEASANT;?>
</span> 
            <span>
            <input name="eSkill" id="eSkill2" type="radio" value="ToImprove" class="validate[required,groupRequired[skll]]"/>
            &nbsp;&nbsp;<?php echo @LBL_COULD_IMPROVED;?>
</span> 
            <span>
            <input name="eSkill" id="eSkill2" type="radio" value="Avoid" class="validate[required,groupRequired[skll]]"/>
            &nbsp;&nbsp;<?php echo @LBL_TO_BE_AVOIDED;?>
 </span>
            </div>
          </div>
          <div class="list-choose-bottom-right"> 
            <img src="<?php echo $_smarty_tpl->getVariable('db_tomember')->value[0]['img'];?>
" alt="" height="72px" width="72px"/>
             <?php echo @LBL_SHARE_EXP_NOTE2;?>
 
          </div>
        </div>
-->
          <div id="publish" style="display:none;">
            <div class="pbulish-but"> <a href="javascript:void(0);" onclick="javascript:publishrating(); return false;"> <?php echo @LBL_PUBLISH;?>
</a> </div>
          </div>
        </form>
      </div>
    </div>
      <?php $_template = new Smarty_Internal_Template("member_rating_left.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
  </div>
  <!-------------------------inner-page end----------------->

  <div style="clear:both;"></div>
</div>

<script>
function publishrating(){
    //alert('hello..');
    jQuery("#frmmemrating").validationEngine('init',{scroll: false});
	  jQuery("#frmmemrating").validationEngine('attach',{scroll: false});
		resp = jQuery("#frmmemrating").validationEngine('validate');
	//	alert(resp); return false;
		if(resp == true){
			document.frmmemrating.submit();
		}else{
			return false;
		}		
}
function showaddress(val)
{
    if(val == "Passenger")
    {
         document.getElementById("experience").style.display = '';
         document.getElementById("publish").style.display = '';
         document.getElementById("skill").style.display = 'none';  
    }
    if(val == "Driver")
    {
         document.getElementById("experience").style.display = '';
         document.getElementById("publish").style.display = '';
         document.getElementById("skill").style.display = '';
    }
    if(val == "None")
    {
         document.getElementById("experience").style.display = '';
         document.getElementById("publish").style.display = '';
         document.getElementById("skill").style.display = 'none';  
    }
}  
/*function cancelsearch()
{
	window.location =  "<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
leave-Rating";
	return false;
} */
$(document).ready(function(){
    //Hide div w/id extra
    $("#evaluate").css("display","");
    // Add onclick handler to checkbox w/id additional_contacts
    $("#eEvaluate").click(function(){
        // If checked
        if ($("#eEvaluate").is(":checked"))
        {
            //hide the div
            $("#evaluate").css("display","none");
        }
        else
        {
            //otherwise, show it
            $("#evaluate").css("display","");
        }
    });

});
</script>

<?php if ($_smarty_tpl->getVariable('Member')->value=='Driver'){?>

<script>
showaddress("Passenger");
</script>

<?php }else{ ?>

<script>
showaddress("Driver");
</script>

<?php }?>