<?php /* Smarty version Smarty-3.0.7, created on 2016-01-13 15:08:41
         compiled from "/home/www/cargosharing1/templates/members/view-receiverating.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1502060392569668791c0cd8-99800995%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '44b9ba2dcf5944a27118ca25b322826e007ae60e' => 
    array (
      0 => '/home/www/cargosharing1/templates/members/view-receiverating.tpl',
      1 => 1436782352,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1502060392569668791c0cd8-99800995',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
jquery.raty.js"></script>
<?php if ($_GET['var_msg']!=''){?>          
<?php if ($_GET['msg_code']=='1'){?>
  
    <script>        
      showNotification({type : 'success', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});  
    </script>
  
<?php }?>
<?php }?> 

<?php if ($_GET['var_msg']!=''){?>          
<?php if ($_GET['msg_code']=='0'){?>
  
    <script>
      showNotification({type : 'error', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});
    </script>
  
<?php }?>
<?php }?> 
<div class="body-inner-part">
  <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_RATING;?>
 <?php echo @LBL_RECEIVED;?>
</span></div>
  <!-------------------------inner-page----------------->
  <div class="main-inner-page">
    <h2><?php echo @LBL_RATING;?>
 <span><?php echo @LBL_RECEIVED;?>
</span></h2>
    <?php $_template = new Smarty_Internal_Template("member_top.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
    <div class="ratings">
      <div class="rating-received">
        <div class="rating-received-left-part" style="width:21%;">
          <span><?php echo $_smarty_tpl->getVariable('rating')->value;?>
 <?php echo @LBL_RATING;?>
<!-- rating - 3 / 5 --> 
            <!--<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
blue-star.png" alt="" />-->
            <span style="float: right;margin-left: 15px;margin-top: 1px;display: block; width: 65px; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 0;">
                  <span style="display: block; width: <?php echo $_smarty_tpl->getVariable('rating_width')->value;?>
%; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 -13px;"></span>
            </span>
         </span> 
        </div>
        <div class="rating-received-right-part">
          <div class="rating-received-right-part-inner"> 
            <span>5 <?php echo @LBL_STARS;?>
</span>
            <div class="progress"><img style="width:<?php echo $_smarty_tpl->getVariable('totalwidth')->value[5];?>
%; padding: 0px 0 7px;max-width: 100%;" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/progress.jpg"></div>
            <span class="last-star"><?php echo $_smarty_tpl->getVariable('totrating')->value[5];?>
</span> 
          </div>
          <div class="rating-received-right-part-inner"> 
            <span>4 <?php echo @LBL_STARS;?>
</span>
            <div class="progress"><img style="width:<?php echo $_smarty_tpl->getVariable('totalwidth')->value[4];?>
%; padding: 0px 0 7px;max-width: 100%;" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/progress.jpg"></div>
            <span class="last-star"><?php echo $_smarty_tpl->getVariable('totrating')->value[4];?>
</span> 
          </div>
          <div class="rating-received-right-part-inner"> 
            <span>3 <?php echo @LBL_STARS;?>
</span>
            <div class="progress"><img style="width:<?php echo $_smarty_tpl->getVariable('totalwidth')->value[3];?>
%; padding: 0px 0 7px;max-width: 100%;" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/progress.jpg"></div>
            <span class="last-star"><?php echo $_smarty_tpl->getVariable('totrating')->value[3];?>
</span>
          </div>
          <div class="rating-received-right-part-inner">
            <span>2 <?php echo @LBL_STARS;?>
</span>
            <div class="progress"><img style="width:<?php echo $_smarty_tpl->getVariable('totalwidth')->value[2];?>
%; padding: 0px 0 7px;max-width: 100%;" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/progress.jpg"></div>
            <span class="last-star"><?php echo $_smarty_tpl->getVariable('totrating')->value[2];?>
</span>
          </div>
          <div class="rating-received-right-part-inner">
            <span>1 <?php echo @LBL_STARS;?>
</span>
            <div class="progress"><img style="width:<?php echo $_smarty_tpl->getVariable('totalwidth')->value[1];?>
%; padding: 0px 0 7px;max-width: 100%;" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/progress.jpg"></div>
            <span class="last-star"><?php echo $_smarty_tpl->getVariable('totrating')->value[1];?>
</span>
          </div>
        </div>
      </div>
      <?php if (count($_smarty_tpl->getVariable('db_rating_from')->value)>0){?>
      <div class="user-comment-list">
        <ul>
          <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_rating_from')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
          <li>
            <div class="user-img2"><img src="<?php echo $_smarty_tpl->getVariable('db_rating_from')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['img'];?>
" alt="" /></div>
            <div class="user-profile-de">
           
              <h2 style="font-size:20px;border-bottom:none;"><div style="float:left;"><?php echo @LBL_RATING;?>
 : <?php echo $_smarty_tpl->getVariable('db_rating_from')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iRate'];?>
</div>
              <div style="margin:0px; float:left;"> <span style="float:left; margin-left: 10px;margin-top:5px;display: block; width: 65px; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 0;">
                <span style="display: block; float:none; width: <?php echo $_smarty_tpl->getVariable('db_rating_from')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['rating_width'];?>
%; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 -13px;"></span>
            </span></div>
            <div style="color:#414141; float:right; font-family:'droid_sansregular'; font-size:13px;"><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_rating_from')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dAddedDate'],7);?>
 </div>
              </h2>
              <p><strong><?php echo @LBL_FROM;?>
 <?php echo $_smarty_tpl->getVariable('db_rating_from')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_rating_from')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vLastName'];?>
 :</strong> <?php echo $_smarty_tpl->getVariable('db_rating_from')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tFeedback'];?>
. </p>
            </div>
          </li>
          <?php endfor; endif; ?>
        </ul>
       </div>
      <?php }?> 
    </div>
	<?php $_template = new Smarty_Internal_Template("member_rating_left.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
  </div>
  <!-------------------------inner-page end----------------->
  
  <div style="clear:both;"></div>
</div>
 
<script>
function findmember(){
    //alert('hello..');
    jQuery("#frmsearch").validationEngine('init',{scroll: false});
	  jQuery("#frmsearch").validationEngine('attach',{scroll: false});
		resp = jQuery("#frmsearch").validationEngine('validate');
	//	alert(resp); return false;
		if(resp == true){
			document.frmsearch.submit();
		}else{
			return false;
		}		
}
function cancelsearch()
{
	window.location =  "<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
leave-Rating";
	return false;
}
</script> 
