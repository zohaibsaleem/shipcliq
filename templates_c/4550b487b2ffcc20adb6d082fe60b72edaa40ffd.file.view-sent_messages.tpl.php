<?php /* Smarty version Smarty-3.0.7, created on 2015-11-02 15:26:34
         compiled from "/home/www/cargosharing1/templates/members/view-sent_messages.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7865713235637335235db03-83096622%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4550b487b2ffcc20adb6d082fe60b72edaa40ffd' => 
    array (
      0 => '/home/www/cargosharing1/templates/members/view-sent_messages.tpl',
      1 => 1433571443,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7865713235637335235db03-83096622',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_truncate')) include '/home/www/cargosharing1/libraries/general/smarty/plugins/modifier.truncate.php';
?><div class="body-inner-part">
  <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_MESSAGES;?>
</span></div>
  <div class="main-inner-page">
    <h2><?php echo @LBL_MESSAGES;?>
</h2>
    <div class="dashbord">
      <?php $_template = new Smarty_Internal_Template("member_top.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
      <div class="bro-message">
        <div class="tab-pane">
          <ul>
            <!--<li> <a href="#"> Questions &amp; Answers </a> </li> -->
            <li> <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsute_url'];?>
received-messages"> <?php echo @LBL_RECEIVED_MESSAGES;?>
 <em><?php echo $_smarty_tpl->getVariable('tot_unread')->value;?>
</em></a> </li>
            <li> <a href="javascript:void(0);" class="active"> <?php echo @LBL_SENT_MESSAGES;?>
 </a> </li>
            <li> <a class="last" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsute_url'];?>
archived-messages" > <?php echo @LBL_ARCHIVED_MESSAGES;?>
</a> </li>
          </ul>
        </div>
        <div class="messages-container">
          <?php if (count($_smarty_tpl->getVariable('db_messages')->value)>0){?>
          <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['name'] = "messages";
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_messages')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['total']);
?>
          <label>
            <span>
            <img src="<?php echo $_smarty_tpl->getVariable('db_messages')->value[$_smarty_tpl->getVariable('smarty')->value['section']['messages']['index']]['img'];?>
" alt="" style="height:70px;width:70px;"/><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
message-details/sent/<?php echo $_smarty_tpl->getVariable('db_messages')->value[$_smarty_tpl->getVariable('smarty')->value['section']['messages']['index']]['iMessageId'];?>
"><?php echo $_smarty_tpl->getVariable('db_messages')->value[$_smarty_tpl->getVariable('smarty')->value['section']['messages']['index']]['vFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_messages')->value[$_smarty_tpl->getVariable('smarty')->value['section']['messages']['index']]['vLastName'];?>
</a></span>
            <span class="stap2"><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
message-details/sent/<?php echo $_smarty_tpl->getVariable('db_messages')->value[$_smarty_tpl->getVariable('smarty')->value['section']['messages']['index']]['iMessageId'];?>
"><?php echo smarty_modifier_truncate(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->getVariable('db_messages')->value[$_smarty_tpl->getVariable('smarty')->value['section']['messages']['index']]['tMessage']),50);?>
</a></span> 
            <span class="stap3"><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_messages')->value[$_smarty_tpl->getVariable('smarty')->value['section']['messages']['index']]['dAddedDate'],7);?>

              <!--<a href="#"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
mail-delete-archive.png" alt="" /></a>-->
            </span> 
          </label>
          <?php endfor; endif; ?>
          <?php }else{ ?>
          <div class="main-block">
          <?php echo @LBL_NO;?>
 <?php echo @LBL_SENT_MESSAGES;?>
</div>
          <?php }?>
        </div>
        <?php if (count($_smarty_tpl->getVariable('db_messages')->value)>0){?>
        <div class="paging"><?php echo $_smarty_tpl->getVariable('page_link')->value;?>
</div>
        <?php }?>
      </div>
      <div style="clear:both;"></div>
    </div>
    <div style="clear:both;"></div>
  </div>
  <div style="clear:both;"></div>
</div>