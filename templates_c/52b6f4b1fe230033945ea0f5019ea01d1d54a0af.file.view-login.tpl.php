<?php /* Smarty version Smarty-3.0.7, created on 2016-03-16 04:56:45
         compiled from "/home4/shipcliq/public_html/templates/content/view-login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:164681866056e92ddd3068b5-29857330%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '52b6f4b1fe230033945ea0f5019ea01d1d54a0af' => 
    array (
      0 => '/home4/shipcliq/public_html/templates/content/view-login.tpl',
      1 => 1458120338,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '164681866056e92ddd3068b5-29857330',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_smarty_tpl->getVariable('var_msg_forget')->value!=''){?>
<?php if ($_smarty_tpl->getVariable('msg_code')->value=='1'){?> 

 <script>
  showNotification({type : 'error', message: '<?php echo $_smarty_tpl->getVariable('var_msg_forget')->value;?>
'});
 </script> 
 
<?php }?>     	
<?php if ($_smarty_tpl->getVariable('msg_code')->value=='0'){?>

 <script>
  showNotification({type : 'success', message: '<?php echo $_smarty_tpl->getVariable('var_msg_forget')->value;?>
'});
 </script> 
     
<?php }?> 
<?php }?> 
<div class="body-inner-part">
  <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_LOGIN;?>
</span></div>
  <div class="main-inner-page">
    <h2><?php echo @LBL_LOGIN;?>
</h2>
    <div class="right-inner-part">
      <div class="login-page">
        <div class="form-login">
          <h4><?php echo @LBL_LOGIN;?>
</h4>
          <form name="login_box" id="login_box" method="post">
          <input type="hidden" name="lgfor" id="lgfor" value="">
          <input type="hidden" name="from" id="from" value="">
          <input type="hidden" name="action" id="action" value="loginchk">
          <div class="inner-form1 login-page-form">
          <span>
            <label class="email-label"><?php echo @LBL_EMAIL;?>
 : <strong>*</strong></label>
            <div class="singlerow-login">
              <input type="text" name="vEmail" id="vEmail" class="validate[required,custom[email]] form-input-login" value="<?php echo $_smarty_tpl->getVariable('vEmail')->value;?>
">
            </div>
            </span>
            <span>
            <label class="email-label"><?php echo @LBL_PASSWORD;?>
 : <strong>*</strong></label>
            <div class="singlerow-login">
              <input type="password"  name="vPassword" id="vPassword" class="validate[required,minSize[6]] form-input-login" value="<?php echo $_smarty_tpl->getVariable('vPassword')->value;?>
">
            </div>
            </span>
            <span>
            <label class="email-label blanck-c">&nbsp;</label>
            <div class="singlerow-login-log"><a onClick="check_login();" href="javascript:void(0);" <?php if ($_smarty_tpl->getVariable('sess_lang')->value=='RS'){?>style="padding:7px 0px;font-size:14px;"<?php }?>><?php echo @LBL_SUBMIT;?>
</a> <a onclick="document.login_box.reset();return false;" href="javascript:void(0);" <?php if ($_smarty_tpl->getVariable('sess_lang')->value=='RS'){?>style="padding:7px 0px;font-size:14px;"<?php }?>><?php echo @LBL_RESET;?>
</a></div>
            </span>
            <div style="clear:both;"></div>
          </div>
          </form>
          <div class="bot-line-login"><?php echo @LBL_OR;?>
</div>
          <table width="100%" cellspacing="0" cellpadding="7" border="0">
            <tbody>
              <tr>
                <td align="center">
                  <button class="btn btn-primary" onclick="fbconnect();return false;">
                  <a style="color:#fff;" onclick="fbconnect();"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
fb-but-icon.png" alt="" /><?php echo @LBL_CONNECT_FACEBOOK;?>
</a>
                  </button>
                </td>
              </tr>
			  <!--
              <tr>
                <td align="center">
					<?php echo $_smarty_tpl->getVariable('draugiem_login_button')->value;?>

                </td>
              </tr>
			  -->
            </tbody>
          </table>
        </div>
        <div class="login-fr">
          <h2 class="inner-hd"><?php echo @LBL_NEW_CUST;?>
?</h2>
          <p><?php echo @LBL_CUST_DESCRIPTION;?>
</p>
          <div class="singlerow-login"><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
sign-up"><?php echo @LBL_REGISTER_NOW;?>
</a></div>
        </div> 
        <form name="forgetpassword" id="forgetpassword" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-login" method="post">
        <input type="Hidden" name="action" value="forgetpsw">
        <div class="fordot">
          <h2 class="inner-hd"><?php echo @LBL_FORGOT_PASS;?>
?</h2>
          <p><?php echo @LBL_FORGOT_DESCRIPTON;?>
</p>
          <span><em><?php echo @LBL_EMAIL;?>
 :</em>
          <input type="text" id="vPassEmail" name="vPassEmail" class="validate[required,custom[email]] form-input-login-for">
          <div class="singlerow-forgot"><a onclick="javascript:checkforgotpassword(); return false;" href="javascript:void(0);"><?php echo @LBL_SEND;?>
</a></div>
          </span>
          <div style="clear:both;"></div>
        </div>
        </form>
        <div style="clear:both;"></div>
      </div>
    </div>
	<?php $_template = new Smarty_Internal_Template("left.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
  </div><div style="clear:both;"></div>
</div>

<script>
  $('#login_box').bind('keydown',function(e){ 
    if(e.which == 13){
      check_login(); return false;
    }
  });
  
   //login with fb
  function fbconnect()
  {
	 javscript:window.location='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
fbconnect.php';
  }
  
  //login with dr
  /*function drconnect()
  {
	 javscript:window.location='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
drconnect.php';
  } */
  
  function check_login(){
	var redirect_url = '<?php echo $_smarty_tpl->getVariable('redirect_link')->value;?>
';
    jQuery("#login_box").validationEngine('attach',{scroll: false});  
    resp = jQuery("#login_box").validationEngine('validate');    
	  if(resp == true){
      //$("#loginbtn").hide(); 
      //$("#loginloader").show();    
      var request = $.ajax({  
    	  type: "POST",
    	  url: site_url+'index.php?file=c-login',  
    	  data: $("#login_box").serialize(), 	  
    	  
    	  success: function(data) {  //alert(data); return false; 
          $("#loginloader").hide();
          $("#loginbtn").show();                	     	
          if(data == 0){             
            showNotification({type : 'error', message: '<?php echo @LBL_LOGIN_FAILED_USER_PASS;?>
.'});
          }
          else if(data == 1){              
            showNotification({type : 'error', message: '<?php echo @LBL_ACC_NOT_ACTIVE;?>
.'});
          }
          else{
				if(redirect_url != '')
				{
					window.location = redirect_url; 
				}
				else
				{
					window.location = site_url+'my-account'; 
				}                          
			}  			
    	}
  	 });
  	
     request.fail(function(jqXHR, textStatus) {
      alert( "Request failed: " + textStatus ); 
     });
      
    }else{
      return false;
    }  
  }
  
  function checkforgotpassword(){    
    resp = jQuery("#forgetpassword").validationEngine('validate');
		if(resp == true){
			document.forgetpassword.submit();
		}else{
			return false;
		}		
	}
	
 
</script>

