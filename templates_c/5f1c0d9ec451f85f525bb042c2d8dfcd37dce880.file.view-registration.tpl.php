<?php /* Smarty version Smarty-3.0.7, created on 2016-02-16 09:32:49
         compiled from "/home4/shipcliq/public_html/templates/content/view-registration.tpl" */ ?>
<?php /*%%SmartyHeaderCode:205224914056c341216c1ef2-21991864%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5f1c0d9ec451f85f525bb042c2d8dfcd37dce880' => 
    array (
      0 => '/home4/shipcliq/public_html/templates/content/view-registration.tpl',
      1 => 1455555478,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '205224914056c341216c1ef2-21991864',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
 <?php if ($_smarty_tpl->getVariable('var_msg')->value!=''){?>
 
 <script>
  showNotification({type : 'error', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});
 </script>
 
<?php }?>
<div class="body-inner-part">
  <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_REGISTRATION;?>
</span></div>
  <div class="main-inner-page">
    <h2><?php echo @LBL_REGISTRATION;?>
</h2>

    <form class="drop form_new" name="frmregister" id="frmregister" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-registration_a" method="post">
    <input type="Hidden" name="mode" value="register">
    <div class="right-inner-part">
      <div class="registration">
        <table width="100%" cellspacing="0" cellpadding="7" border="0">
          <tr>
            <td align="center"><button class="btn btn-primary" onclick="fbconnect();return false;">
              <a style="color:#fff;" href="javascript:void(0);" onclick="fbconnect();"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
fb-but-icon.png" alt="" /><?php echo @LBL_CONNECT_FACEBOOK;?>
</a>
              </button></td>
          </tr>
          <tr>
            <td class="registration-or" align="center"><?php echo @LBL_OR;?>
</td>
          </tr>
          <tr>
            <td style="padding:0px;">&nbsp;</td>
          </tr>
        </table>
        <div class="reg-input1" id="registration">
          <label><em><?php echo @LBL_NICK_NAME;?>
 : <strong>*</strong></em>
            <input type="text" id="vNickName" name="vNickName" class="validate[required] form-input-re" value="<?php echo $_smarty_tpl->getVariable('db_customer')->value[0]['vNickName'];?>
">
          </label>
          <label><em><?php echo @LBL_FIRST_NAME;?>
 : <strong>*</strong></em>
            <input type="text" id="vFirstName" name="vFirstName" class="validate[required] form-input-re" value="<?php echo $_smarty_tpl->getVariable('db_customer')->value[0]['vFirstName'];?>
">
          </label>
          <label><em><?php echo @LBL_LAST_NAME;?>
 : <strong>*</strong></em>
            <input type="text" id="vLastName" name="vLastName" class="validate[required] form-input-re" value="<?php echo $_smarty_tpl->getVariable('db_customer')->value[0]['vLastName'];?>
">
          </label>
          <div style="clear:both;"></div>
        </div>
        <div class="reg-input1">
          <label><em><?php echo @LBL_EMAIL;?>
 : <strong>*</strong></em>
            <input type="text" name="vEmail" id="vEmail" class="validate[required,custom[email]] form-input-re" value="<?php echo $_smarty_tpl->getVariable('db_customer')->value[0]['vEmail'];?>
"></label>
          <label><em><?php echo @LBL_PASSWORD;?>
 : <strong>*</strong></em>
            <input type="password" title="Password" name="vPassword" id="vPassword" class="validate[required,minSize[6]] form-input-re"></label>
          <label><em><?php echo @LBL_RE_ENTER;?>
 <?php echo @LBL_PASSWORD;?>
 : <strong>*</strong></em>
            <input type="password" title="Re-Enter Password" name="vRePassword" id="vRePassword" class="validate[required,equals[vPassword]] form-input-re">
          </label>
           </span>
            <label><em> <?php echo @LBL_MOBILE;?>
 <?php echo @LBL_PHONE;?>
 *</em>
           <select name="vCountry" id="vCountry" class="validate[required] profile-select1" onChange="get_sate(this.value);">
              <option value=""><?php echo @LBL_SELECT_COUNTRY;?>
</option>
             <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_country')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
                <option value="<?php echo $_smarty_tpl->getVariable('db_country')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vCountryCode'];?>
" <?php if ($_smarty_tpl->getVariable('db_country')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vCountryCode']==$_smarty_tpl->getVariable('vCountry')->value){?> selected <?php }?>><?php echo $_smarty_tpl->getVariable('db_country')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vCountry'];?>
</option>
             <?php endfor; endif; ?>
            </select>

            &nbsp;&nbsp;
            <input name="vPhone" id="vPhone" type="text" class="validate[required,custom[phone]] profile-input1" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vPhone'];?>
" />
            <!--<p>
              <input name="Data[eShowPhoneOnline]" id="eShowPhoneOnline" type="checkbox" <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['eShowPhoneOnline']=='No'){?>checked="true"<?php }?> />
              &nbsp;&nbsp;<?php echo @LBL_NEVER_SHOW_PHONE;?>
 </p>-->
            </label>
            <label><em> <?php echo @LBL_STATE;?>
 </em>
            <div id="state">
            <select name="vState" id="vState" class="validate[required] profile-select form-re-selec">
              <option value=""><?php echo @LBL_SELECT;?>
 <?php echo @LBL_STATE;?>
</option>
            </select>
            </div>
            </label>
          <label><em><?php echo @LBL_PREFERRED_LANG;?>
 : <strong></strong></em>
            <select name="vLanguageCode" id="vLanguageCode" class="form-input-select form-re-selec">
              <option value="EN" <?php if ($_smarty_tpl->getVariable('db_customer')->value[0]['vLanguageCode']=='EN'){?> selected <?php }?>><?php echo @LBL_ENGLISH;?>
</option>
              <option value="FN" <?php if ($_smarty_tpl->getVariable('db_customer')->value[0]['vLanguageCode']=='FN'){?> selected <?php }?>><?php echo @LBL_FRENCH;?>
</option>

            </select>
          </label>
		  <?php if ($_smarty_tpl->getVariable('PAYMENT_OPTION')->value=='PayPal'){?>
		  <label><em><?php echo @LBL_PAYMENT_EMAIL;?>
 : </em>
			<input name="vPaymentEmail" id="vPaymentEmail" type="text" class="validate[custom[email]] form-input-re" value="<?php echo $_smarty_tpl->getVariable('db_customer')->value[0]['vPaymentEmail'];?>
" />&nbsp;&nbsp;&nbsp;&nbsp;<a class="create-paypal" href="https://www.paypal.com/" target="_blank"><?php echo @LBL_CREATE_PAYPAL_ACCOUNT;?>
</a>
			</label>
		<?php }?>
          <div style="clear:both;"></div>
        </div>
        <div class="reg-input1" style="border:none;">
          <label><em><?php echo @LBL_ENTER_CODE;?>
 : <strong>*</strong></em>
            <input type="text" title="Captcha Code" name="capthca" id="capthca" class="validate[required] form-input-re1">
            &nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
plugins/captcha/CaptchaSecurityImages.php" align="top" height="31"/>
            </label>
        </div>
        <div class="reg-input1" style="border:none;">
        <label><em class="reg-last1">&nbsp;</em>
        <div class="registration-button"><a onclick="javascript:checkregister();" href="javascript:void(0);"><?php echo @LBL_SUBMIT;?>
</a> <a onclick="document.frmregister.reset();return false;" href="javascript:void(0);"><?php echo @LBL_RESET;?>
</a></div>
        </div></label>
        <div style="clear:both;"></div>
      </div>
    </div>
    </form>
	<?php $_template = new Smarty_Internal_Template("left.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
  </div><div style="clear:both;"></div>
</div>

<script>
  $('#frmregister').bind('keydown',function(e){
    if(e.which == 13){
      checkregister(); return false;
    }
  });
function checkregister(){
  jQuery("#frmregister").validationEngine('init',{scroll: false});
	jQuery("#frmregister").validationEngine('attach',{scroll: false});
	resp = jQuery("#frmregister").validationEngine('validate');
	if(resp == true){
		document.frmregister.submit();
	}else{
		return false;
	}
}

function fbconnect()
  {
	 javscript:window.location='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
fbconnect.php';
  }

 var	site_url = '<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
';
    function get_sate(country){

      $("#state").html('Wait...');
	  //alert("country="+country+"&vStaten="+'<?php echo $_smarty_tpl->getVariable('vState')->value;?>
');
      var request = $.ajax({

    	  type: "POST",
    	  url: site_url+'index.php?file=m-get_state',
    	  data: "country="+country+"&vStaten="+'<?php echo $_smarty_tpl->getVariable('vState')->value;?>
',
    	  success: function(data) {
		  //alert(data);
    		  $("#state").html(data);
         // $('#vState').val('<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vState'];?>
');
    		}
    	});

    	request.fail(function(jqXHR, textStatus) {
    	  alert( "Request failed: " + textStatus );
    	});
    }

</script>


<?php if ($_smarty_tpl->getVariable('vCountry')->value!=''){?>

  <script>
     get_sate('<?php echo $_smarty_tpl->getVariable('vCountry')->value;?>
');
  </script>

<?php }?>
