<?php /* Smarty version Smarty-3.0.7, created on 2015-05-25 14:39:55
         compiled from "/home/www/xfetch/templates/members/view-delete_account.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2576823855562e6e3b9c448-76885858%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5f6bde31a26dc97202781a35e7151d126f77f40c' => 
    array (
      0 => '/home/www/xfetch/templates/members/view-delete_account.tpl',
      1 => 1395812666,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2576823855562e6e3b9c448-76885858',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
  <div class="body-inner-part">
      <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_DELETE_ACCOUNT;?>
</span></div>
      <div class="main-inner-page">
        <h2><?php echo @LBL_DELETE_ACCOUNT;?>
</h2>
        <?php $_template = new Smarty_Internal_Template("member_top.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
        <?php $_template = new Smarty_Internal_Template("member_profile_left.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
        <div class="ratings">
          <div class="profile">
            <h2><?php echo @LBL_DELETE_YOUR_ACC;?>
 </h2>
            <form name="frmdelete" enctype="multipart/form-data" id="frmdelete" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=m-delete_account_a" method="post">
            <?php if ($_smarty_tpl->getVariable('msg')->value!=1){?>
                <span><?php echo @LBL_CLICK_CONFIRM;?>
</span>
                <span class="sav-but-pro"><a href="javascript:void(0);" onclick="javascript:checkprofile(); return false;">Confirm</a></span>
            <?php }?>
            <?php if ($_smarty_tpl->getVariable('msg')->value==1){?><span style="font-size:16px; color:#21610B;"><?php echo @LBL_DELETE_ACC_MSG;?>
</span><?php }?>
            </form>
           </div>
        </div>
      </div>
      <!-------------------------inner-page end----------------->
      
      <div style="clear:both;"></div>
    </div>
 
<script>
function checkprofile(){
 resp = confirm("<?php echo @LBL_SURE_DELET_ACCOUNT;?>
");
	if(resp == true){
		document.frmdelete.submit();
	}else{
		return false;
	}		
}
</script> 
 