<?php /* Smarty version Smarty-3.0.7, created on 2015-01-13 14:25:05
         compiled from "/home/www/blablaclone/templates/member_top.tpl" */ ?>
<?php /*%%SmartyHeaderCode:176811066054b4dd693e10f8-42564646%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '68cd1c131cb271bce6b922064d0acf868ad6e858' => 
    array (
      0 => '/home/www/blablaclone/templates/member_top.tpl',
      1 => 1414668759,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '176811066054b4dd693e10f8-42564646',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="tabs-main">
  <ul>
    <li><a <?php if ($_smarty_tpl->getVariable('script')->value=='dashboard'){?>class="active"<?php }?> href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
my-account"><?php echo @LBL_DASHBOARD;?>
</a></li>
    <?php if (@PAYMENT_OPTION!='Contact'){?>
    <li><a <?php if ($_smarty_tpl->getVariable('script')->value=='mybooking'){?>class="active"<?php }?> href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
my-bookings"><?php echo @LBL_MY_BOOKINGS;?>
</a></li>
    <?php }?>
    <li><a <?php if ($_smarty_tpl->getVariable('script')->value=='list_rides_offer'){?>class="active"<?php }?> href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
List-Rides-Offer"><?php echo @LBL_RIDES_OFFERED1;?>
</a></li>
    <li><a <?php if ($_smarty_tpl->getVariable('script')->value=='received_messages'||$_smarty_tpl->getVariable('script')->value=='sent_messages'||$_smarty_tpl->getVariable('script')->value=='messagedtl'){?>class="active"<?php }?> href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
received-messages"><?php echo @LBL_MESSAGES;?>
</a></li>
    <li><a <?php if ($_smarty_tpl->getVariable('script')->value=='member_alert'){?>class="active"<?php }?> href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
Email-Alerts"><?php echo @LBL_EMAIL_ALERTS;?>
</a></li>
    <li><a <?php if ($_smarty_tpl->getVariable('script')->value=='leaverating'||$_smarty_tpl->getVariable('script')->value=='memberrating'||$_smarty_tpl->getVariable('script')->value=='receiverating'||$_smarty_tpl->getVariable('script')->value=='ratinggiven'){?>class="active"<?php }?> href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
leave-Rating"><?php echo @LBL_RATINGS;?>
</a></li>
    <li><a <?php if ($_smarty_tpl->getVariable('script')->value=='edit_profile'||$_smarty_tpl->getVariable('script')->value=='verification'||$_smarty_tpl->getVariable('script')->value=='changepassword'||$_smarty_tpl->getVariable('script')->value=='notification'){?>class="active"<?php }?> href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
personal-information"><?php echo @LBL_PROFILE;?>
</a></li>
  </ul>
  <div style="clear:both;"></div>
</div>