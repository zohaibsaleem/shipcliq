<?php /* Smarty version Smarty-3.0.7, created on 2015-07-20 18:18:58
         compiled from "/home/www/xfetch/templates/members/view-bookingform.tpl" */ ?>
<?php /*%%SmartyHeaderCode:36361743255acee3aca3d21-30632733%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7490b2e115544124caeffd46d513b066863e692b' => 
    array (
      0 => '/home/www/xfetch/templates/members/view-bookingform.tpl',
      1 => 1436783889,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '36361743255acee3aca3d21-30632733',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="body-inner-part">
  <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_BOOKING;?>
</span></div>
  <div class="main-inner-page">
    <h2><?php echo @LBL_BOOKING;?>
</h2>
    <div class="ratings">
      <?php echo $_smarty_tpl->getVariable('ride_details')->value;?>

      <form name="frmpayment" id="frmpayment" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=m-payment_a" method="post">
      <input type="hidden" name="ride_id" id="ride_id" value="<?php echo $_smarty_tpl->getVariable('id')->value;?>
">
      <input type="hidden" name="iRidePointId" id="iRidePointId" value="<?php echo $_smarty_tpl->getVariable('pointsid')->value;?>
">
      <input type="hidden" name="startpoint" id="startpoint" value="<?php echo $_smarty_tpl->getVariable('startpoint')->value;?>
">
      <input type="hidden" name="endpoint" id="endpoint" value="<?php echo $_smarty_tpl->getVariable('endpoint')->value;?>
">  
      <input type="hidden" name="iNoOfSeats" id="iNoOfSeats" value="1">   
      <input type="hidden" name="price" id="price" value="<?php echo $_smarty_tpl->getVariable('price')->value;?>
">
      <input type="hidden" name="fDocumentPrice" id="fDocumentPrice" value="0">
      <input type="hidden" name="fBoxPrice" id="fBoxPrice" value="0">
      <input type="hidden" name="fLuggagePrice" id="fLuggagePrice" value="0">
      <input type="hidden" name="commission" id="commission" value="<?php echo $_smarty_tpl->getVariable('fCommission')->value;?>
">
      <input type="hidden" name="vat" id="vat" value="<?php echo $_smarty_tpl->getVariable('vat')->value;?>
">
      <input type="hidden" name="totamount" id="totamount" value="<?php echo $_smarty_tpl->getVariable('totamount')->value;?>
"> 
      <input type="hidden" name="mainprice" id="mainprice" value="<?php echo $_smarty_tpl->getVariable('mainprice')->value;?>
">
      <input type="hidden" name="commisionprice" id="commisionprice" value="<?php echo $_smarty_tpl->getVariable('commisionprice')->value;?>
">
      <input type="hidden" name="vatprice" id="vatprice" value="<?php echo $_smarty_tpl->getVariable('vatprice')->value;?>
">
      <input type="hidden" name="totprice" id="totprice" value="<?php echo $_smarty_tpl->getVariable('totprice')->value;?>
">           
      <div class="booking1">
        <h2><?php if (@PAYMENT_OPTION=='PayPal'){?><?php echo @LBL_BILING_INFO;?>
<?php }else{ ?>Booker Information<?php }?></h2>
        <span><em><?php echo @LBL_FIRST_NAME;?>
</em>
        <input name="vFirstName" id="vFirstName" type="text" class="validate[required] profile-input" value="<?php echo $_smarty_tpl->getVariable('db_booker_details')->value[0]['vFirstName'];?>
" />
        </span> <span><em><?php echo @LBL_LAST_NAME;?>
</em>
        <input name="vLastName" id="vLastName" type="text" class="validate[required] profile-input" value="<?php echo $_smarty_tpl->getVariable('db_booker_details')->value[0]['vLastName'];?>
"/>
        </span> <span><em><?php echo @LBL_ADDRESS;?>
 :</em>
        <textarea name="vAddress" id="vAddress" cols="" rows="" class="validate[required] profile-textarea"><?php echo $_smarty_tpl->getVariable('db_booker_details')->value[0]['vAddress'];?>
</textarea>
        </span> <span><em><?php echo @LBL_ZIP_CODE;?>
 :</em>
        <input name="vZip" id="vZip" type="text" class="validate[required] profile-input" value="<?php echo $_smarty_tpl->getVariable('db_booker_details')->value[0]['vZip'];?>
"/>
        </span> <span><em><?php echo @LBL_COUNTRY;?>
 :</em>
        <select name="vCountry" id="vCountry" class="validate[required] profile-select" onChange="get_sate(this.value);">
          <option value=""> -- <?php echo @LBL_SELECT_COUNTRY;?>
 -- </option>
          <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["country"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['name'] = "country";
$_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_country')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["country"]['total']);
?>
          <option value="<?php echo $_smarty_tpl->getVariable('db_country')->value[$_smarty_tpl->getVariable('smarty')->value['section']['country']['index']]['vCountryCode'];?>
" <?php if ($_smarty_tpl->getVariable('db_booker_details')->value[0]['vCountry']==$_smarty_tpl->getVariable('db_country')->value[$_smarty_tpl->getVariable('smarty')->value['section']['country']['index']]['vCountryCode']){?> selected <?php }?>><?php echo $_smarty_tpl->getVariable('db_country')->value[$_smarty_tpl->getVariable('smarty')->value['section']['country']['index']]['vCountry'];?>
</option>
          <?php endfor; endif; ?>
        </select>
        </span><span><em><?php echo @LBL_STATE;?>
 :</em>
        <div id="state">
        <select name="vState" id="vState" class="validate[required] profile-select">
          <option value=""> -- <?php echo @LBL_SELECT;?>
 <?php echo @LBL_STATE;?>
 -- </option>
        </select>
        </div>
        </span> <span><em><?php echo @LBL_CITY;?>
 :</em>
        <input name="vCity" id="vCity" type="text" class="validate[required] profile-input" value="<?php echo $_smarty_tpl->getVariable('db_booker_details')->value[0]['vCity'];?>
"/>
        </span> <span><em><?php echo @LBL_PHONE;?>
</em>
        <input name="vPhone" id="vPhone" type="text" class="validate[required,custom[phone]] profile-input" value="<?php echo $_smarty_tpl->getVariable('db_booker_details')->value[0]['vPhone'];?>
"/>
        </span>
        <!--<h2 class="pay-n1">Pay by Credit Card</h2>
        <span><em>Card Type :</em>
        <select name="CardType" id="CardType" class="validate[required] profile-select">
          <option value=""> -- Select Card -- </option>
          <option value="VISA">Visa</option>
          <option value="MC">Master Card</option>
          <option value="MAESTRO">Maestro</option>
          <option value="AMEX">American Express</option>
          <option value="DISC">Discover</option>
        </select>
        </span> <span><em>Credit Card Number :</em>
        <input name="CardNumber"  id="CardNumber" type="text" class="validate[required] profile-input" value="" />
        </span> <span><em>CVV :</em>
        <input name="CV2"  id="CV2" type="text" class="validate[required] profile-input" value=""/>
        </span> <span><em>Card Type :</em>
        <select name="vExpMonth" id="vExpMonth" class="validate[required] profile-select-n">
          <option value=""> -- Select -- </option>
          <option value="01">01</option>
          <option value="02">02</option>
          <option value="03">03</option>
          <option value="04">04</option>
          <option value="05">05</option>
          <option value="06">06</option>
          <option value="07">07</option>
          <option value="08">08</option>
          <option value="09">09</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
        </select>
        <select name="vExpYear" id="vExpYear" class="validate[required] profile-select-n1">           
          <option value=""> -- Select -- </option>
          <option value="2014">2014</option>
          <option value="2015">2015</option>
          <option value="2016">2016</option>
          <option value="2017">2017</option>
          <option value="2018">2018</option>
          <option value="2019">2019</option>
          <option value="2020">2020</option>
          <option value="2021">2021</option>
          <option value="2022">2022</option>
          <option value="2023">2023</option>
          <option value="2024">2024</option>
          <option value="2025">2025</option>
        </select>
        </span>-->
        <span class="sav-but-pro1"><a href="javascript:void(0);" onClick="checkpayment();"><?php if (@PAYMENT_OPTION=='PayPal'){?><?php echo @LBL_PAY_NOW1;?>
<?php }else{ ?>Book Now<?php }?></a></span> 
      </div>
      </form>
    </div>
 <?php $_template = new Smarty_Internal_Template("left.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
     </div>
  <div style="clear:both;"></div>
</div>

  <script>
    var	site_url = '<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
';
    function get_sate(country){
      $("#state").html('Wait...');  
      var request = $.ajax({  
    	  type: "POST",
    	  url: site_url+'index.php?file=m-get_state',  
    	  data: "country="+country, 	  
    	  
    	  success: function(data) { 
    		  $("#state").html(data);
          $('#vState').val('<?php echo $_smarty_tpl->getVariable('db_booker_details')->value[0]['vState'];?>
');        			
    		}
    	});
    	
    	request.fail(function(jqXHR, textStatus) {
    	  alert( "Request failed: " + textStatus ); 
    	});   	
    }
    
    get_sate('<?php echo $_smarty_tpl->getVariable('db_booker_details')->value[0]['vCountry'];?>
');
    
    function checkpayment(){ 
		resp = jQuery("#frmpayment").validationEngine('validate');    	 
    	if(resp == true){
			var checked = 0;
			if($("#DocumentPrice").attr("checked") == 'checked')
				checked = 1;
			if($("#BoxPrice").attr("checked") == 'checked')
				checked = 1;
			if($("#LuggagePrice").attr("checked") == 'checked')
				checked = 1;
			
			if(checked == 1)
				document.frmpayment.submit();
			else
			{
				alert('Please select at least one item');
				return false;
			}
			
    	}else{
    		return false;
    	}		
    }
    
    function change_seats(val){ 
      $('#iNoOfSeats').val(val);
      var price = $('#price').val();
      var commision = $('#commission').val(); 
      var vat = $('#vat').val(); 
      var seats = val;
      
      var ride_price = price * seats;
      ride_price = parseFloat(ride_price).toFixed(2); 
      var tot_commision = commision * seats;
      tot_commision = parseFloat(tot_commision).toFixed(2);
      var tot_vat = vat * seats;
      tot_vat = parseFloat(tot_vat).toFixed(2);
  
      
      var tot_price = parseFloat(ride_price) + parseFloat(tot_commision) + parseFloat(tot_vat);
      tot_price = parseFloat(tot_price).toFixed(2);
      
      var mainprice = $('#mainprice').val(); 
      rideprice = mainprice.split(' '); 
      $('#ride_price').html(rideprice[0]+' '+ride_price);
      
      var commisionprice = $('#commisionprice').val(); 
      commisionprice = commisionprice.split(' '); 
      $('#commision_price').html(commisionprice[0]+' '+tot_commision);
      
      var vatprice = $('#vatprice').val(); 
      vatprice = vatprice.split(' '); 
      $('#vat_price').html(vatprice[0]+' '+tot_vat);
      
      var totprice = $('#totprice').val();
      totprice = totprice.split(' '); 
      $('#tot_price').html(totprice[0]+' '+tot_price);
    } 
    /*function change_seats(val){ 
      $('#iNoOfSeats').val(val);
      var price = $('#price').val();
      var seats = val;
      
      var tot_price = price * seats; 
      
      var mainprice = $('#mainprice').val(); 
      
      mainprice = mainprice.split(' '); 
      
      $('#tot_price').html(mainprice[0]+' '+tot_price);           
    }     */
	function price_update(id, new_price){
	  var old_price = $('#price').val();
	 
	  var price = $('#price').val();
	  if(price == '')
		price = 0;
	  var id_name = $(id).attr("name");
	  
	  if($(id).attr("checked") == 'checked') {
		price =  parseFloat(price) + parseFloat(new_price);
		$('#price').val(price);
		$('#f'+id_name).val(new_price);
	  }
	  else {
		price =  parseFloat(price) - parseFloat(new_price);
		$('#price').val(price);
		$('#f'+id_name).val(0);
	  }
		
      var commision = $('#commission').val(); 
      var vat = $('#vat').val(); 
      var seats = 1;
      
      var ride_price = price * seats;
      ride_price = parseFloat(ride_price).toFixed(2); 
      var tot_commision = commision * seats;
      tot_commision = parseFloat(tot_commision).toFixed(2);
      var tot_vat = vat * seats;
      tot_vat = parseFloat(tot_vat).toFixed(2);
  
      
      var tot_price = parseFloat(ride_price) + parseFloat(tot_commision) + parseFloat(tot_vat);
      tot_price = parseFloat(tot_price).toFixed(2);
      
      var mainprice = $('#mainprice').val(); 
      rideprice = mainprice.split(' '); 
      $('#ride_price').html(rideprice[0]+' '+ride_price);
      
      var commisionprice = $('#commisionprice').val(); 
      commisionprice = commisionprice.split(' '); 
      $('#commision_price').html(commisionprice[0]+' '+tot_commision);
      
      var vatprice = $('#vatprice').val(); 
      vatprice = vatprice.split(' '); 
      $('#vat_price').html(vatprice[0]+' '+tot_vat);
      
      var totprice = $('#totprice').val();
      totprice = totprice.split(' '); 
      $('#tot_price').html(totprice[0]+' '+tot_price);
	  
	  $('#totamount').val(tot_price);
	}
  </script>
