<?php /* Smarty version Smarty-3.0.7, created on 2015-07-22 11:24:31
         compiled from "/home/www/xfetch/templates/content/view-contactus.tpl" */ ?>
<?php /*%%SmartyHeaderCode:61057123455af30172ae2a9-62251515%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '78bbbb9e212d3a02dca9e89393b3da2b20d38530' => 
    array (
      0 => '/home/www/xfetch/templates/content/view-contactus.tpl',
      1 => 1437401715,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '61057123455af30172ae2a9-62251515',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>

<script> 			
	function checkvalid(){
        //alert('hello..');
		resp = jQuery("#frmcontact").validationEngine('validate');
    //alert(resp);return false;
		if(resp == true){
			document.frmcontact.submit();
		}else{
			return false;
		}		
	}
	function redirectcancel()
  {  
      window.location="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
";
      return false;
  } 
</script>
 
<?php if ($_GET['var_msg']!=''){?>          
<?php if ($_GET['msg_code']=='1'){?>
  
<script>        
      showNotification({type : 'success', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});  
    </script>

<?php }?>
<?php }?> 

<?php if ($_GET['var_msg']!=''){?>          
<?php if ($_GET['msg_code']=='0'){?>
  
<script>
      showNotification({type : 'error', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});
    </script>

<?php }?>
<?php }?>
<?php if ($_smarty_tpl->getVariable('var_err_msg')->value!=''){?>          
  
<script>
      showNotification({type : 'error', message: '<?php echo $_smarty_tpl->getVariable('var_err_msg')->value;?>
'});
    </script>

<?php }?>
<div class="body-inner-part">
  <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_CONTACT_US;?>
</span></div>
  <div class="main-inner-page">
    <h2><?php echo @LBL_CONTACT;?>
<span><?php echo @LBL_US;?>
</span></h2>
    <div class="right-inner-part">
      <div class="contact-inner">
        <div class="contact-left">
          <h2><?php echo @LBL_ADDRESS;?>
</h2>
          <p><?php echo nl2br($_smarty_tpl->getVariable('COMPANY_ADDRESS')->value);?>
<br />
            <?php echo @LBL_PHONE;?>
 : &nbsp;<?php echo $_smarty_tpl->getVariable('SUPPORT_PHONE')->value;?>
<br />
            <?php echo @LBL_EMAIL;?>
 : &nbsp;<a href="mailto:<?php echo $_smarty_tpl->getVariable('SUPPORT_MAIL')->value;?>
"><?php echo $_smarty_tpl->getVariable('SUPPORT_MAIL')->value;?>
</a></p>
          <p class="map"><?php echo $_smarty_tpl->getVariable('GOOGLE_MAP')->value;?>
</p>
        </div>
        <form name="frmcontact" id="frmcontact" action="index.php?file=c-contactus_a" method="post">
          <div class="contact-right">
            <h2><?php echo @LBL_CONTACT_FORM;?>
</h2>
            <p><?php echo @LBL_CONTACT_FORM_DESC;?>
<span>* <?php echo @LBL_REQUERED_FIELD;?>
</span></p>
            <div class="form">
              <label><?php echo @LBL_NAME_SURNAME;?>
 : * </label>
              <br />
              <input class="validate[required] form-box" name="Data[vFirstName]" id="vFirstName" value="<?php echo $_smarty_tpl->getVariable('Data')->value['vFirstName'];?>
" type="text" />
              <label><?php echo @LBL_EMAIL;?>
 : * </label>
              <br />
              <input class="validate[required,custom[email]] form-box" name="Data[vEmail]" id="vEmail" value="<?php echo $_smarty_tpl->getVariable('Data')->value['vEmail'];?>
" type="text" />
              <label><?php echo @LBL_PHONE;?>
 / <?php echo @LBL_MOBILE;?>
 : * </label>
              <br />
              <input class="validate[required,custom[phone]] form-box" name="Data[cellno]" id="cellno" value="<?php echo $_smarty_tpl->getVariable('Data')->value['cellno'];?>
" type="text" />
              <!--<label><?php echo @LBL_SUBJECT;?>
 : * </label><br />
            <select name="Data[eSubject]" id="eSubject" class="validate[required] form-box-cont">
            <option><?php echo @LBL_GENERAL_INQUERY;?>
</option>
            <option><?php echo @LBL_CUST_SUPPORT;?>
</option>
            <option><?php echo @LBL_INVESTOR_RELATION;?>
</option>
            <option><?php echo @LBL_MARKETING;?>
</option>	
            </select>-->
              <label><?php echo @LBL_MESSAGE;?>
 : * </label>
              <br />
              <textarea class="validate[required] form-box1" name="Data[tSubject]" id="tSubject" cols="" rows=""></textarea>
              <span><a href="javascript:void(0);" onClick="javascript:checkvalid();return false;"><?php echo @LBL_SUBMIT;?>
</a></span> </div>
          </div>
        </form>
        <div style="clear:both;"></div>
      </div>
    </div>
    <?php $_template = new Smarty_Internal_Template("left.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?> </div>
  <div style="clear:both;"></div>
</div>
