<?php /* Smarty version Smarty-3.0.7, created on 2015-01-27 16:28:53
         compiled from "/home/www/blablaclone/templates/homeheader1.tpl" */ ?>
<?php /*%%SmartyHeaderCode:53653716154c76f6dd19e19-07459666%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7b8dee764a3697464bc785eaae2dc2a11050d1e0' => 
    array (
      0 => '/home/www/blablaclone/templates/homeheader1.tpl',
      1 => 1422015401,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '53653716154c76f6dd19e19-07459666',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!--For Slider-->
<link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_stylesheets'];?>
front/nivo-slider.css" type="text/css" media="screen" />
<script src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
modernizr.custom.67841.js" type="text/javascript"></script>
<script src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
css3-mediaqueries.js" type="text/javascript"></script>
<!--<script src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
jquery-1.7.1.min.js" type="text/javascript"></script>-->
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
jquery.nivo.slider.js"></script>

<script type="text/javascript">
$(document).ready(function() {
   $('#slider').nivoSlider({
     animSpeed: 700,
     //pauseTime: 7000
     directionNav: false,
     controlNav: true
   });
});
</script>

<div id="top-part">
  <div class="top-part-inner">
    <div class="logo"><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/logo.png" alt="" title="logo" /></a></div>
    <?php if ($_smarty_tpl->getVariable('sess_iMemberId')->value==''){?>
    <div class="top-right-part"> 
      <div class="ho-right-top">
      <div class="ride-ho">
        <ul>
          <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
find-ride" <?php if ($_smarty_tpl->getVariable('sess_lang')->value=='LV'||$_smarty_tpl->getVariable('sess_lang')->value=='LT'){?> style="font-size:11px;"<?php }?>><?php echo @LBL_FIND_A_RIDE;?>
</a></li>
          <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
offer-ride" <?php if ($_smarty_tpl->getVariable('sess_lang')->value=='LV'||$_smarty_tpl->getVariable('sess_lang')->value=='LT'){?> style="font-size:11px;"<?php }?>><?php echo @LBL_OFFER_RIDE;?>
</a></li>
        </ul>
      </div>
      <div class="lag"><span><?php if ($_COOKIE['ride_locations_type']=='international'){?><a href="javascript:void(0);" onClick="chnage_ride_types('local');" style="color:#0094C8;"><?php echo @LBL_VIEW_LOCAL_RIDES;?>
</a> | <?php }else{ ?><a href="javascript:void(0);" onClick="chnage_ride_types('international');" style="color:#0094C8;"><?php echo @LBL_VIEW_INTERNATIONAL_RIDES;?>
</a> | <?php }?><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
sign-in"><?php echo @LBL_LOG_IN;?>
</a>|<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
sign-up"><?php echo @LBL_REGISTER;?>
</a></span> <em>
        <select name="sess_language" id="sess_language" onChange="change_lang(this.value);">
          <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['name'] = "lngrb";
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_lng_mst')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['total']);
?>
          <option value="<?php echo $_smarty_tpl->getVariable('db_lng_mst')->value[$_smarty_tpl->getVariable('smarty')->value['section']['lngrb']['index']]['vCode'];?>
" <?php if ($_smarty_tpl->getVariable('sess_lang')->value==$_smarty_tpl->getVariable('db_lng_mst')->value[$_smarty_tpl->getVariable('smarty')->value['section']['lngrb']['index']]['vCode']){?> selected <?php }?>><?php echo $_smarty_tpl->getVariable('db_lng_mst')->value[$_smarty_tpl->getVariable('smarty')->value['section']['lngrb']['index']]['vTitle'];?>
</option> 
          <?php endfor; endif; ?>
        </select>
         <select name="sess_price_ratio" id="sess_price_ratio" onChange="change_currency(this.value);">
        <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['name'] = "currrb";
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_curr_mst')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['total']);
?>
        <option value="<?php echo $_smarty_tpl->getVariable('db_curr_mst')->value[$_smarty_tpl->getVariable('smarty')->value['section']['currrb']['index']]['vName'];?>
" <?php if ($_smarty_tpl->getVariable('sess_price_ratio')->value==$_smarty_tpl->getVariable('db_curr_mst')->value[$_smarty_tpl->getVariable('smarty')->value['section']['currrb']['index']]['vName']){?> selected <?php }?>><?php echo $_smarty_tpl->getVariable('db_curr_mst')->value[$_smarty_tpl->getVariable('smarty')->value['section']['currrb']['index']]['vName'];?>
</option>
        <?php endfor; endif; ?>
        </select>        
        </em>
      </div>
      <div class="social-icon">
        <a href="<?php echo $_smarty_tpl->getVariable('FACEBOOK_LINK')->value;?>
" target="_blank"><img onmouseout="this.src='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/fb.png'" onmouseover="this.src='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/fb-hover.png'" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/fb.png" alt=""/></a> 
        <a href="<?php echo $_smarty_tpl->getVariable('TWITTER_LINK')->value;?>
" target="_blank"><img onmouseout="this.src='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/twi.png'" onmouseover="this.src='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/twi-hover.png'" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/twi.png" alt=""/></a>
    </div>
    <div style="clear:both;"></div>
    </div>
    <?php }else{ ?>
    <div class="top-right-part2">
    <div class="top-div-iocn">
    <div class="top-icons">
    <ul>
      <?php if ($_SESSION['tot_unread']>=1){?>
      <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
received-messages"><strong><?php echo $_SESSION['tot_unread'];?>
</strong><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/message_notification_new.png"></a></li>
      <?php }else{ ?>
      <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
received-messages"><strong>&nbsp;</strong><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/message_notification.png"></a></li>
      <?php }?>        
      <?php if ($_SESSION['tot_feat_booking']>=1){?>
      <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
my-bookings"><strong><?php echo $_SESSION['tot_feat_booking'];?>
</strong><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/booking_notification_new.png"></a></li>
      <?php }else{ ?>
      <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
my-bookings"><strong>&nbsp;</strong><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/booking_notification.png"></a></li>
      <?php }?>       
    </ul>
          </div>
        <div class="user-part-main">  
          <div class="user-part"> <img src="<?php echo $_smarty_tpl->getVariable('member_image')->value;?>
" alt="" title="logo"/>
            <h2><?php echo $_smarty_tpl->getVariable('sess_vFirstName')->value;?>
</h2>
            <div class="drop-menu">
            <p><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
my-account"><?php echo @LBL_MY_ACCOUNT;?>
 <img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/white-point.png" alt="" /></a></p>
            <ul class="sub-menu">
        			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
my-account"><?php echo @LBL_DASHBOARD;?>
</a></li>
        			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
List-Rides-Offer"><?php echo @LBL_RIDES_OFFERED1;?>
</a></li>
        			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
Email-Alerts"><?php echo @LBL_EMAIL_ALERTS;?>
</a></li>
        			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
received-messages"><?php echo @LBL_MESSAGES;?>
</a></li>
        			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
leave-Rating"><?php echo @LBL_RATINGS;?>
</a></li>
        			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
personal-information"><?php echo @LBL_PROFILE;?>
</a></li>
        			<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
logout"><?php echo @LBL_LOGOUT;?>
</a></li>
        		</ul>
        		</div>
            <p><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
logout"><?php echo @LBL_LOGOUT;?>
</a></p>
            <div style="clear:both;"></div>
          </div>
        </div>
        </div>
      <div class="top-right-lag">  
        <div class="lag"><span><?php if ($_COOKIE['ride_locations_type']=='international'){?><a href="javascript:void(0);" onClick="chnage_ride_types('local');" style="color:#0094C8;"><?php echo @LBL_VIEW_LOCAL_RIDES;?>
</a> | <?php }else{ ?><a href="javascript:void(0);" onClick="chnage_ride_types('international');" style="color:#0094C8;"><?php echo @LBL_VIEW_INTERNATIONAL_RIDES;?>
</a> | <?php }?>
          </span><?php if ($_smarty_tpl->getVariable('script')->value!='offer_ride_second_edit'&&$_smarty_tpl->getVariable('script')->value!='offer_ride_edit'){?>
          <em> 
        <select name="sess_language" id="sess_language" onChange="change_lang(this.value);">
          <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['name'] = "lngrb";
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_lng_mst')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['total']);
?>
          <option value="<?php echo $_smarty_tpl->getVariable('db_lng_mst')->value[$_smarty_tpl->getVariable('smarty')->value['section']['lngrb']['index']]['vCode'];?>
" <?php if ($_smarty_tpl->getVariable('sess_lang')->value==$_smarty_tpl->getVariable('db_lng_mst')->value[$_smarty_tpl->getVariable('smarty')->value['section']['lngrb']['index']]['vCode']){?> selected <?php }?>><?php echo $_smarty_tpl->getVariable('db_lng_mst')->value[$_smarty_tpl->getVariable('smarty')->value['section']['lngrb']['index']]['vTitle'];?>
</option> 
          <?php endfor; endif; ?>
        </select>
         <select name="sess_price_ratio" id="sess_price_ratio" onChange="change_currency(this.value);">
        <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['name'] = "currrb";
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_curr_mst')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['total']);
?>
        <option value="<?php echo $_smarty_tpl->getVariable('db_curr_mst')->value[$_smarty_tpl->getVariable('smarty')->value['section']['currrb']['index']]['vName'];?>
" <?php if ($_smarty_tpl->getVariable('sess_price_ratio')->value==$_smarty_tpl->getVariable('db_curr_mst')->value[$_smarty_tpl->getVariable('smarty')->value['section']['currrb']['index']]['vName']){?> selected <?php }?>><?php echo $_smarty_tpl->getVariable('db_curr_mst')->value[$_smarty_tpl->getVariable('smarty')->value['section']['currrb']['index']]['vName'];?>
</option>
        <?php endfor; endif; ?>
        </select></em><?php }?>   
          </div>
         <div class="social-icon">
          <a href="<?php echo $_smarty_tpl->getVariable('FACEBOOK_LINK')->value;?>
" target="_blank"><img onmouseout="this.src='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/fb.png'" onmouseover="this.src='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/fb-hover.png'" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/fb.png" alt=""/></a>
          <a href="<?php echo $_smarty_tpl->getVariable('TWITTER_LINK')->value;?>
" target="_blank"><img onmouseout="this.src='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/twi.png'" onmouseover="this.src='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/twi-hover.png'" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/twi.png" alt=""/></a>
        </div> 
      </div>
      </div>
    <?php }?>
  <div style="clear:both;"></div>
  </div>
  <div style="clear:both;"></div>
</div>
</div>
<div class="navgaction-main">
  <div class="menu-top">
    <div class="navigation"><span><a class="toggleMenu" href="#"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/menu-hide.png"></a></span>      
     <ul class="nav">
        <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
" <?php if ($_smarty_tpl->getVariable('script')->value=='home1'){?>class="active"<?php }?>><?php echo @LBL_HOME;?>
</a></li>
        <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
find-ride" <?php if ($_smarty_tpl->getVariable('script')->value=='find_ride'){?>class="active"<?php }?>><?php echo @LBL_FIND_RIDE;?>
</a></li>
        <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
offer-ride" <?php if ($_smarty_tpl->getVariable('script')->value=='offer_ride'){?>class="active"<?php }?>><?php echo @LBL_OFFER_RIDE;?>
</a></li>
        <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
blog"><?php echo @LBL_BLOGS;?>
</a></li>
        <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
faqs" <?php if ($_smarty_tpl->getVariable('script')->value=='faqs'){?>class="active"<?php }?>><?php echo @LBL_HELP;?>
</a></li>
        <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
aboutus" <?php if ($_smarty_tpl->getVariable('script')->value=='page'&&$_smarty_tpl->getVariable('iPageId')->value=='1'){?>class="active"<?php }?>><?php echo @LBL_ABOUT_US;?>
 </a></li>
        <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
contactus" class="last <?php if ($_smarty_tpl->getVariable('script')->value=='contactus'){?>active<?php }?>"><?php echo @LBL_CONTACT_US;?>
</a></li>
      </ul>
  </div>
    <!--<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
script.js"></script>-->
  	</div>
<div id="header">
<div class="slider-wrapper theme-default">
  <div id="slider"> 
     <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_banners')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
      <?php if ($_smarty_tpl->getVariable('db_banners')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vImage']!=''){?>
      <img src="<?php echo $_smarty_tpl->getVariable('db_banners')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vImage'];?>
" data-thumb="<?php echo $_smarty_tpl->getVariable('db_banners')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vImage'];?>
" alt="" data-transition="slideInLeft"/><?php }?>
      <?php endfor; endif; ?>
  </div>
</div>
</div>

  <script>
    function change_lang(lang){
      window.location = '<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?lang='+lang; 
    }
    
    function change_currency(currency){
      window.location = '<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?currency='+currency; 
    }
    
    
  </script>

