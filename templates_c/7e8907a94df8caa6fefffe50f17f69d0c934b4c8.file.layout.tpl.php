<?php /* Smarty version Smarty-3.0.7, created on 2016-02-15 04:04:08
         compiled from "/home4/shipcliq/public_html/templates/layout.tpl" */ ?>
<?php /*%%SmartyHeaderCode:81116141756c1a298035303-10586260%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7e8907a94df8caa6fefffe50f17f69d0c934b4c8' => 
    array (
      0 => '/home4/shipcliq/public_html/templates/layout.tpl',
      1 => 1455529938,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '81116141756c1a298035303-10586260',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<title><?php echo $_smarty_tpl->getVariable('generalobj')->value->setMeta('title',$_smarty_tpl->getVariable('script')->value,$_smarty_tpl->getVariable('iPageId')->value);?>
</title>
		<meta name="keywords" content="<?php echo $_smarty_tpl->getVariable('generalobj')->value->setMeta('keyword',$_smarty_tpl->getVariable('script')->value,$_smarty_tpl->getVariable('iPageId')->value);?>
" />
		<meta name="description" content="<?php echo $_smarty_tpl->getVariable('generalobj')->value->setMeta('desc',$_smarty_tpl->getVariable('script')->value,$_smarty_tpl->getVariable('iPageId')->value);?>
" />
		<link rel="icon" href="<?php echo $_smarty_tpl->getVariable('Faviconlogodis')->value;?>
" type="image/x-icon" />
		<link rel="shortcut icon" href="<?php echo $_smarty_tpl->getVariable('Faviconlogodis')->value;?>
" type="image/x-icon">
		<link rel="apple-touch-icon" href="<?php echo $_smarty_tpl->getVariable('Faviconlogodis')->value;?>
">
		<!--<link rel="icon" type="image/vnd.microsoft.icon" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
favicon.ico" />-->
		<meta name="keywords" content="<?php echo $_smarty_tpl->getVariable('DEFAULT_META_KEYWORD')->value;?>
" />
		<meta name="description" content="<?php echo $_smarty_tpl->getVariable('DEFAULT_META_DESCRIPTION')->value;?>
" />
		<base href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"  />
		<script>var site_url = '<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
';</script>
		<?php if ($_smarty_tpl->getVariable('homepage')->value=='Yes'){?>
		<link href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_stylesheets'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/home/home.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_stylesheets'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/home/<?php echo $_smarty_tpl->getVariable('THEMECSS')->value;?>
.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_stylesheets'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/home/homemedia.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_stylesheets'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/home/homemenu.css" rel="stylesheet" type="text/css" />
		
		<!----------------- HOME LANGUAGE CSS ------------------------>
		<link href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_stylesheets'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/<?php echo $_smarty_tpl->getVariable('sess_lang')->value;?>
/home.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_stylesheets'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/<?php echo $_smarty_tpl->getVariable('sess_lang')->value;?>
/homemedia.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_stylesheets'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/<?php echo $_smarty_tpl->getVariable('sess_lang')->value;?>
/homemenu.css" rel="stylesheet" type="text/css" />
		<!----------------- /HOME LANGUAGE CSS ------------------------>
		
		<?php }else{ ?>
		<link href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_stylesheets'];?>
front/style.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_stylesheets'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/home/<?php echo $_smarty_tpl->getVariable('THEMECSS')->value;?>
.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_stylesheets'];?>
front/media.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_stylesheets'];?>
front/menu.css" rel="stylesheet" type="text/css" />
		
		<!----------------- INNER LANGUAGE CSS ------------------------>
		<link href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_stylesheets'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/<?php echo $_smarty_tpl->getVariable('sess_lang')->value;?>
/style.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_stylesheets'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/<?php echo $_smarty_tpl->getVariable('sess_lang')->value;?>
/media.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_stylesheets'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/<?php echo $_smarty_tpl->getVariable('sess_lang')->value;?>
/menu.css" rel="stylesheet" type="text/css" />
		<!----------------- /INNER LANGUAGE CSS ------------------------>
		
		<?php }?>
		<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
jquery-1.7.1.min.js"></script>
		<link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_stylesheets'];?>
front/validationEngine.jquery.css" type="text/css"/> 
		<script src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script> 
		<script src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>   
		<link href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
notifications/css/jquery_notification.css" type="text/css" rel="stylesheet"/>
		<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
notifications/js/jquery_notification_v.1.js"></script>
		<script src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
ajax_newsletter.js"></script>
		<script src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
script.js"></script>  
		<?php if ($_smarty_tpl->getVariable('SHOW_COOKIE_POLICY')->value=='Yes'){?>
		
		<script>
			var cookietext = '<?php echo @LBL_COOKIE_TEXT;?>
';
			var viewcookie = '<?php echo @LBL_VIEW_COOKIE;?>
';
			var cookieaccept = '<?php echo @LBL_ACCEPT_COOKIE;?>
';
		</script>
		
		<link href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_stylesheets'];?>
front/twc.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
cookies.js"></script>
		<!--<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
TWCcookies.js"></script>-->
		<?php }?>
		<style type="text/css">
			
			#rides-content{background:url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_upload_footer_logo'];?>
1_<?php echo $_smarty_tpl->getVariable('RIDE_BACKGROUND')->value;?>
);
			background-repeat:no-repeat;
			height:100%;
			}
			
		</style>
		 
		<script>
			/*(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
				
				ga('create', 'UA-49621023-1', 'blablacarscript.com');
			ga('send', 'pageview');*/
		</script>
		
	</head>
	<body>
	
		<?php if ($_smarty_tpl->getVariable('homepage')->value=='Yes'){?>
		<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('include_template')->value), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
		<?php }else{ ?>
		<div id="main">
			<?php $_template = new Smarty_Internal_Template("innertop.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?> 
			<div id="main-div-top-bg">
				<div id="main-div-top-second-bg">
					<div id="main-div-top-second-bg-new-in">
						<?php $_template = new Smarty_Internal_Template("header.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>   
						<div id="body-content">
							<!--<div class="site-banner-main2">
							<div class="sitebanner-left-img2"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
banner-left1.jpg" /></div>-->
							
							<?php $_template = new Smarty_Internal_Template(($_smarty_tpl->getVariable('include_template')->value), $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
							<!--<div class="sitebanner-right-img2"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
banner-left1.jpg" /></div>
							</div>-->
							<?php $_template = new Smarty_Internal_Template("footer4.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?> 
						</div>
                     <div style="clear:both;"></div>
					</div>
					<div style="clear:both;"></div>
				</div>
				
				<div style="clear:both;"></div>
			</div>
			
			<div style="clear:both;"></div>
		</div>
		<?php }?>
		<?php echo $_smarty_tpl->getVariable('GOOGLE_ANALYTICS_CODE')->value;?>

	</body>
</html>
