<?php /* Smarty version Smarty-3.0.7, created on 2015-05-25 14:39:23
         compiled from "/home/www/xfetch/templates/members/view-changepassword.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11799287295562e6c3c01144-26695608%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '82969106681fed70846e02610938fa0b47e5b5a8' => 
    array (
      0 => '/home/www/xfetch/templates/members/view-changepassword.tpl',
      1 => 1395812666,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11799287295562e6c3c01144-26695608',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_GET['var_msg']!=''){?>          
<?php if ($_GET['msg_code']=='1'){?>
  
    <script>        
      showNotification({type : 'success', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});  
    </script>
  
<?php }?>
<?php }?> 

<?php if ($_GET['var_msg']!=''){?>          
<?php if ($_GET['msg_code']=='0'){?>
  
    <script>
      showNotification({type : 'error', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});
    </script>
  
<?php }?>
<?php }?> 
<div class="body-inner-part">
  <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_CHANGE_PASSWORD;?>
</span></div>
  <!-------------------------inner-page----------------->
  <div class="main-inner-page">
    <h2><?php echo @LBL_CHANGE_PASSWORD;?>
</h2>
    <?php $_template = new Smarty_Internal_Template("member_top.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
    <?php $_template = new Smarty_Internal_Template("member_profile_left.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
    <div class="ratings">
      <div class="profile">
        <h2><?php echo @LBL_CHANGE_PASSWORD;?>
 </h2>
        <form name="frmchangepassword" id="frmchangepassword" method="post" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=m-changepassword">
        <span><em> <?php echo @LBL_OLD;?>
 <?php echo @LBL_PASSWORD;?>
</em>
        <input class="validate[required,minSize[6]] profile-input" name="vOldPassword" id="vOldPassword" type="password" value=""/>
        </span>
        <span><em> <?php echo @LBL_NEW;?>
 <?php echo @LBL_PASSWORD;?>
</em>
        <input class="validate[required,minSize[6]] profile-input" name="vPassword" id="vPassword" type="password" value=""/>
        </span> 
        <span><em> <?php echo @LBL_RETYPE;?>
 <?php echo @LBL_PASSWORD;?>
</em>
        <input class="validate[required,equals[vPassword]] profile-input" name="vPassword2" id="vPassword2" type="password" />
        </span>
         <span class="sav-but-pro"><a href="javascript:void(0);" onClick="javascript:checkvalid(); return false;"><?php echo @LBL_SUBMIT;?>
</a></span>
       </form> 
      </div>
    </div>
  </div>
  <!-------------------------inner-page end----------------->
  <div style="clear:both;"></div>
</div>
 
<script>
function checkvalid(){
    //alert('hello..');
    jQuery("#frmchangepassword").validationEngine('init',{scroll: false});
	  jQuery("#frmchangepassword").validationEngine('attach',{scroll: false});
		resp = jQuery("#frmchangepassword").validationEngine('validate');
	//	alert(resp); return false;
		if(resp == true){
			document.frmchangepassword.submit();
		}else{
			return false;
		}		
}
$('#frmchangepassword').bind('keydown',function(e){
    if(e.which == 13){
      checkvalid(); return false;
    }
});
</script> 
