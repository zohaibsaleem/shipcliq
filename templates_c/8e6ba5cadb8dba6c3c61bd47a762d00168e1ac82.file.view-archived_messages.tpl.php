<?php /* Smarty version Smarty-3.0.7, created on 2016-02-11 00:02:06
         compiled from "/home4/shipcliq/public_html/templates/members/view-archived_messages.tpl" */ ?>
<?php /*%%SmartyHeaderCode:36771905556bc23de06e570-11098334%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8e6ba5cadb8dba6c3c61bd47a762d00168e1ac82' => 
    array (
      0 => '/home4/shipcliq/public_html/templates/members/view-archived_messages.tpl',
      1 => 1455170522,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '36771905556bc23de06e570-11098334',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_truncate')) include '/home4/shipcliq/public_html/libraries/general/license/plugins/modifier.truncate.php';
?><div class="body-inner-part">
  <!--<div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @Home;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_MESSAGES;?>
</span></div>-->
  <div class="main-inner-page">
    <h2><?php echo @LBL_MESSAGES;?>
</h2>
    <div class="dashbord">
      <?php $_template = new Smarty_Internal_Template("member_top.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
      <div class="bro-message">
        <div class="tab-pane">
          <ul>
            <!--<li> <a href="#"> Questions &amp; Answers </a> </li>-->
            <li> <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsute_url'];?>
received-messages"> <?php echo @LBL_RECEIVED_MESSAGES;?>
 <em><?php echo $_smarty_tpl->getVariable('tot_unread')->value;?>
</em> </a> </li>
            <li> <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsute_url'];?>
sent-messages"> <?php echo @LBL_SENT_MESSAGES;?>
 </a> </li>
            <li> <a class="last active" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsute_url'];?>
archived-messages" > <?php echo @LBL_ARCHIVED_MESSAGES;?>
</a> </li>
          </ul>
        </div>
        <div class="messages-container">
          <?php if (count($_smarty_tpl->getVariable('db_messages')->value)>0){?>
          <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['name'] = "messages";
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_messages')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['total']);
?>
          <label <?php if ($_smarty_tpl->getVariable('db_messages')->value[$_smarty_tpl->getVariable('smarty')->value['section']['messages']['index']]['eStatus']=='Unread'){?>style="font-weight: bold;"<?php }?>>
            <span>
              <img src="<?php echo $_smarty_tpl->getVariable('db_messages')->value[$_smarty_tpl->getVariable('smarty')->value['section']['messages']['index']]['img'];?>
" alt="" style="height:70px;width:70px;"/><?php echo $_smarty_tpl->getVariable('db_messages')->value[$_smarty_tpl->getVariable('smarty')->value['section']['messages']['index']]['vFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_messages')->value[$_smarty_tpl->getVariable('smarty')->value['section']['messages']['index']]['vLastName'];?>
</span>
            <span class="stap2"><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
message-details/received/<?php echo $_smarty_tpl->getVariable('db_messages')->value[$_smarty_tpl->getVariable('smarty')->value['section']['messages']['index']]['iMessageId'];?>
"><?php echo smarty_modifier_truncate(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->getVariable('db_messages')->value[$_smarty_tpl->getVariable('smarty')->value['section']['messages']['index']]['tMessage']),50);?>
</a></span> 
            <span class="stap3"><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTimeFormat($_smarty_tpl->getVariable('db_messages')->value[$_smarty_tpl->getVariable('smarty')->value['section']['messages']['index']]['dAddedDate']);?>

              <!--<a href="javascript:void(0);" title="Archived" onClick="archived_messages(<?php echo $_smarty_tpl->getVariable('db_messages')->value[$_smarty_tpl->getVariable('smarty')->value['section']['messages']['index']]['iMessageId'];?>
);"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
mail-delete-archive.png" alt="" /></a>-->
            </span> 
          </label>
          <?php endfor; endif; ?>
          <?php }else{ ?>
          <div class="main-block">
          <?php echo @LBL_NO;?>
 <?php echo @LBL_ARCHIVED_MESSAGES;?>
</div>
          <?php }?>
        </div>
        <?php if (count($_smarty_tpl->getVariable('db_messages')->value)>0){?>
        <div class="paging"><?php echo $_smarty_tpl->getVariable('page_link')->value;?>
</div>
        <?php }?>
      </div>
      <div style="clear:both;"></div>
    </div>
    <div style="clear:both;"></div>
  </div>
  <div style="clear:both;"></div>
</div>

  <script>
    function archived_messages(id){
      var r=confirm("<?php echo @LBL_SURE_DEL_MSG;?>
");
      if (r==true)
      {
        window.location="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=m-received_messages&action=archived&id="+id;
        return false;
      }
      else
      {
        return false;
      }
    }
  </script>
