<?php /* Smarty version Smarty-3.0.7, created on 2015-07-18 16:49:00
         compiled from "/home/www/xfetch/templates/members/view-edit_profile.tpl" */ ?>
<?php /*%%SmartyHeaderCode:181841946055aa3624d40f34-59289094%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '90eb5826a42a46ff92e9b3f81adf1b9fb9c26dcc' => 
    array (
      0 => '/home/www/xfetch/templates/members/view-edit_profile.tpl',
      1 => 1436784785,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '181841946055aa3624d40f34-59289094',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_GET['var_msg']!=''){?>          
<?php if ($_GET['msg_code']=='1'){?>
  
    <script>        
      showNotification({type : 'success', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});  
    </script>
  
<?php }?>
<?php }?> 

<?php if ($_GET['var_msg']!=''){?>          
<?php if ($_GET['msg_code']=='0'){?>
  
    <script>
      showNotification({type : 'error', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});
    </script>
  
<?php }?>
<?php }?> 

<?php if ($_smarty_tpl->getVariable('duplicate')->value=='1'){?>          
<?php if ($_smarty_tpl->getVariable('var_msg')->value!=''){?>
  
    <script>
      showNotification({type : 'error', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});
    </script>
  
<?php }?>
<?php }?> 

  <div class="body-inner-part">
      <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_PROFILE;?>
</span></div>
      <div class="main-inner-page">
        <h2><?php echo @LBL_PROFILE;?>
</h2>
        <?php $_template = new Smarty_Internal_Template("member_top.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
        <div class="ratings">
          <div class="profile profile-edit">
            <h2><?php echo @LBL_MY_PERSONAL_INFO;?>
 </h2>
            <form name="frmprofile" enctype="multipart/form-data" id="frmprofile" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=m-edit_profile_a" method="post">
            <span class="edit-p"><em> <?php echo @LBL_GENDER;?>
</em>
              <input type="radio" name="Data[eGender]" id="eGender" value="Male" <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['eGender']=='Male'){?>checked="true"<?php }?> /><?php echo @LBL_MALE;?>

              <input type="radio" name="Data[eGender]" id="eGender" value="Female" <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['eGender']=='Female'){?>checked="true"<?php }?> /><?php echo @LBL_FEMALE;?>

            </span>
            <span><em> <?php echo @LBL_FIRST_NAME;?>
 *</em>
            <input name="Data[vFirstName]" id="vFirstName" type="text" class="validate[required] profile-input" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vFirstName'];?>
" />
            </span>
            <span><em> <?php echo @LBL_LAST_NAME;?>
 *</em>
            <input name="Data[vLastName]" id="vLastName" type="text" class="validate[required] profile-input" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vLastName'];?>
"/>
            </span> 
            <span><em> <?php echo @LBL_NICK_NAME;?>
</em>
            <input name="Data[vNickName]" id="vNickName" type="text" class="profile-input" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vNickName'];?>
"/>
            </span>
            <span><em> <?php echo @LBL_EMAIL;?>
 *</em>
            <input name="Data[vEmail]" id="vEmail" type="text" class="validate[required,custom[email]] profile-input" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vEmail'];?>
" />
            </span>                          
            <span><em> <?php echo @LBL_MOBILE;?>
 <?php echo @LBL_PHONE;?>
 *</em>
           <select name="Data[vCountry]" id="vCountry" class="validate[required] profile-select1" onChange="get_sate(this.value);">
              <option value=""><?php echo @LBL_SELECT_COUNTRY;?>
</option>
             <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_country')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
                <option value="<?php echo $_smarty_tpl->getVariable('db_country')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vCountryCode'];?>
" <?php if ($_smarty_tpl->getVariable('db_country')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vCountryCode']==$_smarty_tpl->getVariable('db_member')->value[0]['vCountry']){?> selected <?php }?>><?php echo $_smarty_tpl->getVariable('db_country')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vCountry'];?>
</option>
             <?php endfor; endif; ?>
            </select> 
            
            &nbsp;&nbsp;
            <input name="Data[vPhone]" id="vPhone" type="text" class="validate[required,custom[phone]] profile-input1" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vPhone'];?>
" />
            <!--<p>
              <input name="Data[eShowPhoneOnline]" id="eShowPhoneOnline" type="checkbox" <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['eShowPhoneOnline']=='No'){?>checked="true"<?php }?> />
              &nbsp;&nbsp;<?php echo @LBL_NEVER_SHOW_PHONE;?>
 </p>-->
            </span> 
            <span><em> <?php echo @LBL_STATE;?>
 </em>
            <div id="state">
            <select name="vState" id="vState" class="validate[required] profile-select">
              <option value=""><?php echo @LBL_SELECT;?>
 <?php echo @LBL_STATE;?>
</option>              
            </select>
            </div>
            </span>
            <span><em><?php echo @LBL_ADDRESS;?>
 </em>
            <textarea name="Data[vAddress]" id="vAddress" cols="" rows="" class="profile-textarea"><?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vAddress'];?>
</textarea>
            </span>
            <span><em><?php echo @LBL_ZIP_CODE;?>
 </em>
            <input name="Data[vZip]" id="vZip" type="text" class="profile-input" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vZip'];?>
" />
            </span> 
            <span><em> <?php echo @LBL_BIRTH_YEAR;?>
 *</em>
            <?php echo $_smarty_tpl->getVariable('com_year_combo')->value;?>

            </span>
            <span><em> <?php echo @LBL_PREFERRED_LANG;?>
 </em>
            <select name="Data[vLanguageCode]" id="vLanguageCode" class="validate[required] profile-select">
              <option value="EN" <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['vLanguageCode']=='EN'){?> selected <?php }?>><?php echo @LBL_ENGLISH;?>
</option>
              <option value="DN" <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['vLanguageCode']=='DN'){?> selected <?php }?>><?php echo @LBL_DANISH;?>
</option>
              <option value="FI" <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['vLanguageCode']=='FI'){?> selected <?php }?>><?php echo @LBL_FINISH;?>
</option>
              <option value="FN" <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['vLanguageCode']=='FN'){?> selected <?php }?>><?php echo @LBL_FRENCH;?>
</option>
              <option value="LV" <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['vLanguageCode']=='LV'){?> selected <?php }?>><?php echo @LBL_LATVIN;?>
</option>
              <option value="EE" <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['vLanguageCode']=='EE'){?> selected <?php }?>><?php echo @LBL_ESTONIAN;?>
</option>
              <option value="LT" <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['vLanguageCode']=='LT'){?> selected <?php }?>><?php echo @LBL_LITHUNIAN;?>
</option>
              <option value="DE" <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['vLanguageCode']=='DE'){?> selected <?php }?>><?php echo @LBL_GERMAN;?>
</option>
              <option value="NO" <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['vLanguageCode']=='NO'){?> selected <?php }?>><?php echo @LBL_NORWAY;?>
</option>
              <option value="PO" <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['vLanguageCode']=='PO'){?> selected <?php }?>><?php echo @LBL_POLISH;?>
</option>
              <option value="RS" <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['vLanguageCode']=='RS'){?> selected <?php }?>><?php echo @LBL_RUSSIAN;?>
</option>
              <option value="ES" <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['vLanguageCode']=='ES'){?> selected <?php }?>><?php echo @LBL_SPANISH;?>
</option>
              <option value="SW" <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['vLanguageCode']=='SW'){?> selected <?php }?>><?php echo @LBL_SWEDISH;?>
</option>
            </select>
            </span>
            <span><em> <?php echo @LBL_QUIK_DESCRIPTION;?>
</em>
            <textarea name="Data[tDescription]" id="tDescription" cols="" rows="" class="profile-textarea"><?php echo $_smarty_tpl->getVariable('db_member')->value[0]['tDescription'];?>
</textarea>
            <p><?php echo @LBL_PROFILE_NOTE;?>
</p>
            </span>
            <span><em><?php echo @LBL_NEWS_LETTER_EDIT;?>
</em>
            <input type="checkbox" id="newsletter" name="newsletter" <?php if ($_smarty_tpl->getVariable('totalnewsletter')->value!=0){?> checked <?php }?>>
            </span>
            <?php if (@PAYMENT_OPTION=='PayPal'){?>
            <h2><?php echo @LBL_PAYMENT_DETAILS;?>
</h2>
            <p class="memberpaydtls" style="background:#44AA00;color:#fff;padding:5px;width:auto;"><?php echo @LBL_PAYMENT_INFO;?>
</p>
            <span class="edit-p"><em> <?php echo @LBL_MEMBER_TYPE;?>
 </em>
              <input type="radio" name="Data[eMemberType]" id="Passenger" value="Passenger" <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['eMemberType']=='Passenger'){?>checked="true"<?php }?> onclick="checkMeberType(this.value);" /><?php echo @LBL_PASSENGER;?>

              <input type="radio" name="Data[eMemberType]" id="Driver" value="Driver" <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['eMemberType']=='Driver'){?>checked="true"<?php }?> onclick="checkMeberType(this.value);" /><?php echo @LBL_DRIVER1;?>

              <input type="radio" name="Data[eMemberType]" id="Both" value="Both" <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['eMemberType']=='Both'){?>checked="true"<?php }?> onclick="checkMeberType(this.value);" /><?php echo @LBL_BOTH;?>

            </span>
            <span id="paymail">
				<em> <?php echo @LBL_PAYMENT_EMAIL;?>
 </em>
				<input name="vPaymentEmail" id="vPaymentEmail" type="email" class="validate[required,custom[email]] profile-input" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vPaymentEmail'];?>
" />
				<p><a href="https://www.paypal.com/" target="_blank"><?php echo @LBL_CREATE_PAYPAL_ACCOUNT;?>
</a></p>
            </span>
			<span id="paymail1">
				<em> <?php echo @LBL_PAYMENT_EMAIL;?>
 </em>
				<input name="vPaymentEmail1" id="vPaymentEmail1" type="email" class="validate[custom[email]] profile-input" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vPaymentEmail'];?>
" />
				<p><a href="https://www.paypal.com/" target="_blank"><?php echo @LBL_CREATE_PAYPAL_ACCOUNT;?>
</a></p>
            </span>
			
            
            <span><em> &nbsp; </em>
            <?php echo @LBL_OR;?>

            </span>
            <span><em><?php echo @LBL_ACC_HODLER;?>
 </em>
            <input name="Data[vBankAccountHolderName]" id="vBankAccountHolderName" type="text" class="profile-input" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vBankAccountHolderName'];?>
" />
            </span>
            <span><em><?php echo @LBL_ACC_NUMBER;?>
 (IBAN) </em>
            <input name="Data[vAccountNumber]" id="vAccountNumber" type="text" class="profile-input" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vAccountNumber'];?>
" />
            </span>
            <span><em><?php echo @LBL_NAME_BANK;?>
 </em>
            <input name="Data[vBankName]" id="vBankName" type="text" class="profile-input" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vBankName'];?>
" />
            </span>
            <span><em><?php echo @LBL_BANK_LOCATION;?>
 </em>
            <input name="Data[vBankLocation]" id="vBankLocation" type="text" class="profile-input" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vBankLocation'];?>
" />
            </span>
            <span><em><?php echo @LBL_BIC_SWIFT_CODE;?>
 </em>
            <input name="Data[vBIC_SWIFT_Code]" id="vBIC_SWIFT_Code" type="text" class="profile-input" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vBIC_SWIFT_Code'];?>
" />
            </span>
            <?php }?>            
            <span class="sav-but-pro"><a href="javascript:void(0);" onclick="javascript:checkprofile(); return false;"><?php echo @LBL_SUBMIT;?>
</a></span>
            </form>
           </div>
        </div>
		<?php $_template = new Smarty_Internal_Template("member_profile_left.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
	  </div>
      <!-------------------------inner-page end----------------->
      <div style="clear:both;"></div>
    </div>
 
<script>
function checkprofile(){
 resp = jQuery("#frmprofile").validationEngine('validate');// alert(resp);return false;
	if(resp == true){
		document.frmprofile.submit();
	}else{
		return false;
	}		
}
function checkMeberType(val)
{
	//alert(val);
	if(val == "Passenger")
	{
		document.getElementById('paymail').style.display = "none";
		document.getElementById('paymail1').style.display = "block";
	}
	else if(val=="Driver" || val=="Both")
	{
		document.getElementById('paymail').style.display = "block";
		document.getElementById('paymail1').style.display = "none";
	}
	
 /*if(val=="Driver" || val=="Both")
 {
    $('#vPaymentEmail').addClass('validate[required, custom[email]] profile-input');
    //$('#vPaymentEmail').removeClass('validate[required]'); 
 }
  else if(val=="Passenger")
  {
    $('#vPaymentEmail').addClass('validate[required]');
  }*/
}

var	site_url = '<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
';
    function get_sate(country){
      $("#state").html('Wait...');  
      var request = $.ajax({  
    	  type: "POST",
    	  url: site_url+'index.php?file=m-get_state',  
    	  data: "country="+country, 	  
    	  
    	  success: function(data) { 
    		  $("#state").html(data); 
          $('#vState').val('<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vState'];?>
');      			
    		}
    	});
    	
    	request.fail(function(jqXHR, textStatus) {
    	  alert( "Request failed: " + textStatus ); 
    	});   	
    }
    
    
</script>   
 
<?php if ($_smarty_tpl->getVariable('db_member')->value[0]['vCountry']!=''){?>

  <script>
     get_sate('<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vCountry'];?>
');
  </script>

<?php }?>
<?php if ($_smarty_tpl->getVariable('db_member')->value[0]['eMemberType']!=''){?>

  <script>
     checkMeberType('<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['eMemberType'];?>
');
  </script>

<?php }?>