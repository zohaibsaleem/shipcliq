<?php /* Smarty version Smarty-3.0.7, created on 2016-01-21 14:29:16
         compiled from "/home/www/shipcliq/templates/left.tpl" */ ?>
<?php /*%%SmartyHeaderCode:177015177456a0eb3ccf1834-55189104%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '99efd84e56442fc5421b2d4db8cc154c6e11e668' => 
    array (
      0 => '/home/www/shipcliq/templates/left.tpl',
      1 => 1435321018,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '177015177456a0eb3ccf1834-55189104',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="left-about-part">
  <div class="informaction">
    <h1><?php echo @LBL_INFORMATION;?>
</h1>
    <ul>
         <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_pages_1')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
		     <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
page/<?php echo $_smarty_tpl->getVariable('db_pages_1')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPageId'];?>
/<?php ob_start();?><?php echo $_smarty_tpl->getVariable('db_pages_1')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPageId'];?>
<?php $_tmp1=ob_get_clean();?><?php echo $_smarty_tpl->getVariable('generalobj')->value->getPageUrlName($_tmp1);?>
" ><?php ob_start();?><?php echo $_smarty_tpl->getVariable('db_pages_1')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPageId'];?>
<?php $_tmp2=ob_get_clean();?><?php echo $_smarty_tpl->getVariable('generalobj')->value->getPageTitle($_tmp2);?>
</a></li>
		  <?php endfor; endif; ?> 
	</ul>
  </div>
  <div class="informaction">
    <h1><?php echo @LBL_HELP;?>
</h1>
    <ul>
      <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
contactus" <?php if ($_smarty_tpl->getVariable('script')->value=='contactus'){?>class="active"<?php }?>><?php echo @LBL_CONTACT_US;?>
</a></li>
      <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
faqs" <?php if ($_smarty_tpl->getVariable('script')->value=='faqs'){?>class="active"<?php }?>><?php echo @LBL_FAQ;?>
</a></li>
       
    </ul>
  </div>
</div>
