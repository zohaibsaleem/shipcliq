<?php /* Smarty version Smarty-3.0.7, created on 2015-06-29 17:39:26
         compiled from "/home/www/xfetch/templates/members/view-list_rides_offer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1201205619559135767f5478-46367338%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9b3c392b6c08f392eca2fececb65f20580c5f11b' => 
    array (
      0 => '/home/www/xfetch/templates/members/view-list_rides_offer.tpl',
      1 => 1435318151,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1201205619559135767f5478-46367338',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_smarty_tpl->getVariable('var_msg')->value!=''){?>
  
    <script>        
      showNotification({type : 'success', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});  
    </script>
  
<?php }?>
<?php if ($_smarty_tpl->getVariable('var_err_msg')->value!=''){?>
  
    <script>        
      showNotification({type : 'error', message: "<?php echo $_smarty_tpl->getVariable('var_err_msg')->value;?>
"});  
    </script>
  
<?php }?>
<div class="body-inner-part">
  <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_RIDES_OFFERED1;?>
</span></div>
  <!-------------------------inner-page----------------->
  <div class="main-inner-page">
    <h2><?php echo @LBL_RIDES_OFFERED1;?>
</h2>
    <div class="dashbord">
      <?php $_template = new Smarty_Internal_Template("member_top.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
      <div class="rides-offered">
        <div class="tab-pane">
        <ul>                         
        <li><a href="javascript:void(0);" id="Avail" onClick="show_hide('available');" class="active"><?php echo @LBL_AVAILABLE_RIDE;?>
</a></li> 
        <li><a href="javascript:void(0);" id="Past" onClick="show_hide('past');" class="last"><?php echo @LBL_PAST_RIDE;?>
</a></li>
        </ul>
        </div>
        <div id="Avail_ride" style="display:;">
        <?php if ($_smarty_tpl->getVariable('avail_count')->value>0){?>
        <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['name'] = "rideavail";
$_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_ride_list')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['total']);
?>
        <?php if ($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['type']=='Available'){?>
        <div class="main-block">
          <h2><?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['vMainDeparture'];?>
 &rarr; <?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['vMainArrival'];?>
&nbsp;(<?php if ($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['eRideType']=='Reccuring'){?><?php echo @LBL_RECURRING;?>
<?php }else{ ?><?php echo @LBL_ONETIME;?>
<?php }?>)</h2>
		  <div style="float:left; width:50%;" class="main-block-left">
		  <!-- Hemali.. Code for category Start -->
		  
		  <?php if ($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['eDocument']=='Yes'){?>
			<label>
			<p>
				<strong style="color: #000;font-size:14px;"> <?php echo @LBL_DOCUMENT;?>
:				</strong><?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['fDocumentPrice'];?>
 (<?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['vDocumentWeight'];?>
)
			</p>
			</label>
		  <?php }?>
		  <?php if ($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['eBox']=='Yes'){?>
			<label>
			<p>
				<strong style="color: #000;font-size: 14px;"> <?php echo @LBL_BOX;?>
:				</strong><?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['fBoxPrice'];?>
 (<?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['vBoxWeight'];?>
)
			</p>
			</label>
		  <?php }?>
		  <?php if ($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['eLuggage']=='Yes'){?>
			<label>
			<p>
				<strong style="color: #000;font-size: 14px;"> <?php echo @LBL_LUGGAGE;?>
:				</strong><?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['fLuggagePrice'];?>
 (<?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['vLuggageWeight'];?>
)
			</p>
			</label>
		  <?php }?>
		  <!-- Hemali.. Code for category End -->
		  </div>
		  <div style="float:left; width:50%;" class="main-block-right">
          <?php if ($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['eRideType']=='Reccuring'){?>
          <label>
          <p><strong style="color: #000;font-size: 14px;"><?php echo @LBL_START_FROM_DATE;?>
:</strong>&nbsp;<?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTimeFormat($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['dStartDate']);?>
&nbsp;&nbsp;&nbsp;<strong style="color: #000;font-size: 14px;"><?php echo @LBL_END_TO_DATE;?>
:</strong>&nbsp;<?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTimeFormat($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['dEndDate']);?>
</p>
          <!-- <span><em><strong><?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['totprice'];?>
</strong> <?php echo @LBL_PER_PESSENGER;?>
</em></span> -->
          </label>
          <label>
          <p><strong style="color: #000;font-size: 14px;"><?php echo @LBL_OUTBOUND_TIME;?>
:</strong>&nbsp;<?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['vMainOutBoundTime'],12);?>
&nbsp;&nbsp;&nbsp;<strong style="color: #000;font-size: 14px;"><?php echo @LBL_RETURN_TIME;?>
:</strong>&nbsp;<?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['vMainReturnTime'],12);?>
</p>
          </label>
          <?php }else{ ?>
          <label>
          <p><strong style="color: #000;font-size: 14px;"><?php echo @LBL_DEPARTURE_DATE;?>
:</strong>&nbsp;<?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTimeFormat($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['dDepartureDate']);?>

		  <?php if ($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['eRoundTrip']=='Yes'){?>
			&nbsp;&nbsp;&nbsp;<strong style="color: #000;font-size: 14px;"><?php echo @LBL_ARRIVAL_DATE;?>
:</strong>&nbsp;<?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTimeFormat($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['dArrivalDate']);?>

		  <?php }?>
		  </p>
         <!--  <span><em><strong><?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['totprice'];?>
</strong> <?php echo @LBL_PER_PESSENGER;?>
</em></span> -->
          </label>
          <label>
          <p><strong style="color: #000;font-size: 14px;"><?php echo @LBL_DEPARTURE_TIME1;?>
:</strong>&nbsp;<?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['vMainDepartureTime'],12);?>
 
		  <?php if ($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['eRoundTrip']=='Yes'){?>
			&nbsp;&nbsp;&nbsp;<strong style="color: #000;font-size: 14px;"><?php echo @LBL_RETURN_TIME;?>
:</strong>&nbsp;<?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['vMainArrivalTime'],12);?>

		  <?php }?>
		  </p>
          </label>
          <?php }?>
          <?php if ($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['stop_over']!=''){?>
          <label>
          <p><strong style="color: #000;font-size: 14px;"><?php echo @LBL_STOPOVER_POINT;?>
:</strong><?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['stop_over'];?>
</p>
          </label>
          <?php }?>
		  </div>
		  
          <div class="links-main-block">
            <!--<div class="links-main-block-left"> <a href="#"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
booking-blue-return.png" alt="" />Publish your return trip</a> <a href="#"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
booking-blue-duplicate.png" alt="" />Duplicate</a> </div>-->
            <div class="links-main-block-right"> <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=m-create_edit&for=first&id=<?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['iRideId'];?>
"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/edit.png" alt="" /><?php echo @LBL_VIEW_TRIP;?>
 </a> <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-create_duplicate&id=<?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['iRideId'];?>
"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/duplicate.png" alt="" />Duplicate</a> <a href="javascript:void(0);" onClick="delete_booking(<?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['iRideId'];?>
);"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/delete.png" alt="" /><?php echo @LBL_DELETE_TRIP;?>
</a></div>
          </div>
        </div>
        <?php }?>
        <?php endfor; endif; ?>
        <?php }else{ ?>
       <div class="main-block">
         <?php echo @LBL_NO_RIDES;?>
</div>
        <?php }?>
        </div>
        <div id="Past_ride" style="display:none;">
        <?php if ($_smarty_tpl->getVariable('past_count')->value>0){?>
        <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['name'] = "rideavail";
$_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_ride_list')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["rideavail"]['total']);
?>
        <?php if ($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['type']=='Past'){?>
        <div class="main-block">
          <h2><?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['vMainDeparture'];?>
 &rarr; <?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['vMainArrival'];?>
&nbsp;(<?php if ($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['eRideType']=='Reccuring'){?><?php echo @LBL_RECURRING;?>
<?php }else{ ?><?php echo @LBL_ONETIME;?>
<?php }?>)</h2>
          <?php if ($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['eRideType']=='Reccuring'){?>
          <label>
          <p><strong style="color: #000;font-size: 14px;"><?php echo @LBL_START_FROM_DATE;?>
:</strong>&nbsp;<?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTimeFormat($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['dStartDate']);?>
&nbsp;&nbsp;&nbsp;<strong style="color: #000;font-size: 14px;"><?php echo @LBL_END_TO_DATE;?>
:</strong>&nbsp;<?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTimeFormat($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['dEndDate']);?>
</p>
          <!-- <span><em><strong><?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['totprice'];?>
</strong> <?php echo @LBL_PER_PESSENGER;?>
</em></span> -->
          </label>
          <label>
          <p><strong style="color: #000;font-size: 14px;"><?php echo @LBL_OUTBOUND_TIME;?>
:</strong>&nbsp;<?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['vMainOutBoundTime'],12);?>
&nbsp;&nbsp;&nbsp;<strong style="color: #000;font-size: 14px;"><?php echo @LBL_RETURN_TIME;?>
:</strong>&nbsp;<?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['vMainReturnTime'],12);?>
</p>
          </label>
          <?php }else{ ?>
          <label>
          <p><strong style="color: #000;font-size: 14px;"><?php echo @LBL_DEPARTURE_DATE;?>
:</strong>&nbsp;<?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTimeFormat($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['dDepartureDate']);?>

		  <?php if ($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['eRoundTrip']=='Yes'){?>
			&nbsp;&nbsp;&nbsp;<strong style="color: #000;font-size: 14px;"><?php echo @LBL_ARRIVAL_DATE;?>
:</strong>&nbsp;<?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTimeFormat($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['dArrivalDate']);?>

		  <?php }?>
		  </p>
          <!-- <span><em><strong><?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['totprice'];?>
</strong> <?php echo @LBL_PER_PESSENGER;?>
</em></span> -->
          </label>
          <label>
          <p><strong style="color: #000;font-size: 14px;"><?php echo @LBL_DEPARTURE_TIME1;?>
:</strong>&nbsp;<?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['vMainDepartureTime'],12);?>

		  <?php if ($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['eRoundTrip']=='Yes'){?>
			&nbsp;&nbsp;&nbsp;<strong style="color: #000;font-size: 14px;"><?php echo @LBL_RETURN_TIME;?>
:</strong>&nbsp;<?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['vMainArrivalTime'],12);?>

		  <?php }?>
		  </p>
          </label>
          <?php }?>
          <?php if ($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['stop_over']!=''){?>
          <label>
          <p><strong style="color: #000;font-size: 14px;"><?php echo @LBL_STOPOVER_POINT;?>
:</strong><?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['stop_over'];?>
</p>
          </label>
          <?php }?>
          <div class="links-main-block">
            <!--<div class="links-main-block-left"> <a href="#"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
booking-blue-return.png" alt="" />Publish your return trip</a> <a href="#"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
booking-blue-duplicate.png" alt="" />Duplicate</a> </div>-->
            <div class="links-main-block-right"> <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
Rides-Edit-Form/<?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['iRideId'];?>
"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/edit.png" alt="" /><?php echo @LBL_VIEW_TRIP;?>
 </a> <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-create_duplicate&id=<?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rideavail']['index']]['iRideId'];?>
"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/duplicate.png" alt="" />Duplicate</a></div>
          </div>
        </div>
        <?php }?>
        <?php endfor; endif; ?>
        <?php }else{ ?>
        <div class="main-block">
          <?php echo @LBL_NO_RIDES;?>

          </div>
        <?php }?>
        </div>

      <div style="clear:both;"></div>
    </div>
    <div style="clear:both;"></div>
  </div>
  <!-------------------------inner-page end----------------->
  
  <div style="clear:both;"></div>
</div>
</div>
<form name="frmoffered" method="post" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=m-list_rides_offer">
  <input type="hidden" name="action" value="delete">
  <input type="hidden" name="id" id="id"  value="">
</form>

  <script>
    function show_hide(type){
      if(type == 'available'){
        $("#Avail").addClass( "active" );
        $("#Past").removeClass( "active" );
        $('#Past_ride').hide();
        $('#Avail_ride').show();
      }else{
        $("#Past").addClass( "active" );
        $("#Avail").removeClass( "active" );
        $('#Avail_ride').hide();
        $('#Past_ride').show();
      }
    }
    
    function delete_booking(id){
      var r=confirm("<?php echo @LBL_SURE_DELTE_RIDE_TRIP;?>
");
      if (r==true)
      {
        $('#id').val(id);
        document.frmoffered.submit();
        return false;
      }else{
        return false;
      }
    }
  </script>
