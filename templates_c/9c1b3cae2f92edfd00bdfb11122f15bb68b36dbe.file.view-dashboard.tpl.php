<?php /* Smarty version Smarty-3.0.7, created on 2016-01-07 22:06:49
         compiled from "/home/www/cargosharing1/templates/members/view-dashboard.tpl" */ ?>
<?php /*%%SmartyHeaderCode:948027578568ee1796b0d76-27815375%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9c1b3cae2f92edfd00bdfb11122f15bb68b36dbe' => 
    array (
      0 => '/home/www/cargosharing1/templates/members/view-dashboard.tpl',
      1 => 1452204408,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '948027578568ee1796b0d76-27815375',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="body-inner-part">
	<div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_DASHBOARD;?>
</span></div>
	<div class="main-inner-page">
		<div class="dashboard-new-page">
			<h2 class="dashboard-new-page-heading"><?php echo @LBL_DASHBOARD;?>
</h2>
			<div class="dashboard-new-page-main-left-part">
				<div class="dashboard-client">
					<div class="dashboard-client-img"> <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
profile-photo"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
64x64male.png" alt="" /></a> </div>
					<h3>
						<?php echo $_SESSION['sess_vFirstName'];?>
 <?php echo $_SESSION['sess_vLastName'];?>
<br />
						<?php if ($_smarty_tpl->getVariable('img')->value=='No'){?> <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
profile-photo"><?php echo nl2br(@LBL_ADD_PHOTO);?>
</a><br/>
					<?php }?> <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
personal-information">[<?php echo @LBL_EDIT_YOUR_PROFILE;?>
]</a> </h3></div>
					
					<div class="dashboard-new-page-bottom-part-left">
						<div class="dashboard-new-your-box">
							<div class="dashboard-new-your-box-top"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
dashboard-new-your-box-top.jpg" alt="" /></div>
							<div class="dashboard-new-your-box-mid">
								<h2><?php echo @LBL_PROFILE;?>
</h2>
								<ul>
									<!--<li><?php echo @LBL_PROFILE_VIEW;?>
 :<b><?php echo $_smarty_tpl->getVariable('db_member')->value[0]['iMemberViewCnt'];?>
</b></li>-->
									<li><?php echo @LBL_RIDE_VIEW;?>
 :<b><?php echo $_smarty_tpl->getVariable('iRideCount')->value;?>
</b></li>
									<li><?php echo @LBL_RATINGS;?>
 :<b><?php echo $_smarty_tpl->getVariable('rating')->value;?>
</b></li>
								</ul>
							</div>
							<div class="dashboard-new-your-box-bottom"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/dashboard-new-your-box-bottom.jpg" alt="" /></div>
						</div>
						<div class="dashboard-new-profile-status">
							<h2><?php echo @LBL_PROFILE_STATUS;?>
</h2>
							<ul>
								<div class="progress-striped"><span style="width:<?php echo $_smarty_tpl->getVariable('prof_percent')->value;?>
%;"><?php echo $_smarty_tpl->getVariable('prof_percent')->value;?>
%</span></div>
								<b class="din-tetx"><?php echo @LBL_FIND_MORE_MACHES;?>
 </b>
								<li><?php if ($_smarty_tpl->getVariable('first_last_name_img')->value=='green'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
green-arrow.jpg" alt="" /><?php }else{ ?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
gray-arrow.jpg" alt="" /><?php }?><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
personal-information"><?php echo @LBL_FIRST_NAME_AND_LAST_NAME;?>
</a></li>
								<li><?php if ($_smarty_tpl->getVariable('nickname_img')->value=='green'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
green-arrow.jpg" alt="" /><?php }else{ ?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
gray-arrow.jpg" alt="" /><?php }?><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
personal-information"><?php echo @LBL_NICK_NAME;?>
</a></li>
								<li><?php if ($_smarty_tpl->getVariable('birthdate_img')->value=='green'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
green-arrow.jpg" alt="" /><?php }else{ ?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
gray-arrow.jpg" alt="" /><?php }?><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
personal-information"><?php echo @LBL_AGE;?>
</a></li>
								<li><?php if ($_smarty_tpl->getVariable('email_varified_img')->value=='green'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
green-arrow.jpg" alt="" /><?php }else{ ?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
gray-arrow.jpg" alt="" /><?php }?><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
Member-Varification"><?php echo @LBL_EMAIL_VERIFIED;?>
</a></li>
								<li><?php if ($_smarty_tpl->getVariable('profile_photo_img')->value=='green'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
green-arrow.jpg" alt="" /><?php }else{ ?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
gray-arrow.jpg" alt="" /><?php }?><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
profile-photo"><?php echo @LBL_PROFILE_PHOTO;?>
</a></li>
								<li><?php if ($_smarty_tpl->getVariable('car_pref_status_img')->value=='green'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
green-arrow.jpg" alt="" /><?php }else{ ?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
gray-arrow.jpg" alt="" /><?php }?><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
car-preferences"><?php echo @LBL_SHARE_PREFERENCE;?>
</a></li>
								<?php if ($_smarty_tpl->getVariable('phone_verify')->value=='Yes'||$_smarty_tpl->getVariable('phone_verify')->value=='yes'){?>
								<li><?php if ($_smarty_tpl->getVariable('phone_verified_img')->value=='green'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
green-arrow.jpg" alt="" /><?php }else{ ?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
gray-arrow.jpg" alt="" /><?php }?><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
Member-Varification"><?php echo @LBL_PHONE;?>
 <?php echo @LBL_VERIFIED;?>
</a></li><?php }?>
								<li><?php if ($_smarty_tpl->getVariable('address_img')->value=='green'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
green-arrow.jpg" alt="" /><?php }else{ ?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
gray-arrow.jpg" alt="" /><?php }?><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
personal-information"><?php echo @LBL_ADDRESS;?>
</a></li>
								<li><?php if ($_smarty_tpl->getVariable('descritption_img')->value=='green'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
green-arrow.jpg" alt="" /><?php }else{ ?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
gray-arrow.jpg" alt="" /><?php }?><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
personal-information"><?php echo @LBL_QUIK_DESCRIPTION;?>
</a></li>
								<?php if ($_smarty_tpl->getVariable('PAYMENT_OPTION')->value=='PayPal'){?>
								<li><?php if ($_smarty_tpl->getVariable('payment_email_img')->value=='green'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
green-arrow.jpg" alt="" /><?php }else{ ?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
gray-arrow.jpg" alt="" /><?php }?><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
personal-information"><?php echo @LBL_PAYMENT_DETAILS;?>
</a>
								</li>
								<?php }?>
								<li><?php if ($_smarty_tpl->getVariable('car_detail_img')->value=='green'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
green-arrow.jpg" alt="" /><?php }else{ ?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
gray-arrow.jpg" alt="" /><?php }?><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
car-details"><?php echo @LBL_CAR_DETAILS;?>
</a></li>
								<div class="update-profile"><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
personal-information"><?php echo @LBL_UPDATE_PROFILE;?>
 &raquo;</a></div>
							</ul>
						</div>
						<!-- <div class="dashboard-new-profile-status">
							<h2>Your profile status</h2>
							<b><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/profile-status-img.jpg" alt="" /></b>
							<p>Find more matches by complete your profile:</p>
							<ul>
							<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/arrow-gr.jpg" alt="" /><a href="#">First Name & Last Name</a></li>
							<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/arrow-gra.jpg" alt="" /><a href="#">Nick Name</a></li>
							<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/arrow-gr.jpg" alt="" /><a href="#">Age</a></li>
							<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/arrow-gra.jpg" alt="" /><a href="#">Email Verified</a></li>
							<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/arrow-gr.jpg" alt="" /><a href="#">Profile Picture</a></li>
							<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/arrow-gra.jpg" alt="" /><a href="#">Car Share Preferences</a></li>
							<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/arrow-gra.jpg" alt="" /><a href="#">Phone Number Verified</a></li>
							<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/arrow-gra.jpg" alt="" /><a href="#">Address</a></li>
							<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/arrow-gra.jpg" alt="" /><a href="#">Description</a></li>
							<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/arrow-gra.jpg" alt="" /><a href="#">Payment Details</a></li>
							<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/arrow-gra.jpg" alt="" /><a href="#">Car Details</a></li>
							</ul>
							<div class="update-profile"><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
personal-information">update profile</a></div>
						</div>  -->
					</div>
			</div>
			<div class="dashboard-new-page-main-right-part"> <?php if ($_smarty_tpl->getVariable('tot_notofications')->value>'0'){?>
				<div class="dashboard-notifications">
					<div class="dashboard-notifications-inner">
						<h2><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/notifications-icon.png" alt="" /><?php echo @LBL_NOTIFICATION;?>
 (<?php echo $_smarty_tpl->getVariable('tot_notofications')->value;?>
)</h2>
						<ul>
							<?php if ($_smarty_tpl->getVariable('db_email_verification')->value[0]['eEmailVarified']=='No'){?>
							<li> <span>
								<h3><?php echo @LBL_VERIFY_EMAIL;?>
</h3>
								<form name="frmemailverify" enctype="multipart/form-data" id="frmemailverify" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=m-verification_a" method="post">
									<input type="hidden" name="action" id="action" value="emailverify">
									<input type="hidden" name="vEmail" id="vEmail" value="<?php echo $_SESSION['sess_vEmail'];?>
">
									<input type="hidden" name="vName" id="vName" value="<?php echo $_SESSION['sess_vFirstName'];?>
 <?php echo $_SESSION['sess_vLastName'];?>
">
								</form>
								<p><?php echo @LBL_EMAIL_VERI_NOTE1;?>

								<?php echo @LBL_EMAIL_VERI_NOTE2;?>
 </p>
							</span> <a href="javascript:void(0);" class="link-dashbord" onClick="checkmail();"><?php echo @LBL_VERIFY_EMAIL;?>
</a> </li>
							<?php }?>
							<?php if ($_smarty_tpl->getVariable('PHONE_VERIFICATION_REQUIRED')->value=='Yes'){?>
							<?php if ($_smarty_tpl->getVariable('db_email_verification')->value[0]['ePhoneVerified']=='No'){?>
							<li> <span>
								<h3><?php echo @LBL_VERIFY_PHONE;?>
</h3>
								<p><?php echo @LBL_EMAIL_VERI_NOTE1;?>

								<?php echo @LBL_EMAIL_VERI_NOTE2;?>
</p>
							</span> <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
Member-Varification" class="link-dashbord"><?php echo @LBL_VERIFY_PHONE;?>
</a> </li>
							<?php }?>
							<?php }?>
						</ul>
					</div>
				</div>
				<?php }?>
				<div class="dashboard-new-page-bottom-part-right">
					<div class="dashboard-new-page-bottom-part-list">
						<ul>
							<li>
								<div class="dashboard-bottom-box1"> 
									<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
my-bookings">
										<h2><?php echo @LBL_MY_BOOKINGS;?>
</h2>
										<h3><?php echo $_smarty_tpl->getVariable('tot_bookings')->value;?>
</h3>
									<span><?php echo @LBL_BOOKING;?>
</span> 
									</a> 
									</div>
							</li>
							<li>
								<div class="dashboard-bottom-box2"> 
									<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
List-Rides-Offer">
										<h2><?php echo @LBL_RIDES_OFFERED1;?>
</h2>
										<h3><?php echo $_smarty_tpl->getVariable('tot_rides')->value;?>
</h3>
									<span><?php echo @LBL_RIDES_OFFERED1;?>
</span> 
									</a>
									</div>
							</li>
							<li>
								<div class="dashboard-bottom-box3"> 
									<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
received-messages">
										<h2><?php echo @LBL_MESSAGES;?>
</h2>
										<h3><?php echo $_smarty_tpl->getVariable('tot_messages')->value;?>
</h3>
									<span><?php echo @LBL_MESSAGES;?>
</span> 
									</a>
									</div>
							</li>
							<li>
								<div class="dashboard-bottom-box4"> 
									<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
Email-Alerts">
										<h2><?php echo @LBL_MY_ALERTS;?>
</h2>
										<h3><?php echo $_smarty_tpl->getVariable('tot_ride_alerts')->value;?>
</h3>
										<span><?php echo @LBL_EMAIL_ALERTS;?>
</span> 
									</a>
								</div>
							</li>
							<li>
								<div class="dashboard-bottom-box5"> 
									<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
my-bookings">
										<h2><?php echo @LBL_PAID_BY_YOU;?>
</h2>
										<h3><?php echo $_smarty_tpl->getVariable('tot_paid')->value;?>
</h3>
									<span><?php echo $_SESSION['sess_price_ratio'];?>
</span> 
									</a>
									</div>
							</li>
							<li>
								<div class="dashboard-bottom-box6"> 
									<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
my-bookings">
										<h2><?php echo @LBL_YOU_EARNED;?>
</h2>
										<h3><?php echo $_smarty_tpl->getVariable('tot_earned')->value;?>
</h3>
									<span><?php echo $_SESSION['sess_price_ratio'];?>
</span> 
									</a>
									</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div style="clear:both;"></div>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>
 
<script>
	function checkmail(){
		document.frmemailverify.submit();
	}
</script> 

