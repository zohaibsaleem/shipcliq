<?php /* Smarty version Smarty-3.0.7, created on 2015-01-23 15:54:40
         compiled from "/home/www/blablaclone/templates/content/view-offer_ride.tpl" */ ?>
<?php /*%%SmartyHeaderCode:117589429354c22168097cc2-48306942%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a3b4e14f7c05c92a705f08dbbd68c42abe3a8ade' => 
    array (
      0 => '/home/www/blablaclone/templates/content/view-offer_ride.tpl',
      1 => 1422008677,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '117589429354c22168097cc2-48306942',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&language=en"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
gmap3.js"></script>   
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
rrb/zebra_datepicker.src.js"></script>
<link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
rrb/default.css" type="text/css"> 
<?php if ($_smarty_tpl->getVariable('sess_browser')->value=='Internet Explorer'){?>
  
    <script>
      alert('<?php echo @LBL_RIDE_POST_NOT_SUPPORT;?>
');
      window.location = "<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
";
    </script>
  
<?php }?>
<?php if ($_smarty_tpl->getVariable('var_err_msg')->value!=''){?>
 
<script>
      showNotification({type : 'error', message: '<?php echo $_smarty_tpl->getVariable('var_err_msg')->value;?>
'});
</script> 

<?php }?>  

<style>
  #map-canvas {
    height: 383px;
    margin-top: 0px;
    padding: 0px;
    width: 370px;
  }  
</style>


  <script type="text/javascript">        
    $(function() {
        var from = document.getElementById('from');        
        var autocomplete_from = new google.maps.places.Autocomplete(from);
    		google.maps.event.addListener(autocomplete_from, 'place_changed', function() {
    			var place = autocomplete_from.getPlace();     		
    			go_for_action();
    		});
    		
    		var to = document.getElementById('to');
    		var autocomplete_to = new google.maps.places.Autocomplete(to);
    		google.maps.event.addListener(autocomplete_to, 'place_changed', function() {
    			var place = autocomplete_to.getPlace();     			
    			go_for_action();
    		});
        
        var loc1 = document.getElementById('loc1');
        var autocomplete_loc1 = new google.maps.places.Autocomplete(loc1);
    		google.maps.event.addListener(autocomplete_loc1, 'place_changed', function() {
    			var place = autocomplete_loc1.getPlace();
    			go_for_action(); 
    		}); 
    		
    		var loc2 = document.getElementById('loc2');
        var autocomplete_loc2 = new google.maps.places.Autocomplete(loc2);
    		google.maps.event.addListener(autocomplete_loc2, 'place_changed', function() {
    			var place = autocomplete_loc2.getPlace();
    			go_for_action();
    		});
    		
    		var loc3 = document.getElementById('loc3');
        var autocomplete_loc3 = new google.maps.places.Autocomplete(loc3);
    		google.maps.event.addListener(autocomplete_loc3, 'place_changed', function() {
    			var place = autocomplete_loc3.getPlace(); 
    			go_for_action();  
    		});
    		
    		var loc4 = document.getElementById('loc4');
        var autocomplete_loc4 = new google.maps.places.Autocomplete(loc4);
    		google.maps.event.addListener(autocomplete_loc4, 'place_changed', function() {
    			var place = autocomplete_loc4.getPlace();
    			go_for_action(); 
    		});
    		
    		var loc5 = document.getElementById('loc5');
        var autocomplete_loc5 = new google.maps.places.Autocomplete(loc5);
    		google.maps.event.addListener(autocomplete_loc5, 'place_changed', function() {
    			var place = autocomplete_loc5.getPlace();
    			go_for_action();
    		});
    		
    		var loc6 = document.getElementById('loc6');
        var autocomplete_loc6 = new google.maps.places.Autocomplete(loc6);
    		google.maps.event.addListener(autocomplete_loc6, 'place_changed', function() {
    			var place = autocomplete_loc6.getPlace();
    			go_for_action();  
    		});
    		
    		function go_for_action(){
           if($("#from").val() != '' &&  $("#to").val() == ''){               
            show_location($("#from").val());
           }
           
           if($("#to").val() != '' &&  $("#from").val() == ''){
            show_location($("#to").val());
           }
           
           if($("#from").val() != '' &&  $("#to").val() != ''){
            from_to($("#from").val(), $("#to").val());
           }
        }
    });     
  </script>

<div class="body-inner-part">
  <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_OFFER;?>
 <?php echo @LBL_A_RIDE;?>
</span></div>
  <div class="main-inner-page">
    <h2><?php echo @LBL_OFFER;?>
<span><?php echo @LBL_A_RIDE;?>
</span></h2>
    <div class="offer-seats">      
      <div class="tabing"> <a href="javascript:void(0);" class="active"><?php echo @LBL_MY_ITINERARY;?>
</a><a href="javascript:void(0);"><?php echo @LBL_PRICE;?>
</a> </div>
      <form name="frmride" id="frmride" method="post" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-offer_ride_second">
      <input type="hidden" name="distance" id="distance" value=""> 
      <input type="hidden" name="duration" id="duration" value=""> 
      <input type="hidden" name="from_lat_long" id="from_lat_long" value="<?php echo $_smarty_tpl->getVariable('from_lat_long')->value;?>
">
      <input type="hidden" name="to_lat_long" id="to_lat_long" value="<?php echo $_smarty_tpl->getVariable('to_lat_long')->value;?>
">
      <input type="hidden" name="loc1_lat_long" id="loc1_lat_long" value="<?php echo $_smarty_tpl->getVariable('loc1_lat_long')->value;?>
">
      <input type="hidden" name="loc2_lat_long" id="loc2_lat_long" value="<?php echo $_smarty_tpl->getVariable('loc2_lat_long')->value;?>
">
      <input type="hidden" name="loc3_lat_long" id="loc3_lat_long" value="<?php echo $_smarty_tpl->getVariable('loc3_lat_long')->value;?>
">
      <input type="hidden" name="loc4_lat_long" id="loc4_lat_long" value="<?php echo $_smarty_tpl->getVariable('loc4_lat_long')->value;?>
">
      <input type="hidden" name="loc5_lat_long" id="loc5_lat_long" value="<?php echo $_smarty_tpl->getVariable('loc5_lat_long')->value;?>
">
      <input type="hidden" name="loc6_lat_long" id="loc6_lat_long" value="<?php echo $_smarty_tpl->getVariable('loc6_lat_long')->value;?>
">       
      <div class="offer-seats-left">
        <div class="type-of-trip">
          <h2><?php echo @LBL_TYPE_TRIP;?>
</h2>
          <span>
          <em><input name="triptype" id="onetime" type="radio" value="onetime" onClick="show_trip_type(this.value);"/> <?php echo @LBL_ONE_TIME_TRIP;?>
</em> 
          <em><input name="triptype" id="recurring" type="radio" value="recurring" onClick="show_trip_type(this.value);"/> <?php echo @LBL_RECURRING_TRIP;?>
 </em>
          </span> 
        </div>
        <div class="type-of-trip2">
          <h2><?php echo @LBL_ROUTE;?>
</h2>
          <span> <em>
          <input name="from" id="from" type="text" class="location-input" value="<?php echo $_smarty_tpl->getVariable('from')->value;?>
"  placeholder="<?php echo @LBL_YOUR_DEPARTURE_POINT;?>
"/>
          <br />
          <p><?php echo @LBL_EXAMPLE;?>
: Canary Wharf, London</p>
          </em> <em>
          <input name="to" id="to" type="text" class="location-input1" value="<?php echo $_smarty_tpl->getVariable('to')->value;?>
" placeholder="<?php echo @LBL_YOUR_DEPARTURE_POINT;?>
"/>
          </em> 
          <em> <?php echo @LBL_RIDE_OFFER_NOTE1;?>

          </em>
          <em> 
          <input name="loc1" id="loc1" type="text" class="location-input1" value="<?php echo $_smarty_tpl->getVariable('loc1')->value;?>
" placeholder="<?php echo @LBL_POSSIBLE_STOP_POINT;?>
"/>           
          <input name="loc2" id="loc2" type="text" class="location-input1" value="<?php echo $_smarty_tpl->getVariable('loc2')->value;?>
" placeholder="<?php echo @LBL_POSSIBLE_STOP_POINT;?>
"/>          
          <input name="loc3" id="loc3" type="text" class="location-input1" value="<?php echo $_smarty_tpl->getVariable('loc3')->value;?>
" placeholder="<?php echo @LBL_POSSIBLE_STOP_POINT;?>
"/>          
          <input name="loc4" id="loc4" type="text" class="location-input1" value="<?php echo $_smarty_tpl->getVariable('loc4')->value;?>
" placeholder="<?php echo @LBL_POSSIBLE_STOP_POINT;?>
"/>          
          <input name="loc5" id="loc5" type="text" class="location-input1" value="<?php echo $_smarty_tpl->getVariable('loc5')->value;?>
" placeholder="<?php echo @LBL_POSSIBLE_STOP_POINT;?>
"/>         
          <input name="loc6" id="loc6" type="text" class="location-input1" value="<?php echo $_smarty_tpl->getVariable('loc6')->value;?>
" placeholder="<?php echo @LBL_POSSIBLE_STOP_POINT;?>
"/>
          </em> 
          <!--<em class="last"><a href="#">Add another stopover point</a></em>-->
          </span> 
          </div>         
        <div style="display:none;" id="frmric" class="type-of-trip3">
          <h2><?php echo @LBL_DATE_TIME;?>
 <div style="float:right;margin-right:5px;font-size:15px;"><input type="checkbox" name="roundtripric" id="roundtripric" value="Yes" checked><?php echo @LBL_ROUND_TRIP;?>
</div></h2>
          <span> 
          <em>
          <?php echo @LBL_OUTBOUND_DAY;?>
:                                                                                                                              
          <table width="70%" class="outbdday">                                                                                                                                            
            <tr>
              <td style="background-color: #ADDE71;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="outbound[]" id="outbound" value="Sunday" <?php if ($_smarty_tpl->getVariable('out_Sunday')->value=='Yes'){?> checked <?php }?>>Sun</td>
              <td style="background-color: #ADDE71;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="outbound[]" id="outbound" value="Monday" <?php if ($_smarty_tpl->getVariable('out_Monday')->value=='Yes'){?> checked <?php }?>>Mon</td>
              <td style="background-color: #ADDE71;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="outbound[]" id="outbound" value="Tuesday" <?php if ($_smarty_tpl->getVariable('out_Tuesday')->value=='Yes'){?> checked <?php }?>>Tue</td>
              <td style="background-color: #ADDE71;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="outbound[]" id="outbound" value="Wednesday" <?php if ($_smarty_tpl->getVariable('out_Wednesday')->value=='Yes'){?> checked <?php }?>>Wed</td>
              <td style="background-color: #ADDE71;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="outbound[]" id="outbound" value="Thursday" <?php if ($_smarty_tpl->getVariable('out_Thursday')->value=='Yes'){?> checked <?php }?>>Thu</td>
              <td style="background-color: #ADDE71;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="outbound[]" id="outbound" value="Friday" <?php if ($_smarty_tpl->getVariable('out_Friday')->value=='Yes'){?> checked <?php }?>>Fri</td>
              <td style="background-color: #ADDE71;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="outbound[]" id="outbound" value="Saturday" <?php if ($_smarty_tpl->getVariable('out_Saturday')->value=='Yes'){?> checked <?php }?>>Sat</td>               
            </tr>
          </table> 
          </em>
          <em id="returndays">
          <?php echo @LBL_RETURN_DAY;?>
:
          <table width="70%">
            <tr>
              <td style="background-color: #C5F0FF;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="retturn[]" id="retturn" value="Sunday" <?php if ($_smarty_tpl->getVariable('ret_Sunday')->value=='Yes'){?> checked <?php }?>>Sun</td>
              <td style="background-color: #C5F0FF;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="retturn[]" id="retturn" value="Monday" <?php if ($_smarty_tpl->getVariable('ret_Monday')->value=='Yes'){?> checked <?php }?>>Mon</td>
              <td style="background-color: #C5F0FF;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="retturn[]" id="retturn" value="Tuesday" <?php if ($_smarty_tpl->getVariable('ret_Tuesday')->value=='Yes'){?> checked <?php }?>>Tue</td>
              <td style="background-color: #C5F0FF;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="retturn[]" id="retturn" value="Wednesday" <?php if ($_smarty_tpl->getVariable('ret_Wednesday')->value=='Yes'){?> checked <?php }?>>Wed</td>
              <td style="background-color: #C5F0FF;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="retturn[]" id="retturn" value="Thursday" <?php if ($_smarty_tpl->getVariable('ret_Thursday')->value=='Yes'){?> checked <?php }?>>Thu</td>
              <td style="background-color: #C5F0FF;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="retturn[]" id="retturn" value="Friday" <?php if ($_smarty_tpl->getVariable('ret_Friday')->value=='Yes'){?> checked <?php }?>>Fri</td>
              <td style="background-color: #C5F0FF;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><input type="checkbox" name="retturn[]" id="retturn" value="Saturday" <?php if ($_smarty_tpl->getVariable('ret_Saturday')->value=='Yes'){?> checked <?php }?>>Sat</td>                              
            </tr>
          </table>  
          </em>
          <em> <?php echo @LBL_OUTBOUND_TIME;?>
:<br />           
          <select name="richourstart" id="richourstart" class="location-select">            
            <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['name'] = "ots";
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['loop'] = is_array($_loop=24) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start'] = (int)1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['step'] = 1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start'] = max($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['step'] > 0 ? 0 : -1, $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['loop'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start']);
else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start'] = min($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['loop'] : $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['loop']-1);
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['total']);
?>
              <option value="<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['ots']['index'];?>
" <?php if ($_smarty_tpl->getVariable('richourstart')->value==$_smarty_tpl->getVariable('smarty')->value['section']['ots']['index']){?> selected <?php }?>><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['ots']['index'];?>
</option>
            <?php endfor; endif; ?>
          </select>
          <select class="location-select" name="ricminstart" id="ricminstart">
            <option value="00" <?php if ($_smarty_tpl->getVariable('ricminstart')->value=='00'){?> selected <?php }?>>00</option>
            <option value="10" <?php if ($_smarty_tpl->getVariable('ricminstart')->value=='10'){?> selected <?php }?>>10</option>
            <option value="20" <?php if ($_smarty_tpl->getVariable('ricminstart')->value=='20'){?> selected <?php }?>>20</option>
            <option value="30" <?php if ($_smarty_tpl->getVariable('ricminstart')->value=='30'){?> selected <?php }?>>30</option>
            <option value="40" <?php if ($_smarty_tpl->getVariable('ricminstart')->value=='40'){?> selected <?php }?>>40</option>
            <option value="50" <?php if ($_smarty_tpl->getVariable('ricminstart')->value=='50'){?> selected <?php }?>>50</option>
          </select>
          </em> 
          <em id="returntime"> <?php echo @LBL_RETURN_TIME;?>
:<br />           
         <select name="richourend" id="richourend" class="location-select">            
            <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['name'] = "ote";
$_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['loop'] = is_array($_loop=24) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['start'] = (int)1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['step'] = 1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['start'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['start'] = max($_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['step'] > 0 ? 0 : -1, $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['loop'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['start']);
else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['start'] = min($_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['loop'] : $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['loop']-1);
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["ote"]['total']);
?>
              <option value="<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['ote']['index'];?>
" <?php if ($_smarty_tpl->getVariable('richourend')->value==$_smarty_tpl->getVariable('smarty')->value['section']['ots']['index']){?> selected <?php }?>><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['ote']['index'];?>
</option>
            <?php endfor; endif; ?>
          </select>
          <select class="location-select" name="ricminend" id="ricminend">
            <option value="00" <?php if ($_smarty_tpl->getVariable('ricminend')->value=='00'){?> selected <?php }?>>00</option>
            <option value="10" <?php if ($_smarty_tpl->getVariable('ricminend')->value=='10'){?> selected <?php }?>>10</option>
            <option value="20" <?php if ($_smarty_tpl->getVariable('ricminend')->value=='20'){?> selected <?php }?>>20</option>
            <option value="30" <?php if ($_smarty_tpl->getVariable('ricminend')->value=='30'){?> selected <?php }?>>30</option>
            <option value="40" <?php if ($_smarty_tpl->getVariable('ricminend')->value=='40'){?> selected <?php }?>>40</option>
            <option value="50" <?php if ($_smarty_tpl->getVariable('ricminend')->value=='50'){?> selected <?php }?>>50</option>
          </select>
          </em> 
          <em><?php echo @LBL_START_FROM_DATE;?>
:<br />
          <input name="sdate" id="sdate" type="text" class="location-input3" value="<?php echo $_smarty_tpl->getVariable('sdate')->value;?>
"/>           
          </em>             
          <em><?php echo @LBL_END_TO_DATE;?>
:<br />
          <input name="edate" id="edate" type="text" class="location-input3" value="<?php echo $_smarty_tpl->getVariable('edate')->value;?>
"/>          
          </em> 
          <em>
          <a class="cont-bot-but" href="javascript:void(0);" onClick="show_me_dates();" style="float: left;"><?php echo @LBL_VIEW_ALL_DATE;?>
</a>
          </em>
          <em id="rb" class="last"></em>
          </span> 
        </div>                
        <div style="display:none;" id="frmonetime" class="type-of-trip3">
          <h2><?php echo @LBL_DATE_TIME;?>
 <div style="float:right;margin-right:5px;font-size:15px;"><input type="checkbox" name="roundtriponetime" id="roundtriponetime" value="Yes"><?php echo @LBL_ROUND_TRIP;?>
</div></h2>
          <span> <em> <?php echo @LBL_DEPARTURE_DATE;?>
:<br />
          <input name="sdateone" id="sdateone" type="text" class="location-input3" value="<?php echo $_smarty_tpl->getVariable('sdateone')->value;?>
"/>
          <select name="onetihourstart" id="onetihourstart" class="location-select">
            <option value=""><?php echo @LBL_HR;?>
</option>
            <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['name'] = "ots";
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['loop'] = is_array($_loop=24) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start'] = (int)1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['step'] = 1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start'] = max($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['step'] > 0 ? 0 : -1, $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['loop'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start']);
else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start'] = min($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['loop'] : $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['loop']-1);
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['total']);
?>
              <option value="<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['ots']['index'];?>
" <?php if ($_smarty_tpl->getVariable('onetihourstart')->value==$_smarty_tpl->getVariable('smarty')->value['section']['ots']['index']){?> selected <?php }?>><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['ots']['index'];?>
</option>
            <?php endfor; endif; ?>
          </select>
          <select class="location-select" name="onetimeminstart" id="onetimeminstart"> 
            <option value=""><?php echo @LBL_MIN;?>
</option>
            <option value="00" <?php if ($_smarty_tpl->getVariable('onetimeminstart')->value=='00'){?> selected <?php }?>>00</option>
            <option value="10" <?php if ($_smarty_tpl->getVariable('onetimeminstart')->value=='10'){?> selected <?php }?>>10</option>
            <option value="20" <?php if ($_smarty_tpl->getVariable('onetimeminstart')->value=='20'){?> selected <?php }?>>20</option>
            <option value="30" <?php if ($_smarty_tpl->getVariable('onetimeminstart')->value=='30'){?> selected <?php }?>>30</option>
            <option value="40" <?php if ($_smarty_tpl->getVariable('onetimeminstart')->value=='40'){?> selected <?php }?>>40</option>
            <option value="50" <?php if ($_smarty_tpl->getVariable('onetimeminstart')->value=='50'){?> selected <?php }?>>50</option>
          </select>
          </em> 
          <em id="returndaystime" class="last"> <?php echo @LBL_RETURN_DATE;?>
:<br />
          <input name="edateone" id="edateone" type="text" class="location-input3" value="<?php echo $_smarty_tpl->getVariable('edateone')->value;?>
"/>
          <select name="onetihourend" id="onetihourend" class="location-select">
            <option value=""><?php echo @LBL_HR;?>
</option>
            <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['name'] = "ots";
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['loop'] = is_array($_loop=24) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start'] = (int)1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['step'] = 1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start'] = max($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['step'] > 0 ? 0 : -1, $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['loop'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start']);
else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start'] = min($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['loop'] : $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['loop']-1);
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["ots"]['total']);
?>
              <option value="<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['ots']['index'];?>
" <?php if ($_smarty_tpl->getVariable('onetihourend')->value==$_smarty_tpl->getVariable('smarty')->value['section']['ots']['index']){?> selected <?php }?>><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['ots']['index'];?>
</option>
            <?php endfor; endif; ?>
          </select>
          <select class="location-select" name="onetimeminend" id="onetimeminend">
            <option value=""><?php echo @LBL_MIN;?>
</option>
            <option value="00" <?php if ($_smarty_tpl->getVariable('onetimeminend')->value=='00'){?> selected <?php }?>>00</option>
            <option value="10" <?php if ($_smarty_tpl->getVariable('onetimeminend')->value=='10'){?> selected <?php }?>>10</option>
            <option value="20" <?php if ($_smarty_tpl->getVariable('onetimeminend')->value=='20'){?> selected <?php }?>>20</option>
            <option value="30" <?php if ($_smarty_tpl->getVariable('onetimeminend')->value=='30'){?> selected <?php }?>>30</option>
            <option value="40" <?php if ($_smarty_tpl->getVariable('onetimeminend')->value=='40'){?> selected <?php }?>>40</option>
            <option value="50" <?php if ($_smarty_tpl->getVariable('onetimeminend')->value=='50'){?> selected <?php }?>>50</option>
          </select>
          </em> 
          </span> 
          </div>         
        <a class="cont-bot-but" href="javascript:void(0);" onClick="go_next_page();"><?php echo @LBL_CONTINUE;?>
</a> 
      </div> 
      </form>
      <div class="offer-seats-right"><div id="map-canvas" class="gmap3"></div></div>
    </div>
  </div>
  <div style="clear:both;"></div>
</div>

<div style="display:none">
  <div id="searchmodel" class="form-login">
    <h4><?php echo @LBL_ADD_RIDE;?>
</h4>
    <form method="post" id="add_box" name="add_box">
    <input type="hidden" value="" id="addtype" name="addtype">
    <input type="hidden" value="" id="adddate" name="adddate">  
    <input type="hidden" value="add_date" id="add_action" name="add_action">
    <div class="inner-form1">        
      <div class="singlerow-login" style="background-color:#ADDE71;width:130px;padding: 10px;border: 1px solid #D2D2D2;font-size: 16px;">
        <input type="checkbox" value="addout" id="addout" name="addout">&nbsp;<?php echo @LBL_OURBOUND;?>

      </div> 
      <div class="singlerow-login" style="background-color:#C5F0FF;width:130px;padding: 10px;border: 1px solid #D2D2D2;font-size: 16px;">
        <input type="checkbox" value="addret" id="addret" name="addret">&nbsp;<?php echo @LBL_RETURN;?>

      </div>  
      <div class="singlerow-login-log"><a href="javascript:void(0);" onclick="check_add();"><?php echo @LBL_ADD;?>
</a></div>
      <div style="clear:both;"></div>
    </div>
    </form>           
  </div>
</div>

  <script>
    var map;
    function initialize() {
      var mapOptions = {
        zoom: 5,
        center: new google.maps.LatLng('<?php echo $_smarty_tpl->getVariable('MAP_LATITUDE')->value;?>
', '<?php echo $_smarty_tpl->getVariable('MAP_LONGITUDE')->value;?>
')
      };
      map = new google.maps.Map(document.getElementById('map-canvas'),
          mapOptions);
    }
    
    $(document).ready(function(){
      google.maps.event.addDomListener(window, 'load', initialize);
    });  

  </script>
  <script type="text/javascript">
      function show_location(address){ 
          clearThat()
          $('#map-canvas').gmap3({
            marker:{
              address: address
            },
            map:{
              options:{
                zoom: 8
              }
            }
          });
      }  
      
      function clearThat(){ 
          var opts = {};
          opts.name = ["marker", "directionsrenderer"];
          opts.first = true;        
          $('#map-canvas').gmap3({clear:opts});         
      }
      
      function from_to(from, to){
        clearThat();
        
        var chks = document.getElementsByName('loc');
        var waypts = [];
        
        $("#map-canvas").gmap3({
            getlatlng:{
              address:  from,
              callback: function(results){
               $("#from_lat_long").val(results[0].geometry.location);
              }
            }
        });
            
        $("#map-canvas").gmap3({
            getlatlng:{
              address:  to,
              callback: function(results){
               $("#to_lat_long").val(results[0].geometry.location);
              }
            }
        });
        
        if($("#loc1").val() != ''){
          waypts.push({location:$("#loc1").val(), stopover:true});
          $("#map-canvas").gmap3({
          getlatlng:{
            address:  $("#loc1").val(),
            callback: function(results){
             $("#loc1_lat_long").val(results[0].geometry.location);
            }
          }
          });
        }
        if($("#loc2").val() != ''){
          waypts.push({location:$("#loc2").val(), stopover:true});
          $("#map-canvas").gmap3({
          getlatlng:{
            address:  $("#loc2").val(),
            callback: function(results){
            $("#loc2_lat_long").val(results[0].geometry.location);
            }
          }
          });
        }
        if($("#loc3").val() != ''){
          waypts.push({location:$("#loc3").val(), stopover:true});
          $("#map-canvas").gmap3({
          getlatlng:{
            address:  $("#loc3").val(),
            callback: function(results){
            $("#loc3_lat_long").val(results[0].geometry.location);
            }
          }
          });
        }
        if($("#loc4").val() != ''){
          waypts.push({location:$("#loc4").val(), stopover:true});
          $("#map-canvas").gmap3({
          getlatlng:{
            address:  $("#loc4").val(),
            callback: function(results){
            $("#loc4_lat_long").val(results[0].geometry.location);
            }
          }
          });
        }
        if($("#loc5").val() != ''){
          waypts.push({location:$("#loc5").val(), stopover:true});
          $("#map-canvas").gmap3({
          getlatlng:{
            address:  $("#loc5").val(),
            callback: function(results){
            $("#loc5_lat_long").val(results[0].geometry.location);
            }
          }
          });
        }
        if($("#loc6").val() != ''){
          waypts.push({location:$("#loc6").val(), stopover:true});
          $("#map-canvas").gmap3({
          getlatlng:{
            address:  $("#loc6").val(),
            callback: function(results){
            $("#loc6_lat_long").val(results[0].geometry.location);
            }
          }
          });
        }
        
        $("#map-canvas").gmap3({ 
          getroute:{
            options:{
                origin:from,
                destination:to,
                waypoints:waypts,               
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            },
            callback: function(results){
              if (!results) return; 
              $(this).gmap3({
                map:{
                  options:{
                    zoom: 8,  
             //       center: [51.511214, -0.119824]
                    center: [58.0000, 20.0000]
                  }
                },
                directionsrenderer:{
                  options:{
                    directions:results
                  } 
                }
              });
            }
          }
        });
        
        $("#map-canvas").gmap3({
          getdistance:{
            options:{ 
              origins:from, 
              destinations:to,
              travelMode: google.maps.TravelMode.DRIVING
            },
            callback: function(results, status){
              var html = "";
              if (results){
                for (var i = 0; i < results.rows.length; i++){
                  var elements = results.rows[i].elements;
                  for(var j=0; j<elements.length; j++){
                    switch(elements[j].status){
                      case "OK":
                        html += elements[j].distance.text + " (" + elements[j].duration.text + ")<br />";
                        document.getElementById("distance").value = elements[j].distance.text;
                        document.getElementById("duration").value = elements[j].duration.text;
                        break;
                      
                    }
                  }
                } 
              } else {
                html = "error";
              }
              $("#results").html( html );
            }
          }
        });
      }
  </script>
  <script>
      function show_me_dates(){
        var sdate=document.getElementById('sdate').value;
          var edate=document.getElementById('edate').value;
          if(sdate == '' || edate == '')
          {
            $("#rb").hide();
            return false;
          }else{$("#rb").show();}
        var request = $.ajax({  
      	  type: "POST",
      	  url: site_url+'index.php?file=c-ride_dates',  
      	  data: $("#frmride").serialize(), 	  
      	  
      	  success: function(data) {//alert(data);  
      	   $("#rb").html(data);
      		}
    	 });                                        
    	
       request.fail(function(jqXHR, textStatus) {
        //alert( "Request failed: " + textStatus ); 
       });
      }
      
      function select_date(type, date){
        document.getElementById("addtype").value = type;
        document.getElementById("adddate").value = date;
        if(type == 'out'){
         document.getElementById("addout").checked=true;
         document.getElementById("addret").checked=false;
        }else if(type == 'ret'){
         document.getElementById("addout").checked=false;
         document.getElementById("addret").checked=true;
        }else if(type == 'both'){
         document.getElementById("addout").checked=true;
         document.getElementById("addret").checked=true;
        }else{
         document.getElementById("addout").checked=false;
         document.getElementById("addret").checked=false;  
        }
        $.fancybox("#searchmodel");return false;
      }
      
      function check_add(){
        var request = $.ajax({  
      	  type: "POST",
      	  url: site_url+'index.php?file=c-add_date', 
      	  data: $("#add_box").serialize(), 	  
      	  
      	  success: function(data) { //alert(data);     		 
            $.fancybox.close();
            show_me_dates();			
      		}
    	  });
        
        request.fail(function(jqXHR, textStatus) {
    	   loading_hide() ; 
         //alert( "Request failed: " + textStatus ); 
    	  });
      }
      
      $('#sdate').Zebra_DatePicker({direction: ['<?php echo $_smarty_tpl->getVariable('startdatepicker')->value;?>
', '<?php echo $_smarty_tpl->getVariable('enddatepicker')->value;?>
'],onSelect: function(view, elements){if(document.getElementById("edate").value != ''){show_me_dates();}}});  
      $('#edate').Zebra_DatePicker({direction: ['<?php echo $_smarty_tpl->getVariable('startdatepicker')->value;?>
', '<?php echo $_smarty_tpl->getVariable('enddatepicker')->value;?>
'],onSelect: function(view, elements){if(document.getElementById("sdate").value != ''){show_me_dates();}}});
      
      $('#sdateone').Zebra_DatePicker({direction: ['<?php echo $_smarty_tpl->getVariable('startdatepicker')->value;?>
', '<?php echo $_smarty_tpl->getVariable('enddatepicker')->value;?>
']});  
      $('#edateone').Zebra_DatePicker({direction: ['<?php echo $_smarty_tpl->getVariable('startdatepicker')->value;?>
', '<?php echo $_smarty_tpl->getVariable('enddatepicker')->value;?>
']});
      
      function show_trip_type(type){
        if(type == 'onetime'){
         $("#frmric").hide();
         $("#frmonetime").show();
        }else{
         $("#frmonetime").hide();
         $("#frmric").show();
        }
      }
      
      $("#roundtripric").on( "click", function() {
        if($("#roundtripric").is(':checked')){
          $("#returndays").show();
          $("#returntime").show();
        }else{
          $("#returndays").hide();
          $("#returntime").hide();              
        }
      });
      
      $("#roundtriponetime").on( "click", function() {
        if($("#roundtriponetime").is(':checked')){
          $("#returndaystime").show();
        }else{
          $("#returndaystime").hide();
          document.getElementById("edateone").value = '';
          document.getElementById("onetihourend").value = '';
          document.getElementById("onetimeminend").value = '';
        }
      });
      
      function magage_trip_type(){
        $("#onetime").attr('checked', 'checked');
        show_trip_type('onetime');
      }
      
      function go_next_page(){ 
        document.frmride.submit();
      }
      
      magage_trip_type();       
  </script>

<?php if ($_smarty_tpl->getVariable('from')->value!=''&&$_smarty_tpl->getVariable('to')->value!=''){?>

<script>
from_to('<?php echo $_smarty_tpl->getVariable('from')->value;?>
', '<?php echo $_smarty_tpl->getVariable('to')->value;?>
');</script>

<?php }?>   

<?php if ($_smarty_tpl->getVariable('roundtriponetime')->value=='Yes'){?>
  
    <script>    
      $("#returndaystime").show(); 
      $('#roundtriponetime').attr("checked", "checked");   
    </script>
  
<?php }else{ ?>
  
    <script>
      $("#returndaystime").hide();       
      $('#roundtriponetime').attr("checked", false); 
    </script>
  
<?php }?>
 
<?php if ($_smarty_tpl->getVariable('triptype')->value!=''){?>

  <script>          
     $('#<?php echo $_smarty_tpl->getVariable('triptype')->value;?>
').attr("checked", "checked");
     show_trip_type('<?php echo $_smarty_tpl->getVariable('triptype')->value;?>
');
  </script>

<?php }?>
<?php if ($_smarty_tpl->getVariable('dateshow')->value=='Yes'){?>

  <script>
    //show_me_dates();
  </script>

<?php }?>
<?php if ($_smarty_tpl->getVariable('roundtripric')->value=='Yes'){?>
  
    <script>    
      $("#returndays").show();
      $("#returntime").show();
      $('#roundtripric').attr("checked", "checked");   
    </script>
  
<?php }else{ ?>
  
    <script>
      $("#returndays").hide();
      $("#returntime").hide();
      $('#roundtripric').attr("checked", false); 
    </script>
  
<?php }?>
<?php if ($_smarty_tpl->getVariable('go_to_second')->value=='Yes'){?>
  
    <script>         
       go_next_page();
    </script>
  
<?php }?>


