<?php /* Smarty version Smarty-3.0.7, created on 2015-05-23 12:55:16
         compiled from "/home/www/xfetch/templates/members/view-ride_form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:49817534655602b5cb89901-64709343%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a9861bbd31dd7996890ab2c2fdc0ff2085582b96' => 
    array (
      0 => '/home/www/xfetch/templates/members/view-ride_form.tpl',
      1 => 1431432577,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '49817534655602b5cb89901-64709343',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
gmap3.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
rrb/zebra_datepicker.src.js"></script>
<link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
rrb/default.css" type="text/css"> 
<?php if ($_smarty_tpl->getVariable('var_err_msg')->value!=''){?>
 
<script>
      showNotification({type : 'error', message: '<?php echo $_smarty_tpl->getVariable('var_err_msg')->value;?>
'});
</script> 

<?php }?>  

<style>
  #map-canvas {
    height: 383px;
    margin-top: 0px;
    padding: 0px;
    width: 370px;
  }  
</style>

<div class="body-inner-part">
  <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
List-Rides-Offer"><?php echo @LBL_RIDES_OFFERED1;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_EDIT_RIDE;?>
 <?php echo @LBL_DETAILS;?>
</span></div>
  <div class="main-inner-page">
    <h2><?php echo @LBL_EDIT_RIDE;?>
<span><?php echo @LBL_DETAILS;?>
</span></h2>
    <div class="offer-seats">      
      <form name="frmride" id="frmride" method="post" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-offer_ride_second">
      <input type="hidden" name="distance" id="distance" value=""> 
      <input type="hidden" name="duration" id="duration" value=""> 
      <input type="hidden" name="from_lat_long" id="from_lat_long" value="">
      <input type="hidden" name="to_lat_long" id="to_lat_long" value="">
      <input type="hidden" name="loc1_lat_long" id="loc1_lat_long" value="">
      <input type="hidden" name="loc2_lat_long" id="loc2_lat_long" value="">
      <input type="hidden" name="loc3_lat_long" id="loc3_lat_long" value="">
      <input type="hidden" name="loc4_lat_long" id="loc4_lat_long" value="">
      <input type="hidden" name="loc5_lat_long" id="loc5_lat_long" value="">
      <input type="hidden" name="loc6_lat_long" id="loc6_lat_long" value="">       
      <div class="offer-seats-left">
        <div class="type-of-trip">
          <h2><?php echo @LBL_TYPE_TRIP;?>
</h2>
          <span>
          <em><?php if ($_smarty_tpl->getVariable('db_memberride')->value[0]['eRideType']=='Reccuring'){?>Reccuring trip<?php }else{ ?>One-time trip<?php }?></em>
          </span> 
        </div>
        <div class="type-of-trip2">
          <h2><?php echo @LBL_ROUTE;?>
</h2>
          <span> <em><?php echo @LBL_FROM;?>
 :  <?php echo $_smarty_tpl->getVariable('db_memberride')->value[0]['vMainDeparture'];?>

           </em>
           <em> <?php echo @LBL_TO;?>
 :  <?php echo $_smarty_tpl->getVariable('db_memberride')->value[0]['vMainArrival'];?>
 <!-- Zohaib .tpl.php -->
          </em>
          <?php if (count($_smarty_tpl->getVariable('routearr')->value)>0){?>
          <em> <?php echo @LBL_RIDE_OFFER_NOTE1;?>

          <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('routearr')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
          <input name="loc<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index'];?>
" id="loc<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index'];?>
" type="text" readonly class="location-input2" value="<?php echo $_smarty_tpl->getVariable('routearr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"/>
          <?php endfor; endif; ?>
          </em>
          <?php }?>
          <!--<em class="last"><a href="#">Add another stopover point</a></em>-->
         </span> </div> 
        <!--------------------Div For Recurring Trip-------------------------------------->         
        <?php if ($_smarty_tpl->getVariable('db_memberride')->value[0]['eRideType']=='Reccuring'){?>
        <div style="display:" id="frmric" class="type-of-trip3">
          <h2>Date and time <div style="float:right;margin-right:5px;font-size:15px;">
           <?php if ($_smarty_tpl->getVariable('db_memberride')->value[0]['eRoundTrip']=='Yes'){?>Round Trip<?php }?>
           <!--<input type="checkbox" name="roundtripric" id="roundtripric" value="Yes">Round Trip-->
           </div>
          </h2>
          <span> 
          <em>
          <?php echo @LBL_OUTBOUND_DAY;?>
:
          <table width="70%">
            <tr>
              <td style="background-color: #ADDE71;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><?php echo $_smarty_tpl->getVariable('db_memberride')->value[0]['vOutBoundDays'];?>
</td>
            </tr>
          </table> 
          </em>
          <?php if ($_smarty_tpl->getVariable('db_memberride')->value[0]['eRoundTrip']=='Yes'){?>
          <em id="returndays">
          <?php echo @LBL_RETURN_DAY;?>
:
          <table width="70%">
            <tr>
              <td style="background-color: #C5F0FF;font-size:13px;height:25px;text-align:center;border:1px solid #D2D2D2;"><?php echo $_smarty_tpl->getVariable('db_memberride')->value[0]['vReturnDays'];?>
</td>
            </tr>
          </table>  
          </em>
          <?php }?>
          <em> <?php echo @LBL_OUTBOUND_TIME;?>
:&nbsp;&nbsp;<?php echo $_smarty_tpl->getVariable('db_memberride')->value[0]['vMainOutBoundTime'];?>

          </em> 
          <?php if ($_smarty_tpl->getVariable('db_memberride')->value[0]['eRoundTrip']=='Yes'){?>
          <em id="returntime"> <?php echo @LBL_RETURN_TIME;?>
:&nbsp;&nbsp;<?php echo $_smarty_tpl->getVariable('db_memberride')->value[0]['vMainReturnTime'];?>
         
          </em> 
          <?php }?>
          <em><?php echo @LBL_START_FROM_DATE;?>
:&nbsp;&nbsp;<?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTimeFormat($_smarty_tpl->getVariable('db_memberride')->value[0]['dStartDate']);?>

          </em>             
          <em><?php echo @LBL_END_TO_DATE;?>
:&nbsp;&nbsp;<?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTimeFormat($_smarty_tpl->getVariable('db_memberride')->value[0]['dEndDate']);?>

          </em> 
          <em>
          <a class="cont-bot-but" href="javascript:void(0);" onClick="show_me_dates();" style="float: left;">View All Dates</a>
          </em>
          <em id="rb" class="last"></em>
          </span> 
        </div>  
        <?php }?>              
        <!--------------------Div For Recurring Trip-------------------------------------->
        <!--------------------Div For One Time Trip-------------------------------------->
        <?php if ($_smarty_tpl->getVariable('db_memberride')->value[0]['eRideType']=='One Time'){?>
        <div style="display:;" id="frmonetime" class="type-of-trip3">
          <h2><?php echo @LBL_DATE_TIME;?>

           <div style="float:right;margin-right:5px;font-size:15px;">
            <?php if ($_smarty_tpl->getVariable('db_memberride')->value[0]['eRoundTrip']=='Yes'){?>Round Trip<?php }?>
            <!--<input type="checkbox" name="roundtriponetime" id="roundtriponetime" value="Yes">Round Trip-->
           </div></h2>
          <span> <em> <?php echo @LBL_DEPARTURE_DATE;?>
:&nbsp;&nbsp;
          <?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTimeFormat($_smarty_tpl->getVariable('db_memberride')->value[0]['dDepartureDate']);?>
 - 
          <?php echo $_smarty_tpl->getVariable('db_memberride')->value[0]['vMainDepartureTime'];?>

          </em> 
          <?php if ($_smarty_tpl->getVariable('db_memberride')->value[0]['eRoundTrip']=='Yes'){?>
          <em id="returndaystime" class="last"> <?php echo @LBL_RETURN_DATE;?>
:
          <?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTimeFormat($_smarty_tpl->getVariable('db_memberride')->value[0]['dArrivalDate']);?>
 - 
          <?php echo $_smarty_tpl->getVariable('db_memberride')->value[0]['vMainArrivalTime'];?>

          </em>
          <?php }?> 
          </span> 
          </div> 
         <?php }?> 
         <!--------------------Div For One Time Trip-------------------------------------->         
        <!--<a class="cont-bot-but" href="javascript:void(0);" onClick="go_next_page();">Continue</a>-->
        <div id="pricing" class="offer-a-ride">
          <h2><?php echo @LBL_PRICE_PER_PESSANGER;?>
</h2>
          <?php if (count($_smarty_tpl->getVariable('db_ride_price')->value)>1){?>
          <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_ride_price')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
          <span class="spa-off">
          <em class="ord1"><?php echo $_smarty_tpl->getVariable('db_ride_price')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vStartPoint'];?>
 &rarr; <?php echo $_smarty_tpl->getVariable('db_ride_price')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vEndPoint'];?>
</em>
          <p class="ord2">           
            <?php echo $_smarty_tpl->getVariable('db_ride_price')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fPrice'];?>

          </p>
          </span>
          <?php endfor; endif; ?>
          <?php }?>
          <div style="display:">   
          <span class="spa-off" style="color:#0094C8;"><em class="ord1"> <?php echo $_smarty_tpl->getVariable('db_memberride')->value[0]['vMainDeparture'];?>
 &rarr; <?php echo $_smarty_tpl->getVariable('db_memberride')->value[0]['vMainArrival'];?>
</em>
          <p class="ord2">
          <?php echo $_smarty_tpl->getVariable('db_total_price')->value;?>

          </p>
          </span>
          </div>  
        </div> 
        
        <!------------------------------Car Details----------------------------------->
         <div id="pricing" class="offer-a-ride"> 
        <h2><?php echo @LBL_CAR_DETAILS;?>
</h2> 
          <span class="spa-off"> <em class="ord1" style="width: 62%;"><?php echo @LBL_SELECT_CAR;?>
:</em>
          <p class="ord2">  
          <?php echo $_smarty_tpl->getVariable('db_car')->value[0]['vMake'];?>
 <?php echo $_smarty_tpl->getVariable('db_car')->value[0]['vTitle'];?>
        
          </p>
          </span>       
          <span class="spa-off"> <em class="ord1"><?php echo @LBL_NUMBER_SEATS_OFFERED;?>
:</em>
          <p class="ord2">          
            <?php echo $_smarty_tpl->getVariable('db_memberride')->value[0]['iSeats'];?>

          </p>
          </span>  
        </div>
        <!------------------------------Car Details----------------------------------->
        <!------------------------------Further Details----------------------------------->
        <div class="offer-a-ride3">
          <h2><?php echo @LBL_FURTHER_DETAILS;?>
</h2>
          <span>
          <!--<p>
            <input name="" type="checkbox" value="" />
            &nbsp;Publish the same comment for the departure and the return </p>-->
          <p>
            <?php echo $_smarty_tpl->getVariable('db_memberride')->value[0]['tDetails'];?>
  </p>
          <p>&nbsp;</p>
          <p class="offer-ride1"><?php echo @LBL_MAX_LUGGAGE_SIZE;?>
:&nbsp;&nbsp;&nbsp;&nbsp;
            <?php echo $_smarty_tpl->getVariable('db_memberride')->value[0]['eLuggageSize'];?>
  
          </p>
          <p class="offer-ride1"><?php echo @LBL_I_WILL_LEAVE;?>
:&nbsp;&nbsp;&nbsp;&nbsp;
            <?php if ($_smarty_tpl->getVariable('db_memberride')->value[0]['eLeaveTime']=='ON_TIME'){?>Right on time
            <?php }elseif($_smarty_tpl->getVariable('db_memberride')->value[0]['eLeaveTime']=='FIFTEEN_MINUTES'){?>In a 15 minute window
            <?php }elseif($_smarty_tpl->getVariable('db_memberride')->value[0]['eLeaveTime']=='THIRTY_MINUTES'){?>In a 30 minute window
            <?php }elseif($_smarty_tpl->getVariable('db_memberride')->value[0]['eLeaveTime']=='ONE_HOUR'){?>In a 1 hour window
            <?php }elseif($_smarty_tpl->getVariable('db_memberride')->value[0]['eLeaveTime']=='TWO_HOURS'){?>In a 2 hour window
            <?php }?>
          </p>
          <p class="offer-ride1"><?php echo @LBL_I_MAKE_DETOUR;?>
:&nbsp;&nbsp;&nbsp;&nbsp;
            <?php if ($_smarty_tpl->getVariable('db_memberride')->value[0]['eWaitTime']=='NONE'){?>None
            <?php }elseif($_smarty_tpl->getVariable('db_memberride')->value[0]['eWaitTime']=='FIFTEEN_MINUTES'){?>15 minute detour, max.
            <?php }elseif($_smarty_tpl->getVariable('db_memberride')->value[0]['eWaitTime']=='THIRTY_MINUTES'){?>30 minute detour, max.
            <?php }elseif($_smarty_tpl->getVariable('db_memberride')->value[0]['eWaitTime']=='WHATEVER_IT_TAKES'){?>Anything is fine
            <?php }?>
          </p>
     <!-- <p class="offer-ride1"><?php echo @LBL_LADIES_ONLY;?>
:&nbsp;&nbsp;&nbsp;&nbsp;
            <?php echo $_smarty_tpl->getVariable('db_memberride')->value[0]['eLadiesOnly'];?>

          </p>
          <?php if ($_smarty_tpl->getVariable('db_memberride')->value[0]['eRidePlaceType']!=''){?>
          <p class="offer-ride1"><?php echo @LBL_RIDE_PLACE;?>
:&nbsp;&nbsp;&nbsp;&nbsp;
            <?php echo $_smarty_tpl->getVariable('db_memberride')->value[0]['eRidePlaceType'];?>

          </p>
          <?php }?>-->
          </span> </div>
        <!------------------------------Further Details----------------------------------->
      </div> 
      </form>
      <div class="offer-seats-right"><div id="map-canvas" class="gmap3"></div></div>
    </div>
  </div>
  <div style="clear:both;"></div>
</div>

<div style="display:none">
  <div id="searchmodel" class="form-login">
    <h4>Add a trip</h4>
    <form method="post" id="add_box" name="add_box">
    <input type="hidden" value="" id="addtype" name="addtype">
    <input type="hidden" value="" id="adddate" name="adddate">  
    <input type="hidden" value="add_date" id="add_action" name="add_action">
    <div class="inner-form1">        
      <div class="singlerow-login" style="background-color:#ADDE71;width:130px;padding: 10px;border: 1px solid #D2D2D2;font-size: 16px;">
        <input type="checkbox" value="addout" id="addout" name="addout">&nbsp;<?php echo @LBL_OURBOUND;?>

      </div> 
      <div class="singlerow-login" style="background-color:#C5F0FF;width:130px;padding: 10px;border: 1px solid #D2D2D2;font-size: 16px;">
        <input type="checkbox" value="addret" id="addret" name="addret">&nbsp;<?php echo @LBL_RETURN;?>

      </div>  
      <div class="singlerow-login-log"><a href="javascript:void(0);" onclick="check_add();">Add</a></div>
      <div style="clear:both;"></div>
    </div>
    </form>           
  </div>
</div>

  <script type="text/javascript">
      function clearThat(){ 
          var opts = {};
          opts.name = ["marker", "directionsrenderer"];
          opts.first = true;        
          $('#map-canvas').gmap3({clear:opts});         
      }
      
      function from_to(){
            clearThat();
            
            var waypts = [];
            var loc1 = '<?php echo $_smarty_tpl->getVariable('loc1')->value;?>
';
            var loc2 = '<?php echo $_smarty_tpl->getVariable('loc2')->value;?>
';
            var loc3 = '<?php echo $_smarty_tpl->getVariable('loc3')->value;?>
';
            var loc4 = '<?php echo $_smarty_tpl->getVariable('loc4')->value;?>
';
            var loc5 = '<?php echo $_smarty_tpl->getVariable('loc5')->value;?>
';
            var loc6 = '<?php echo $_smarty_tpl->getVariable('loc6')->value;?>
';
            
            if(loc1 != ''){
            waypts.push({location:loc1, stopover:true});
            }
            if(loc2 != ''){
            waypts.push({location:loc2, stopover:true});
            }
            if(loc3 != ''){
            waypts.push({location:loc3, stopover:true});
            }
            if(loc4 != ''){
            waypts.push({location:loc4, stopover:true});
            }
            if(loc5 != ''){
            waypts.push({location:loc5, stopover:true});
            }
            if(loc6 != ''){
            waypts.push({location:loc6, stopover:true});
            }
            
            $("#map-canvas").gmap3({
            getroute:{
            options:{
            origin:'<?php echo $_smarty_tpl->getVariable('db_memberride')->value[0]['vMainDeparture'];?>
',
            destination:'<?php echo $_smarty_tpl->getVariable('db_memberride')->value[0]['vMainArrival'];?>
',
            waypoints:waypts,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
            },
            callback: function(results){
            if (!results) return;
            $(this).gmap3({
            map:{
            options:{
            zoom: 13,
            center: ['<?php echo $_smarty_tpl->getVariable('MAP_LATITUDE')->value;?>
', '<?php echo $_smarty_tpl->getVariable('MAP_LONGITUDE')->value;?>
']             
            }
            },
            directionsrenderer:{
            options:{
            directions:results
            }
            }
            });
            }
            }
            });
            //$('#map').show();
        }
  </script>
  <script>
      function show_me_dates(){
        var request = $.ajax({  
      	  type: "POST",
      	  url: site_url+'index.php?file=c-ride_dates',  
      	  data: $("#frmride").serialize(), 	  
      	  
      	  success: function(data) {//alert(data);  
      	   $("#rb").html(data);
      		}
    	 });                                        
    	
       request.fail(function(jqXHR, textStatus) {
        alert( "Request failed: " + textStatus ); 
       });
      }
      
      function select_date(type, date){
        document.getElementById("addtype").value = type;
        document.getElementById("adddate").value = date;
        if(type == 'out'){
         document.getElementById("addout").checked=true;
         document.getElementById("addret").checked=false;
        }else if(type == 'ret'){
         document.getElementById("addout").checked=false;
         document.getElementById("addret").checked=true;
        }else if(type == 'both'){
         document.getElementById("addout").checked=true;
         document.getElementById("addret").checked=true;
        }else{
         document.getElementById("addout").checked=false;
         document.getElementById("addret").checked=false;  
        }
        $.fancybox("#searchmodel");return false;
      }
      
      function check_add(){
        var request = $.ajax({  
      	  type: "POST",
      	  url: site_url+'index.php?file=c-add_date', 
      	  data: $("#add_box").serialize(), 	  
      	  
      	  success: function(data) { //alert(data);     		 
            $.fancybox.close();
            show_me_dates();			
      		}
    	  });
        
        request.fail(function(jqXHR, textStatus) {
    	   loading_hide() ; 
         alert( "Request failed: " + textStatus ); 
    	  });
      }
      
      $('#sdate').Zebra_DatePicker({direction:  [1, 180], pair: $('#edate'),onSelect: function(view, elements){if(document.getElementById("edate").value != ''){show_me_dates();}}});  
      $('#edate').Zebra_DatePicker({direction:  [1, 180],onSelect: function(view, elements){if(document.getElementById("sdate").value != ''){show_me_dates();}}});
      
      $('#sdateone').Zebra_DatePicker({direction:  [1, 180], pair: $('#edateone')});  
      $('#edateone').Zebra_DatePicker({direction:  [1, 180]});
      
      function show_trip_type(type){
        if(type == 'onetime'){
         $("#frmric").hide();
         $("#frmonetime").show();
        }else{
         $("#frmonetime").hide();
         $("#frmric").show();
        }
      }
      
     /* $("#roundtripric").on( "click", function() {
        if($("#roundtripric").is(':checked')){
          $("#returndays").show();
          $("#returntime").show();
        }else{
          $("#returndays").hide();
          $("#returntime").hide();              
        }
      });
      
      $("#roundtriponetime").on( "click", function() {
        if($("#roundtriponetime").is(':checked')){
          $("#returndaystime").show();
        }else{
          $("#returndaystime").hide();
          document.getElementById("edateone").value = '';
          document.getElementById("onetihourend").value = '';
          document.getElementById("onetimeminend").value = '';
        }
      });
      
      function magage_trip_type(){
        $("#onetime").attr('checked', 'checked');
        show_trip_type('onetime');
      }
      
      function go_next_page(){
        document.frmride.submit();
      }
      
      magage_trip_type();       */
  </script>


<script>
from_to('<?php echo $_smarty_tpl->getVariable('db_memberride')->value[0]['vMainDeparture'];?>
', '<?php echo $_smarty_tpl->getVariable('db_memberride')->value[0]['vMainArrival'];?>
');</script>

