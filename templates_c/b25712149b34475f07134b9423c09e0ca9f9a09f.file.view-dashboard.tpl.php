<?php /* Smarty version Smarty-3.0.7, created on 2015-06-10 15:45:28
         compiled from "/home/www/xfetch/templates/members/view-dashboard.tpl" */ ?>
<?php /*%%SmartyHeaderCode:173342795655780e405ebf48-33055549%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b25712149b34475f07134b9423c09e0ca9f9a09f' => 
    array (
      0 => '/home/www/xfetch/templates/members/view-dashboard.tpl',
      1 => 1433773415,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '173342795655780e405ebf48-33055549',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_truncate')) include '/home/www/xfetch/libraries/general/smarty/plugins/modifier.truncate.php';
?><div class="body-inner-part">
  <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_DASHBOARD;?>
</span></div>
  <div class="main-inner-page">
    <h2><?php echo @LBL_DASHBOARD;?>
</h2>
    <div class="dashbord">
      <?php $_template = new Smarty_Internal_Template("member_top.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
     <?php if ($_smarty_tpl->getVariable('img')->value=='No'){?><div class="dashboard-callingimag"><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
profile-photo"><?php echo @LBL_ADD_PHOTO;?>
</a></div><?php }?>
      <div class="dashboard-calling"> 
        <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
profile-photo">
          <img src="<?php echo $_smarty_tpl->getVariable('member_image')->value;?>
" alt=""/>
          <?php if ($_smarty_tpl->getVariable('img')->value=='No'){?>                         
            <div class="text" <?php if ($_smarty_tpl->getVariable('sess_lang')->value=='RS'){?>style="font-size:9px;"<?php }?><?php if ($_smarty_tpl->getVariable('sess_lang')->value=='LT'||$_smarty_tpl->getVariable('sess_lang')->value=='LV'){?>style="font-size:12px;"<?php }?>><?php echo nl2br(@LBL_ADD_PHOTO);?>
</div>
          <?php }?>  
        </a> 
        <h2><?php echo @LBL_HELLO;?>
 <?php echo $_smarty_tpl->getVariable('sess_vFirstName')->value;?>
 <?php echo $_smarty_tpl->getVariable('sess_vLastName')->value;?>
<span><br />
          <!-- <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
personal-information"><?php echo @LBL_EDIT_YOUR_PROFILE;?>
 </a></span></h2> -->
          <!--<p style="color:#44AA00;font-size:15px;"><?php echo @LBL_BALANCE;?>
 : <?php if ($_smarty_tpl->getVariable('total_balance')->value!=0.00){?><?php echo $_smarty_tpl->getVariable('total_balance')->value;?>
 Euros<?php }else{ ?>0 Euro<?php }?></p>-->
      </div>
      <div class="dashbord-left-part">
          <div class="dashbord-left-in">
          <h1><?php echo @LBL_USEFUL_LINKS;?>
</h1>
          <ul>
            <li>&rarr;&nbsp;&nbsp;<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
leave-Rating"><?php echo @LBL_LEAVE_RATING;?>
 </a></li>
            <li>&rarr;&nbsp;&nbsp;<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-user_profile&iMemberId=<?php echo $_smarty_tpl->getVariable('sess_iMemberId')->value;?>
"><?php echo @LBL_YOUR_PUBLIC_PROFILE;?>
 </a></li>
          </ul>
        </div>
        <div class="dashbord-left-in">
          <h1><?php echo @LBL_MEMBER_VERIFICATION;?>
</h1>
          <ul>
            <?php if ($_smarty_tpl->getVariable('PHONE_VERIFICATION_REQUIRED')->value=='Yes'){?>
            <li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
phone-right-i.png" alt="" />&nbsp;&nbsp;<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
Member-Varification"><?php echo @LBL_PHONE_NUMBER_VERIFICATION;?>
</a> <em><?php if ($_smarty_tpl->getVariable('db_email_verification')->value[0]['ePhoneVerified']=='No'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/verification-no-check.png" alt="" /><?php }else{ ?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/verification-checked.png" alt="" /><?php }?></em></li>
            <?php }?>
            <li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
email-right-i.png" alt="" />&nbsp;&nbsp;<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
Member-Varification"><?php echo @LBL_EMAIL_VARIFICATION;?>
</a> <em><?php if ($_smarty_tpl->getVariable('db_email_verification')->value[0]['eEmailVarified']=='No'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/verification-no-check.png" alt="" /><?php }else{ ?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/verification-checked.png" alt="" /><?php }?></em></li>
            <li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
email-right-i.png" alt="" />&nbsp;&nbsp;<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
Member-Varification"><?php echo @LBL_PAYPALEMAIL_VARIFICATION;?>
</a> <em><?php if ($_smarty_tpl->getVariable('db_email_verification')->value[0]['ePaymentEmailVerified']=='No'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
verification-no-check.png" alt="" /><?php }else{ ?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/verification-checked.png" alt="" /><?php }?></em></li>
			
            <!-- 
			<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
fb-right-i.png" alt="" />&nbsp;&nbsp;<?php echo $_smarty_tpl->getVariable('db_email_verification')->value[0]['vFbFriendCount'];?>
 <?php echo @LBL_FRIENDS;?>
 <em><?php if ($_smarty_tpl->getVariable('db_email_verification')->value[0]['vFbFriendCount']==''){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/verification-no-check.png" alt="" /><?php }else{ ?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/verification-checked.png" alt="" /><?php }?></em></li>
			-->
          </ul>
        </div>
        <div class="dashbord-left-in">
          <h1><?php echo @LBL_MEMBER_ACTIVITY;?>
</h1>
          <ul>
            <li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
rides-right-i.png" alt="" />&nbsp;&nbsp;<?php echo $_smarty_tpl->getVariable('tot_rides')->value;?>
 <?php echo @LBL_RIDES_OFFERED;?>
</li>
            <li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
member-right-i.png" alt="" />&nbsp;&nbsp;<?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTimeFormat($_smarty_tpl->getVariable('db_email_verification')->value[0]['dAddedDate']);?>
</li>
            <?php if ($_smarty_tpl->getVariable('tot_notofications')->value>'0'){?>
             <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
Member-Varification" class="active1"><?php echo @LBL_COMPLETE_MEM_VERIFICATION;?>
</a></li> 
            <?php }?> 
          </ul>
        </div>
        <div style="clear:both;"></div>
      </div>
      <div class="dashbord-right-part">
		 
        <?php if ($_smarty_tpl->getVariable('tot_notofications')->value>'0'){?>
        <h2><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
dashboard-notifications.png" alt="" />&nbsp;&nbsp;<?php echo @LBL_NOTIFICATION;?>
 (<?php echo $_smarty_tpl->getVariable('tot_notofications')->value;?>
)</h2>
        <?php if ($_smarty_tpl->getVariable('db_email_verification')->value[0]['eEmailVarified']=='No'){?>          
        <h3><?php echo @LBL_VERIFY_EMAIL;?>
</h3>
        <form name="frmemailverify" enctype="multipart/form-data" id="frmemailverify" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=m-verification_a" method="post">
        <input type="hidden" name="action" id="action" value="emailverify">
        <input type="hidden" name="vEmail" id="vEmail" value="<?php echo $_SESSION['sess_vEmail'];?>
">
        <input type="hidden" name="vName" id="vName" value="<?php echo $_SESSION['sess_vFirstName'];?>
 <?php echo $_SESSION['sess_vLastName'];?>
">
        </form>
        <p><?php echo @LBL_EMAIL_VERI_NOTE1;?>
 <br />
          <?php echo @LBL_EMAIL_VERI_NOTE2;?>
 <br /><a href="javascript:void(0);" class="link-dashbord" onClick="checkmail();"><?php echo @LBL_VERIFY_EMAIL;?>
</a>
        </p>
        <?php }?>
        <?php if ($_smarty_tpl->getVariable('PHONE_VERIFICATION_REQUIRED')->value=='Yes'){?>
        <?php if ($_smarty_tpl->getVariable('db_email_verification')->value[0]['ePhoneVerified']=='No'){?>                
        <h3><?php echo @LBL_VERIFY_PHONE;?>
</h3>        
        <p><?php echo @LBL_EMAIL_VERI_NOTE1;?>
 <br />
          <?php echo @LBL_EMAIL_VERI_NOTE2;?>
 <br /><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
Member-Varification" class="link-dashbord"><?php echo @LBL_VERIFY_PHONE;?>
</a>
        </p>
        <?php }?>
        <?php }?>
        <?php }?>
        <h2 class="about-head"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
dashboard-messages.png" alt="" />&nbsp;&nbsp;<?php echo @LBL_PRIVATE_MESSAGES;?>
</h2>
        <div class="messages-dashbord">
          <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['name'] = "messages";
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_messages')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["messages"]['total']);
?>
          <label><span><?php echo $_smarty_tpl->getVariable('db_messages')->value[$_smarty_tpl->getVariable('smarty')->value['section']['messages']['index']]['vFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_messages')->value[$_smarty_tpl->getVariable('smarty')->value['section']['messages']['index']]['vLastName'];?>
</span> <span class="stap2"><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
message-details/received/<?php echo $_smarty_tpl->getVariable('db_messages')->value[$_smarty_tpl->getVariable('smarty')->value['section']['messages']['index']]['iMessageId'];?>
"><?php echo smarty_modifier_truncate(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->getVariable('db_messages')->value[$_smarty_tpl->getVariable('smarty')->value['section']['messages']['index']]['tMessage']),50);?>
</a></span> <span class="stap3"><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTimeFormat($_smarty_tpl->getVariable('db_messages')->value[$_smarty_tpl->getVariable('smarty')->value['section']['messages']['index']]['dAddedDate']);?>
</span> </label>
          <?php endfor; endif; ?>
        </div>
		<h2>Welcome to <?php echo $_smarty_tpl->getVariable('SITE_NAME')->value;?>
 !</h2>
		<p>Coming soon</p>
      </div> 
	  
      <div style="clear:both;"></div>
    </div>
    <div style="clear:both;"></div>
  </div><div style="clear:both;"></div>
</div>
 
<script>
function checkmail(){
 document.frmemailverify.submit();
}
</script> 

