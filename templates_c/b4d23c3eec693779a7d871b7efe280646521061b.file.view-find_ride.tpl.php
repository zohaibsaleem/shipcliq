<?php /* Smarty version Smarty-3.0.7, created on 2015-12-24 18:10:30
         compiled from "/home/www/cargosharing1/templates/content/view-find_ride.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1876886029567c3516859067-61071029%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b4d23c3eec693779a7d871b7efe280646521061b' => 
    array (
      0 => '/home/www/cargosharing1/templates/content/view-find_ride.tpl',
      1 => 1434969777,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1876886029567c3516859067-61071029',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_truncate')) include '/home/www/cargosharing1/libraries/general/smarty/plugins/modifier.truncate.php';
?><script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
rrb/zebra_datepicker.src.js"></script>
<link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
rrb/default.css" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_stylesheets'];?>
front/simptip-mini.css" media="screen,projection" />
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
gmap3.js"></script> 
<div class="body-inner-part">
<div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_SEARCH;?>
 <?php echo @LBL_RIDE;?>
</span></div>
<div class="main-inner-page">
  <h2><?php echo @LBL_SEARCH;?>
<span><?php echo @LBL_RIDE;?>
</span></h2>
  <div class="search-car">
    <div class="search-car-top-part"> <span><?php echo @LBL_SEARCH_RIDE;?>
</span>          
      <form name="home_search" id="home_search" method="post" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_list">
        <p>
          <input name="search" id="search" type="hidden" value="place">
          <input name="From_lat_long" id="From_lat_long" type="hidden" value="">
          <input name="To_lat_long" id="To_lat_long" type="hidden" value="">      
          <input name="From" id="From" type="text" placeholder="<?php echo @LBL_FROM;?>
" class="validate[required] search-car-find-cla" value=""/>
          <input placeholder="<?php echo @LBL_TO;?>
" name="To" id="To" type="text" class="validate[required] search-car-find-cla" value=""/>
          <input name="searchdate" id="searchdate" type="text" class="search-car-find-date" value="" placeholder="<?php echo @LBL_DATE;?>
"/>
          <a href="javascript:void(0);" onClick="search_now();"><?php echo @LBL_SEARCH;?>
</a>
        </p>
      </form>
    </div>
    <div class="body-mid-part-search-car">
     <!--<?php if (count($_smarty_tpl->getVariable('ridesarroundyou')->value)>0){?>
      <div class="body-mid-part-inner">
        <div class="rides-around">
          <p><?php echo @LBL_RIDE;?>
<br />
            <?php echo @LBL_AROUND;?>
<br />
            <strong><?php echo @LBL_YOU;?>
</strong></p>
          <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_list&search=arround_you"><?php echo @LBL_VIEW_MORE;?>
</a> </div>
        <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["around"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['name'] = "around";
$_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('ridesarroundyou')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['max'] = (int)2;
$_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['show'] = true;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['max'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["around"]['total']);
?> <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_details&from=home&id=<?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['around']['index']]['iRideId'];?>
&dt=<?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['around']['index']]['strtotime'];?>
&ret=<?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['around']['index']]['return'];?>
">
        <div class="trip-main <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['around']['last']){?> trip-main-last <?php }?>">
          <div class="trip roundcol <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['around']['last']){?> last <?php }?> simptip-position-top simptip-multiline simptip-info simptip-smooth" data-tooltip="<?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['around']['index']]['vMainDeparture'];?>
 - <?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['around']['index']]['vMainArrival'];?>
">
            <div style="float:left;width: 114px;">
              <div style="float:left;width: 114px;"> <img src="<?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['around']['index']]['img'];?>
" alt="" style="margin: 0 0 10px;"/> </div>
              <?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['around']['index']]['pref'];?>
 </div>
            <div class="trip-deails">
              <div class="trip-deails-left">
                <h2><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['around']['index']]['dDate'],10);?>
 - <?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['around']['index']]['time'],12);?>
</h2>
                <p><?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['around']['index']]['vMainDeparture'],10);?>
 - <?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['around']['index']]['vMainArrival'],10);?>
</p>
                <p><span style="display: block; width: 65px; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 0;"> <span style=" position:relative; top:-10px; margin-right: 10px;display: block; width: <?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['around']['index']]['rates'];?>
%; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 -13px;"></span> </span></p>
                <span><?php echo @LBL_CAR;?>
: <?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['around']['index']]['membercar'];?>
</span> </div>
              <div class="trip-deails-right"><?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['around']['index']]['price'];?>
</div>
            </div>
          </div>
          <div style=" clear:both;"></div>
        </div>
        </a> 
        <?php endfor; endif; ?> 
        </div>
      <?php }?>-->
     
      <!--Rides arroundyou Rides ----->
      <?php if (count($_smarty_tpl->getVariable('ridesarroundyou')->value)>0){?>
      <div class="body-mid-part-inner">
        <div class="rides-around">    
          <p><?php echo @LBL_RIDE;?>
<br />
          <?php echo @LBL_AROUND;?>
<br />
          <strong><?php echo @LBL_YOU;?>
</strong>
          </p>
          <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_list&search=arround_you"><?php echo @LBL_VIEW_MORE;?>
</a> </div>
        <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('ridesarroundyou')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = (int)2;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?> <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_details&from=home&id=<?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iRideId'];?>
&dt=<?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['strtotime'];?>
&ret=<?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['return'];?>
">
        <div <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['i']['last']&&count($_smarty_tpl->getVariable('ridesarroundyou')->value)>1){?>class="trip-main trip-main-last"<?php }else{ ?>class="trip-main"<?php }?>>
          <div class="trip roundcol <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['i']['last']){?> last <?php }?> simptip-position-top simptip-multiline simptip-info simptip-smooth" data-tooltip="<?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainDeparture'];?>
 - <?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainArrival'];?>
">
            <div class="ho-box-left2">
           <div class="ho-box-left-img"> <img src="<?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['img'];?>
" alt="" style="margin: 0 0 10px;"/> </div>
              <?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['pref'];?>
 </div>
            <div class="trip-deails">
              <div class="trip-deails-left">
                <h2><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTimeFormat($_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dDate']);?>
 - <?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['time'],12);?>
</h2>
                <p><?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainDeparture'],10);?>
 - <?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainArrival'],10);?>
</p>
                <p> <span style="display: block; width: 65px; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 0;"> <span style="  position:relative; top:-10px; margin-right: 10px;display: block; width: <?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['rating'];?>
%; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 -13px;"></span> </span> </p>
                <span><?php echo @LBL_CAR;?>
: <?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['membercar'],10);?>
</span></div>
              <div class="trip-deails-right" style="font-size:21px;"><?php echo $_smarty_tpl->getVariable('ridesarroundyou')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['price'];?>
</div>
              <div class="trip-deails-right-bot"></div>
            </div>
          </div>
          </a>
          <div style=" clear:both;"></div>
        </div>
        <?php endfor; endif; ?> </div>
      <?php }?>
      <!--Rides arroundyou Rides ends----->
      <!--Airport Rides ----->    
      <?php if ($_smarty_tpl->getVariable('AIRPORTS_RIDES_SHOW')->value=='Yes'){?>
      <?php if (count($_smarty_tpl->getVariable('airportmainarr')->value)>0){?>
      <div class="body-mid-part-inner">
        <div class="rides-around2">
          <p><?php echo @LBL_AIRPORT;?>
<br />
            <strong><?php echo @LBL_RIDE;?>
</strong>
          </p>
          <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_list&search=simple&type=airport"><?php echo @LBL_VIEW_MORE;?>
</a> </div>
        <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('airportmainarr')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = (int)2;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?> <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_details&from=home&id=<?php echo $_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iRideId'];?>
&dt=<?php echo $_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['strtotime'];?>
&ret=<?php echo $_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['return'];?>
">
        <div <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['i']['last']&&count($_smarty_tpl->getVariable('airportmainarr')->value)>1){?>class="trip-main trip-main-last"<?php }else{ ?>class="trip-main"<?php }?>>
          <div class="trip airportcol <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['i']['last']){?> last <?php }?> simptip-position-top simptip-multiline simptip-info simptip-smooth" data-tooltip="<?php echo $_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainDeparture'];?>
 - <?php echo $_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainArrival'];?>
">
            <div class="ho-box-left2">
             <div class="ho-box-left-img"><img src="<?php echo $_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['memberimage'];?>
" alt="" style="margin: 0 0 10px;"/> </div>
              <?php echo $_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['pref'];?>
 </div>
            <div class="trip-deails">
              <div class="trip-deails-left">
                <h2><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTimeFormat($_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dDateOut']);?>
 - <?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['deptime'],12);?>
</h2>
                <p><?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainDeparture'],10);?>
 - <?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainArrival'],10);?>
</p>
                <p> <span style="display: block; width: 65px; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 0;"> <span style=" position:relative; top:-10px; margin-right: 10px;display: block; width: <?php echo $_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['rating'];?>
%; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 -13px;"></span> </span></p>
                <span><?php echo @LBL_CAR;?>
: <?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['membercar'],15);?>
</span> </div>
              <div class="trip-deails-right" style="font-size:21px;"><?php echo $_smarty_tpl->getVariable('airportmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['totprice'];?>
</div>
              <div class="trip-deails-right-bot"></div>
            </div>
          </div>
          <div style=" clear:both;"></div>
        </div>
        </a> <?php endfor; endif; ?>
      </div>
      <?php }?>
      <?php }?>
      <!--Airport Rides ends----->
                               
      <!--Ladies Only Rides ----->
      <?php if ($_smarty_tpl->getVariable('LADIES_ONLY_SHOW')->value=='Yes'){?>
      <?php if (count($_smarty_tpl->getVariable('ladiesmainarr')->value)>0){?>
      <div class="body-mid-part-inner">
        <div class="rides-around3">
          <p><?php echo @LBL_LADIES;?>
<br />
          <strong><?php echo @LBL_ONLY;?>
</strong>
          </p>
          <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_list&search=simple&type=ladiesonly"><?php echo @LBL_VIEW_MORE;?>
</a> </div>
        <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('ladiesmainarr')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = (int)2;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?> <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_details&from=home&id=<?php echo $_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iRideId'];?>
&dt=<?php echo $_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['strtotime'];?>
&ret=<?php echo $_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['return'];?>
">
        <div <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['i']['last']&&count($_smarty_tpl->getVariable('ladiesmainarr')->value)>1){?>class="trip-main trip-main-last"<?php }else{ ?>class="trip-main"<?php }?>>
          <div class="trip ladiescol <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['i']['last']){?> last <?php }?> simptip-position-top simptip-multiline simptip-info simptip-smooth" data-tooltip="<?php echo $_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainDeparture'];?>
 - <?php echo $_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainArrival'];?>
">
                        <div class="ho-box-left2">
             <div class="ho-box-left-img"> <img src="<?php echo $_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['memberimage'];?>
" alt="" style="margin: 0 0 10px;"/> </div>
              <?php echo $_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['pref'];?>
 </div>
            <div class="trip-deails">
              <div class="trip-deails-left">
                <h2><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTimeFormat($_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dDateOut']);?>
 - <?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['deptime'],12);?>
</h2>
                <p><?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainDeparture'],10);?>
 - <?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainArrival'],10);?>
</p>
                <p> <span style="display: block; width: 65px; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 0;"> <span style=" position:relative; top:-10px; margin-right: 10px;display: block; width: <?php echo $_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['rating'];?>
%; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 -13px;"></span> </span> </p>
                <span><?php echo @LBL_CAR;?>
: <?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['membercar'],15);?>
</span> </div>
              <div class="trip-deails-right" style="font-size:21px;"><?php echo $_smarty_tpl->getVariable('ladiesmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['totprice'];?>
</div>
              <div class="trip-deails-right-bot"></div>
            </div>
          </div>
          <div style=" clear:both;"></div>
        </div>
        </a> <?php endfor; endif; ?> 
        </div>
      <?php }?>
      <?php }?>
      <!--Ladies Only Rides Ends----->
      
      <!--Shopping Rides----->
      <?php if ($_smarty_tpl->getVariable('SHOPPING_RIDES_SHOW')->value=='Yes'){?>
      <?php if (count($_smarty_tpl->getVariable('shoppingmainarr')->value)>0){?>
      <div class="body-mid-part-inner">
        <div class="rides-around4">
          <p><?php echo @LBL_SHOPPING;?>
<br />
            <strong><?php echo @LBL_RIDE;?>
</strong>
          </p>
          <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_list&search=simple&type=shopping"><?php echo @LBL_VIEW_MORE;?>
</a> </div>
        <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('shoppingmainarr')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = (int)2;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?> <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_details&from=home&id=<?php echo $_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iRideId'];?>
&dt=<?php echo $_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['strtotime'];?>
&ret=<?php echo $_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['return'];?>
">
        <div <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['i']['last']&&count($_smarty_tpl->getVariable('shoppingmainarr')->value)>1){?>class="trip-main trip-main-last"<?php }else{ ?>class="trip-main"<?php }?>>
          <div class="trip shoppingcol <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['i']['last']){?> last <?php }?> simptip-position-top simptip-multiline simptip-info simptip-smooth" data-tooltip="<?php echo $_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainDeparture'];?>
 - <?php echo $_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainArrival'];?>
">
                        <div class="ho-box-left2">
             <div class="ho-box-left-img"> <img src="<?php echo $_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['memberimage'];?>
" alt="" style="margin: 0 0 10px;"/> </div>
              <?php echo $_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['pref'];?>
 </div>
            <div class="trip-deails">
              <div class="trip-deails-left">
                <h2><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTimeFormat($_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dDateOut']);?>
 - <?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['deptime'],12);?>
</h2>
                <p><?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainDeparture'],10);?>
 - <?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vMainArrival'],10);?>
</p>
                <p> <span style="display: block; width: 65px; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 0;"> <span style=" position:relative; top:-10px; margin-right: 10px;display: block; width: <?php echo $_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['rating'];?>
%; height: 13px; background: url(<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
star-rating-sprite.png) 0 -13px;"></span> </span> </p>
                <span><?php echo @LBL_CAR;?>
: <?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['membercar'],15);?>
</span> </div>
              <div class="trip-deails-right" style="font-size:21px;"><?php echo $_smarty_tpl->getVariable('shoppingmainarr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['totprice'];?>
</div>
              <div class="trip-deails-right-bot"></div>
            </div>
          </div>
          <div style=" clear:both;"></div>
        </div>
        </a> <?php endfor; endif; ?> 
        </div>
      <?php }?>
      <?php }?>
      <!--Shopping Rides Ends----->
      
      
      <div style="clear:both;"></div>
    </div>
  </div>
</div>
<div style="clear:both;"></div>
</div>
<div id="map-canvas" class="gmap3"></div>

<script type="text/javascript">
$('#searchdate').Zebra_DatePicker({
      direction: 0
});

function initialize() {
    //var input = document.getElementById("From);     
    //var options = {componentRestrictions: {country: 'us'}};
                 
    var from = new google.maps.places.Autocomplete(document.getElementById('From'));
    var to = new google.maps.places.Autocomplete(document.getElementById('To'));
}  
             
google.maps.event.addDomListener(window, 'load', initialize);

function search_now(){ 
  resp = jQuery("#home_search").validationEngine('validate');    
	if(resp == true){
    if(document.getElementById('From').value !='' && document.getElementById('To').value != ''){
      var From = $("#From").val();
      $("#map-canvas").gmap3({
              getlatlng:{
                address:  From,
                callback: function(results){
                 $("#From_lat_long").val(results[0].geometry.location);
                }
              }
      });
      
      var To = $("#To").val();
      $("#map-canvas").gmap3({
              getlatlng:{
                address:  To,
                callback: function(results){
                 $("#To_lat_long").val(results[0].geometry.location);
                 document.home_search.submit();
                }
              }
      }); 
      return false;
    }else{
      alert('<?php echo @LBL_SELECT_FROM_TO_ADD;?>
');
      return false;
    }
  }else{
    return false;
  }
}

/*function go_get_latlong(from){
   if(from == 'From'){
     var From = $("#From").val();
     $("#map-canvas").gmap3({
            getlatlng:{
              address:  From,
              callback: function(results){
               $("#From_lat_long").val(results[0].geometry.location);
              }
            }
      });      
   }else{
     var To = $("#To").val();
     $("#map-canvas").gmap3({
            getlatlng:{
              address:  To,
              callback: function(results){
               $("#To_lat_long").val(results[0].geometry.location);
              }
            }
      });       
   }
} */
</script>
  <script type="text/javascript">        
    $(function() {
        var from = document.getElementById('From');        
        var autocomplete_from = new google.maps.places.Autocomplete(from);
    		google.maps.event.addListener(autocomplete_from, 'place_changed', function() {
    			var place = autocomplete_from.getPlace();     		
    			go_get_latlong('From');
    		});
    		
    		var to = document.getElementById('To');
    		var autocomplete_to = new google.maps.places.Autocomplete(to);
    		google.maps.event.addListener(autocomplete_to, 'place_changed', function() {
    			var place = autocomplete_to.getPlace();     			
    			go_get_latlong('to');
    		});
    });
      
  </script>
