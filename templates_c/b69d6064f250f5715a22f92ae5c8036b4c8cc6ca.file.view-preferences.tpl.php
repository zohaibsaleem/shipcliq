<?php /* Smarty version Smarty-3.0.7, created on 2015-07-20 15:49:58
         compiled from "/home/www/xfetch/templates/members/view-preferences.tpl" */ ?>
<?php /*%%SmartyHeaderCode:138925922155accb4e871ad7-51518543%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b69d6064f250f5715a22f92ae5c8036b4c8cc6ca' => 
    array (
      0 => '/home/www/xfetch/templates/members/view-preferences.tpl',
      1 => 1436783821,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '138925922155accb4e871ad7-51518543',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_stylesheets'];?>
front/simptip-mini.css" media="screen,projection" />
<?php if ($_smarty_tpl->getVariable('var_msg')->value!=''){?> 
  
    <script> 
      showNotification({type : 'success', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});  
    </script>
  
<?php }?>

<?php if ($_smarty_tpl->getVariable('var_err_msg')->value!=''){?> 
  
    <script>
      showNotification({type : 'error', message: '<?php echo $_smarty_tpl->getVariable('var_err_msg')->value;?>
'});
    </script>
  
<?php }?> 

<div class="body-inner-part">
<div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_SHARE_PREFERENCE;?>
</span></div>
<div class="main-inner-page">
  <h2><?php echo @LBL_CAR_SHARE;?>
 <span><?php echo @LBL_PREFERENCES;?>
</span></h2>
  <?php $_template = new Smarty_Internal_Template("member_top.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
  <div class="ratings">
    <form name="prefrm" id="prefrm" method="post" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
car-preferences">
    <input type="hidden" name="action" id="action" value="edit">
    <div class="car-share-preferences">
      <h3><?php echo @LBL_MY_CAR_REFERENCE;?>
</h3>
      
      <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['name'] = "pref";
$_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_preferences')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['total']);
?>
        <label><em><?php echo $_smarty_tpl->getVariable('db_preferences')->value[$_smarty_tpl->getVariable('smarty')->value['section']['pref']['index']]['vTitle'];?>
:</em>
        <span class="simptip-position-top" ><img id="img_vYes_<?php echo $_smarty_tpl->getVariable('db_preferences')->value[$_smarty_tpl->getVariable('smarty')->value['section']['pref']['index']]['iPreferencesId'];?>
" src="<?php echo $_smarty_tpl->getVariable('db_preferences')->value[$_smarty_tpl->getVariable('smarty')->value['section']['pref']['index']]['vYes_img'];?>
" alt="" onClick="checked_me('vYes', <?php echo $_smarty_tpl->getVariable('db_preferences')->value[$_smarty_tpl->getVariable('smarty')->value['section']['pref']['index']]['iPreferencesId'];?>
);"/></span> 
        <span class="simptip-position-top" ><img id="img_vMAYBE_<?php echo $_smarty_tpl->getVariable('db_preferences')->value[$_smarty_tpl->getVariable('smarty')->value['section']['pref']['index']]['iPreferencesId'];?>
" src="<?php echo $_smarty_tpl->getVariable('db_preferences')->value[$_smarty_tpl->getVariable('smarty')->value['section']['pref']['index']]['vMAYBE_img'];?>
" alt="" onClick="checked_me('vMAYBE', <?php echo $_smarty_tpl->getVariable('db_preferences')->value[$_smarty_tpl->getVariable('smarty')->value['section']['pref']['index']]['iPreferencesId'];?>
);"/></span> 
        <span class="simptip-position-top"><img id="img_vNO_<?php echo $_smarty_tpl->getVariable('db_preferences')->value[$_smarty_tpl->getVariable('smarty')->value['section']['pref']['index']]['iPreferencesId'];?>
" src="<?php echo $_smarty_tpl->getVariable('db_preferences')->value[$_smarty_tpl->getVariable('smarty')->value['section']['pref']['index']]['vNO_img'];?>
" alt="" onClick="checked_me('vNO', <?php echo $_smarty_tpl->getVariable('db_preferences')->value[$_smarty_tpl->getVariable('smarty')->value['section']['pref']['index']]['iPreferencesId'];?>
);"/></span> 
        </label>
         
        <span style="display:none;">
        <input type="radio" name="vYes_<?php echo $_smarty_tpl->getVariable('db_preferences')->value[$_smarty_tpl->getVariable('smarty')->value['section']['pref']['index']]['iPreferencesId'];?>
" id="vYes_<?php echo $_smarty_tpl->getVariable('db_preferences')->value[$_smarty_tpl->getVariable('smarty')->value['section']['pref']['index']]['iPreferencesId'];?>
" value="vYes">
        <input type="radio" name="vMAYBE_<?php echo $_smarty_tpl->getVariable('db_preferences')->value[$_smarty_tpl->getVariable('smarty')->value['section']['pref']['index']]['iPreferencesId'];?>
" id="vMAYBE_<?php echo $_smarty_tpl->getVariable('db_preferences')->value[$_smarty_tpl->getVariable('smarty')->value['section']['pref']['index']]['iPreferencesId'];?>
" value="vMAYBE">
        <input type="radio" name="vNO_<?php echo $_smarty_tpl->getVariable('db_preferences')->value[$_smarty_tpl->getVariable('smarty')->value['section']['pref']['index']]['iPreferencesId'];?>
" id="vNO_<?php echo $_smarty_tpl->getVariable('db_preferences')->value[$_smarty_tpl->getVariable('smarty')->value['section']['pref']['index']]['iPreferencesId'];?>
" value="vNO">
        </span>  
      <?php endfor; endif; ?>
      
      <!--<label><em><?php echo @LBL_CHATTINESS;?>
:</em>
      <span class="simptip-position-top" data-tooltip="<?php echo @LBL_I_LOVE_CHAT;?>
"><img <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eChattiness']=='YES'){?>class="borderClass"<?php }?> id="eChattiness_YES_img" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
chat1.png" alt="" onClick="checked_me('eChattiness', 'YES');"/></span> 
      <span class="simptip-position-top" data-tooltip="<?php echo @LBL_TALK_MOOD;?>
"><img <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eChattiness']=='MAYBE'){?>class="borderClass"<?php }?> class="" id="eChattiness_MAYBE_img" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
chat2.png" alt="" onClick="checked_me('eChattiness', 'MAYBE');"/></span> 
      <span class="simptip-position-top simptip-danger" data-tooltip="<?php echo @LBL_QUIET_TYPE;?>
 :)"><img <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eChattiness']=='NO'){?>class="borderClass"<?php }?> class="" id="eChattiness_NO_img" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
chat3.png" alt="" onClick="checked_me('eChattiness', 'NO');"/></span> 
      </label>      
      <label><em><?php echo @LBL_MUSIC;?>
:</em> 
      <span class="simptip-position-top" data-tooltip="<?php echo @LBL_ABOUT_PLAYLIST;?>
"><img <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eMusic']=='YES'){?>class="borderClass"<?php }?> id="eMusic_YES_img" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
music1.png" alt="" onClick="checked_me('eMusic', 'YES');"/></span> 
      <span class="simptip-position-top" data-tooltip="<?php echo @LBL_DEPEND_MOOD;?>
"><img <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eMusic']=='MAYBE'){?>class="borderClass"<?php }?> id="eMusic_MAYBE_img" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
music2.png" alt="" onClick="checked_me('eMusic', 'MAYBE');"/></span> 
      <span class="simptip-position-top simptip-danger" data-tooltip="<?php echo @LBL_SILENCE_GOLDEN;?>
"><img <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eMusic']=='NO'){?>class="borderClass"<?php }?> id="eMusic_NO_img" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
music3.png" alt="" onClick="checked_me('eMusic', 'NO');"/></span>
      </label>
      <label><em><?php echo @LBL_SMOKING;?>
:</em> 
      <span class="simptip-position-top" data-tooltip="<?php echo @LBL_CIGARETTE_SMOKE;?>
"><img <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eSmoking']=='YES'){?>class="borderClass"<?php }?> id="eSmoking_YES_img" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
smoke1.png" alt="" onClick="checked_me('eSmoking', 'YES');"/></span> 
      <span class="simptip-position-top" data-tooltip="<?php echo @LBL_DEPEND_MOOD;?>
"><img <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eSmoking']=='MAYBE'){?>class="borderClass"<?php }?> id="eSmoking_MAYBE_img" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
smoke2.png" alt="" onClick="checked_me('eSmoking', 'MAYBE');"/></span> 
      <span class="simptip-position-top simptip-danger" data-tooltip="<?php echo @LBL_NO_SMOKE;?>
"><img <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eSmoking']=='NO'){?>class="borderClass"<?php }?> id="eSmoking_NO_img" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
smoke3.png" alt="" onClick="checked_me('eSmoking', 'NO');"/></span> 
      </label>
      <label><em>E. <?php echo @LBL_SMOKING;?>
:</em> 
      <span class="simptip-position-top" data-tooltip="E. <?php echo @LBL_CIGARETTE_SMOKE;?>
"><img <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eEcig']=='YES'){?>class="borderClass"<?php }?> id="eEcig_YES_img" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
ecig1.png" alt="" onClick="checked_me('eEcig', 'YES');"/></span> 
      <span class="simptip-position-top" data-tooltip="<?php echo @LBL_DEPEND_MOOD;?>
"><img <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eEcig']=='MAYBE'){?>class="borderClass"<?php }?> id="eEcig_MAYBE_img" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
ecig2.png" alt="" onClick="checked_me('eEcig', 'MAYBE');"/></span> 
      <span class="simptip-position-top simptip-danger" data-tooltip="<?php echo @LBL_NO_SMOKE;?>
"><img <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eEcig']=='NO'){?>class="borderClass"<?php }?> id="eEcig_NO_img" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
ecig3.png" alt="" onClick="checked_me('eEcig', 'NO');"/></span> 
      </label>
      <label><em><?php echo @LBL_PETS;?>
:</em> 
      <span class="simptip-position-top" data-tooltip="<?php echo @LBL_PETS1;?>
"><img <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['ePets']=='YES'){?>class="borderClass"<?php }?> id="ePets_YES_img" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
pets1.png" alt="" onClick="checked_me('ePets', 'YES');"/></span> 
      <span class="simptip-position-top" data-tooltip="<?php echo @LBL_DEPEND_MOOD;?>
"><img <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['ePets']=='MAYBE'){?>class="borderClass"<?php }?> id="ePets_MAYBE_img" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
pets2.png" alt="" onClick="checked_me('ePets', 'MAYBE');"/></span> 
      <span class="simptip-position-top simptip-danger" data-tooltip="<?php echo @LBL_PETS2;?>
"><img <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['ePets']=='NO'){?>class="borderClass"<?php }?> id="ePets_NO_img" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
pets3.png" alt="" onClick="checked_me('ePets', 'NO');"/></span>
      </label>-->
      <span style="display:none;">
      <input type="radio" name="Data[eChattiness]" id="eChattiness_YES" value="YES" <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eChattiness']=='YES'){?> checked <?php }?>>
      <input type="radio" name="Data[eChattiness]" id="eChattiness_MAYBE" value="MAYBE" <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eChattiness']=='MAYBE'){?> checked <?php }?>>
      <input type="radio" name="Data[eChattiness]" id="eChattiness_NO" value="NO" <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eChattiness']=='NO'){?> checked <?php }?>>
      
      <input type="radio" name="Data[eMusic]" id="eMusic_YES" value="YES" <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eMusic']=='YES'){?> checked <?php }?>>
      <input type="radio" name="Data[eMusic]" id="eMusic_MAYBE" value="MAYBE" <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eMusic']=='MAYBE'){?> checked <?php }?>>
      <input type="radio" name="Data[eMusic]" id="eMusic_NO" value="NO" <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eMusic']=='NO'){?> checked <?php }?>>
      
      <input type="radio" name="Data[eSmoking]" id="eSmoking_YES" value="YES" <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eSmoking']=='YES'){?> checked <?php }?>>
      <input type="radio" name="Data[eSmoking]" id="eSmoking_MAYBE" value="MAYBE" <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eSmoking']=='MAYBE'){?> checked <?php }?>>
      <input type="radio" name="Data[eSmoking]" id="eSmoking_NO" value="NO" <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eSmoking']=='NO'){?> checked <?php }?>>
      
      <input type="radio" name="Data[eEcig]" id="eEcig_YES" value="YES" <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eEcig']=='YES'){?> checked <?php }?>>
      <input type="radio" name="Data[eEcig]" id="eEcig_MAYBE" value="MAYBE" <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eEcig']=='MAYBE'){?> checked <?php }?>>
      <input type="radio" name="Data[eEcig]" id="eEcig_NO" value="NO" <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['eEcig']=='NO'){?> checked <?php }?>>
      
      <input type="radio" name="Data[ePets]" id="ePets_YES" value="YES" <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['ePets']=='YES'){?> checked <?php }?>>
      <input type="radio" name="Data[ePets]" id="ePets_MAYBE" value="MAYBE" <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['ePets']=='MAYBE'){?> checked <?php }?>>
      <input type="radio" name="Data[ePets]" id="ePets_NO" value="NO" <?php if ($_smarty_tpl->getVariable('db_pref')->value[0]['ePets']=='NO'){?> checked <?php }?>>
      </span>
      <label><em></em><a href="javascript:void(0);" onClick="submit_frm();"><?php echo @LBL_SAVE;?>
</a></label>
    </div>
    </form>
  </div>
  <?php $_template = new Smarty_Internal_Template("member_profile_left.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
  <div style="clear:both;"></div>
</div>
</div>

  <script>
    function submit_frm(){
       document.prefrm.submit();
    }
    
    function checked_me(type, val){         
      $("#img_vYes_"+val).removeClass('borderClass');
      $("#img_vMAYBE_"+val).removeClass('borderClass');
      $("#img_vNO_"+val).removeClass('borderClass');
      $("#img_"+type+"_"+val).addClass('borderClass');
      
      $("#vYes_"+val).prop("checked", false);
      $("#vMAYBE_"+val).prop("checked", false);
      $("#vNO_"+val).prop("checked", false);
      $("#"+type+"_"+val).prop("checked", true);          
      return false;
    }
  </script>


<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['name'] = "pref";
$_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_member_preferences')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["pref"]['total']);
?>

  <script>
   checked_me('<?php echo $_smarty_tpl->getVariable('db_member_preferences')->value[$_smarty_tpl->getVariable('smarty')->value['section']['pref']['index']]['vType'];?>
', '<?php echo $_smarty_tpl->getVariable('db_member_preferences')->value[$_smarty_tpl->getVariable('smarty')->value['section']['pref']['index']]['iPreferencesId'];?>
'); 
  </script>

<?php endfor; endif; ?>