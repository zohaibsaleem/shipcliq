<?php /* Smarty version Smarty-3.0.7, created on 2015-07-04 11:19:54
         compiled from "/home/www/xfetch/templates/footer4.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6256778345597740285bec1-62868498%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bc6180fb567ed1f19fa81757fab08116c6945415' => 
    array (
      0 => '/home/www/xfetch/templates/footer4.tpl',
      1 => 1435930774,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6256778345597740285bec1-62868498',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div id="footer">
<div class="footer-inner">
  <div class="footer-top-part">
    <div class="footer-top-logo-part">
      <div class="footer-logo"><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><img src="<?php echo $_smarty_tpl->getVariable('logodis')->value;?>
" alt=""></a></div>
      <span> 
		<a href="<?php echo $_smarty_tpl->getVariable('FACEBOOK_LINK')->value;?>
" target="_blank">
			<img onmouseout="this.src='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/fb.png'" onmouseover="this.src='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/fb-hover.png'" onclick="return submitsearch(document.frmsearch);" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/fb.png" alt=""/>
		</a>
		<a href="<?php echo $_smarty_tpl->getVariable('TWITTER_LINK')->value;?>
" target="_blank">
			<img onmouseout="this.src='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/twi.png'" onmouseover="this.src='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/twi-hover.png'" onclick="return submitsearch(document.frmsearch);" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/twi.png" alt=""/>
		</a>
		<a href="<?php echo $_smarty_tpl->getVariable('PINTEREST_LINK')->value;?>
" target="_blank">
			<img onmouseout="this.src='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/p-icon.png'" onmouseover="this.src='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/p-icon-hover.png'" onclick="return submitsearch(document.frmsearch);" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/p-icon.png" alt=""/>
		</a>        
        <a href="<?php echo $_smarty_tpl->getVariable('INSTRAGRAM_LINK')->value;?>
" target="_blank">
			<img onmouseout="this.src='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/instagram.png'" onmouseover="this.src='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/instagram-hover.png'" onclick="return submitsearch(document.frmsearch);" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/instagram.png" alt=""/>
		</a>        
	</span> 
	</div>
    <div class="footer-mid-part">
      <div class="footer-box1">
        <h2><?php echo @LBL_USING;?>
 <?php echo @LBL_CAR_SHARING_WEBSITE;?>
</h2>
        <ul>
		  <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_pages_1')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
		    <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
page/<?php echo $_smarty_tpl->getVariable('db_pages_1')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPageId'];?>
/<?php ob_start();?><?php echo $_smarty_tpl->getVariable('db_pages_1')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPageId'];?>
<?php $_tmp1=ob_get_clean();?><?php echo $_smarty_tpl->getVariable('generalobj')->value->getPageUrlName($_tmp1);?>
" ><?php ob_start();?><?php echo $_smarty_tpl->getVariable('db_pages_1')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPageId'];?>
<?php $_tmp2=ob_get_clean();?><?php echo $_smarty_tpl->getVariable('generalobj')->value->getPageTitle($_tmp2);?>
</a></li>
		  <?php endfor; endif; ?>
		    <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
faqs" <?php if ($_smarty_tpl->getVariable('script')->value=='faqs'){?>class="active"<?php }?>><?php echo @LBL_FAQ;?>
</a></li>
          <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
Member-Stories" <?php if ($_smarty_tpl->getVariable('script')->value=='member_stories'){?>class="active"<?php }?>><?php echo @LBL_MEMBER_STORY;?>
</a></li>
        </ul>
      </div>
      <div class="footer-box2">
        <h2><?php echo @LBL_OUR_COMPANY;?>
</h2>
		  <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_pages_2')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
		   <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
page/<?php echo $_smarty_tpl->getVariable('db_pages_2')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPageId'];?>
/<?php ob_start();?><?php echo $_smarty_tpl->getVariable('db_pages_2')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPageId'];?>
<?php $_tmp3=ob_get_clean();?><?php echo $_smarty_tpl->getVariable('generalobj')->value->getPageUrlName($_tmp3);?>
" ><?php ob_start();?><?php echo $_smarty_tpl->getVariable('db_pages_2')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPageId'];?>
<?php $_tmp4=ob_get_clean();?><?php echo $_smarty_tpl->getVariable('generalobj')->value->getPageTitle($_tmp4);?>
</a></li>
		  <?php endfor; endif; ?>
		   <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
contactus" <?php if ($_smarty_tpl->getVariable('script')->value=='contactus'){?>class="active"<?php }?>><?php echo @LBL_CONTACT_US1;?>
</a></li>
      </div>
      <div class="footer-box3">
        <form name="frmnewsletter" id="frmnewsletter" method="post" action="">
        <div class="footer-search-box">
          <h2><?php echo @LBL_SUBSRIBE;?>
 <?php echo @LBL_NEWSLETTERS;?>
</h2>
          <span>
          <input name="vNewsletterEmail" id="vNewsletterEmail" type="text" class="validate[required,custom[email]] footer-search" placeholder="Enter E-mail Address" />
          <input value="<?php echo @LBL_SUBMIT;?>
" id="newletter_subscribe" name="" type="button" class="search-button" onClick="checknewsletter();" style="display:;"/>
          <input value="<?php echo @LBL_WAIT;?>
"   id="newletter_loading" name="" type="button" class="search-button" onClick="checknewsletter();" style="display:none;"/>
          </span> </div>
        <p><?php echo @LBL_FOOTER_TERMS;?>
 </p>
        </form>  
      </div>
    </div>
  </div>
  <div class="footer-bottom-part">
    <p>
		<?php echo $_smarty_tpl->getVariable('COPY_RIGHT_TEXT')->value;?>

		<!-- <br />
      <?php echo @LBL_SITE_DESIGN_DEVELOP;?>
 : <a href="http://www.esiteworld.com/" target="_blank" class="site-name">eSiteWorld.com</a>
	  -->
	  </p>
  </div>
</div>
</div>

<script>
$('#frmnewsletter').bind('keydown',function(e){
    if(e.which == 13){
      checknewsletter(); return false;
    }
});
</script>  

