<?php /* Smarty version Smarty-3.0.7, created on 2015-02-12 11:59:40
         compiled from "/home/www/blablaclone/templates/members/view-notification.tpl" */ ?>
<?php /*%%SmartyHeaderCode:168314079754dc4854bd2c99-09878072%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bdcdaabb20788d97bdb8aa0f28a619ccf6d86ef2' => 
    array (
      0 => '/home/www/blablaclone/templates/members/view-notification.tpl',
      1 => 1395812662,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '168314079754dc4854bd2c99-09878072',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_GET['var_msg']!=''){?>          
<?php if ($_GET['msg_code']=='1'){?>
  
    <script>        
      showNotification({type : 'success', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});  
    </script>
  
<?php }?>
<?php }?> 

<?php if ($_GET['var_msg']!=''){?>          
<?php if ($_GET['msg_code']=='0'){?>
  
    <script>
      showNotification({type : 'error', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});
    </script>
  
<?php }?>
<?php }?> 
<div class="body-inner-part">
  <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_NOTIFICATION1;?>
</span></div>
  <!-------------------------inner-page----------------->
  <div class="main-inner-page">
    <h2><?php echo @LBL_NOTIFICATION1;?>
 </h2>
    <?php $_template = new Smarty_Internal_Template("member_top.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
    <?php $_template = new Smarty_Internal_Template("member_profile_left.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
    <div class="ratings">
    <!--
      <div class="notifications">
        <div class="notifications-top-part">
          <h2>SMS <?php echo @LBL_NOTIFICATION1;?>
 <span>/ <?php echo @LBL_SEND_TO;?>
 <strong>07400 215478</strong> </span></h2>
          <p><strong><?php echo @LBL_RECEIVE_SMS;?>
 :</strong> <em>
            <input name="" type="checkbox" value="" />
            &nbsp;&nbsp;<?php echo @LBL_NOTIFICATION_NOTE;?>
 </em> <a href="#"><?php echo @LBL_SAVE;?>
</a> </p>
        </div>
      </div>
      -->
      <div class="notifications">
        <div class="notifications-bottom-part">
        <form name="frmnotification" enctype="multipart/form-data" id="frmnotification" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=m-notification_a" method="post">
        <?php if (count($_smarty_tpl->getVariable('db_member_notification')->value)>0){?>
        <input type="hidden" name="action" id="action" value="Update">
        <?php }else{ ?>
        <input type="hidden" name="action" id="action" value="Add">
        <?php }?>
          <h2><?php echo @LBL_EMAIL_NOTIFICATION;?>
 <span>/ <?php echo @LBL_EMAIL_NOTI_SENT;?>
 <strong><?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vEmail'];?>
</strong> </span></h2>
          <ul>
            <h2><?php echo @LBL_RECEIVE_EMAIL_NOTI;?>
 :</h2>
            <li>
              <input name="eSuccessPublish" id="eSuccessPublish" type="checkbox" value="Yes" class="radio-input" <?php if ($_smarty_tpl->getVariable('db_member_notification')->value[0]['eSuccessPublish']=='Yes'){?>checked <?php }?>/>
              <?php echo @LBL_EMAIL_NOTI_NOTE1;?>
 </li>
            <li>
              <input name="eSuccessUpdate" id="eSuccessUpdate" type="checkbox" value="Yes" class="radio-input" <?php if ($_smarty_tpl->getVariable('db_member_notification')->value[0]['eSuccessUpdate']=='Yes'){?>checked <?php }?>/>
              <?php echo @LBL_EMAIL_NOTI_NOTE2;?>
 </li>
            <li>
              <input name="ePrivateMessage" id="ePrivateMessage" type="checkbox" value="Yes" class="radio-input" <?php if ($_smarty_tpl->getVariable('db_member_notification')->value[0]['ePrivateMessage']=='Yes'){?>checked <?php }?>/>
              <?php echo @LBL_EMAIL_NOTI_NOTE3;?>
 </li>
            <li>
              <input name="eRatePassenger" id="eRatePassenger" type="checkbox" value="Yes" class="radio-input" <?php if ($_smarty_tpl->getVariable('db_member_notification')->value[0]['eRatePassenger']=='Yes'){?>checked <?php }?>/>
              <?php echo @LBL_EMAIL_NOTI_NOTE4;?>
 </li>
            <li>
              <input name="eNewRating" id="eNewRating" type="checkbox" value="Yes" class="radio-input" <?php if ($_smarty_tpl->getVariable('db_member_notification')->value[0]['eNewRating']=='Yes'){?>checked <?php }?>/>
              <?php echo @LBL_EMAIL_NOTI_NOTE5;?>
 </li>
          </ul>
          <ul>
            <h2><?php echo @LBL_RECEIVE_EMAIL;?>
 :</h2>
            <li>
              <input name="eOtherInformation" id="eOtherInformation" type="checkbox" value="Yes" class="radio-input" <?php if ($_smarty_tpl->getVariable('db_member_notification')->value[0]['eOtherInformation']=='Yes'){?>checked <?php }?>/>
              <?php echo @LBL_RECEIVE_EMAIL_NOTE;?>
 </li>
          </ul>
          <ul>
            <h2><?php echo @LBL_MY_ALERTS;?>
</h2>
            <li class="my-alerts"><?php echo @LBL_MY_ALERTS_NOTE;?>
 <br />
              <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
Email-Alerts"><?php echo @LBL_SEE_RIDE_ALERT;?>
</a></li>
          </ul>
          <div class="notification-save"><a href="javascript:void(0);" onclick="javascript:checknotification(); return false;"><?php echo @LBL_SAVE;?>
</a></div>
          </form>
        </div>
      </div>
    </div>
    <!-------------------------inner-page end-----------------> 
  </div>
  <div style="clear:both;"></div>
</div>
 
<script>
function checknotification(){
    //alert('hello..');
    jQuery("#frmnotification").validationEngine('init',{scroll: false});
	  jQuery("#frmnotification").validationEngine('attach',{scroll: false});
		resp = jQuery("#frmnotification").validationEngine('validate');
	//	alert(resp); return false;
		if(resp == true){
			document.frmnotification.submit();
		}else{
			return false;
		}		
}
</script> 
