<?php /* Smarty version Smarty-3.0.7, created on 2015-07-21 21:40:53
         compiled from "/home/www/xfetch/templates/members/view-mybooking.tpl" */ ?>
<?php /*%%SmartyHeaderCode:84824602355ae6f0d570dc4-46946734%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c259c7ad898bde23d8a3b848ec172216ec49e8aa' => 
    array (
      0 => '/home/www/xfetch/templates/members/view-mybooking.tpl',
      1 => 1437466090,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '84824602355ae6f0d570dc4-46946734',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>

<style>
.tab-pane ul {width:80%;}
</style>

<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_stylesheets'];?>
front/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_stylesheets'];?>
front/jquery.fancybox.css?v=2.1.5" media="screen" />
<div class="body-inner-part">
  <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_MY_BOOKINGS;?>
</span></div>
  <!-------------------------inner-page----------------->
  <div class="main-inner-page">
    <h2><?php echo @LBL_MY_BOOKINGS;?>
</h2>
    <div class="dashbord">
    <form name="frmbooking" id="frmbooking" method="post" action="">
    <input type="hidden" id="Member" name="Member" value="<?php echo $_smarty_tpl->getVariable('Member')->value;?>
">
    <input type="hidden" id="BookDate" name="BookDate" value="<?php echo $_smarty_tpl->getVariable('BookDate')->value;?>
">
      <?php $_template = new Smarty_Internal_Template("member_top.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
      <div class="rides-offered">
        <div class="tab-pane">
        <ul>                         
        <li><a href="javascript:void(0);" onclick="getDetails(1);" <?php if ($_smarty_tpl->getVariable('Member')->value=='Driver'){?>class="active"<?php }?>><?php echo @LBL_AS_DRIVER;?>
</a></li> 
        <li><a href="javascript:void(0);" onclick="getDetails(2);" <?php if ($_smarty_tpl->getVariable('Member')->value=='Booker'){?>class="last active"<?php }else{ ?>class="last"<?php }?>><?php echo @LBL_AS_BOOKER;?>
</a></li>
        </ul>
        <span style="float:right;">
          <select id="bookDate" name="bookDate" onchange="getDateWise(this.value);" style="background: none repeat scroll 0 0 #FFFFFF;border: 1px solid #CCCCCC;margin: 0;margin-right:4px; padding: 7px;width: 145px;">
           <option value="">-- <?php echo @LBL_SELECT;?>
 --</option>
           <option value="1" <?php if ($_smarty_tpl->getVariable('BookDate')->value=='Past'){?>selected <?php }?>><?php echo @LBL_PAST_BOOKING;?>
</option>
           <option value="2" <?php if ($_smarty_tpl->getVariable('BookDate')->value=='Today'){?>selected <?php }?>><?php echo @LBL_TODAY_BOOKING;?>
</option>
           <option value="3" <?php if ($_smarty_tpl->getVariable('BookDate')->value=='Futur'){?>selected <?php }?>><?php echo @LBL_FUTURE_BOOKING;?>
</option>
           <option value="5" <?php if ($_smarty_tpl->getVariable('BookDate')->value=='Canc'){?>selected <?php }?>><?php echo @LBL_CANCEL_BOOKING;?>
</option>
           <option value="4" <?php if ($_smarty_tpl->getVariable('BookDate')->value=='All'){?>selected <?php }?>><?php echo @LBL_SHOW_ALL;?>
</option>
          </select>
        </span>
        </div>
      </div>
      </form>
      <?php if (count($_smarty_tpl->getVariable('db_ride_list')->value)>0){?>
        <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_ride_list')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?> 		
          <div class="main-block-new">
          <div class="main-block-1 main-block-2">
            <h2><?php echo @LBL_BOOKING_NO;?>
 #<?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBookingNo'];?>

            <p>
            <img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
member-right-i.png" alt="" /> <?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTimeFormat($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dBookingDate']);?>
 @ <?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dBookingTime'],18);?>
</p>
            
            <span><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
input-iocn.png" alt="" /><?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vFromPlace'];?>
 &rarr; <b><?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vToPlace'];?>
</b><a href="javascript:void(0);" onclick="validateride('<?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iBookingId'];?>
');" class="give-rate"><?php echo @LBL_VALIDATE_RIDE;?>
</a></span>
            </h2>
            <div class="leave-rating-list">
              <table width="100%">
                <tr>
                  <td width="30%" valign="top" style="border-right:1px solid #CCCCCC;">
                  <?php if ($_smarty_tpl->getVariable('Member')->value=='Driver'){?>
                  <strong><?php echo @LBL_BOOKER_DETAIL;?>
</strong><br>
                  <?php echo @LBL_NAME;?>
 : <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-user_profile&iMemberId=<?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iBookerId'];?>
"><?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBookerFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBookerLastName'];?>
</a><br>
                  <?php echo @LBL_PHONE;?>
 : <?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBookerPhone'];?>
<br>
                  <?php echo @LBL_EMAIL;?>
 : <?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBookerEmail'];?>
<br>
                  <?php }else{ ?>
                  <strong><?php echo @LBL_DRIVER_DETAILS;?>
</strong><br>
                  <?php echo @LBL_NAME;?>
 : <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-user_profile&iMemberId=<?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iDriverId'];?>
"><?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vDriverFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vDriverLastName'];?>
</a><br>
                  <?php echo @LBL_PHONE;?>
 : <?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vDriverPhone'];?>
<br>
                  <?php echo @LBL_EMAIL;?>
 : <?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vDriverEmail'];?>
<br>
                  <?php }?>
                  </td>
                  <td width="30%" valign="top" style="border-right:1px solid #CCCCCC;padding-left:5px;">
                  <strong><?php echo @LBL_PAYMENT_DETAILS;?>
</strong><br>
				  <?php if ($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fDocumentPrice']!=0){?>
				   <?php echo @LBL_DOCUMENT;?>
: <?php echo $_smarty_tpl->getVariable('generalobj')->value->booking_currency($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fDocumentPrice'],$_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBookerCurrencyCode']);?>
<br/>
				  <?php }?>
				  <?php if ($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fBoxPrice']!=0){?>
				   <?php echo @LBL_BOX;?>
: <?php echo $_smarty_tpl->getVariable('generalobj')->value->booking_currency($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fBoxPrice'],$_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBookerCurrencyCode']);?>
<br/>
				  <?php }?>
				  <?php if ($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fLuggagePrice']!=0){?>
				   <?php echo @LBL_LUGGAGE;?>
: <?php echo $_smarty_tpl->getVariable('generalobj')->value->booking_currency($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fLuggagePrice'],$_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBookerCurrencyCode']);?>
<br/>
				  <?php }?>
				  <br/>
                  </td>
                  <?php if (@PAYMENT_OPTION=='PayPal'){?>
                  <td width="25%" valign="top" <?php if ($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eStatus']!='Cencelled'){?>style="border-right:1px solid #CCCCCC;padding-left:5px;"<?php }else{ ?>style="padding-left:5px;"<?php }?>>
                  <strong><?php echo @LBL_PAYMENT_BOOKING_STATUS;?>
</strong><br>
                  <?php echo @LBL_PAYMENT;?>
 : <?php if ($_smarty_tpl->getVariable('Member')->value=='Driver'){?><?php if ($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eDriverPaymentPaid']=='Yes'){?>Paid<?php }else{ ?>Unpaid<?php }?><?php }else{ ?><?php if ($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eBookerPaymentPaid']=='Yes'){?>Paid<?php }elseif($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eBookerPaymentPaid']=='No'){?>Unpaid <?php }else{ ?>Refund <?php }?><?php }?> <br>
                  <?php echo @LBL_BOOKING;?>
 :  <?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eStatus'];?>
<br>
                  <?php echo @LBL_PRICE;?>
 : <?php echo $_smarty_tpl->getVariable('generalobj')->value->booking_currency($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fAmount'],$_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBookerCurrencyCode']);?>

                  </td>
                  <?php }else{ ?>
                  <td width="25%" valign="top" <?php if ($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eStatus']!='Cencelled'){?>style="border-right:1px solid #CCCCCC;padding-left:5px;"<?php }else{ ?>style="padding-left:5px;"<?php }?>>
                  <strong>Booking Status</strong><br>                        
                  <?php echo @LBL_BOOKING;?>
 :  <?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eStatus'];?>
<br>
                  <?php echo @LBL_PRICE;?>
 : <?php echo $_smarty_tpl->getVariable('generalobj')->value->booking_currency($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fAmount'],$_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBookerCurrencyCode']);?>

                  </td>
                  <?php }?>
                  <?php if ($_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eStatus']!='Cencelled'&&$_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['bookingtype']=='Avail'){?>
                   <td width="15%" valign="bottom" style="padding-left:5px;">
					 <?php if ($_smarty_tpl->getVariable('Member')->value=='Booker'&&$_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vSmsCode']!=''){?>
						<?php echo @LBL_SECURE_CODE;?>
: <?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vSmsCode'];?>

					 <?php }?>
                     <!--<a href="javascript:void(0);" onclick="cancelbooking('<?php echo $_smarty_tpl->getVariable('Member')->value;?>
','<?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iBookingId'];?>
');" class="canbooking"><?php echo @LBL_CANCEL_BOOKING;?>
</a>-->
                     <a href="javascript:void(0);" onClick="cancel_booking_conf(<?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iBookingId'];?>
);" class="canbooking" style="fount-size:12px;margin-top:2px;float: right;"><?php echo @LBL_CANCEL_BOOKING;?>
</a>
                   </td>
                  <?php }else{ ?>
                    <?php if ($_smarty_tpl->getVariable('Member')->value=='Driver'&&$_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eBookerConfirmation']=='No'&&$_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eStatus']!='Cencelled'){?>
                                   
                    <?php }else{ ?>
                    <td><?php if ($_smarty_tpl->getVariable('Member')->value=='Booker'&&$_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vSmsCode']!=''){?>
						<?php echo @LBL_SECURE_CODE;?>
: <?php echo $_smarty_tpl->getVariable('db_ride_list')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vSmsCode'];?>

					 <?php }?></td>
                    <?php }?>
                  <?php }?>
                </tr>
              </table>
            </div>
          </div>
          </div>
        <?php endfor; endif; ?>
        <?php }else{ ?>
          <div class="main-block">
            <?php if ($_smarty_tpl->getVariable('Member')->value=='Driver'){?>
               <?php echo @LBL_NO_BOOKING_AS_DRIVER;?>

            <?php }else{ ?>
               <?php echo @LBL_NO_BOOKING_AS_BOOKER;?>

            <?php }?>
          </div>
      <?php }?>
        <?php if (count($_smarty_tpl->getVariable('db_ride_list')->value)>0){?>
            <div class="paging"><?php echo $_smarty_tpl->getVariable('page_link')->value;?>
</div>
        <?php }?>
      </div>
   
      <div style="clear:both;"></div>
    </div>
    <div style="clear:both;"></div>

  <!-------------------------inner-page end----------------->
  <div style="clear:both;"></div>
</div>
<form method="post" id="validbookerride" name="validbookerride" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
my-bookings">
   <input type="hidden" value="" id="bookingid" name="bookingid">  
   <input type="hidden" value="" id="vSmsCode" name="vSmsCode">  
   <input type="hidden" value="validbookerride" id="action" name="action">
</form>                        
<div style="display:none">
  <div id="bookingcancel" class="form-login">
    <h4><?php echo @LBL_BOOK_CANCEL;?>
</h4>
    <form method="post" id="book_cancel" name="book_cancel" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
my-bookings">
    <input type="hidden" value="" id="eCancelBy" name="eCancelBy">
    <input type="hidden" value="" id="ibookid" name="ibookid">  
    <input type="hidden" value="cancelbooking" id="action" name="action">
    <div class="inner-form1"> 
      <span><strong><?php echo @LBL_REASON;?>
 :</strong>     
        <textarea value="" id="tCancelreason" name="tCancelreason" class="validate[required] login-ageinput-po"></textarea>
      </span>
      <div class="singlerow-login-log"><a href="javascript:void(0);" onclick="check_cancel();"><?php echo @LBL_CONFIRM;?>
</a></div>
      <div style="clear:both;"></div>
    </div>
    </form>           
  </div>
</div>

<script>
  function getDetails(val){
    if(val==2){
       document.frmbooking.Member.value="Booker";
       document.frmbooking.submit();
    }else{
       document.frmbooking.Member.value="Driver";
       document.frmbooking.submit();
    }
  }
  function getDateWise(val){
    if(val==1){
      document.frmbooking.BookDate.value="Past";
      document.frmbooking.submit();
    }else if(val==2){
      document.frmbooking.BookDate.value="Today";
      document.frmbooking.submit();
    }else if(val==3){
      document.frmbooking.BookDate.value="Futur";
      document.frmbooking.submit();
    }else if(val==4){
      document.frmbooking.BookDate.value="All";
      document.frmbooking.submit();
    }else if(val==5){
      document.frmbooking.BookDate.value="Canc";
      document.frmbooking.submit();
    }
  }
  
  function cancelbooking(membertype,bookingid){
    //ans = confirm("Confirm to cancel selected booking?");
    ans = confirm("<?php echo @LBL_CONFIRM_CANCEL;?>
");
  	if(ans == true){
  	  //alert(bookingid);return false;
  	  $("#eCancelBy").val(membertype);
  	  $("#ibookid").val(bookingid);
  	  $.fancybox("#bookingcancel");return false;
    }else{
      return false;
    } 
  }
  
  function validateride(bookingid){
    //alert(bookingid);return false;
	ans = prompt("<?php echo @LBL_RIDEVALIDE_MESSAGE;?>
", "");
    if (ans != null && ans != '') {
  	  $("#bookingid").val(bookingid);
  	  $("#vSmsCode").val(ans);
  	  document.validbookerride.submit();
    }else{
	 alert('Please Enter Security Code');
      return false;
    } 
  }       
  
  function check_cancel(){
    jQuery("#book_cancel").validationEngine('init',{scroll: false});
	  jQuery("#book_cancel").validationEngine('attach',{scroll: false});
	  resp = jQuery("#book_cancel").validationEngine('validate');
    
    if(resp == true){
      $.fancybox.close();
      document.book_cancel.submit();
    }else{
      return false;
    }
  } 
  
  function cancel_booking_conf(id){
    var r = confirm("Are you sure want to cancel this booking?");
    if (r == true) {
       window.location = "<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=m-booking_cancelation_form&id="+id; 
    } else {
        return false;
    }
  }                        
</script>


<?php if ($_GET['var_msg']!=''){?>          
<?php if ($_GET['msg_code']=='1'){?>
  
    <script>  
      $( document ).ready(function(){
       showNotification({type : 'success', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});
      });      
    </script>
  
<?php }?>
<?php }?> 

<?php if ($_GET['var_msg']!=''){?>          
<?php if ($_GET['msg_code']=='0'){?>
  
    <script>
      $( document ).ready(function(){
       showNotification({type : 'error', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});
      });
    </script>
  
<?php }?>
<?php }?> 
