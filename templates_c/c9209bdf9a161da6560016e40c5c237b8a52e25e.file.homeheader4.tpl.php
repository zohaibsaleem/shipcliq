<?php /* Smarty version Smarty-3.0.7, created on 2016-01-20 13:49:36
         compiled from "/home/www/cargosharing1/templates/homeheader4.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2091543914569f907004e2a8-94407017%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c9209bdf9a161da6560016e40c5c237b8a52e25e' => 
    array (
      0 => '/home/www/cargosharing1/templates/homeheader4.tpl',
      1 => 1453297771,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2091543914569f907004e2a8-94407017',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div id="top-part" class="<?php if ($_smarty_tpl->getVariable('sess_iMemberId')->value!=''){?>login-part<?php }?>">
	<div class="top-part-inner">
		<div class="top-select-part">
			<div class="top-select-part-inner">
				<div class="login-b-login">
					<div class="top-div-iocn"> <?php if ($_smarty_tpl->getVariable('sess_iMemberId')->value!=''){?>
						<div class="top-icons">
							<ul>
								<?php if ($_SESSION['tot_unread']>=1){?>
								<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
received-messages"><strong><?php echo $_SESSION['tot_unread'];?>
</strong><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/message_notification_new.png"></a></li>
								<?php }else{ ?>
								<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
received-messages"><strong>&nbsp;</strong><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/message_notification.png"></a></li>
								<?php }?>        
								<?php if ($_SESSION['tot_feat_booking']>=1){?>
								<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
my-bookings"><strong><?php echo $_SESSION['tot_feat_booking'];?>
</strong><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/booking_notification_new.png"></a></li>
								<?php }else{ ?>
								<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
my-bookings"><strong>&nbsp;</strong><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/booking_notification.png"></a></li>
								<?php }?>
							</ul>
						</div>
						<div class="user-part-main">
							<div class="user-part"> <img src="<?php echo $_smarty_tpl->getVariable('member_image')->value;?>
" alt="" title="logo"/>
								<h2><?php echo $_smarty_tpl->getVariable('sess_vFirstName')->value;?>
</h2>
								<div class="drop-menu">
									<p><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
my-account"><?php echo @LBL_MY_ACCOUNT;?>
 <img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/white-point.png" alt="" /></a></p>
									<ul class="sub-menu">
										<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
my-account"><?php echo @LBL_DASHBOARD;?>
</a></li>
										<?php if (@PAYMENT_OPTION!='Contact'){?>
										<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
my-bookings"><?php echo @LBL_MY_BOOKINGS;?>
</a></li>
										<?php }?>
										<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
List-Rides-Offer"><?php echo @LBL_RIDES_OFFERED1;?>
</a></li>
										<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
received-messages"><?php echo @LBL_MESSAGES;?>
</a></li>
										<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
Email-Alerts"><?php echo @LBL_EMAIL_ALERTS;?>
</a></li>
										<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
leave-Rating"><?php echo @LBL_RATINGS;?>
</a></li>
										<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
personal-information"><?php echo @LBL_PROFILE;?>
</a></li>
										<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=m-money_detail&pagetype=money&type=available"><?php echo @LBL_PAYMENT;?>
</a></li>
										<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
logout"><?php echo @LBL_LOGOUT;?>
</a></li>
									</ul>
								</div>
								<!-- <p><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
logout"><?php echo @LBL_LOGOUT;?>
</a></p> -->
								<div style="clear:both;"></div>
							</div>
						</div>
					<?php }?> </div>
				</div>
				<div class="select-box">
					<select name="" class="custom-select custom-select01" onchange="chnage_ride_types(this.value);">
						<option value="international" <?php if ($_COOKIE['ride_locations_type']=='international'){?>selected<?php }?>><?php echo @LBL_VIEW_INTERNATIONAL_RIDES;?>
</option>
						<option value="local" <?php if ($_COOKIE['ride_locations_type']=='local'){?>selected<?php }?> ><?php echo @LBL_VIEW_LOCAL_RIDES;?>
</option>
					</select>
					<?php if (count($_smarty_tpl->getVariable('db_lng_mst')->value)>1){?>
					<select name="sess_language" id="sess_language" onChange="change_lang(this.value);" class="custom-select se-in">
						
						<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['name'] = "lngrb";
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_lng_mst')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['total']);
?>
						
						<option value="<?php echo $_smarty_tpl->getVariable('db_lng_mst')->value[$_smarty_tpl->getVariable('smarty')->value['section']['lngrb']['index']]['vCode'];?>
" <?php if ($_smarty_tpl->getVariable('sess_lang')->value==$_smarty_tpl->getVariable('db_lng_mst')->value[$_smarty_tpl->getVariable('smarty')->value['section']['lngrb']['index']]['vCode']){?> selected <?php }?>><?php echo $_smarty_tpl->getVariable('db_lng_mst')->value[$_smarty_tpl->getVariable('smarty')->value['section']['lngrb']['index']]['vTitle'];?>
</option>
						
						<?php endfor; endif; ?>
						
					</select>
					<?php }else{ ?>
					<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['name'] = "lngrb";
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_lng_mst')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['total']);
?>
						<input type="hidden" name="sess_language" id="sess_language" value="<?php echo $_smarty_tpl->getVariable('db_lng_mst')->value[$_smarty_tpl->getVariable('smarty')->value['section']['lngrb']['index']]['vCode'];?>
">
					<?php endfor; endif; ?>
					
					<?php }?>
					<?php if (count($_smarty_tpl->getVariable('db_curr_mst')->value)>1){?>
					<select name="sess_price_ratio" id="sess_price_ratio" onChange="change_currency(this.value);" class="custom-select se-in">
						
						<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['name'] = "currrb";
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_curr_mst')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["currrb"]['total']);
?>
						
						<option value="<?php echo $_smarty_tpl->getVariable('db_curr_mst')->value[$_smarty_tpl->getVariable('smarty')->value['section']['currrb']['index']]['vName'];?>
" <?php if ($_smarty_tpl->getVariable('sess_price_ratio')->value==$_smarty_tpl->getVariable('db_curr_mst')->value[$_smarty_tpl->getVariable('smarty')->value['section']['currrb']['index']]['vName']){?> selected <?php }?>><?php echo $_smarty_tpl->getVariable('db_curr_mst')->value[$_smarty_tpl->getVariable('smarty')->value['section']['currrb']['index']]['vName'];?>
</option>
						
						<?php endfor; endif; ?>              
					</select>
					<?php }else{ ?>
					<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['name'] = "lngrb";
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_curr_mst')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["lngrb"]['total']);
?>
						<input type="hidden" name="sess_price_ratio" id="sess_price_ratio" value="<?php echo $_smarty_tpl->getVariable('db_curr_mst')->value[$_smarty_tpl->getVariable('smarty')->value['section']['lngrb']['index']]['vName'];?>
">
					<?php endfor; endif; ?>
					<?php }?>
				<?php if ($_smarty_tpl->getVariable('sess_iMemberId')->value==''){?> <span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
sign-in"><?php echo @LBL_LOG_IN;?>
</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
sign-up"><?php echo @LBL_SIGN_UP;?>
</a></span> <?php }else{ ?> </div>
			<?php }?> </div>
		</div>
		<div class="top-logo-menu-part">
			<div class="logo"> <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"> <img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_upload_site_logo'];?>
1_<?php echo $_smarty_tpl->getVariable('SITE_LOGO')->value;?>
" alt="" title="logo" />
				<!--<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/logo.png" alt="" />-->
			</a> </div>
			<div class="menu-part">
				<!--navigation-res-->
				<div class="navigation"><span><a class="toggleMenu" href="#"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/menu-bg1.png" alt=""></a></span>
					<ul class="nav">
						<li class="test"><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
" <?php if ($_smarty_tpl->getVariable('script')->value=='home4'){?>class="active"<?php }?>><?php echo @LBL_HOME;?>
</a></li>
						<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_list&search=simple" <?php if ($_smarty_tpl->getVariable('script')->value=='ride_list'){?>class="active"<?php }?>><?php echo @LBL_FIND_RIDE;?>
</a></li>
						<!--<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
find-ride" <?php if ($_smarty_tpl->getVariable('script')->value=='find_ride'){?>class="active"<?php }?>><?php echo @LBL_FIND_RIDE;?>
</a></li>    -->
						<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
blog"><?php echo @LBL_BLOGS;?>
</a></li>
						<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_pages_1')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						<?php if ($_smarty_tpl->getVariable('db_pages_1')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPageId']=='1'){?>
						<li> <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
page/<?php echo $_smarty_tpl->getVariable('db_pages_1')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPageId'];?>
/<?php ob_start();?><?php echo $_smarty_tpl->getVariable('db_pages_1')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPageId'];?>
<?php $_tmp1=ob_get_clean();?><?php echo $_smarty_tpl->getVariable('generalobj')->value->getPageUrlName($_tmp1);?>
" <?php if ($_smarty_tpl->getVariable('script')->value=='page'&&$_smarty_tpl->getVariable('iPageId')->value=='1'){?> class="active"<?php }?>><?php ob_start();?><?php echo $_smarty_tpl->getVariable('db_pages_1')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPageId'];?>
<?php $_tmp2=ob_get_clean();?><?php echo $_smarty_tpl->getVariable('generalobj')->value->getPageTitle($_tmp2);?>
 </a> </li>
						<?php }?>
						<?php endfor; endif; ?>
						<!-- <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
aboutus" <?php if ($_smarty_tpl->getVariable('script')->value=='page'&&$_smarty_tpl->getVariable('iPageId')->value=='1'){?>class="active"<?php }?> ><?php echo @LBL_ABOUT_US;?>
 </a> </li> -->
						<li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
contactus" <?php if ($_smarty_tpl->getVariable('script')->value=='contactus'){?>class="active"<?php }?>><?php echo @LBL_CONTACT_US;?>
</a></li>
						<li class="last"><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
offer-ride" <?php if ($_smarty_tpl->getVariable('script')->value=='offer_ride'){?>class="active"<?php }?>><?php echo @LBL_OFFER_RIDE;?>
</a></li>
					</ul>
				</div>
				<!--<script type="text/javascript" src="js/script.js"></script>-->
				<!--navigation-res end-->
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
	<div style="clear:both;"></div>
</div>
</div>

<script>
	$(document).ready(function(){ 
		$('#searchdate').Zebra_DatePicker({ 
			direction: 1 
		});
	});   
    
    function change_lang(lang){
		window.location = '<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?lang='+lang; 
	}
    
    function change_currency(currency){
		window.location = '<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?currency='+currency; 
	}
    
    function setCookie(cname,cvalue,exdays)
    {
		var d = new Date();
		d.setTime(d.getTime()+(exdays*24*60*60*1000));
		var expires = "expires="+d.toGMTString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	} 
    
    function getCookie(cname)
    {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) 
        {
			var c = ca[i].trim();
			if (c.indexOf(name)==0) return c.substring(name.length,c.length);
		}
		return "";
	}
    
    function showPosition(position)
    { 
		//setCookie('ride_locations_type', 'local', 1);
		setCookie('userlatitude', position.coords.latitude, 1);
		setCookie('userlongitude', position.coords.longitude, 1); 
		if(getCookie('country_code') == ''){
			var request = $.ajax({  
				type: "POST",
				url: site_url+"createcookie.php",  
				data: "", 	  
				
				success: function(data) {    		  
					document.location.reload(true);      			
				}
			});
		} 
	}
    
    function getLocation()
    {        
		if (navigator.geolocation)
		{
			navigator.geolocation.getCurrentPosition(showPosition);
			//window.location = site_url;
		}
		else{x.innerHTML="Geolocation is not supported by this browser.";}
	}
    
    function checkCookie()
    {
		var userlatitude = getCookie("userlatitude");
		var userlongitude = getCookie("userlongitude");
		if (userlatitude != "" && userlongitude != '')
		{
			}else{  
			getLocation();
		}
	} 
    
    function chnage_ride_types(type){
		if(type == 'international'){
			document.cookie = "country_code=; expires=Thu, 01 Jan 1970 00:00:00 GMT";
			setCookie('ride_locations_type', 'international', 1);
			window.location = site_url;
			}else{
			setCookie('ride_locations_type', 'local', 1);
			getLocation();
		}
	}      
</script>

<?php if ($_COOKIE['ride_locations_type']==''){?>

<script>      
	/*var ride_locations_type = getCookie("ride_locations_type");
		if(ride_locations_type == ''){ 
        chnage_ride_types('international');
	} */
</script>

<?php }?> 