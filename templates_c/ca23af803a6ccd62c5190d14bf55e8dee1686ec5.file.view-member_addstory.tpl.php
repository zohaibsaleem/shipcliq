<?php /* Smarty version Smarty-3.0.7, created on 2015-02-12 11:43:04
         compiled from "/home/www/blablaclone/templates/members/view-member_addstory.tpl" */ ?>
<?php /*%%SmartyHeaderCode:44860443254dc4470a35125-66999406%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ca23af803a6ccd62c5190d14bf55e8dee1686ec5' => 
    array (
      0 => '/home/www/blablaclone/templates/members/view-member_addstory.tpl',
      1 => 1414417603,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '44860443254dc4470a35125-66999406',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_GET['var_msg']!=''){?>          
<?php if ($_GET['msg_code']=='1'){?>
  
    <script>        
      showNotification({type : 'success', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});  
    </script>
  
<?php }?>
<?php }?> 

<?php if ($_GET['var_msg']!=''){?>          
<?php if ($_GET['msg_code']=='0'){?>
  
    <script>
      showNotification({type : 'error', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});
    </script>
  
<?php }?>
<?php }?>
<div class="body-inner-part">
  <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
Member-Stories"><?php echo @LBL_MEMBER_STORY;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_MEMBER_STORY;?>
 <?php echo @LBL_FORM;?>
</span></div>
  <!-------------------------inner-page----------------->
  <div class="main-inner-page">
    <h2><?php echo @LBL_MEMBER_STORY;?>
<span><?php echo @LBL_FORM;?>
</span></h2>
    <?php $_template = new Smarty_Internal_Template("left.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
    <div class="right-inner-part">
      <h2 class="store-h">
        <em><?php echo @LBL_MEMBER_STORY;?>
 <?php echo @LBL_FORM;?>
</em>
        <!--<a href="#" class="tell-u">Tell us your stories</a>-->
      </h2>
      <div class="profile">
        <h2><?php echo @LBL_MEMBER_STORY;?>
 <?php echo @LBL_FORM;?>
</h2>
        <form name="frmaddstory" id="frmaddstory" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=m-member_addstory_a" method="post" enctype="multipart/form-data">
        <input type="hidden" name="action" id="action" value="Add">
        <span><em><?php echo @LBL_MEMBER_NAME;?>
</em>
        <?php echo $_SESSION['sess_vFirstName'];?>
 <?php echo $_SESSION['sess_vLastName'];?>

        </span>
        <span><em><?php echo @LBL_I_AM;?>
</em>
          <select name="Data[eType]" id="eType" class="validate[required] profile-select">
              <option value="">-----<?php echo @LBL_SELECT_TYPE;?>
-----</option>
                <option value="Passengers"><?php echo @LBL_PASSENGER;?>
</option>
                <option value="Drivers"><?php echo @LBL_DRIVER1;?>
</option>
          </select>   
        </span> 
        <span><em><?php echo @LBL_SUBJECT;?>
</em>
        <input name="Data[vTitle]" id="vTitle" type="text" class="validate[required] profile-input" value="" />
        </span> 
        <span><em><?php echo @LBL_MESSAGE;?>
</em>
        <textarea name="Data[tDescription]" id="tDescription" cols="" rows="" class="validate[required] profile-textarea"></textarea>
        </span>
        <!-- <span><em>Email</em>
        <input name="" type="text" class="profile-input" value=""/>
        </span> <span><em>Phone</em>
        <input name="" type="text" class="profile-input" value=""/>
        </span>--> 
        <span class="sav-but-pro">
          <a href="javascript:void(0);" onclick="checkstory();"><?php echo @LBL_SEND;?>
</a>
        </span> 
        </form>
      </div>
    </div>
  </div>
  <!-------------------------inner-page end----------------->
  <div style="clear:both;"></div>
</div>

<script>
function checkstory(){
    jQuery("#frmaddstory").validationEngine('init',{scroll: false});
	  jQuery("#frmaddstory").validationEngine('attach',{scroll: false});
	  resp = jQuery("#frmaddstory").validationEngine('validate');
    //alert(resp);return false;
		if(resp == true){
			document.frmaddstory.submit();
		}else{
			return false;
		}	
}
</script>
    