<?php /* Smarty version Smarty-3.0.7, created on 2015-01-13 14:15:39
         compiled from "/home/www/blablaclone/templates/content/view-registration.tpl" */ ?>
<?php /*%%SmartyHeaderCode:209845248054b4db33b69ec4-72337060%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dbcc78441e3f58b605c4e76872721d3f348ab7a3' => 
    array (
      0 => '/home/www/blablaclone/templates/content/view-registration.tpl',
      1 => 1415202003,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '209845248054b4db33b69ec4-72337060',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
 <?php if ($_smarty_tpl->getVariable('var_msg')->value!=''){?>
 
 <script>
  showNotification({type : 'error', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});
 </script> 
 
<?php }?>   
<div class="body-inner-part">
  <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_REGISTRATION;?>
</span></div>
  <div class="main-inner-page">
    <h2><?php echo @LBL_REGISTRATION;?>
</h2>
    <?php $_template = new Smarty_Internal_Template("left.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>      
    <form class="drop form_new" name="frmregister" id="frmregister" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-registration_a" method="post">
    <input type="Hidden" name="mode" value="register">
    <div class="right-inner-part">
      <div class="registration">
        <table width="100%" cellspacing="0" cellpadding="7" border="0">
          <tr>
            <td align="center"><button class="btn btn-primary" onclick="fbconnect();return false;">
              <a style="color:#fff;" href="javascript:void(0);" onclick="fbconnect();"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
fb-but-icon.png" alt="" /><?php echo @LBL_CONNECT_FACEBOOK;?>
</a>
              </button></td>
          </tr>
          <tr>
            <td class="registration-or" align="center"><?php echo @LBL_OR;?>
</td>
          </tr>
          <tr>
            <td style="padding:0px;">&nbsp;</td>
          </tr>
        </table>
        <div class="reg-input1">
          <label><em><?php echo @LBL_NICK_NAME;?>
 : <strong>*</strong></em>
            <input type="text" id="vNickName" name="vNickName" class="validate[required] form-input-re" value="<?php echo $_smarty_tpl->getVariable('db_customer')->value[0]['vNickName'];?>
">
          </label>
          <label><em><?php echo @LBL_FIRST_NAME;?>
 : <strong>*</strong></em>
            <input type="text" id="vFirstName" name="vFirstName" class="validate[required] form-input-re" value="<?php echo $_smarty_tpl->getVariable('db_customer')->value[0]['vFirstName'];?>
">
          </label>
          <label><em><?php echo @LBL_LAST_NAME;?>
 : <strong>*</strong></em>
            <input type="text" id="vLastName" name="vLastName" class="validate[required] form-input-re" value="<?php echo $_smarty_tpl->getVariable('db_customer')->value[0]['vLastName'];?>
">
          </label>
          <div style="clear:both;"></div>
        </div>
        <div class="reg-input1">
          <label><em><?php echo @LBL_EMAIL;?>
 : <strong>*</strong></em>
            <input type="text" name="vEmail" id="vEmail" class="validate[required,custom[email]] form-input-re" value="<?php echo $_smarty_tpl->getVariable('db_customer')->value[0]['vEmail'];?>
"></label>
          <label><em><?php echo @LBL_PASSWORD;?>
 : <strong>*</strong></em>
            <input type="password" title="Password" name="vPassword" id="vPassword" class="validate[required,minSize[6]] form-input-re"></label>
          <label><em><?php echo @LBL_RE_ENTER;?>
 <?php echo @LBL_PASSWORD;?>
 : <strong>*</strong></em>
            <input type="password" title="Re-Enter Password" name="vRePassword" id="vRePassword" class="validate[required,equals[vPassword]] form-input-re">
          </label>    
          <label><em><?php echo @LBL_PREFERRED_LANG;?>
 : <strong></strong></em>
            <select name="vLanguageCode" id="vLanguageCode" class="form-input-select">
              <option value="EN" <?php if ($_smarty_tpl->getVariable('db_customer')->value[0]['vLanguageCode']=='EN'){?> selected <?php }?>><?php echo @LBL_ENGLISH;?>
</option>
              <option value="DN" <?php if ($_smarty_tpl->getVariable('db_customer')->value[0]['vLanguageCode']=='DN'){?> selected <?php }?>><?php echo @LBL_DANISH;?>
</option>
              <option value="FI" <?php if ($_smarty_tpl->getVariable('db_customer')->value[0]['vLanguageCode']=='FI'){?> selected <?php }?>><?php echo @LBL_FINISH;?>
</option>
              <option value="FN" <?php if ($_smarty_tpl->getVariable('db_customer')->value[0]['vLanguageCode']=='FN'){?> selected <?php }?>><?php echo @LBL_FRENCH;?>
</option>
              <option value="LV" <?php if ($_smarty_tpl->getVariable('db_customer')->value[0]['vLanguageCode']=='LV'){?> selected <?php }?>><?php echo @LBL_LATVIN;?>
</option>
              <option value="EE" <?php if ($_smarty_tpl->getVariable('db_customer')->value[0]['vLanguageCode']=='EE'){?> selected <?php }?>><?php echo @LBL_ESTONIAN;?>
</option>
              <option value="LT" <?php if ($_smarty_tpl->getVariable('db_customer')->value[0]['vLanguageCode']=='LT'){?> selected <?php }?>><?php echo @LBL_LITHUNIAN;?>
</option>
              <option value="DE" <?php if ($_smarty_tpl->getVariable('db_customer')->value[0]['vLanguageCode']=='DE'){?> selected <?php }?>><?php echo @LBL_GERMAN;?>
</option>
              <option value="NO" <?php if ($_smarty_tpl->getVariable('db_customer')->value[0]['vLanguageCode']=='NO'){?> selected <?php }?>><?php echo @LBL_NORWAY;?>
</option>
              <option value="PO" <?php if ($_smarty_tpl->getVariable('db_customer')->value[0]['vLanguageCode']=='PO'){?> selected <?php }?>><?php echo @LBL_POLISH;?>
</option>
              <option value="RS" <?php if ($_smarty_tpl->getVariable('db_customer')->value[0]['vLanguageCode']=='RS'){?> selected <?php }?>><?php echo @LBL_RUSSIAN;?>
</option>
              <option value="ES" <?php if ($_smarty_tpl->getVariable('db_customer')->value[0]['vLanguageCode']=='ES'){?> selected <?php }?>><?php echo @LBL_SPANISH;?>
</option>
              <option value="SW" <?php if ($_smarty_tpl->getVariable('db_customer')->value[0]['vLanguageCode']=='SW'){?> selected <?php }?>><?php echo @LBL_SWEDISH;?>
</option>
            </select>
          </label>
          <div style="clear:both;"></div>
        </div>
        <div class="reg-input1" style="border:none;">
          <label><em><?php echo @LBL_ENTER_CODE;?>
 : <strong>*</strong></em>
            <input type="text" title="Captcha Code" name="capthca" id="capthca" class="validate[required] form-input-re1">
            &nbsp;<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
plugins/captcha/CaptchaSecurityImages.php" align="top" height="31"/>
            </label>
        </div>
        <label></label>
        <div class="singlerow-reg"><a onclick="javascript:checkregister();" href="javascript:void(0);"><?php echo @LBL_SUBMIT;?>
</a> <a onclick="document.frmregister.reset();return false;" href="javascript:void(0);"><?php echo @LBL_RESET;?>
</a></div>
        <div style="clear:both;"></div>
      </div>
    </div>
    </form>
  </div><div style="clear:both;"></div>
</div>

<script>
function checkregister(){
  jQuery("#frmregister").validationEngine('init',{scroll: false});
	jQuery("#frmregister").validationEngine('attach',{scroll: false});
	resp = jQuery("#frmregister").validationEngine('validate');
	if(resp == true){
		document.frmregister.submit();
	}else{
		return false;
	}		
}

function fbconnect()
  {
	 javscript:window.location='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
fbconnect.php';
  }
</script>
