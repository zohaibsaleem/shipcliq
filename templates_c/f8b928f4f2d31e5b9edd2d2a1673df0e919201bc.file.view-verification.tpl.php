<?php /* Smarty version Smarty-3.0.7, created on 2016-02-11 05:50:59
         compiled from "/home4/shipcliq/public_html/templates/members/view-verification.tpl" */ ?>
<?php /*%%SmartyHeaderCode:153308148956bc75a31cbb06-76734214%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f8b928f4f2d31e5b9edd2d2a1673df0e919201bc' => 
    array (
      0 => '/home4/shipcliq/public_html/templates/members/view-verification.tpl',
      1 => 1455188181,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '153308148956bc75a31cbb06-76734214',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_smarty_tpl->getVariable('var_msg')->value!=''){?>
<?php if ($_smarty_tpl->getVariable('msg_code')->value=='1'){?>
  
<script>
      showNotification({type : 'success', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});
    </script>

<?php }?>
<?php }?>

<?php if ($_smarty_tpl->getVariable('var_msg')->value!=''){?>
<?php if ($_smarty_tpl->getVariable('msg_code')->value=='0'){?>
  
<script>
  showNotification({type : 'error', message: '<?php echo $_smarty_tpl->getVariable('var_msg')->value;?>
'});
</script>

<?php }?>
<?php }?>
<div class="body-inner-part">
  <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_EMAIL_VARIFICATION;?>
</span></div>
  <div class="main-inner-page">
    <h2><?php echo @LBL_MEMBER1;?>
 <span><?php echo @LBL_VERIFICATION;?>
</span></h2>
    <?php $_template = new Smarty_Internal_Template("member_top.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
    <div class="ratings">
      <div class="email-verification">
        <h3><?php echo @LBL_MY_VERIFICATION;?>
</h3>
        <div class="verifications-zone">
          <form name="frmemailverify" enctype="multipart/form-data" id="frmemailverify" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=m-verification_a" method="post">
            <input type="hidden" name="action" id="action" value="emailverify">
            <input type="hidden" name="vEmail" id="vEmail" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vEmail'];?>
">
            <input type="hidden" name="vName" id="vName" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vLastName'];?>
">
            <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['eEmailVarified']=='No'){?> <img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/verification-no-check.png" alt="">
            <h2><?php echo @LBL_EMAIL;?>
: <?php echo @LBL_NOT;?>
 <?php echo @LBL_VERIFIED;?>
 </h2>
            <?php }else{ ?> <img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/verification-checked.png" alt="">
            <h2><?php echo @LBL_EMAIL;?>
: <?php echo @LBL_VERIFIED;?>
 </h2>
            <?php }?>
            <p><?php echo @LBL_YOUR_EMAIL_ADD;?>
: <strong><?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vEmail'];?>
</strong>&nbsp;&nbsp;<!--(&nbsp;<a href="#">edit</a>&nbsp;)--> </p>
            <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['eEmailVarified']=='No'){?>
              <p><?php echo @LBL_SENT_MAIL;?>
 <?php echo @LBL_NOTGET_EMAIL;?>
 <a href="javascript:void(0);" onclick="javascript:checkmail(); return false;"><?php echo @LBL_CLICK_HERE;?>
</a> <?php echo @LBL_NEW_EMAIL;?>
</p>
              <!--<span><a href="javascript:void(0);" onclick="javascript:checkmail(); return false;"><?php echo @LBL_VERIFY_THIS;?>
</a></span>-->
            <?php }?>
          </form>
        </div>
        <?php if ($_smarty_tpl->getVariable('PHONE_VERIFICATION_REQUIRED')->value=='Yes'){?>
        <div class="verifications-zone">
         <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['ePhoneVerified']=='No'){?>
          <img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/verification-no-check.png" alt="">
          <h2><?php echo @LBL_PHONE;?>
: <?php echo @LBL_NOT;?>
 <?php echo @LBL_VERIFIED;?>
</h2>
         <?php }else{ ?>
         <img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/verification-checked.png" alt="">
         <h2><?php echo @LBL_PHONE;?>
: <?php echo @LBL_VERIFIED;?>
</h2>
         <?php }?>

          <p><?php echo @LBL_YOUR_PHONE;?>
: <strong><?php echo $_smarty_tpl->getVariable('db_country')->value[0]['vPhoneCode'];?>
<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vPhone'];?>
</strong>&nbsp;&nbsp;(&nbsp;<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
personal-information">edit</a>&nbsp;) </p>
         <?php if ($_smarty_tpl->getVariable('db_member')->value[0]['ePhoneVerified']=='No'){?>  <span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
sendsms.php?to_number=<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vPhone'];?>
&country_code=<?php echo $_smarty_tpl->getVariable('db_country')->value[0]['vPhoneCode'];?>
&email=<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vEmail'];?>
"><?php echo @LBL_VERFY_NUMBER;?>
</a></span> <?php }?> </div>
         <?php }?>

		 <?php if ($_smarty_tpl->getVariable('LICENSE_VERIFICATION_REQUIRED')->value=='Yes'){?>

			<!-- LICENSE User varification -->
				<div class="verifications-zone ">
					<form name="frmlicenseverify" enctype="multipart/form-data" id="frmlicenseverify" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=m-verification_a" method="post">
						<input type="hidden" name="action" id="action" value="licenseverify">
						<input type="hidden" name="vEmail" id="vEmail" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vEmail'];?>
">
						<input type="hidden" name="vName" id="vName" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vLastName'];?>
">
						<?php if ($_smarty_tpl->getVariable('db_member')->value[0]['vLicense']==''&&$_smarty_tpl->getVariable('db_member')->value[0]['eLicenseStatus']=='Pending'){?>
						<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/verification-no-check.png" alt="" >
						<h2><?php echo @LBL_LICENSE_USER;?>
: <?php echo @LBL_NOT;?>
 <?php echo @LBL_VERIFIED;?>
 </h2>
						<input type="file" name="vLicense"  id="vLicense" required onchange="filename();">
						<input type="submit" name="submit"  id="submit" value="Add License">
						<?php }elseif($_smarty_tpl->getVariable('db_member')->value[0]['vLicense']!=''&&$_smarty_tpl->getVariable('db_member')->value[0]['eLicenseStatus']=='Pending'){?>
						<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/verification-no-check.png" alt="" >
						<h2><?php echo @LBL_LICENSE_USER;?>
: <?php echo @LBL_NOT;?>
 <?php echo @LBL_VERIFIED;?>
 </h2>
						<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vLicense'];?>
 - Awaiting from Admin side to approve
						<?php }elseif($_smarty_tpl->getVariable('db_member')->value[0]['eLicenseStatus']=='Unapproved'){?>
						<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/verification-no-check.png" alt="" >
						<h2><?php echo @LBL_LICENSE_USER;?>
:  <?php echo @LBL_UNAPPROVED;?>
 </h2>
						<input type="file" name="vLicense"  id="vLicense" required onchange="filename();">
						<input type="submit" name="submit"  id="submit" value="Add License">
						<?php }else{ ?>
						<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/verification-checked.png" alt="" >
						<h2><?php echo @LBL_LICENSE_USER;?>
: <?php echo @LBL_VERIFIED;?>
 </h2>
						<a href="<?php echo $_smarty_tpl->getVariable('path')->value;?>
/<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vLicense'];?>
" target="_blank">view</a>
						<?php }?>
					</form>
				</div>

		 <?php }?>
		 <?php if ($_smarty_tpl->getVariable('CARPAPER_VERIFICATION_REQUIRED')->value=='Yes'){?>

			<!-- LICENSE User varification -->
				<div class="verifications-zone ">
					<form name="frmcarverify" enctype="multipart/form-data" id="frmcarverify" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=m-verification_a" method="post">
						<input type="hidden" name="action" id="action" value="carpaperverify">
						<input type="hidden" name="vEmail" id="vEmail" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vEmail'];?>
">
						<input type="hidden" name="vName" id="vName" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vLastName'];?>
">
						<?php if ($_smarty_tpl->getVariable('db_member')->value[0]['vCarPaper']==''&&$_smarty_tpl->getVariable('db_member')->value[0]['eCarPaperStatus']=='Pending'){?>
						<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/verification-no-check.png" alt="" >
						<h2><?php echo @LBL_CAR_PAPER;?>
: <?php echo @LBL_NOT;?>
 <?php echo @LBL_VERIFIED;?>
 </h2>
						<input type="file" name="vCarPaper"  id="vCarPaper" required onchange="filename();">
						<input type="submit" name="submit"  id="submit" value="Add Proof">
						<?php }elseif($_smarty_tpl->getVariable('db_member')->value[0]['vCarPaper']!=''&&$_smarty_tpl->getVariable('db_member')->value[0]['eCarPaperStatus']=='Pending'){?>
						<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/verification-no-check.png" alt="" >
						<h2><?php echo @LBL_CAR_PAPER;?>
: <?php echo @LBL_NOT;?>
 <?php echo @LBL_VERIFIED;?>
 </h2>
						<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vCarPaper'];?>
 - Awaiting from Admin side to approve
						<?php }elseif($_smarty_tpl->getVariable('db_member')->value[0]['eCarPaperStatus']=='Unapproved'){?>
						<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/verification-no-check.png" alt="" >
						<h2><?php echo @LBL_CAR_PAPER;?>
:  <?php echo @LBL_UNAPPROVED;?>
 </h2>
						<input type="file" name="vCarPaper"  id="vCarPaper" required onchange="filename();">
						<input type="submit" name="submit"  id="submit" value="Add Proof">
						<?php }else{ ?>
						<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/verification-checked.png" alt="" >
						<h2><?php echo @LBL_CAR_PAPER;?>
: <?php echo @LBL_VERIFIED;?>
 </h2>
						<a href="<?php echo $_smarty_tpl->getVariable('path')->value;?>
/<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vCarPaper'];?>
" target="_blank">view</a>
						<?php }?>
					</form>
				</div>

		 <?php }?>
        <?php if ($_smarty_tpl->getVariable('PAYMENTEMAIL_VERIFICATION_REQUIRED')->value=='Yes'){?>
			<div class="verifications-zone">
			<?php if ($_smarty_tpl->getVariable('db_member')->value[0]['ePaymentEmailVerified']=='No'){?>
				<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/verification-no-check.png" alt="">
				<h2><?php echo @LBL_PAYMENT_EMAIL;?>
: <?php echo @LBL_NOT;?>
 <?php echo @LBL_VERIFIED;?>
</h2>
				<p><?php echo @LBL_PAYMENT_EMAIL;?>
: <strong><?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vPaymentEmail'];?>
</strong>&nbsp;&nbsp;(&nbsp;<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
personal-information">edit</a>&nbsp;) </p>
			<?php }else{ ?>
				<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/verification-checked.png" alt="">
				<h2><?php echo @LBL_PAYMENT_EMAIL;?>
: <?php echo @LBL_VERIFIED;?>
</h2>
				<p><?php echo @LBL_PAYMENT_EMAIL;?>
: <strong><?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vPaymentEmail'];?>
</strong></p>
			<?php }?>

			  <form name="frmpaypalemailverify" enctype="multipart/form-data" id="frmpaypalemailverify" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=m-verification_a" method="post">
				<input type="hidden" name="action" id="action" value="paymentemailverify">
				<input type="hidden" name="vEmail" id="vEmail" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vPaymentEmail'];?>
">
				<input type="hidden" name="vName" id="vName" value="<?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vFirstName'];?>
 <?php echo $_smarty_tpl->getVariable('db_member')->value[0]['vLastName'];?>
">
				<?php if ($_smarty_tpl->getVariable('db_member')->value[0]['ePaymentEmailVerified']=='No'){?>
				  <p><?php echo @LBL_SENT_MAIL;?>
 <?php echo @LBL_NOTGET_EMAIL;?>
 <a href="javascript:void(0);" onclick="javascript:checkpaymail(); return false;"><?php echo @LBL_CLICK_HERE;?>
</a> <?php echo @LBL_NEW_EMAIL;?>
</p>
				<?php }?>
			  </form>
			</div>
         <?php }?>
      </div>
    </div>
    <!-------------------------inner-page end----------------->
	<?php $_template = new Smarty_Internal_Template("member_profile_left.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
  </div>
  <div style="clear:both;"></div>
</div>

<script>
function filename(){

	var vLicense = document.getElementById("vLicense").value;
    if(vLicense != '')
    {

        var vLicense=vLicense.split(".");
        if(vLicense[1] != 'pdf' && vLicense[1] != 'doc' && vLicense[1] != 'jpg' && vLicense[1] != 'jpeg' && vLicense[1] != 'gif' && vLicense[1] != 'png')
        {
          alert("Only .jpeg .png, .gif, .doc, .pdf Files Are Supported..");
          document.getElementById("vLicense").value = "";
          return false;
        }
    }
	if(vCarPaper != '')
    {

        var vCarPaper=vCarPaper.split(".");
        if(vCarPaper[1] != 'pdf' && vCarPaper[1] != 'doc' && vCarPaper[1] != 'jpg' && vCarPaper[1] != 'jpeg' && vCarPaper[1] != 'gif' && vCarPaper[1] != 'png')
        {
          alert("Only .jpeg .png, .gif, .doc, .pdf Files Are Supported..");
          document.getElementById("vCarPaper").value = "";
          return false;
        }
    }
}
function checkmail(){
 resp = jQuery("#frmemailverify").validationEngine('validate');// alert(resp);return false;
	if(resp == true){
		document.frmemailverify.submit();
	}else{
		return false;
	}
}
function checkpaymail(){
 resp = jQuery("#frmpaypalemailverify").validationEngine('validate');// alert(resp);return false;
	if(resp == true){
		document.frmpaypalemailverify.submit();
	}else{
		return false;
	}
}

</script>

