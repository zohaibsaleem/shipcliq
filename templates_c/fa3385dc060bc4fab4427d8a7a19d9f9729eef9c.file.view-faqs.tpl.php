<?php /* Smarty version Smarty-3.0.7, created on 2015-07-18 13:01:10
         compiled from "/home/www/xfetch/templates/content/view-faqs.tpl" */ ?>
<?php /*%%SmartyHeaderCode:180915623855aa00be95f0a0-19475563%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fa3385dc060bc4fab4427d8a7a19d9f9729eef9c' => 
    array (
      0 => '/home/www/xfetch/templates/content/view-faqs.tpl',
      1 => 1436782265,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '180915623855aa00be95f0a0-19475563',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="body-inner-part">
  <div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_FAQ;?>
</span></div>
  <div class="main-inner-page">
    <h2><?php echo @LBL_FAQ;?>
</h2>   
    <div class="right-inner-part">
      <h2><?php echo @LBL_FAQ;?>
 
      <span class="faq-select">
        <select name="iFaqcategoryId" id="iFaqcategoryId" onChange="flt_me(this.value);" style="border: 1px solid #CCCCCC;margin: 0 3px;padding: 6px;">
          <option value=""><?php echo @LBL_ALL;?>
</option>
          <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['name'] = "fcat";
$_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_faq_cat')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["fcat"]['total']);
?>
            <option value="<?php echo $_smarty_tpl->getVariable('db_faq_cat')->value[$_smarty_tpl->getVariable('smarty')->value['section']['fcat']['index']]['iFaqcategoryId'];?>
" <?php if ($_smarty_tpl->getVariable('iFaqcategoryId')->value==$_smarty_tpl->getVariable('db_faq_cat')->value[$_smarty_tpl->getVariable('smarty')->value['section']['fcat']['index']]['iFaqcategoryId']){?> selected <?php }?>><?php echo $_smarty_tpl->getVariable('db_faq_cat')->value[$_smarty_tpl->getVariable('smarty')->value['section']['fcat']['index']]['vTitle'];?>
</option>
          <?php endfor; endif; ?>
        </select>
      </span>
      </h2>
      <div class="faq">
        <ul>
          <?php if ($_smarty_tpl->getVariable('iFaqcategoryId')->value==''){?>
          <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["que"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['name'] = "que";
$_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_faq_cat')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['total']);
?>   
          <li> <h2><?php echo $_smarty_tpl->getVariable('db_faq_cat')->value[$_smarty_tpl->getVariable('smarty')->value['section']['que']['index']]['vTitle'];?>
</h2>
            <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_faq')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
            <?php if ($_smarty_tpl->getVariable('db_faq')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iFaqcategoryId']==$_smarty_tpl->getVariable('db_faq_cat')->value[$_smarty_tpl->getVariable('smarty')->value['section']['que']['index']]['iFaqcategoryId']){?>
            <div class="faq-ans" style="width:100%;">
              <h3><a name="<?php echo $_smarty_tpl->getVariable('db_faq')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iFaqId'];?>
">Q.&nbsp;&nbsp;<?php echo $_smarty_tpl->getVariable('db_faq')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vTitle'];?>
</a></h3>
              <span><!--<strong>A.</strong>-->
              <?php echo $_smarty_tpl->getVariable('db_faq')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tAnswer'];?>
</span> </div>
            <?php }?>
            <?php endfor; endif; ?> </li>
          <?php endfor; endif; ?>
          <?php }else{ ?>
          <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["que"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['name'] = "que";
$_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_faq_cat')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["que"]['total']);
?>             
          <?php if ($_smarty_tpl->getVariable('db_faq_cat')->value[$_smarty_tpl->getVariable('smarty')->value['section']['que']['index']]['iFaqcategoryId']==$_smarty_tpl->getVariable('iFaqcategoryId')->value){?>
          <li> <h2><?php echo $_smarty_tpl->getVariable('db_faq_cat')->value[$_smarty_tpl->getVariable('smarty')->value['section']['que']['index']]['vTitle'];?>
</h2>
            <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_faq')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
            <?php if ($_smarty_tpl->getVariable('db_faq')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iFaqcategoryId']==$_smarty_tpl->getVariable('db_faq_cat')->value[$_smarty_tpl->getVariable('smarty')->value['section']['que']['index']]['iFaqcategoryId']){?>
            <div class="faq-ans" style="width:100%;">
              <h3><a name="<?php echo $_smarty_tpl->getVariable('db_faq')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iFaqId'];?>
">Q.&nbsp;&nbsp;<?php echo $_smarty_tpl->getVariable('db_faq')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vTitle'];?>
</a></h3>
              <span><strong>A.</strong>
              <?php echo $_smarty_tpl->getVariable('db_faq')->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tAnswer'];?>
</span> </div>
            <?php }?>
            <?php endfor; endif; ?> </li>
            <?php }?>            
          <?php endfor; endif; ?>
          <?php }?>
        </ul>
      </div>
    </div>
	<?php $_template = new Smarty_Internal_Template("left.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php unset($_template);?>
  </div>
  <div style="clear:both;"></div>
</div>

  <script>
    function flt_me(id){
      window.location="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-faqs&id="+id;
      return false;
    }
  </script>

