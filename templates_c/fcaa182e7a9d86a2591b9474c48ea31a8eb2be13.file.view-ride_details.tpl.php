<?php /* Smarty version Smarty-3.0.7, created on 2016-03-02 00:39:12
         compiled from "/home4/shipcliq/public_html/templates/content/view-ride_details.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20882889156d68a90dc8043-85368101%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fcaa182e7a9d86a2591b9474c48ea31a8eb2be13' => 
    array (
      0 => '/home4/shipcliq/public_html/templates/content/view-ride_details.tpl',
      1 => 1456900749,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20882889156d68a90dc8043-85368101',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
gmap3.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
/jquery.fancybox.css?v=2.1.5" media="screen" />

<style>
	#map-canvas {
    height: 250px;
    margin-top: 10px;
    padding: 0px;
    width: 97.5%;
    border:1px solid #cccccc;
	}
</style>

<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_javascript'];?>
jquery.raty.js"></script>
<div class="body-inner-part">
	<div class="bradcram"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><?php echo @LBL_HOME;?>
</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<?php echo @LBL_TRIP_DETAIL;?>
</span></div>
	<div class="main-inner-page">
		<form name="frmrent" id="frmrent" method="post" action="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=m-bookingform&id=<?php echo $_smarty_tpl->getVariable('id')->value;?>
">
			<input type="hidden" name="imemberId" id="imemberId" value="<?php echo $_smarty_tpl->getVariable('imemberId')->value;?>
">
			<input type="hidden" name="id" id="id" value="<?php echo $_smarty_tpl->getVariable('id')->value;?>
">
		</form>
		<?php if ($_smarty_tpl->getVariable('from')->value!='home'){?>
		<div class="beck-prv"><span><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_list&action=old">&larr; <?php echo @LBL_BACK_TO_SEARCH;?>
</a></span>
			<p>
				<?php if ($_smarty_tpl->getVariable('previous')->value>='0'){?>
				<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_details&id=<?php echo $_smarty_tpl->getVariable('previous')->value;?>
">&larr; <?php echo @LBL_PREVIOUS_RIDE;?>
</a>
				<?php }?>
				<?php if ($_smarty_tpl->getVariable('id')->value>'0'&&$_smarty_tpl->getVariable('id')->value<$_smarty_tpl->getVariable('tot_res')->value){?>
				&nbsp;&nbsp;|&nbsp;&nbsp;
				<?php }?>
				<?php if ($_smarty_tpl->getVariable('next')->value!=''){?>
				<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_details&id=<?php echo $_smarty_tpl->getVariable('next')->value;?>
"><?php echo @LBL_NEXT_RIDE;?>
 &rarr;</a>
				<?php }?>
			</p>
		</div>
		<?php }?>
		<div class="event-details">
			<div class="top-title">
				<!--<div class="bredcrumbs"><span><a href="#">Home</a>&nbsp;&raquo;&nbsp;<a href="#">Find Your Scholar</a>&nbsp;&raquo;&nbsp;Scholar Details</span></div> -->
				<!--<h2><?php echo $_smarty_tpl->getVariable('places_str')->value;?>
</h2>-->
			<span><?php echo $_smarty_tpl->getVariable('places_str')->value;?>
</span></div>
			<div class="details-main">
				<div class="left-deta">
					<?php if (count($_smarty_tpl->getVariable('db_ratings')->value)>0){?>
					<div class="tabs2"><a id="detia" href="javascript:void(0);" onClick="show_hide('details');" class="active"><?php echo @LBL_TRIP_DETAIL;?>
</a> <a id="reva" href="javascript:void(0);" onClick="show_hide('reviews');"><?php echo @LBL_DRIVER_REVIEWS;?>
</a>
						<span style="float:right;"><?php if ($_smarty_tpl->getVariable('db_ride')->value[0]['eLadiesOnly']=='Yes'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
ladies.png"><?php }?><?php if ($_smarty_tpl->getVariable('db_ride')->value[0]['eRidePlaceType']=='Airport'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
airport.png"><?php }?><?php if ($_smarty_tpl->getVariable('db_ride')->value[0]['eRidePlaceType']=='Shopping'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
shopping.png"><?php }?></span>
					</div>
					<?php }else{ ?>
					<div class="tabs2"><a id="detia" href="javascript:void(0);" class="active"><?php echo @LBL_TRIP_DETAIL;?>
</a>
						<span style="float:right;"><?php if ($_smarty_tpl->getVariable('db_ride')->value[0]['eLadiesOnly']=='Yes'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
ladies.png"><?php }?><?php if ($_smarty_tpl->getVariable('db_ride')->value[0]['eRidePlaceType']=='Airport'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
airport.png"><?php }?><?php if ($_smarty_tpl->getVariable('db_ride')->value[0]['eRidePlaceType']=='Shopping'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
shopping.png"><?php }?></span>
					</div>
					<?php }?>
					<div id="geninfo" class="trip-detail-container" style="dispaly:;">
						<div class="trip-detail-data">
							<ul>
								<li><strong><?php echo @LBL_PICK_UP_POINT;?>
</strong><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
search-from-plot.png" alt="" /><?php echo $_smarty_tpl->getVariable('startpoint')->value;?>
</li>
								<li><strong><?php echo @LBL_DROP_OFF_POINT;?>
</strong><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
search-to-plot.png" alt="" /><?php echo $_smarty_tpl->getVariable('endpoint')->value;?>
</li>
								<li><strong><?php echo @LBL_DATE;?>
</strong><?php echo $_smarty_tpl->getVariable('date')->value;?>
</li>
							<li><strong><?php echo @LBL_DEPARTURE_TIME_APP;?>
</strong><?php echo $_smarty_tpl->getVariable('time')->value;?>
</strong></li>
						</ul>
					</div>
					<div class="trip-details">
						<h2><?php echo @LBL_TRIP_DETAIL;?>
</h2>
						<!--<div class="details-trip-photo"><img src="<?php echo $_smarty_tpl->getVariable('stack')->value[0]['image'];?>
" alt=""/></div>  -->
						<?php if ($_smarty_tpl->getVariable('db_ride')->value[0]['tDetails']!=''){?>
						<div class="details-trip-right" style="float:left;">
							<p><?php echo nl2br($_smarty_tpl->getVariable('db_ride')->value[0]['tDetails']);?>
</p>
						</div>
						<?php }?>
						<div class="user-vehicle">
							<ul>
								<?php if ($_smarty_tpl->getVariable('db_ride')->value[0]['eLeaveTime']!=''){?>
								<li><strong><?php echo @LBL_I_WILL_LEAVE;?>
 : </strong><?php echo $_smarty_tpl->getVariable('db_ride')->value[0]['eLeaveTime'];?>
</li>
								<?php }?>
								<?php if ($_smarty_tpl->getVariable('doc')->value=="Yes"){?>
								<li><strong><?php echo @LBL_DOCUMENT;?>
 : </strong><?php echo $_smarty_tpl->getVariable('docprice')->value;?>
 (<?php echo $_smarty_tpl->getVariable('docweight')->value;?>
) <?php echo $_smarty_tpl->getVariable('docunit')->value;?>
</li>
								<?php }?>
								<?php if ($_smarty_tpl->getVariable('box')->value=="Yes"){?>
								<li><strong><?php echo @LBL_BOX;?>
 : </strong><?php echo $_smarty_tpl->getVariable('boxprice')->value;?>
 (<?php echo $_smarty_tpl->getVariable('boxweight')->value;?>
) <?php echo $_smarty_tpl->getVariable('boxunit')->value;?>
</li>
								<?php }?>
								<?php if ($_smarty_tpl->getVariable('lug')->value=="Yes"){?>
								<li><strong><?php echo @LBL_LUGGAGE;?>
 : </strong><?php echo $_smarty_tpl->getVariable('lugprice')->value;?>
 (<?php echo $_smarty_tpl->getVariable('lugweight')->value;?>
) <?php echo $_smarty_tpl->getVariable('lugunit')->value;?>
</li>
								<?php }?>

							</ul>
						</div>
						<!-- Bhumi <div class="user-vehicle">
							<ul>
							<li><strong><?php echo @LBL_DETOUR;?>
</strong><?php echo $_smarty_tpl->getVariable('db_ride')->value[0]['eWaitTime'];?>
</li>
							<li><strong><?php echo @LBL_SCHEDULE_FLEXIBLITY;?>
</strong><?php echo $_smarty_tpl->getVariable('db_ride')->value[0]['eLeaveTime'];?>
</li>
							<li><strong><?php echo @LBL_LAGGAGE_SIZE;?>
</strong><!--<img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('db_ride')->value[0]['eLuggageSize'];?>
.png" alt="" />--888<?php echo $_smarty_tpl->getVariable('db_ride')->value[0]['eLuggageSize'];?>
</li>
							<li><strong><?php echo @LBL_CAR;?>
</strong><?php echo $_smarty_tpl->getVariable('stack')->value[0]['carname'];?>
&nbsp;&nbsp;<b><?php echo $_smarty_tpl->getVariable('stack')->value[0]['carcomfort'];?>
</b></li>
							<li><strong><?php echo @LBL_CAR_SHARE;?>
 <?php echo @LBL_PREFERENCES;?>
</strong><?php echo $_smarty_tpl->getVariable('stack')->value[0]['pref1'];?>
</li>
							</ul>
							<img src="<?php echo $_smarty_tpl->getVariable('stack')->value[0]['car_image'];?>
" alt="" />
						</div> -->
					</div>
				</div>
				<div class="user-comment-list"  id="ratinfo" style="display:none;width:100%;">
					<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['name'] = "rat";
$_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('db_ratings')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["rat"]['total']);
?>
					<div style="width:100%;background: none repeat scroll 0 0 #F6F7F7;border: 1px solid #DEDFDF;<?php if ($_smarty_tpl->getVariable('smarty')->value['section']['rat']['first']){?><?php }else{ ?>margin-top:10px;<?php }?>">
						<table width="100%">
							<tr>
								<td valign="top" width="10%" style="text-align:center;border-right:1px solid #cccccc;">
									<img src="<?php echo $_smarty_tpl->getVariable('db_ratings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rat']['index']]['image'];?>
"><br><br>
									<span style="color: #05A2DB;font-size: 15px;"><?php echo @LBL_RATE;?>
: <?php echo $_smarty_tpl->getVariable('db_ratings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rat']['index']]['iRate'];?>

									</td>
									<td width="90%" valign="top">
										<table width="100%">
											<tr>
												<td width="20%" style="text-align:left;color: #000000;font-size: 15px;font-weight: normal;"><?php echo $_smarty_tpl->getVariable('db_ratings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rat']['index']]['vFirstName'];?>
&nbsp;<?php echo $_smarty_tpl->getVariable('db_ratings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rat']['index']]['vLastName'];?>
</td>
												<td width="80%" style="text-align:right;"><?php echo $_smarty_tpl->getVariable('db_ratings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rat']['index']]['rating'];?>
</td>
											</tr>
											<tr>
												<td width="100%" style="text-align:left;" colspan="2"><?php echo nl2br($_smarty_tpl->getVariable('db_ratings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rat']['index']]['tFeedback']);?>
</td>
											</tr>
											<tr>
												<td width="100%" style="text-align:right;" colspan="2"><strong><?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTimeFormat($_smarty_tpl->getVariable('db_ratings')->value[$_smarty_tpl->getVariable('smarty')->value['section']['rat']['index']]['dAddedDate']);?>
</strong></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</div>
						<?php endfor; endif; ?>
					</div>
				</div>
				<div class="right-deta">
					<h2 id="show_link"><a href="javascript:void(0);" onClick="from_to();"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
location.png" alt="" /><?php echo @LBL_SHOW_MAP;?>
</a></h2>
					<h2 id="hide_link" style="display:none;"><a href="javascript:void(0);" onClick="from_to_hide();"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
location.png" alt="" /><?php echo @LBL_HIDE_MAP;?>
</a></h2>
					<div class="donate">
						<div id="map" style="display:none;">
							<div id="map-canvas" class="gmap3"></div>
						</div>
						<div class="trip-price-container">
							<!--Bhumi  <div class="big-price-container"> <span class="price-green" style="color:<?php echo $_smarty_tpl->getVariable('price_color')->value;?>
;"><?php echo $_smarty_tpl->getVariable('price')->value;?>
</span><span><?php echo @LBL_PER_PESSENGER1;?>
</span> </div>
								<div class="big-price-container"> <span class="price-black"><?php echo $_smarty_tpl->getVariable('seats')->value;?>
</span><span><?php echo @LBL_SEATS_LEFT;?>
 </span>
								<p><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/user-man-driver-36-transparent.png" alt="" /> <img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
empty-seat.png" /> <img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
empty-seat.png" /></p>
							</div>  -->
							<?php if ($_smarty_tpl->getVariable('button_show')->value=='Yes'){?>
							<div class="support_link">
								<em><?php echo @LBL_CLICK_FOR_BOOK;?>
:</em>
								<?php if ($_smarty_tpl->getVariable('db_ride')->value[0]['iMemberId']!=$_smarty_tpl->getVariable('sess_iMemberId')->value){?>
									<?php if ($_smarty_tpl->getVariable('Remaindoc')->value==0&&$_smarty_tpl->getVariable('Remainbox')->value==0&&$_smarty_tpl->getVariable('Remainluggage')->value==0){?>
										<a href="javascript:void()" class="donateGreenBtn" id="support_link">
											<span class="support_text"><?php echo @LBL_BOOK_SEAT_FULL;?>
</span>
										</a>
									<?php }elseif(@PAYMENT_OPTION=='Contact'){?>
										<a id="support_link" class="donateGreenBtn" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-user_profile&iMemberId=<?php echo $_smarty_tpl->getVariable('stack')->value[0]['iMemberId'];?>
&megop=1"><span class="support_text"><?php echo @LBL_CONTACT_DRIVER;?>
</span></a>
									<?php }else{ ?>
										<a href="javascript:void()" class="donateGreenBtn" id="support_link" onClick = "chk_booking_frm();">
											<span class="support_text"><?php echo @LBL_BOOK_SEAT;?>
</span>
										</a>
									<?php }?>
								<?php }else{ ?>
								<a href="#" class="donateGreenBtn" id="support_link" >
									<span class="support_text"><?php echo @LBL_CANT_BOOK;?>
</span>
								</a>
								<?php }?>
							</div>
							<?php }else{ ?>
							Ride is Expired
							<?php }?>
							</div>
							<div class="clint-category">
								<ul>
									<li>
										<h2><?php echo @LBL_DRIVER1;?>
</h2>
										<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-user_profile&iMemberId=<?php echo $_smarty_tpl->getVariable('stack')->value[0]['iMemberId'];?>
">
											<div class="trip-box-right"> <img src="<?php echo $_smarty_tpl->getVariable('stack')->value[0]['image'];?>
" alt="" />
												<p><strong><?php echo $_smarty_tpl->getVariable('stack')->value[0]['drivername'];?>
</strong>  <em><?php echo $_smarty_tpl->getVariable('stack')->value[0]['pref'];?>
</em></p>
												<?php echo $_smarty_tpl->getVariable('stack')->value[0]['rating'];?>
<!--<span><?php echo @LBL_GOOD_DRIVE_SKILL;?>
 � 3 / 3 </span>-->
											</div>
										</a>
										<?php if ($_SESSION['sess_iMemberId']!=''){?>
										<div class="ask-d">
											<?php if ($_smarty_tpl->getVariable('db_ride')->value[0]['iMemberId']!=$_smarty_tpl->getVariable('sess_iMemberId')->value){?>
											<a id="support_link" class="donateGreenBtn" href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-user_profile&iMemberId=<?php echo $_smarty_tpl->getVariable('stack')->value[0]['iMemberId'];?>
&megop=1"><span class="support_text"><?php echo @LBL_ASK_DRIVER;?>
</span></a>
											<?php }?>
										</div>
										<?php }?>
									</li>
								</ul>
							</div>
							<div class="clint-category1">
								<h2><?php echo @LBL_MEMBER_VERIFICATION;?>
</h2>
								<ul>
									<?php if ($_smarty_tpl->getVariable('PHONE_VERIFICATION_REQUIRED')->value=='Yes'){?>
									<li style="background:#F4F4F4;"><?php if ($_smarty_tpl->getVariable('stack')->value[0]['ePhoneVerified']=='No'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
verification-no-check.png" alt="" /><?php }else{ ?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/verification-checked.png" alt="" /><?php }?><?php echo @LBL_PHONE_VARIFIED;?>
</li>
									<?php }?>
									<!--
									<li style="background:#F4F4F4;"><?php if ($_smarty_tpl->getVariable('stack')->value[0]['eEmailVarified']=='No'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
verification-no-check.png" alt="" /><?php }else{ ?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/verification-checked.png" alt="" /><?php }?><?php echo @LBL_EMAIL_VERIFIED;?>
 </li>
									<li style="background:#F4F4F4;"><?php if ($_smarty_tpl->getVariable('stack')->value[0]['ePaymentEmailVerified']=='No'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
verification-no-check.png" alt="" /><?php }else{ ?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/verification-checked.png" alt="" /><?php }?><?php echo @LBL_PAYPALEMAIL_VARIFICATION;?>
 </li>-->
									<?php if ($_smarty_tpl->getVariable('LICENSE_VERIFICATION_REQUIRED')->value=='Yes'){?>
									<li style="background:#F4F4F4;"><?php if ($_smarty_tpl->getVariable('stack')->value[0]['eLicenseStatus']!='Approved'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
verification-no-check.png" alt="" /><?php }else{ ?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/verification-checked.png" alt="" /><?php }?><?php echo @LBL_LICENSE_VERIFIED;?>
 </li>
									<?php }?>
									<?php if ($_smarty_tpl->getVariable('CARPAPER_VERIFICATION_REQUIRED')->value=='Yes'){?>
									<li style="background:#F4F4F4;"><?php if ($_smarty_tpl->getVariable('stack')->value[0]['eCarPaperStatus']!='Approved'){?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
verification-no-check.png" alt="" /><?php }else{ ?><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
/verification-checked.png" alt="" /><?php }?><?php echo @LBL_CAR_PAPER_VERIFIED;?>
 </li>
									<?php }?>
									<!-- <li style="background:#F4F4F4;"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
fb-right-i.png" alt="" /><?php echo $_smarty_tpl->getVariable('stack')->value[0]['vFbFriendCount'];?>
 <?php echo @LBL_FRIENDS;?>
</li> -->
								</ul>
							</div>
							<div class="clint-category2">
								<h2><?php echo @LBL_MEMBER_ACTIVITY;?>
</h2>
								<ul>
									<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
rides-right-i.png" alt="" /><?php echo $_smarty_tpl->getVariable('stack')->value[0]['tot_ride'];?>
 <?php echo @LBL_RIDES_OFFERED;?>
</li>
								<!--<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
fb-right-i.png" alt="" /><?php echo $_smarty_tpl->getVariable('stack')->value[0]['vFbFriendCount'];?>
 <?php echo @LBL_FACEBOOK_FRIEND;?>
</li>-->
									<!--<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
rate-right-i.png" alt="" />100 % response rate </li>-->
									<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
clock-right-i.png" alt="" />Last online: <?php echo $_smarty_tpl->getVariable('generalobj')->value->DateTime($_smarty_tpl->getVariable('stack')->value[0]['tLastLogin']);?>
 </li>
									<li><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
member-right-i.png" alt="" /><?php echo @LBL_MEMBER_SINCE;?>
: <?php echo $_smarty_tpl->getVariable('stack')->value[0]['JoinDate'];?>
 </li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div style="clear:both;"></div>
			</div>
		</div>
		<div style="clear:both;"></div>
	</div>
	<div style="display:none">
		<div id="loginmodel">
			<div class="form-login">

				<h4><?php echo @LBL_NEW_CUST;?>
?</h4>
				<p><?php echo @LBL_CUST_DESCRIPTION;?>
</p>
				<div class="singlerow-login-log">
					<a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
sign-up"><?php echo @LBL_REGISTER_NOW;?>
</a>
				</div>
				<div class="bot-line-login"><?php echo @LBL_OR;?>
</div>
				<Br/>
				<h4><?php echo @LBL_LOGIN;?>
</h4>
				<form name="login_box" id="login_box" method="post">
					<input type="hidden" name="lgfor" id="lgfor" value="">
					<input type="hidden" name="from" id="from" value="">

					<input type="hidden" name="action" id="action" value="loginchk">
					<div class="inner-form1">
						<label class="email-label"><?php echo @LBL_EMAIL;?>
 : <strong>*</strong></label>
						<div class="singlerow-login">
							<input type="text" name="vEmail" id="vEmail" class="validate[required,custom[email]] form-input-login" value="<?php echo $_smarty_tpl->getVariable('vEmail')->value;?>
">
						</div>
						<label class="email-label"><?php echo @LBL_PASSWORD;?>
 : <strong>*</strong></label>
						<div class="singlerow-login">
							<input type="password"  name="vPassword" id="vPassword" class="validate[required,minSize[6]] form-input-login" value="<?php echo $_smarty_tpl->getVariable('vPassword')->value;?>
">
						</div>
						<label class="email-label"></label>
						<div class="singlerow-login-log">
							<a onClick="check_login_small();" href="javascript:void(0);"><?php echo @LBL_SUBMIT;?>
</a>
							<a onclick="document.login_box.reset();return false;" href="javascript:void(0);"><?php echo @LBL_RESET;?>
</a>
						</div>
						<div style="clear:both;"></div>
					</div>
				</form>
				<div class="bot-line-login"><?php echo @LBL_OR;?>
</div>
				<table width="100%" cellspacing="0" cellpadding="7" border="0">
					<tbody>
						<tr>
							<td align="center"><button class="btn btn-primary" onclick="fbconnect();return false;">
								<a style="color:#fff;" onclick="fbconnect();"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
fb-but-icon.png" alt="" /><?php echo @LBL_CONNECT_FACEBOOK;?>
</a>
							</button></td>
						</tr>
					</tbody>
				</table>
		</div>
	</div>
</div>
	
	<script>
		function show_hide(type){
			if(type == 'details'){
				$("#detia").addClass( "active" );
				$("#reva").removeClass( "active" );
				$('#ratinfo').hide();
				$('#geninfo').show();
				}else{
				$("#reva").addClass( "active" );
				$("#detia").removeClass( "active" );
				$('#geninfo').hide();
				$('#ratinfo').show();
			}
		}

		function clearThat(){
			var opts = {};
			opts.name = ["marker", "directionsrenderer"];
			opts.first = true;
			$('#map-canvas').gmap3({clear:opts});
		}
		function from_to(){
			clearThat();

			var waypts = [];
			var loc1 = '<?php echo $_smarty_tpl->getVariable('loc1')->value;?>
';
			var loc2 = '<?php echo $_smarty_tpl->getVariable('loc2')->value;?>
';
			var loc3 = '<?php echo $_smarty_tpl->getVariable('loc3')->value;?>
';
			var loc4 = '<?php echo $_smarty_tpl->getVariable('loc4')->value;?>
';
			var loc5 = '<?php echo $_smarty_tpl->getVariable('loc5')->value;?>
';
			var loc6 = '<?php echo $_smarty_tpl->getVariable('loc6')->value;?>
';

			if(loc1 != ''){
				waypts.push({location:loc1, stopover:true});
			}
			if(loc2 != ''){
				waypts.push({location:loc2, stopover:true});
			}
			if(loc3 != ''){
				waypts.push({location:loc3, stopover:true});
			}
			if(loc4 != ''){
				waypts.push({location:loc4, stopover:true});
			}
			if(loc5 != ''){
				waypts.push({location:loc5, stopover:true});
			}
			if(loc6 != ''){
				waypts.push({location:loc6, stopover:true});
			}

			$("#map-canvas").gmap3({
				getroute:{
					options:{
						origin:'<?php echo $_smarty_tpl->getVariable('db_ride')->value[0]['vMainDeparture'];?>
',
						destination:'<?php echo $_smarty_tpl->getVariable('db_ride')->value[0]['vMainArrival'];?>
',
						waypoints:waypts,
						travelMode: google.maps.DirectionsTravelMode.DRIVING
					},
					callback: function(results){
						if (!results) return;
						$(this).gmap3({
							map:{
								options:{
									zoom: 13,
									center: [-33.879, 151.235]
								}
							},
							directionsrenderer:{
								options:{
									directions:results
								}
							}
						});
					}
				}
			});
			$('#map').show();
			$('#show_link').hide();
			$('#hide_link').show();
		}

		function from_to_hide(){
			//clearThat();
			$('#map').hide();
			$('#hide_link').hide();
			$('#show_link').show();
		}
		function chk_booking_frm(){
			if('<?php echo $_SESSION['sess_iMemberId'];?>
' == ''){
				$.fancybox("#loginmodel");return false;
				}else{
				document.frmrent.submit();
			}
		}
		function check_login_small(){
			jQuery("#login_box").validationEngine('attach',{scroll: false});
			resp = jQuery("#login_box").validationEngine('validate');
			if(resp == true){
				var request = $.ajax({
					type: "POST",
					url: site_url+'index.php?file=c-login',
					data: $("#login_box").serialize(),

					success: function(data) {

						$("#loginloader").hide();
						$("#loginbtn").show();
						if(data == 0){
							showNotification({type : 'error', message: '<?php echo @LBL_LOGIN_FAILED_USER_PASS;?>
.'});
						}
						else if(data == 1){
							showNotification({type : 'error', message: '<?php echo @LBL_ACC_NOT_ACTIVE;?>
.'});
						}
						else if(data == 2){    // Male user logged in

							if('<?php echo $_smarty_tpl->getVariable('db_ride')->value[0]['eLadiesOnly'];?>
' == 'Yes')
							{
								window.location = '<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
index.php?file=c-ride_details&from=<?php echo $_REQUEST['from'];?>
&id=<?php echo $_REQUEST['id'];?>
&dt=<?php echo $_REQUEST['dt'];?>
&ret=<?php echo $_REQUEST['ret'];?>
';
							}
							else {
								document.frmrent.submit();
							}
						}
						else if(data == 3) { // Female user logged in

							document.frmrent.submit();
						}
						else{
							//alert('<?php echo $_SESSION['sess_iMemberId'];?>
')'
							document.frmrent.submit();
						}
					}
				});

				request.fail(function(jqXHR, textStatus) {
					alert( "Request failed: " + textStatus );
				});
				}else{
				return false;
			}
		}

		function fbconnect()
		{
			javscript:window.location='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
fbconnect.php';
		}
	</script>

