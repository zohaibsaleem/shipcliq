<?php /* Smarty version Smarty-3.0.7, created on 2015-04-02 11:16:31
         compiled from "/home/www/blablaclone/templates/footer4.tpl" */ ?>
<?php /*%%SmartyHeaderCode:987648054551cd7b7e4ce72-74164863%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fefa0cca8550cbbf8766cef85f94580ecb2b07bb' => 
    array (
      0 => '/home/www/blablaclone/templates/footer4.tpl',
      1 => 1427951905,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '987648054551cd7b7e4ce72-74164863',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div id="footer">
<div class="footer-inner">
  <div class="footer-top-part">
    <div class="footer-top-logo-part">
      <div class="footer-logo"><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
"><img src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/logo.png" alt="" /></a></div>
      <span> <a href="#"><img onmouseout="this.src='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/fb.png'" onmouseover="this.src='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/fb-hover.png'" onclick="return submitsearch(document.frmsearch);" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/fb.png" alt=""/></a> <a href="#"><img onmouseout="this.src='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/twi.png'" onmouseover="this.src='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/twi-hover.png'" onclick="return submitsearch(document.frmsearch);" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/twi.png" alt=""/></a> <a href="#"><img onmouseout="this.src='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/p-icon.png'" onmouseover="this.src='<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/p-icon-hover.png'" onclick="return submitsearch(document.frmsearch);" src="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_images'];?>
home/<?php echo $_smarty_tpl->getVariable('THEME')->value;?>
/p-icon.png" alt=""/></a></span> </div>
    <div class="footer-mid-part">
      <div class="footer-box1">
        <h2><?php echo @LBL_USING;?>
 <?php echo @LBL_CAR_SHARING_WEBSITE;?>
</h2>
        <ul>
          <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
how-we-works" <?php if ($_smarty_tpl->getVariable('iPageId')->value==4){?>class="active"<?php }?>><?php echo @LBL_HOW_WORKS;?>
</a></li>
          <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
trust-safety-insurance" <?php if ($_smarty_tpl->getVariable('iPageId')->value==5){?>class="active"<?php }?>><?php echo @LBL_TRUST_SAFTEY_INSURENCE1;?>
</a></li>
          <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
faqs" <?php if ($_smarty_tpl->getVariable('script')->value=='faqs'){?>class="active"<?php }?>><?php echo @LBL_FAQ;?>
</a></li>
          <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
Member-Stories" <?php if ($_smarty_tpl->getVariable('script')->value=='member_stories'){?>class="active"<?php }?>><?php echo @LBL_MEMBER_STORY;?>
</a></li>
          <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
terms-conditions" <?php if ($_smarty_tpl->getVariable('iPageId')->value==3){?>class="active"<?php }?>><?php echo @LBL_TERMS_CONDITIONS;?>
</a></li>
        </ul>
      </div>
      <div class="footer-box2">
        <h2><?php echo @LBL_OUR_COMPANY;?>
</h2>
        <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
aboutus" <?php if ($_smarty_tpl->getVariable('iPageId')->value==1){?>class="active"<?php }?>><?php echo @LBL_ABOUT_US;?>
</a></li>
        <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
contactus" <?php if ($_smarty_tpl->getVariable('script')->value=='contactus'){?>class="active"<?php }?>><?php echo @LBL_CONTACT_US1;?>
</a></li>
        <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
Partners" <?php if ($_smarty_tpl->getVariable('iPageId')->value==9){?>class="active"<?php }?>><?php echo @LBL_PARTNERS;?>
</a></li>
        <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
we-are-hiring" <?php if ($_smarty_tpl->getVariable('iPageId')->value==6){?>class="active"<?php }?>><?php echo @LBL_HIRING;?>
</a></li>
        <li><a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
privacy-policy" <?php if ($_smarty_tpl->getVariable('iPageId')->value==2){?>class="active"<?php }?>><?php echo @LBL_PRIVACY_POLICY;?>
</a></li>
      </div>
      <div class="footer-box3">
        <form name="frmnewsletter" id="frmnewsletter" method="post" action="">
        <div class="footer-search-box">
          <h2><?php echo @LBL_SUBSRIBE;?>
 <?php echo @LBL_NEWSLETTERS;?>
</h2>
          <span>
          <input name="vNewsletterEmail" id="vNewsletterEmail" type="text" class="validate[required,custom[email]] footer-search" placeholder="Enter E-mail Address" />
          <input value="<?php echo @LBL_SUBMIT;?>
" id="newletter_subscribe" name="" type="button" class="search-button" onClick="checknewsletter();" style="display:;"/>
          <input value="<?php echo @LBL_WAIT;?>
"   id="newletter_loading" name="" type="button" class="search-button" onClick="checknewsletter();" style="display:none;"/>
          </span> </div>
        <p><?php echo @LBL_FOOTER_TERMS;?>
 </p>
        </form>  
      </div>
    </div>
  </div>
  <div class="footer-bottom-part">
    <p>&copy; <?php echo @LBL_COPYRIGHT1;?>
 2015 - <a href="<?php echo $_smarty_tpl->getVariable('tconfig')->value['tsite_url'];?>
">www.blablacarscript.com</a>. <?php echo @BL_ALL_RIGHT_RESERVE;?>
.<br />
      <?php echo @LBL_SITE_DESIGN_DEVELOP;?>
 : <a href="http://www.esiteworld.com/" target="_blank" class="site-name">eSiteWorld.com</a></p>
  </div>
</div>
</div>

<script>
$('#frmnewsletter').bind('keydown',function(e){
    if(e.which == 13){
      checknewsletter(); return false;
    }
});
</script>  

