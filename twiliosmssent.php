<?php
    /* Send an SMS using Twilio. You can run this file 3 different ways:
     *
     * - Save it as sendnotifications.php and at the command line, run 
     *        php sendnotifications.php
     *
     * - Upload it to a web host and load mywebhost.com/sendnotifications.php 
     *   in a web browser.
     * - Download a local server like WAMP, MAMP or XAMPP. Point the web root 
     *   directory to the folder containing this file, and load 
     *   localhost:8888/sendnotifications.php in a web browser.
     */
 
    // Step 1: Download the Twilio-PHP library from twilio.com/docs/libraries, 
    // and move it into the folder containing this file.
    ob_start();
	define( '_TEXEC', 1 );
	define('TPATH_BASE', dirname(__FILE__) );
	define( 'DS', DIRECTORY_SEPARATOR );
	require_once ( TPATH_BASE .DS.'includes'.DS.'defines.php' );
	require_once ( TPATH_BASE .DS.'includes'.DS.'configuration.php' );
    require TPATH_LIBRARIES.'/Services/Twilio.php';
    // Step 2: set our AccountSid and AuthToken from www.twilio.com/user/account
    $AccountSid = "ACab757e9763d1e9ddc169f82a75249f71";
    $AuthToken = "d572721ea8253d197728740aac6f244b";
 
    // Step 3: instantiate a new Twilio Rest Client
    $client = new Services_Twilio($AccountSid, $AuthToken);
 
    // Step 4: make an array of people we know, to send them a message. 
    // Feel free to change/add your own phone number and name here.
    $people = array(
        "+918000834964" => "Hemali Sanghvi",
        "+919825570361" => "Chirag Darji",
      
    );
 
    // Step 5: Loop over all our friends. $number is a phone number above, and 
    // $name is the name next to it
    foreach ($people as $number => $name) {
 
        $sms = $client->account->messages->sendMessage(
 
        // Step 6: Change the 'From' number below to be a valid Twilio number 
        // that you've purchased, or the (deprecated) Sandbox number
        //from number
        "224-900-2979", 
 
        // the number we are sending to - Any phone number
        $number,
 
        // the sms body
        "Hey $name,please confirm your ride with sms CON BG201 and send to this number +12249002979"
        );
 
        // Display a confirmation message on the screen
        echo "Sent message to $name";
    }
