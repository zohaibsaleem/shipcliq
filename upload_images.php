<?php
/*
Uploadify v2.1.4
Release Date: November 8, 2010

Copyright (c) 2010 Ronnie Garcia, Travis Nickels

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
session_start();
define( '_TEXEC', 1 );
define('TPATH_BASE', dirname(__FILE__) );
define( 'DS', DIRECTORY_SEPARATOR );

require_once ( TPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once ( TPATH_BASE .DS.'includes'.DS.'configuration.php' );                                                                     

$user_sessionid = $_REQUEST['id'];   
$temp_gallery = $tconfig["tsite_temp_gallery"]; 

include_once(TPATH_LIBRARIES."/general/Imagecrop.class.php");
$thumb = new thumbnail();

if (!empty($_FILES)) {
   
	if(!is_dir($tconfig['tsite_upload_images_tmpphotos_path'].$user_sessionid)){
		mkdir($tconfig['tsite_upload_images_tmpphotos_path'].$user_sessionid, 0777);
	}
	else
	{	   
    $target_path = $tconfig['tsite_upload_images_tmpphotos_path'].$user_sessionid."/";
    $dir_handle = @opendir($target_path);  
    while ($file = readdir($dir_handle)) {
      unlink($target_path.$file);
    }
  }     
	$tempFile = $_FILES['Filedata']['tmp_name'];
	$image_name = $_FILES['Filedata']['name'];  
	$target_path = $tconfig['tsite_upload_images_tmpphotos_path'].$user_sessionid.DS; 
  //$imgname = $generalobj->general_upload_image_rb($tempFile,$image_name,$target_path, $tconfig["tsite_upload_images_photo_size1"], $tconfig["tsite_upload_images_photo_size2"], $tconfig["tsite_upload_images_photo_size3"],'','','','Y');
  $vdFile = $generalobj->fileupload($target_path,$tempFile,$image_name,$prefix='', $vaildExt="jpg,jpeg,gif,png");
  $imgname = $vdFile[0];
  if(is_file($target_path.'/'.$imgname))
  {
    $rrb = 'yes';
    list($width, $height, $type, $attr)= getimagesize($target_path.'/'.$imgname);     
    //$new_width = round(((77/100)*$width));
    //$new_height = round(((245/100)*$height));
    
    $percent_resizing = 30;    
    $new_width = round((($percent_resizing/100)*$width));
    $new_height = round((($percent_resizing/100)*$height));  
    
    $target_url = $tconfig['tsite_upload_images_tmpphotos'].$user_sessionid.DS.$imgname;
  } 
  else
  {
    $rrb = 'no';
  }          
	echo $rrb.'|'.$imgname.'|'.$new_width.'|'.$new_height.'|'.$target_url;
	exit;
}
exit;
?>