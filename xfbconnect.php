<?php 
/**
 * Copyright 2011 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
ob_start();
session_start();
define( '_TEXEC', 1 );
define('TPATH_BASE', dirname(__FILE__) );
define( 'DS', DIRECTORY_SEPARATOR );
require_once ( TPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once ( TPATH_BASE .DS.'includes'.DS.'configuration.php' );

require TPATH_LIBRARIES.'/facebook/facebook.php';
// Create our Application instance (replace this with your appId and secret).
$facebook = new Facebook(array(
  //'appId'  => '736353516385244',
  //'secret' => '3d6d860db78ef67ba6488581a7eb0164',
  'appId'  => $FACEBOOK_APPID,
  'secret' => $FACEBOOK_SECRET,
));


include_once(TPATH_LIBRARIES."/general/Imagecrop.class.php");
$thumb = new thumbnail();
$temp_gallery = $tconfig["tsite_temp_gallery"];

include_once(TPATH_LIBRARIES."/general/SimpleImage.class.php");
$img = new SimpleImage();           


// Get User ID
$user = $facebook->getUser();
// We may or may not have this data based on whether the user is logged in.
//
// If we have a $user id here, it means we know the user is logged into
// Facebook, but we don't know if the access token is valid. An access
// token is invalid if the user logged out of Facebook.

//exit;
$ctype=$_REQUEST['ctype'];

if($ctype == ''){
    $ctype = "fblogin";
}

if($ctype == "fblogin"){
    if ($user) {
    
      try {
        // Proceed knowing you have a logged in user who's authenticated.
       $user_profile = $facebook->api('/me?fields=id,picture,username,first_name,last_name,email,location,hometown,gender');
      
        $location = $user_profile['location']['name'];
        if($location == ""){
            $location = $user_profile['hometown']['name'];
        }
        $location_arr = array();
        $location_arr = explode(",",$location);
        $city = $location_arr[0];
        $country_long = trim($location_arr[1]); 
    
        $sql = "SELECT vCountryCode FROM country WHERE vCountry='".$country_long."'";
        $db_counrtry_code = $obj->MySQLSelect($sql);
        $country_short = $db_counrtry_code[0]['vCountryCode'];
    
        $sql = "SELECT iMemberId,vImage FROM member WHERE vEmail='".$user_profile['email']."'";
        $db_user = $obj->MySQLSelect($sql);
      
        if(count($db_user) > 0){
    		
    		$_SESSION['sess_iMemberId']=$db_user[0]['iMemberId'];		
    		$_SESSION["sess_vFirstName"]=$user_profile['first_name'];
    		$_SESSION["sess_vLastName"]=$user_profile['last_name'];
    		$_SESSION["sess_vEmail"]=$user_profile['email'];       	
    		$_SESSION["sess_eGender"]=$user_profile[0]['eGender'];
    		
    		
    		$Photo_Gallery_folder = $tconfig["tsite_upload_images_member_path"].$_SESSION['sess_iMemberId']."/";
    		
    	    unlink($Photo_Gallery_folder.$db_user[0]['vImage']);
    		unlink($Photo_Gallery_folder."1_".$db_user[0]['vImage']);
    		unlink($Photo_Gallery_folder."2_".$db_user[0]['vImage']);
    		unlink($Photo_Gallery_folder."3_".$db_user[0]['vImage']);   
    		unlink($Photo_Gallery_folder."4_".$db_user[0]['vImage']);   
    		        
            $baseurl =  "http://graph.facebook.com/".$user."/picture?type=large";
            $url = $user.".jpg";
            $image_name =  system("wget -O ".$Photo_Gallery_folder.$url." ".$baseurl);
            if(!is_dir($Photo_Gallery_folder))
            {
    	      mkdir($Photo_Gallery_folder, 0777);
    	    }
    	              
            $baseurl =  "http://graph.facebook.com/".$user."/picture?type=large";
            $url = $user.".jpg";
            $image_name =  system("wget -O ".$Photo_Gallery_folder.$url." ".$baseurl);
            //$imgname = $generalobj->general_upload_image1($url,$Photo_Gallery_folder,$tconfig["tsite_upload_images_member_size1"],$tconfig["tsite_upload_images_member_size2"],$tconfig["tsite_upload_images_member_size3"],$tconfig["tsite_upload_images_member_size4"],'','','','Y','');
           
            
            if(is_file($Photo_Gallery_folder.$url))
            {
               include_once(TPATH_LIBRARIES."/general/SimpleImage.class.php");
               $img = new SimpleImage();           
               list($width, $height, $type, $attr)= getimagesize($Photo_Gallery_folder.$url);           
      
               if($width < $height){
                  $final_width = $width;
               }else{
                  $final_width = $height;
               }       
               $img->load($Photo_Gallery_folder.$url)->crop(0, 0, $final_width, $final_width)->save($Photo_Gallery_folder.$url);
               $imgname = $generalobj->img_data_upload($Photo_Gallery_folder,$url,$Photo_Gallery_folder, $tconfig["tsite_upload_images_member_size1"], $tconfig["tsite_upload_images_member_size2"], $tconfig["tsite_upload_images_member_size3"],"");                       
            }  
             @unlink($Photo_Gallery_folder.$url);
             //list($width, $height, $type, $attr)= getimagesize($Photo_Gallery_folder.'2_'.$imgname);           
             
             //if($width < $height){
               // $final_width = $width;
             //}else{
              //  $final_width = $height;
             //}
             
          //   $img->load($Photo_Gallery_folder.'2_'.$imgname)->crop(0, 0, $final_width, $final_width)->save($Photo_Gallery_folder.'1_'.$imgname);
            
             $myfriends = $facebook->api( array(
                     'method' => 'fql.query',
                     'query' => 'SELECT friend_count FROM user WHERE uid ='.$user.'',
             ));
    
             $friends_count = $myfriends[0]["friend_count"];
             
             //$sql = "UPDATE member set iFBId='".$user."', vImage='".$imgname."', vCountry='".$country_short."',vCity='".$city."',vFbFriendCount='".$friends_count."' WHERE iMemberId='".$_SESSION['sess_iMemberId']."'";
             $sql = "UPDATE member set iFBId='".$user."', vImage='".$imgname."',vFbFriendCount='".$friends_count."' WHERE iMemberId='".$_SESSION['sess_iMemberId']."'";
             $obj->sql_query($sql); 
            
             $db_sql = "select * from member WHERE iMemberId='".$_SESSION['sess_iMemberId']."'";
             $db_data = $obj->MySQLSelect($db_sql);
             $_SESSION["sess_vImage"]= $db_data[0]['vImage'];		
            
            
    	     header("Location:".$tconfig["tsite_url"]."my-account");
             exit;
    
        }else{
    	  
    	   $myfriends = $facebook->api( array(
                     'method' => 'fql.query',
                     'query' => 'SELECT friend_count FROM user WHERE uid ='.$user.'',
             ));
    
           $friends_count = $myfriends[0]["friend_count"];
    		
           $sql = "INSERT INTO member (iFBId, vFBUsername, vImage, vFirstName, vLastName, vEmail, dAddedDate, eStatus, vCity, vCountry,vFbFriendCount,vLanguageCode) VALUES ('".$user."', '".$user_profile['username']."','".$user_photo['picture']['data']['url']."', '".$user_profile['first_name']."', '".$user_profile['last_name']."', '".$user_profile['email']."', '".date("Y-m-d H:i:s")."', 'Active', '".$city."','".$country_short."','".$friends_count."','EN')";
           $id =  $obj->MySQLInsert($sql);
           
           $_SESSION['sess_iMemberId']=$id;		
    	   $_SESSION["sess_vFirstName"]=$user_profile['first_name'];
    	   $_SESSION["sess_vLastName"]=$user_profile['last_name'];
    	   $_SESSION["sess_vEmail"]=$user_profile['email'];  
    	   $_SESSION["sess_eGender"]=$user_profile[0]['eGender'];
    	   
    	   $Photo_Gallery_folder = $tconfig["tsite_upload_images_member_path"].$_SESSION['sess_iMemberId']."/";
    	   unlink($Photo_Gallery_folder.$db_user[0]['vImage']);
    	   unlink($Photo_Gallery_folder."1_".$db_user[0]['vImage']);
    	   unlink($Photo_Gallery_folder."2_".$db_user[0]['vImage']);
    	   unlink($Photo_Gallery_folder."3_".$db_user[0]['vImage']);   
    	   unlink($Photo_Gallery_folder."4_".$db_user[0]['vImage']);   
    
           if(!is_dir($Photo_Gallery_folder))
           {
    	      mkdir($Photo_Gallery_folder, 0777);
    	   }
    	              
           $baseurl =  "http://graph.facebook.com/".$user."/picture?type=large";
           $url = $user.".jpg";
           $image_name =  system("wget -O ".$Photo_Gallery_folder.$url." ".$baseurl);
           //$imgname=$generalobj->general_upload_image1($url,$Photo_Gallery_folder,$tconfig["tsite_upload_images_member_size1"],$tconfig["tsite_upload_images_member_size2"],$tconfig["tsite_upload_images_member_size3"],$tconfig["tsite_upload_images_member_size4"],'','','','Y','');
          
           
           if(is_file($Photo_Gallery_folder.$url))
          {
             include_once(TPATH_LIBRARIES."/general/SimpleImage.class.php");
             $img = new SimpleImage();           
             list($width, $height, $type, $attr)= getimagesize($Photo_Gallery_folder.$url);           
    
             if($width < $height){
                $final_width = $width;
             }else{
                $final_width = $height;
             }       
             $img->load($Photo_Gallery_folder.$url)->crop(0, 0, $final_width, $final_width)->save($Photo_Gallery_folder.$url);
             $imgname = $generalobj->img_data_upload($Photo_Gallery_folder,$url,$Photo_Gallery_folder, $tconfig["tsite_upload_images_member_size1"], $tconfig["tsite_upload_images_member_size2"], $tconfig["tsite_upload_images_member_size3"],"");                       
          }  
    	     @unlink($Photo_Gallery_folder.$url);
    	   //list($width, $height, $type, $attr)= getimagesize($Photo_Gallery_folder.'2_'.$imgname);           
             
           //if($width < $height){
             //$final_width = $width;
           //}else{
            //$final_width = $height;
           //}
             
            //$img->load($Photo_Gallery_folder.'2_'.$imgname)->crop(0, 0, $final_width, $final_width)->save($Photo_Gallery_folder.'1_'.$imgname);
         		
            $sql = "UPDATE member set  vImage='".$imgname."' WHERE iMemberId='".$_SESSION['sess_iMemberId']."'";
            $obj->sql_query($sql); 
            
             $db_sql = "select * from member WHERE iMemberId='".$_SESSION['sess_iMemberId']."'";
             $db_data = $obj->MySQLSelect($db_sql);
             $_SESSION["sess_vImage"]= $db_data[0]['vImage'];
             $_SESSION["sess_eGender"]=$db_data[0]['eGender'];
             
             $Data_pref['iMemberId'] = $id;
             $Data_pref['eChattiness'] = 'NO';
             $Data_pref['eMusic'] = 'NO';
             $Data_pref['eSmoking'] = 'NO';
             $Data_pref['ePets'] = 'NO';
             $id_pref = $obj->MySQLQueryPerform("member_car_preferences",$Data_pref,'insert');
             
             $Data_not['iMemberId'] = $id;
             $Data_not['eSuccessPublish'] = 'Yes';
             $Data_not['eSuccessUpdate'] = 'Yes';
             $Data_not['ePrivateMessage'] = 'Yes';
             $Data_not['eRatePassenger'] = 'Yes';
             $Data_not['eNewRating'] = 'Yes';
             $Data_not['eOtherInformation'] = 'Yes';
             $Data_not['dAddedDate'] = date("Y-m-d H:i:s");
             $id_not = $obj->MySQLQueryPerform("member_email_notification",$Data_not,'insert');
           
            header("Location:".$tconfig["tsite_url"]."my-account");
            exit;
        }
      } catch (FacebookApiException $e) {
        error_log($e);
        $user = null;
      }
    
    }
}

if($ctype == "fbphoto"){
    if ($user) {
    
      try {
        // Proceed knowing you have a logged in user who's authenticated.
       $user_profile = $facebook->api('/me?fields=id,picture');
    
        $sql = "SELECT iMemberId,vImage FROM member WHERE vEmail='".$user_profile['email']."'";
        $db_user = $obj->MySQLSelect($sql);
    		
		$Photo_Gallery_folder = $tconfig["tsite_upload_images_member_path"].$_SESSION['sess_iMemberId']."/";
		
	    @unlink($Photo_Gallery_folder.$db_user[0]['vImage']);
		@unlink($Photo_Gallery_folder."1_".$db_user[0]['vImage']);
		@unlink($Photo_Gallery_folder."2_".$db_user[0]['vImage']);
		@unlink($Photo_Gallery_folder."3_".$db_user[0]['vImage']);   
		@unlink($Photo_Gallery_folder."4_".$db_user[0]['vImage']);   
		        
        $baseurl =  "http://graph.facebook.com/".$user."/picture?type=large";
        $url = $user.".jpg";
        $image_name =  system("wget -O ".$Photo_Gallery_folder.$url." ".$baseurl);
        if(!is_dir($Photo_Gallery_folder))
        {
	      mkdir($Photo_Gallery_folder, 0777);
	    }
	              
        $baseurl =  "http://graph.facebook.com/".$user."/picture?type=large";
        $url = $user.".jpg";
        $image_name =  system("wget -O ".$Photo_Gallery_folder.$url." ".$baseurl);
        
        if(is_file($Photo_Gallery_folder.$url))
        {
           include_once(TPATH_LIBRARIES."/general/SimpleImage.class.php");
           $img = new SimpleImage();           
           list($width, $height, $type, $attr)= getimagesize($Photo_Gallery_folder.$url);           
  
           if($width < $height){
              $final_width = $width;
           }else{
              $final_width = $height;
           }       
           $img->load($Photo_Gallery_folder.$url)->crop(0, 0, $final_width, $final_width)->save($Photo_Gallery_folder.$url);
           $imgname = $generalobj->img_data_upload($Photo_Gallery_folder,$url,$Photo_Gallery_folder, $tconfig["tsite_upload_images_member_size1"], $tconfig["tsite_upload_images_member_size2"], $tconfig["tsite_upload_images_member_size3"],"");                       
        }  
         @unlink($Photo_Gallery_folder.$url);
         
         $sql = "UPDATE member set vImage='".$imgname."' WHERE iMemberId='".$_SESSION['sess_iMemberId']."'";
         $obj->sql_query($sql); 
        
         $db_sql = "select * from member WHERE iMemberId='".$_SESSION['sess_iMemberId']."'";
         $db_data = $obj->MySQLSelect($db_sql);
         $_SESSION["sess_vImage"]= $db_data[0]['vImage'];		
        
        
	     header("Location:".$tconfig["tsite_url"]."profile-photo");
         exit;
    

      } catch (FacebookApiException $e) {
        error_log($e);
        $user = null;
      }
    
    }
}

if($ctype == "fbsocial"){

    if ($user) {
    
      try {
        // Proceed knowing you have a logged in user who's authenticated.
       $user_profile = $facebook->api('/me?fields=id,picture,username');
      
	     $fbusername= $user_profile['username'];
     
         $sql = "UPDATE member set iFBId='".$user."',vFBUsername='".$fbusername."' WHERE iMemberId='".$_SESSION['sess_iMemberId']."'";
         $obj->sql_query($sql); 
              
         header("Location:".$tconfig["tsite_url"]."social-sharrings");
         exit;
    
      } catch (FacebookApiException $e) {
        error_log($e);
        $user = null;
      }
    
    }

}


// Login or logout url will be needed depending on current user state.
if ($user) {
  $logoutUrl = $facebook->getLogoutUrl();
  $user_friends = $facebook->api('/me/friends');
  //$friends_count = count($data['data']);
} else {
    $params = array(
      'scope' => 'email,user_about_me,publish_stream',
      //'redirect_uri'=>$tconfig['tsite_url'].'/fbconnect.php?ctype='.$ctype
      'redirect_uri'=>'http://www.webprojectsdemo.com/bcar/fbconnect.php?ctype='.$ctype
      //'redirect_uri'=>'http://www.balticcar.co/fbconnect.php?ctype='.$ctype
    );
  $loginUrl = $facebook->getLoginUrl($params);
  header("Location:".$loginUrl);
  exit;  
}
?>




